/*------------------------------------------------------------------------------
 *  Date            Project             Developer                       Justification
 *  07/25/2017  Invoicing-Phase2        Vinayak Sharma, REI Systems     Creation
 *  07/25/2017  Invoicing-Phase2        Vinayak Sharma, REI Systems     Use to get the count of all Firm fixed price Invoice Line Items associated with Invoices 
 * -----------------------------------------------------------------------------
*/  
    public class InnerInvLineItemClass{    
        /*recCount acts as a index for a row. This will be helpful to identify the row to be deleted */
        public String recInvLineItemCount {get;set;}        
        public Invoice_Line_Item__c  invLineItem {get;set;}
        public string paymentamount{get;set;}
        public innerInvLineItemClass(Integer intCount){
          recInvLineItemCount = String.valueOf(intCount);             
          /*create a new account*/
          invLineItem = new Invoice_Line_Item__c ();           
        }
        
    }