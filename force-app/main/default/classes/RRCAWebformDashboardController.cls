/*
* Class : RRCAWebformDashboardController
* Author : Kalyan Vuyyuru (PCORI)
* Version History : 1.0
* Creation : 4/25/2017
* Last Modified By: Kalyan Vuyyuru (PCORI)
* Last Modified Date: 5/25/2017
* Modification Description:Just aligned the text and ensured coverage is proper and added the custom labels
* Description : This Class is to ensure the Proposal submitted by the Project Lead is submitted as application and is available to PI
to submit in full and AO to review and approve.
*/
public class RRCAWebformDashboardController {
    
    public list<Opportunity> ot{get;set;}
    public Id roprt{get;set;}
    User u=[select contactid from User where id=:UserInfo.getUserId()];
    Id CntId=U.ContactId;
    Contact c=[select email from contact where id=:cntId];
    Id rcrdTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RRCA').getRecordTypeId();
    Id rcrdTypeId1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RRCA Locked').getRecordTypeId();
    Id rcrdTypeId2 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RRCA Unlocked').getRecordTypeId();
    
    public RRCAWebformDashboardController(){
        
        try{
            ot= [select Account.Name,AccountId,Name,StageName,Project_Lead_Email_Address__c,AO_Email__c,RecordTypeId,Project_Lead_First_Nam__c,Project_Lead_Last_Name__c from opportunity where (RecordtypeId=:rcrdTypeId OR RecordtypeId=:rcrdTypeId1 OR RecordtypeId=:rcrdTypeId2) AND (Project_Lead_Email_Address__c=:c.email OR AO_Email__c=:c.email)];
            if(!(ot!=null && ot.size()>0)){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.RRCA_No_Projects));
            }
        }
        
        catch(exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.RRCA_No_Projects));
        }
    }
    
    
    public List<Opportunity> getrrcaoops() {
        
        
        List<Opportunity> ro=[select Account.Name,AccountId,Name,StageName,Project_Lead_First_Nam__c,Project_Lead_Last_Name__c from opportunity where (RecordtypeId=:rcrdTypeId OR RecordtypeId=:rcrdTypeId1 OR RecordtypeId=:rcrdTypeId2)AND (Project_Lead_Email_Address__c=:c.email OR AO_Email__c=:c.email)];
        
        if(ro.size()>0){
            return ro;
        }
        else{
            return null;
        }
    }
    
    
    public pagereference editrrcaopt(){
        
        String urlrrca1='/'+roprt;
        PageReference result1= new PageReference(urlrrca1);
        return result1;
        
        
        
    }
    
    public pagereference viewrrcaopt(){
        
        String urlrrca2='/'+roprt;
        PageReference result2= new PageReference(urlrrca2);
        return result2;
        
    }
    
}