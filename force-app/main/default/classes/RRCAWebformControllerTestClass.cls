/*
* Class : RRCAWebformControllerTestClass
* Author : Kalyan Vuyyuru (PCORI)
* Version History : 1.0
* Creation : 4/25/2017
* Last Modified By: Kalyan Vuyyuru (PCORI)
* Last Modified Date: 5/24/2017
* Description : This Class is to ensure proper coverage for RRCAWebformController
*/

@IsTest
public class RRCAWebformControllerTestClass {
    
    static testmethod void RRCAWebformControllerMethod1 (){
        
        Id r1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RRCA').getRecordTypeId();
        Id r2 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RRCA Locked').getRecordTypeId();
        Id r3 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RRCA Unlocked').getRecordTypeId();
        
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt;
        acnt.IsPartner=true;
        Contact con = new Contact(LastName ='PIUser',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA',email='piuser546@yopmail.com');  
        insert con;  
        User user = new User(alias = 'test123', email='piuser546@yopmail.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con.Id,
                             timezonesidkey='America/New_York', username='piuser546@yopmail.com');
        
        insert user;
        
        RRCAWebformController rwfc= new RRCAWebformController();
        
        rwfc.AccntId=acnt.Id;
        rwfc.City='test city';
        rwfc.Country='USA';
        rwfc.Email='piuser546n7@yopmail.com';
        rwfc.FirstName='Kalyan';
        rwfc.LastName='RRCA';
        rwfc.Org='ksg corp';
        rwfc.Phone='123-234-3456';
        rwfc.ProjectName='Test Project';
        rwfc.recordTypeId=r1;
        rwfc.State='VA';
        rwfc.Street='test street';
        rwfc.ZipCode='20191';
        rwfc.Submit(); 
    }
    
    static testmethod void RRCAWebformControllerMethod2 (){
        
        Id r1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RRCA').getRecordTypeId();
        Id r2 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RRCA Locked').getRecordTypeId();
        Id r3 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RRCA Unlocked').getRecordTypeId();
        
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt;
        acnt.IsPartner=true;
        Contact con = new Contact(LastName ='PIUser',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA',email='piusergk546@yopmail.com');  
        insert con;  
        User user = new User(alias = 'test123', email='piusergk546@yopmail.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con.Id,
                             timezonesidkey='America/New_York', username='piuser546gk@yopmail.com');
        
        insert user;
        
        
        RRCAWebformController rwfc= new RRCAWebformController();
        
        
        rwfc.Submit();
        rwfc.City='';
        rwfc.Country='';
        rwfc.Email='';
        rwfc.FirstName='';
        rwfc.LastName='';
        rwfc.Org='';
        rwfc.Phone='';
        rwfc.ProjectName='';
        rwfc.State='';
        rwfc.Street='';
        rwfc.ZipCode='';
        rwfc.Submit();
        
    }
    
    static testmethod void RRCAWebformControllerMethod3 (){
        
        Id r1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RRCA').getRecordTypeId();
        Id r2 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RRCA Locked').getRecordTypeId();
        Id r3 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RRCA Unlocked').getRecordTypeId();
        
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt;
        acnt.IsPartner=true;
        Contact con = new Contact(LastName ='PIUser',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA',email='piuserkt546@yopmail.com');  
        insert con;  
        User user = new User(alias = 'test123', email='piuser546@yopmail.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con.Id,
                             timezonesidkey='America/New_York', username='piuserkt546@yopmail.com');
        
        insert user;
        
        Opportunity o= new opportunity();
        
        o.Awardee_Institution_Organization__c=acnt.Id;
        o.RecordTypeId=r1;
        o.Project_Lead_Email_Address__c='piuserkt546@yopmail.com';
        o.Full_Project_Title__c='test';
        o.Project_Name_off_layout__c='test';
        o.CloseDate=Date.today();
        o.Application_Number__c='Ra-1234';
        o.StageName='In-Process';
        o.Name='test';
        o.PI_Name_User__c=user.Id;
        insert o;
        
        RRCAWebformController rwfc= new RRCAWebformController();
        
        rwfc.AccntId=acnt.Id;
        rwfc.City='test city';
        rwfc.Country='USA';
        rwfc.Email='piuserkt546@yopmail.com';
        rwfc.FirstName='Kalyan';
        rwfc.LastName='RRCA';
        rwfc.Org='Test Account';
        rwfc.Phone='123-234-3456';
        rwfc.ProjectName='Test Project';
        rwfc.recordTypeId=r1;
        rwfc.State='VA';
        rwfc.Street='test street';
        rwfc.ZipCode='20191';
        rwfc.Submit(); 
    }
    
    
}