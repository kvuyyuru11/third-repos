public with sharing class BudgetSummaryTableController {
    Budget__c budget;

    public List<BudgetSummary> budgetSummaryList {
        get {
            return BudgetSummary.getBudgetSummaryList(this.budget);
        }
    }

    /* Returns a list for iterating through the years the budget covers in the VisualForce page.
       This is preferred to using <apex:variable> as using <apex:variable> as a counter is not supported.
    */
    public List<String> years{
        get{
            if (years == null) {
                years = new List<String>();
                for(Integer i = 1; i <= Integer.valueOf(System.Label.Max_Years_Budget_Covers); i++) {
                    years.add(String.valueOf(i));
                }
            }
            return years;
        }
        private set;
    }

    public Boolean hasPeerReviewPeriod {
        get {
            return this.budget.Associated_App_Project__r.Campaign.Name != System.Label.Dissemination_and_Implementation;
        }
    }

    public BudgetSummaryTableController(ApexPages.StandardController ctr) {
        this.budget = [SELECT Id, Personnel_Costs_Year_1__c, Personnel_Costs_Year_2__c,
                              Personnel_Costs_Year_3__c, Personnel_Costs_Year_4__c, Personnel_Costs_Year_5__c,
                              Personnel_Costs_Year_6__c, Consultant_Costs_Year_1__c, Consultant_Costs_Year_2__c,
                              Consultant_Costs_Year_3__c, Consultant_Costs_Year_4__c, Consultant_Costs_Year_5__c,
                              Consultant_Costs_Year_6__c, Supplies_Costs_Year_1__c, Supplies_Costs_Year_2__c,
                              Supplies_Costs_Year_3__c, Supplies_Costs_Year_4__c, Supplies_Costs_Year_5__c,
                              Supplies_Costs_Year_6__c, Scientific_Travel_Costs_Year_1__c, Scientific_Travel_Costs_Year_2__c,
                              Scientific_Travel_Costs_Year_3__c, Scientific_Travel_Costs_Year_4__c,
                              Scientific_Travel_Costs_Year_5__c, Scientific_Travel_Costs_Year_6__c,
                              Programmatic_Travel_Costs_Year_1__c, Programmatic_Travel_Costs_Year_2__c,
                              Programmatic_Travel_Costs_Year_3__c, Programmatic_Travel_Costs_Year_4__c,
                              Programmatic_Travel_Costs_Year_5__c, Programmatic_Travel_Costs_Year_6__c,
                              Other_Expenses_Year_1__c, Other_Expenses_Year_2__c, Other_Expenses_Year_3__c,
                              Other_Expenses_Year_4__c, Other_Expenses_Year_5__c, Other_Expenses_Year_6__c,
                              Equipment_Costs_Year_1__c, Equipment_Costs_Year_2__c, Equipment_Costs_Year_3__c,
                              Equipment_Costs_Year_4__c, Equipment_Costs_Year_5__c, Equipment_Costs_Year_6__c,
                              Direct_Subcontractor_Costs_Year_1__c, Direct_Subcontractor_Costs_Year_2__c,
                              Direct_Subcontractor_Costs_Year_3__c, Direct_Subcontractor_Costs_Year_4__c,
                              Direct_Subcontractor_Costs_Year_5__c, Direct_Subcontractor_Costs_Year_6__c,
                              Subcontractor_Indirect_Costs_Year_1__c, Subcontractor_Indirect_Costs_Year_2__c,
                              Subcontractor_Indirect_Costs_Year_3__c, Subcontractor_Indirect_Costs_Year_4__c,
                              Subcontractor_Indirect_Costs_Year_5__c, Subcontractor_Indirect_Costs_Year_6__c,
                              Grand_Total_Year_1__c, Grand_Total_Year_2__c, Grand_Total_Year_3__c, Grand_Total_Year_4__c,
                              Grand_Total_Year_5__c, Grand_Total_Year_6__c, Total_Direct_Costs_Year_1__c, Total_Direct_Costs_Year_2__c,
                              Total_Direct_Costs_Year_3__c, Total_Direct_Costs_Year_4__c, Total_Direct_Costs_Year_5__c,
                              Total_Direct_Costs_Year_6__c, Subtotal_Direct_Costs_Year_1__c, Subtotal_Direct_Costs_Year_2__c,
                              Subtotal_Direct_Costs_Year_3__c, Subtotal_Direct_Costs_Year_4__c, Subtotal_Direct_Costs_Year_5__c,
                              Subtotal_Direct_Costs_Year_6__c, Total_Prime_Indirect_Costs_Year_1__c,
                              Total_Prime_Indirect_Costs_Year_2__c, Total_Prime_Indirect_Costs_Year_3__c,
                              Total_Prime_Indirect_Costs_Year_4__c, Total_Prime_Indirect_Costs_Year_5__c,
                              Total_Prime_Indirect_Costs_Year_6__c, Final_Grand_Total__c, Associated_App_Project__r.Campaign.Name
                        FROM Budget__c
                        WHERE Id = : ctr.getRecord().Id];
    }
}