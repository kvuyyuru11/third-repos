/*
    Author     : Vijaya Kumar Sripathi
    Date       : 14th Dec 2016
    Name       : AccountEditRedirectController 
    Description: 
*/
public class AccountEditRedirectController 
{
    public ID usrProfileId;
    public Account objAcc;
    public AccountEditRedirectController(ApexPages.StandardController controller)
    {
        this.objAcc = (Account)controller.getRecord();
        usrProfileId = userInfo.getProfileId();
    }
    
    public pagereference accEditRedirect()
    {
        if (usrProfileId == Label.MRA_Profile)
        {
            return new PageReference('/AccountEditRestricted');
        }
        else
        {
            PageReference pgRef = new PageReference('/' + objAcc.Id + '/e');
            pgRef.setRedirect(true);
            return pgRef ;
        }          
	}
}