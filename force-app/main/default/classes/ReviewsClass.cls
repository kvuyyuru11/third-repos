/*
* Class : ReviewsClass
* Author : Kalyan Vuyyuru (Accenture)
* Version History : 1.0
* Creation : 1/27/2017
* Last Modified By: Sowmya Goli (Accenture)
* Last Modified Date: 03/20/2017
* Modification Description: Updated Code to cover all functionalities
* Description : This class takes care of displaying the Application Review Downloads data based on the record type and the project or application selected.
*/
public class ReviewsClass 
{
    public FC_Reviewer__External_Review__c rvs {get;set;}
    public string baseURL {get; set;}
    
    public Map<String, String> OnlineReview {get; set;}
    public Map<String, String> InPersonReview {get; set;}
    
    public ReviewsClass()
    {        
        //Getting the id of the record which is nothing but the review record id from the DownLoadsTABforReviewsController
        Id ReviewId = ApexPages.currentPage().getParameters().get('Id');
        
        //Below Soql query full the Application download and Combined critique download and displays the data as hyper links on pages related to the respective record types selected.
        rvs = [select Id,Combined_Online_Critique_Download__c,Application_Download_R2__c,Cycle__c,Campaign_Name__c,
               FC_Reviewer__Opportunity__r.Campaign.Online_Review_Resources__c,PI_Project_Lead_1_Name__c, FC_Reviewer__Opportunity__r.Application_Number__c 
               from FC_Reviewer__External_Review__c 
               where Id=:ReviewId];
        
        try
        {
            OnlineReview = new Map<String,String>();
            InPersonReview = new Map<String,String>();
            List<String> onlineReviewFieldValues = new List<String>();
            List<String> InPersonReviewFieldValue = new List<String>();
            
            List<FC_Reviewer__External_Review__c> anchorList2 = [select FC_Reviewer__Opportunity__r.Campaign.Online_Review_Resources__c, 
                                                                 FC_Reviewer__Opportunity__r.Campaign.In_Person_Review__c 
                                                                 from FC_Reviewer__External_Review__c 
                                                                 where Id =:ReviewId];
            
            for( FC_Reviewer__External_Review__c m:anchorList2)
            {
                onlineReviewFieldValues.add(m.FC_Reviewer__Opportunity__r.Campaign.Online_Review_Resources__c);
                InPersonReviewFieldValue.add(m.FC_Reviewer__Opportunity__r.Campaign.In_Person_Review__c);
            }
            
            List<String> links = onlineReviewFieldValues[0].split('\\;');
            for(integer i=0;i < links.size();i++)
            {
                List<String> nameWithLinks = links.get(i).split('\\*');
                String url = nameWithLinks.get(0);
                String title = nameWithLinks.get(1);
                OnlineReview.put(url,title);
                
            }   
            
            List<String> links2 = InPersonReviewFieldValue[0].split('\\;');
            for(integer i=0;i < links2.size();i++)
            {
                List<String> nameWithLinks2 = links2.get(i).split('\\*');
                String url2 = nameWithLinks2.get(0);
                String title2 = nameWithLinks2.get(1);
                InPersonReview.put(url2,title2);
            }   
        }
        catch (exception e) {
            system.debug('The exception is =====>' + e);
        }
    }
}