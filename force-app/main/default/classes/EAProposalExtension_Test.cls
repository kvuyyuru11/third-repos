/****************************************************************************************************
*Description:           This is the test class for the EAProposalWebform extension
*                                         
*                       
*
*Required Class(es):    N/A
*
*Organization: 
*
*Revision   |Date       |Author             |AdditionalNotes
*====================================================================================================
*   1.0     07/07/2014   Justin Padilla     Initial Version
*****************************************************************************************************/
@isTest
private class EAProposalExtension_Test {

    static testMethod void VisualforcePageTest()
    {
        Lead l = new Lead();
        Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(l);
        
        EAProposalExtension page = new EAProposalExtension(controller);
        //Attempt to submit with errors first
        page.Submit();
        //Now complete the form and submit
        page.FirstName = 'Justin1';
        page.LastName = 'Padilla1';
        page.Org = 'Rainmaker-LLC';
        page.Email = 'jpadillaXXX@rainmaker-llc.com';
        page.Phone = '410-555-5555';
        page.Street = 'Street';
        page.City = 'City';
        page.State = 'MD';
        page.ZipCode = '12345';
        page.Country = 'Country';
        page.ProjectName = 'Project Name';
        page.Submitted = page.Submitted;
        page.Submit();
    }
}