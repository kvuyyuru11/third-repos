@isTest
private class updateOriginalAmountTest
{
static testMethod void Test1() 
{

RecordType r1=[Select Id from RecordType where name=:'Research Awards' limit 1];
Account a = new Account();
a.Name = 'Acme1';
insert a;

Id profileId = [select id from profile where name='PCORI Community Partner'].id;  
Contact con = new Contact(LastName ='testCon',AccountId = a.Id);  
insert con;  

User user = new User(alias = 'test123', email='test123fvb@noemail.com',
emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,   
contactId = con.Id,
timezonesidkey='America/New_York', username='testerfvb1@noemail.com');
insert user;

Id profileId1 = [select id from profile where name='PCORI Community Partner'].id;  
Contact con1 = new Contact(LastName ='testCon1',AccountId = a.Id);  
insert con1;
User user1 = new User(alias = 'test1234', email='test1234fvb@noemail.com',
emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
localesidkey='en_US', profileid = profileId1 , country='United States',IsActive = true,   
contactId = con1.Id,
timezonesidkey='America/New_York', username='testerfvb123@noemail.com');
insert user1;

Id profileId2 = [select id from profile where name='PCORI Community Partner'].id;  
Contact con2 = new Contact(LastName ='testCon1',AccountId = a.Id);  
insert con2;
User user2 = new User(alias = 'test145', email='test12345fvb@noemail.com',
emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
localesidkey='en_US', profileid = profileId2 , country='United States',IsActive = true,   
contactId = con2.Id,
timezonesidkey='America/New_York', username='testerfvb12@noemail.com');
insert user2;

Id profileId3 = [select id from profile where name='PCORI Community Partner'].id;  
Contact con3 = new Contact(LastName ='testCon2',AccountId = a.Id);  
insert con3;

User user3 = new User(alias = 'test52', email='test135fvb@noemail.com',
emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
localesidkey='en_US', profileid = profileId3 , country='United States',IsActive = true,   
contactId = con3.Id,
timezonesidkey='America/New_York', username='testerfvb82@noemail.com');
insert user3;


Cycle__c c = new Cycle__c();
c.Name = 'testcycle';
c.COI_Due_Date__c = date.valueof(System.now());
insert c;

Panel__c p = new Panel__c();
p.name= 'testpanel';
p.Cycle__c= c.id ;
p.MRO__c = user2.id;
p.Panel_Due_Dtae__c=  Date.newInstance(2019,12,20);
insert p;


opportunity o1=new opportunity();

o1.Awardee_Institution_Organization__c=a.Id;
o1.RecordTypeId=r1.Id;
o1.Full_Project_Title__c='test5';
o1.Project_Name_off_layout__c='test5';
//o.Panel__c=p.Id;
o1.CloseDate=Date.today();
o1.Application_Number__c='Ra-12354';
o1.StageName='Programmatic Review';
o1.Name='test';
o1.PI_Name_User__c=user.id;
o1.AO_Name_User__c=user1.id;
o1.Project_Start_Date__c=date.newInstance(2017, 06, 10);

insert o1;
List<Budget__c> listOfBudgets = new List<Budget__c>();



Budget__c b5 = new Budget__c();
b5.Associated_App_Project__c= o1.id;
b5.Budget_Status__c= 'Approved for Contract';
b5.Personnel_Costs_Year_1__c = 1000; 
b5.Bypass_Flow__c=true;
insert b5;
o1.StageName = 'Executed';
//o1.Contract_Number__c = '12345';
//o1.AccountId = a.Id;
//o1.CampaignId =
update o1;
/*
listOfBudgets.add(b5);
Budget__c b3 = new Budget__c();
b3.Associated_App_Project__c= o1.id;
b3.Budget_Status__c= 'Executed';
b3.Personnel_Costs_Year_1__c = 1000; 
b3.Bypass_Flow__c=true;

listOfBudgets.add(b3);

Budget__c b4 = new Budget__c();
b4.Associated_App_Project__c= o1.id;
b4.Budget_Status__c= 'PIR Approved Budget';
b4.Personnel_Costs_Year_1__c = 1000; 
b4.Bypass_Flow__c=true;

listOfBudgets.add(b4);
insert listOfBudgets;
b2.Budget_Status__c= 'Executed';
b5.Budget_Status__c = 'Application Budget';
List<Budget__c> listOfBudgetsToUpdate = new List<Budget__c >();
listOfBudgetsToUpdate.add(b2);
listOfBudgetsToUpdate.add(b5);
update listOfBudgetsToUpdate;*/



}
}