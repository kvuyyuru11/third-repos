public class FluxxLOIReviewsCntrlrExt {
    public Fluxx_LOI_Review_Form__c curReview;
    public Id curReviewId;
    public RecordType curRecordType = new RecordType();
    public String curRecordTypeName {get; set;}
    public boolean Cycle22016 {get; set;}
    public boolean preCycle22016 {get; set;}
	public boolean Cycle32016 {get; set;}
	public boolean APDTO_tPFA_MS {get; set;}	//used to render MS page
    public boolean APDTO_tPFA_Opioids {get; set;}	//used to render MS page
    public boolean Cycle32016_MS_Only {get; set;}
    public boolean Cycle32016_Opioids_Only {get; set;}
    
    public FluxxLOIReviewsCntrlrExt(ApexPages.StandardController stdController) {
        preCycle22016 = false;
        Cycle22016 = false;
        Cycle32016 = false;
 		APDTO_tPFA_MS = false;
        APDTO_tPFA_Opioids = false;
        
             system.debug('\r\n******** Cycle32016 is ' + Cycle32016 + '\r\n******** Cycle22016 is ' + Cycle22016 + 
                          '\r\n******** preCycle22016 is ' + preCycle22016 + '\r\n tPFA MS is ' + APDTO_tPFA_MS + 
                          '\r\n tPFA Opioids is ' + APDTO_tPFA_Opioids);
        
        curReview = (Fluxx_LOI_Review_Form__c)stdController.getRecord();
        curReviewId = ApexPages.currentPage().getParameters().get('id');      
        curRecordType = [SELECT Name, Id FROM RecordType where id = :curReview.RecordTypeId and IsActive=true limit 1];
        curRecordTypeName = curRecordType.Name;
        
		//For APDTO find out which tPFA  we're working with
        if (curReview.PFA__c == 'APDTO' && curReview.PFA_Type__c == 'Targeted') {
            if (curReview.Targeted_PFA__c == 'Multiple Sclerosis (MS)') {
                APDTO_tPFA_MS = true;
            } else {
                 if (curReview.Targeted_PFA__c == 'Opioids') {
                    APDTO_tPFA_Opioids = true;
                }               
            }           
        }       
        
        if (curReview.Cycle__c == 'Cycle 2 2016') {
            Cycle22016 = true;
        } else {
            if (curReview.Cycle__c  == 'Cycle 3 2016') {
                Cycle32016 = true;
            } else {
                preCycle22016 = true;                
            }
        }
         Cycle32016_MS_Only = APDTO_tPFA_MS && Cycle32016;
         Cycle32016_Opioids_Only = APDTO_tPFA_Opioids && Cycle32016;
        
             system.debug('\r\n******** Cycle32016 is ' + Cycle32016 + '\r\n******** Cycle22016 is ' + Cycle22016 + 
                          '\r\n******** preCycle22016 is ' + preCycle22016 + '\r\n tPFA MS is ' + APDTO_tPFA_MS + 
                          '\r\n tPFA Opioids is ' + APDTO_tPFA_Opioids +
                         	'\r\n Cycle32016_MS_Only is ' + Cycle32016_MS_Only +
                         '\r\n Cycle32016_Opioids_Only is ' + Cycle32016_Opioids_Only);
        system.debug('\r\n******** Current Review: ' + JSON.serializePretty(curReview) + '\r\n');
       
    }
    public pageReference editPage() {
        PageReference reviewEdit;
         if (curRecordTypeName == 'Pragmatic') {
            reviewEdit =  new PageReference('/apex/FluxxLOIsPragmaticReviewsEditForm?id=' + curReviewId);
         } else {
             if (curRecordTypeName == 'Targeted'){
             	reviewEdit =  new PageReference('/apex/FluxxLOIsTargetedReviewsEditForm?id=' + curReviewId);
             } else {
                 if (curRecordTypeName == 'CDR Targeted'){
             			reviewEdit =  new PageReference('/apex/FluxxLOIsCDRTargetedReviewsEditForm?id=' + curReviewId);
                 } else {
                      if (curRecordTypeName == 'AD Targeted'){
                            reviewEdit =  new PageReference('/apex/FluxxLOIsADTargetedReviewsEditForm?id=' + curReviewId);
                      }  else {
                      		if (curRecordTypeName == 'IHS Targeted'){
                            	reviewEdit =  new PageReference('/apex/FluxxLOIsIHSTargetedReviewsEditForm?id=' + curReviewId);
                      		}
                      } //end if (curRecordTypeName == 'AD Targeted')
                 }  //end if (curRecordTypeName == 'CDR Targeted')       
             } //end if (curRecordTypeName == 'Targeted')               
         } //end if (curRecordTypeName == 'Pragmatic')        
        reviewEdit.setRedirect(true);
        return reviewEdit;               
    }

    public pageReference saveEdits() {
        PageReference saveEdit = null;
        try {
             update curReview;
           /* 
             if (curRecordTypeName == 'Pragmatic') {
                saveEdit =  new PageReference('/apex/FluxxLOIsPragmaticReviewsDetailForm?id=' + curReviewId);
             } else if (curRecordTypeName == 'Targeted'){
                 saveEdit =  new PageReference('/apex/FluxxLOIsTargetedReviewDetailForm?id=' + curReviewId);            
             }
          */
             if (curRecordTypeName == 'Pragmatic') {
                saveEdit =  new PageReference('/apex/FluxxLOIsPragmaticReviewsDetailForm?id=' + curReviewId);
             } else {
                 if (curRecordTypeName == 'Targeted'){
                    saveEdit =  new PageReference('/apex/FluxxLOIsTargetedReviewDetailForm?id=' + curReviewId);
                 } else {
                     if (curRecordTypeName == 'CDR Targeted'){
                            saveEdit =  new PageReference('/apex/FluxxLOIsCDRTargetedReviewDetailForm?id=' + curReviewId);
                     } else {
                          if (curRecordTypeName == 'AD Targeted'){
                                saveEdit =  new PageReference('/apex/FluxxLOIsADTargetedReviewDetailForm?id=' + curReviewId);
                          }  else {
                                if (curRecordTypeName == 'IHS Targeted'){
                                    saveEdit =  new PageReference('/apex/FluxxLOIsIHSTargetedReviewsDetailForm?id=' + curReviewId);
                                }
                      	  } //end if (curRecordTypeName == 'AD Targeted')                  
                     } //end if (curRecordTypeName == 'CDR Targeted')                
                 } //end if (curRecordTypeName == 'Targeted')               
             } //end if (curRecordTypeName == 'Pragmatic')           
             saveEdit.setRedirect(true);           
        } catch (exception e) {
             system.debug('An error occured--' +e);
            curReview.addError(e);           
        } 
        return saveEdit;                       
    }

    public pageReference cancelEdits() {
         PageReference cancelEdit;
        /*
         if (curRecordTypeName == 'Pragmatic') {
            cancelEdit =  new PageReference('/apex/FluxxLOIsPragmaticReviewsDetailForm?id=' + curReviewId);
         } else if (curRecordTypeName == 'Targeted'){
             cancelEdit =  new PageReference('/apex/FluxxLOIsTargetedReviewDetailForm?id=' + curReviewId);            
         }
        */
        
         if (curRecordTypeName == 'Pragmatic') {
            cancelEdit =  new PageReference('/apex/FluxxLOIsPragmaticReviewsDetailForm?id=' + curReviewId);
         } else {
             if (curRecordTypeName == 'Targeted'){
             	cancelEdit =  new PageReference('/apex/FluxxLOIsTargetedReviewDetailForm?id=' + curReviewId);
             } else {
                 if (curRecordTypeName == 'CDR Targeted'){
             			cancelEdit =  new PageReference('/apex/FluxxLOIsCDRTargetedReviewDetailForm?id=' + curReviewId);
                 } else {
                      if (curRecordTypeName == 'AD Targeted'){
                            cancelEdit =  new PageReference('/apex/FluxxLOIsADTargetedReviewDetailForm?id=' + curReviewId);
                      }  else {
                          	if (curRecordTypeName == 'IHS Targeted'){
                              	cancelEdit =  new PageReference('/apex/FluxxLOIsIHSTargetedReviewsDetailForm?id=' + curReviewId);
                          	}
                      } //end if (curRecordTypeName == 'AD Targeted')                     
                 } //end if (curRecordTypeName == 'CDR Targeted')                
             } //end if (curRecordTypeName == 'Targeted')               
         } //end if (curRecordTypeName == 'Pragmatic')
        
        cancelEdit.setRedirect(true);
        return cancelEdit;               
    }    
} // end class FluxxLOIReviewsCntrlrExt