/*
Application Inventory#: AI-0751
Requirement: 16973
*/
/*
* Class : CreateInPersonReview
* Author : Kalyan Vuyyuru (PCORI)
* Version History : 2.0
* Description : This class is called when the move to in person is clicked on the panel to create the in person preview records as 
many as the coi records on the panel who reported no conflict of interest.If in any case the coicount and review count doesnt match it will generate
which ones were deleted or if the count matches it will display error that records were already created.
*/
global class CreateInPersonReviewforAdmin {
    webService static String createInPersonRecord(String pId){
        set<Id> revscontid= new set<ID>();
        set<Id> revsopptid= new set<ID>();
        set<Id> contsid= new set<ID>();
        set<string> rvsstring= new set<string>();
        map<Id,Id> contactusermap= new map<Id,Id>();
        Map<String,String> coiandcontplusoptmap = new Map<String, String>();
        Map<String,String> rvsandcontplusoptmap = new Map<String, String>();
        
        Id rvwRecordTypeId1 = Schema.SObjectType.FC_Reviewer__External_Review__c.getRecordTypeInfosByName().get(Label.InPersonReview).getRecordTypeId();
        Id rvwRecordTypeId2 = Schema.SObjectType.FC_Reviewer__External_Review__c.getRecordTypeInfosByName().get(Label.InPersonPreview).getRecordTypeId();
        // RecordType rec = [SELECT Id FROM RecordType WHERE sObjectType = 'FC_Reviewer__External_Review__c' AND DeveloperName = 'In_Person_Preview'];
        List<FC_Reviewer__External_Review__c> rlist = [SELECT Id,FC_Reviewer__Contact__c,FC_Reviewer__Opportunity__c FROM FC_Reviewer__External_Review__c WHERE Panel__c = :pId  AND (RecordTypeId = :rvwRecordTypeId2 OR RecordTypeId = :rvwRecordTypeId1)];
        
        //  List<FC_Reviewer__External_Review__c> rvslstofinprsnreview = [SELECT Id,FC_Reviewer__Opportunity__c FROM FC_Reviewer__External_Review__c WHERE Panel__c = :pId AND RecordTypeId = :rvwRecordTypeId1];
        Panel__c panel = [SELECT InPerson_Review_Deadline__c,MRO__c,MRO__r.Email FROM Panel__c WHERE Id = :pId];    
        List<COI_Expertise__c> coilst = [SELECT Id, Related_Project__c, Reviewer_Name__c, Research_Application__c FROM COI_Expertise__c WHERE Panel_R2__c = :pId AND Active__c = true AND Conflict_of_Interest__c = 'No'];
        
        //From the coilist preparing out map to have coi id as key and a unique combination of contactid+oppId as value
        for(COI_Expertise__c c:coilst){
            contsid.add(c.Reviewer_Name__c);
            string s1=c.Reviewer_Name__c;
            String s2 = c.Related_Project__c;
            coiandcontplusoptmap.put(c.id,s1+s2);
        }
        //From the rlist preparing out map to have review id as key and a unique combination of contactid+oppId as value
        for(FC_Reviewer__External_Review__c r:rlist){
            revscontid.add(r.FC_Reviewer__Contact__c);
            string r1=r.FC_Reviewer__Contact__c;
            string r2=r.FC_Reviewer__Opportunity__c;
            string rkey=r1+r2;
            rvsstring.add(rkey);
        } 
        //preparing map to get contactId and respective userId
        list<User> userlistforMap=[select Id,ContactId from user where ContactId IN :contsid];
        for(User u:userlistforMap){
            contactusermap.put(u.ContactId, u.Id);  
        }
        //First case where there are no reviews at all and when move to in person button is clicked
        List<FC_Reviewer__External_Review__c> revlst = new List<FC_Reviewer__External_Review__c>();
        if(coilst.size() > 0 && rlist.isempty()){
            for(COI_Expertise__c coi :coilst){
                FC_Reviewer__External_Review__c rev = new FC_Reviewer__External_Review__c();
                rev.RecordTypeId = rvwRecordTypeId2;
                rev.FC_Reviewer__Reviewer_Type__c = 'In-Person';
                rev.FC_Reviewer__Status__c = 'Prep for In-Person';
                rev.Panel__c = pId;
                rev.Panel_MRO__c=panel.MRO__c;
                rev.OwnerId =contactusermap.get(coi.Reviewer_Name__c) ;
                rev.FC_Reviewer__Opportunity__c = coi.Related_Project__c;
                rev.FC_Reviewer__Contact__c = coi.Reviewer_Name__c;
                rev.FC_Reviewer__Deadline__c = panel.InPerson_Review_Deadline__c;
                revlst.add(rev);
                
            }
            recursiveTrigger.disableTrigger = true;
            insert revlst;
            return '';
        }
        //Second case where there are few reviews created and if any deleted than expected and when move to in person button is clicked
        if(coilst.size() > 0 && !rlist.isEmpty() && rlist.size()<coilst.size()){
            integer count=0;
            for(COI_Expertise__c coi :coilst){
                //checking which combination of the review is missing and only creating that record/s.
                if(!rvsstring.contains(coiandcontplusoptmap.get(coi.Id))){
                    count++;
                    FC_Reviewer__External_Review__c rev = new FC_Reviewer__External_Review__c();
                    rev.RecordTypeId = rvwRecordTypeId2;
                    rev.FC_Reviewer__Reviewer_Type__c = 'In-Person';
                    rev.FC_Reviewer__Status__c = 'Prep for In-Person';
                    rev.Panel__c = pId;
                    rev.Panel_MRO__c=panel.MRO__c;
                    rev.OwnerId = contactusermap.get(coi.Reviewer_Name__c) ;
                    rev.FC_Reviewer__Opportunity__c = coi.Related_Project__c;
                    rev.FC_Reviewer__Contact__c = coi.Reviewer_Name__c;
                    rev.FC_Reviewer__Deadline__c = panel.InPerson_Review_Deadline__c;
                    revlst.add(rev);
                    //if we find the records that were deleted instead of going further in the loop breaking and coming out
                    if(coilst.size()-rlist.size()==count){
                        break;
                    }
                }
            }
            recursiveTrigger.disableTrigger = true;
            insert revlst;
            return '';
        } 
        
        
        else {
            return 'Review records were already created';
        }
    }
}