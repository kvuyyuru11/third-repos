//This Class deals with handling the PFA page which shows up first for every Panel before viewing the COI associated to respective Panel
public class PFAPageController {

    public ID MyId{get;set;}


    string NotValid;
    string CheckValidation;
    public String PFA{get;set;}
    public boolean showadditionaltext{get;set;}
    public string textforPFA{get;set;}
    public boolean ErrorMsg1{get;set;}
    public Panel__c p{get;private set;}
    
    RecordType rt1=[Select Id from RecordType where name=:Label.COI_PFA_Record_Type limit 1];
    Id myId3 = userinfo.getUserId();
    User U=[select id,ContactId from User where Id=:myId3];
    Contact c=[select id,PFA__c,Reviewer_Status__c from Contact where id=:u.ContactId];
   
   public PFAPageController() 
    {
           myID=ApexPages.currentPage().getParameters().get('id');
           P=[select Id, Name, cycle__c,Program_Lookup__r.Name,PFA__r.Name from Panel__c where id=:myID limit 1];
    }
    
    public List<SelectOption> getPFAvalues()
     {
        List<SelectOption> options = new List<SelectOption>(); 
        Schema.DescribeFieldResult fieldResult = COI_Expertise__c.PFA_Level_COI__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple)
        {
          options.add(new SelectOption(f.getLabel(), f.getValue()));
        } 
        return options; 
    }
    
 //Method to show the text box when selected Yes or I dont Know
    public void showdiv()
    {
        if(PFA==Label.Yes ||PFA==Label.Unclear)
        {    
            showadditionaltext=true;
        }
        if(PFA==Label.No)
        {  
            showadditionaltext=false; 
            ErrorMsg1=false;  
        }
        
    }  
    //Method for validating the PFA page to fill all values when hit submit
    public string validateValues1()
    {

     ErrorMsg1 = false;
     NotValid= 'false';

     if((PFA==Label.Yes && textforPFA==null) ||(PFA==Label.Yes && textforPFA=='')||(PFA==Label.Unclear && textforPFA==null) ||(PFA==Label.Unclear && textforPFA==''))
     {
         ErrorMsg1=true;
         NotValid='true';
     }

    return NotValid;
    }
    
     //Method to update the COI record associated to the panel for the PFA record type based on the PFA selection type
    public pagereference submit1()
    {    
    //Panel_Assignment__c palist=[Select id,Panel__c from Panel_Assignment__c where Reviewer_Name__c=:c.id order by lastmodifieddate desc limit 1];
   //Id PanelId=palist.Panel__c;
   // get the panel assignment 
   // check if the reviewer is assigned to more than 1 panel 
    list<Panel_Assignment__c> palist=[Select id,Panel__c from Panel_Assignment__c where Reviewer_Name__c=:c.id ];
    system.debug(palist.size());   
    
    string PanelName=p.Name;
        integer iUnclearyes=1;
    Opportunity ra=[select id from Opportunity where Panel__c=:MyId limit 1];
    Cycle__c cy=[select Id,Name from Cycle__c where id=:p.Cycle__c limit 1];
    string CycleName=cy.Name;
    
    COI_Expertise__c coi2= [select id from COI_Expertise__c where Reviewer_Name__c=:c.Id AND Panel_R2__c=:MyId AND RecordtypeID=:rt1.ID];

              CheckValidation = validateValues1();
             if(checkvalidation=='true')
             {
                return null;
             }
             else if(checkvalidation=='false')
              {
              if(PFA==Label.Yes ||PFA==Label.Unclear)
               {
                coi2.Reviewer_Rationale__c=textforPFA;
                coi2.PFA_Level_COI__c=PFA;
                coi2.Reviewer_Name__c=c.Id;
                coi2.RecordTypeId=rt1.Id;
                coi2.Conflict_of_Interest__c=Label.Unclear_if_COI_Exists;
                coi2.Panel_for_PFA__c=PanelName;
                coi2.Cycle_for_PFA__c=CycleName;
                  //coi2.Related_Project__c=ra.id;
                database.update(coi2);
       
        // c.PFA__c=PFA;
                    //database.update(c);
                   
                   
                system.debug(palist.size());
                PageReference pageReference = new PageReference(Label.PFA_Submit_Page+'?size='+ iUnclearyes+'&programname='+p.PFA__r.Name);
                pageReference.setRedirect(true);
                return pageReference; 
              }  
             
             if(PFA==Label.No)
             {
                coi2.PFA_Level_COI__c=PFA;
                coi2.Reviewer_Name__c=c.Id;
                coi2.RecordTypeId=rt1.Id;
                coi2.Panel_for_PFA__c=PanelName;
                coi2.Cycle_for_PFA__c=CycleName;
                     // coi2.Related_Project__c=ra.id;
                database.update(coi2);
                 //c.PFA__c=PFA;
                // database.update(c);
             }
            }
       
        PageReference pageReference = new PageReference(Label.COIcustomlistviewpage+mYId);
        pageReference.setRedirect(true);
        return pageReference; 
    }

}