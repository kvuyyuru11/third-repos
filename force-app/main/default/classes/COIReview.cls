global class COIReview {
    public COI_Expertise__c objCOI_Expertise{get;set;}
    public list<Key_Personnel__c> lstkeypersonel{get;set;}
    public string strAwardee{get;set;}
    public String strHeader{get;set;}
    public  string sReturl;
    public boolean isPiuser{get;set;}
    public string sPiName{get;set;}
    public boolean breclocked{get;set;}
    public COIReview(ApexPages.StandardController controller){
        objCOI_Expertise=new COI_Expertise__c();
        lstkeypersonel= new list<Key_Personnel__c>();
       
         sReturl=ApexPages.currentPage().getParameters().get('Pid');
        string sretid=ApexPages.currentPage().getParameters().get('coid');
        if(string.isblank(sReturl)){
            string returl=ApexPages.currentPage().getHeaders().get('Referer');
     if(string.isnotblank ( returl)){
      
       string[] sSplit=returl.split('/');
       
       if(sSplit[sSplit.size()-1].startsWithIgnoreCase('006')){
          sReturl= sSplit[sSplit.size()-1];
             system.debug('==sSplit'+sReturl);
         }
        }
        }
        system.debug('sReturl=='+sReturl);
        if(string.isNotBlank(sReturl)){
        objCOI_Expertise.Related_Project__c=sReturl;
        GetKeypersonelDetails(sReturl,sretid);
        }
    }
    public void GetKeypersonelDetails(string pid,string coid){
        
        lstkeypersonel=[ Select id,Institution_Org__c,Full_Name__c,First_Name__c,Last_Name__c from Key_Personnel__c where Key_Personnel_Flag__c=true	and Opportunity__c =: pid and Status__c=:Label.COI_Review_Status_Label];
        breclocked=true;
        opportunity objproj=[select id,Awardee_Institution_Organization__c,Awardee_Institution_Organization__r.Name,
                                    Account_Name__c,AO_Name_User__c,PI_Name_User__c,PI_Name_User__r.Name
                             from opportunity where id=:pid];
        strAwardee=objproj.Account_Name__c;
        objCOI_Expertise.Related_Project__c=objproj.id;
        sPiName=objproj.PI_Name_User__r.Name;
        if(string.isNotEmpty(coid)){
            objCOI_Expertise=[select id,Related_Project__c,KeyPersonelValidated__c,COI_Policy__c,
                              Not_Influenced_COI__c,Financial_PI__c,Direct_Indirect_Links__c,Additional_Material_Information__c
                              From COI_Expertise__c where id=:coid];
            strHeader='Edit COI & Expertise Review';
            breclocked=doesOpportunityHavePendingApproval(coid);
           
             if(UserInfo.getUserId()==objproj.PI_Name_User__c){
                 if(breclocked){
                     
                     breclocked=false;
                 }
                 else{
                     breclocked=true;
                 }
             }
        }
        else{
            strHeader='New COI & Expertise Review';
        }
       if(UserInfo.getUserId()==objproj.PI_Name_User__c){
            isPiuser=false;
          
        }
        else{
            isPiuser=true;
            
        }
        system.debug('==strAwardee'+strAwardee);
    }
    
    public pagereference save(){
        RecordType objRecordType=  [SELECT Id,name FROM RecordType where name =: 'Awardee COI'];
        objCOI_Expertise.RecordTypeId=objRecordType.id;
        upsert objCOI_Expertise;
         PageReference pr=new PageReference('/'+sReturl);
     pr.setRedirect(true);
   return pr; 
    }
    public pagereference cancel(){   
         
        PageReference pr=new PageReference('/'+sReturl);
     pr.setRedirect(true);
   return pr; 
    }
    public Boolean doesOpportunityHavePendingApproval(id coi)
    {
        return ![ Select Id From ProcessInstance WHERE TargetObjectId =: coi AND Status = 'Pending' ].isEmpty();
    }



}