global without sharing class LightningSelfRegisterController {
    
    
    @AuraEnabled
    public static String login(String firstname, String lastname, String eemaaiil, string cnfemail,string password) {
        try{
          string accountId;
            
             
            list<contact> c=[select id,accountid,firstname, lastname, Registered__c,email from contact where Registered__c=false and email=:eemaaiil];
            String profileId = Label.MRA_Profile; 
            if(!c.isempty()){
             accountId = c[0].AccountId; 
                  }
            else{
                accountId = Label.MRA_Account; 
            }
            contact ct=new contact();
            ct.FirstName=firstname;
            ct.LastName=lastname;
            ct.Email=eemaaiil;
            ct.Community__c='TBD';
            ct.FGM_Portal__User_Profile__c = Label.Grantee;
            ct.RecordTypeId = Label.Standard_Contact_Record_Type;
            ct.AccountId=accountId;
            ct.Registered__c=true;
            insert ct;
            
                    User u = new User();
                    u.username = eemaaiil;
                    u.Email = eemaaiil;
                    u.FirstName = firstname;
                    u.LastName = lastname;
                    u.communityNickname = cnfemail;
                    u.profileId = profileId;
                    u.TimeZoneSidKey='America/New_York';
                    u.contactId=ct.Id;
                    u.Alias=firstname.substring(0, 1)+lastname.substring(0, 3);
                    u.IsActive=true;
                    u.LOCALESIDKEY='en_US';
                    u.LANGUAGELOCALEKEY='en_US';
                    u.EMAILENCODINGKEY='UTF-8';
                    u.Country='United States';
                    insert u;
                    System.setPassword(u.Id, password);
            
            
            ApexPages.PageReference lgn = Site.login(u.Username, password,null);
            aura.redirect(lgn);
            return null;  
        }
        catch (Exception ex) {
           
            AuraHandledException e=new AuraHandledException(ex.getMessage());
            e.SetMessage(ex.getMessage());
            system.debug('ex.getMessage()'+e);
            throw e;           
        }
        
    }

}