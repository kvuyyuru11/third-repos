@istest
public class listredirecttest{
    
    static testMethod void testRouController() {
        list<Account> acnt = TestDataFactory.getAccountTestRecords(1);
        list<contact> cons  = TestDataFactory.getContactTestRecords(1,1);
        list<User> usrs    = TestDataFactory.getpartnerUserTestRecords(1,1); 
        list<User> ussrs    = TestDataFactory.getstandardUserTestRecords(1);
        cycle__c c = new cycle__c(name='test20161', COI_Due_Date__c = Date.newInstance(2016,12,31));        
        insert c;       
        RecordType RecType = [Select Id From RecordType  Where SobjectType = 'Merit_Reviewer_Evaluation__c' and DeveloperName = 'Mentor_Evaluation'];
        System.runAs(ussrs[0]) {            
            Merit_Reviewer_Evaluation__c MRE = new Merit_Reviewer_Evaluation__c();
            mre.How_would_you_rate_the_time_with_MRO__c='Test11';
            mre.Rating_Scale__c='Test21';
            mre.How_would_you_rate_the_time_with_reviewe__c='Test31';
            mre.timeliness_scale__c='Test41';
            mre.Did_the_mentor_make_themselves_available__c='Test51';  
            mre.themselves_available_scale__c='31';  
            mre.Demeanor_conduct_and_tone_towards_MR__c='Test71';  
            mre.towards_MRO_scale__c='Test81';   
            mre.Demeanor_conduct_and_tone_towards_revie__c='Test91'; 
            mre.towards_reviewers_scale__c='Test101'; 
            mre.Interaction_with_reviewer_and_MRO_regard__c='Test111'; 
            mre.MRO_regarding_real_time_feedback__c='Test121';
            mre.Ability_to_flex_to_reviewers_styles__c='Test131'; 
            mre.reviewers_styles__c='Test141'; 
            mre.Kept_reviewers_progression_on_track__c ='Test151';
            mre.progression_on_track__c='41';  
            mre.Quality_of_guidance_to_mentees__c= 'Test171'; 
            mre.guidance_to_mentees__c='Test181';  
            mre.Understanding_of_the_merit_review_proces__c= 'Test191'; 
            mre.review_process_and_their_role_as_a_mento__c='Test201';  
            mre.Understanding_of_PCORI_merit_review_crit__c ='Test211'; 
            mre.PCORI_merit_review_criteria_and_goals__c ='41';
            mre.address_the_quality_of_this_relationship__c='51';
            mre.Panelist_complete_merit_review_cycle__c='Test20161';
            mre.RecordTypeId=RecType.id;
            mre.Reviewer_Name__c= cons[0].id; 
            mre.Mentor_Name__c= cons[0].id;
            mre.CycleLookup__c= c.id;  
            Insert mre;
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(mre);
            ApexPages.currentPage().getParameters().put('id',mre.id);
            
            listdirect RC = new listdirect(sc);
            RC.redirectDefaultLead1();
        }
    } 
    
    static testMethod void testRouController1() {
        list<Account> acnt = TestDataFactory.getAccountTestRecords(1);
        list<contact> con1  = TestDataFactory.getContactTestRecords(1,1);
        list<User> usrs    = TestDataFactory.getpartnerUserTestRecords(1,1); 
        list<User> ussrs    = TestDataFactory.getstandardUserTestRecords(1);        
        cycle__c c = new cycle__c(name='test201612', COI_Due_Date__c = Date.newInstance(2016,12,31));        
        insert c;       
        System.runAs(ussrs[0]) {            
            Merit_Reviewer_Evaluation__c MRE = new Merit_Reviewer_Evaluation__c();
            mre.How_would_you_rate_the_time_with_MRO__c='Test12';
            mre.Rating_Scale__c='Test22';
            mre.How_would_you_rate_the_time_with_reviewe__c='Test32';
            mre.timeliness_scale__c='Test42';
            mre.Did_the_mentor_make_themselves_available__c='Test52';  
            mre.themselves_available_scale__c='32';  
            mre.Demeanor_conduct_and_tone_towards_MR__c='Test72';  
            mre.towards_MRO_scale__c='Test82';   
            mre.Demeanor_conduct_and_tone_towards_revie__c='Test92'; 
            mre.towards_reviewers_scale__c='Test102'; 
            mre.Interaction_with_reviewer_and_MRO_regard__c='Test112'; 
            mre.MRO_regarding_real_time_feedback__c='Test122';
            mre.Ability_to_flex_to_reviewers_styles__c='Test132'; 
            mre.reviewers_styles__c='Test142'; 
            mre.Kept_reviewers_progression_on_track__c ='Test152';
            mre.progression_on_track__c='42';  
            mre.Quality_of_guidance_to_mentees__c= 'Test172'; 
            mre.guidance_to_mentees__c='Test182';  
            mre.Understanding_of_the_merit_review_proces__c= 'Test192'; 
            mre.review_process_and_their_role_as_a_mento__c='Test202';  
            mre.Understanding_of_PCORI_merit_review_crit__c ='Test212'; 
            mre.PCORI_merit_review_criteria_and_goals__c ='42';
            mre.address_the_quality_of_this_relationship__c='52';
            mre.Panelist_complete_merit_review_cycle__c='Test20162';
            //mre.RecordTypeId=RecType.id;
            mre.Reviewer_Name__c= con1[0].id; 
            mre.Mentor_Name__c= con1[0].id;
            mre.CycleLookup__c= c.id;  
            Insert mre;
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(mre);
            ApexPages.currentPage().getParameters().put('id',mre.id);            
            listdirect RC = new listdirect(sc);
            RC.redirectDefaultLead1();
        }
    } 
    
}