public without sharing class ContactInformationLightningController {
    
  
    
    
   /* @AuraEnabled
    public static Contact getcontact() {
        
        user u2=[select contactid from User where id=:userinfo.getUserId()];
        contact c2=[SELECT id,Phone FROM contact WHERE id =: u2.contactid];
        return (c2);
    }
    
    @AuraEnabled
      public static List<string> getgenderpicklistoptions() {
        List<string> options = new List<string>(); 
        Schema.DescribeFieldResult fieldResult = Contact.Gender__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple)
        {
            options.add(f.getLabel());
        }    
        return options; 
    } */
    
    /* @AuraEnabled
      public static Map<String, String> getgenderpicklistoptionsvalues() {
        Map<String, String> options = new Map<String, String>(); 
        Schema.DescribeFieldResult fieldResult = Contact.Gender__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple)
        {
            options.put(f.getLabel(), f.getValue());
        }    
        return options; 
    } */
    
    
        public class    SelectPicklistOption
    {
        @AuraEnabled  public String label {get;set;}
        @AuraEnabled  public String value {get;set;}
        
        public SelectPicklistOption(String lab, String val)
        {
            this.label = lab;
            this.value = val;
        }
        
    }

    
    
        public class initWrapper    
    {
        @AuraEnabled public List<SelectPicklistOption> programapplyingfor{get;set;}
        @AuraEnabled public List<SelectPicklistOption> saloptions{get;set;}
        @AuraEnabled public List<SelectPicklistOption> genderoptions{get;set;}
        @AuraEnabled public List<SelectPicklistOption> hispaniclatinooptions{get;set;}
        @AuraEnabled public List<SelectPicklistOption> raceooptions{get;set;}
        @AuraEnabled public List<String> yobooptions{get;set;} 
        @AuraEnabled public List<SelectPicklistOption> prvinvwthpcorioptions{get;set;}
        @AuraEnabled public List<SelectPicklistOption> pfedemplyeoptions{get;set;}
        @AuraEnabled public Contact cont{get;set;}
    }
    
    
        @AuraEnabled
    public static List<SelectPicklistOption> getPickListValuesIntoList(String objectType, String selectedField){
        List<SelectPicklistOption> allOpts = new List<SelectPicklistOption>();
        Schema.SObjectType convertToObj = Schema.getGlobalDescribe().get(objectType);
        Schema.DescribeSObjectResult res = convertToObj.getDescribe();
        Schema.DescribeFieldResult fieldResult = res.fields.getMap().get(selectedField).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a: ple) {
            SelectPicklistOption opt = new SelectPicklistOption(a.getLabel(),a.getValue()) ;
            allOpts.add(opt);
        }
        
        return allOpts;
    }
    
       @AuraEnabled
    public static List<string> getPickListValuesstringIntoList(String objectType, String selectedField){
        List<string> allstringOpts = new List<string>();
        Schema.SObjectType convertToObj = Schema.getGlobalDescribe().get(objectType);
        Schema.DescribeSObjectResult res = convertToObj.getDescribe();
        Schema.DescribeFieldResult fieldResult = res.fields.getMap().get(selectedField).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a: ple) {
            
            allstringOpts.add(a.getValue());
        }
        
        return allstringOpts;
    }

@AuraEnabled
    public static initWrapper getContactRecords()
    {
        user u2=[select contactid from User where id=:userinfo.getUserId()];
        contact c2=[SELECT id,Program_Applying__c,Phone,MailingStreet,MailingCity,MailingState,MailingCountry,MailingPostalCode,
                    Salutation,Gender__c,Hispanic_Latino__c,Race__c,Year_of_Birth__c,Previous_involvement_with_PCORI__c,
                    Current_Position_or_Title__c,Department,Federal_Employee__c,Registered__c,How_did_you_hear_about_PCORI_Other_c__c FROM Contact where id=:u2.contactid];
        system.debug('c2'+c2);
       
        initWrapper wrap = new initWrapper();
        wrap.programapplyingfor=getPickListValuesIntoList('Contact','Program_Applying__c');
        wrap.saloptions=getPickListValuesIntoList('Contact','Salutation');
        wrap.genderoptions=getPickListValuesIntoList('Contact','Gender__c');
        wrap.hispaniclatinooptions=getPickListValuesIntoList('Contact','Hispanic_Latino__c');
        wrap.raceooptions=getPickListValuesIntoList('Contact','Race__c');
        wrap.yobooptions=getPickListValuesstringIntoList('Contact','Year_of_Birth__c');
        wrap.prvinvwthpcorioptions=getPickListValuesIntoList('Contact','Previous_involvement_with_PCORI__c');
        wrap.pfedemplyeoptions=getPickListValuesIntoList('Contact','Federal_Employee__c');
        wrap.cont=c2;
        return wrap;
    }
    
    @AuraEnabled
    public static list<Account> getaccountscntrl(string acct){
        list<Account> AccountNames= new list<Account>();
        String queryName = acct + '%';
   list<Account>    queryResult = [SELECT id,Name from Account WHERE Name like :queryName];
    
return queryResult;
    }
    
    @AuraEnabled
    public static string srvrsavemethod(contact ct){
        
        ct.Registered__c=true;
        update ct;
        return null;
    }

}