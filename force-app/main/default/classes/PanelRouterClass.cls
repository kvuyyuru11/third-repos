//This Class is to support the Panel Router page which actually handles the logic of showing the panel record or PFA page or COI custom listview page
public class PanelRouterClass
 {   
    PageReference pageref;
    Id recdid;
    RecordType rt1=[Select Id from RecordType where name=:Label.COI_PFA_Record_Type limit 1];
    string PName = Label.Single_Community_Profile;
    profile p=[select id from Profile where name=:PName];
    Id ProfileId=p.id; 
    list<Id> PnlIDlist= new list<Id>();
    Map<Id,Id> coimap= new Map<Id,Id>();
    Map<Id,string> coipfamap= new Map<Id,string>();
    map<id,string> coiprogram = new map<id,string>();
    public PanelRouterClass(ApexPages.StandardController controller)
    {

    }
    public pagereference router3()
    {
    //Gets the record id
    recdid =ApexPages.currentPage().getParameters().get('Id');
    ID usrid=Userinfo.getUserId();
    User u=[select id,contactid from user where id=:usrid];
     try
       {
        Contact cnt=[select id,PFA__c from Contact where id=:u.ContactId limit 1];
        list<COI_Expertise__c> cplist=[select id,submitted__c,Panel_R2__c,Reviewer_Name__c,PFA_Level_COI__c,Panel_R2__r.Program_Lookup__r.Name,Panel_R2__r.PFA__r.Name  from COI_Expertise__c where recordtypeID=:rt1.Id AND Reviewer_Name__c=:u.ContactId];
        for(COI_Expertise__c c:cplist)
        {    
             coipfamap.put(c.Panel_R2__c,c.PFA_Level_COI__c);
             coiprogram.put(c.Panel_R2__c,c.Panel_R2__r.PFA__r.Name);
        }
      //Checks the profile id and routes to the page required
      
   
        if (UserInfo.getProfileId().equals(ProfileId)&&coipfamap.get(recdid)==Label.No)
          {
            string url1=Label.COIcustomlistviewpage+recdid;
            pageRef = new PageReference(url1);
            pageref.setredirect(True);
          }
       
          else if(UserInfo.getProfileId().equals(ProfileId) && coipfamap.get(recdid)==Label.Yes)
          {
             
              
               
             
                 string url2=label.PFA_Restricted+'?programname='+coiprogram.get(recdid)+'&page=2';
              
            pageRef = new PageReference(url2);
            pageref.setRedirect(True);
          }
           else if(UserInfo.getProfileId().equals(ProfileId) && coipfamap.get(recdid)==Label.Unclear){
                string url2=label.PFA_Restricted+'?programname='+coiprogram.get(recdid)+'&page=1';
               pageRef = new PageReference(url2);
            pageref.setRedirect(True);
           }
        
          else if(UserInfo.getProfileId().equals(ProfileId) && coipfamap.get(recdid)=='' ||UserInfo.getProfileId().equals(ProfileId) && coipfamap.get(recdid)==null) 
          {
            string url3=Label.PfaPagewithid+recdid;
            pageRef = new PageReference(url3);
            pageref.setRedirect(True);
          }    
        
           return pageref;
         }
      catch(exception e)
      {
        string url4='/'+recdID+'?nooverride=1';
        pageRef = new PageReference(url4);
        pageref.setRedirect(True);
        return pageref;
      }
     
    }
}