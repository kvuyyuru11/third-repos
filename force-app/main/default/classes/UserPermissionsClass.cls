public class UserPermissionsClass {
   
    
    public static void giveUserPermissionSets(object[] newTrigger)
    {
        List<User> newUserList = (List<User>) newTrigger;
        
        List<PermissionSet> leadPermSet = [SELECT id FROM PermissionSet WHERE Name =: 'Lead_Full_Access'];
        Id leadPermSetID = leadPermSet[0].id;
        
        //List<PermissionSet> granteePermSet = [SELECT id FROM PermissionSet WHERE Name =: 'Community_Grantee_Permission_set_User_Login_Licence'];
        //Id granteePermSetID = granteePermSet[0].id;
        
        List<Profile> profList = [SELECT id FROM Profile WHERE Name =: 'PCORI Community Partner'];
        Id communityProfileID = profList[0].id;
        
        List<Id> userIdList = new List<Id>();
        List<Id> userProfileList = new List<Id>();
        
        for (User u : newUserList)
        {
            userIdList.add(u.id);
            userProfileList.add(u.ProfileId);
        }
         if(recursiveTrigger.run && !System.isFuture()) {
              recursiveTrigger.run =false;
        prepareForInsert(userIdList, userProfileList, communityProfileID, leadPermSetID);
         }
    }
    
    
    
    @future
    public static void prepareForInsert(List<Id> userIdList, List<Id> userProfileList, Id communityProfileID, Id leadPermSetId)
    {
        List<Profile> chatter1List = [SELECT id FROM Profile WHERE Name =: 'Chatter Free User'];
        Id chatter1Profile = chatter1List[0].id;
        
        List<Profile> chatter2List = [SELECT id FROM Profile WHERE Name =: 'Chatter Moderator User'];
        Id chatter2Profile = chatter2List[0].id;
        
        List<Profile> chatter3List = [SELECT id FROM Profile WHERE Name =: 'Chatter External User'];
        Id chatter3Profile = chatter3List[0].id;
        
        list<PermissionSetAssignment> insertList = new list<PermissionSetAssignment>();
        /*
        List<PermissionSetAssignment> communityPermAssignmentsList = [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId =: granteePermSetId];
        Map<Id, Id> communityPermAssignmentsMap = new Map<Id, Id>();
        for (PermissionSetAssignment psa : communityPermAssignmentsList)
        {
            communityPermAssignmentsMap.put(psa.AssigneeId, psa.AssigneeId);
        }
        */
        List<PermissionSetAssignment> leadPermAssignmentsList = [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId =: leadPermSetId];
        Map<Id, Id> leadPermAssignmentsMap = new Map<Id, Id>();
        for (PermissionSetAssignment psa : leadPermAssignmentsList)
        {
            leadPermAssignmentsMap.put(psa.AssigneeId, psa.AssigneeId);
        }
        
        for(integer i=0; i<userIdList.size(); i++)
        {
            if (userProfileList[i] != communityProfileID && 
                userProfileList[i] != chatter1Profile && 
                userProfileList[i] != chatter2Profile && 
                userProfileList[i] != chatter3Profile)
            {
                if (!leadPermAssignmentsMap.containsKey(userIdList[i]))
                {
                    // not a community user. give lead access if they dont already have it
                    PermissionSetAssignment permAssign = new PermissionSetAssignment();
                    permAssign.AssigneeId = userIdList[i];
                    permAssign.PermissionSetId = leadPermSetId;
                    insertList.add(permAssign);
                }    
                /*
                // this is a community user. give grantee permission set if they dont already have it
                if (!communityPermAssignmentsMap.containsKey(userIdList[i]))
                {

                    PermissionSetAssignment permAssign = new PermissionSetAssignment();
                    permAssign.AssigneeId = userIdList[i];
                    permAssign.PermissionSetId = granteePermSetId;
                    //insertList.add(permAssign);
                } 
				*/
            }

        }
        if (insertList.size() > 0)
        {
            insert insertList;
        }
        
    }
    
    

/*
	public static void checkIfNonActive_toActive(object[] newTrigger, Map<Id, User> oldUserMap)
	{        
		List<PermissionSetAssignment> insertList = new List<PermissionSetAssignment>();
        List<User> newUserList = (List<User>) newTrigger;
           
        List<Profile> profList = [SELECT id FROM Profile WHERE Name =: 'PCORI Community Partner'];
        Id communityProfileID = profList[0].id;
        
        List<PermissionSet> granteePermSet = [SELECT id FROM PermissionSet WHERE Name =: 'Community_Grantee_Permission_set_User_Login_Licence'];
        Id granteePermSetID = granteePermSet[0].id;
        
        List<PermissionSet> leadPermSet = [SELECT id FROM PermissionSet WHERE Name =: 'Lead_Full_Access'];
        Id leadPermSetID = leadPermSet[0].id;
        
        
        List<PermissionSetAssignment> communityPermAssignmentsList = [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId =: granteePermSet];
        Map<Id, Id> communityPermAssignmentsMap = new Map<Id, Id>();
        for (PermissionSetAssignment psa : communityPermAssignmentsList)
        {
            communityPermAssignmentsMap.put(psa.AssigneeId, psa.AssigneeId);
        }
        
        List<PermissionSetAssignment> leadPermAssignmentsList = [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId =: leadPermSet];
        Map<Id, Id> leadPermAssignmentsMap = new Map<Id, Id>();
        for (PermissionSetAssignment psa : leadPermAssignmentsList)
        {
            leadPermAssignmentsMap.put(psa.AssigneeId, psa.AssigneeId);
        }
        
        
        for (User newUser : newUserList)
        {
        	if (oldUserMap.get(newUser.id).IsActive == false && newUser.IsActive == true)
            {
                if (newUser.ProfileId == communityProfileID)
                {
                    //System.debug('is community');
                    if (!communityPermAssignmentsMap.containsKey(newUser.Id))
                    {
                        PermissionSetAssignment permAssign = new PermissionSetAssignment();
                        permAssign.AssigneeId = newUser.id;
                        permAssign.PermissionSetId = granteePermSetID;
                        //insertList.add(permAssign);
                    }
                    
                }
                
                else
                {
                    //System.debug('is grantee');
                    if (!leadPermAssignmentsMap.containsKey(newUser.Id))
                    {
                        PermissionSetAssignment permAssign = new PermissionSetAssignment();
                        permAssign.AssigneeId = newUser.id;
                        permAssign.PermissionSetId = leadPermSetID;
                        //insertList.add(permAssign);
                    }                  
                }               
            }
        }
        insert insertList;
	}
	*/
}






/*
    public static void giveLeadConvertPermSet(object[] newTrigger)
    {
        List<User> newUserList = (List<User>) newTrigger;
        
        List<PermissionSet> permSet = [SELECT id FROM PermissionSet WHERE Name =: 'Lead_Full_Access'];
        Id permSetID = permSet[0].id;
        //List<PermissionSetAssignment> insertList = new List<PermissionSetAssignment>();
        
        for (User u : newUserList)
        {
            if (u.UserType == 'Standard' && u.IsActive == True)
            {
                doInsert(u.id, permSetID);
            }
        }
    }
    
    
    public static void giveExternalGranteePermSet(object[] newTrigger)
    {
        List<User> newUserList = (List<User>) newTrigger;
        
        List<PermissionSet> permSet = [SELECT id FROM PermissionSet WHERE Name =: 'Community_Grantee_Permission_set_User_Login_Licence'];
        Id permSetID = permSet[0].id;
        //List<PermissionSetAssignment> insertList = new List<PermissionSetAssignment>();
        
        List<Profile> profList = [SELECT id FROM Profile WHERE Name =: 'PCORI Community Partner'];
        Id profileID = profList[0].id;
        
        for (User u : newUserList)
        {
            //if (u.UserType != 'Standard' && u.IsActive == True) 
            if (u.ProfileId == profileID && u.IsActive == True)
            {
                doInsert(u.id, permSetID);
            }
        }
    }
    */   



/*
@InvocableMethod
    public static void checkIfNonActive_toActive(List<User> userList)
    {
        List<PermissionSetAssignment> insertList = new List<PermissionSetAssignment>();     
        List<Profile> profList = [SELECT id FROM Profile WHERE Name =: 'PCORI Community Partner'];
        Id communityProfileID = profList[0].id;
        
        List<PermissionSet> granteePermSet = [SELECT id FROM PermissionSet WHERE Name =: 'Community_Grantee_Permission_set_User_Login_Licence'];
        Id granteePermSetID = granteePermSet[0].id;
        
        List<PermissionSet> leadPermSet = [SELECT id FROM PermissionSet WHERE Name =: 'Lead_Full_Access'];
        Id leadPermSetID = leadPermSet[0].id;
        
        
        List<PermissionSetAssignment> communityPermAssignmentsList = [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId =: granteePermSet];
        Map<Id, Id> communityPermAssignmentsMap = new Map<Id, Id>();
        for (PermissionSetAssignment psa : communityPermAssignmentsList)
        {
            communityPermAssignmentsMap.put(psa.AssigneeId, psa.AssigneeId);
        }
        
        List<PermissionSetAssignment> leadPermAssignmentsList = [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId =: leadPermSet];
        Map<Id, Id> leadPermAssignmentsMap = new Map<Id, Id>();
        for (PermissionSetAssignment psa : leadPermAssignmentsList)
        {
            leadPermAssignmentsMap.put(psa.AssigneeId, psa.AssigneeId);
        }
        
        
        for (User newUser : userList)
        {
            if (newUser.ProfileId == communityProfileID)
            {
                System.debug('is community');
                if (!communityPermAssignmentsMap.containsKey(newUser.Id))
                {
                    PermissionSetAssignment permAssign = new PermissionSetAssignment();
                    permAssign.AssigneeId = newUser.id;
                    permAssign.PermissionSetId = granteePermSetID;
                    
                    insertList.add(permAssign);
                    System.debug('adding grantee');
                }
                
            }
            
            else
            {
                System.debug('is grantee');
                if (!leadPermAssignmentsMap.containsKey(newUser.Id))
                {
                    PermissionSetAssignment permAssign = new PermissionSetAssignment();
                    permAssign.AssigneeId = newUser.id;
                    permAssign.PermissionSetId = leadPermSetID;
                    
                    insertList.add(permAssign);
                    System.debug('adding lead');
                }                  
            }               
            insert insertList;
        }         
    }
*/