public without sharing class cloneRejectedReportHelper{

public static Void cloneRejectedMethod(List<Opportunity> Listopp){
//List used for cloned report inserts
    System.debug('We Made IT!!!');
    List<FGM_Base__Grantee_Report__c> granteeListToInsert = new List<FGM_Base__Grantee_Report__c>();
    
    //Object used to store report data when PI AO CO or PO changes
    FGM_Base__Grantee_Report__c r; 
    
    //List used for updating progress reports that need PI AO CO or PO changes
    List<FGM_Base__Grantee_Report__c> prList = new List<FGM_Base__Grantee_Report__c>(); 
    
    //List of Ids used to find progress reports and opportunities
    Set<id> idList = new Set<Id>();
    Map<Id, FGM_Base__Grantee_Report__c> reportMap = new Map<Id, FGM_Base__Grantee_Report__c>();
           
    for(Opportunity ops: Listopp)
    {
        idList.add(ops.Clone_Report_ID__c);
    }
    
    List<FGM_Base__Grantee_Report__c> repList = [SELECT CO_Officer__c, e_Recruiting_or_retaining_study_partici__c, Summary_Expected_Future_Challenges_2__c, PI_Lead_2__c, PI_Lead_Designee_1__c, PI_Lead_Designee_2__c, EO_Officer__c, CO_Cooridnator__c, Program_Associate__c, No_changes_in_Key_Personnel_2__c, Time_points_which_participants_are_lost__c, Systematic_effort_to_reduce_attrition__c, Reasons_for_declining__c, Reason_for_ineligibility__c, Significant_changes_from_research_plan__c, Stakeholder_Email_1__c, Stakeholder_Email_2__c, Stakeholder_Email_3__c, Stakeholder_Name_1__c, Stakeholder_Name_2__c, Stakeholder_Name_3__c, Stakeholder_Telephone_1__c, Stakeholder_Telephone_3__c, Stakeholder_Telephone_2__c, Rate_Your_Experience__c, Will_you_engage_with_patients_and_or_o__c, What_suggestions_do_you_have_for_overc__c, RecordTypeId, a_Deciding_what_the_study_is_about_cho__c, Accomplishments_achieved_during_the_f__c, Account_Officer_Name__c, 
                Actual_Percentage_Effort__c, Adaptive_Trials_if_applicable_AT_1_5__c, FGM_Base__Address__c, 
                Amount_of_Influence__c, Start_Date__C,
                AO_Approved__c, AO_City_Address_del__c, AO_Email_del__c, AO_Sign_Off_Checkbox__c, AO_Sign_off_date__c, AO_Sign_off_Name__c,  
                Audience__c, Authors__c, FGM_Base__Award_Date__c, b_Choosing_designing_interventions__c,
                c_Choosing_outcomes_and_deciding_how_to__c, Category__c, Challenges_With_Project_Progress__c, Challenges_With_Stakeholder_Engagement__c, 
                Change_Type__c, FGM_Base__Comment__c, Completed__c, Confirmation_Code_From_Survey__c,                 
                FGM_Base__Contact_Organization__c, d_Other_aspects_of_study_design__c, Data_Integrity_and_Rigorous_Analysis__c, Data_Integrity_and_Rigorous_Analysis_IR__c,
                Data_Registries_if_applicable_DR_3__c, Date__c , Date_Approved__c, Date_Completed__c, Date_Rejected__c,  
                Describe_the_limitations_of_your_study__c, Discuss_the_implications_of_your_finding__c, 
                FGM_Base__Display_Benchmark__c, Dissemination__c, FGM_Base__Due_Date__c, FGM_Base__Year__c,
                Engagement_may_not_occur_at_all_time_poi__c, Explanation_of_Changes__c, f_Data_collection__c, FGM_Base__Fax__c, FGM_Base__Final_Report__c, For_projects_testing_methodologies__c, 
                g_Analyzing_or_reviewing_results__c, h_Dissemination__c,  
                Heterogeneity_of_Treatment_Effects__c,  
                How_did_you_engage_with_patients_and_or__c, How_will_your_study_findings_and_other_l__c, If_Not_Completed_Reason_for_Delay__c, 
                If_relevant_briefly_summarize_differenc__c, FGM_Base__Impact__c, Intended_Audience__c, FGM_Base__Interim_Report__c, Is_Overdue__c,
                j_The_way_the_research_team_and_partner__c, k_Research_projects_other_than_this_spe__c, Key_Personnel__c, Late__c, Meeting_Location__c, Milestone_Name__c, Missing_Data__c, 
                Missing_Data_MD_3_MD_4_MD_5__c, FGM_Base__Name__c, Name_and_Location__c, 
                Name_of_Publication__c, Next5Businessdays__c, No_changes_in_Key_Personnel__c, Notable_Progress__c,
                Not_Engaged_Other_Please_Describe__c, FGM_Base__Organization__c, Other_Please_describe__c, 
                Other_part_of_the_project_Please_descri__c, Other_Reason_For_Delay__c, Other_Research_Projects__c, Other_Type__c, 
                Oversight_Summary__c, Page_Numbers__c, PCORI_contract_number__c, PCORI_s_Methodology_Standards__c, Period_Covered_by_this_Report_End__c,
                Period_Covered_by_this_Report_Start__c, FGM_Base__Phone__c, PI_Approved__c, PI_Sign_off_checkbox__c, PI_Sign_off_date__c, PI_Sign_off_Name__c, Please_describe_the_data_management_and__c, 
                PMID__c, Portal_User__c, Portal_User_Id__c, Presentation_Date__c, Presentation_Peer_Reviewed__c, Presentation_Review_Status_Indicator__c, 
                Presentation_Type__c, Presenters__c, Presenters_role_in_the_project__c, Principal_Investigator_Last_First_Mid__c, Principal_Investigator_Sign_Off_Name__c, Progress_During_Reporting_Period__c,
                Progress_Report_Name__c, Progress_Report_Time_Period__c, Project__c, PR_Reject__c, PR_Time_Period_Other_Specify__c, Publication_Reviewed__c,
                Publication_Status__c, Publication_Type__c, Reason_Returned__c,  Reject__c, RejectClone__c, Rejected__c, FGM_Base__Remind_Awardee__c,
                FGM_Base__Remind_Staff__c, Report_Completion_Date__c, FGM_Base__Report_Contact__c, Reporting_Period__c, Report_Type__c,
                Report_Version__c, FGM_Base__Request__c, FGM_Base__Request_Number__c, Role__c, Scientific_Manuscript_Type__c, FGM_Base__Type__c, Significant_Decisions_Findings_Summary__c, 
                Significant_Deviations_in_Costs__c, FGM_Base__Status__c, Status__c, FGM_Base__Submission_Date__c, Submission_Date__c, 
                FGM_Base__Submitted_By__c, Submitted_Date_Time__c, Summarize_any_significant_change_s_from__c, Summarize_project_findings__c,  
                Summary_of_Changes_From_Proposal__c, Thank_you_for_your_responses_to_the_abov__c, The_Way_The_Research_Team_Partner_work__c,
                FGM_Base__Title__c, Title__c, Type__c, URL_if_applicable__c, Volume_issue__c, What_if_anything_will_you_do_d__c, What_are_the_next_steps_for_this__c, What_are_the_opportunities_for_futur__c,
                When_in_the_project_were_patients_and_or__c, Year__c, Name, Patient_Stakeholder_Impact_Outcome__c, Patient_Stakeholder_Engagement_Summary__c, Who_are_the_key_end_users_of_you__c, 
                Summarize_project_findings_Public__c, Patient_Stakeholder_Impact_Public__c, Accomplishments_Achieved_During_Public__c, Challenges_With_Project_Public__c,
                Challenges_With_Stakeholder_Public__c, Patient_Stakeholder_Engagement_Public__c, Significant_Decisions_Findings_Public__c 
                //Commented By Vijay Jun_9_2017 //Choosing_outcomes_and_deciding_how_to_me__c, Recruiting_or_retaining_study_participan__c, Data_collection__c, Analyzing_or_reviewing_results__c, Deciding_what_the_study_is_about_choosi__c, Choosing_or_designing_interventions_or_c__c, 
                //Commented By Vijay Aug_25_2017 //Adaptive_Trials__c, Additional_Engagement_Info__c, Discuss_implications_of_your_finding__c, How_will_your_study_findings_and_oth__c, Instructions__c, Negative_Effect_On_Your_Project__c, Data_Registries__c, Describe_notable_progress__c, 
                //Heterogeneity_of_Treatment_Effects_HT_3__c,Other__c, Other_Accomplishments__c, Other_aspects_of_study_design__c, Other_Challenges__c,PCORI_Project_Adherence__c, Please_report_how_your_project_meets__c, Positive_Effect_on_Your_Project__c, Progress_on_Engagement_Plan__c, 
                //Report_how_these_Standards_have_been_met__c, State_the_research_question_s_addressed__c,
                FROM FGM_Base__Grantee_Report__c WHERE id IN :idList];
    
    for(FGM_Base__Grantee_Report__c reps: repList)
    {
        reportMap.put(reps.FGM_Base__Request__c, reps);
    }
    //Iterating through the new opportunities to find matching reports and add them to the list of reports to be updated
    for (Opportunity op : Listopp) 
    {
        //Checking to see if the updated opportunity's reject temp checkbox is equal to true before attempting to clone
        if(op.Reject_Temp__c)
        {
            System.debug('Rejecttttttttttttttt');
            //Selecting the most recently rejected report based on the opportunity id and the lastModifiedDate
            //List<FGM_Base__Grantee_Report__c> GIds = [SELECT FGM_Base__Request__c, Id FROM FGM_Base__Grantee_Report__c WHERE FGM_Base__Request__c = :op.Id AND FGM_Base__Status__c = 'Returned' ORDER BY LastModifiedDate DESC];
            FGM_Base__Grantee_Report__c allfields = reportMap.get(op.Id);
            
            /*Checking to ensure that we returned an expected result before performing the clone operation
            We need to ensure the Gids list populated and that our cloneRejectProjectTrig trigger fired and updated the 
            Clone_Report_Id__c field.*/
            if (op.Clone_Report_ID__c != null) 
            {
                System.debug('CloneIddddddddddddddddddddd');

                //Instantiating the report to be added to the list of updates
                FGM_Base__Grantee_Report__c report = new FGM_Base__Grantee_Report__c();
                
                //Instantiating a new object based on our query, this object is used to assign values to the clone
                
                
                if(allFields != null)   
                {  
                    System.debug('Dataaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');
                    report.FGM_Base__Status__c = 'Rejected';
                    report.FGM_Base__Request__c = op.Id;
                    
                    //PI new fields
                    report.PI_Lead_2__c = allFields.PI_Lead_2__c;
                    
                    report.PI_Lead_Designee_1__c = allFields.PI_Lead_Designee_1__c;
                    report.PI_Lead_Designee_2__c = allFields.PI_Lead_Designee_2__c;
                    report.EO_Officer__c = allFields.EO_Officer__c;
                    report.CO_Cooridnator__c = allFields.CO_Cooridnator__c;
                    report.Program_Associate__c = allFields.Program_Associate__c; 
                    report.Summary_Expected_Future_Challenges_2__c = allFields.Summary_Expected_Future_Challenges_2__c;
                    report.CO_Officer__c = allFields.CO_Officer__c;
                    
                    //Hoffman's new fields
                    report.Significant_changes_from_research_plan__c = allFields.Significant_changes_from_research_plan__c;
                    report.Reason_for_ineligibility__c = allFields.Reason_for_ineligibility__c;
                    report.Reasons_for_declining__c = allFields.Reasons_for_declining__c;
                    report.Systematic_effort_to_reduce_attrition__c = allFields.Systematic_effort_to_reduce_attrition__c;
                    report.Time_points_which_participants_are_lost__c  = allFields.Time_points_which_participants_are_lost__c; 
                    report.No_changes_in_Key_Personnel_2__c = allFields.No_changes_in_Key_Personnel_2__c;
                    
                    report.Accomplishments_achieved_during_the_f__c = allFields.Accomplishments_achieved_during_the_f__c;
                    //report.Additional_Engagement_Info__c = allFields.Additional_Engagement_Info__c;
                              

                    // Recently added
                    report.Rate_Your_Experience__c = allFields.Rate_Your_Experience__c;
                    //report.Positive_Effect_on_Your_Project__c = allFields.Positive_Effect_on_Your_Project__c; 
                    //report.Negative_Effect_On_Your_Project__c = allFields.Negative_Effect_On_Your_Project__c; 
                    //report.Will_you_engage_with_patients_and_or_o__c = allFields.Will_you_engage_with_patients_and_or_o__c; 
                    //report.What_suggestions_do_you_have_for_overc__c = allFields.What_suggestions_do_you_have_for_overc__c; 
                    report.Stakeholder_Email_1__c = allFields.Stakeholder_Email_1__c;
                    report.Stakeholder_Email_2__c = allFields.Stakeholder_Email_2__c;
                    report.Stakeholder_Email_3__c = allFields.Stakeholder_Email_3__c;
                    report.Stakeholder_Name_1__c = allFields.Stakeholder_Name_1__c;
                    report.Stakeholder_Name_2__c = allFields.Stakeholder_Name_2__c;
                    report.Stakeholder_Name_3__c = allFields.Stakeholder_Name_3__c;
                    report.Stakeholder_Telephone_1__c = allFields.Stakeholder_Telephone_1__c;
                    report.Stakeholder_Telephone_2__c = allFields.Stakeholder_Telephone_2__c;
                    report.Stakeholder_Telephone_3__c = allFields.Stakeholder_Telephone_3__c;        
                    report.Audience__c = allFields.Audience__c;     
                    report.Authors__c = allFields.Authors__c;                 
                    report.Challenges_With_Project_Progress__c = allFields.Challenges_With_Project_Progress__c;        
                    report.Challenges_With_Stakeholder_Engagement__c = allFields.Challenges_With_Stakeholder_Engagement__c;        
                    report.Confirmation_Code_From_Survey__c = allFields.Confirmation_Code_From_Survey__c;       
                    report.Data_Integrity_and_Rigorous_Analysis__c = allFields.Data_Integrity_and_Rigorous_Analysis__c;        
                    report.Data_Integrity_and_Rigorous_Analysis_IR__c = allFields.Data_Integrity_and_Rigorous_Analysis_IR__c;       
                    report.Data_Registries_if_applicable_DR_3__c = allFields.Data_Registries_if_applicable_DR_3__c;        
                    report.Date__c = allFields.Date__c;       
                    report.Date_Rejected__c = allFields.Date_Rejected__c;
                    report.Heterogeneity_of_Treatment_Effects__c = allFields.Heterogeneity_of_Treatment_Effects__c;
                    report.Missing_Data__c = allFields.Missing_Data__c;
                    report.Missing_Data_MD_3_MD_4_MD_5__c = allFields.Missing_Data_MD_3_MD_4_MD_5__c;
                    
                    report.Name_and_Location__c = allFields.Name_and_Location__c;
                    report.Notable_Progress__c = allFields.Notable_Progress__c;
                    
                    //report.PI_Certify__c = allFields.PI_Certify__c;
                    //report.Progress_on_Engagement_Plan__c = allFields.Progress_on_Engagement_Plan__c;
                    //report.FGM_Base__Request__c = allFields.FGM_Base__Request__c;
                    report.Significant_Decisions_Findings_Summary__c = allFields.Significant_Decisions_Findings_Summary__c;
                    report.Significant_Deviations_in_Costs__c = allFields.Significant_Deviations_in_Costs__c;
                    report.Summarize_any_significant_change_s_from__c = allFields.Summarize_any_significant_change_s_from__c;
                  
                    report.RecordTypeId = allFields.RecordTypeId;
                    
                    //report.Project__c = op.Id;
                    
                    // the rest of the object
                    report.a_Deciding_what_the_study_is_about_cho__c = allFields.a_Deciding_what_the_study_is_about_cho__c;
                    report.Account_Officer_Name__c = allFields.Account_Officer_Name__c;
                    report.Actual_Percentage_Effort__c = allFields.Actual_Percentage_Effort__c;
                    //report.Adaptive_Trials__c = allFields.Adaptive_Trials__c;
                    report.Adaptive_Trials_if_applicable_AT_1_5__c = allFields.Adaptive_Trials_if_applicable_AT_1_5__c;
                    //report.FGM_Base__Address__c = allFields.FGM_Base__Address__c;
                    report.e_Recruiting_or_retaining_study_partici__c = allFields.e_Recruiting_or_retaining_study_partici__c;
                    report.Amount_of_Influence__c = allFields.Amount_of_Influence__c;
                    //report.Analyzing_or_reviewing_results__c = allFields.Analyzing_or_reviewing_results__c; //Commented By Vijay
                    report.AO_Approved__c = allFields.AO_Approved__c;
                    //report.AO_City_Address_del__c = allFields.AO_City_Address_del__c;
                    //report.AO_Email_del__c = allFields.AO_Email_del__c;
                    report.AO_Sign_Off_Checkbox__c = allFields.AO_Sign_Off_Checkbox__c;
                    report.AO_Sign_off_date__c = allFields.AO_Sign_off_date__c;
                    report.AO_Sign_off_Name__c = allFields.AO_Sign_off_Name__c;
                    //report.FGM_Base__Award_Date__c = allFields.FGM_Base__Award_Date__c;
                    report.b_Choosing_designing_interventions__c = allFields.b_Choosing_designing_interventions__c;
                    report.c_Choosing_outcomes_and_deciding_how_to__c = allFields.c_Choosing_outcomes_and_deciding_how_to__c;
                    report.Category__c = allFields.Category__c;
                    report.Change_Type__c = allFields.Change_Type__c;
                    //report.Choosing_or_designing_interventions_or_c__c = allFields.Choosing_or_designing_interventions_or_c__c; //Commented By Vijay
                    //report.Choosing_outcomes_and_deciding_how_to_me__c = allFields.Choosing_outcomes_and_deciding_how_to_me__c; //Commented By Vijay
                    report.FGM_Base__Comment__c = allFields.FGM_Base__Comment__c;
                    report.Completed__c = allFields.Completed__c;
                    //report.FGM_Base__Contact_Organization__c = allFields.FGM_Base__Contact_Organization__c;
                    report.d_Other_aspects_of_study_design__c = allFields.d_Other_aspects_of_study_design__c;
                    report.Data_Integrity_and_Rigorous_Analysis__c = allFields.Data_Integrity_and_Rigorous_Analysis__c;
                    //report.Data_Registries__c = allFields.Data_Registries__c;
                    report.Date_Approved__c = allFields.Date_Approved__c;
                    report.Date_Completed__c = allFIelds.Date_Completed__c;
                    //report.Deciding_what_the_study_is_about_choosi__c = allFields.Deciding_what_the_study_is_about_choosi__c; //Commented By Vijay
                    //report.Describe_notable_progress__c = allFields.Describe_notable_progress__c;
                    report.Describe_the_limitations_of_your_study__c = allFields.Describe_the_limitations_of_your_study__c ;
                    report.Discuss_the_implications_of_your_finding__c = allFields.Discuss_the_implications_of_your_finding__c ;
                    //report.Discuss_implications_of_your_finding__c = allFields.Discuss_implications_of_your_finding__c ;
                    report.FGM_Base__Display_Benchmark__c = allFields.FGM_Base__Display_Benchmark__c ;
                    report.Dissemination__c = allFields.Dissemination__c ;
                    report.FGM_Base__Due_Date__c = allFields.FGM_Base__Due_Date__c ;
                    //report.FGM_Base__Year__c = allFields.FGM_Base__Year__c ;
                    report.Engagement_may_not_occur_at_all_time_poi__c = allFields.Engagement_may_not_occur_at_all_time_poi__c ;
                    report.Explanation_of_Changes__c = allFields.Explanation_of_Changes__c ;
                    report.f_Data_collection__c = allFields.f_Data_collection__c ;
                    //report.FGM_Base__Fax__c = allFields.FGM_Base__Fax__c ;
                    report.FGM_Base__Final_Report__c = allFields.FGM_Base__Final_Report__c;
                    report.For_projects_testing_methodologies__c = allFields.For_projects_testing_methodologies__c;
                    report.g_Analyzing_or_reviewing_results__c = allFields.g_Analyzing_or_reviewing_results__c;
                    report.h_Dissemination__c = allFields.h_Dissemination__c;
                    //report.Heterogeneity_of_Treatment_Effects_HT_3__c = allFields.Heterogeneity_of_Treatment_Effects_HT_3__c;
                    report.How_did_you_engage_with_patients_and_or__c = allFields.How_did_you_engage_with_patients_and_or__c;
                    //report.How_will_your_study_findings_and_oth__c = allFields.How_will_your_study_findings_and_oth__c;
                    report.How_will_your_study_findings_and_other_l__c = allFields.How_will_your_study_findings_and_other_l__c;
                    report.If_Not_Completed_Reason_for_Delay__c = allFields.If_Not_Completed_Reason_for_Delay__c ;
                    report.If_relevant_briefly_summarize_differenc__c = allFields.If_relevant_briefly_summarize_differenc__c ;
                    //report.Instructions__c = allFields.Instructions__c;
                    report.FGM_Base__Impact__c = allFields.FGM_Base__Impact__c;
                    report.Intended_Audience__c = allFields.Intended_Audience__c;
                    report.FGM_Base__Interim_Report__c = allFields.FGM_Base__Interim_Report__c;
                    //report.Is_Overdue__c = allFields.Is_Overdue__c;
                    report.j_The_way_the_research_team_and_partner__c = allFields.j_The_way_the_research_team_and_partner__c;
                    report.k_Research_projects_other_than_this_spe__c = allFields.k_Research_projects_other_than_this_spe__c;
                    report.Key_Personnel__c = allFields.Key_Personnel__c ;
                    report.Late__c = allFields.Late__c ;
                    report.Meeting_Location__c = allFields.Meeting_Location__c;
                    report.Milestone_Name__c = allFields.Milestone_Name__c;
                    //report.FGM_Base__Name__c = allFields.FGM_Base__Name__c;
                    report.Name_of_Publication__c = allFields.Name_of_Publication__c;
                    report.Next5Businessdays__c = allFields.Next5Businessdays__c;
                    report.No_changes_in_Key_Personnel__c = allFields.No_changes_in_Key_Personnel__c;
                    // report.FGM_Base__Organization__c = allFields.FGM_Base__Organization__c;
                    //report.Other__c = allFields.Other__c;
                    report.Other_Please_describe__c = allFields.Other_Please_describe__c;
                    //report.Other_Accomplishments__c = allFields.Other_Accomplishments__c;
                    //report.Other_aspects_of_study_design__c = allFields.Other_aspects_of_study_design__c;
                    //report.Other_Challenges__c = allFields.Other_Challenges__c;
                    report.Other_part_of_the_project_Please_descri__c = allFields.Other_part_of_the_project_Please_descri__c;
                    report.Other_Reason_For_Delay__c = allFields.Other_Reason_For_Delay__c;
                    report.Other_Research_Projects__c = allFields.Other_Research_Projects__c;
                    
                    report.Other_Type__c = allFields.Other_Type__c;
                    report.Oversight_Summary__c = allFields.Oversight_Summary__c;
                    report.Page_Numbers__c = allFields.Page_Numbers__c;
                    //report.PCORI_contract_number__c = allFields.PCORI_contract_number__c;
                    //report.PCORI_Project_Adherence__c= allFields.PCORI_Project_Adherence__c;
                    report.PCORI_s_Methodology_Standards__c = allFields.PCORI_s_Methodology_Standards__c;
                    report.Period_Covered_by_this_Report_End__c = allFields.Period_Covered_by_this_Report_End__c;
                    report.Period_Covered_by_this_Report_Start__c = allFields.Period_Covered_by_this_Report_Start__c;
                    //report.FGM_Base__Phone__c = allFields.FGM_Base__Phone__c;
                    report.PI_Approved__c = allFields.PI_Approved__c;
                    report.PI_Sign_off_checkbox__c = allFields.PI_Sign_off_checkbox__c;
                    report.PI_Sign_off_date__c = allFields.PI_Sign_off_date__c;
                    report.PI_Sign_off_Name__c = allFields.PI_Sign_off_Name__c;
                    report.Please_describe_the_data_management_and__c = allFields.Please_describe_the_data_management_and__c;
                    //report.Please_report_how_your_project_meets__c = allFields.Please_report_how_your_project_meets__c;
                    report.PMID__c = allFields.PMID__c;
                    report.Portal_User__c = allFields.Portal_User__c;
                    report.Presentation_Date__c = allFields.Presentation_Date__c;
                    report.Presentation_Peer_Reviewed__c = allFields.Presentation_Peer_Reviewed__c;
                    report.Presentation_Review_Status_Indicator__c = allFields.Presentation_Review_Status_Indicator__c;
                    report.Presentation_Type__c = allFields.Presentation_Type__c;
                    report.Presenters__c = allFields.Presenters__c;
                    report.Presenters_role_in_the_project__c = allFields.Presenters_role_in_the_project__c ;
                    report.Principal_Investigator_Last_First_Mid__c = allFields.Principal_Investigator_Last_First_Mid__c;
                    report.Principal_Investigator_Sign_Off_Name__c = allFields.Principal_Investigator_Sign_Off_Name__c;
                    report.Progress_During_Reporting_Period__c = allFields.Progress_During_Reporting_Period__c;
                    report.Progress_Report_Name__c = allFields.Progress_Report_Name__c;
                    report.Progress_Report_Time_Period__c = allFields.Progress_Report_Time_Period__c ;
                    report.Project__c = allFields.Project__c;
                    report.PR_Reject__c = allFields.PR_Reject__c ;
                    report.PR_Time_Period_Other_Specify__c = allFields.PR_Time_Period_Other_Specify__c;
                    report.Publication_Reviewed__c = allFields.Publication_Reviewed__c;
                    report.Publication_Status__c = allFields.Publication_Status__c;
                    report.Publication_Type__c = allFields.Publication_Type__c;
                    report.Reason_Returned__c = allFields.Reason_Returned__c ;
                    //report.Recruiting_or_retaining_study_participan__c = allFields.Recruiting_or_retaining_study_participan__c; //Commented By Vijay
                    report.Reject__c = allFields.Reject__c ;
                    report.RejectClone__c = allFields.RejectClone__c ;
                    report.Rejected__c = allFields.Rejected__c;
                    report.FGM_Base__Remind_Awardee__c = allFields.FGM_Base__Remind_Awardee__c;
                    report.FGM_Base__Remind_Staff__c = allFields.FGM_Base__Remind_Staff__c;
                    report.Report_Completion_Date__c = allFields.Report_Completion_Date__c;
                    report.FGM_Base__Report_Contact__c = allFields.FGM_Base__Report_Contact__c;
                    //report.Report_how_these_Standards_have_been_met__c = allFields.Report_how_these_Standards_have_been_met__c;
                    report.Reporting_Period__c = allFields.Reporting_Period__c;
                    report.Report_Type__c = allFields.Report_Type__c;
                    report.Report_Version__c = allFields.Report_Version__c;
                    //report.FGM_Base__Request__c = allFields.FGM_Base__Request__c;
                    //report.FGM_Base__Request_Number__c = allFields.FGM_Base__Request_Number__c;
                    report.Role__c = allFields.Role__c;
                    report.Scientific_Manuscript_Type__c = allFields.Scientific_Manuscript_Type__c;
                    //report.State_the_research_question_s_addressed__c = allFields.State_the_research_question_s_addressed__c ;
                    //report.FGM_Base__Status__c = allFields.FGM_Base__Status__c;
                    report.Status__c = allFields.Status__c;
                    //report.FGM_Base__Submission_Date__c, Submission_Date__c = allFields.FGM_Base__Submission_Date__c, Submission_Date__c;
                    report.FGM_Base__Submitted_By__c = allFields.FGM_Base__Submitted_By__c;
                    report.Submitted_Date_Time__c = allFields.Submitted_Date_Time__c;
                    report.Summarize_project_findings__c = allFields.Summarize_project_findings__c ;
                    report.Summary_of_Changes_From_Proposal__c = allFields.Summary_of_Changes_From_Proposal__c;
                    report.Thank_you_for_your_responses_to_the_abov__c = allFields.Thank_you_for_your_responses_to_the_abov__c;
                    report.The_Way_The_Research_Team_Partner_work__c = allFields.The_Way_The_Research_Team_Partner_work__c;
                    //report.FGM_Base__Title__c = allFields.FGM_Base__Title__c;
                    report.Title__c = allFields.Title__c;
                    report.Type__c = allFields.Type__c;
                    report.URL_if_applicable__c = allFields.URL_if_applicable__c;
                    report.Volume_issue__c = allFields.Volume_issue__c;
                    report.What_if_anything_will_you_do_d__c = allFields.What_if_anything_will_you_do_d__c;
                    report.What_are_the_opportunities_for_futur__c = allFields.What_are_the_opportunities_for_futur__c ;
                    report.When_in_the_project_were_patients_and_or__c = allFields.When_in_the_project_were_patients_and_or__c ;
                    // report.o_are_the_key_end_users_of_you__c = allFields.o_are_the_key_end_users_of_you__c;
                    report.Year__c = allFields.Year__c;
                    report.Patient_Stakeholder_Impact_Outcome__c = allFields.Patient_Stakeholder_Impact_Outcome__c;
                    report.Patient_Stakeholder_Engagement_Summary__c = allFields.Patient_Stakeholder_Engagement_Summary__c;
                    report.Summarize_project_findings_Public__c = allFields.Summarize_project_findings_Public__c;
                    report.Patient_Stakeholder_Impact_Public__c = allFields.Patient_Stakeholder_Impact_Public__c;
                    report.Accomplishments_Achieved_During_Public__c = allFields.Accomplishments_Achieved_During_Public__c;
                    report.Challenges_With_Project_Public__c = allFields.Challenges_With_Project_Public__c;
                    report.Challenges_With_Stakeholder_Public__c = allFields.Challenges_With_Stakeholder_Public__c;
                    report.Patient_Stakeholder_Engagement_Public__c = allFields.Patient_Stakeholder_Engagement_Public__c;
                    report.Significant_Decisions_Findings_Public__c = allFields.Significant_Decisions_Findings_Public__c;
                    report.Start_Date__C = allFields.Start_Date__C;
                    report.FGM_Base__Due_Date__c = allFields.FGM_Base__Due_Date__c;
                    report.FGM_Base__Submission_Date__c = allFields.FGM_Base__Submission_Date__c;
                    
                    if(report.Report_Version__c == null) 
                    {
                        
                        report.Report_Version__c = 0;
                    }
                    
                    report.Report_Version__c = report.Report_Version__c + 1;
                    granteeListToInsert.add(report);
                    
                }
            }
        }
    }
    
    if(!granteeListToInsert.isEmpty())
    {
        system.debug('Insertionnnnnnnnnnnnnnnnnnnnnnnnnnn');
        insert granteeListToInsert;
    }
}
}