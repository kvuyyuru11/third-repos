@isTest
private class OpportunityServiceTest{
    static testMethod void OpportunityServiceTestmethod() {
        
        //Gets the recordtypeId of the Opportunity Recordtype "Research Awards"
        Schema.DescribeSObjectResult oppsche = Schema.SObjectType.Opportunity;
        Map<string, schema.RecordtypeInfo> sObjectMap = oppsche.getRecordtypeInfosByName();
        Id stroppRecordTypeId = sObjectMap.get('Research Awards').getRecordTypeId();
        
        list<Account> acnt=TestDataFactory.getAccountTestRecords(1);
        list<contact> con=TestDataFactory.getContactTestRecords(1,1); 
        list<opportunity> opp=TestDataFactory.getresearchawardsopportunitytestrecords(1,2,10); 
        test.startTest();
        OpportunityService.insertRecruitment(opp);
        opp[0].Contract_Number__c='1234';
        opp[1].Contract_Number__c='123413';
        opp[2].Contract_Number__c='123412';
        opp[3].Contract_Number__c='123411';
        opp[4].Contract_Number__c='123410';
        opp[5].Contract_Number__c='12349';
        opp[6].Contract_Number__c='12348';
        opp[7].Contract_Number__c='12347';
        opp[8].Contract_Number__c='12346';
        opp[9].Contract_Number__c='12345';
            update opp;
        OpportunityService.updateRecruitment(opp);
            test.stopTest();
        
        
    }
}