//This is a helper class that is used to support the controller of the COIBoard of Directors
public class AddMultipleAffiliationHelperClass {
       
    //method for adding rows for financial and business block
    public static List<AddMultipleAffiliationsClass.WrapperpaAffiliationList> addNewRowToAffList(List<AddMultipleAffiliationsClass.WrapperpaAffiliationList> waAffObjList){
        AddMultipleAffiliationsClass.WrapperpaAffiliationList newRecord = new AddMultipleAffiliationsClass.WrapperpaAffiliationList();
        Affiliation__c newAffiliationRecord = new Affiliation__c();        
        newRecord.record = newAffiliationRecord;
        newRecord.index = waAffObjList.size();
        waAffObjList.add(newRecord);
        return waAffObjList; 
    }
    //method for adding rows for personal block
     public static List<AddMultipleAffiliationsClass.WrapperpaAffiliationList> addNewRowToAffList2(List<AddMultipleAffiliationsClass.WrapperpaAffiliationList> waAffObjList2){
        AddMultipleAffiliationsClass.WrapperpaAffiliationList newRecord = new AddMultipleAffiliationsClass.WrapperpaAffiliationList();
        Affiliation__c newAffiliationRecord = new Affiliation__c();        
        newRecord.record = newAffiliationRecord;
        newRecord.index = waAffObjList2.size();
        waAffObjList2.add(newRecord);
        return waAffObjList2; 
    }
    
        //method for removing rows for financial and business block
     public static List<AddMultipleAffiliationsClass.WrapperpaAffiliationList> removeRowToAffiliationList(Integer rowToRemove, List<AddMultipleAffiliationsClass.WrapperpaAffiliationList> waAffiliationList){
        waAffiliationList.remove(rowToRemove);
        return waAffiliationList;
    }
    //method for removing rows for personal block
        public static List<AddMultipleAffiliationsClass.WrapperpaAffiliationList> removeRowToAffiliationList2(Integer rowToRemove2, List<AddMultipleAffiliationsClass.WrapperpaAffiliationList> waAffiliationList2){
        waAffiliationList2.remove(rowToRemove2);
        return waAffiliationList2;
    }

    //method for creating affiliation records for financial and business block
     public static void save(List<AddMultipleAffiliationsClass.WrapperpaAffiliationList> waAffList) {
         
          User Uid=[select id,contactid from User where id=:userinfo.getUserId() limit 1];
               Contact Contctid=[select id from contact where id=:uid.contactid limit 1];            
        system.debug('==waAffList==>'+waAffList.size());
        List<Affiliation__c> affiliationRecordsToBeInserted = new List<Affiliation__c>();
       
        if(waAffList !=null && !waAffList.isEmpty()){
            for(AddMultipleAffiliationsClass.WrapperpaAffiliationList aff : waAffList ){
                
                Affiliation__c accTemp = aff.record;
               accTemp.Contact__c=Contctid.id;
                accTemp.Financial_and_Business_Association__c=true;
                affiliationRecordsToBeInserted.add(accTemp);
                     
            system.debug('==affiliationRecordsToBeInserted==>'+affiliationRecordsToBeInserted.size());
            }
          
            database.insert(affiliationRecordsToBeInserted);

        }
    }
    
     //method for creating affiliation records for personal block
    public static void save2(List<AddMultipleAffiliationsClass.WrapperpaAffiliationList> waAffList2) {
         
          User Uid=[select id,contactid from User where id=:userinfo.getUserId() limit 1];
         Contact Contctid=[select id from contact where id=:uid.contactid limit 1];
                     
        system.debug('==waAffList2==>'+waAffList2.size());
        List<Affiliation__c> affiliationRecordsToBeInserted2 = new List<Affiliation__c>();
       
        if(waAffList2 !=null && !waAffList2.isEmpty() ){
            for(AddMultipleAffiliationsClass.WrapperpaAffiliationList aff : waAffList2 ){
               
                Affiliation__c accTemp = aff.record; 
               accTemp.Contact__c=Contctid.id; 
           accTemp.Personal_Association__c=true;
                affiliationRecordsToBeInserted2.add(accTemp);
   
            system.debug('==affiliationRecordsToBeInserted2==>'+affiliationRecordsToBeInserted2.size());
            }
        
            database.insert(affiliationRecordsToBeInserted2);

        }
    } 
}