/****************************************************************************************************
*Description:           This class provides the unit test coverage for the 
*                       FoundationConnectPasswordUpdate trigger
*
*Required Class(es):    N/A
*
*Organization: Rainmaker-LLC
*Revision   |Date       |Author             |AdditionalNotes
*====================================================================================================
*   1.0     03/28/2014   Justin Padilla      Initial Version
*****************************************************************************************************/
@isTest
private class FoundationConnectPasswordUpdate_Test {

    static testMethod void TriggerTest()
    {
        String profileId = Label.CommunityAdminAccount;
        //User u = [SELECT Id,Email FROM User WHERE ProfileId =:profileId LIMIT 1];
        Account account = new Account(Name='Test');
    	insert account ;
    	Contact contact = new Contact(FirstName='Rainmaker', LastName='Test', AccountId=account.Id);
    	insert contact;
		User newUser = new User(
        profileId = profileId,
        username = 'newUser' + Integer.valueOf(Math.random()) + '@yahoo.com',
        email = 'pb@ff.com',
        emailencodingkey = 'UTF-8',
        localesidkey = 'en_US',
        languagelocalekey = 'en_US',
        timezonesidkey = 'America/Los_Angeles',
        alias='nuser',
        lastname='lastname',
        contactId = contact.id
        );
        insert newUser;
    }
    
    static testMethod void TriggerTest1()
    {
        String profileId = Label.CommunityAdminAccount;
        Account account = new Account(Name='Test');
    	insert account ;
    	Contact contact = new Contact(FirstName='Rainmaker', LastName='Test', AccountId=account.Id);
    	insert contact;
		User newUser = new User(
        profileId = profileId,
        username = 'newUser' + Integer.valueOf(Math.random()) + '@yahoo.com',
        email = 'pb@ff.com',
        emailencodingkey = 'UTF-8',
        localesidkey = 'en_US',
        languagelocalekey = 'en_US',
        timezonesidkey = 'America/Los_Angeles',
        alias='nuser',
        lastname='lastname',
        contactId = contact.id
        );
        insert newUser;
        System.runAs(newUser){
            newUser.Email = 'Name@gmail.com';
            update newUser;
        }

    }
}