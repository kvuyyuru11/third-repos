/*
* Class : SharingReviewsTriggerHandlerClass
* Author : Kalyan Vuyyuru (PCORI)
* Version History : 1.0
* Description : This is a Handler Class for SharingReviewsTrigger which is been called on insert or update which has two methods on isert and update
where reviewsshares, opportunitiesteammembers and leadsshares are shared and deleted accordingly.
*/
public class SharingReviewsTriggerHandlerClass {
    
    //This Method is called on insert of reviews to share the reviews, opportunities and leads if any accordingly
    @future
    public static void MethodonInsertofReviews(set<ID> reviewsListId){
        list<FC_Reviewer__External_Review__c> reviewsList=[select Id,FC_Reviewer__Contact__c,FC_Reviewer__Opportunity__c,FC_Reviewer__Lead__c from FC_Reviewer__External_Review__c where ID IN:reviewsListId];  
        set<Id> cntlist = new set<Id>();
        set<Id> oplist= new set<Id>();
        set<Id> ldlist= new set<Id>();
        set<Id> rvlist= new set<Id>();
        list<User> usrlist= new list<User>();
        //First ID Key is contactId and second Id Value is UserId
        Map<ID,ID> revscntandusermap=new Map<Id,Id>();
        Map<ID,set<ID>> opptandusermap=new Map<Id,set<Id>>();
        Map<ID,set<ID>> leadandusermap=new Map<Id,set<Id>>();
        List<FC_Reviewer__External_Review__share> reviewShares= new List<FC_Reviewer__External_Review__share>();
        set<ID> reviewSharesIds= new set<ID>();
        List<FC_Reviewer__External_Review__share> reviewShareslisttoInsert= new List<FC_Reviewer__External_Review__share>();
        List<OpportunityTeamMember> optShares= new List<OpportunityTeamMember>();
        List<OpportunityShare> oppsharelist=new list<OpportunityShare>();
        List<OpportunityTeamMember> optShareslisttoInsert= new List<OpportunityTeamMember>();
        List<Leadshare> leadShares= new List<Leadshare>();
        List<Leadshare> leadShareslisttoInsert= new List<Leadshare>();
        //Iterating the list and collecting all contacts, opportunities,reviews and leads Id's.
        for(FC_Reviewer__External_Review__c er:reviewsList){
            if(er.FC_Reviewer__Contact__c!=null){
                cntlist.add(er.FC_Reviewer__Contact__c);
            }
            if(er.FC_Reviewer__Opportunity__c!=null){
                oplist.add(er.FC_Reviewer__Opportunity__c);
                system.debug('***'+oplist);
            }
            if(er.FC_Reviewer__Lead__c!=null){
                ldlist.add(er.FC_Reviewer__Lead__c);
            }
            // rvlist.add(er.Id);
        }
        //from the list collected now checking for existing shares on reviews,opportunities and leads and also getting user related data from contacts on reviews   
        usrlist=[select id,contactId from User where contactid IN : cntlist];
        // reviewShares = [SELECT id, ParentId, UserOrGroupId From FC_Reviewer__External_Review__share where ParentId IN :rvlist];
        optShares=[SELECT id, OpportunityId , UserId FROM OpportunityTeamMember where OpportunityId IN : oplist];
         oppsharelist=[SELECT id, OpportunityId , userorgroupid FROM opportunityshare where OpportunityId IN : oplist];
        system.debug('***'+optShares);
        leadShares=[SELECT id, LeadId , UserOrGroupId FROM LeadShare where LeadId IN :ldlist];
        if(!usrlist.isEmpty() && usrlist.size()>0){
            for(User u:usrlist){
                revscntandusermap.put(u.contactId,u.Id); 
            }
        }
        
        // for(FC_Reviewer__External_Review__share rs:reviewShares){
        //reviewSharesIds.add(rs.UserOrGroupId);
        //}
        
        
        for(OpportunityTeamMember os:optShares){
            if(!opptandusermap.containsKey(os.OpportunityId)) {
                set<Id> temp = new set<Id>();
                temp.add(os.UserId);
                opptandusermap.put(os.OpportunityId,temp);
            }
            else
            {
                set<Id> temp = opptandusermap.get(os.OpportunityId);
                temp.add(os.UserId);
                opptandusermap.put(os.OpportunityId,temp);
            }
            
        }
        system.debug('***'+opptandusermap);
        
        for(Leadshare ls:leadShares){
            if(!leadandusermap.containsKey(ls.LeadId)) {
                set<Id> temp1 = new set<Id>();
                temp1.add(ls.UserOrGroupId);
                leadandusermap.put(ls.LeadId,temp1);
            }
            else
            {
                set<Id> temp1 = leadandusermap.get(ls.LeadId);
                temp1.add(ls.UserOrGroupId);
                leadandusermap.put(ls.LeadId,temp1);
            }
        }
        
        for(FC_Reviewer__External_Review__c er1:reviewsList){
            //checking if the contact on the reviews definitely has a user and also ensuring that user is not already having share to the review record and only then adding him in the list to insert review shares.
            /*if(revscntandusermap.get(er1.FC_Reviewer__Contact__c)!=null && !reviewSharesIds.contains(revscntandusermap.get(er1.FC_Reviewer__Contact__c))){
FC_Reviewer__External_Review__share newrvwsharetoInsert = new FC_Reviewer__External_Review__share();
newrvwsharetoInsert.AccessLevel='Edit';
newrvwsharetoInsert.ParentId=er1.Id;
newrvwsharetoInsert.UserOrGroupId=revscntandusermap.get(er1.FC_Reviewer__Contact__c);
reviewShareslisttoInsert.add(newrvwsharetoInsert);
}*/
            //checking if the contact on the reviews definitely has a user and also ensuring that user is not already having share to the opportunity record and only then adding him in the list to insert opportunity shares.  
            if(opptandusermap.isEmpty()){
            if(revscntandusermap.get(er1.FC_Reviewer__Contact__c)!=null && er1.FC_Reviewer__Opportunity__c!=null){
                OpportunityTeamMember oppSharetoInsert = new OpportunityTeamMember();
                oppSharetoInsert.OpportunityAccessLevel='Read';
                oppSharetoInsert.OpportunityId=er1.FC_Reviewer__Opportunity__c;
                oppSharetoInsert.UserId=revscntandusermap.get(er1.FC_Reviewer__Contact__c);
                optShareslisttoInsert.add(oppSharetoInsert);
            }
}
            if(!opptandusermap.isEmpty()){
            if(revscntandusermap.get(er1.FC_Reviewer__Contact__c)!=null && er1.FC_Reviewer__Opportunity__c!=null && opptandusermap.get(er1.FC_Reviewer__Opportunity__c)!=null && !opptandusermap.get(er1.FC_Reviewer__Opportunity__c).contains(revscntandusermap.get(er1.FC_Reviewer__Contact__c))){
                OpportunityTeamMember oppSharetoInsert = new OpportunityTeamMember();
                oppSharetoInsert.OpportunityAccessLevel='Read';
                oppSharetoInsert.OpportunityId=er1.FC_Reviewer__Opportunity__c;
                oppSharetoInsert.UserId=revscntandusermap.get(er1.FC_Reviewer__Contact__c);
                optShareslisttoInsert.add(oppSharetoInsert);
            }
            }
            system.debug('***'+optShareslisttoInsert);
            //checking if the contact on the reviews definitely has a user and also ensuring that user is not already having share to the lead record and only then adding him in the list to insert lead shares.             
            if(revscntandusermap.get(er1.FC_Reviewer__Contact__c)!=null && er1.FC_Reviewer__Lead__c!=null && leadandusermap.get(er1.FC_Reviewer__Lead__c)!=null && !leadandusermap.get(er1.FC_Reviewer__Lead__c).contains(revscntandusermap.get(er1.FC_Reviewer__Contact__c))){
                LeadShare leadSharetoInsert = new LeadShare();
                leadSharetoInsert.LeadAccessLevel='Read';
                leadSharetoInsert.LeadId=er1.FC_Reviewer__Lead__c;
                leadSharetoInsert.UserOrGroupId=revscntandusermap.get(er1.FC_Reviewer__Contact__c);
                leadShareslisttoInsert.add(leadSharetoInsert);
            }
            
        }
        //if(!reviewShareslisttoInsert.isEmpty() && reviewShareslisttoInsert.size()>0){
        // database.insert(reviewShareslisttoInsert);
        // }
        if(!optShareslisttoInsert.isEmpty() && optShareslisttoInsert.size()>0){
            database.insert(optShareslisttoInsert);
        }
        
        if(!leadShareslisttoInsert.isEmpty() && leadShareslisttoInsert.size()>0){
            database.insert(leadShareslisttoInsert);  
        }
        
    }
    
    //This Method is called on update of reviews to share the reviews, opportunities and leads if any accordingly
    public static void MethodonUpdateofReviews(list<FC_Reviewer__External_Review__c> NewreviewsList,list<FC_Reviewer__External_Review__c> OldreviewsList,map<Id, FC_Reviewer__External_Review__c> newMapofReviews, map<Id, FC_Reviewer__External_Review__c> oldMapofReviews){
        set<Id> cntlist = new set<Id>();
        set<Id> oldcntlist= new set<Id>();
        set<Id> oplist= new set<Id>();
        set<Id> ldlist= new set<Id>();
        set<Id> rvlist= new set<Id>();
        list<User> usrlist= new list<User>();
        list<User> oldusrlist= new list<User>();
        //First ID Key is contactId and second Id Value is UserId
        Map<ID,ID> revscntandusermap=new Map<Id,Id>();
        //First ID Key is contactId and second Id Value is UserId but this map id oldmap /previous values.
        Map<ID,ID> revscntanduseroldmap=new Map<Id,Id>(); 
        List<FC_Reviewer__External_Review__share> reviewShares= new List<FC_Reviewer__External_Review__share>();
        set<ID> reviewSharesIds= new set<ID>();
        List<FC_Reviewer__External_Review__share> reviewShareslisttoInsert= new List<FC_Reviewer__External_Review__share>();
        List<FC_Reviewer__External_Review__c> reviewstoupdate= new List<FC_Reviewer__External_Review__c>();
        set<Id> reviewSharesuserlisttoDelete= new set<Id>();
        List<OpportunityTeamMember> optShares= new List<OpportunityTeamMember>();
        set<ID> optSharesIds= new set<ID>();
        List<OpportunityTeamMember> optShareslisttoInsert= new List<OpportunityTeamMember>();
        set<Id> opportSharesuserlisttoDelete= new set<Id>();
        List<Leadshare> leadShares= new List<Leadshare>();
        set<ID> leadSharesIds= new set<ID>();
        List<Leadshare> leadShareslisttoInsert= new List<Leadshare>();
        set<Id> leadSharesuserlisttoDelete= new set<Id>();
        set<ID> reviewparentshareslisttodelete = new set<ID>();
        set<ID> oppparentshareslisttodelete = new set<ID>();
        set<ID> leadparentshareslisttodelete = new set<ID>();
        list<FC_Reviewer__External_Review__share> reviewSharestodelete= new List<FC_Reviewer__External_Review__share>();
        List<OpportunityTeamMember> optSharestodelete= new List<OpportunityTeamMember>();
        List<Leadshare> leadSharestodelete = new List<Leadshare>();
        //Iterating the list and collecting all contacts, opportunities,reviews and leads Id's.
        for(FC_Reviewer__External_Review__c er:NewreviewsList){
            
            if(er.FC_Reviewer__Contact__c!=null){
                cntlist.add(er.FC_Reviewer__Contact__c);
            }
            if(er.FC_Reviewer__Opportunity__c!=null){
                oplist.add(er.FC_Reviewer__Opportunity__c);
            }
            if(er.FC_Reviewer__Lead__c!=null){
                ldlist.add(er.FC_Reviewer__Lead__c);
            }
            rvlist.add(er.Id);
            
        }
        //Getting contact and user related data for old map  
        for(FC_Reviewer__External_Review__c er2:OldreviewsList){
            if(er2.FC_Reviewer__Contact__c!=null){
                oldcntlist.add(er2.FC_Reviewer__Contact__c);
            }
        }
        //from the list collected now checking for existing shares on reviews,opportunities and leads and also getting user related data from contacts on reviews
        usrlist=[select id,contactId from User where contactid IN : cntlist];
        oldusrlist=[select id,contactId from User where contactid IN : oldcntlist];
        reviewShares = [SELECT id, ParentId, UserOrGroupId From FC_Reviewer__External_Review__share where ParentId IN :rvlist];
        optShares=[SELECT id, OpportunityId ,UserId FROM OpportunityTeamMember where OpportunityId IN : oplist];
        leadShares=[SELECT id, LeadId , UserOrGroupId FROM LeadShare where LeadId IN :ldlist];
        if(!usrlist.isEmpty() && usrlist.size()>0){
            for(User u:usrlist){
                revscntandusermap.put(u.contactId,u.Id); 
            }
        }
        if(!oldusrlist.isEmpty() && oldusrlist.size()>0){
            for(User u1:oldusrlist){
                revscntanduseroldmap.put(u1.contactId,u1.Id); 
            }
        }
        if(!reviewShares.isEmpty() && reviewShares.size()>0){
            for(FC_Reviewer__External_Review__share rs:reviewShares){
                reviewSharesIds.add(rs.UserOrGroupId);
            }}
        
        if(!optShares.isEmpty() && optShares.size()>0){
            for(OpportunityTeamMember os:optShares){
                optSharesIds.add(os.userId); 
            }}
        
        if(!leadShares.isEmpty() && leadShares.size()>0){
            for(Leadshare ls:leadShares){
                leadSharesIds.add(ls.userorgroupId); 
            }}   
        
        for(FC_Reviewer__External_Review__c er1:NewreviewsList){
            //checking if user related to the contact is not null and if contact changed on review and then if changed without opportunity change then adding the new contact user to list to insert share for him on review
            //if((er1.FC_Reviewer__Contact__c!= null ) && (er1.FC_Reviewer__Contact__c != oldMapofReviews.get(er1.Id).FC_Reviewer__Contact__c) && (er1.FC_Reviewer__Opportunity__c == oldMapofReviews.get(er1.Id).FC_Reviewer__Opportunity__c)){
            
            //if(revscntandusermap.get(er1.FC_Reviewer__Contact__c)!=null && !reviewSharesIds.contains(revscntandusermap.get(er1.FC_Reviewer__Contact__c)) && !reviewSharesIds.isEmpty() && reviewSharesIds.size()>0){
            //  er1.OwnerId=revscntandusermap.get(er1.FC_Reviewer__Contact__c);
            // reviewstoupdate.add(er1);
            //}
            //checking if user related to the contact is not null and if contact changed on review and then if changed without opportunity change then did above and also adding him to ooportunity share list to insert
            // if(revscntandusermap.get(er1.FC_Reviewer__Contact__c)!=null && er1.FC_Reviewer__Opportunity__c !=null){
            // OpportunityTeamMember oppSharetoInsert = new OpportunityTeamMember();
            // oppSharetoInsert.OpportunityAccessLevel='Read';
            // oppSharetoInsert.OpportunityId=er1.FC_Reviewer__Opportunity__c;
            // oppSharetoInsert.UserId=revscntandusermap.get(er1.FC_Reviewer__Contact__c);
            // optShareslisttoInsert.add(oppSharetoInsert);
            //}
            
            //}
            //now also looking at old contact if user of old contact not null we need to ensure that his share to review and opportunity are removed and adding him in the list to delete         
            // if((er1.FC_Reviewer__Contact__c!= null ) && (oldMapofReviews.get(er1.Id).FC_Reviewer__Contact__c!=null) && (er1.FC_Reviewer__Opportunity__c == oldMapofReviews.get(er1.Id).FC_Reviewer__Opportunity__c) && (er1.FC_Reviewer__Contact__c != oldMapofReviews.get(er1.Id).FC_Reviewer__Contact__c)){
            // if((revscntanduseroldmap.get(oldMapofReviews.get(er1.Id).FC_Reviewer__Contact__c)!=null) && (reviewSharesIds.contains(revscntanduseroldmap.get(oldMapofReviews.get(er1.Id).FC_Reviewer__Contact__c)))){
            //reviewSharesuserlisttoDelete.add(revscntanduseroldmap.get(oldMapofReviews.get(er1.Id).FC_Reviewer__Contact__c));
            // opportSharesuserlisttoDelete.add(revscntanduseroldmap.get(oldMapofReviews.get(er1.Id).FC_Reviewer__Contact__c));
            // reviewparentshareslisttodelete.add(er1.Id);
            // oppparentshareslisttodelete.add(er1.FC_Reviewer__Opportunity__c);
            //  }
            
            // }
            //now also looking at old contact if user of old contact not null. This case is when old contact on review exists and new contact was null on review update. we need to ensure that his share to review and opportunity are removed and adding him in the list to delete          
            if((er1.FC_Reviewer__Contact__c==null) && (oldMapofReviews.get(er1.Id).FC_Reviewer__Contact__c!=null) && (er1.FC_Reviewer__Opportunity__c == oldMapofReviews.get(er1.Id).FC_Reviewer__Opportunity__c)){
                if((revscntanduseroldmap.get(oldMapofReviews.get(er1.Id).FC_Reviewer__Contact__c)!=null) && (reviewSharesIds.contains(revscntanduseroldmap.get(oldMapofReviews.get(er1.Id).FC_Reviewer__Contact__c)))){
                    if(er1.Contact_Copied_due_to_COI__c==null){
                        //reviewSharesuserlisttoDelete.add(revscntanduseroldmap.get(oldMapofReviews.get(er1.Id).FC_Reviewer__Contact__c));
                    }
                    opportSharesuserlisttoDelete.add(revscntanduseroldmap.get(oldMapofReviews.get(er1.Id).FC_Reviewer__Contact__c));
                    //reviewparentshareslisttodelete.add(er1.Id);
                    oppparentshareslisttodelete.add(er1.FC_Reviewer__Opportunity__c);
                }
            }
            //now checking if the review update was on opportunity with contact being the same then we need to ensure new opportunity should be shared to contact user on the review record and old share to opportunity record needs to be deleted.           
            if((er1.FC_Reviewer__Contact__c!= null ) && (er1.FC_Reviewer__Opportunity__c!=null) && (er1.FC_Reviewer__Contact__c == oldMapofReviews.get(er1.Id).FC_Reviewer__Contact__c) && (er1.FC_Reviewer__Opportunity__c != oldMapofReviews.get(er1.Id).FC_Reviewer__Opportunity__c)){
                if(revscntandusermap.get(er1.FC_Reviewer__Contact__c)!=null){
                    OpportunityTeamMember oppSharetoInsert = new OpportunityTeamMember();
                    oppSharetoInsert.OpportunityAccessLevel='Read';
                    oppSharetoInsert.OpportunityId=er1.FC_Reviewer__Opportunity__c;
                    oppSharetoInsert.UserId=revscntandusermap.get(er1.FC_Reviewer__Contact__c);
                    optShareslisttoInsert.add(oppSharetoInsert);
                }
            }
            //checking if new opportunity is different from old opportunity and old opportunity and new not null then deleting share from old opportunity adding into list
            if((oldMapofReviews.get(er1.Id).FC_Reviewer__Opportunity__c!=null) && (er1.FC_Reviewer__Contact__c == oldMapofReviews.get(er1.Id).FC_Reviewer__Contact__c) && (er1.FC_Reviewer__Opportunity__c != oldMapofReviews.get(er1.Id).FC_Reviewer__Opportunity__c)){
                if(revscntandusermap.get(er1.FC_Reviewer__Contact__c)!=null){
                    opportSharesuserlisttoDelete.add(revscntandusermap.get(er1.FC_Reviewer__Contact__c));
                    oppparentshareslisttodelete.add(oldMapofReviews.get(er1.Id).FC_Reviewer__Opportunity__c);    
                }
            } 
            //checking if new opportunity is different from old opportunity and old opportunity not null and new opportunity null then deleting share from old opportunity adding into list
            if((er1.FC_Reviewer__Opportunity__c==null) && (oldMapofReviews.get(er1.Id).FC_Reviewer__Opportunity__c!=null) && (er1.FC_Reviewer__Contact__c == oldMapofReviews.get(er1.Id).FC_Reviewer__Contact__c)){
                if(revscntandusermap.get(er1.FC_Reviewer__Contact__c)!=null){
                    opportSharesuserlisttoDelete.add(revscntandusermap.get(er1.FC_Reviewer__Contact__c));
                    oppparentshareslisttodelete.add(oldMapofReviews.get(er1.Id).FC_Reviewer__Opportunity__c); 
                }
            }
            //now checking if the review update was on lead with contact being the same then we need to ensure new lead should be shared to contact user on the review record and old share to lead record needs to be deleted.           
            
            if((er1.FC_Reviewer__Contact__c!= null ) && (er1.FC_Reviewer__Lead__c!=null) && (er1.FC_Reviewer__Contact__c == oldMapofReviews.get(er1.Id).FC_Reviewer__Contact__c) && (er1.FC_Reviewer__Lead__c != oldMapofReviews.get(er1.Id).FC_Reviewer__Lead__c)){
                if(revscntandusermap.get(er1.FC_Reviewer__Contact__c)!=null){
                    LeadShare leadSharetoInsert = new LeadShare();
                    leadSharetoInsert.LeadAccessLevel='Read';
                    leadSharetoInsert.LeadId=er1.FC_Reviewer__Lead__c;
                    leadSharetoInsert.UserOrGroupId=revscntandusermap.get(er1.FC_Reviewer__Contact__c);
                    leadShareslisttoInsert.add(leadSharetoInsert);
                }
            }
            //checking if new lead is different from old lead and old lead and new not null then deleting share from old lead adding into list          
            if((oldMapofReviews.get(er1.Id).FC_Reviewer__Lead__c!=null) && (er1.FC_Reviewer__Contact__c == oldMapofReviews.get(er1.Id).FC_Reviewer__Contact__c)){
                if(revscntandusermap.get(er1.FC_Reviewer__Contact__c)!=null){
                    leadSharesuserlisttoDelete.add(revscntandusermap.get(er1.FC_Reviewer__Contact__c));
                    leadparentshareslisttodelete.add(oldMapofReviews.get(er1.Id).FC_Reviewer__Lead__c);    
                }
            } 
            //checking if new lead is different from old lead and old lead not null and new lead null then deleting share from old lead adding into list          
            if((er1.FC_Reviewer__Lead__c==null) && (oldMapofReviews.get(er1.Id).FC_Reviewer__Lead__c!=null) && (er1.FC_Reviewer__Contact__c == oldMapofReviews.get(er1.Id).FC_Reviewer__Contact__c)){
                if(revscntandusermap.get(er1.FC_Reviewer__Contact__c)!=null){
                    leadSharesuserlisttoDelete.add(revscntandusermap.get(er1.FC_Reviewer__Contact__c));
                    leadparentshareslisttodelete.add(oldMapofReviews.get(er1.Id).FC_Reviewer__Lead__c); 
                }
            }
            
            
        }
        
        // if(!reviewstoupdate.isEmpty() && reviewstoupdate.size()>0){
        //database.update(reviewstoupdate);
        // }
        if(!optShareslisttoInsert.isEmpty() && optShareslisttoInsert.size()>0){
            database.insert(optShareslisttoInsert);
        }
        
        if(!leadShareslisttoInsert.isEmpty() && leadShareslisttoInsert.size()>0){
            database.insert(leadShareslisttoInsert);  
        }
        //collecting final share records from the list above to delete 
        // reviewSharestodelete=[select id from FC_Reviewer__External_Review__share where UserOrGroupId IN:reviewSharesuserlisttoDelete AND ParentId IN:reviewparentshareslisttodelete];
        optSharestodelete=[select id from OpportunityTeamMember where userId IN:opportSharesuserlisttoDelete AND opportunityId IN:oppparentshareslisttodelete];
        leadSharestodelete=[select id from Leadshare where UserOrGroupId IN:leadSharesuserlisttoDelete AND LeadId IN:leadparentshareslisttodelete];
        
        //if(!reviewSharestodelete.isEmpty() && reviewSharestodelete.size()>0){
        // database.delete(reviewSharestodelete);
        //}
        if(!optSharestodelete.isEmpty() && optSharestodelete.size()>0){
            database.delete(optSharestodelete);
        }
        
        if(!leadSharestodelete.isEmpty() && leadSharestodelete.size()>0){
            database.delete(leadSharestodelete);  
        }
        
       
    }
    
    public static void MethodonbeforeUpdateofReviewsforHistory(list<FC_Reviewer__External_Review__c> NewreviewsList,list<FC_Reviewer__External_Review__c> OldreviewsList,map<Id, FC_Reviewer__External_Review__c> newMapofReviews, map<Id, FC_Reviewer__External_Review__c> oldMapofReviews){
                List<FC_Reviewer__External_Review__c> reviewstoupdateforhistory= new List<FC_Reviewer__External_Review__c>();
         for(FC_Reviewer__External_Review__c er5:NewreviewsList){
            if(er5.MRO_Action__c=='Revisions Out' && oldMapofReviews.get(er5.Id).MRO_Action__c!='Revisions Out'){
                if(er5.Criterion_1_Strengths__c!=null){
                er5.Criterion_1_Strengths_History__c=System.Now()+' '+oldMapofReviews.get(er5.Id).Criterion_1_Strengths__c;
                }
                if(er5.Criterion_1_Strengths__c==null){
             er5.Criterion_1_Strengths_History__c=string.valueof(System.Now());
  }
                if(er5.Criterion_2_Strengths__c!=null){
                er5.Criterion_2_Strengths_History__c=System.Now()+' '+oldMapofReviews.get(er5.Id).Criterion_2_Strengths__c;
                }
                               if(er5.Criterion_2_Strengths__c==null){
             er5.Criterion_2_Strengths_History__c=string.valueof(System.Now());
  } 
                 if(er5.Criterion_3_Strengths__c!=null){
                er5.Criterion_3_Strengths_History__c=System.Now()+' '+oldMapofReviews.get(er5.Id).Criterion_3_Strengths__c;
                 }
                if(er5.Criterion_3_Strengths__c==null){
             er5.Criterion_3_Strengths_History__c=string.valueof(System.Now());
  } 
                if(er5.Criterion_4_Strengths__c!=null){
                er5.Criterion_4_Strengths_History__c=System.Now()+' '+oldMapofReviews.get(er5.Id).Criterion_4_Strengths__c;
                }
                if(er5.Criterion_4_Strengths__c==null){
                er5.Criterion_4_Strengths_History__c=string.valueof(System.Now());
                }
                if(er5.Criterion_5_Strengths_r2__c!=null){
                er5.Criterion_5_Strengths_History__c=System.Now()+' '+oldMapofReviews.get(er5.Id).Criterion_5_Strengths_r2__c;
                }
                if(er5.Criterion_5_Strengths_r2__c==null){
                er5.Criterion_5_Strengths_History__c=string.valueof(System.Now());            
                }
                if(er5.Criterion_6_Strengths__c!=null){
                er5.Criterion_6_Strengths_History__c=System.Now()+' '+oldMapofReviews.get(er5.Id).Criterion_6_Strengths__c;
                }
                if(er5.Criterion_6_Strengths__c==null){
                er5.Criterion_6_Strengths_History__c=string.valueof(System.Now());                   
                }
                if(er5.Criterion_1_Weaknesses__c!=null){
                er5.Criterion_1_Weaknesses_History__c=System.Now()+' '+oldMapofReviews.get(er5.Id).Criterion_1_Weaknesses__c;
                }
                if(er5.Criterion_1_Weaknesses__c==null){
                er5.Criterion_1_Weaknesses_History__c=string.valueof(System.Now());                    
                }
                if(er5.Criterion_2_Weaknesses__c!=null){
                er5.Criterion_2_Weaknesses_History__c=System.Now()+' '+oldMapofReviews.get(er5.Id).Criterion_2_Weaknesses__c;
                }
                if(er5.Criterion_2_Weaknesses__c==null){
                er5.Criterion_2_Weaknesses_History__c=string.valueof(System.Now());                    
                }
                if(er5.Criterion_3_Weaknesses__c!=null){
                er5.Criterion_3_Weaknesses_History__c=System.Now()+' '+oldMapofReviews.get(er5.Id).Criterion_3_Weaknesses__c;
                }
                if(er5.Criterion_3_Weaknesses__c==null){
                er5.Criterion_3_Weaknesses_History__c=string.valueof(System.Now());                    
                }
                if(er5.Criterion_4_Weaknesses__c!=null){
                er5.Criterion_4_Weaknesses_History__c=System.Now()+' '+oldMapofReviews.get(er5.Id).Criterion_4_Weaknesses__c;
                }
                if(er5.Criterion_4_Weaknesses__c==null){
                er5.Criterion_4_Weaknesses_History__c=string.valueof(System.Now());                    
                }
                if(er5.Criterion_5_Weaknesses__c!=null){
                er5.Criterion_5_Weaknesses_History__c=System.Now()+' '+oldMapofReviews.get(er5.Id).Criterion_5_Weaknesses__c;
                }
                if(er5.Criterion_5_Weaknesses__c==null){
                er5.Criterion_5_Weaknesses_History__c=string.valueof(System.Now());                    
                }
                if(er5.Criterion_6_Weaknesses__c!=null){
                er5.Criterion_6_Weaknesses_History__c=System.Now()+' '+oldMapofReviews.get(er5.Id).Criterion_6_Weaknesses__c;
                }
                if(er5.Criterion_6_Weaknesses__c==null){
                er5.Criterion_6_Weaknesses_History__c=string.valueof(System.Now());                    
                }
                if(er5.Over_all_Comments__c!=null){
                er5.Overall_Narrative_History__c=System.Now()+' '+oldMapofReviews.get(er5.Id).Over_all_Comments__c;
                }
                if(er5.Over_all_Comments__c==null){
                er5.Overall_Narrative_History__c=string.valueof(System.Now());
                }
                er5.FC_Reviewer__Status__c='Requesting Update';
reviewstoupdateforhistory.add(er5);
                
            }
        }
    }
   
    
}