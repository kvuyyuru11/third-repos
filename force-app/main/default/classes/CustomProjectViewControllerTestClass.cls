@isTest
public class CustomProjectViewControllerTestClass {
   public static testmethod void test1(){
       RecordType r1=[Select Id from RecordType where name=:'Research Awards' limit 1];
   User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
    Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
    insert acnt;
        acnt.IsPartner=true;
        update acnt;
    Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                              Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
    insert con;  
      User user = new User(alias = 'test123', email='test66dfklu@myhnmmail234.com',
      emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
      localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
      ContactId = con.Id,
      timezonesidkey='America/New_York', username='test66dfklu@myhnmmail234.com');
        
     insert user;

  

       Opportunity o= new opportunity();

o.Awardee_Institution_Organization__c=acnt.Id;
o.RecordTypeId=r1.id;
o.Full_Project_Title__c='test';
      o.Project_Name_off_layout__c='test';
o.CloseDate=Date.today();
o.Application_Number__c='Ra-1234';
o.StageName='In Progress';
o.Name='test';
       o.PI_Name_User__c='00539000004H9El';
      o.AO_Name_User__c='00570000002dmiQ';
insert o;
ProjectViews__c setting = new ProjectViews__c();
      setting.Name='Research';
setting.OppType__c = 'My Research Awards Projects';
setting.ViewLocation__c=2;
insert setting;
PageReference pageRef = ApexPages.currentPage();
        Test.setCurrentPage(pageRef);
      ApexPAges.StandardController sc = new ApexPages.StandardController(o);
      CustomProjectViewController cpvc= new CustomProjectViewController(sc);
cpvc.recCheck='research';
     
cpvc.listAll();
   


       
    }
    
       public static testmethod void test2(){
       RecordType r1=[Select Id from RecordType where name=:'Research Awards' limit 1];
   User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
    Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
    insert acnt;
        acnt.IsPartner=true;
        update acnt;
    Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                              Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
    insert con;  
      User user = new User(alias = 'test123', email='test66dfklu@my567hnmmail234.com',
      emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
      localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
      ContactId = con.Id,
      timezonesidkey='America/New_York', username='test66dfklu@my567hnmmail234.com');
        
     insert user;

  

       Opportunity o= new opportunity();

o.Awardee_Institution_Organization__c=acnt.Id;
o.RecordTypeId=r1.id;
o.Full_Project_Title__c='test';
      o.Project_Name_off_layout__c='test';
o.CloseDate=Date.today();
o.Application_Number__c='Ra-1234';
o.StageName='In Progress';
o.Name='test';
       o.PI_Name_User__c='00539000004H9El';
      o.AO_Name_User__c='00570000002dmiQ';
insert o;
ProjectViews__c setting = new ProjectViews__c();
      setting.Name='Engage';
setting.OppType__c = 'My Engagement Awards Projects';
setting.ViewLocation__c=1;
insert setting;
PageReference pageRef = ApexPages.currentPage();
        Test.setCurrentPage(pageRef);
      ApexPAges.StandardController sc = new ApexPages.StandardController(o);
      CustomProjectViewController cpvc= new CustomProjectViewController(sc);
      
      cpvc.recCheck='engage';
cpvc.listAll();
   
 

       
    }
    
   public static testmethod void test3(){
       RecordType r1=[Select Id from RecordType where name=:'Pipeline to Proposal' limit 1];
   User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
    Account acnt = new Account(name ='Test Account p2p',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
    insert acnt;
        acnt.IsPartner=true;
        update acnt;
    Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                              Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
    insert con;  
      User user = new User(alias = 'test345', email='test66dfklu@myhnmmail2345.com',
      emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
      localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
      ContactId = con.Id,
      timezonesidkey='America/New_York', username='test66dfklu@myhnmmail2345.com');
        
     insert user;

  

       Opportunity o= new opportunity();

o.Awardee_Institution_Organization__c=acnt.Id;
o.RecordTypeId=r1.id;
o.Full_Project_Title__c='test4';
      o.Project_Name_off_layout__c='test4';
o.CloseDate=Date.today();
o.Application_Number__c='Ra-1234';
o.StageName='In Progress';
o.Name='test';
       o.PI_Name_User__c='00539000004H9El';
      o.AO_Name_User__c='00570000002dmiQ';
insert o;
ProjectViews__c setting = new ProjectViews__c();
      setting.Name='P2P';
setting.OppType__c = 'Pipeline to Proposal Awards';
setting.ViewLocation__c=6;
insert setting;
PageReference pageRef = ApexPages.currentPage();
        Test.setCurrentPage(pageRef);
      ApexPAges.StandardController sc = new ApexPages.StandardController(o);
      CustomProjectViewController cpvc= new CustomProjectViewController(sc);
cpvc.recCheck='P2P';
     
cpvc.listAll();
   
 

       
    }
    
       public static testmethod void test4(){
       RecordType r1=[Select Id from RecordType where name=:'Infrastructure' limit 1];
   User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
    Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
    insert acnt;
        acnt.IsPartner=true;
        update acnt;
    Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                              Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
    insert con;  
      User user = new User(alias = 'test1253', email='test66dfklu@my567hnmmail2354.com',
      emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
      localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
      ContactId = con.Id,
      timezonesidkey='America/New_York', username='test66dfklu@my567hnmmail2354.com');
        
     insert user;

  

       Opportunity o= new opportunity();

o.Awardee_Institution_Organization__c=acnt.Id;
o.RecordTypeId=r1.id;
o.Full_Project_Title__c='test';
      o.Project_Name_off_layout__c='test';
o.CloseDate=Date.today();
o.Application_Number__c='Ra-1234';
o.StageName='In Progress';
o.Name='test';
       o.PI_Name_User__c='00539000004H9El';
      o.AO_Name_User__c='00570000002dmiQ';
insert o;
ProjectViews__c setting = new ProjectViews__c();
      setting.Name='Infrastructure';
setting.OppType__c = 'Infrastructure Awards';
setting.ViewLocation__c=5;
insert setting;
PageReference pageRef = ApexPages.currentPage();
        Test.setCurrentPage(pageRef);
      ApexPAges.StandardController sc = new ApexPages.StandardController(o);
      CustomProjectViewController cpvc= new CustomProjectViewController(sc);
      
      cpvc.recCheck='Infrastructure';
cpvc.listAll();
   


       
    }   
}