@isTest
public class RAKeyPersonnelCRTriggerTest{
    private static testMethod void Triggertest1(){
        RecordType rC = [SELECT Id FROM RecordType WHERE sObjectType = 'Contact' AND Name = 'Standard Salesforce Contact'];
       
        RecordType rAt = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Standard Salesforce Account'];
        
        Account acc = new Account();
        acc.RecordTypeId = rAt.Id;
        acc.Name = 'TestA';
        insert acc;
        
        Contact c = new Contact();
        c.FirstName = 'FT';
        c.LastName = 'LT';
        c.AccountId = acc.Id;
        c.RecordTypeId = rC.Id;
        insert c;
        
        Cycle__c cy = new Cycle__c();
        cy.Name = 'Test';
        cy.COI_Due_Date__c = Date.newInstance(2016,11,30);
        insert cy;
        Panel__c p = new Panel__c();
        p.Name = 'Test';
        p.Cycle__c = cy.Id;
        p.Panel_Due_Dtae__c=  Date.newInstance(2019,12,20); 
        insert p;
        
        Research_Application__c ra = new Research_Application__c();
        ra.Name = 'Test';
        ra.Panel__c = p.Id;
        
        insert ra;
        
        Research_Key__c rk = new Research_Key__c();
        rk.Name = 'test';
        rk.Key_Personnel_Contact_Name__c = c.Id;
        rk.Research_Application__c = ra.Id;
        insert rk;
    }
    private static testMethod void TriggerTest2(){
        
        Cycle__c c = new Cycle__c();
        c.Name = 'Test';
        c.COI_Due_Date__c = Date.newInstance(2016,11,30);
        insert c;
        Panel__c p = new Panel__c();
        p.Name = 'Test';
        p.Cycle__c = c.Id;
        p.Panel_Due_Dtae__c=  Date.newInstance(2019,12,20); 
        insert p;
        Research_Application__c ra = new Research_Application__c();
        ra.Name = 'Test';
        ra.Panel__c = p.Id;
        insert ra;
    }
}