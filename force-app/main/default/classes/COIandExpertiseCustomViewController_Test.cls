@isTest
public class COIandExpertiseCustomViewController_Test{
    static testMethod void testlistview1() {
        RecordType r1=[Select Id from RecordType where name=:'RA Applications' limit 1];
        RecordType rt1=[Select Id from RecordType where name=:Label.COI_PFA_Record_Type limit 1];
        RecordType rt=[Select Id from RecordType where name=:Label.COI_General_Record_Type limit 1];
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Id recordTypeId2 = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Standard Salesforce Contact').getRecordTypeId();         
        
        Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt;
        acnt.IsPartner=true;
        update acnt;
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA',PFA__c='No',recordtypeid=recordTypeId2);  
        insert con;  
        User user = new User(alias = 'test123', email='test123ghjt6yhj8@hydemail.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con.Id,
                             timezonesidkey='America/New_York', username='test123ghjt6yhj8@hydemail.com');
        
        insert user;
        
         
            Cycle__c cy=new Cycle__c();
            cy.Name='test Cycle';
            cy.COI_Due_Date__c=date.valueof(System.now());
            insert cy;
            Panel__c p= new panel__c();
            p.name='test panel';
            p.Cycle__c=cy.Id;
            p.MRO__c='00570000002dmiQ';
            p.Panel_Due_Dtae__c=  Date.newInstance(2019,12,20); 
            insert p;
            Opportunity o= new opportunity();
            
            o.Awardee_Institution_Organization__c=acnt.Id;
            o.RecordTypeId=r1.Id;
            o.Full_Project_Title__c='test';
            o.Project_Name_off_layout__c='test';
            o.Panel__c=p.Id;
            o.CloseDate=Date.today();
            o.Application_Number__c='Ra-1234';
            o.StageName='In Progress';
            o.Name='test';
            o.PI_Name_User__c='00570000002dmiQ';
            o.AO_Name_User__c='005700000047Ydq';
            insert o;
            Panel_Assignment__c pa= new Panel_Assignment__c();
            pa.Reviewer_Name__c=con.Id;
            pa.Panel__c=p.Id;
            pa.Panel_Role__c='Reviewer';
            insert pa;
            COI_Expertise__c ce=new COI_Expertise__c(Reviewer_Name__c=con.Id,Related_Project__c=o.Id);
            ce.RecordTypeId=rt.Id;
            insert ce;
         Test.setCurrentPageReference(new PageReference('Page.COIandExpertiseCustomView')); 
            System.currentPageReference().getParameters().put('id', p.Id); 
        system.runAs(user){
              
            COIandExpertiseCustomViewController cc = new COIandExpertiseCustomViewController();
            cc.getCOIandExpertise();
            
            cc.selectedView='';
            
            }

        
    }
    
}