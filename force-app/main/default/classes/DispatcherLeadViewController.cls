public with sharing class DispatcherLeadViewController {
   PageReference pageref; 
    public DispatcherLeadViewController (ApexPages.StandardController controller){
    }
    
    public PageReference redirectDefaultLead(){
       String baseURL = URL.getSalesforceBaseUrl().toExternalForm(); // it will return: < https://cs14.salesforce.com >
       String PageURL = ApexPages.currentPage().getUrl();    
        String wholeURL = baseURL+PageURL;  
        //Get the role of the current user and the lead record type ID.
        User CurrentUser = [Select id, name, UserRoleId from User where Id = :Userinfo.getUserId()];
                
       // Merit_Reviewer_Evaluation__c l = [Select id, recordtypeid from Merit_Reviewer_Evaluation__c where Id = :ApexPages.currentPage().getParameters().get('id')];
     id  recdid =ApexPages.currentPage().getParameters().get('Id'); 
     Id rcdid=ApexPages.currentPage().getParameters().get('RecordType');
      Id i = Id.valueOf(rcdid);
       //string url='/003/e?retURL=%2F003%2Fo&nooverride=1&name_lastcon2='+ strLast + '&name_firstcon2=' + strFirst +'&RecordType='+i;   
        //Only leads without this record type ID selected are viewable for all other role IDs.
     // https://c.cs24.visual.force.com/apex/redirectpage?RecordType=012700000001hFgAAI&retURL=%2Fa1c%2Fo&save_new=1&sfdc.override=1
    if(i=='012700000001hFgAAI'){
        pagereference pg =new pagereference('/apex/MentorEvaluation');
       pg.setRedirect(True);
        return pg;
    }else{
PageReference newPage1 = new PageReference('/a1c/e?retURL=%2Fa1c%2Fo&RecordType='+i+'&ent=01I7000000084Bi&sfdc.override=1');
newPage1.setRedirect(true);
newPage1.getParameters().put('nooverride', '1');
return newpage1;
    }
        return null;
        }
   
    }