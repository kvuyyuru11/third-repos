@isTest
public class COIclassTest{
    static testMethod void testCOIClassTestMethod(){
        
            RecordType rt=[Select Id from RecordType where name=:Label.COI_General_Record_Type limit 1];
       RecordType r1=[Select Id from RecordType where name=:'RA Applications' limit 1];
          User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
    Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
    insert acnt;
        acnt.IsPartner=true;
        update acnt;
    Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                              Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
    insert con;  
      User user = new User(alias = 'test123', email='test123ghj@hydemail.com',
      emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
      localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
      ContactId = con.Id,
      timezonesidkey='America/New_York', username='test123ghj@hydemail.com');
        
     insert user;
        
   
             Cycle__c cy=new Cycle__c();
        cy.Name='test Cycle';
      cy.COI_Due_Date__c = Date.newInstance(2025,12,31);
        insert cy;
        Panel__c p= new panel__c();
        p.name='test panel';
        p.Cycle__c=cy.Id;
        p.MRO__c='00570000002dmiQ';
        p.Panel_Due_Dtae__c=  Date.newInstance(2019,12,20); 
        insert p;
        Opportunity o= new opportunity();

o.Awardee_Institution_Organization__c=acnt.Id;
o.RecordTypeId=r1.Id;
o.Full_Project_Title__c='test';
             o.Project_Name_off_layout__c='test';
o.Panel__c=p.Id;
o.CloseDate=Date.today();
o.Application_Number__c='Ra-1234';
o.StageName='In Progress';
o.Name='test';
               o.PI_Name_User__c='00570000002dmiQ';
          o.AO_Name_User__c='005700000047Ydq';
insert o;
        Panel_Assignment__c pa= new Panel_Assignment__c();
        pa.Reviewer_Name__c=con.Id;
        pa.Panel__c=p.Id;
        pa.Panel_Role__c='Reviewer';
        insert pa;
          system.runas(user){
        COI_Expertise__c ce = new COI_Expertise__c();
        ce.Active__c=true;
        ce.PFA_Level_COI__c='No';
        ce.Conflict_of_Interest__c='Unclear if COI Exists';
        ce.If_unclear_if_COI_exists_please_explain__c=null;
        ce.Expertise_Level__c=null;
             ce.RecordTypeId=rt.Id;
        insert ce;
         Test.startTest();
        Pagereference pageRef;
        pageRef = page.COIPage;
        pageRef.getparameters().put('id',ce.Id);
        Test.setCurrentPage(pageRef);
      
        COIclass cc = new COIclass(new ApexPages.StandardController(ce));
        cc.getItems1();
        cc.getvalues();
        
       
        cc.online = 'Unclear if COI Exists';
        cc.other1 = '';
       
        cc.Expertise='';
   
        cc.submit();
       cc.updateValues();
        cc.validateValues();
        Test.StopTest();
     }    
    }
    static testMethod void testCOIClassTestMethod1(){
        
            RecordType rt=[Select Id from RecordType where name=:Label.COI_General_Record_Type limit 1];
  User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
    Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
    insert acnt;
        acnt.IsPartner=true;
        update acnt;
    Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                              Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
    insert con;  
      User user = new User(alias = 'test123', email='test321ghj@hydemail.com',
      emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
      localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
      ContactId = con.Id,
      timezonesidkey='America/New_York', username='test321ghj@hydemail.com');
        
     insert user;
        
    
             Cycle__c cy=new Cycle__c();
        cy.Name='test Cycle';
      cy.COI_Due_Date__c = Date.newInstance(2025,12,31);
        insert cy;
        Panel__c p= new panel__c();
        p.name='test panel';
        p.Cycle__c=cy.Id;
        p.MRO__c='00570000002dmiQ';
        p.Panel_Due_Dtae__c=  Date.newInstance(2019,12,20); 
        insert p;
         Opportunity o= new opportunity();

o.Awardee_Institution_Organization__c=acnt.Id;
o.Full_Project_Title__c='test';
             o.Project_Name_off_layout__c='test';
o.Panel__c=p.Id;
o.CloseDate=Date.today();
o.Application_Number__c='Ra-1234';
o.StageName='In Progress';
o.Name='test';
               o.PI_Name_User__c='00570000002dmiQ';
          o.AO_Name_User__c='005700000047Ydq';
insert o;
        Panel_Assignment__c pa= new Panel_Assignment__c();
        pa.Reviewer_Name__c=con.Id;
        pa.Panel__c=p.Id;
        pa.Panel_Role__c='Reviewer';
        insert pa;
       system.runas(user){
         COI_Expertise__c ce = new COI_Expertise__c();
        ce.Active__c=true;
        ce.PFA_Level_COI__c='Yes';
        ce.Reviewer_Rationale__c='test123';
        ce.Conflict_of_Interest__c='Unclear if COI Exists';
        ce.Reviewer_Name__c=con.Id;
             ce.RecordTypeId=rt.Id;
        insert ce;
        Pagereference pageRef;
        pageRef = page.COIPage;
        pageRef.getparameters().put('id',ce.Id);
        Test.setCurrentPage(pageRef);
      
        COIclass cc = new COIclass(new ApexPages.StandardController(ce));
        cc.getItems1();
        cc.getvalues();
      
        Test.StartTest();
      
       
        cc.submit();
       cc.updateValues();
        cc.validateValues();
        Test.StopTest();
        }  
}
       static testMethod void testCOIClassTestMethod2(){
           
            RecordType rt=[Select Id from RecordType where name=:Label.COI_General_Record_Type limit 1];
 User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
    Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
    insert acnt;
           acnt.IsPartner=true;
        update acnt;
    Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                              Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
    insert con;  
      User user = new User(alias = 'test123', email='test3216ghj@hyd1email.com',
      emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
      localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
      ContactId = con.Id,
      timezonesidkey='America/New_York', username='test3216ghj@hyd1email.com');
        
     insert user;
        
      
             Cycle__c cy=new Cycle__c();
        cy.Name='test Cycle';
      cy.COI_Due_Date__c = Date.newInstance(2025,12,31);
        insert cy;
        Panel__c p= new panel__c();
        p.name='test panel';
        p.Cycle__c=cy.Id;
        p.MRO__c='00570000002dmiQ';
        p.Panel_Due_Dtae__c=  Date.newInstance(2019,12,20); 
        insert p;
        Opportunity o= new opportunity();

o.Awardee_Institution_Organization__c=acnt.Id;
o.Full_Project_Title__c='test';
             o.Project_Name_off_layout__c='test';
o.Panel__c=p.Id;
o.CloseDate=Date.today();
o.Application_Number__c='Ra-1234';
o.StageName='In Progress';
o.Name='test';
               o.PI_Name_User__c='00570000002dmiQ';
          o.AO_Name_User__c='005700000047Ydq';
insert o;
        Panel_Assignment__c pa= new Panel_Assignment__c();
        pa.Reviewer_Name__c=con.Id;
        pa.Panel__c=p.Id;
        pa.Panel_Role__c='Reviewer';
        insert pa;
         system.runas(user){
        COI_Expertise__c ce = new COI_Expertise__c();
        ce.Active__c=true;
        ce.PFA_Level_COI__c='No';
        ce.Conflict_of_Interest__c='';
        ce.Expertise_Level__c=null;
             ce.RecordTypeId=rt.Id;
        insert ce;
        Pagereference pageRef;
        pageRef = page.COIPage;
        pageRef.getparameters().put('id',ce.Id);
        Test.setCurrentPage(pageRef);
         
        COIclass cc = new COIclass(new ApexPages.StandardController(ce));
        cc.getItems1();
        cc.getvalues();
        Test.StartTest();
        cc.online = '';
        cc.Expertise = null;
        cc.submit();
        Test.StopTest();
           }
}
  
       static testMethod void testCOIClassTestMethod3(){
           
            RecordType rt=[Select Id from RecordType where name=:Label.COI_General_Record_Type limit 1];
User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
    Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
    insert acnt; 
           acnt.IsPartner=true;
        update acnt;
    Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                              Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
    insert con;  
      User user = new User(alias = 'test123', email='test3216ghhj@hyd2email.com',
      emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
      localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
      ContactId = con.Id,
      timezonesidkey='America/New_York', username='test3216ghhj@hyd2email.com');
        
     insert user;
        
        
             Cycle__c cy=new Cycle__c();
        cy.Name='test Cycle';
        cy.COI_Due_Date__c = Date.newInstance(2025,12,31);
        insert cy;
        Panel__c p= new panel__c();
        p.name='test panel';
        p.Cycle__c=cy.Id;
        p.MRO__c='00570000002dmiQ';
        p.Panel_Due_Dtae__c=  Date.newInstance(2019,12,20); 
        insert p;
        Opportunity o= new opportunity();

o.Awardee_Institution_Organization__c=acnt.Id;
o.Full_Project_Title__c='test';
             o.Project_Name_off_layout__c='test';
o.Panel__c=p.Id;
o.CloseDate=Date.today();
o.Application_Number__c='Ra-1234';
o.StageName='In Progress';
o.Name='test';
               o.PI_Name_User__c='00570000002dmiQ';
          o.AO_Name_User__c='005700000047Ydq';
insert o;
        Panel_Assignment__c pa= new Panel_Assignment__c();
        pa.Reviewer_Name__c=con.Id;
        pa.Panel__c=p.Id;
        pa.Panel_Role__c='Reviewer';
        insert pa;
              system.runas(user){
        COI_Expertise__c ce = new COI_Expertise__c();
        ce.Active__c=true;
        ce.PFA_Level_COI__c='No';
        ce.Conflict_of_Interest__c='';
        ce.Expertise_Level__c='High';
        ce.Submitted__c=true;
             ce.RecordTypeId=rt.Id;
        insert ce;
        Pagereference pageRef;
        pageRef = page.COIPage;
        pageRef.getparameters().put('id',ce.Id);
        Test.setCurrentPage(pageRef);
        
        COIclass cc = new COIclass(new ApexPages.StandardController(ce));
        cc.getItems1();
        cc.getvalues();
        Test.StartTest();
        cc.online = '';
        cc.Expertise = 'High';
        cc.submit();
        Test.StopTest();
           }
} 
  
    static testMethod void testCOIClassTestMethod4(){
        
            RecordType rt=[Select Id from RecordType where name=:Label.COI_General_Record_Type limit 1];
User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
    Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
    insert acnt; 
        acnt.IsPartner=true;
        update acnt;
    Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                              Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
    insert con;  
      User user = new User(alias = 'test123', email='tes3216ghhj@hyd3email.com',
      emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
      localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
      ContactId = con.Id,
      timezonesidkey='America/New_York', username='tes3216ghhj@hyd3email.com');
        
     insert user;
        
       
             Cycle__c cy=new Cycle__c();
        cy.Name='test Cycle';
        cy.COI_Due_Date__c = Date.newInstance(2025,12,31);
        insert cy;
        Panel__c p= new panel__c();
        p.name='test panel';
        p.Cycle__c=cy.Id;
        p.MRO__c='00570000002dmiQ';
        p.Panel_Due_Dtae__c=  Date.newInstance(2019,12,20); 
        insert p;
         Opportunity o= new opportunity();

o.Awardee_Institution_Organization__c=acnt.Id;
o.Full_Project_Title__c='test';
             o.Project_Name_off_layout__c='test';
o.Panel__c=p.Id;
o.CloseDate=Date.today();
o.Application_Number__c='Ra-1234';
o.StageName='In Progress';
o.Name='test';
               o.PI_Name_User__c='00570000002dmiQ';
          o.AO_Name_User__c='005700000047Ydq';
insert o;
        Panel_Assignment__c pa= new Panel_Assignment__c();
        pa.Reviewer_Name__c=con.Id;
        pa.Panel__c=p.Id;
        pa.Panel_Role__c='Reviewer';
        insert pa;
        system.runas(user){
       COI_Expertise__c ce = new COI_Expertise__c();
        ce.Active__c=true;
        ce.PFA_Level_COI__c='No';
        ce.Conflict_of_Interest__c='Financial Benefit';
        ce.Expertise_Level__c=null;
             ce.RecordTypeId=rt.Id;
        insert ce;
        Pagereference pageRef;
        pageRef = page.COIPage;
        pageRef.getparameters().put('id',ce.Id);
        Test.setCurrentPage(pageRef);
        
        COIclass cc = new COIclass(new ApexPages.StandardController(ce));
        cc.getItems1();
        cc.getvalues();
        Test.StartTest();
        cc.online = 'Financial Benefit';
        cc.Expertise = null;
        cc.submit();
        Test.StopTest();
        } 
}
        static testMethod void testCOIClassTestMethod5(){
            
            RecordType rt=[Select Id from RecordType where name=:Label.COI_General_Record_Type limit 1];
User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
    Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
    insert acnt; 
            acnt.IsPartner=true;
        update acnt;
    Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                              Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
    insert con;  
      User user = new User(alias = 'test123', email='tes8216ghhj@hyd4email.com',
      emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
      localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
      ContactId = con.Id,
      timezonesidkey='America/New_York', username='tes8216ghhj@hyd4email.com');
        
     insert user;
        
        
             Cycle__c cy=new Cycle__c();
        cy.Name='test Cycle';
        cy.COI_Due_Date__c = Date.newInstance(2025,12,31);
        insert cy;
        Panel__c p= new panel__c();
        p.name='test panel';
        p.Cycle__c=cy.Id;
        p.MRO__c='00570000002dmiQ';
        p.Panel_Due_Dtae__c=  Date.newInstance(2019,12,20); 
        insert p;
        Opportunity o= new opportunity();

o.Awardee_Institution_Organization__c=acnt.Id;
o.Full_Project_Title__c='test';
             o.Project_Name_off_layout__c='test';
o.Panel__c=p.Id;
o.CloseDate=Date.today();
o.Application_Number__c='Ra-1234';
o.StageName='In Progress';
o.Name='test';
               o.PI_Name_User__c='00570000002dmiQ';
          o.AO_Name_User__c='005700000047Ydq';
insert o;
        Panel_Assignment__c pa= new Panel_Assignment__c();
        pa.Reviewer_Name__c=con.Id;
        pa.Panel__c=p.Id;
        pa.Panel_Role__c='Reviewer';
        insert pa;
              system.runas(user){
        COI_Expertise__c ce = new COI_Expertise__c();
        ce.Active__c=true;
        ce.PFA_Level_COI__c='No';
        ce.Conflict_of_Interest__c='Financial Benefit';
        
        ce.Expertise_Level__c='Low';
             ce.RecordTypeId=rt.Id;
        insert ce;
        Pagereference pageRef;
        pageRef = page.COIPage;
        pageRef.getparameters().put('id',ce.Id);
          
        Test.setCurrentPage(pageRef);
        COIclass cc = new COIclass(new ApexPages.StandardController(ce));
        cc.getItems1();
        cc.getvalues();
        Test.StartTest();
          
        cc.online = 'Financial Benefit';
        cc.Expertise = 'Low';
        cc.submit();
           
        Test.StopTest();
            }
} 
     
    
    static testMethod void testCOIClassTestMethod6(){
        
            RecordType rt=[Select Id from RecordType where name=:Label.COI_General_Record_Type limit 1];
User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
    Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
    insert acnt; 
        acnt.IsPartner=true;
        update acnt;
    Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                              Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
    insert con;  
      User user = new User(alias = 'test123', email='tes821ghhj@hyd5email.com',
      emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
      localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
      ContactId = con.Id,
      timezonesidkey='America/New_York', username='tes821ghhj@hyd5email.com');
        
     insert user;
        
 
             Cycle__c cy=new Cycle__c();
        cy.Name='test Cycle';
        cy.COI_Due_Date__c = Date.newInstance(2025,12,31);
        insert cy;
        Panel__c p= new panel__c();
        p.name='test panel';
        p.Cycle__c=cy.Id;
        p.MRO__c='00570000002dmiQ';
        p.Panel_Due_Dtae__c=  Date.newInstance(2019,12,20); 
        insert p;
     Opportunity o= new opportunity();

o.Awardee_Institution_Organization__c=acnt.Id;
o.Full_Project_Title__c='test';
             o.Project_Name_off_layout__c='test';
o.Panel__c=p.Id;
o.CloseDate=Date.today();
o.Application_Number__c='Ra-1234';
o.StageName='In Progress';
o.Name='test';
               o.PI_Name_User__c='00570000002dmiQ';
          o.AO_Name_User__c='005700000047Ydq';
insert o;
        Panel_Assignment__c pa= new Panel_Assignment__c();
        pa.Reviewer_Name__c=con.Id;
        pa.Panel__c=p.Id;
        pa.Panel_Role__c='Reviewer';
        insert pa;
         system.runas(user){
        COI_Expertise__c ce = new COI_Expertise__c();
        ce.Active__c=true;
        ce.PFA_Level_COI__c='Yes';
        ce.Reviewer_Rationale__c='';
             ce.RecordTypeId=rt.Id;
        insert ce;
        Pagereference pageRef;
        pageRef = page.COIPage;
        pageRef.getparameters().put('id',ce.Id);
        Test.setCurrentPage(pageRef);
       
        COIclass cc = new COIclass(new ApexPages.StandardController(ce));
        cc.getItems1();
        cc.getvalues();
       
        Test.StartTest();
      
        Test.StopTest();
        }
}
     
}