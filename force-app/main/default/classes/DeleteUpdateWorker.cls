/*------------------------------------------------------------------------------------------------------------------------------------
Code Written by SADC PCORI Team: Matt Hoffman
Written During Development Phase 2: June/2016 - September/2016
Application ID: AI-0767
Requirement ID: RAr2_C4_056

This class cleans up the old records made from the Updater Worker object, and is called from the 
Process "Update Opportunity Rank" one hour after the record is created.
-------------------------------------------------------------------------------------------------------------------------------------*/
public class DeleteUpdateWorker {

    @InvocableMethod
    
    public static void doDelete()
    {
        List<Updater_Worker__c> deleteList = new List<Updater_Worker__c>();
        Map<Id, Updater_Worker__c> deleteMap = new Map<Id, Updater_Worker__c>();
        
        Map<Id, Updater_Worker__c> workerMap = new Map<Id, Updater_Worker__c>();
        
        List<Updater_Worker__c> workerList = [SELECT id, Opportunity__c, New_Rank__c
                                            FROM Updater_Worker__c];
        for (Updater_Worker__c worker : workerList)
        {
            workerMap.put(worker.id, worker);
        }
        
        for (Id workerID : workerMap.keySet())
        {
            Updater_Worker__c updateWorker = workerMap.get(workerID);
                              
            deleteMap.put(updateWorker.id, updateWorker);
        }

        for (Id workerId : deleteMap.keySet())
        {
            deleteList.add(deleteMap.get(workerId));
        }   
        delete deleteList; 
    }

    /*
    public static void doDelete(List<Opportunity> oppList)
    {
        List<Updater_Worker__c> deleteList = new List<Updater_Worker__c>();
        Map<Id, Updater_Worker__c> deleteMap = new Map<Id, Updater_Worker__c>();
        
        Map<Id, Updater_Worker__c> workerMap = new Map<Id, Updater_Worker__c>();
        
        List<Updater_Worker__c> workerList = [SELECT id, Opportunity__c, New_Rank__c
                                            FROM Updater_Worker__c];
        for (Updater_Worker__c worker : workerList)
        {
            workerMap.put(worker.id, worker);
        }
        
        for (Opportunity theOpp : oppList)
        {
            for (Id workerID : workerMap.keySet())
            {
                Updater_Worker__c updateWorker = workerMap.get(workerID);
                
                if (updateWorker.Opportunity__c == theOpp.id)
                {                      
                    deleteMap.put(updateWorker.id, updateWorker);
                }
            }
        }

        for (Id workerId : deleteMap.keySet())
        {
            deleteList.add(deleteMap.get(workerId));
        }   
        delete deleteList;    
    }
*/
    
}