/*
* Class : RRCAWebformController
* Author : Kalyan Vuyyuru (PCORI)
* Version History : 1.0
* Creation : 4/25/2017
* Last Modified By: Kalyan Vuyyuru (PCORI)
* Last Modified Date: 5/24/2017
* Modification Description:Just aligned the text and ensured coverage is proper and added the custom labels
* Description : This Class is to ensure that a RRCA opportunity is created whenever people access the RRCA webproposal form. Also
it throws appropriate error messages if trying to refill or any data is missing.
*/


public class RRCAWebformController {
    
    public string FirstName{get;set;}
    public string LastName{get;set;}
    public string Org{get;set;}
    public string Email{get;set;}
    public string Phone{get;set;}
    public string Street{get;set;}
    public string City{get;set;}
    public string State{get;set;}
    public string ZipCode{get;set;}
    public string Country{get;set;}
    public string ProjectName{get;set;}
    public Id recordTypeId{get;set;}
    public boolean Submitted{get;set;}
    public Id AccntId{get;set;}
    public RRCAWebformController(){
        Submitted=false;
    }
    
    public void Submit()
    {
        Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RRCA').getRecordTypeId();
        Id rcrdTypeId1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RRCA Locked').getRecordTypeId();
        Id rcrdTypeId2 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RRCA Unlocked').getRecordTypeId();
        
        list<Opportunity> oprtlist=[select Project_Lead_First_Nam__c,Project_Lead_Last_Name__c,Project_Lead_Email_Address__c from Opportunity where Project_Lead_Email_Address__c=:Email AND (RecordtypeId=:recordTypeId OR RecordtypeId=:rcrdTypeId1 OR RecordtypeId=:rcrdTypeId2)];
        list<Account> ActId=[select id from Account where name=:Org limit 1];
        if(ActId != null && ActId.size() > 0)
            AccntId=ActId[0].Id;
        else if(Org!='' && Org!=null) {
            account a = new account();
            a.Name=Org;
            a.Community__c='TBD';
            insert a;
            a.IsPartner=true;
            update a;
            AccntId =a.Id;
        } 
        
        boolean error = false;
        if(oprtlist!=null && oprtlist.size()>0){
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,Label.RRCA_Application_Already_Exists)); 
        }
        else{
            if (FirstName == null || FirstName == '')
            {
                error = true;
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,Label.RRCA_First_Name));
            }
            if (LastName == null || LastName == '')
            {
                error = true;
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,Label.RRCA_Last_Name));
            }
            if (Org == null || Org == '')
            {
                error = true;
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,Label.RRCA_Organization));
            }
            if (Email == null || Email == '')
            {
                error = true;
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,Label.RRCA_Email));
            }
            
            if (Phone == null || Phone == '')
            {
                error = true;
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,Label.RRCA_Phone));
            }
            if (Street == null || Street == '')
            {
                error = true;
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,Label.RRCA_Street));
            }
            if (City == null || City == '')
            {
                error = true;
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,Label.RRCA_City));
            }
            if (State == null || State == '')
            {
                error = true;
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,Label.RRCA_State));
            }
            if (ZipCode == null || ZipCode == '')
            {
                error = true;
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,Label.RRCA_Zip));
            }
            if (Country == null || Country == '')
            {
                error = true;
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,Label.RRCA_Country));
            }
            if (ProjectName == null || ProjectName == '')
            {
                error = true;
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,Label.RRCA_Project));
            }
            
            if (error==false) 
            {
                
                
                
                Opportunity o = new Opportunity();
                o.Project_Lead_First_Nam__c = FirstName;
                o.Project_Lead_Last_Name__c = LastName;
                o.AccountId=AccntId;
                o.Project_Lead_Email_Address__c = Email;
                o.Project_Lead_Telephone__c = Phone;
                o.RecordTypeId = recordTypeId;
                o.OwnerId = Label.RRCA_OwnerId;
                o.Street_for_RRCA__C = Street;
                o.city_for_RRCA__C = City;
                o.State_for_RRCA__C = State;
                o.Zip_for_RRCA__C = ZipCode;
                o.Country_for_RRCA__C = Country;
                o.Name = ProjectName;
                o.StageName=Label.RRCA_StageName;
                o.CloseDate=date.valueOf(Label.RRCA_Close_Date);
                insert(o);
                submitted=true;
            }
        } 
    }
    
}