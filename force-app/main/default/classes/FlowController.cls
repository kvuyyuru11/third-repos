public class FlowController{
    public String grId {get;set;}
    public String grId2 {get;set;}
    public String erRecId {get;set;}
    public String ferRecId {get;set;}
    public String ferId {get;set;}
    public String erId {get;set;}
    public Boolean test{get;set;}
    public String webLink {get;set;}
    public String backLink{get; set;}
    public List<Engagement_Report__c> erlst;
    public List<RecordType> reclst;
    public string nextpage{get;set;}
    public string back {get; set;}
    private FGM_Base__Grantee_Report__c gr;
    public FlowController(){
        grId = ApexPages.currentPage().getParameters().get('Id');
        grId2 = ApexPages.currentPage().getParameters().get('page');
        getcheckEA();
        if(test == null){
            test = true;
        }
        if(grId2 == '5')
        {
            Integer num = integer.valueof(grId2)-2;
            Integer fornum = integer.valueof(grId2)-1;
            nextpage = string.valueof(fornum);
            back = string.valueof(num);
        }
        else if(grId2 == '4')
        {
           Integer num = integer.valueof(grId2)-1;
           Integer fornum = integer.valueof(grId2);
           back = string.valueof(num);
           nextpage = string.valueof(fornum);
        }
        reclst = [SELECT Id, DeveloperName FROM RecordType WHERE sObjectType = 'Engagement_Report__c'];

        for(RecordType rec :reclst){
            if(rec.DeveloperName == 'Engagement_Report'){
                erRecId = rec.Id;
            }
            if(rec.DeveloperName == 'Final_Engagement_Report'){
                ferRecId = rec.Id;
            }
        }
        if(grId != null){
            gr = [SELECT RecordType.Name FROM FGM_Base__Grantee_Report__c WHERE Id = :grId];
            if(gr.RecordType.Name == 'Interim Progress Report - RA' || gr.RecordType.Name == 'Interim Progress Report - EA'){
                webLink = '/apex/Interim_Pagination_RA?id=' + grId+'&page='+nextpage;
                backLink = '/apex/Interim_Pagination_RA?id=' + grId+'&page='+back;
            }
            else if(gr.RecordType.Name == 'Final Progress Report - RA' || gr.RecordType.Name == 'Final Progress Report - EA'){
                webLink = '/apex/Final_Progress_Pagination_RA?id=' + grId+'&page='+nextpage;
                backLink = '/apex/Final_Progress_Pagination_RA?id=' + grId+'&page='+back;
            }

            try{
                erlst = [SELECT Id, RecordTypeId FROM Engagement_Report__c Where Grantee_Report__c = :grId];
                for(Engagement_Report__c er :erlst){
                    if(er.RecordTypeId == erRecId){
                        erId = er.Id;
                    }
                    if(er.RecordTypeId == ferRecId){
                        ferId = er.Id;
                    }
                }
            }
            catch(Exception e){
                System.debug('Exception is ---->' + e);
            }
        }
    }
    
    public void getcheckEA(){
        try{
            List<Engagement_Report__c> engrept = [SELECT Id, RecordType.Name, Q8_Not_Worked_Strategies__c, Challenges_with_patient_stkholdr__c FROM Engagement_Report__c Where Grantee_Report__c = :grId];
            for(Engagement_Report__c er :engrept){
                if(er.RecordType.Name == 'Engagement Report'){
                    if(er.Challenges_with_patient_stkholdr__c == ''){
                        test = true;
                    }
                    else if(er.Challenges_with_patient_stkholdr__c == null){
                        test = true;
                    }
                    else {
                        test = false;
                    }
                }
                else {
                    if(er.Q8_Not_Worked_Strategies__c == ''){
                        test = true;
                    }
                    else if(er.Q8_Not_Worked_Strategies__c == null){
                        test = true;
                    }
                    else {
                        test = false;
                    }
                }
            }
        }
        catch(Exception e){
            System.debug('Exception===>' + e);
        }        
    }
    public PageReference ReturnBack(){
        if(gr.RecordType.Name == 'Final Progress Report - RA' || gr.RecordType.Name == 'Final Progress Report - EA'){
            return new PageReference('/apex/FinalProgressReportOutputRA?Id=' + grId);
        }
        return new PageReference('/apex/InterimProgressReportOutputRA?Id=' + grId);
    }
    
    //Written by Vijay for Breadcrumb pagination
    public string BreadcrumbMethod(integer pgNum)
    {
        String pref;        
        if (gr.RecordType.Name == 'Interim Progress Report - RA') 
        { 
            if (pgNum != null && pgNum >= 0 && grId != null)
            {
                if (pgNum == 0)
                    pref = '/apex/Interim_Pagination_RA?Id=' + grId;
                else                
                    pref = '/apex/Interim_Pagination_RA?Id=' + grId + '&page=' + pgNum;
            }
        }
        else if (gr.RecordType.Name == 'Final Progress Report - RA')
        {
            if (pgNum != null && pgNum >= 0 && grId != null)
            {            
                if (pgNum == 0)
                    pref = '/apex/Final_Progress_Pagination_RA?Id=' + grId;
                else
                    pref = '/apex/Final_Progress_Pagination_RA?Id=' + grId + '&page=' + pgNum;
            }
        }
		
        return pref;
    }
    public pagereference BreadcrumbPage1()
    {
        String pref;
        pref = BreadcrumbMethod(0);
        PageReference pr = new PageReference(pref);
        pr.setRedirect(true);
        return pr;         
    }
    public pagereference BreadcrumbPage2()
    {
        String pref;        
        pref = BreadcrumbMethod(1);
        PageReference pr = new PageReference(pref);
        pr.setRedirect(true);
        return pr;        
    }
    public pagereference BreadcrumbPage3()
    {
        String pref;     
		pref = BreadcrumbMethod(2);
        PageReference pr = new PageReference(pref);
        pr.setRedirect(true);
        return pr; 
    }
    public pagereference BreadcrumbPage4()
    {
        String pref;                
        pref = BreadcrumbMethod(3);
        PageReference pr = new PageReference(pref);
        pr.setRedirect(true);
        return pr;
    }
    public pagereference BreadcrumbPage5()
    {        
        String pref;
        if (gr.RecordType.Name == 'Interim Progress Report - RA') 
        {
            if (grId != null)
            {            
        		pref = '/apex/EngReport_Flow?Id=' + grId + '&page=4';
            }
        }
        else if (gr.RecordType.Name == 'Final Progress Report - RA')
        {
            if (grId != null)
            {             
            	pref = '/apex/FEngReport_Flow?Id=' + grId +'&page=5';
            }
        }
        PageReference pr = new PageReference(pref);
        pr.setRedirect(true);
        return pr;        
    }
    public pagereference BreadcrumbPage6()
    {
        String pref;                
        pref = BreadcrumbMethod(4);
        PageReference pr = new PageReference(pref);
        pr.setRedirect(true);
        return pr;
    }
    public pagereference BreadcrumbPage7()
    {
        String pref;                
        pref = BreadcrumbMethod(5);
        PageReference pr = new PageReference(pref);
        pr.setRedirect(true);
        return pr;
    }
    public pagereference BreadcrumbPage8()
    {
        String pref;                
        pref = BreadcrumbMethod(6);
        PageReference pr = new PageReference(pref);
        pr.setRedirect(true);
        return pr;
    }    
}