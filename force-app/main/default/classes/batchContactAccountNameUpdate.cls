global class batchContactAccountNameUpdate implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        String query = 'SELECT Id, Sync_Source__c,AccountId  FROM Contact WHERE Sync_Source__c=\'Constant Contact\' AND AccountId=null';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Contact> scope) {
         for(Contact c : scope)
             
         {
             c.AccountId = Label.ConstantContactsAccountId;            
}
         update scope;
    }   
    
    global void finish(Database.BatchableContext BC) {
    }
    
  
    
}