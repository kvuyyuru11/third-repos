/*This class belongs to consolidatedTrigger on Opportunity object. This class will update the email fields in the task object when the Opportunity record is updated.*/
public with Sharing class UpdateInTaskHelper {

    public UpdateInTaskHelper() {
        
    }
    public void updateEmailIdsinTask(List<Opportunity> oplist) {   /* This method will update all email fields in the task when the opportunity record is edited*/
          
           Set<id> idList = new Set<Id>();
        for(Opportunity p : oplist){
           idList.add(p.id);
        }// for loop
                       
                Map<Id, Opportunity> oppmap = new Map<Id, Opportunity>();
                Map<Id, Task> tskmap = new Map<Id, Task>();
                List<Task> tskList = new List<Task>();             
                                                          
                List<Task> tskSOQLList =[SELECT id,Email_Program_Associate__c, Email_Program_Officer__c, Email_Contract_Administrator__c, Email_Contract_Coordinator__c, WhatId FROM Task WHERE WhatId IN : idList LIMIt 50000];

                List<Opportunity> oppSOQLList = [SELECT Id, Name, PI_Name_User__c,PI_Name_User__r.email, PI_Project_Lead_2_Name_New__r.Email, PI_Project_Lead_Designee_1_New__r.Email, PI_Project_Lead_Designee_2_Name_New__r.Email,Program_Associate__r.Email,Program_Officer__r.Email,AO_Name_User__r.Email,Contract_Administrator__r.Email,Contract_Coordinator__r.Email FROM Opportunity where Id IN : idList Limit 50000]; 

               
                for(Opportunity opp: oppSOQLList)
                {
                   oppmap.put(opp.id, opp);
                }
                
                for(Task tsk: tskSOQLList)
                {
                   tskmap.put(tsk.id, tsk);
                }
                try{
                   for(Task tskl : tskSOQLList)
                    {
                                                
                        tskl.Email_Program_Associate__c = oppmap.get(tskl.WhatId).Program_Associate__r.Email;
                        tskl.Email_Program_Officer__c = oppmap.get(tskl.WhatId).Program_Officer__r.Email;
                        tskl.Email_Contract_Administrator__c = oppmap.get(tskl.WhatId).Contract_Administrator__r.Email;
                        tskl.Email_Contract_Coordinator__c = oppmap.get(tskl.WhatId).Contract_Coordinator__r.Email;
                        tskList.add(tskl);                                                      
                   }// for loop
                  
                   update tskList;
               }catch(DmlException e) {
                         System.debug('The following exception has occurred: ' + e.getMessage());       
               }Catch(Exception e)  {
                        System.debug('The following exception has occurred: ' + e.getMessage());
               }// try catch block     
      
          }//end updateEmailIdsinTask Method
    
}//end UpdateInTaskHelper