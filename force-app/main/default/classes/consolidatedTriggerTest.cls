@isTest

public class consolidatedTriggerTest
{
    static testMethod void Test() 
    {
    
        RecordType r1=[Select Id from RecordType where name=:'Research Awards' limit 1];
        RecordType r2=[Select Id from RecordType where name=:'Consultant Cost' limit 1];
        Account a = new Account();
        a.Name = 'Acme1';
        insert a;
        a.IsPartner=true;
        update a;
        
        Id profileId = [select id from profile where name='System Administrator'].id;  
        
        User user = new User(alias = 'test123', email='test123fvb@noemail.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,   
                             
                             timezonesidkey='America/New_York', username='testerfvb1@noemail.com');
        insert user;
        
        Id profileId1 = [select id from profile where name='PCORI Community Partner'].id;  
        Contact con1 = new Contact(LastName ='testCon1',AccountId = a.Id);  
        insert con1;
        User user1 = new User(alias = 'test1234', email='test1234fvb@noemail.com',
                              emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                              localesidkey='en_US', profileid = profileId1 , country='United States',IsActive = true,   
                              contactId = con1.Id,
                              timezonesidkey='America/New_York', username='testerfvb123@noemail.com');
        insert user1;
        
        Id profileId2 = [select id from profile where name='PCORI Community Partner'].id;  
        Contact con2 = new Contact(LastName ='testCon1',AccountId = a.Id);  
        insert con2;
        User user2 = new User(alias = 'test145', email='test12345fvb@noemail.com',
                              emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                              localesidkey='en_US', profileid = profileId2 , country='United States',IsActive = true,   
                              contactId = con2.Id,
                              timezonesidkey='America/New_York', username='testerfvb12@noemail.com');
        insert user2;
        
        Id profileId3 = [select id from profile where name='PCORI Community Partner'].id;  
        Contact con3 = new Contact(LastName ='testCon2',AccountId = a.Id);  
        insert con3;
        
        User user3 = new User(alias = 'test52', email='test135fvb@noemail.com',
                              emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                              localesidkey='en_US', profileid = profileId3 , country='United States',IsActive = true,   
                              contactId = con3.Id,
                              timezonesidkey='America/New_York', username='testerfvb82@noemail.com');
        insert user3;
        
        
        Cycle__c c = new Cycle__c();
        c.Name = 'testcycle';
        c.COI_Due_Date__c = date.valueof(System.now());
        insert c;
        
        Panel__c p = new Panel__c();
        p.name= 'testpanel';
        p.Cycle__c= c.id ;
        p.MRO__c = user2.id;
        p.Panel_Due_Dtae__c=  Date.newInstance(2019,12,20); 
        insert p;

        campaign cpg= new campaign();
        cpg.Name='test';
        cpg.IsActive=true;
         cpg.RecordTypeId='01239000000Hr4i';
        insert cpg;
            
        opportunity o=new opportunity();
        
        o.Awardee_Institution_Organization__c=a.Id;
        o.RecordTypeId=r1.Id;
        o.Full_Project_Title__c='test';
        o.Project_Name_off_layout__c='test';
        //o.Panel__c=p.Id;
        o.CloseDate=Date.today();
        o.Application_Number__c='Ra-1234';
        o.StageName='Executed';
        o.Name='test';
        o.CampaignId=cpg.Id;
        o.PI_Name_User__c=user.id;
        o.AO_Name_User__c=user1.id;
        o.Project_Start_Date__c=system.Today();
        
        insert o;
        
          
        opportunity opp1=new opportunity();
        
        opp1.Awardee_Institution_Organization__c=a.Id;
        opp1.RecordTypeId=r1.Id;
        opp1.Full_Project_Title__c='test';
        opp1.Project_Name_off_layout__c='test';
        //o.Panel__c=p.Id;
        opp1.CloseDate=Date.today();
        opp1.Application_Number__c='Ra-1234';
        opp1.StageName='Executed';
        opp1.Name='test';
        opp1.CampaignId=cpg.Id;
        opp1.PI_Name_User__c=user.id;
        opp1.AO_Name_User__c=user1.id;
        opp1.Project_Start_Date__c=system.Today().adddays(-600);
        
        insert opp1;
        
        opportunityteammember optm=new opportunityteammember();
        optm.userid=user.id;
        optm.opportunityid=o.id;
        optm.TeamMemberRole='Principal Investigator';
        optm.OpportunityAccessLevel='Edit';
        insert optm;
    
        
        
        opportunityteammember optm1=new opportunityteammember();
        optm1.userid=user1.id;
        optm1.opportunityid=o.id;
        optm1.TeamMemberRole='Principal Investigator';
        optm1.OpportunityAccessLevel='Edit';
        insert optm1;
            


        Test.startTest();

        // RecordType r3= Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('RA Supplemental Funding').getRecordTypeId();
        RecordType r3=[Select Id from RecordType where name=:'RA Supplemental Funding' limit 1];
        opportunity o1=new opportunity();
        o1.Parent_Contract__c = o.id; 
        o1.CampaignId=cpg.Id;
        o1.Awardee_Institution_Organization__c=a.Id;
        o1.AccountId=a.Id;
        o1.RecordTypeId=r3.Id;
        o1.Full_Project_Title__c='test';
        o1.Project_Name_off_layout__c='test';
            o1.Contract_Number__c='12345';
        //o.Panel__c=p.Id;
        o1.CloseDate=Date.today();
        o1.Application_Number__c='Ra-1234';
        o1.StageName='Executed';
        o1.Name='test';
        o1.PI_Name_User__c=user.id;
        o1.AO_Name_User__c=user1.id;
        o1.Project_Start_Date__c=system.Today();
o1.Budget_Approve_for_Contract__c=true;
o1.Milestone_Approve_for_Contract__c=true;
o1.CER_Category__c='Studies of Interventions for Caregivers';
o1.Research_Plan_Approve_for_Contract__c=true; 
        o1.Project_End_Date__c=Date.newInstance(2028,12,31);
        o1.Research_Period_End_Date__c=Date.newInstance(2028,12,31);
        o1.Program_Officer__c='00570000002dmiQ';
        o1.Program_Associate__c='00539000004H9El';
        o1.Contract_Administrator__c='00539000005rMaC';
        o1.Financial_Contact__c='00539000004k06h';
        o1.Contracted_Budget__c=12345.20;
        o1.Contract_Execution_Date__c=system.Today();
        o1.Primary_Completion_Date__c=Date.newInstance(2028,12,31);
        o1.FGM_Base__Award_Date__c=system.Today();
        o1.Original_Board_Approved_Budget__c=12345.20;
        o1.Program__c='HDDR';
        insert o1;
        
        Opportunity o1Wdata = [SELECT Id, Application_Number__c, Parent_Contract__c FROM Opportunity WHERE Id = : o1.id LIMIT 1];
        System.assert(o1Wdata != null);
        System.assert(o1Wdata.Application_Number__c.contains('SF'));
        System.assert(o1Wdata.Parent_Contract__c != null);
        
        update o1;

        Test.stopTest();
    }
}