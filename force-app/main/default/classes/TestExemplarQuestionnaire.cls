@isTest
public class TestExemplarQuestionnaire {
    static testMethod void TestExemplarQuestionnaire()
    {
       opportunity objopp=Opportunityrec();
        system.debug('==objopp.id'+objopp.id);
        Science_Qualifications_Results__c obj_SQR=new  Science_Qualifications_Results__c();
      
       obj_SQR.Contract_Id__c=objopp.id;
        insert obj_SQR;
         
              ApexPages.StandardController controller =new ApexPages.StandardController(obj_SQR);
          PageReference pageRef =Page.ExemplarQuestionnaire;
       
        pageRef.getParameters().put('Pid', String.valueOf(objopp.Id));
        pageRef.getParameters().put('Referer', '/'+String.valueOf(objopp.id));
          pageRef.getParameters().put('id', obj_SQR.id);
        Test.setCurrentPage(pageRef);
             ExemplarQuestionnaire objExemplarQuestionnaire=new ExemplarQuestionnaire(controller);
             objExemplarQuestionnaire.projid=objopp.id;
        objExemplarQuestionnaire.objopportunity=objopp;
        objExemplarQuestionnaire.objExemplar=obj_SQR;
      
        objExemplarQuestionnaire.Save();
        objExemplarQuestionnaire.cancel();
        objExemplarQuestionnaire.ExempEditRedirect();
        
        
    }
    
      static testMethod Opportunity Opportunityrec(){
           Account a = new Account();
    a.Name = 'Acme';
    insert a;
    contact c= new Contact();
    c.LastName='opttest234h7';
        c.Email='opttest23gh7h6@test.com';
    c.AccountId=a.Id;
    insert c;
           Schema.DescribeSObjectResult oppsche = Schema.SObjectType.Opportunity;
    Map<string, schema.RecordtypeInfo> sObjectMap = oppsche.getRecordtypeInfosByName();    
    Id stroppRecordTypeId = sObjectMap.get('Research Awards').getRecordTypeId();
        opportunity objopp=new opportunity();
    objopp.name='testopp';
    objopp.Contract_Number__c='34567890';
    objopp.StageName='Qualification';
    objopp.closedate=system.today()+5;
    objopp.accountid=a.id;
    objopp.Full_Project_Title__c='testpms';
    objopp.Project_End_Date__c=system.today()+1;
    objopp.recordtypeid=stroppRecordTypeId;
    objopp.Project_Start_Date__c=system.today();
    
        insert objopp;
       
        Return objopp;
        
    }
  

}