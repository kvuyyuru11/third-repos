public with sharing class BudgetPageRouterController {

    //query strings from url
	public String projectAppId {get;set;}
	public String budgetId {get;set;}
	public String retUrl {get;set;}
	
	public BudgetPageRouterController() {
		
	}

	//This is a method that routes to the standard page layout edit view or VFpage edit view based on profile
    public pagereference router(){
        PageReference pageref;

        //Gets the record ids
        if (String.isBlank(this.projectAppId) && String.isBlank(this.budgetId)) {
            this.budgetId = ApexPages.currentPage().getParameters().get('Id');
        }

        //get the return url
        if (!String.isBlank(this.budgetId) && String.isBlank(this.retUrl)) {
            this.retUrl = ApexPages.currentPage().getParameters().get('retURL');
        }

        if (String.isBlank(this.projectAppId)) {
            this.projectAppId = [SELECT Associated_App_Project__c FROM Budget__c WHERE Id = :this.budgetId].Associated_App_Project__c;
        }
        
        //Checks the profile id and routes to the page required
        if (UserInfo.getProfileId().equals(System.Label.PCORI_Community_Partner_Id))
        {
            pageRef = new PageReference('/engagement/BudgetPage');
            pageRef.getParameters().put('appID',this.projectAppId);
            if(String.isNotBlank(this.budgetId)) {
                pageRef.getParameters().put('budgetID',this.budgetId);
            }
        }
        else{
            pageRef = new PageReference('/' + this.budgetId + '/e');
            pageRef.getParameters().put('nooverride','1');
        }
        //set the return url
        if(!String.isBlank(this.retUrl)) {
            pageRef.getParameters().put('retURL',this.retUrl);
        } else {
            pageRef.getParameters().put('retURL','/engagement/fgm_portal__communitydashboard');
        }
        //coming from foundation connect don't show header on next page
        if(String.isBlank(this.budgetId)) {
            pageRef.getParameters().put('showHeader','false');
        } else {
            pageRef.getParameters().put('showHeader','true');
        }
        pageref.setRedirect(True);
        
        return pageRef;
    }
}