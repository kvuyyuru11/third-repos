@isTest
public class COIRouterclass1Test {
    
    static testMethod void testCOIController() {       
        
        List<User> u2 = TestDataFactory.getpartnerUserTestRecords(1,1);
        RecordType rt = [SELECT Id FROM RecordType WHERE sObjectType = 'COI_Expertise__c' AND Name = 'General Record Type'];
        System.runAs(u2[0]) {            
            COI_Expertise__c a = new COI_Expertise__c();
            a.RecordTypeId = rt.Id;
            insert a;
            
            ApexPages.StandardController ctrl = new ApexPages.StandardController(a);
            PageReference pageRef =Page.COIRouterPage1;
            pageRef.getParameters().put('Id', String.valueOf(a.Id));
            
            Test.setCurrentPage(pageRef);
            COIRouterClass1 COIRC = new COIRouterClass1(ctrl);
            COIRC.router4();
        }        
    }
    static testMethod void testRouterController1() {        
        
        list<User> u2 =TestDataFactory.getpartnerUserTestRecords(1,1);
        RecordType rt = [SELECT Id FROM RecordType WHERE sObjectType = 'COI_Expertise__c' AND Name = 'General Record Type'];
        System.runAs(u2[0]) { 
            test.starttest();     
            COI_Expertise__c a = new COI_Expertise__c();      
            a.RecordTypeId = rt.Id;
            insert a;
            
            ApexPages.StandardController ctrl = new ApexPages.StandardController(a);
            PageReference pageRef =Page.COIRouterPage1;
            pageRef.getParameters().put('Id', String.valueOf(a.Id));
            
            Test.setCurrentPage(pageRef);
            COIRouterClass1 COIRC = new COIRouterClass1(ctrl);
            COIRC.router4();
            test.stoptest();
        }
    }
}