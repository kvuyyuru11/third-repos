@isTest
public class FluxxLOIReviewsCntrlrExtTest {
    static testMethod void testFluxxLOIReviewsCntrlrExt(){
        //Targeted
            Fluxx_LOI__c targetedLoi = new Fluxx_LOI__c(Fluxx_Request_Id__c = 'FC14-1234-56798',
                                                 Cycle__c = 'Cycle 3 2015',
                                                 PFA__c = 'APDTO',
                                                 PFA_Type__c='Targeted',
                                                 Targeted_PFA__c='Opoids',       
                                                 Project_Title__c = 'Assessment of Prevention Diagnostic and Treatment Options');
            insert targetedLoi;
            Fluxx_LOI_Review_Form__c targ_primaryReview = [select id, Cycle__c, PFA__c, PFA_Type__c, Targeted_PFA__c, RecordTypeId from Fluxx_LOI_Review_Form__c 
                                                      where Salesforce_LOI_Number__c = :targetedLoi.id and Reviewer_Label__c = 'Primary'
                                                      limit 1];
            system.debug('******\r\n' + json.serializePretty(targ_primaryReview) + '\r\n******');
            PageReference pageRef = Page.FLuxxLOIsTargetedReviewDetailForm; //instantiate FLuxxLOIsTargetedReviewDetailForm VF page and
            Test.setCurrentPageReference(pageRef);      					//set it in test mode
            ApexPages.StandardController targ_sc = new ApexPages.standardController(targ_primaryReview); //create an instance of the standard controller
            FluxxLOIReviewsCntrlrExt targ_ConExt = new FluxxLOIReviewsCntrlrExt(targ_sc);  //and pass it to the controller extension
            targ_ConExt.editPage();
            ApexPages.CurrentPage().getparameters().put('COI__c', 'No');        
            targ_ConExt.saveEdits();
        	targ_ConExt.editPage();
            ApexPages.CurrentPage().getparameters().put('COI__c', 'Yes');        
            targ_ConExt.cancelEdits();       
        
         //Targeted Cycle 2 2016
            Fluxx_LOI__c c22016TargetedLoi = new Fluxx_LOI__c(Fluxx_Request_Id__c = 'FC14-1234-98765',
                                                 Cycle__c = 'Cycle 2 2016',
                                                 PFA__c = 'APDTO',
                                                 PFA_Type__c='Targeted',
                                                 Targeted_PFA__c='NOACs',       
                                                 Project_Title__c = 'NOACs - Assessment of Prevention Diagnostic and Treatment Options');
            insert c22016TargetedLoi;
            Fluxx_LOI_Review_Form__c c22016TargPrimaryReview = [select id, Cycle__c, PFA__c, PFA_Type__c, Targeted_PFA__c, RecordTypeId from Fluxx_LOI_Review_Form__c 
                                                      where Salesforce_LOI_Number__c = :targetedLoi.id and Reviewer_Label__c = 'Primary'
                                                      limit 1];
            system.debug('******\r\n' + json.serializePretty(c22016TargPrimaryReview) + '\r\n******');
            PageReference c22016PageRef = Page.FLuxxLOIsTargetedReviewDetailForm; //instantiate FLuxxLOIsTargetedReviewDetailCycle22016 VF page and
            Test.setCurrentPageReference(c22016PageRef);   					//set it in test mode
            ApexPages.StandardController c22016Targ_sc = new ApexPages.standardController(c22016TargPrimaryReview); //create an instance of the standard controller
            FluxxLOIReviewsCntrlrExt c22016Targ_ConExt = new FluxxLOIReviewsCntrlrExt(c22016Targ_sc);  //and pass it to the controller extension
            targ_ConExt.editPage();
            ApexPages.CurrentPage().getparameters().put('COI__c', 'No');        
            c22016Targ_ConExt.saveEdits();
        	c22016Targ_ConExt.editPage();
            ApexPages.CurrentPage().getparameters().put('COI__c', 'Yes');        
            c22016Targ_ConExt.cancelEdits(); 
        
        
		//Pragmatic 
 
        	Fluxx_LOI__c pragmaticLoi = new Fluxx_LOI__c(Fluxx_Request_Id__c = 'FC14-1234-999999',
                                             Cycle__c = 'Cycle 3 2015',
                                             PFA__c = 'APDTO',
                                             PFA_Type__c='Pragmatic',
                                             Project_Title__c = 'Pragmatic Assessment of Prevention Diagnostic and Treatment Options');
        	insert pragmaticLoi;
            Fluxx_LOI_Review_Form__c prag_primaryReview = [select id, Cycle__c, PFA__c, PFA_Type__c, RecordTypeId from Fluxx_LOI_Review_Form__c 
                                                      where Salesforce_LOI_Number__c = :pragmaticLoi.id and Reviewer_Label__c = 'Primary'
                                                      limit 1];
            system.debug('******\r\n' + json.serializePretty(prag_primaryReview) + '\r\n******');
            PageReference prag_pageRef = Page.FLuxxLOIsTargetedReviewDetailForm; //instantiate FLuxxLOIsTargetedReviewDetailForm VF page and
            Test.setCurrentPageReference(prag_pageRef);      					//set it in test mode
            ApexPages.StandardController prag_sc = new ApexPages.standardController(prag_primaryReview); //create an instance of the standard controller
            FluxxLOIReviewsCntrlrExt prag_ConExt = new FluxxLOIReviewsCntrlrExt(prag_sc);  //and pass it to the controller extension
            prag_ConExt.editPage();
            ApexPages.CurrentPage().getparameters().put('COI__c', 'No');        
            prag_ConExt.saveEdits();
            prag_ConExt.editPage();        
            ApexPages.CurrentPage().getparameters().put('COI__c', 'Yes');        
            prag_ConExt.cancelEdits();             
    }  //end testFluxxLOIReviewsCntrlrExt method
} //end FluxxLOIReviewsCntrlrExtTest