global class ReviewComplete {
    webservice static void UpdateAPR(List<Id> oId){
        List <Advisory_Panel_Application_Review__c> apar = [SELECT Id, Review_Complete__c from Advisory_Panel_Application_Review__c where Id IN: oID];
        List <Advisory_Panel_Application_Review__c> apar2=new List <Advisory_Panel_Application_Review__c>();
        for(Advisory_Panel_Application_Review__c apar1 : apar){
            apar1.Review_Complete__c = true;
            apar2.add(apar1);
        }
        database.update(apar2);
    }

}