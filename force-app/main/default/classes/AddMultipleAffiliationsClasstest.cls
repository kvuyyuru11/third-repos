// This is a test class to test all cases to cover the AddmultipleAffiliationsClass and the respective helper class
@IsTest
public class AddMultipleAffiliationsClasstest {
    
    
    public static testmethod void amcmethod1() {
        
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Accountty6',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt; 
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
        insert con;  
        User usr = new User(alias = 'test1234', email='test123ty6@noemailtest.com',
                            emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                            localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                            ContactId = con.Id,
                            timezonesidkey='America/New_York', username='test123ty6@noemailtest.com');
        
        insert usr;
        
        System.runAs(usr){
            AddMultipleAffiliationsClass amc=new AddMultipleAffiliationsClass();
            amc.addNewRowToAffList();
            amc.addNewRowToAffList2();
            amc.checkbox01=false;
            amc.checkbox02=false;
            
            amc.checkbox04=true;
            amc.disablemethod();
            amc.disablemethod2();
            amc.SaveMultipleAffiliations();
        }     
    }
    
    public static testmethod void amcmethod2() {
        
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Accountbf1',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt; 
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
        insert con;  
        User usr = new User(alias = 'test1234', email='test1bf1@noemailtest.com',
                            emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                            localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                            ContactId = con.Id,
                            timezonesidkey='America/New_York', username='test1bf1@noemailtest.com');
        
        insert usr;
        
        System.runAs(usr){
            AddMultipleAffiliationsClass amc=new AddMultipleAffiliationsClass();
            amc.addNewRowToAffList();
            amc.addNewRowToAffList2();
            amc.checkbox01=true;
            amc.checkbox02=false;
            
            amc.checkbox04=true;
            amc.disablemethod();
            amc.disablemethod2();
            amc.SaveMultipleAffiliations();            
        }     
    }
    
    public static testmethod void amcmethod3() {
        
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Accounta56',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt; 
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
        insert con;  
        User usr = new User(alias = 'test1234', email='test1a56@noemailtest.com',
                            emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                            localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                            ContactId = con.Id,
                            timezonesidkey='America/New_York', username='test1a56@noemailtest.com');        
        insert usr;        
        System.runAs(usr){
            AddMultipleAffiliationsClass amc=new AddMultipleAffiliationsClass();
            amc.addNewRowToAffList();
            amc.addNewRowToAffList2();
            amc.checkbox01=false;
            amc.checkbox02=true;
            
            amc.checkbox04=true;
            amc.disablemethod();
            amc.disablemethod2();
            amc.SaveMultipleAffiliations();
        }     
    }    
    
    public static testmethod void amcmethod4() {
        
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Accountfd5',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt; 
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
        insert con;  
        User usr = new User(alias = 'test1234', email='test1234fd5@noemailtest.com',
                            emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                            localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                            ContactId = con.Id,
                            timezonesidkey='America/New_York', username='test1234fd5@noemailtest.com');
        
        insert usr;
        
        System.runAs(usr){
            AddMultipleAffiliationsClass amc=new AddMultipleAffiliationsClass();
            amc.addNewRowToAffList();
            amc.addNewRowToAffList2();
            amc.checkbox01=false;
            amc.checkbox02=false;
            
            amc.checkbox04=false;
            amc.disablemethod();
            amc.disablemethod2();
            amc.SaveMultipleAffiliations();
        }     
    }
    
    public static testmethod void amcmethod5() {
        
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Accountkl6',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt; 
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
        insert con;  
        User usr = new User(alias = 'test1234', email='test123kl6@noemailtest.com',
                            emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                            localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                            ContactId = con.Id,
                            timezonesidkey='America/New_York', username='test123kl6@noemailtest.com');
        
        insert usr;
        
        System.runAs(usr){
            list<Affiliation__c> afftoinsert=new list<Affiliation__c>();
            list<Affiliation__c> afftoinsert2=new list<Affiliation__c>();
            Affiliation__c aff= new Affiliation__c();
            aff.Organization_Name__c=acnt.Id;
            aff.Contact__c=con.Id;
            aff.Nature_of_relationship__c='Stock';
            aff.Applies_to__c='Child';
            aff.Financial_and_Business_Association__c=true;
            afftoinsert.add(aff);
            insert afftoinsert;
            Affiliation__c aff2= new Affiliation__c();
            aff2.Organization_Name__c=acnt.Id;
            aff2.Contact__c=con.Id;
            aff2.Nature_of_relationship_Personal__c='Board member';
            aff2.Applies_to__c='Child';
            aff2.Personal_Association__c=true;
            afftoinsert2.add(aff2);
            insert afftoinsert2;
            AddMultipleAffiliationsClass amc=new AddMultipleAffiliationsClass();
            
            
            amc.addNewRowToAffList();
            amc.addNewRowToAffList2();
            amc.checkbox01=false;
            amc.checkbox02=true;
            
            amc.checkbox04=true;
            amc.disablemethod();
            amc.disablemethod2();
            amc.SaveMultipleAffiliations();
            amc.rowToRemove=1;
            amc.rowToRemove2=1;
            amc.removeRowFromAffList();
            
            amc.removeRowFromAffList2();
            
        }     
    }
    
    public static testmethod void amcmethod6() {
        
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Accountbj9',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt; 
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
        insert con;  
        User usr = new User(alias = 'test1234', email='test12bj9@noemailtest.com',
                            emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                            localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                            ContactId = con.Id,
                            timezonesidkey='America/New_York', username='test12bj9@noemailtest.com');
        
        insert usr;        
        
        System.runAs(usr){
            list<Affiliation__c> afftoinsert=new list<Affiliation__c>();
            list<Affiliation__c> afftoinsert2=new list<Affiliation__c>();
            Affiliation__c aff= new Affiliation__c();
            aff.Organization_Name__c=acnt.Id;
            aff.Contact__c=con.Id;
            aff.Nature_of_relationship__c='Other';
            aff.Financial_and_Business_Association_Other__c='';
            aff.Applies_to__c='Other';
            aff.Financial_and_Business_Applied_Other__c='';
            aff.Financial_and_Business_Association__c=true;
            afftoinsert.add(aff);
            insert afftoinsert;
            Affiliation__c aff2= new Affiliation__c();
            aff2.Organization_Name__c=acnt.Id;
            aff2.Contact__c=con.Id;
            aff2.Nature_of_relationship_Personal__c='Other';
            aff2.Personal_Association_Other__c='';
            aff2.Applies_to__c='Other';
            aff2.Personal_Applied_Other__c='';
            aff2.Personal_Association__c=true;
            afftoinsert2.add(aff2);
            insert afftoinsert2;
            AddMultipleAffiliationsClass amc=new AddMultipleAffiliationsClass();
            
            amc.addNewRowToAffList();
            amc.addNewRowToAffList2();
            amc.checkbox01=false;
            amc.checkbox02=false;
            
            amc.checkbox04=true;
            amc.disablemethod();
            amc.disablemethod2();
            amc.SaveMultipleAffiliations();
            amc.rowToRemove=1;
            amc.rowToRemove2=1;
            amc.removeRowFromAffList();
            
            amc.removeRowFromAffList2();            
            
        }
    }
    
    public static testmethod void amcmethod7() {
        
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Accountui8',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt; 
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
        insert con;  
        User usr = new User(alias = 'test1234', email='test1ui8@noemailtest.com',
                            emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                            localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                            ContactId = con.Id,
                            timezonesidkey='America/New_York', username='test1ui8@noemailtest.com');        
        insert usr;     
        
        System.runAs(usr){
            list<Affiliation__c> afftoinsert=new list<Affiliation__c>();
            list<Affiliation__c> afftoinsert2=new list<Affiliation__c>();
            Affiliation__c aff= new Affiliation__c();
            aff.Organization_Name__c=acnt.Id;
            aff.Contact__c=con.Id;
            aff.Nature_of_relationship__c='Other';
            aff.Financial_and_Business_Association_Other__c='';
            aff.Applies_to__c='Other';
            aff.Financial_and_Business_Applied_Other__c='';
            aff.Financial_and_Business_Association__c=true;
            afftoinsert.add(aff);
            insert afftoinsert;
            Affiliation__c aff2= new Affiliation__c();
            aff2.Organization_Name__c=acnt.Id;
            aff2.Contact__c=con.Id;
            aff2.Nature_of_relationship_Personal__c='Other';
            aff2.Personal_Association_Other__c='';
            aff2.Applies_to__c='Other';
            aff2.Personal_Applied_Other__c='';
            aff2.Personal_Association__c=true;
            afftoinsert2.add(aff2);
            insert afftoinsert2;
            AddMultipleAffiliationsClass amc=new AddMultipleAffiliationsClass();  
            
            amc.addNewRowToAffList();
            amc.addNewRowToAffList2();
            amc.checkbox01=true;
            amc.checkbox02=false;            
            amc.checkbox04=true;
            amc.disablemethod();
            amc.disablemethod2();
            amc.SaveMultipleAffiliations();
            amc.rowToRemove=1;
            amc.rowToRemove2=1;
            amc.removeRowFromAffList();            
            amc.removeRowFromAffList2();            
        }
    }    
    public static testmethod void amcmethod8() {
        
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Account1w2',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt; 
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
        insert con;  
        User usr = new User(alias = 'test1234', email='test1234i78@noemailtest.com',
                            emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                            localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                            ContactId = con.Id,
                            timezonesidkey='America/New_York', username='test1234i78@noemailtest.com');        
        insert usr;        
        
        System.runAs(usr){
            list<Affiliation__c> afftoinsert=new list<Affiliation__c>();
            list<Affiliation__c> afftoinsert2=new list<Affiliation__c>();
            Affiliation__c aff= new Affiliation__c();
            aff.Organization_Name__c=acnt.Id;
            aff.Contact__c=con.Id;
            aff.Nature_of_relationship__c='Other';
            aff.Financial_and_Business_Association_Other__c='test1';
            aff.Applies_to__c='Other';
            aff.Financial_and_Business_Applied_Other__c='test2';
            aff.Financial_and_Business_Association__c=true;
            afftoinsert.add(aff);
            insert afftoinsert;
            Affiliation__c aff2= new Affiliation__c();
            aff2.Organization_Name__c=acnt.Id;
            aff2.Contact__c=con.Id;
            aff2.Nature_of_relationship_Personal__c='Other';
            aff2.Personal_Association_Other__c='test3';
            aff2.Applies_to__c='Other';
            aff2.Personal_Applied_Other__c='test4';
            aff2.Personal_Association__c=true;
            afftoinsert2.add(aff2);
            insert afftoinsert2;
            AddMultipleAffiliationsClass amc=new AddMultipleAffiliationsClass();            
            amc.checkbox01=false;
            amc.checkbox02=false;            
            amc.checkbox04=true;            
            amc.SaveMultipleAffiliations();           
        }   
    }    
    public static testmethod void amcmethod9() {
        
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Account1w2',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt; 
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
        insert con;  
        User usr = new User(alias = 'test1234', email='test1234i78@noemailtest.com',
                            emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                            localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                            ContactId = con.Id,
                            timezonesidkey='America/New_York', username='test1234i78@noemailtest.com');        
        insert usr;        
        System.runAs(usr){            
            AddMultipleAffiliationsClass amc=new AddMultipleAffiliationsClass();            
            amc.checkbox01=true;
            amc.checkbox02=true;            
            amc.checkbox04=true;            
            amc.SaveMultipleAffiliations();               
        }
    }   
}