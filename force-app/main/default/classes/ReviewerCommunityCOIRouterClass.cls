//This is the class that shows the programs that are associated to different panels where an approved reviewer is added into.
public class ReviewerCommunityCOIRouterClass {
     Public  List<Panel__c> pnllist {get; set;}
  //constructor
    public ReviewerCommunityCOIRouterClass(ApexPages.StandardController controller){
     
   
    
        set<Id> PnlIDlist= new set<Id>();
         set<Id> pnlidtocheck= new set<Id>();
        Map<Id,Id> coipnlmap= new Map<ID,ID>();
        Id myId3 = userinfo.getUserId();
        User U=[select id,ContactId from User where Id=:myId3];
        //The below soql pulls all the COI and expertise records associated to the approved reviewer
        List<COI_Expertise__c> coilist=[select Id,Panel_R2__c,Reviewer_Name__c from COI_Expertise__c where Reviewer_Name__c=:u.ContactId];
        list<Panel_Assignment__c> pnlassgnmtlist=[select Id, panel__c,Active__c,Reviewer_Name__c from Panel_Assignment__c where Reviewer_Name__c=:u.ContactId AND Active__c=:TRUE];
        for(Panel_Assignment__c p:pnlassgnmtlist){
           pnlidtocheck.add(p.Panel__c);
        }
        for(COI_Expertise__c c1:coilist){
            coipnlmap.put(c1.Id,c1.Panel_R2__c);
                }
        for(COI_Expertise__c c:coilist){
            if(pnlidtocheck.contains(coipnlmap.get(c.Id))){
            PnlIDlist.add(c.Panel_R2__c);
                }
        }
        //The below soql pulls all the panels associated to the COI records which are used to display on the page through program.
         pnllist=[Select Id, Name,Program_Lookup__r.Name,PFA__r.Name from Panel__c where ID IN:PnlIDlist AND Panel_Due_Dtae__c>=TODAY ORDER BY PFA__r.Name];
    }
    
}