/*
*------------------------------------------------------------------------------
 *  Date      Project             Developer             Justification
 *  04-10-17  Invoicing-Phase2       Himanshu Kalra, M&S Consulting  Creation
 *  04-10-17  Invoicing-Phase2       Himanshu Kalra, M&S Consulting  Enables the Awardee to Submit an Invoice to PCORI 
 *  
 * -----------------------------------------------------------------------------
*/ 
global class callApprovalProcessController {
    webservice static void submitRecords(Id invoiceId){
        Approval.ProcessSubmitRequest objReq;
        list<Approval.ProcessSubmitRequest> lstReqs = new list<Approval.ProcessSubmitRequest>();
        string submitComments = '';
        if(invoiceId != null ){
                objReq = new Approval.ProcessSubmitRequest();
                objReq.setComments(submitComments);
                objReq.setObjectId(invoiceId);
                lstReqs.add(objReq);

            list<Approval.ProcessResult> lstRes = Approval.process(lstReqs, false);
            system.debug(lstRes);
        }
    } 
    
     webservice static void RejectRecords(Id invoiceId){
       Set<Id> pIds = (new Map<Id, ProcessInstance>([SELECT Id,Status,TargetObjectId FROM ProcessInstance where Status='Pending' and TargetObjectId = :invoiceId])).keySet();
Set<Id> pInstanceWorkitems = (new Map<Id, ProcessInstanceWorkitem>([SELECT Id,ProcessInstanceId FROM ProcessInstanceWorkitem WHERE ProcessInstanceId in :pIds])).keySet();

         list<Approval.ProcessWorkitemRequest> allReq = New list<Approval.ProcessWorkitemRequest>(); 
             for (Id pInstanceWorkitemsId:pInstanceWorkitems){
    system.debug(pInstanceWorkitemsId);
        Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
         req2.setComments(label.Invoice_FinanceRejectMessage);
        req2.setAction('Reject'); //to approve use 'Approve'
        req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});

        // Use the ID from the newly created item to specify the item to be worked
        req2.setWorkitemId(pInstanceWorkitemsId);

        // Add the request for approval
        allReq.add(req2);
      }
      Approval.ProcessResult[] result2 =  Approval.process(allReq);

        }
     
   /* @future
    webservice static void submitRecordsFuture(Id invoiceId){
        callApprovalProcessController.submitRecords(invoiceId);
    }*/
}