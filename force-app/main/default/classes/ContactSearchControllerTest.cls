/**
    * ContactSearchControllerTest - <description>
    * Created by BrainEngine Cloud Studio
    * @author: brian Matthews
    * @version: 1.0
*/

@isTest
private class ContactSearchControllerTest {
    static testMethod void myUnitTest() {
    test.starttest();
        Contact a = new Contact();
        a.FirstName = 'Jim';
        a.LastName = 'Jones';
        Database.insert(a);
        
        PageReference pageRef = Page.ContactSearchPage;
        Test.setCurrentPageReference(pageRef);
        // create an instance of the controller
        ApexPages.StandardController sc = new ApexPages.standardController(a);
        ContactSearchController myPageCon = new ContactSearchController(sc);

        ApexPages.CurrentPage().getparameters().put('accountName', 'Acme');
        ApexPages.CurrentPage().getparameters().put('firstname', 'Jim');
        ApexPages.CurrentPage().getparameters().put('lastname', 'Jones');
        //myPageCon.searchText = 'ACME';
        myPageCon.enabled = true;
        myPageCon.runSearch();
        
        myPageCon.createNew();
        myPageCon.runQuery();
        myPageCon.toggleSort();
        
        string sortDir = myPageCon.sortDir;
        string sortField = myPageCon.sortField;
        string debugSoql = myPageCon.debugSoql;
        test.stoptest();
    }
}