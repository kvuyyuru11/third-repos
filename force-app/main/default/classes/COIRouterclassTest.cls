@isTest
public class COIRouterclassTest {
    
    static testMethod void testCOIController() {       
        
        RecordType rt = [SELECT Id FROM RecordType WHERE sObjectType = 'COI_Expertise__c' AND Name = 'General Record Type'];
        list<User> u2 = TestDataFactory.getpartnerUserTestRecords(1,1);
        
        System.runAs(u2[0]) {            
            COI_Expertise__c a = new COI_Expertise__c();
            a.RecordTypeId = rt.Id;
            insert a;
            
            ApexPages.StandardController ctrl = new ApexPages.StandardController(a);
            
            COIRouterClass COIRC = new COIRouterClass(ctrl);
            COIRC.router3();
        }        
    }
    static testMethod void testRouterController1() {
        
        RecordType rt = [SELECT Id FROM RecordType WHERE sObjectType = 'COI_Expertise__c' AND Name = 'General Record Type'];
        list<User> u2 = TestDataFactory.getpartnerUserTestRecords(1,1);
        
        System.runAs(u2[0]) {  
            test.starttest();    
            COI_Expertise__c a = new COI_Expertise__c();      
            a.RecordTypeId = rt.Id;
            insert a;
            
            ApexPages.StandardController ctrl = new ApexPages.StandardController(a);
            
            COIRouterClass COIRC = new COIRouterClass(ctrl);
            COIRC.router3();
            test.stoptest();
        }
    }
}