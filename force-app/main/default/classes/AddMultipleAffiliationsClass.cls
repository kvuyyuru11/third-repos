//This is a controller class for handling the COIboard of directors page and logic of creating the multiple affiliation records which takes the help of the helper class to achieve this
public class AddMultipleAffiliationsClass {
    
    public boolean disablerelationship2{get;set;}
    public boolean disableapplies2{get;set;}
    public boolean disablecompany2{get;set;}
    public boolean disablerelationship{get;set;}
    public boolean disableapplies{get;set;}
    public boolean disablecompany{get;set;}
    public boolean othfbnr{get;set;}
    public boolean othfbat{get;set;}
    public boolean othpnr{get;set;}
    public boolean othpat{get;set;}
    public boolean checkbox01{get;set;}
    public boolean checkbox02{get;set;}
    
    public boolean checkbox04{get;set;}    
    public boolean showbutton{get;set;}
    public boolean showbutton2{get;set;}
    public Integer rowToRemove {get;set;}
    public Integer rowToRemove2 {get;set;}
    public List<WrapperpaAffiliationList> waAffList {get;set;}
    public List<WrapperpaAffiliationList> waAffList2 {get;set;}    
    public Contact con{get;set;}
    public string Date123{get;set;}  
    public boolean showcheckbox{get;set;}   
    public boolean showcheckbox1{get;set;} 
    public boolean showcheckbox2{get;set;} 
    Public String copyRecepients;
    
    //Map<Id,Affiliation__c> affemails = new Map<Id,Affiliation__c>();
    
    //constructor
    public AddMultipleAffiliationsClass(){
        
        
        User u=[select contactId,LastLoginDate from User where id=:userinfo.getUserId()];
        con=[select Id,firstname,lastname from contact where id=:U.contactId];
        
        disablerelationship2=true;
        disableapplies2=true;
        disablecompany2=true;
        disablerelationship=true;
        disableapplies=true;
        disablecompany=true;
        showcheckbox=true;
        showcheckbox1=true;
        showcheckbox2=true;
        showbutton=true;
        showbutton2=true;
        othfbnr=true;
        othfbat=true;
        othpnr=true;
        othpat=true;
        Date123=date.today().format();
        
        waAffList = new List<WrapperpaAffiliationList>();
        waAffList2 = new List<WrapperpaAffiliationList>();
        // to Identify Financial and Business Associations:
        for(Affiliation__c aff : [select Organization_Name__c,Financial_and_Business_Association_Other__c,Financial_and_Business_Applied_Other__c,Nature_of_relationship__c,Applies_to__c,CreatedDate,LastModifiedDate from Affiliation__c where Contact__c=:con.Id and Financial_and_Business_Association__c=true and Old__c=:false]){
            WrapperpaAffiliationList temp = new WrapperpaAffiliationList();
            Affiliation__c tempaff= new Affiliation__c();
            tempaff.Nature_of_relationship__c   = aff.Nature_of_relationship__c;
            tempaff.Organization_Name__c   = aff.Organization_Name__c;
            tempaff.Financial_and_Business_Applied_Other__c   = aff.Financial_and_Business_Applied_Other__c;
            tempaff.Applies_to__c   = aff.Applies_to__c;
            tempaff.Financial_and_Business_Association_Other__c   = aff.Financial_and_Business_Association_Other__c;
            temp.record=tempaff;
            waAffList.add(temp);
        }
        //to Identify Personal Associations 
        for(Affiliation__c aff1 : [select Organization_Name__c,Nature_of_relationship_Personal__c,Personal_Association_Other__c,Personal_Applied_Other__c,Financial_and_Business_Association_Other__c,Applies_to__c from Affiliation__c where Contact__c=:con.Id and Personal_Association__c=true and Old__c=:false]){
            WrapperpaAffiliationList temp1 = new WrapperpaAffiliationList();
            Affiliation__c tempaff1= new Affiliation__c();
            tempaff1.Nature_of_relationship_Personal__c   = aff1.Nature_of_relationship_Personal__c;
            tempaff1.Organization_Name__c   = aff1.Organization_Name__c;
            tempaff1.Personal_Association_Other__c   = aff1.Personal_Association_Other__c;
            tempaff1.Personal_Applied_Other__c   = aff1.Personal_Applied_Other__c;
            tempaff1.Applies_to__c   = aff1.Applies_to__c;
            temp1.record=tempaff1;
            waAffList2.add(temp1);
        }
        if(waAffList.size() == 0)
            addNewRowToAffList();
        if(waAffList2.size() == 0)
            addNewRowToAffList2();
        
    }
    
    //remove action method
    public void removeRowFromAffList(){
        waAffList = AddMultipleAffiliationHelperClass.removeRowToAffiliationList(rowToRemove, waAffList);
    }
    
    //remove action method for block 2
    public void removeRowFromAffList2(){
        waAffList2 = AddMultipleAffiliationHelperClass.removeRowToAffiliationList2(rowToRemove2, waAffList2);
    }
    
    //add action method
    public void addNewRowToAffList(){
        waAffList = AddMultipleAffiliationHelperClass.addNewRowToAffList(waAffList);
    }
    
    //add action method block2
    public void addNewRowToAffList2(){
        waAffList2 = AddMultipleAffiliationHelperClass.addNewRowToAffList2(waAffList2);
        
    }
    
    //wrapper list
    public class WrapperpaAffiliationList{
        public Integer index {get;set;}
        public Affiliation__c record {get;set;}
    }
    
    //method for checkbox1
    Public void disablemethod(){
        if(checkbox01==true){
            disablerelationship=false;
            disableapplies=false;
            disablecompany=false;
            showbutton=false;
            othfbnr=false;
            othfbat=false;
            
        }
        else{
            disablerelationship=true;
            disableapplies=true;
            disablecompany=true;
            showbutton=true;
            othfbnr=true;
            othfbat=true;
        }
    }
    
    //method for checkbox1
    Public void disablemethod2(){
        if(checkbox02==true){
            disablerelationship2=false;
            disableapplies2=false;
            disablecompany2=false;
            showbutton2=false;
            othpnr=false;
            othpat=false;
            
        }
        else{
            disablerelationship2=true;
            disableapplies2=true;
            disablecompany2=true;
            showbutton2=true;
            othpnr=true;
            othpat=true;
        }
        
    }
    
    //inserting mutiple affiliation records through the submit button
    public PageReference SaveMultipleAffiliations() {
        
        Integer x=0;
        Integer y=0;
        Integer z=0;
        Integer a=0;
        Integer b=0;
        Integer c=0;
        Integer d=0;
        Integer e=0;
        Integer f=0;
        Integer g=0;
        list<Affiliation__c> affts= new list<Affiliation__c>();
        list<Affiliation__c> affts1= new list<Affiliation__c>();
        list<Affiliation__c> affts2= new list<Affiliation__c>();
        
        for(WrapperpaAffiliationList altn:waAffList){
            if(altn.record.Organization_Name__c==null && checkbox01==false){
                
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select a name of company or organization or select "I have no financial or business disclosures."'));
                x=1;
            }
            if(altn.record.Nature_of_relationship__c==null && checkbox01==false){
                
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select a nature of relationship or select "I have no financial or business disclosures."'));
                y=1;
            }
            
            if(altn.record.Applies_to__c==null && checkbox01==false){
                
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select a applies to or select "I have no financial or business disclosures."'));
                z=1;
            }
            
            if((altn.record.Nature_of_relationship__c=='Other'&& altn.record.Financial_and_Business_Association_Other__c==''&& checkbox01==false) ||(altn.record.Nature_of_relationship__c=='Other'&& altn.record.Financial_and_Business_Association_Other__c==null&& checkbox01==false)){
                system.debug('entered');   
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please specify the nature of relationship if "other" is selected.'));
                d=1;
            }
            
            if((altn.record.Applies_to__c=='Other'&& altn.record.Financial_and_Business_Applied_Other__c==null&& checkbox01==false)||(altn.record.Applies_to__c=='Other'&& altn.record.Financial_and_Business_Applied_Other__c==''&& checkbox01==false)){
                
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please specify the applies to if "other" is selected.'));
                e=1;
            }
            
        }
        
        for(WrapperpaAffiliationList altn:waAffList2){
            if(altn.record.Organization_Name__c==null && checkbox02==false){
                
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select a name of company or organization or select "I have no personal associations disclosures."'));
                a=1;
            }
            if(altn.record.Nature_of_relationship_Personal__c==null && checkbox02==false){
                
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select a nature of relationship or select "I have no personal associations disclosures."'));
                b=1;
            }
            
            if(altn.record.Applies_to__c==null && checkbox02==false){
                
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select a applies to or select "I have no personal associations disclosures."'));
                c=1;
            }
            
            if((altn.record.Nature_of_relationship_Personal__c=='Other'&& altn.record.Personal_Association_Other__c==''&& checkbox02==false)||(altn.record.Nature_of_relationship_Personal__c=='Other'&& altn.record.Personal_Association_Other__c==null&& checkbox02==false)){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please specify the nature of relationship if "other" is selected.'));
                f=1;
            }
            
            if((altn.record.Applies_to__c=='Other'&& altn.record.Personal_Applied_Other__c==''&& checkbox02==false)||(altn.record.Applies_to__c=='Other'&& altn.record.Personal_Applied_Other__c==null&& checkbox02==false)){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please specify the applies to if "other" is selected.'));
                g=1;
            }
            
        }
        
        
        if(checkbox01==true && checkbox02==false){
            for(Affiliation__c aff : [select Id,Old__c from Affiliation__c where Contact__c=:con.Id and Financial_and_Business_Association__c=true ]){
                aff.Old__c=true;
                affts.add(aff);
            }
            database.update(affts);
        }
        
        
        if(checkbox01==false && checkbox02==true){
            for(Affiliation__c aff1 : [select Id,Old__c from Affiliation__c where Contact__c=:con.Id and Personal_Association__c=true ]){
                aff1.Old__c=true;
                affts1.add(aff1);
            }
            database.update(affts1);
        }
        
        
        
        
        if(checkbox04==false){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please check the checkbox below certification'));
        }
        
        
        if( ((checkbox01==false&&checkbox02==false&&checkbox04==true)||((checkbox01==false&&checkbox02==true)&&(checkbox04==true))||((checkbox01==true&&checkbox02==false)&&(checkbox04==true))) && (x==0 && y==0 && z==0 && a==0 &&b==0 &&c==0 && d==0 && e==0 && f==0 && g==0)){
            
            if(checkbox01==false&&checkbox04==true){
                system.debug('controller save method is calling-->');
                for(Affiliation__c aff : [select Id,Old__c from Affiliation__c where Contact__c=:con.Id and Financial_and_Business_Association__c=true ]){
                    aff.Old__c=true;
                    affts.add(aff);
                }
                database.update(affts);
                if(checkbox02==true){
                    Affiliation__c accTemp = new Affiliation__c();
                    accTemp.Contact__c=con.id; 
                    accTemp.Personal_Association__c=true;
                    affts2.add(accTemp);
                }
                
                
                AddMultipleAffiliationHelperClass.save(waAffList);
                
            }   
            
            if(checkbox02==false&&checkbox04==true){
                system.debug('controller save method is calling-->');
                for(Affiliation__c aff1 : [select Id,Old__c from Affiliation__c where Contact__c=:con.Id and Personal_Association__c=true ]){
                    aff1.Old__c=true;
                    affts1.add(aff1);
                }
                database.update(affts1);
                if(checkbox01==true){
                    Affiliation__c accTemp = new Affiliation__c();
                    accTemp.Contact__c=con.id; 
                    accTemp.Financial_and_Business_Association__c=true;
                    affts2.add(accTemp);
                }
                
                AddMultipleAffiliationHelperClass.save2(waAffList2);
                
            }
            if(!affts2.isempty()&&affts2.size()>0){
                insert affts2;
            }
            /***************************** Code Block to send emails to submitter and Jayne START ******************************************/
            
            EmailTemplate et = [Select id, body from EmailTemplate where DeveloperName =:system.label.BOG_COI_Email_template];
            User u1 = [select Id,email,contactId,LastLoginDate from User where id=:userinfo.getUserId()];
            OrgWideEmailAddress owea = [select id from OrgWideEmailAddress where Address =:system.label.BOG_COI_Email_From_Address];             
            copyRecepients = system.label.BOG_COI_Email_Recipient;            
            string targetid=u1.Id;
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTargetObjectId(targetid);
            mail.setTemplateId(et.Id);
            mail.setOrgWideEmailAddressId(owea.Id);
            mail.setUseSignature(false);
            mail.setBccSender(false);           
            String[] CCEmails =copyRecepients.split(',');            
            mail.setCcAddresses(CCEmails);              
            mail.setSaveAsActivity(false);
            if(!Test.isRunningTest()){
                Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});                  
            }
            
            /***************************** Code Block to send emails to submitter and Jayne END ******************************************/   
                       
            PageReference pageRef= new PageReference(label.COIBODsubmitpage); 
            pageRef.setRedirect(true);            
            return pageRef; 
        }
        if(checkbox01==true&&checkbox04==true&&checkbox02==true){
            
            Affiliation__c accTemp = new Affiliation__c();
            accTemp.Contact__c=con.id; 
            accTemp.Personal_Association__c=true;
            affts2.add(accTemp);
            Affiliation__c accTemp1 = new Affiliation__c();
            accTemp1.Contact__c=con.id; 
            accTemp1.Financial_and_Business_Association__c=true;
            affts2.add(accTemp1);
            for(Affiliation__c aff : [select Id,Old__c from Affiliation__c where Contact__c=:con.Id and (Financial_and_Business_Association__c=true OR Personal_Association__c=true)]){
                aff.Old__c=true;
                affts.add(aff);
            }
            update affts;
            insert affts2;
            
            /***************************** Code Block to send emails to submitter and Jayne START ******************************************/
            
            EmailTemplate et = [Select id, body from EmailTemplate where DeveloperName =:system.label.BOG_COI_Email_template];
            User u1 = [select Id,email,contactId,LastLoginDate from User where id=:userinfo.getUserId()];
            OrgWideEmailAddress owea = [select id from OrgWideEmailAddress where Address =:system.label.BOG_COI_Email_From_Address];             
            copyRecepients = system.label.BOG_COI_Email_Recipient;            
            string targetid=u1.Id;
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTargetObjectId(targetid);
            mail.setTemplateId(et.Id);
            mail.setOrgWideEmailAddressId(owea.Id);
            mail.setUseSignature(false);
            mail.setBccSender(false);           
            String[] CCEmails =copyRecepients.split(',');            
            mail.setCcAddresses(CCEmails);              
            mail.setSaveAsActivity(false);
            if(!Test.isRunningTest()){
                Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});                  
            }
            
            /***************************** Code Block to send emails to submitter and Jayne END******************************************/
            
            PageReference pageRef= new PageReference(label.COIBODsubmitpage); 
            pageRef.setRedirect(true);
            
            return pageRef;
        }
        else return null;
    }
    
    
}