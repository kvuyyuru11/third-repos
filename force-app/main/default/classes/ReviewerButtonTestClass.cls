@isTest
public class ReviewerButtonTestClass {
    
   public static testmethod void test1(){
   User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
    Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
    insert acnt;
        acnt.IsPartner=true;
        update acnt;
    Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                              Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
    insert con;  
      User user = new User(alias = 'test123', email='test156klu@myhnmmail234.com',
      emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
      localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
      ContactId = con.Id,
      timezonesidkey='America/New_York', username='test156klu@myhnmmail234.com');
        
     insert user;

  
  System.RunAs(user) {     
           ReviewerButton.pickPage(user.id,'reviewer');
      PermissionSetAssignment per = new PermissionSetAssignment(PermissionSetId = [SELECT Name, Id from PermissionSet where Label ='Merit Reviewer Approved- PCL' limit 1].id,AssigneeId=user.id);
      insert per;
      ReviewerButton.pickPage(user.id,'reviewer');
  }

       
    }
   


}