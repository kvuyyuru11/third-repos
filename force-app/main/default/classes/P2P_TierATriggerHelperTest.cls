@isTest
private class P2P_TierATriggerHelperTest{
    
    @testSetup
    public static void testDataSetup() {
        List<P2P_TierA__c> p2pTierAList = new List<P2P_TierA__c>();
        for (Integer i=0;i<200;i++) {
            P2P_TierA__c p2pRec = new P2P_TierA__c(TierA_MidProj_Attachments_Note__c = 'Test Name'+ i);
            p2pTierAList.add(p2pRec);
        }
        insert p2pTierAList;
    }

    public static testMethod void testLockingOfP2P() {
        List<P2P_TierA__c> p2pTierAList = [SELECT Id, Name FROM P2P_TierA__c];
        List<P2P_TierA__c> p2pUpdateList = new List<P2P_TierA__c>();
        List<P2P_TierA__c> p2psecondUList = new List<P2P_TierA__c>();
        for (P2P_TierA__c p2p : p2pTierAList) {
            p2p.IsSubmitted__c = true;
            p2pUpdateList.add(p2p); 
        }
        update p2pUpdateList;
        for (P2P_TierA__c p2p : p2pUpdateList) {
            p2p.IsSubmitted__c = false;
            p2psecondUList.add(p2p);
        }
        update p2psecondUList;
    } 
}