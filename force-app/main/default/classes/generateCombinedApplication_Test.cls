@isTest
private class generateCombinedApplication_Test 
{
     @testSetup
    private static void testDataCreation() {


        Id devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RA Applications').getRecordTypeId();
        Opportunity Opp = new Opportunity(Name='Test Opportunity',Project_Name_off_layout__c='Test Opportunity',StageName='Invited',
                                        closedate=system.today(),LOI_resubmission_D_I__c='Yes',D_I_Resubmission__c='Yes',
                                        Was_LOI_invited_D_I__c='Yes',Number_of_submissions__c='1',Summary_Statement_received__c='Yes',
                                        Application_Number__c='123',RecordTypeId=devRecordTypeId);
        insert Opp;
test.startTest();
        ContentVersion contentVersion_1 = new ContentVersion(
            Title = Opp.Name+' '+Opp.Application_Number__c+' Application Budget.pdf',
            PathOnClient = 'Penguins.pdf',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert contentVersion_1;

        ContentVersion contentVersion_2 = [Select Id,ContentDocumentId from Contentversion where Id=:contentVersion_1.Id];
        contentdocumentLink cdl = new contentdocumentLink(LinkedEntityId = Opp.Id,ContentDocumentId = contentVersion_2.ContentDocumentId,
                                                            ShareType='I');
        insert cdl;
        
        FGM_Portal__Quiz__c quiz = new FGM_Portal__Quiz__c(Name='17 cycle 3');
        Insert quiz;

        FGM_Portal__Related_List_Controller__c rlc = new FGM_Portal__Related_List_Controller__c(Name = 'Templates and Uploads',
                                                     FGM_Portal__ObjectName__c = 'Opportunity', FGM_Portal__ParentObject__c = 'Opportunity',
                                                     FGM_Portal__LookupField__c = 'Opportunity', FGM_Portal__Quiz__c = quiz.Id,
                                                     RecordTypeId='012700000001ewl');
        insert rlc;

        FGM_Portal__Questions__c quest = new FGM_Portal__Questions__c();
        quest.FGM_Portal__Question__c = 'Research Plan';
        quest.FGM_Portal__Type__c = 'Attachment';
        insert quest;
        
        FGM_Portal__Quiz_Questions__c qq = new FGM_Portal__Quiz_Questions__c();
        qq.FGM_Portal__Question__c = quest.Id;
        qq.FGM_Portal__Quiz__c = quiz.Id;
        
        FGM_Portal__Question_Attachment__c qa = new FGM_Portal__Question_Attachment__c();
        qa.FGM_Portal__Question__c = quest.Id;
        qa.FGM_Portal__Opportunity__c = Opp.Id;
        insert qa;
        
        Attachment attach1=new Attachment(); 
        attach1.Name = '123_PriorSummaryStatement.pdf'; 
        Blob bodyBlob1=Blob.valueOf('Unit Test Attachment Body'); 
        attach1.body=bodyBlob1; 
        attach1.parentId=Opp.id;
        attach1.ContentType = 'pdf'; 
        attach1.IsPrivate = false; 
        attach1.Description = 'Test'; 
        insert attach1;
        
        Attachment attach2=new Attachment(); 
        attach2.Name='Unit Test Attachment'; 
        Blob bodyBlob2=Blob.valueOf('Unit Test Attachment Body'); 
        attach2.body=bodyBlob2; 
        attach2.parentId=qa.id;
        attach2.ContentType = '.pdf'; 
        attach2.IsPrivate = false; 
        attach2.Description = 'Test'; 
        insert attach2;

        Campaign cmp = new Campaign(Name='Dissemination and Implementation');
        cmp.FGM_Portal__Application_Quiz__c = quiz.Id;
        cmp.EndDate = Date.newInstance(2016, 12, 9);
        insert cmp;

        Campaign cmp2 = new Campaign(Name='Addressing Disparities');
        cmp.FGM_Portal__Application_Quiz__c = quiz.Id;
        cmp.EndDate = Date.newInstance(2016, 12, 9);
        insert cmp2;

        Campaign cmp3 = new Campaign(Name='Implementation of Shared Decision Making Approach');
        cmp.FGM_Portal__Application_Quiz__c = quiz.Id;
        cmp.EndDate = Date.newInstance(2016, 12, 9);
        insert cmp3;

test.stopTest();

    }

    private static testMethod void generatePDFforNormalApplicaitons()
    {   
        
        List<Opportunity> opportunityList = [Select Id, Name From Opportunity Limit 1];
        List<Campaign> campaignList = [Select Id, Name from Campaign];
        List<FGM_Portal__Quiz__c> quizList = [Select Id, name from FGM_Portal__Quiz__c];
        List<Attachment> attachmentList = [Select Id, Name from Attachment Where Name =: '123_PriorSummaryStatement.pdf'];
        List<FGM_Portal__Question_Attachment__c> qq = [Select Id, Name from FGM_Portal__Question_Attachment__c where Question_Text__c =: 'Research Plan'];
        String str = '';
        for (Campaign camp : campaignList) {
            if (camp.Name == 'Addressing Disparities') {
                opportunityList[0].CampaignId = camp.Id;
               opportunityList[0].D_I_Resubmission__c='Yes';
               opportunityList[0].Previous_Application__c='123456';

            }
        }
        test.startTest();
        update opportunityList[0];
        str = generateCombinedApplication.updateCongaRequestLink(opportunityList[0].Id, 'PriorSummaryStatement', 'static', 'template', 'session', 'serverurl');
        opportunityList[0].D_I_Resubmission__c = 'No';
        opportunityList[0].App_Resubmission__c = 'No';
        update opportunityList[0];
        str = generateCombinedApplication.updateCongaRequestLink(opportunityList[0].Id, 'PriorSummaryStatement', 'static', 'template', 'session', 'serverurl');
        generateCombinedApplication.updateQuiz(opportunityList[0].Id);

        FGM_Portal__Questions__c quest = new FGM_Portal__Questions__c();
        quest.FGM_Portal__Question__c = 'Budget Detailed Attachment';
        quest.FGM_Portal__Type__c = 'Attachment';
        quest.FGM_Portal__Quiz__c = quizList[0].Id;
        insert quest;
        generateCombinedApplication.updateQuiz(opportunityList[0].Id);

        delete attachmentList[0];
        str = generateCombinedApplication.updateCongaRequestLink(opportunityList[0].Id, 'PriorSummaryStatement', 'static', 'template', 'session', 'serverurl');
        delete qq[0];
        str = generateCombinedApplication.updateCongaRequestLink(opportunityList[0].Id, 'PriorSummaryStatement', 'static', 'template', 'session', 'serverurl');
test.stopTest();
    }

    private static testMethod void generatePDFforDIApplications()
    {   
        List<Opportunity> opportunityList = [Select Id, Name From Opportunity Limit 1];
        List<Campaign> campaignList = [Select Id, Name from Campaign];
        String str = '';
        for (Campaign camp : campaignList) {
            if (camp.Name == 'Dissemination and Implementation') {
                opportunityList[0].CampaignId = camp.Id;
                opportunityList[0].D_I_Resubmission__c='Yes';
               opportunityList[0].Previous_Application__c='123456';
            }
        }
        test.startTest();
        update opportunityList[0];
        str = generateCombinedApplication.updateCongaRequestLink(opportunityList[0].Id, 'PriorSummaryStatement', 'static', 'template', 'session', 'serverurl');
        opportunityList[0].D_I_Resubmission__c = 'No';
        opportunityList[0].App_Resubmission__c = 'No';
        update opportunityList[0];
        str = generateCombinedApplication.updateCongaRequestLink(opportunityList[0].Id, 'PriorSummaryStatement', 'static', 'template', 'session', 'serverurl');
        generateCombinedApplication.updateQuiz(opportunityList[0].Id);
        test.stopTest();
    }

    private static testMethod void generatePDFforSDMApplications()
    {   
        List<Opportunity> opportunityList = [Select Id, Name From Opportunity Limit 1];
        List<Campaign> campaignList = [Select Id, Name from Campaign];
        String str = '';
        for (Campaign camp : campaignList) {
            if (camp.Name == 'Implementation of Shared Decision Making Approach') {
                opportunityList[0].CampaignId = camp.Id;
                opportunityList[0].D_I_Resubmission__c='Yes';
               opportunityList[0].Previous_Application__c='123456';
            }
        }
        test.startTest();
        update opportunityList[0];
        str = generateCombinedApplication.updateCongaRequestLink(opportunityList[0].Id, 'PriorSummaryStatement', 'static', 'template', 'session', 'serverurl');
        opportunityList[0].D_I_Resubmission__c = 'No';
        opportunityList[0].App_Resubmission__c = 'No';
        update opportunityList[0];
        str = generateCombinedApplication.updateCongaRequestLink(opportunityList[0].Id, 'PriorSummaryStatement', 'static', 'template', 'session', 'serverurl');
        generateCombinedApplication.updateQuiz(opportunityList[0].Id);
        test.stopTest();
    }
}