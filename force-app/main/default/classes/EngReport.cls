public with sharing class EngReport{
    public Engagement_Report__c er {get;set;}
    public Engagement_Report__c fer {get;set;}
    public List<Engagement_Report__c> engrept;
    String erId;
    String ferId;
    public Id grId;
    public EngReport (){
        grId = ApexPages.currentPage().getParameters().get('Id');
        List<RecordType> reclst = [SELECT Id, developerName FROM RecordType WHERE sObjectType = 'Engagement_Report__c'];
        for(RecordType rec :reclst){
            if(rec.DeveloperName == 'Engagement_Report'){
                erId = rec.Id;
            }
            if(rec.DeveloperName == 'Final_Engagement_Report'){
                ferId = rec.Id;
            }
        }
        if(grId != null){
            try{
                engrept = [SELECT Id, RecordTypeId, Examples_of_impacts_of_engagement__c, Why_Not__c, Another_Q7__c, Another_Q8__c, Another_Q9__c, Challenges_with_patient_stkholdr__c, Choice_Q7__c, Choice_Q8__c,
                        Choice_Q9__c, Communities_engaged_with_project__c, Data_Analysis_Q7__c, Data_Analysis_Q8__c, Data_Analysis_Q9__c,
                        Data_Collection_Q7__c, Data_Collection_Q8__c, Data_Collection_Q9__c, Describe_progress_on_engagmnt_plan__c, 
                        Design_Q7__c, Design_Q8__c, Design_Q9__c, Dissemination_Q7__c, Dissemination_Q8__c, Dissemination_Q9__c, 
                        Engage_with_patients_stakeholders__c, How_you_engaged_with_patients__c, Participant_Q7__c, Participant_Q8__c, 
                        Participant_Q9__c, Are_patient_stakeholders__c, Phase_of_project_patient_stkeholdr_engag__c, SDesign_Q7__c, 
                        SDesign_Q8__c, SDesign_Q9__c, Study_Q7__c, Study_Q8__c, Study_Q9__c,    Phase_Name__c,
                        Q1_impact_on_quality__c, Q2_impact_on_process__c, Q3_Positive_impact_on_your_project__c, Q4_Negative_impact_on_your_project__c,
                        Q5_Describe_unexpected_impact_on_project__c, Q6_How_Likely__c, Q6a_Likely_to_engage_with_patients__c, Q7_Worked_Strategies__c,
                        Q8_Not_Worked_Strategies__c, Q3_N_Notable_Impacts_on_study_operations__c, Q4_N_Notable_impacts_on_study_quality__c, 
                        Q5_N_Notable_impacts_on_usefulness_study__c, Q6_N_Impacts_on_Engagement__c, Q6a_N_Likelihood_of_engagement__c, Research_Team_Partners_work_together__c,
                        Other_Research_Projects__c FROM Engagement_Report__c WHERE Grantee_Report__c = :grId];
                for(Engagement_Report__c erp :engrept){
                        if(erp.RecordTypeId == erId){
                            er = erp;
                        }
                        if(erp.RecordTypeId == ferId){
                            fer = erp;
                        }

                }
                
                        
            }
            catch(Exception e){
                System.debug('Exception is ---->' + e);
            }    
        }
        
    }
}