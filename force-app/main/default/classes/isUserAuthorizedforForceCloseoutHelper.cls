public class isUserAuthorizedforForceCloseoutHelper{

    public static void updateAmounts(Map<Id, Contract_Checklist__c> contratcChecklistNewMap, Map<Id, Contract_Checklist__c> contratcChecklistOldMap){
        Map<Id, Project_Invoice_Calculation__c> projectIdInvoiceCalculationMap = new Map<Id, Project_Invoice_Calculation__c>();
        Map<Id, Project_Invoice_Calculation__c> cmaprojectIdInvoiceCalculationMap = new Map<Id, Project_Invoice_Calculation__c>();
        Map<Id, Invoice__c> projectIdInvoiceMap = new Map<Id, Invoice__c>();
        List<Id> projectIdList = new List<Id>();
        List<Id> InvoiceprojectIdList = new List<Id>();
        List<Contract_Checklist__c> ccList = new List<Contract_Checklist__c>();
        List<Contract_Checklist__c> cmaList = new List<Contract_Checklist__c>();
        List<Contract_Checklist__c> ccUpdateList = new List<Contract_Checklist__c>();
        List<Contract_Checklist__c> cmaUpdateList = new List<Contract_Checklist__c>();
        Id devRecordTypeId = Schema.SObjectType.Contract_Checklist__c.getRecordTypeInfosByName().get('Finance Checklist').getRecordTypeId();
  
        for (Contract_Checklist__c cc : contratcChecklistNewMap.values()) {
                 if(cc.RecordTypeId == devRecordTypeId) {
                     projectIdList.add(cc.Contract__c);
                     ccList.add(cc);   
                    }
                    else if((cc.Closeout_Checklist_Status__c!= contratcChecklistOldMap.get(cc.Id).Closeout_Checklist_Status__c) && cc.Closeout_Checklist_Status__c == 'Completed') {
                     InvoiceprojectIdList.add(cc.Contract__c);
                     cmaList.add(cc);
                     }               
                     
        }
        if(!projectIdList.isEmpty()){
            for (Project_Invoice_Calculation__c pic : getProjectInvoiceCalculations(projectIdList)) {
            projectIdInvoiceCalculationMap.put(pic.Project__c, pic); 
            }
        }
        for (Contract_Checklist__c cc : ccList) {
             if(projectIdInvoiceCalculationMap.containsKey(cc.Contract__c)){
                 Contract_Checklist__c ccClone = new Contract_Checklist__c(id = cc.Id);
                 ccClone.Cumulative_Amount__c = projectIdInvoiceCalculationMap.get(cc.Contract__c).Total_Contract_Cumulative__c;
                 ccClone.De_Obligated_Amount__c = projectIdInvoiceCalculationMap.get(cc.Contract__c).Total_Deobligated_Amount__c;
                 ccUpdateList.add(ccClone); 
             }       
        }
        
        if(!InvoiceprojectIdList.isEmpty()){
            for (Project_Invoice_Calculation__c pic : getProjectInvoiceCalculations(InvoiceprojectIdList)) {
            cmaprojectIdInvoiceCalculationMap.put(pic.Project__c, pic); 
            }
            for (Invoice__c inv : getFinalInvoices(InvoiceprojectIdList)) {
            projectIdInvoiceMap.put(inv.Project__c, inv);
            }
        }
        for (Contract_Checklist__c cc : cmaList) {
             if(projectIdInvoiceMap.containsKey(cc.Contract__c)){
                 Contract_Checklist__c ccClone = new Contract_Checklist__c(id = cc.Id);
                 ccClone.Invoice_Number__c = projectIdInvoiceMap.get(cc.Contract__c).Invoice_Name__c;
                 ccClone.Invoice_Submission_Date__c = projectIdInvoiceMap.get(cc.Contract__c).Invoice_Submission_Date__c;
                 ccClone.Cumulative_Amount__c = cmaprojectIdInvoiceCalculationMap.get(cc.Contract__c).Total_Contract_Cumulative__c;
                 ccClone.De_Obligated_Amount__c = cmaprojectIdInvoiceCalculationMap.get(cc.Contract__c).Total_Deobligated_Amount__c;
                 cmaUpdateList.add(ccClone); 
             }       
        }
       
        if(recursiveTrigger.runOnce()) {
            if(!ccUpdateList.isEmpty())
            database.update(ccUpdateList);
            if(!cmaUpdateList.isEmpty())
            database.update(cmaUpdateList);
            
        }
        
        
        
    }
    private static List<Project_Invoice_Calculation__c> getProjectInvoiceCalculations (List<Id> projectIdList) {
        return [Select Id, Name, Total_Contract_Cumulative__c, Total_Deobligated_Amount__c, Project__c  from Project_Invoice_Calculation__c Where Project__c IN : projectIdList ORDER BY LastModifiedDate Desc];
    }
    
    private static List<Invoice__c> getFinalInvoices (List<Id> InvoiceprojectIdList) {
        return [Select Id, Name, Invoice_Name__c, Invoice_Submission_Date__c, Project__c FROM Invoice__c Where Project__c IN : InvoiceprojectIdList AND Is_Final_Invoice__c='Yes'];
    }
    
    public static void validationForContractCloseOut (List<Contract_Checklist__c> contratcChecklist, Map<Id, Contract_Checklist__c> contratcChecklistNewMap) {
        Map<Id, Contract_Checklist__c> cma_ProjectId_ContractChecklistIdsMap = new Map<Id, Contract_Checklist__c>();
        Map<Id, Contract_Checklist__c> finance_ProjectId_ContractChecklistIdsMap = new Map<Id, Contract_Checklist__c>();
        List<Id> projectIdList = new List<Id>();
        
        Id financeRecordTypeId = Schema.SObjectType.Contract_Checklist__c.getRecordTypeInfosByName().get('Finance Checklist').getRecordTypeId();
        Id cmaRecordTypeId = Schema.SObjectType.Contract_Checklist__c.getRecordTypeInfosByName().get('CMA Checklist').getRecordTypeId();
        
        for (Contract_Checklist__c cc : contratcChecklist) {
            projectIdList.add(cc.Contract__c);       
        }
        
        for(Contract_Checklist__c cc : getContractCloseoutChecklist(projectIdList)) {
            if(cc.recordTypeId == cmaRecordTypeId) {
                cma_ProjectId_ContractChecklistIdsMap.put(cc.Contract__c, cc);
                
            }else if (cc.recordTypeId == financeRecordTypeId) {
                finance_ProjectId_ContractChecklistIdsMap.put(cc.Contract__c, cc);
                
            }
        }
        
        if(contratcChecklistNewMap == null) {
        for (Contract_Checklist__c cc : contratcChecklist) {
            if (cc.recordTypeId == cmaRecordTypeId) {
                if(cma_ProjectId_ContractChecklistIdsMap.containsKey(cc.Contract__c)){
                    cc.addError('A CMA Closeout Checklist has already been Created for this Project');
                }
            }
            if (cc.recordTypeId == financeRecordTypeId) {
                if(finance_ProjectId_ContractChecklistIdsMap.containsKey(cc.Contract__c)){
                    cc.addError('A Finance Closeout Checklist has already been Created for this Project');
                }
            }

        }
        }
        system.debug('Here is the Culprit'+contratcChecklistNewMap);
        if(contratcChecklistNewMap != null) {
        for (Id ccid : contratcChecklistNewMap.keySet()) {

            if(contratcChecklistNewMap.get(ccid).recordTypeId == cmaRecordTypeId && contratcChecklistNewMap.get(ccid).Closeout_Checklist_Status__c =='Completed') {
                if(finance_ProjectId_ContractChecklistIdsMap.containsKey(contratcChecklistNewMap.get(ccid).Contract__c)) {
                   if(finance_ProjectId_ContractChecklistIdsMap.get(contratcChecklistNewMap.get(ccid).Contract__c).Finance_Checklist_Status__c !='Completed') {
                       contratcChecklistNewMap.get(ccid).addError('Finance Team has not Completed the Contract Closeout Checklist for this Project');
                   }
                }else {
                       contratcChecklistNewMap.get(ccid).addError('Finance Team has not created a Contract Closeout Checklist for this Project');
                   }
            }
        }
       } 
    }
    private static List<Contract_Checklist__c> getContractCloseoutChecklist (List<Id> projectIdList) {
        return [Select Id, Name, Contract__c, RecordTypeId, Closeout_Checklist_Status__c, Finance_Checklist_Status__c from Contract_Checklist__c Where Contract__c IN : projectIdList];
    }
    

}