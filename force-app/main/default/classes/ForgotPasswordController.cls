/**
 * An apex page controller that exposes the site forgot password functionality
 */
public with sharing class ForgotPasswordController {
    public String usernamee {get; set;}   
       
    public ForgotPasswordController() {}
    
    public PageReference forgotPassword() {
        boolean success = Site.forgotPassword(usernamee);
         list<user> usrlist=[select username,email from user where username=:usernamee];
         String err = 'This username/email address does not exist in our system. Please check your username/email or email <a href="mailto:help@pcori.org">help@pcori.org</a>for assistance.';
        PageReference pr = Page.ForgotPasswordConfirm;
        pr.setRedirect(true);
        
        if(usrlist.isEmpty()){
        
                 ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, err));
            return null;
        }
        else            
            return pr;
        //}
        //return null;
    }
}