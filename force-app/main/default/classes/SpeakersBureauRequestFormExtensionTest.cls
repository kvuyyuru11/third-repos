/**
 * This class contains unit tests for validating the behavior of Apex class:SpeakersBureauRequestFormExtension
 * 
 * created by Vinayak Sharma    Created Date : 03/07/2017
 * Updated by Himanshu Kalra     Date: 03/10/2017
 */
@isTest
private class SpeakersBureauRequestFormExtensionTest {
    //@Himamnshu @Purpose: To create valid data and submit request form successfully
    static testMethod void validInfo() { 
           
        SpeakersBureauRequestFormExtension extController;

        PageReference pageRef = Page.SpeakersBureauRequestForm;
        Test.setCurrentPage(pageRef); 
        
        Speakers_Bureau__c sb_ValidData = new Speakers_Bureau__c();
        
        ApexPages.StandardController con = new ApexPages.StandardController(sb_ValidData);
        extController = new SpeakersBureauRequestFormExtension(con);

        extController.requestType = 'Internal PCORI staff';
        extController.PCORI_StafName ='Staff member A';
        extController.PCORI_StafEmail ='staffemail@pcori.org';
        extController.orgType = 'Manufacturer';
        extController.organization = 'Pfizer';
        extController.organizerPrefix = 'Mr.';
        extController.organizerFirstName = 'Andrew';
        extController.organizerLastName = 'Smith';
        extController.Phone = '8322320932';
        extController.Email = 'andrew.smith@gmail.com';
        extController.eventType = 'Board Meeting';
        extController.speakTime = '1 hour';
        extController.topicPPT = 'Chrinic Pain';
        extController.formatPPT = 'Workshop';
        extController.otherHostOrg = 'Bristol Mayers Squibb';
        extController.eventTitle = 'Be Ready';
        extController.speakDate = System.Today()+64;
        extController.alternSpeakDate  = System.Today()+84;
        
        extController.preferSpeakrs = 'Samantha Fox';
        extController.otherPreferSpeakrs = 'Andrea Brown'; 
        extController.audienceSize = 400; 
        extController.audienceComp = 'Research members';
        
        extController.venueCity ='San Francisco';
        extController.venueState = 'California';        
        extController.confirmationRequestDate  = System.Today()+90;
        extController.Submit(); 
    }
    //@Himamnshu @Purpose: To create Invalid data to cover Error messages and submit request form unsuccessfully
    static testMethod void invalidInfo() { 
           
        SpeakersBureauRequestFormExtension extController;
        PageReference pageRef = Page.SpeakersBureauRequestForm;
        Test.setCurrentPage(pageRef); 
        
        Speakers_Bureau__c sb_InValidData = new Speakers_Bureau__c();
        
        ApexPages.StandardController con = new ApexPages.StandardController(sb_InValidData);
        extController = new SpeakersBureauRequestFormExtension(con);
    
        extController.PCORI_StafEmail ='staffemailpcori.org.aa';
        extController.Phone = '832-232-093' ;
        extController.orgType = 'Manufacturer';
        extController.organizerPrefix = '-Select-';
        extController.orgType = '-Select-'; 
        extController.requestType = '-Select-';
        extController.eventType = '-Select-';
        extController.formatPPT = '-Select-';
        extController.Submit(); 
        
        //for empty Phone number
        extController.Phone = '';
        System.assertEquals('',extController.Phone);
        extController.Submit(); 
    }
}