@isTest
public class TestMnevalutioncontroller{
    public static testmethod void MnevalutionMethod(){
        User sysAdminUser = [select id from User where profile.name='System Administrator' and IsActive = true limit 1]  ;
        list<Account> acnt = TestDataFactory.getAccountTestRecords(1);
        list<contact> con  = TestDataFactory.getContactTestRecords(1,5);
        list<User> usrs    = TestDataFactory.getpartnerUserTestRecords(1,5); 
        cycle__c c = new cycle__c(name='test20116', COI_Due_Date__c = Date.newInstance(2016,12,02));
        
        insert c;
        Profile p =[Select id from profile where name='PCORI Community Partner'];
        System.RunAs(sysAdminUser) {
            Merit_Reviewer_Evaluation__c MRE = new Merit_Reviewer_Evaluation__c();
            mre.How_would_you_rate_the_time_with_MRO__c='Test1';
            mre.Rating_Scale__c='Test2';
            mre.How_would_you_rate_the_time_with_reviewe__c='Test3';
            mre.timeliness_scale__c='Test4';
            mre.Did_the_mentor_make_themselves_available__c='Test5';  
            mre.themselves_available_scale__c='3';  
            mre.Demeanor_conduct_and_tone_towards_MR__c='Test7';  
            mre.towards_MRO_scale__c='Test8';   
            mre.Demeanor_conduct_and_tone_towards_revie__c='Test9'; 
            mre.towards_reviewers_scale__c='Test10'; 
            mre.Interaction_with_reviewer_and_MRO_regard__c='Test11'; 
            mre.MRO_regarding_real_time_feedback__c='Test12';
            mre.Ability_to_flex_to_reviewers_styles__c='Test13'; 
            mre.reviewers_styles__c='Test14'; 
            mre.Kept_reviewers_progression_on_track__c ='Test15';
            mre.progression_on_track__c='4';  
            mre.Quality_of_guidance_to_mentees__c= 'Test17'; 
            mre.guidance_to_mentees__c='Test18';  
            mre.Understanding_of_the_merit_review_proces__c= 'Test19'; 
            mre.review_process_and_their_role_as_a_mento__c='Test20';  
            mre.Understanding_of_PCORI_merit_review_crit__c ='Test21'; 
            mre.PCORI_merit_review_criteria_and_goals__c ='4';
            mre.address_the_quality_of_this_relationship__c='5';
            mre.Panelist_complete_merit_review_cycle__c='Test';
            mre.Reviewer_Name__c= usrs[0].contactId; 
            mre.Mentor_Name__c= usrs[1].contactId;
            mre.CycleLookup__c= c.id;  
            Insert mre;
            
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(mre);
            ApexPages.currentPage().getParameters().put('Id',mre.id);
            
            Mnevalutioncontroller ec = new Mnevalutioncontroller(sc);
            ec.Mnsavemethod();
            ec.Mnupdatemethod();
            ec.submethod();
            ec.Reviewmethod();
        }
    }
    
    public static testmethod void MnevalutionMethod1(){
        User sysAdminUser = [select id from User where profile.name='System Administrator' and IsActive = true limit 1] ;
        list<Account> acnt = TestDataFactory.getAccountTestRecords(1);
        list<contact> con  = TestDataFactory.getContactTestRecords(1,5);
        list<User> usrs    = TestDataFactory.getpartnerUserTestRecords(1,5);  
        cycle__c c = new cycle__c(name='test20116', COI_Due_Date__c = Date.newInstance(2016,12,25));
        
        insert c;
        Profile p =[Select id from profile where name='PCORI Community Partner'];
        //Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();  
        User u2 = [Select id from User where ProfileId =:p.id AND isActive = TRUE LIMIT 1];
        System.RunAs(sysAdminUser) {
            Merit_Reviewer_Evaluation__c MRE = new Merit_Reviewer_Evaluation__c();
            mre.How_would_you_rate_the_time_with_MRO__c='Test1';
            mre.Rating_Scale__c='Test2';
            mre.How_would_you_rate_the_time_with_reviewe__c='Test3';
            mre.timeliness_scale__c='Test4';
            mre.Did_the_mentor_make_themselves_available__c='Test5';  
            mre.themselves_available_scale__c='3';  
            mre.Demeanor_conduct_and_tone_towards_MR__c='Test7';  
            mre.towards_MRO_scale__c='Test8';   
            mre.Demeanor_conduct_and_tone_towards_revie__c='Test9'; 
            mre.towards_reviewers_scale__c='Test10'; 
            mre.Interaction_with_reviewer_and_MRO_regard__c='Test11'; 
            mre.MRO_regarding_real_time_feedback__c='Test12';
            mre.Ability_to_flex_to_reviewers_styles__c='Test13'; 
            mre.reviewers_styles__c='Test14'; 
            mre.Kept_reviewers_progression_on_track__c ='Test15';
            mre.progression_on_track__c='4';  
            mre.Quality_of_guidance_to_mentees__c= 'Test17'; 
            mre.guidance_to_mentees__c='Test18';  
            mre.Understanding_of_the_merit_review_proces__c= 'Test19'; 
            mre.review_process_and_their_role_as_a_mento__c='Test20';  
            mre.Understanding_of_PCORI_merit_review_crit__c ='Test21'; 
            mre.PCORI_merit_review_criteria_and_goals__c ='4';
            mre.address_the_quality_of_this_relationship__c='5';
            mre.Panelist_complete_merit_review_cycle__c='Test';
            mre.Reviewer_Name__c= usrs[0].contactId; 
            mre.Mentor_Name__c= usrs[1].contactId; 
            mre.CycleLookup__c= c.id;  
            Insert mre;
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(mre);
            ApexPages.currentPage().getParameters().put('Id',null);
            
            Mnevalutioncontroller ec = new Mnevalutioncontroller(sc);
            ec.submethod();
        }
    }
}