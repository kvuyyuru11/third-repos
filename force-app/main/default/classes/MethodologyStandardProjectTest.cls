/*
    Author     : VenuGopal Mokkapati
    Date       : 7th Apr 2017
    Name       : AccountEditRedirectControllerTest
    Description: Methodologystandardproject test class
*/
@isTest
public class MethodologyStandardProjectTest{
    
    private id oppid;
    private id stanid;
    static testMethod void MethodologyStandardProjectControllerTest()
    {
    //Gets the recordtypeId of the Opportunity Recordtype "Research Awards"
    Schema.DescribeSObjectResult oppsche = Schema.SObjectType.Opportunity;
    Map<string, schema.RecordtypeInfo> sObjectMap = oppsche.getRecordtypeInfosByName();    
    Id stroppRecordTypeId = sObjectMap.get('Research Awards').getRecordTypeId();
    
        Account a = new Account();
    a.Name = 'Acme';
    insert a;
    contact c= new Contact();
    c.LastName='opttest234h7';
        c.Email='opttest23gh7h6@test.com';
    c.AccountId=a.Id;
    insert c;
    
     opportunity objopp=new opportunity();
    objopp.name='testopp';
    objopp.Contract_Number__c='34567890';
    objopp.StageName='Qualification';
    objopp.closedate=system.today()+5;
    objopp.accountid=a.id;
    objopp.Full_Project_Title__c='testpms';
    objopp.Project_End_Date__c=system.today()+1;
    objopp.recordtypeid=stroppRecordTypeId;
    insert objopp;
       
     //opportunity objopp=   Opportunityrec();
     Methodology_Standard__c objmeth=  Methstanrec();
     Methodology_Standard_for_Project__c objmethstan= Methstanprojrec();
        objmethstan.Project_Name__c=objopp.id;
        objmethstan.Methodology_Standard_Name__c=objmeth.id;
        //insert objMethodology_Standard_for_Project;
        MethodologyStandardProject objMethodologyStandardProject= new MethodologyStandardProject();
        objMethodologyStandardProject.mos=objmethstan;
        Test.setCurrentPageReference(new PageReference('MethodologyStandardProject')); 
       System.currentPageReference().getParameters().put('Referer', objopp.id);
        objMethodologyStandardProject.projid=objopp.id;
        objMethodologyStandardProject.save();
        objMethodologyStandardProject.cancel();
        Methodology_Standard_for_Project__c objmethstan1= Methstanprojrec();
         objmethstan1.Project_Name__c=objopp.id;
        objmethstan1.Methodology_Standard_Name__c=objmeth.id;
        objMethodologyStandardProject.mos=objmethstan1;
        objMethodologyStandardProject.savenew();
        MethodologyStandardProject.Abbrid=objmeth.id;
        MethodologyStandardProject.GetAbbrProjName(objmeth.Abbrev__c);  
        
        
    }   
   /* static testMethod Opportunity Opportunityrec(){
        opportunity objopp=new opportunity(Name='TestOpp', Full_Project_Title__c = 'TestOpp', Project_Start_Date__c = system.TODAY(), Project_End_Date__c = system.TODAY()+1, StageName='Active', CloseDate=system.TODAY());
        insert objopp;
       
        Return objopp;
        
    }*/
    static testMethod Methodology_Standard__c Methstanrec(){
        Methodology_Standard__c objMethodology_Standard=new  Methodology_Standard__c(name='Test',Abbrev__c='Ab1');
        insert objMethodology_Standard;
       return  objMethodology_Standard;
    }
    static testMethod Methodology_Standard_for_Project__c Methstanprojrec(){
         Methodology_Standard_for_Project__c objMethodology_Standard_for_Project= new Methodology_Standard_for_Project__c();
        
       return  objMethodology_Standard_for_Project;
    }
    

}