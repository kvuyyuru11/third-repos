public class ObjectCreator {
    
    public static List<User> getUsers(Integer dbEntries, String uName)
    {
        List<User> uList = new List<User>();
        List<Profile> profList = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 20];
        Integer RandProf = Integer.valueOf(Math.floor(Math.random()*((profList.size()-1)-1+1)) + 1);
        
        for(Integer x = 0; x < dbEntries; x++)
        {
            uList.add(new User(LastName = uName + String.valueOf(x), Email = 'test@test.com', Alias = uName + String.valueOf(x), Username = uName + String.valueOf(x) +'@test.com', CommunityNickname = uName + String.valueOf(x), 
            LocaleSidKey = 'en_US', TimeZoneSidKey = 'GMT', ProfileId = profList[0].Id, LanguageLocaleKey = 'en_US', emailEncodingKey = 'UTF-8'));
        }
        
        return uList;
    }
    
    public static List<Account> getAccts(String acctName, Integer dbAccountEntries)
    {
        List<Account> acctList = new List<Account>();
        
        for(Integer x = 0; x < dbAccountEntries; x++)
        {
            acctList.add(new Account(Name = acctName + String.valueOf(x), Community__c = 'Patient'));
        }
        
        return acctList;
    }
    
    public static List<Opportunity> getOpps(Integer dbEntries, String name)
    {
        List<Opportunity> oppList = new List<Opportunity>();
        
list<user> userlist= Testdatafactory.getstandardUserTestRecords(10);  
    list<account> accntlist=   Testdatafactory.getAccountTestRecords(1);
        for(Integer x = 0; x < dbEntries; x++)
        {
            oppList.add(new Opportunity(Name = name + String.valueOf(x), Full_Project_Title__c = name + String.valueOf(x), Project_Name_off_layout__c = name + String.valueOf(x),
            Awardee_Institution_Organization__c = accntlist[0].Id, AccountId = accntlist[0].Id,  Program__c = 'Engagement Awards', PFA_Type__c = 'Targeted', PFA__c = 'Asthma', Priority_Area__c = 'Pilot Projects', 
            Project_Start_Date__c = Date.today(), Project_End_Date__c = Date.today(), Status_DEPRECATED__c = 'Awarded', Cycle__c = 'Cycle II',  Application_Number__c = 'name + String.valueOf(x)', 
            Application_Amount__c = Math.random()*100, Program_Officer__c = userlist[0].Id, Contract_Administrator__c = userlist[1].Id, Contract_Coordinator__c = userlist[2].Id,  
            PI_Name_User__c = userlist[3].Id, AO_Name_User__c = userlist[4].Id, Contract_Number__c = name + String.valueOf(x), Contracted_Budget__c = Math.random()*100, 
            Contract_Execution_Date__c = Date.today(), StageName = 'Prospecting', CloseDate = Date.today()));
        }
       
        system.debug('Oppty '+oppList);
        return oppList;
    }
    
    public static List<FGM_Base__Benchmark__c> getMilestones(String mName, Integer dbEntries)
    {
        List<FGM_Base__Benchmark__c> milList = new List<FGM_Base__Benchmark__c>();
        List<Opportunity> oppList = [SELECT Id FROM Opportunity LIMIT 20];
        Integer RandProj = Integer.valueOf(Math.floor(Math.random()*((oppList.size()-1)-1+1)) + 1);
        
        for(Integer x = 0; x < dbEntries; x++)
        {
            milList.add(new FGM_Base__Benchmark__c(Milestone_Name__c = mName + String.valueOf(x), FGM_Base__Request__c = oppList[RandProj].Id, FGM_Base__Due_Date__c = Date.today(),
            Milestone_Status__c = 'Completed', FGM_Base__Completion_Date__c = Date.today(), Milestone_Type__c = 'Recruitment', Milestone_ID2__c = mName + String.valueOf(x)));
        }
        
        return milList;
    }
    
    
    public static List<FGM_Base__Grantee_Report__c> getReports(Integer dbEntries)
    {
        List<FGM_Base__Grantee_Report__c> repList = new List<FGM_Base__Grantee_Report__c>();
        List<Opportunity> oppList = [SELECT Id FROM Opportunity LIMIT 20];
        Integer RandProj = Integer.valueOf(Math.floor(Math.random()*((oppList.size()-1)-1+1)) + 1);
        
        for(Integer x = 0; x < dbEntries; x++)
        {
            repList.add(new FGM_Base__Grantee_Report__c(FGM_Base__Request__c = oppList[RandProj].Id));
        }
        
        return repList;
    }
    
    public static List <Panel__c> getPanel()
    {
        List<Panel__c> reviewPanel = new List<Panel__c>();
        List<User> userList = [SELECT Id FROM User];  
        Integer RandUser = Integer.valueOf(Math.floor(Math.random()*((userList.size()-1)-1+1)) + 1);
        reviewPanel.add(new Panel__c(Name = 'TestPanel', MRO__c = userList[RandUser].Id,Panel_Due_Dtae__c=  Date.newInstance(2019,12,20)));
        
        return reviewPanel;
    } 
    public static List <Cycle__c> getCycle()
    {
        List<Cycle__c> reviewCycle = new List<Cycle__c>();  
        reviewCycle.add(new Cycle__c(Name = 'TestCycle', COI_Due_Date__c = system.Today()));
        
        return reviewCycle;
    } 
}