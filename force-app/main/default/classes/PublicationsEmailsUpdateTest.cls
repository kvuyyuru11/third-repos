/*
    Author     : Vijaya Kumar Sripathi
    Date       : 2nd Dec 2017
    Name       : PublicationsEmailsUpdateTest 
    Description: This is a test class for PublicationsEmailsUpdate class.
*/
@isTest
public class PublicationsEmailsUpdateTest
{
    static testMethod void Test() 
    {
        string pid=createProject();
        Publication__c objPub =new Publication__c();
        objPub.PI_Lead_Name__c='Test';
        objPub.PI_Email__c='test@gmail.com';
        objPub.AO_Name__c ='Test2';
        objPub.AO_Email__c='test@gmail.com';
        objPub.Program_Associate_Name__c= 'Test3';
        objPub.Program_Associate_Email__c='test4@gmail.com'; 
        objPub.Program_Officer_Name__c='Test5';
        objPub.Program_Officer_Email__c ='test5@gmail.com'; 
        objPub.Project_Publication_del__c=pid;
        list<Publication__c> lstpub=new list<Publication__c>();
        lstpub.add(objPub);
        insert lstpub;        
         
        PublicationsEmailsUpdate.PublicationNameEmailUpdate(lstpub);
    }
    
    public static  string createProject()
    {
        Schema.DescribeSObjectResult oppsche = Schema.SObjectType.Opportunity;
        Map<string, schema.RecordtypeInfo> sObjectMap = oppsche.getRecordtypeInfosByName();  
        Id stroppRecordTypeId = sObjectMap.get('Research Awards').getRecordTypeId();
    
        Account obj_Acc=createAccount();
        contact c= new Contact();
        c.LastName='opttest234h7';
        c.Email='opttest23gh7h6@test.com';
        c.AccountId=obj_Acc.Id;
        insert c;
        string ProgramOfficer= createUser('usrName', 'PCORI Community Partner',obj_Acc).Id;
        opportunity objopp=new opportunity();
        objopp.name='testopp';
        objopp.Contract_Number__c='34567890';
        objopp.StageName='Qualification';
        objopp.closedate=system.today()+5;
        objopp.accountid=obj_Acc.id;
        objopp.Full_Project_Title__c='testpms';
        objopp.Project_End_Date__c=system.today()+1;
        objopp.recordtypeid=stroppRecordTypeId;
        objopp.Project_Start_Date__c=system.today();
        insert objopp;
        return objopp.Id ;
    }
    public static  User createUser( string usrName, string Userprof,Account objacc) 
    {
        //string Userprof = 'CMA Operations';
        User usr;
        string usrFullName = usrName +'testInvoiceUser@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name=:Userprof];
        if(Userprof !='PCORI Community Partner')
        {   
            usr = new User(Alias = 'testUser', Email='testInvoiceUser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Test', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName=usrFullName);
            insert usr ; 
        }
        else if(Userprof =='PCORI Community Partner')
        {
            Contact cont= new Contact();
            cont.Is_User_Active__c = true;
            cont.LastName = 'Test';
            cont.AccountId = objacc.id;
            cont.Community__c = 'TBD';
            cont.Email = 'Test@aooo.com';
            insert cont;
            
            usr = new User(Alias = 'Hari', ContactId = cont.Id,Email='testInvoiceUser@testorg.com',EmailEncodingKey='UTF-8', LastName='Hari', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName=usrFullName);
            insert usr ;                
         }
         return usr ;
         //CMA Operations
         //Finance Operations
         //PCORI Community Partner
    }  
    public static  Account createAccount() 
    {
        Account a = new Account();
        a.Name = 'Acme';
        insert a;
        return a;
    }
}