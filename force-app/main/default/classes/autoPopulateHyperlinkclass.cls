Public with sharing class autoPopulateHyperlinkclass
{
    @Future
    public static void Urlpopulatemethod(set<id> contentDocumentLinkIdSet)
    {
        List<ContentDocumentLink> contentdoclinklist =  [select 
                                                         id,ContentDocument.Title,
                                                         linkedentityid,
                                                         contentdocumentid
                                                         FROM contentdocumentlink 
                                                         WHERE Id In : contentDocumentLinkIdSet];
        Set<string> Allcontentids =  new set<string>();
        set<string> contentid = new set<string>();
        set<Id> opportunityIdSet = new Set<Id>();
        map<id,contentdocumentlink> contendoclinkmap = new map<id,contentdocumentlink>();
        map<id,String> contendismap = new map<id,String>();
        List<Opportunity> OpportunityListToUpdate = new List<Opportunity>();
        list<ContentDocument> conlist = new list<ContentDocument>(); 
        map<id,contentversion> contentversionmap = new map<id,contentversion>();
        list<ContentDistribution> ContentDistributionlist = new list<ContentDistribution>();
        List<Files__c> filesList = new List<Files__c>();
        filesList  = Files__c.getAll().Values();
        for(contentdocumentlink conlin : contentdoclinklist)
        {
            string str = conlin.linkedentityid;
            string str1 = str.substring(0,3);
            if(str1 == '006'){
                for(Files__c f : filesList) 
                {
                    if(conlin.ContentDocument.Title.contains(f.Name)){
                        contendoclinkmap.put(conlin.contentdocumentid,conlin);
                        opportunityIdSet.add(conlin.linkedentityid);
                    }
                }
            }
        } 
        map<Id,Opportunity> opportunityMap = new Map<Id,Opportunity>([Select Id,Application_Download_MR__c,Combined_Online_Critique_Download__c,Summary_Statement_Download__c FROM Opportunity WHERE Id IN : opportunityIdSet]);
        for(contentversion cv : [Select Id,Title,ContentDocumentId from contentversion where ContentDocumentId in : contendoclinkmap.Keyset()])
        {
            contentversionmap.put(cv.ContentDocumentId,cv);
        }
        for(Id cdoc : contendoclinkmap.Keyset())
        {
            if(contendoclinkmap.containskey(cdoc))
            {
                if(opportunityMap.containskey(contendoclinkmap.get(cdoc).linkedentityid))
                {
                    opportunity opp = opportunityMap.get(contendoclinkmap.get(cdoc).linkedentityid);
                    
                    //Since File is inserted we will get Content Version Id's.Now its possible to insert the Content Distribution with the Content Version Id. 
                    //Insert Content Distribution inorder to genereate the DistibutionPublicURL
                    //The link autopopulates only when the opportunity fields Application_Download_MR__c,Combined_Online_Critique_Download__c,Summary_Statement_Download__c are null.
                    if(opp.Application_Download_MR__c==null || opp.Combined_Online_Critique_Download__c==null || opp.Summary_Statement_Download__c==null)
                    {
                        
                        ContentDistribution cd = new ContentDistribution();
                        if(contentversionmap.containsKey(cdoc)){
                            cd.ContentVersionId = contentversionmap.get(cdoc).Id;
                            cd.Name = contentversionmap.get(cdoc).Title.right(99);
                            system.debug('@@@@@@@@@@@@@@@'+cd.Name);
                            cd.PreferencesAllowOriginalDownload=true;
                            cd.PreferencesAllowPDFDownload=true;
                            cd.PreferencesAllowViewInBrowser=true;
                            cd.PreferencesExpires=true;
                            cd.PreferencesNotifyOnVisit=false;
                            cd.ExpiryDate=system.today()+365;
                            ContentDistributionlist.add(cd);
                        }
                    }
                }
            }
        }
        if(ContentDistributionlist.size()>0){
            insert ContentDistributionlist;
        }
        
        List<contentdistribution> contdlist = new List<contentdistribution>([select id,Name,DistributionPublicUrl,contentdocumentid from contentdistribution where contentdocumentid in : contendoclinkmap.Keyset()]);
        
        for(contentdistribution condis: contdlist)
        {
            contendismap.put(condis.contentdocumentid,condis.DistributionPublicUrl);
            Opportunity opp =opportunityMap.get(contendoclinkmap.get(condis.contentdocumentid).linkedentityid); 
            String contenttitle =contentversionmap.get(condis.contentdocumentid).Title;
            for(Files__c f : filesList) {
                
                String fieldName = f.API_Name__c;
                
                if(contenttitle.contains(f.name)   && opp.get(fieldName)==null) {
                    
                    opp.put(fieldName,contendismap.get(condis.contentdocumentid));
                }
            }
            OpportunityListToUpdate.add(opp);
        }
        update OpportunityListToUpdate;
    }
}