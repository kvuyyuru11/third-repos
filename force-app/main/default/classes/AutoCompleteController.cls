/*
* An apex page controller that helps in capturing additional information about a self registered user in the communities
*/
public without sharing class AutoCompleteController {
    //Instance fields
    
    String communityurl=Label.Portal_URL;
    string customlandingpage=Label.PCORI_Portal_custom_landing_page;
    public Boolean optoutdisp { get; set; }
    public String[] hearabout = new String[]{};
        public String hearother{get;set;}
    public boolean field1 {get; set;}
    public Boolean emplist { get; set; }
    public Boolean curposdisable { get; set; }
    public Boolean searchdisable { get; set; }    
    public Boolean othergenderpopup { get; set; }  
    public Boolean otherracepopup { get; set; } 
    public Boolean otherotherpopup { get; set; } 
    public Boolean statelistpopup { get; set; }
    public Boolean statetextpopup { get; set; }
    public Boolean empstatelistpopup { get; set; }
    public Boolean empstatetextpopup { get; set; }
    public String curposstyle { get; set; }
    public String empzipstyle { get; set; }
    public String empstatetextstyle { get; set; }
    public String empcitystyle { get; set; }
    public String empstreetstyle { get; set; }
    public String empcurposstyle { get; set; }
    public String empnamestyle { get; set; }
    public String zipstyle { get; set; }
    public String statestyle { get; set; }
    public String citystyle { get; set; }
    public String streetstyle { get; set; }
    public String phonestyle { get; set; }
    public String searchtermstyle { get; set; }
    public string hearaboutotherstyle{get;set;}
    public String empcountry { get; set; }
    public string statelist {get; set;}
    public string statetext {get; set;}
    public String empzip { get; set; }
    public String empzipcode { get; set; }
    public String empcity { get; set; }
    public String empstreet { get; set; }
    public String empcurrentpos { get; set; }
    public String empname { get; set; }
    public String empstatelist {get; set; }
    public String empstatetext { get; set;}
    public String mailingaddress1 { get; set; }
    public String name { get; set; }
    public String federalemployee {get; set;}  
    public String ArmedServices {get; set;}   
    public String employerchange { get; set; }   
    public String race{get;set;}
    public String latino{get;set;}
    public String country{get;set;}
    public String gender{get;set;}
    public String salutation{get;set;}
    public String phonenumber { get; set; }
    public String age {get; set;}
    public String yearofbirth {get; set;}
    public String communities {get; set;}
    public String city {get;set;}
    public String street {get;set;}
    public String zip{get;set;}
    public String searchTerm {get; set;}
    public String Organization {get; set;}
    public String title {get; set;}
    public String othergender {get; set; }
    public String otherrace {get; set; }
    public String otherother {get; set; }
    public String dept {get; set;}
    //public string radio{get;set;}
    public String empdept {get; set;}
    public String unempAcc;
    public String optoutAcc;
    public boolean bOthers{get;set;}
    public String selection4;
    public string programregister{get;set;}
    public Contact__c AmbCustomSettings;
    public String CommunityLink;
    public String APLink;
    public String MRLink;
    public String AmbLink;
    public String EALink;
    public String RALink; 
    public String P2PLink;
    
    //Queries the Account Record Type Id for Standard Salesforce Account to get only these Record Types in the Account Search Results.
    RecordType RecType = [Select Id From RecordType  Where SobjectType = 'Account' and DeveloperName = 'Standard_Salesforce_Account'];
    Id devRecordTypeId=RecType.ID;
    
    public String[] gethearabout () {
        return hearabout ;
    }
    
    public void sethearabout(String[] hearabout ) {
        this.hearabout  = hearabout ;
    }
    
    //getOptions4 method adds the values to the corresponding question How_did_you_hear_about_PCORI.
    public List<SelectOption> getOptions4() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Joined a PCORI email list','Joined a PCORI email list'));
        options.add(new SelectOption('Visited PCORI\'s website','Visited PCORI\'s website'));
        options.add(new SelectOption('Participated in applicant training','Participated in applicant training'));
        options.add(new SelectOption('Watched a PCORI webinar','Watched a PCORI webinar'));
        options.add(new SelectOption('Attended a PCORI sponsored event','Attended a PCORI sponsored event in-person'));
        options.add(new SelectOption('Attended event where PCORI was featured','Attended an event where PCORI was featured'));
        options.add(new SelectOption('Met with PCORI staff','Met with PCORI staff'));
        options.add(new SelectOption('Met with a PCORI Ambassador','Met with a PCORI Ambassador'));
        options.add(new SelectOption('Applied to be a PCORI Merit Reviewer','Applied to be a PCORI Merit Reviewer'));
        options.add(new SelectOption('Served as a PCORI Merit Reviewer','Served as a PCORI Merit Reviewer'));
        //options.add(new SelectOption('Applied to be a PCORI reviewer','Applied to be a reviewer of PCORI funding applications')); Removed
        options.add(new SelectOption('Applied for PCORI research funding','Applied for PCORI research funding'));
        options.add(new SelectOption('Received PCORI research funding','Received PCORI research funding'));
        options.add(new SelectOption('Participated in a PCORI Advisory Panel','Participated in a PCORI advisory panel'));
        options.add(new SelectOption('Contacted by PCORI Merit Review Officer','Contacted by PCORI Merit Review Officer'));
        options.add(new SelectOption('None of the above','None of the above'));
        options.add(new SelectOption('Other','Other (please specify) Please enter another value for this selection'));
        
        return options;
    }
    Public Id ACCID;
    Public Id strAccRecordTypeId;
    public AutoCompleteController()
    {
        
        
        //contructor for the AutoCompleteController
        
        employerchange='Employer found';
        empstatelistpopup=true;  
        statelistpopup=true;     
        othergenderpopup= false;
        otherotherpopup=false;  
        otherracepopup=false;
        optoutdisp =false;
        curposdisable = false;
        searchdisable = false;
        emplist=false;
        bOthers=false;
        hearaboutotherstyle='';
        //Custom Settings Links
        //Links from Custom Settings
        AmbCustomSettings = Contact__c.getValues('Program Links');
        APLink = AmbCustomSettings.Advisory_Panel_Link__c;
        MRLink = AmbCustomSettings.Merit_Reviewer_Link__c;
        AmbLink = AmbCustomSettings.Ambassador_Link__c;
        EALink = AmbCustomSettings.Engagement_Awards_Link__c;
        RALink = AmbCustomSettings.Research_Awards_Link__c;
        CommunityLink = AmbCustomSettings.Sandbox_Domain__c;
        P2PLink = AmbCustomSettings.P2P_Link__c;
        
    } 
    
    // JS Remoting action called when searching for a employer name
    @RemoteAction
    public static List<Account> searchMovie(String searchTerm) {
        RecordType RecType = [Select Id From RecordType  Where SobjectType = 'Account' and DeveloperName = 'Standard_Salesforce_Account'];
        Id devRecordTypeId=RecType.ID;
        List<Account> org = Database.query('Select Id, Name, RecordTypeId, isPartner from Account where name like \'%' + String.escapeSingleQuotes(searchTerm) + '%\' AND  RecordTypeID =:devRecordTypeId');
        return org;
    }
    
    
    //list of countries to use for a dropdown list
    String[] countryList= new String[]{'USA',
        'Afghanistan','Aland Islands','Albania','Algeria','American Samoa','Angola','Anguilla','Antarctica','Antigua and Barbuda','Argentina','Armenia','Aruba',
        'Australia','Austria','Azerbaijan','Bahamas','Bahrain','Bangladesh','Barbados','Belarus','Belgium','Belize','Benin','Bermuda','Bhutan','Bolivia, Plurinational State of','Bonaire, Sint Eustatius and Saba',
        'Bosnia and Herzegovina','Botswana','Bouvet Island','Brazil','British Indian Ocean Territory','Brunei Darussalam','Bulgaria','Burkina Faso','Burundi',
        'Cambodia','Cameroon','Canada','Cape Verde','Cayman Islands','Central African Republic','Chad','Chile','China','Chinese Taipei','Christmas Island','Cocos (Keeling) Islands',
        'Colombia','Comoros','Congo','Congo, the Democratic Republic of the','Cook Islands','Costa Rica','Cote d \'Ivoire','Croatia','Cuba','Curaao','Cyprus',
        'Czech Republic','Denmark','Djibouti','Dominica','Dominican Republic','Ecuador','Egypt','El Salvador','Equatorial Guinea','Eritrea','Estonia','Ethiopia',
        'Falkland Islands (Malvinas)','Faroe Islands','Fiji','Finland','France','French Guiana','French Polynesia','French Southern Territories','Gabon',
        'Gambia','Georgia','Germany','Ghana','Gibraltar','Greece','Greenland','Grenada','Guadeloupe','Guatemala','Guernsey','Guinea','Guinea-Bissau','Guyana',
        'Haiti','Heard Island and McDonald Islands','Holy See (Vatican City State)','Honduras','Hungary','Iceland','India','Indonesia','Iran, Islamic Republic of','Iraq',
        'Ireland','Isle of Man','Israel','Italy','Jamaica','Japan','Jersey','Jordan','Kazakhstan','Kenya','Kiribati','Korea, Democratic People\'s Republic of','Korea, Republic of',
        'Kuwait','Kyrgyzstan','Lao People\'s Democratic Republic','Latvia','Lebanon','Lesotho','Liberia','Libyan Arab Jamahiriya','Liechtenstein','Lithuania','Luxembourg',
        'Macao','Macedonia, the former Yugoslav Republic of','Madagascar','Malawi','Malaysia','Maldives','Mali','Malta','Martinique','Mauritania','Mauritius',
        'Mayotte','Mexico','Moldova, Republic of','Monaco','Mongolia','Montenegro','Montserrat','Morocco','Mozambique','Myanmar','Namibia','Nauru','Nepal',
        'Netherlands','New Caledonia','New Zealand','Nicaragua','Niger','Nigeria','Niue','Norfolk Island','Norway','Oman','Pakistan','Palestinian Territory, Occupied',
        'Panama','Papua New Guinea','Paraguay','Peru','Philippines','Pitcairn','Poland','Portugal','Qatar','Reunion','Romania','Russian Federation','Rwanda',
        'Saint BarthŽlemy','Saint Helena, Ascension and Tristan da Cunha','Saint Kitts and Nevis','Saint Lucia','Saint Martin (French part)','Saint Pierre and Miquelon','Saint Vincent and the Grenadines',
        'Samoa','San Marino','Sao Tome and Principe','Saudi Arabia','Senegal','Serbia','Seychelles','Sierra Leone','Singapore','Sint Maarten (Dutch part)','Slovakia',
        'Slovenia','Solomon Islands','Somalia','South Africa','South Georgia and the South Sandwich Islands','South Sudan','Spain','Sri Lanka',
        'Sudan','Suriname','Svalbard and Jan Mayen','Swaziland','Sweden','Switzerland','Syrian Arab Republic','Tajikistan','Tanzania, United Republic of','Thailand','Timor-Leste','Togo','Tokelau','Tonga','Trinidad and Tobago',
        'Tunisia','Turkey','Turkmenistan','Turks and Caicos Islands','Tuvalu','Uganda','Ukraine','United Arab Emirates','United Kingdom','Uruguay',
        'Uzbekistan','Vanuatu','Venezuela, Bolivarian Republic of','Viet Nam','Virgin Islands, British','Wallis and Futuna','Western Sahara','Yemen','Zambia','Zimbabwe'
        };
            //list of states for USA
            String[] usaStatesList= new String[]{'Alabama',
                'Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District of Columbia','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada',
                'New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','West Virginia','Wisconsin','Wyoming'};
                    
                    //method to render an output label when user selects opt out
                    public PageReference empchange() {
                        searchtermstyle='';
                        curposstyle='';
                        if(employerchange=='Opt out')
                        {
                            optoutdisp=true;  
                            searchtermstyle=Label.ErrormsgStyling;
                        }
                        else{
                            optoutdisp=false;
                        }         
                        return null;
                    }
    // method to render input field when user selects other gender
    public PageReference showgender() {
        if(gender=='Another Gender'){
            othergenderpopup=true;}
        else
        {othergenderpopup=false;
        }
        return null;
    }
    // method to render input field when user selects other race
    public PageReference showrace() {
        if(race=='Other Race'){
            otherracepopup=true;
        }
        else
        {otherracepopup=false;
        }
        return null;
    }
    
    public PageReference showother() {
        for(string s:hearabout){
            if(s.contains('Other')){
                
                otherotherpopup=true;
                break;
            }
            else
            {otherotherpopup=false;
            }
        }
        return null;
    }
    
    
    //to render a input textfield or a dropdown list based on the country selected by the user in the AutoComplete page
    public PageReference showstatemain() {
        if(country=='USA'){
            statelistpopup=true;
            statetextpopup=false;
        }
        else
        {
            statelistpopup=false;
            statetextpopup=true;
        }
        return null;
    }
    
    //to render a input textfield or a dropdown list based on the country selected by the user in the SaveEmployerdetails page
    public PageReference showstate() {
        if(empcountry=='USA'){
            empstatelistpopup=true;
            empstatetextpopup=false;
        }
        else{
            empstatelistpopup=false;
            empstatetextpopup=true;
        }
        return null;
    }
    /* To save the Employer details entered in the SaveEmployerDetails page and redirect to AccountSaveSuccess page
*/
    public PageReference saveemp() {
        Integer x,flag,flag1;
        empzipstyle ='';
        empstatetextstyle='';
        empcitystyle ='';
        empstreetstyle='';
        empcurposstyle='';
        empnamestyle='';
        
        //Custom validations for fields on SaveEmployerDetails page
        if(empname==null || empname==''){
            empnamestyle =Label.ErrormsgStyling;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));
            x=1;
        }
        if(empcurrentpos==''|| empcurrentpos==null){
            empcurposstyle = Label.ErrormsgStyling;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));
            x=1;
        }
        if(empstreet== '' || empstreet==null){
            empstreetstyle = Label.ErrormsgStyling;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));
            x=1;} 
        if(empcity==null|| empcity==''){
            empcitystyle = Label.ErrormsgStyling;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));
            x=1;}
        if(CheckValidZip(empzip)==false || empzip==''){
            empzipstyle = Label.ErrormsgStyling;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));
            x=1;}
        
        if(empcountry!='USA')
        {
            if(empstatetext==''||empstatetext==null)
            {    
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));
                empstatetextstyle=Label.ErrormsgStyling;
                x=1;
            }
        }
        
        //  if(programregister==''|| programregister==Null){
        //  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));      
        //  x=1;
        // }
        
        List<Account> a = [select name from account where name=:empname];
        
        if(!a.isempty()){
            flag=1;
        }
        else{
            flag1=1;}
        
        
        if(flag==1){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Employer already exists.  Please close this window and enter this Employer Name in the Employer Name Lookup field on the main page.'));
            x=1;    
        }
        if(x==1)
        {
            return null;
        }
        else   { 
            Id myId2=userinfo.getUserId();
            user u1=[select contactid from User where id=:myId2];
            Contact cnt1=[select accountid,Current_Position_or_Title__c,Department,current_employer__c from Contact where id=:u1.ContactId];
            account acc = new account();
            acc.name = empname;
            acc.Website = Label.Account_Website;
            acc.Community__c=Label.Account_Community;
            acc.BillingStreet=empstreet;
            acc.BillingCity=empcity;
            if(empcountry=='USA')
                acc.BillingState=empstatelist;
            else
                acc.BillingState=empstatetext;
            acc.BillingPostalCode=empzip;
            acc.BillingCountry=empcountry;
            acc.ownerId=Label.OwnerId_Account;
            acc.New_Account_created_by_Community__c=true;
            database.insert(acc);
            cnt1.AccountId=acc.id;
            cnt1.Current_Position_or_Title__c=empcurrentpos;
            cnt1.Department=empdept;
            System.debug('emp dept*****'+empdept);      
            cnt1.Current_Employer__c=empname;
            database.update(cnt1);     
            System.debug('cnt dept*****'+cnt1);
            
            return new PageReference('/apex/AccountSaveSuccess');
        }
    }
    /* to update the contact created on self registration page with the additional information on the autocomplete page 
* to assign an account to the contact
*/
    public PageReference save()
    {
        
        phonestyle='';
        zipstyle='';
        statestyle ='';
        citystyle='';
        streetstyle='';
        searchtermstyle='';
        curposstyle='';
        Integer x =0;
        
        //custom validations
        if(programregister==''|| programregister==Null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));      
            x=1;
        }
        
        if(phonenumber ==null || CheckValidPhone(phonenumber)==false){
            phonestyle= Label.ErrormsgStyling;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please input a 10-digit phone number including hyphens in the displayed format – i.e., 123-456-7890.'));
            x=1;
        }
        if(street== '' || street==null){
            streetstyle= Label.ErrormsgStyling;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));
            x=1;
        }
        if(city==null|| city==''){
            citystyle= Label.ErrormsgStyling;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));
            x=1;
        }    
        if(CheckValidZip(zip)==false || zip==''){
            zipstyle= Label.ErrormsgStyling; 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));
            x=1;
        }
        if(salutation==null||salutation==''){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));
            x=1; 
        }
        
        if(country!='USA')
        {
            if(statetext==''||statetext==null)
            {
                statestyle= Label.ErrormsgStyling;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));
                x=1;
            }
        }
        //if(ArmedServices==''|| ArmedServices==Null){
        // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));      
        // x=1;
        // }
        // 
        if(hearabout.size()==0){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));      
            x=1;
        }
        boolean isValOther = false;
        for(String s : hearabout){
            
            if(s.contains('Other') && (otherother==null || otherother=='')){
                isValOther = true;
                break;
            }
        }
        if(isValOther){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please put other value!')); 
            hearaboutotherstyle=Label.ErrormsgStyling;
            x=1;
        }
        if(federalemployee==null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));      
            x=1;
        }
        if(communities==null)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));      
            x=1;
        }
        
        
        if(employerchange=='Employer found') 
        {
            List<Account> a = [select name from account];
            Integer flag,flag1;
            
            for(Account a1: a)
            {
                if(string.valueof(a1).contains(searchterm))
                {
                    flag=1;
                }
                else{
                    // flag1=1;
                }
            }
            //custom validation for Employer search and employer current position
            
            if(searchTerm==null || searchterm==''||flag!=1){
                searchtermstyle= Label.ErrormsgStyling;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));
                x=1;
            }
            if(title==null || title==''){
                curposstyle=Label.ErrormsgStyling;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));
                x=1;
            }
        }
        
        else{
            //check for employer lookup   
        }  
        
        if(x==1){
            return null;
        }        
        //the below code is to get the contact created on the self registration page and update it with the information entered on AutoComplete page     
        
        Id MyId=userinfo.getUserId();
        User u=[select contactId from User where id=:MyId];
        Contact c=[select Id,Group__c,Program_Applying__c,Subgroup__c,Community__c,How_did_you_hear_about_PCORI_Other_c__c,Previous_involvement_with_PCORI__c,Phone,Reviewer_Status__c,Street_Address__c,Current_Position_or_Title__c,Reviewer_Role__c,Current_Employer__c,Title, City__c, State_Province__c, Country__c, Zip__c, Gender__c, Race__c, Hispanic_Latino__c, Age__c, Registered__c,Department,Armed_Services__c, Salutation from Contact where id=:u.ContactId];  
        if(gender=='Another Gender')
            c.Gender__c=othergender;    
        else
            c.Gender__c=gender; 
        if(salutation=='None')
            c.Salutation = '';
        else
            c.Salutation=salutation;
        if(race=='Other Race')
            c.Race__c=otherrace;
        else
            c.Race__c=race;
        c.Hispanic_Latino__c = latino;
        c.Phone=phonenumber;
        c.MailingStreet=street;
        c.MailingCity=city;       
        c.MailingCountry=country;
        for(integer i=0;i<hearabout.size();i++)
        {
            if(selection4==NULL) selection4=string.valueOf(hearabout[i]);
            
            Else selection4=selection4 +';'+ string.valueOf(hearabout[i]);
            
        }
        
        c.Previous_involvement_with_PCORI__c=selection4;
        c.How_did_you_hear_about_PCORI_Other_c__c=otherother;
        
        
        /*
if(communities=='Patient' && (c.Community__c=='Scientist'||c.Community__c=='Stakeholder'||c.Community__c=='Other'||c.Community__c=='TBD')){
c.Group__c='';
c.Subgroup__c='';
}
if(communities=='Scientist' && (c.Community__c=='Patient'||c.Community__c=='Stakeholder'||c.Community__c=='Other'||c.Community__c=='TBD')){
c.Group__c='';
c.Subgroup__c='';
}
if(communities=='Stakeholder' && (c.Community__c=='Patient'||c.Community__c=='Scientist'||c.Community__c=='Other'||c.Community__c=='TBD')){
c.Group__c='';
c.Subgroup__c='';
}
*/
        
        //Logic to update Group based on new Community values
        if(communities =='Patient/Consumer'){
            c.Community__c = 'Patient';
            c.Group__c='Patient/Consumer';           
        }
        if(communities=='Caregiver/Family Member of Patient'){
            c.Community__c = 'Patient';
            c.Group__c='Caregiver/Family Member of Patient';
        }
        if(communities=='Patient/Caregiver Advocacy Organization'){
            c.Community__c = 'Patient';
            c.Group__c='Patient/Caregiver Advocacy Organization';
        }
        if(communities=='Researcher'){
            c.Community__c = 'Scientist';
            c.Group__c='Health Research';
        }
        if(communities=='Clinician'){
            c.Community__c = 'Stakeholder';
            c.Group__c='Clinician';
        }
        if(communities=='Hospital/Health System'){
            c.Community__c = 'Stakeholder';
            c.Group__c='Hospital/Health System';
            //system.debug('Community Selected'+ communities);
            //system.debug('Community Selected Group'+ c.Group__c);
        }
        if(communities=='Purchaser'){
            c.Community__c = 'Stakeholder';
            c.Group__c='Purchaser';
        }
        if(communities=='Payer'){
            c.Community__c = 'Stakeholder';
            c.Group__c='Payer';
        }
        if(communities=='Industry'){
            c.Community__c = 'Stakeholder';
            c.Group__c='Industry';
        }
        if(communities=='Policy Maker'){
            c.Community__c = 'Stakeholder';
            c.Group__c='Policy Maker';
        }
        if(communities=='Training Institution'){
            c.Community__c = 'Stakeholder';
            c.Group__c='Training Institution';
        }
        
        
        
        
        //c.Reviewer_Status__c=Label.Applicant;
        c.MailingPostalcode=zip;
        c.Age__c=age;
        c.Program_Applying__c=programregister;
        c.Registered__c=true;
        
        if(statetext==''||statetext==null)
            c.MailingState=statelist;       
        else
            c.MailingState=statetext;
        c.Federal_Employee__c=federalemployee;
        c.Armed_Services__c=ArmedServices;    
        
        
        
        c.Year_of_Birth__c = yearofbirth; 
        
        if(searchterm!=null && employerchange=='Employer found')
        {
            account act=[select Id,name,isPartner from Account where name=:searchTerm AND  RecordTypeID =:devRecordTypeId limit 1]; 
            if(act.isPartner==False){
                system.debug('this is inside the condition'+ act.isPartner);
                act.isPartner=True;
                database.update(act);
            }   
            c.AccountId=act.ID;
            c.Current_Position_or_Title__c=title;
            c.Current_Employer__c=act.name;
            c.Department=dept;
        }    
        if(employerchange=='Unemployed'){    
            if(c.community__c!=null||c.community__c!=''){
                if(c.Community__c=='Scientist'){
                    account act=[select Id,Name from Account where name=:Label.Scientist_Unassociated_Individuals_Account];     
                    c.AccountId=act.ID;     
                    
                }
                if(c.Community__c=='Patient'){
                    account act=[select Id,Name from Account where name=:Label.Patient_Unassociated_Individuals_Account];     
                    c.AccountId=act.ID;     
                    
                }
                if(c.Community__c=='Other Stakeholder'){
                    account act=[select Id,Name from Account where name=:Label.Stakeholder_Unassociated_Individuals_Account];     
                    c.AccountId=act.ID;     
                    
                }
            }               
            c.Current_Employer__c='';
            c.Department='';
            c.Current_Position_or_Title__c='';
        }
        
        if(employerchange=='Opt out'){
            optoutAcc=Label.Opt_Out_Account;
            account ac=[select Id,name from Account where name=:Label.Opt_Out_Account]; 
            c.AccountId=ac.Id;
            c.Current_Employer__c='';
        }
        
        database.update(c);
        //return new PageReference(communityUrl + customlandingpage) ;
        if(programregister.equalsIgnoreCase('Ambassador Program')) 
        { 
            
            return new PageReference(AmbLink);
        }
        else if (programregister.equalsIgnoreCase('Advisory Panel')) {
            AutoCompleteControllerHelper.assignAdvisoryPanelPermissionSet(MyId);
            return new PageReference(CommunityLink + '/s');
        } 
        else if (programregister.equalsIgnoreCase('Merit Reviewer')) {
            AutoCompleteControllerHelper.assignMeritReviewPermissionSet(MyId);
            return new PageReference(CommunityLink + '/s');
        }
        else if (programregister.equalsIgnoreCase('Engagement Awards')) {
            AutoCompleteControllerHelper.assignEngagementAwardsPermissionSet(MyId);
            return new PageReference(CommunityLink + '/s'); 
        }
        else if (programregister.equalsIgnoreCase('Research Awards')) {
            AutoCompleteControllerHelper.assignResearchAwardsPermissionSet(MyId);
            return new PageReference(CommunityLink + '/s');
        }
        else if (programregister.equalsIgnoreCase('Pipeline to Proposal Awards')) {
            return new PageReference(CommunityLink + '/s');
       }
        else {      
            return null;
        } 
    }       
    
    
    
    
    
    
    
    
    //To validate the zip code field to accept numbers only
    public static Boolean CheckValidZip(String sZip) {
        return Pattern.matches('\\w*',sZip);
    }
    // To validate the phone number field to accept 10 numbers only
    public static Boolean CheckValidPhone(String sphone) {
        //return Pattern.matches('\\d{10}',sphone);
        return Pattern.matches('\\d{3}-\\d{3}-\\d{4}',sphone);
    }
    
    public List<SelectOption> getItemsGender() {
        List<SelectOption> options = new List<SelectOption>(); 
        //options.add(new SelectOption('Male','Male')); 
        // options.add(new SelectOption('Female','Female'));
        //options.add(new SelectOption('Other Gender','Other Gender'));  
        Schema.DescribeFieldResult fieldResult = Contact.Gender__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }    
        return options; 
    } 
    public List<SelectOption> getItemsSalutation() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Mr.','Mr.')); 
        options.add(new SelectOption('Ms.','Ms.'));
        options.add(new SelectOption('Mrs.','Mrs.'));
        options.add(new SelectOption('Dr.','Dr.'));
        options.add(new SelectOption('Prof.','Prof.'));
        options.add(new SelectOption('Mx.','Mx.'));
        options.add(new SelectOption('None','None'));
        //  Schema.DescribeFieldResult fieldResult = Contact.Salutation.getDescribe();
        // List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        // for( Schema.PicklistEntry f : ple)
        
        // {
        
        // options.add(new SelectOption(f.getLabel(), f.getValue()));
        // }    
        return options; 
    } 
    
    public List<SelectOption> getprogramoptions() {
        List<SelectOption> options = new List<SelectOption>(); 
        
        Schema.DescribeFieldResult fieldResult = Contact.Program_Applying__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }    
        return options; 
    } 
    
    
    public List<SelectOption> getItemsRace() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('White','White')); 
        options.add(new SelectOption('Black or African American','Black or African American')); 
        // options.add(new SelectOption('Hispanic, Latino or Spanish Origin','Hispanic, Latino or Spanish Origin')); 
        options.add(new SelectOption('American Indian or Alaskan Native','American Indian or Alaskan Native')); 
        options.add(new SelectOption('Native Hawaiian or other Pacific Islander','Native Hawaiian or other Pacific Islander')); 
        options.add(new SelectOption('Asian','Asian')); 
        options.add(new SelectOption('Other Race','Other Race')); 
        return options;
    }
    public List<SelectOption> getItemsLatino() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Yes','Yes')); 
        options.add(new SelectOption('No','No'));  
        return options;
    }
    
    public List<SelectOption> getItemsYearOfBirth() {
        List<SelectOption> options = new List<SelectOption>();
        for (Integer i = 1900; i <= System.today().year() - 12; i++)
        {
            options.add(new SelectOption('' + i, '' + i));
        }
        
        return options;
    }
    
    
    public List<SelectOption> getItemsemployer() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Employer found','Employer found'));
        options.add(new SelectOption('Employer not found','Employer not found'));
        options.add(new SelectOption('Unemployed','Unemployed'));         
        options.add(new SelectOption('Opt out','Opt out'));      
        return options;
    }
    
    
    public List<SelectOption> getItemsCountry(){
        List<SelectOption> options = new List<SelectOption>();
        
        for(String c:countryList){
            options.add(new SelectOption(c,c));
        }
        return options;
        
    }
    public List<SelectOption> getItemsState(){
        List<SelectOption> options = new List<SelectOption>();
        for(String c:usaStatesList){
            options.add(new SelectOption(c,c));
        }
        return options;
    }
    
    
    public List<SelectOption> getItemsArmedServices(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('No','No'));
        options.add(new SelectOption('Yes; Active Duty','Yes; Active Duty'));
        options.add(new SelectOption('Yes; Reservist','Yes; Reservist'));
        options.add(new SelectOption('Yes; Veteran','Yes; Veteran'));
        options.add(new SelectOption('Yes; Family/Caregiver of Armed Services Member','Yes; Family/Caregiver of Armed Services Member'));
        return options;
    }
    
    /* public pagereference Visible1() {
if(FederalEmployee=='Yes'){
field1=True;
}
else{
field1=False;
}
return null;
}*/
    
    public List<SelectOption> getItemsFederalEmployee(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Yes','Yes'));
        options.add(new SelectOption('No','No'));
        return options;
    }
    public List<SelectOption> getItemsCommunity(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Patient/Consumer','Patient/Consumer'));
        options.add(new SelectOption('Caregiver/Family Member of Patient','Caregiver/Family Member of Patient'));
        options.add(new SelectOption('Patient/Caregiver Advocacy Organization','Patient/Caregiver Advocacy Organization'));
        options.add(new SelectOption('Researcher','Researcher'));
        options.add(new SelectOption('Clinician','Clinician'));
        options.add(new SelectOption('Hospital/Health System','Hospital/Health System'));
        options.add(new SelectOption('Purchaser','Purchaser'));
        options.add(new SelectOption('Payer','Payer'));
        options.add(new SelectOption('Industry','Industry'));
        options.add(new SelectOption('Policy Maker','Policy Maker'));
        options.add(new SelectOption('Training Institution','Training Institution'));
        
        return options;
    }
}