@isTest
public class FluxxCreateLOIReviewTest {
    static testMethod void fluxInsertLOIs() {
        //Insert single record
            Fluxx_LOI__c loi = new Fluxx_LOI__c(Fluxx_Request_Id__c = 'FC14-1234-56789',
                                            PFA__c = 'IHS',
                                            PFA_Type__c='Broad',
                                            Project_Title__c = 'Improving Healthcare Systems');
            insert loi;
        
        /*
            AggregateResult[] revCnt = [select count(id) cnt from Fluxx_LOI_Review_Form__c 
                                    where Salesforce_LOI_Number__c = :loi.id];
            System.assertEquals(2, revCnt[0].get('cnt'));   //confirm 2 review records were created                
        */
            //Insert 201 LOI records
     
            List<Fluxx_LOI__c> lois = new List<Fluxx_LOI__c>();
            integer i = 1;
            integer j = 10000;
            string reqId;
            do {
                reqId = 'FC14-1234-' + j.format();
                Fluxx_LOI__c floi = new Fluxx_LOI__c(Fluxx_Request_Id__c = reqId,                                         
                                            PFA__c = 'APDTO',
                                            PFA_Type__c='Targeted',
                                            Targeted_PFA__c='Opoids',       
                                            Project_Title__c = 'Assessment of Prevention Diagnostic and Treatment Options');
                lois.add(floi);               
                i++;   
            } while (i < 202);
            insert lois;

        /*        
            AggregateResult[] revCnt1 = [select count(id) cnt1 from Fluxx_LOI_Review_Form__c 
                                    where   PFA__c = 'APDTO' and
                                            PFA_Type__c='Targeted' and
                                            Targeted_PFA__c='Opoids'
                                   ];
            System.assertEquals(402, revCnt1[0].get('cnt1'));   //confirm 402 review records were created
            
        */               
    } 
}