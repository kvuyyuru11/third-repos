/*
* Class : ClasstohandleContactinfutureTest
* Author : Kalyan Vuyyuru
* Version History : 1.0
* Creation : 8/17/2017
* Last Modified By: Kalyan Vuyyuru
* Last Modified Date: 8/21/2017
* Modification Description: Fully completed the test class to cover ClasstohandleContactinfuture class and checkuseractive trigger.
* Description : This Class ensures that there is 100% coverage for ClasstohandleContactinfuture class and checkuseractive trigger.
*/
@isTest
public class ClasstohandleContactinfutureTest {
    public static testMethod void TestMethod1(){
        
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt;
        acnt.IsPartner=true;
        update acnt;
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA',email='bhm2345ytu@yopmail.com');  
        insert con;  
        User user = new User(alias = 'test123', email='bhm2345ytu@yopmail.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con.Id,
                             timezonesidkey='America/New_York', username='bhm2345ytu@yopmail.com');
        insert user;
        system.runAs(sysAdminUser){
            test.startTest();
            user.Username='bhm23456ytu@yopmail.com';
            update user;
            user.Email='bhm111345ytu@yopmail.com';
            update user;
            user.IsActive=false;
            update user;
            test.stopTest();
        }
    }
    
}