@isTest
    public class ProjectInvoiceCalTriggerLogic_Test {

 @testSetup
  private static void testDataCreation() {
      KeyValueStore__c kvs = new KeyValueStore__c(Name='ProjectInvoiceCalculation',Boolean__c=True);
      insert kvs;
      KeyValueStore__c kvsili = new KeyValueStore__c(Name='InvoiceLineItemTrigger',Boolean__c=True);
      insert kvsili;
      List<Account> accts = TestDataFactory.getAccountTestRecords(1);
      List<User> users = TestDataFactory.getpartnerUserTestRecords(1, 3);
      List<User> internalUsers = TestDataFactory.getstandardUserTestRecords(3);
      list<opportunity> opp=TestDataFactory.getresearchawardsopportunitytestrecords(1,2,10);
      
      Id devRecordTypeId = Schema.SObjectType.Contract_Checklist__c.getRecordTypeInfosByName().get('Finance Checklist').getRecordTypeId();
      Contract_Checklist__c cc = new Contract_Checklist__c(Contract__c=opp[0].Id, RecordTypeId=devRecordTypeId);
      insert cc;
      Project_Invoice_Calculation__c pic = new Project_Invoice_Calculation__c(Project__c=opp[0].Id,Indirect_Costs_TRP_Cumulative__c = 1000.00);
      insert pic;
      
      KeyValueStore__c kvsinv = new KeyValueStore__c(Name='InvoiceTrigger',Boolean__c=True);
      insert kvsinv;
      
      Invoice__c inv = new Invoice__c(Project__c=opp[0].Id, Invoice_Name__c = 'testInvoice', Start_of_Billing_Period__c = System.today() +1,
                       End_of_Billing_Period__c = System.today() +32, Invoice_Status_External__c = 'Draft', Invoice_Status_Internal__c = 'Finance Review', 
                       Grand_Total_Total_Research_Period_Budget__c= 1.00, Grand_Total_Supplemental_Funding_Budget__c= 1.00, Grand_Total_Peer_Review_Budget__c= 1.00,
                       Attachment__c = true);
      insert inv;
      Invoice_Line_Item__c ili = new Invoice_Line_Item__c(Invoice__c=inv.Id,PIC__c=pic.Id,Line_Item_Type__c='Research Period');
      insert ili;
  }
  
  private static testMethod void testforSingleRecord () {
      
      List<Contract_Checklist__c> ccList = [Select Id,Cumulative_Amount__c,De_Obligated_Amount__c from Contract_Checklist__c];
      List<Project_Invoice_Calculation__c > picList = [Select Id, Indirect_Costs_TRP_Cumulative__c, Indirect_Costs_SUP_Cumulative__c, Salary_SUP_Budget__c from Project_Invoice_Calculation__c];
      List<Invoice_Line_Item__c> iliList = [Select Id, Line_Item_Type__c,Invoice__c from Invoice_Line_Item__c];
      picList[0].Indirect_Costs_SUP_Cumulative__c = 100;
      picList[0].Salary_SUP_Budget__c = 100;    
      test.startTest();
      update picList[0];
      
      iliList[0].Line_Item_Type__c = 'Supplemental Funding';
      update iliList[0];
      picList[0].Salary_SUP_Budget__c = 101;
      update picList[0];
      iliList[0].Line_Item_Type__c = 'Peer Review';
      update iliList[0];
      picList[0].Salary_SUP_Budget__c = 102;
      update picList[0];
      test.stopTest();
      //system.assertEquals(ccList[0].Cumulative_Amount__c, 1000.00);
  } 

}