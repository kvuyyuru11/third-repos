/****************************************************************************************************
*Description:           Test unit for ap_Controllerpage1
*                       
*
*Required Class(es):    N/A
*
*Organization: PCORI / Accenture
*Revision   |Date       |Author             |AdditionalNotes
*====================================================================================================
*   1.00     1/10/2016    Daniel Haro          Inital version
*  
*****************************************************************************************************/  
 


@isTest
private  with sharing class AP_Controller1Test {
public static testMethod void myTest(){
  PageReference pageRef = new pagereference(label.AP_Application_Start); //replace with your VF page name
  Test.setCurrentPage(pageRef);
   Attachment  myAttachment =new Attachment();
   Contact  contact1;
  // String profileId = Label.CommunityAdminAccount;
         Profile Profile1 = [SELECT Id, Name FROM Profile WHERE Name = 'PCORI Community Partner' limit 1];
         String profileId = Profile1.id;
      String accountId = Label.CommunityAdminProfile;
         AP_ApplicationController testCon;
      //Profile CProfile = [SELECT Id, Name FROM Profile WHERE Name ='Customer Community Login User'];
      Profile AProfile = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' limit 1];
      //User u = [SELECT Id FROM User WHERE ProfileId =:AProfile.Id AND isActive = true LIMIT 1];
      User u = [SELECT Id FROM User WHERE ProfileId =:AProfile.Id AND isActive = true AND UserRoleId!=null LIMIT 1];
      User temp = new User();
      User user2 = new User();
    
    system.runAs(u){ 
    Account account = new Account(Name='Test');
        insert(account);
        Contact contact = new Contact(FirstName='Rainmaker', LastName='Test', AccountId=account.Id);
        insert(contact);
        temp.Email = 'testing@test.com';
        temp.ContactId = contact.Id;
        temp.UserName = 'testingXII@test.com';
        temp.LastName = 'Padilla';
        temp.Alias = 'jpadille';
        temp.CommunityNickName = 'jpadilla';
        temp.TimeZoneSidKey = 'GMT';
        temp.LocaleSidKey = 'en_US';
        temp.EmailEncodingKey = 'ISO-8859-1';
        temp.ProfileId = profileId;
        temp.LanguageLocaleKey = 'en_US';
       
        String userId = Site.createPortalUser(temp, accountId, 'Testing');
        
         contact1 = new Contact(FirstName='Rainmaker1', LastName='Test1', AccountId=account.Id,Reviewer_Status__c='New Reviewer');
        insert(contact1); 
    
    }
   test.startTest(); 
   
    System.runAs(temp)
    {
  
    
    testCon = new AP_ApplicationController();
       
    testCon.apa.Application_Status_new__c = 'YES';
   testCon.apa.Categories_identified_with__c = 'Patient/Consumer;Clinician';
    testCon.saveMethod();
    testCon.Nextmethod();
    testCon.updateMe();
    testCon.myApp = new List <Advisory_Panel_Application__c>();
    testCon.Nextmethod();    
    testCon.attachment = new attachment();
    //TestCon.upload();
        
      
    }
}
    /*
    System.runAs(temp){
        
        Advisory_Panel_Application__c tempApp = new Advisory_Panel_Application__c();
        Contact tempContact = [select id from Contact where LastName =: 'Test1' ];
        tempApp.Last_Name__c = 'HAROtest';
        tempApp.Contact__c = tempContact.Id;
        insert tempApp;
        tempApp = [select id, Last_Name__c, Contact__c from Advisory_Panel_Application__c where Last_Name__c =: 'HAROtest' limit 1];
         testCon = new AP_ApplicationController();
        
         testCon.attachment = new attachment();
         TestCon.upload();
        
        testCon.apa.Abide_by_PCORI_COI_Policy__c = 'No';
        testCon.Nextmethod();
        testCon.attachment.body = Blob.valueOf('testBosdyData');
        testCon.upload();
        testCon.cnt =null;
        testCon.mycon = null;
        testCon.disb = null;
        testCon.myApp = new List<Advisory_Panel_Application__c>();
        
        
        system.runAs(u)
        {
            tempapp.Application_Status_New__c = 'stop'; 
            update tempapp;
          database.delete(tempapp);
        }
        testCon.Nextmethod();
        system.runAs(u){
            
            //   AP_ApplicationController testCon = new AP_ApplicationController();
 
          Blob b = Blob.valueOf('a' +'b');
        //myAttachment.ParentId =  temp.ContactId;
        myAttachment.Body = b;
        myAttachment.Name = 'test';
        myAttachment.Description = 'DanielHarotest';
        myAttachment.OwnerId = temp.id;
        //System.assert(false,' value ' + myAttachment.Description);
        //insert myAttachment;
       //System.assert(false,' value ' + myAttachment.Description);
      // myAttachment = [select id,ParentId,Body,Name, Description from Attachment where Description =: 'DanielHarotest' limit 1];
        testCon.selectedAttachmentId  = myAttachment.id;

      
            
          testCon.attachment  = myAttachment;
            testCon.upload();
            //insert myAttachment;
            myAttachment = [select id,ParentId,Body,Name, Description from Attachment where Description =: 'DanielHarotest' limit 1];
             testCon.selectedAttachmentId  = myAttachment.id;
           // pagereference stub = testCon.removeMember();
         // Attachment nullAttachment =   testCon.attachment();
 
           
        }
        
       // Blob b = Blob.valueOf('a' +'b');
       // myAttachment.ParentId =  temp.ContactId;
       // myAttachment.Body = b;
       // myAttachment.Name = 'test';
       // myAttachment.Description = 'DanielHarotest';
       // myAttachment.OwnerId = temp.id;
       // insert myAttachment;
       //System.assert(false,' value ' + myAttachment.Description);
     //  myAttachment = [select id,ParentId,Body,Name, Description from Attachment where Description =: 'DanielHarotest'];
      //  testCon.selectedAttachmentId = myAttachment.id;
      // pagereference stub = testCon.removeMember();

        testCon.refreshAttachment();
    }
    
    system.runAs(u)
    {
 
          Blob b = Blob.valueOf('a' +'b');
     //   myAttachment.ParentId =  temp.ContactId;
        myAttachment.Body = b;
        myAttachment.Name = 'test';
        myAttachment.Description = 'DanielHarotest';
        myAttachment.OwnerId = temp.id;
        //System.assert(false,' value ' + myAttachment.Description);
       // insert myAttachment;
       //System.assert(false,' value ' + myAttachment.Description);
      // myAttachment = [select id,ParentId,Body,Name, Description from Attachment where Description =: 'DanielHarotest'];
        //testCon.selectedAttachmentId = myAttachment.id;
      //  pagereference stub = testCon.removeMember();
        
        
    }
    
    
     test.stopTest();
}*/
    public static testMethod void myTest2(){
       
          Profile Profile1 = [SELECT Id, Name FROM Profile WHERE Name = 'PCORI Community Partner' limit 1];
         String profileId = Profile1.id;
      String accountId = Label.CommunityAdminProfile;
         AP_ApplicationController testCon;
      //Profile CProfile = [SELECT Id, Name FROM Profile WHERE Name ='Customer Community Login User'];
      Profile AProfile = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' limit 1];
      //User u = [SELECT Id FROM User WHERE ProfileId =:AProfile.Id AND isActive = true LIMIT 1];
      User u = [SELECT Id FROM User WHERE ProfileId =:AProfile.Id AND isActive = true AND UserRoleId!=null LIMIT 1];
      User temp = new User();
      User user2 = new User();
    
    system.runAs(u){ 
    Account account = new Account(Name='Test');
        insert(account);
        Contact contact = new Contact(FirstName='Rainmaker', LastName='Test', AccountId=account.Id);
        insert(contact);
        temp.Email = 'testing@test.com';
        temp.ContactId = contact.Id;
        temp.UserName = 'testingXIII@test.com';
        temp.LastName = 'Padilla';
        temp.Alias = 'jpadille';
        temp.CommunityNickName = 'jpadilla';
        temp.TimeZoneSidKey = 'GMT';
        temp.LocaleSidKey = 'en_US';
        temp.EmailEncodingKey = 'ISO-8859-1';
        temp.ProfileId = profileId;
        temp.LanguageLocaleKey = 'en_US';
       
        String userId = Site.createPortalUser(temp, accountId, 'Testing');
        
        Contact contact1 = new Contact(FirstName='Rainmaker1', LastName='Test1', AccountId=account.Id,Reviewer_Status__c='New Reviewer');
        insert(contact1); 
    
    }
   test.startTest(); 
   
         System.runAs(temp)
    {
  
    
    testCon = new AP_ApplicationController();
       
 //   testCon.apa.Application_Status_new__c = 'YES';
 //   testCon.apa.Categories_identified_with__c = 'Patient/Consumer;Clinician';
 //   testCon.saveMethod();
    testCon.apa.Abide_by_PCORI_COI_Policy__c = 'No';
    testCon.cnt.Accepts_Stipend_s__c = 'None';
    testCon.cnt.Accepts_Reimbursements__c = 'None';  
        testCon.stipend = 'None';
        testCon.Reimbursements = 'None';
        testCon.apa.Abide_by_PCORI_COI_Policy__c = null;
    testCon.Nextmethod();
        testCon.apa.Abide_by_PCORI_COI_Policy__c = 'No';
            testCon.Nextmethod();
            testCon.saveMethod();
 testCon.apa.Abide_by_PCORI_COI_Policy__c = 'go';
    testCon.cnt.Accepts_Stipend_s__c = 'go';
    testCon.cnt.Accepts_Reimbursements__c = 'go'; 
        testCon.stipend = 'go';
        testCon.Reimbursements = 'go';
        testCon.apa.Abide_by_PCORI_COI_Policy__c = 'go;';
         testCon.Nextmethod();
        
 //   testCon.updateMe();
 //   testCon.myApp = new List <Advisory_Panel_Application__c>();
  //  testCon.Nextmethod();    
  //  testCon.attachment = new attachment();
    //TestCon.upload();
        
       
       
   }
   }
}