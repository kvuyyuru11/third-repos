/**
 * An apex page controller that exposes the site forgot password functionality
 */
@IsTest public with sharing class ForgotPasswordController_ResearchTest {
      public static testMethod void testForgotPasswordController() {
      // Instantiate a new controller with all parameters in the page
      ForgotPasswordController_Research controller = new ForgotPasswordController_Research();
      controller.username = 'test@salesforce.com';       
    
      System.assertEquals(controller.forgotPassword(),null); 
    }
}