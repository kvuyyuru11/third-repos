@isTest
public class ParseMultiSelectTest {
    
    @isTest static void noresponse() {
        Test.startTest();
        List<String> ls= new list<String>();
        ls.add('Patient/consumer; ');
        ls.add('Caregiver/family member of patient; ');
        ls.add('Advocacy organization; ');
        ls.add('Clinician; ');
        ls.add('Clinic/ Hospital/ Health System; ');
        ls.add('Purchaser (small or large employers); ');
        ls.add('Payer (public or private insurance); ');
        ls.add('Life sciences industry; ');
        ls.add('Policy maker (government official); ');
        ls.add('Training institution (non-research health professions educator); ');
        ls.add('Subject matter expert - Please describe below; ');
        ls.add('Other - Please describe below; ');
        ls.add('ABC; ');
        ls.add('1234; ');
        ParseMultiSelect.ParseMultiSelect(ls);
    }
    
}