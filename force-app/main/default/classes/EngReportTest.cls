@isTest
public class EngReportTest{
    private static testMethod void eReportNoGrId(){
        Engagement_Report__c er = new Engagement_Report__c();
        insert er;
        EngReport erpt = new EngReport();
    }
    private static testMethod void eReport4Catch(){
        Opportunity opp = new Opportunity();
        opp.Name = 'Test';
        opp.Full_Project_Title__c = 'Test';
        opp.Project_Name_off_layout__c='Test';
        opp.StageName = 'Closed';
        opp.CloseDate = Date.newInstance(2016,5,15);
        insert opp;
        FGM_Base__Grantee_Report__c gr = new FGM_Base__Grantee_Report__c();
        gr.FGM_Base__Request__c = opp.Id;
        insert gr;
        PageReference pageRef = Page.Engagement_Report_PDF;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', gr.Id);
        EngReport erpt = new EngReport();
    }
    private static testMethod void eReport(){
        Opportunity opp = new Opportunity();
        opp.Name = 'Test';
         opp.Full_Project_Title__c = 'Test';
        opp.Project_Name_off_layout__c='Test';
        opp.StageName = 'Closed';
        opp.CloseDate = Date.newInstance(2016,5,15);
        insert opp;
        FGM_Base__Grantee_Report__c gr = new FGM_Base__Grantee_Report__c();
        gr.FGM_Base__Request__c = opp.Id;
        insert gr;
        Engagement_Report__c er = new Engagement_Report__c();
        er.Another_Q9__c = 'hey Test';
        er.Grantee_Report__c = gr.Id;
        insert er;
        PageReference pageRef = Page.Engagement_Report_PDF;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', gr.Id);
        EngReport erpt = new EngReport();
    }
}