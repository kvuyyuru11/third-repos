/****************************************************************************************************
*Description:           This class provides unit test coverage for the Foundation Connect Links class
*                       for the Community Portal and is used in the FoundationConnectLinks component 
*                       located on the Foundation Connect custom VF Page
*
*Required Class(es):    N/A
*Original: Rainmaker LLC
*Organization: Accenture
*Revision   |Date       |Author             |AdditionalNotes
*====================================================================================================
*   1.1     07/10/2015   Sanket Dable      Updated Version
*****************************************************************************************************/
@isTest
private class FoundationConnectLink_Test {
    
    static testMethod void VisualPageTest()
    {
        list<Account> acnt = TestDataFactory.getAccountTestRecords(1);
        list<contact> con  = TestDataFactory.getContactTestRecords(1,1);
        list<User> usrs    = TestDataFactory.getpartnerUserTestRecords(1,1);  
        
        FoundationConnectionLink__c temp = new FoundationConnectionLink__c();
        temp.Name = 'Test';
        temp.Url__c = 'http://google.com';
        temp.isActive__c = true;
        temp.Order__c = 0;
        temp.Access__c = 'Grantee';
        insert(temp);
        
        FoundationConnectionLink__c temp2 = new FoundationConnectionLink__c();
        temp2.Name = 'Test2';
        temp2.Url__c = 'http://google.com';
        temp2.isActive__c = true;
        temp2.Order__c = 1;
        temp2.Access__c = 'Reviewer';
        insert(temp2);
        
        String profileId = Label.MRA_Profile;
        Contact c = [SELECT Id, FGM_Portal__User_Profile__c FROM Contact where Id =: usrs[0].contactId LIMIT 1];
        c.FGM_Portal__User_Profile__c = 'Grantee;Reviewer';
        update c;        
        String accountId = Label.MRA_Account;
        Account account = new Account(Name='Test');
        insert(account);
        account.IsPartner=true;
        Contact contact = new Contact(FirstName='Rainmaker', LastName='Padilla', AccountId=account.Id);
        insert(contact);
        User user2 = new User();
        user2.Email = 'testingXVII@test.com';
        user2.ContactId = contact.Id;
        user2.UserName = 'testingXVII1ym@test.com';
        user2.LastName = 'Padilla1';
        user2.Alias = 'jpadilla';
        user2.IsActive=true;
        user2.CommunityNickName = 'jpadilla1';
        user2.TimeZoneSidKey = 'GMT';
        user2.LocaleSidKey = 'en_US';
        user2.EmailEncodingKey = 'ISO-8859-1';
        user2.ProfileId = profileId;
        user2.LanguageLocaleKey = 'en_US';
        String userId1 = Site.createPortalUser(user2, accountId, 'Testing2');
        
        PermissionSet permissionSet_User = [SELECT id, Name FROM PermissionSet where Name =: 'Community_Grantee_Permission_set_User_Login_Licence'];
        List<PermissionSetAssignment> psaUsers = [Select AssigneeId, PermissionSetId from PermissionSetAssignment where AssigneeId =:user2.Id and PermissionSetId =:permissionSet_User.Id];
        System.assertEquals(0, psaUsers.size());
        system.runAs(user2)
        {
            FoundationConnectLink fcl = new FoundationConnectLink();
            fcl.getGranteeLinks();
            fcl.getReviewerLinks();
            fcl.giveGranteePermissionSetIfNeeded();
            
        }         
        system.runAs(usrs[0])
        {
            FoundationConnectLink fcl = new FoundationConnectLink();
            fcl.getGranteeLinks();
            fcl.getReviewerLinks();
            fcl.giveGranteePermissionSetIfNeeded();
            Boolean tempBool = false;
            tempBool = fcl.showGrantee;
            tempBool = fcl.showReviewer;
            tempBool = fcl.showNotConfigured;            
        }    
        test.startTest();
        system.runAs(usrs[0]){
            FoundationConnectLink fcl1 = new FoundationConnectLink();            
            fcl1.EA_RedirectToFundingOpportunities();
            fcl1.EA_RedirectToLOIs();
            fcl1.EAIN_Meetingmethod();
            fcl1.RA_RedirectToLOIs();
            fcl1.RedirectToP2PTierA();
            fcl1.redirecttoRRCA();
            fcl1.redirecttodashboard();
            fcl1.RA_RedirectToFundingOpportunities();            
            fcl1.RA_RedirectToProjects();
            fcl1.EA_RedirectToProjects();
            fcl1.RA_RedirectToSiteVisits();           
            fcl1.Engagementmethod();
            fcl1.CreateLOImethod();
            fcl1.CreateMeetingmethod();        
            fcl1.RA_RedirectToPIRs();
            fcl1.RedirectToP2P();        
            fcl1.RA_RedirectToInfrastructure();         
            fcl1.RA_RedirectToDI();           
        } test.stopTest();
        
    }
}