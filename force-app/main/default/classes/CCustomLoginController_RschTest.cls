/**
 * An apex page controller that exposes the site login functionality
 */
@IsTest global with sharing class CCustomLoginController_RschTest {
    
    global static testMethod void testCommunitiesCustomLoginController () {
       CommunitiesCustomLoginController_Rsch controller = new CommunitiesCustomLoginController_Rsch();
       controller.username = 'test@force.com';
    controller.password = 'abcd1234';
       System.assertEquals(null, controller.login());       
    }     
}