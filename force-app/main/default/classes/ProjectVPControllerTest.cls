@isTest(SeeAllData=true) 
public class ProjectVPControllerTest {
    
    static testMethod void testgetProjects() {
        set<Id> def= new set<Id>();
        
        list<Account>  a=TestDataFactory.getAccountTestRecords(1);
        kwzd__KnowWho_Elected_Official__c kwa= new kwzd__KnowWho_Elected_Official__c();
        kwa.kwzd__Account__c=a[0].Id;
        insert kwa;
        
        ProjectVPController pv=new ProjectVPController(new ApexPages.StandardController(kwa));
        Test.startTest();
        pv.getProjects();
        pv.recid=kwa.Id;
    }    
    
}