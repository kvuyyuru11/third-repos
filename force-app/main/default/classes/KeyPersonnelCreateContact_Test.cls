/****************************************************************************************************
*Description:           This is the test class for the KeyPersonnelCreateContact trigger
*                       object
*
*Required Class(es):    N/A
*
*Organization: Rainmaker-LLC
*Revision   |Date       |Author             |AdditionalNotes
*====================================================================================================
*   1.0     04/04/2014   Justin Padilla      Initial Version
*   2.0     05/16/2016   Matt Hoffman        Modified the created Key Personnel record to be of type
                                             Engagement_Award_Project_Personnel.
*   3.0     5/24/2017    Kalyan Vuyyuru      Added logic to check if project personnel added to RRCA Opportunity and check the Project Personnel accordingly    

*   4.0     08/02/2018   Sowmya Goli        Added method for Project Personnel related list (Jira Ticket SPS-3689).

*****************************************************************************************************/
@isTest
public class KeyPersonnelCreateContact_Test {

    static testMethod void TriggerTest()
    {
        //Create an Account
        Account account = new Account(Name='Rainmaker-Test');
        insert(account);
        //Get the Engagement_Award_Project_Personnel record type id
        RecordType engagementRecordType = [SELECT id FROM RecordType where DeveloperName =: 'Engagement_Award_Project_Personnel' limit 1];
        //Create an Opportunity
        Opportunity opportunity = new Opportunity(Name='Rainmaker-Test-Opportunity', Full_Project_Title__c = 'Rainmaker-Test-Opportunity',Project_Name_off_layout__c='Rainmaker-Test-Opportunity',StageName='Prospecting', CloseDate=Date.today().addDays(5), AccountId = account.Id,Project_Lead_Last_Name__c='test');
        insert(opportunity);
        //Create a Progress Report
        FGM_Base__Grantee_Report__c pr = new FGM_Base__Grantee_Report__c(FGM_Base__Request__c=opportunity.Id,FGM_Base__Status__c='open');
        //Create a Key Personnel record 
        Key_Personnel__c kp = new Key_Personnel__c();
        kp.Opportunity__c = opportunity.Id;
        kp.First_Name__c = 'Justin';
        kp.Last_Name__c = 'Padilla';
        kp.RecordTypeId = engagementRecordType.Id;
        kp.Degrees__c = 'BS';
        kp.Degree_Other__c = 'Salesforce Developer';
        kp.Institution_Org__c = 'Rainmaker';
        kp.Primary_Affiliation__c = 'Research';
        kp.Relevant_Experience__c = '6-9 years';
        kp.Secondary_Email__c = 'jp_1_2@yahoo.com';
        kp.Telephone__c = '443-739-0628';
        kp.Email__c = 'justinpadilla@rainmaker-llc.com';
        kp.Progress_Report__c = pr.Id;
        insert(kp);
        
        //Test update section of the trigger
        kp.Email__c = 'jpadilla@rainmaker-llc.com';
        update(kp);
        
        //Test Update without a Contact Associated
        kp.Contact__c = null;
        kp.Email__c = 'justinpadilla@rainmaker-llc.com';
        update(kp);
        
        Contact c = new Contact(Lastname='test', Community__c='Patient',AccountID=account.id,Email = 'justinpadillaactpri45tfh8@rainmaker-llc.com');
        insert c;
        kp.Contact__c = c.id;
        kp.Email__c = 'justinpadillaactpri45tfh8@rainmaker-llc.com';
        update(kp);
    }
    
    static testMethod void TriggerTest2()
    {
        Id r1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RRCA').getRecordTypeId();
        Id r2 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RRCA Locked').getRecordTypeId();
        Id r3 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RRCA Unlocked').getRecordTypeId();
        Id r4 = Schema.SObjectType.Key_Personnel__c.getRecordTypeInfosByName().get('RRCA Project Personnel').getRecordTypeId();
        
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt;
        acnt.IsPartner=true;
        Contact con = new Contact(LastName ='PIUser',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA',email='mypiuserkt546@yopmail.com');  
        insert con;  
        User user = new User(alias = 'test123', email='mypiuserkt546@yopmail.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con.Id,
                             timezonesidkey='America/New_York', username='mypiuserkt546@yopmail.com');
        
        insert user;
        system.runAs(user){
        Opportunity o= new opportunity();
        
        o.Awardee_Institution_Organization__c=acnt.Id;
        o.RecordTypeId=r1;
        o.Project_Lead_Email_Address__c='mypiuserkt546@yopmail.com';
        o.Full_Project_Title__c='test';
        o.Project_Name_off_layout__c='test';
        o.CloseDate=Date.today();
        o.Application_Number__c='Ra-1234';
        o.StageName='In-Process';
        o.Name='test';
        o.PI_Name_User__c=user.Id;
        insert o;
        Key_Personnel__c kp1 = new Key_Personnel__c();
        kp1.First_Name_RRCA__c='Test';
        kp1.Last_Name_RRCA__c='Last';
        kp1.Institution_Org_RRCA__c='TestOrg';
        kp1.Relevant_Experience_RRCA__c='0-2 years';
        kp1.Telephone_RRCA__c='123-234-2345';
        kp1.Email_RRCA__c='testlast@test.com';
        kp1.RecordTypeId=r4;
        kp1.Opportunity__c=o.Id;
        insert kp1;
             Key_Personnel__c kp2 = new Key_Personnel__c();
        kp2.First_Name_RRCA__c='Test';
        kp2.Last_Name_RRCA__c='Last';
        kp2.Institution_Org_RRCA__c='TestOrg';
        kp2.Relevant_Experience_RRCA__c='0-2 years';
        kp2.Telephone_RRCA__c='123-234-2345';
        kp2.Email_RRCA__c='testlast@test.com';
        kp2.RecordTypeId=r4;
        kp2.Opportunity__c=o.Id;
        insert kp2;
            delete kp2;
            delete kp1;

    }
    }
    
   // Added by Sowmya for  JIRA Ticket SPS-3689
    static testMethod void TriggerTest3(){
   List<Opportunity> opp= testDataFactory.getopportunitytestrecords(1,7,1);
   PPlistController ppc = new PPlistController(new ApexPages.StandardController(opp[0]));
  
    
    }
}