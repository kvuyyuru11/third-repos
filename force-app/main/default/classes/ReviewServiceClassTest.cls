@isTest
public class ReviewServiceClassTest {
    
    static testmethod void ReviewSetup_Test ()
    {
        List<Account> accts = ObjectCreator.getAccts('Jimbo', 10);
        insert accts;
        
        List<User> users = ObjectCreator.getUsers(20, 'Donnie');
        insert users;
        
        List <Cycle__c> cList = ObjectCreator.getCycle();
        insert cList;
        
        List <Panel__c> pList = ObjectCreator.getPanel();
        pList[0].cycle__c = clist[0].Id;
        insert pList;
        
        Map<String,Id> recTypeNameWithIdMap=new Map<String,Id>();
        for(Schema.RecordTypeInfo recInfo : Schema.getGlobalDescribe().get('Opportunity').getDescribe().getRecordTypeInfosByName().values())
            recTypeNameWithIdMap.put(recInfo.getName(),recInfo.getRecordTypeId());
        
        List<Opportunity> opps = ObjectCreator.getOpps(5, 'CoolioHoolio');
        integer x = 0;
        for (Opportunity theOpp : opps)
        {
            Id recordType;           
            if (math.mod(x, 2) == 0)
                recordType = recTypeNameWithIdMap.get('RA Applications');
            else
                recordType = recTypeNameWithIdMap.get('RA Applications Methods');
            x++;
            theOpp.recordtypeid = recordType;
        } 
        insert opps;
        
        Map<String,Id> recTypeNameWithIdMapReview=new Map<String,Id>();
        for(Schema.RecordTypeInfo recInfo : Schema.getGlobalDescribe().get('FC_Reviewer__External_Review__c').getDescribe().getRecordTypeInfosByName().values())
            recTypeNameWithIdMapReview.put(recInfo.getName(),recInfo.getRecordTypeId());
        
        Id recordTypeReview = recTypeNameWithIdMapReview.get('LOI Review');
        
        List<FC_Reviewer__External_Review__c> reviewerList = new List<FC_Reviewer__External_Review__c>{
        new FC_Reviewer__External_Review__c(RecordTypeId= recordTypeReview, FC_Reviewer__Opportunity__c= opps[0].Id, OwnerId='005700000047D7AAAU', Panel__c = pList[0].Id)};

        insert reviewerList;  
        
    }

}