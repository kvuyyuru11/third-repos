/*------------------------------------------------------------------------------------------
*  Date            Project             Developer                       Justification
*  06/20/2017  Invoicing-Phase2        Vinayak Sharma                  Developer, REI Systems..
*  06/20/2017  Invoicing-Phase2        Vinayak Sharma                  REI Systems Inc This is a Trigger on the Project Invoice Calculation object which is used to update the Invoice Line Items which are in Finance Review , The business logic is contained in a  ProjectInvoiceCalTriggerLogic class 
* ---------------------------------------------------------------------------------------------
*/

public with sharing class ProjectInvoiceCalTriggerLogic extends TriggerHelper{
    
    /* @purpose This method is called in the After Update Context of the 
*/     
    public override void processAfterUpdate(){
        
        System.debug(' Start of the ProjectInvoiceCalTriggerLogic ');
        updateLineItems(Trigger.new); 
        //updatePICFieldsOnProject(Trigger.newMap, Trigger.oldMap);
        //updateFFPLineItems(); 
        System.debug(' End of the ProjectInvoiceCalTriggerLogic ');
    }
    
   
    
    /* @purpose This method is called to Update the Invoice Line Items which are under Finance Review ,this method is called  
*  when the Project Invoice Calculation is updated when the Invoice is Approved or Rejected and which in turn updates the
*  Project Invoice Calculation object.        
*/
    
    private void updateLineItems(List<Project_Invoice_Calculation__c> lstPIC){
        Map<Id,List<Invoice_Line_Item__c>> picILineItemMap = new Map<Id,List<Invoice_Line_Item__c>>(); // Map PIC --> list lineItems 
        
        // Query all the Line Items which are under Finance Review for the PIC ( Project Invoice Calcualtion) object being updated 
        List<Invoice_Line_Item__c> lstInvLineItem = [Select Salary__c,Salary_CED__c,Fringe_Benefits__c,Fringe_Benefits_CED__c, 
                                                     Consultant_Cost__c,Consultant_Cost_CED__c,Supplies__c,Supplies_CED__c,
                                                     Travel__c,Travel_CED__c,Consortium_Contractual_Costs__c, Consortium_Contractual_Costs_CED__c,
                                                     Equipment_CED__c , Equipment__c, Indirect_Costs__c ,Indirect_Costs_CED__c                                           
                                                     ,Other_Costs__c ,Other_Costs_CED__c,Line_Item_Type__c,PIC__c ,Invoice_status__c ,Milestone__c , 
                                                     Milestone_Number1__c , Milestone_Name1__c,Available_Funds1__c ,Cumulative1__c
                                                     from  Invoice_Line_Item__c
                                                     where PIC__c in:Trigger.new and Invoice_status__c =: System.Label.Finance_Review and Line_Item_Type__c != :System.Label.Milestone];
        
        System.debug(' List of the Line Items 1 is ' + lstInvLineItem);  
        try{                                 
            
            for(Invoice_Line_Item__c invLIObj  :lstInvLineItem){  
                List<Invoice_Line_Item__c> invLineItemsPIClst = picILineItemMap.get(invLIObj.PIC__c);
                if(invLineItemsPIClst == null ){   
                    System.debug(' List of the Line Items  4 is ' + invLIObj.PIC__c)  ;                                             
                    invLineItemsPIClst = new List<Invoice_Line_Item__c>(); 
                    picILineItemMap.put(invLIObj.PIC__c,invLineItemsPIClst);
                    System.debug('List of the Line Items 5 is ' + picILineItemMap);
                }
                invLineItemsPIClst.add(invLIObj);  
            } 
            
            System.debug('List of the Line Items 6 is ' + picILineItemMap);
            
            if(lstInvLineItem.size() != 0 ){ 
                for(Project_Invoice_Calculation__c picObj: (List<Project_Invoice_Calculation__c>)Trigger.new){
                    List<Invoice_Line_Item__c> invlineItemObj = picILineItemMap.get(picObj.Id);
                    for(Invoice_Line_Item__c ili: invlineItemObj){  
                        
                        //   if(ili.Line_Item_Type__c != System.Label.Milestone){
                        System.debug(' The Line Item Type is 7  '+ili.Line_Item_Type__c); 
                        
                        if( ili.Line_Item_Type__c == System.Label.Research_Period || ili.Line_Item_Type__c == System.Label.Engagement ){
                            ili.Salary_CED__c                       =  getDecimalvalue(ili.Salary__c)            +getDecimalvalue(picObj.Salary_TRP_Cumulative__c ) ; 
                            ili.Fringe_Benefits_CED__c              =  getDecimalvalue(ili.Fringe_Benefits__c)   + getDecimalvalue(picObj.Fringe_Benefits_TRP_Cumulative__c);  
                            ili.Consultant_Cost_CED__c              =  getDecimalvalue( ili.Consultant_Cost__c)   + getDecimalvalue(picObj.Consultant_Costs_TRP_Cumulative__c)  ;
                            ili.Supplies_CED__c                     =  getDecimalvalue(ili.Supplies__c)          + getDecimalvalue (picObj.Supplies_TRP_Cumulative__c)  ;
                            ili.Travel_CED__c                       =  getDecimalvalue(ili.Travel__c)            + getDecimalvalue(picObj.Travel_TRP_Cumulative__c)  ;
                            ili.Other_Costs_CED__c                  =  getDecimalvalue(ili.Other_Costs__c)       + getDecimalvalue(picObj.Other_Costs_TRP_Cumulative__c)  ;
                            //ili.Research_Related_IP_OP_Costs_CED__c =  ili.Research_Related_Inpatient_and_Outp__c   + picObj.Research_Related_I_O_TRP_Cumulative__c  ;
                            ili.Equipment_CED__c                    =  getDecimalvalue(ili.Equipment__c)         + getDecimalvalue(picObj.Equipment_TRP_Cumulative__c) ;
                            ili.Consortium_Contractual_Costs_CED__c =  getDecimalvalue(ili.Consortium_Contractual_Costs__c)   + getDecimalvalue(picObj.Consortium_Contractual_Costs_TRP_Cumulat__c)  ;
                            ili.Indirect_Costs_CED__c               =  getDecimalvalue(ili.Indirect_Costs__c )   + getDecimalvalue(picObj.Indirect_Costs_TRP_Cumulative__c)  ;
                        }  
                        if(ili.Line_Item_Type__c == System.Label.Supplemental_Funding){
                            ili.Salary_CED__c                       =  getDecimalvalue(ili.Salary__c)            + getDecimalvalue(picObj.Salary_SUP_Cumulative__c ) ; 
                            ili.Fringe_Benefits_CED__c              =  getDecimalvalue(ili.Fringe_Benefits__c)   + getDecimalvalue(picObj.Fringe_Benefits_SUP_Cumulative__c)  ;
                            ili.Consultant_Cost_CED__c              =  getDecimalvalue(ili.Consultant_Cost__c)   + getDecimalvalue(picObj.Consultant_Costs_SUP_Cumulative__c)  ;
                            ili.Supplies_CED__c                     =  getDecimalvalue(ili.Supplies__c)          + getDecimalvalue(picObj.Supplies_SUP_Cumulative__c)  ;
                            ili.Travel_CED__c                       =  getDecimalvalue(ili.Travel__c)            + getDecimalvalue(picObj.Travel_SUP_Cumulative__c)  ;
                            ili.Other_Costs_CED__c                  =  getDecimalvalue(ili.Other_Costs__c)       + getDecimalvalue(picObj.Other_Costs_SUP_Cumulative__c)  ;
                            //ili.Research_Related_IP_OP_Costs_CED__c =  ili.Research_Related_Inpatient_and_Outp__c   + picObj.Research_Related_I_O_SUP_Cumulative__c  ;
                            ili.Equipment_CED__c                    =  getDecimalvalue(ili.Equipment__c)         + getDecimalvalue(picObj.Equipment_SUP_Cumulative__c) ;
                            ili.Consortium_Contractual_Costs_CED__c =  getDecimalvalue(ili.Consortium_Contractual_Costs__c)   +  getDecimalvalue(picObj.Consortium_Contractual_Costs_SUP_Cumulat__c)  ;
                            ili.Indirect_Costs_CED__c               =  getDecimalvalue(ili.Indirect_Costs__c)    + getDecimalvalue(picObj.Indirect_Costs_SUP_Cumulative__c ) ;
                        }
                        if(ili.Line_Item_Type__c == System.Label.Peer_Review){
                            ili.Salary_CED__c                       =  getDecimalvalue(ili.Salary__c)            +  getDecimalvalue(picObj.Salary_PR_Cumulative__c)  ; 
                            ili.Fringe_Benefits_CED__c              =  getDecimalvalue(ili.Fringe_Benefits__c)   + getDecimalvalue(picObj.Fringe_Benefits_PR_Cumulative__c)  ;
                            ili.Consultant_Cost_CED__c              =  getDecimalvalue(ili.Consultant_Cost__c)   + getDecimalvalue(picObj.Consultant_Costs_PR_Cumulative__c)  ;
                            System.debug( '    ili.Other_Costs_CED__c                  8   >>>   '+ ili.Other_Costs_CED__c );
                            System.debug( '    ili.Other_Costs__c                      9   >>> '+ili.Other_Costs__c);
                            System.debug( '    picObj.Other_Costs_PR_Cumulative__c      10 >>> '+picObj.Other_Costs_PR_Cumulative__c );                    
                            ili.Other_Costs_CED__c                  =  getDecimalvalue(ili.Other_Costs__c)       + getDecimalvalue(picObj.Other_Costs_PR_Cumulative__c)  ;
                            ili.Consortium_Contractual_Costs_CED__c =  getDecimalvalue(ili.Consortium_Contractual_Costs__c)   + getDecimalvalue(picObj.Consortium_Contractual_Costs_PR_Cumulat__c)  ;
                            ili.Indirect_Costs_CED__c               =  getDecimalvalue(ili.Indirect_Costs__c)    + getDecimalvalue(picObj.Indirect_Costs_PR_Cumulative__c)  ;
                        } 
                        // }   
                        
                        System.debug('Project Invoice Calculation Trigger 11 >>>> '+ili);   
                        System.debug('ili.Salary_CED__c                   12 >>>> '+ili.Salary_CED__c);                    
                    }
                } 
            }                 
            System.debug('lstInvLineItem   13  >>>> '+lstInvLineItem);    
            update lstInvLineItem;  // update the Invoice Line Items which are under the Finance review
        }Catch(Exception e){
            System.debug(' The updateLineItems method Message exception    is  '+e.getMessage());
            System.debug(' The updateLineItems method LineNumber exception is  '+e.getLineNumber());
            System.debug(' The updateLineItems method cause exception      is  '+e.getCause()); 
            //     System.debug('The reason is             '+e.initCause(APEX_OBJECT cause) );
        }
        updateContractCloseOut(lstPIC);
    }
    
    //Commented as this logic not in use but could be used to expand the functionality in the future
    /*private void  updateFFPLineItems(){

Map<Id,List<Invoice_Line_Item__c>> picILineItemMap = new Map<Id,List<Invoice_Line_Item__c>>(); // Map PIC --> list lineItems 

// Query all the Line Items which are under Finance Review for the PIC ( Project Invoice Calcualtion) object being updated 
List<Invoice_Line_Item__c> lstInvLineItem = [Select Line_Item_Type__c,PIC__c ,Invoice_status__c ,Milestone__c , Milestone_Number1__c , Milestone_Name1__c,
Available_Funds1__c ,Cumulative1__c from  Invoice_Line_Item__c where PIC__c in:Trigger.new and 
Invoice_status__c =: System.Label.Finance_Review and Line_Item_Type__c =:System.Label.Milestone order by Name limit 50000];
System.debug('The list of invoive line Items is 1 '+lstInvLineItem);

Map<Id,Decimal> projectIdAmountMap = new Map<Id,Decimal>(); 

Map<Id,Project_Invoice_Calculation__c> proInvCalIdMap = new Map<Id,Project_Invoice_Calculation__c>(); 

for(Project_Invoice_Calculation__c projectInvCalc: (List<Project_Invoice_Calculation__c>)Trigger.new){                                                                  
Decimal totalAmount = 0;   

Project_Invoice_Calculation__c  oldObj = (Project_Invoice_Calculation__c)Trigger.oldMap.get(projectInvCalc.Id);
Project_Invoice_Calculation__c  newObj = (Project_Invoice_Calculation__c)Trigger.newMap.get(projectInvCalc.Id);

System.debug('The newObj.Milestone_Cumulative__c   is 2  '+newObj.Milestone_Cumulative__c);
System.debug('The oldObj.Milestone_Cumulative__c   is 3  '+oldObj.Milestone_Cumulative__c);

totalAmount =  newObj.Milestone_Cumulative__c - oldObj.Milestone_Cumulative__c;

System.debug('The totalAmountApproved   is 4  '+totalAmount);

if(totalAmount != 0){
projectIdAmountMap.put(projectInvCalc.Id,totalAmount);
}
}

System.debug('The projectIdAmountMap   is  5 '+projectIdAmountMap);                                              


for(Invoice_Line_Item__c objInvLineItem :lstInvLineItem){

Decimal approveRejectAmount = projectIdAmountMap.get(objInvLineItem.PIC__c);
System.debug('THE APPROVE REJECT AMOUNT 1 IS 5 ' + approveRejectAmount);

if(approveRejectAmount!=null && approveRejectAmount!=0 ){ // ie Reject   
objInvLineItem.Cumulative1__c      += approveRejectAmount ;
objInvLineItem.Available_Funds1__c -= approveRejectAmount ; 
}  

System.debug('THE APPROVE REJECT AMOUNT 2 IS 6 ' + objInvLineItem.Cumulative1__c );
System.debug('THE APPROVE REJECT AMOUNT 3 IS 7  ' + objInvLineItem.Available_Funds1__c);
} 

System.debug('The list of invoive line Items is 1 '+lstInvLineItem);
update lstInvLineItem;

} */
    
    private void updateContractCloseOut(List<Project_Invoice_Calculation__c> lstPIC){
        List<Id> opportunityIdList = new List<Id>(); 
        Map<Id, List<Contract_Checklist__c>> projectIdContractChecklistMap = new Map<Id, List<Contract_Checklist__c>>();
        List<Contract_Checklist__c> contractUpdateList = new List<Contract_Checklist__c>();
        for (Project_Invoice_Calculation__c pic : lstPIC){
            opportunityIdList.add(pic.Project__c);
        }
        for(Contract_Checklist__c ccl : getContractCloseoutRecords(opportunityIdList)){
            if(projectIdContractChecklistMap.containsKey(ccl.Contract__c )){
                projectIdContractChecklistMap.get(ccl.Contract__c).add(ccl);
            }
            else{
                projectIdContractChecklistMap.put(ccl.Contract__c, new List<Contract_Checklist__c>{ccl});
            }
        }
        for(Project_Invoice_Calculation__c pic : lstPIC){
            if(projectIdContractChecklistMap.containsKey(pic.Project__c)){
                if(!(updateContractFields(projectIdContractChecklistMap.get(pic.Project__c), pic).isEmpty())){
                    contractUpdateList.addAll(updateContractFields(projectIdContractChecklistMap.get(pic.Project__c), pic));
                }
                
            } 
        }
        if(!contractUpdateList.isEmpty()){
            update contractUpdateList;
        }
        
    }
    
    private List<Contract_Checklist__c> getContractCloseoutRecords(List<Id> oppList){
        return [Select Id, Name, Cumulative_Amount__c, De_Obligated_Amount__c, Contract__c from Contract_Checklist__c where Contract__c IN : oppList];
    }
    
    public static void updatePICFieldsOnProject(Map<Id,Project_Invoice_Calculation__c> newPicMap, Map<Id,Project_Invoice_Calculation__c> oldPicMap){
        system.debug('This is called ' + newPicMap);
        set<Id> opportunityIdSet = new Set<Id>();
        
        Map<Id,Map<String,Decimal>> oppFieldValuesMap = new Map<Id,Map<String,Decimal>>();
        
        for(Id newpicId : newPicMap.KeySet()){
        if(oldPicMap!=null){
            if(newPicMap.get(newpicId).Total_Contract_Cumulative__c != oldPicMap.get(newpicId).Total_Contract_Cumulative__c ||
               newPicMap.get(newpicId).Total_Deobligated_Amount__c != oldPicMap.get(newpicId).Total_Deobligated_Amount__c){
                   Map<String,Decimal> fieldValueMap = new Map<String,Decimal>();
                   opportunityIdSet.add(newPicMap.get(newpicId).Project__c);
                   if(newPicMap.get(newpicId).Total_Contract_Cumulative__c !=null)
                   fieldValueMap.put('Total_Contract_Cumulative__c',Math.round(newPicMap.get(newpicId).Total_Contract_Cumulative__c));
                   if(newPicMap.get(newpicId).Total_Deobligated_Amount__c!=null)
                   fieldValueMap.put('Total_Deobligated_Amount__c',Math.round(newPicMap.get(newpicId).Total_Deobligated_Amount__c));
                   if(getbalancePercentage(newPicMap.get(newpicId).Total_Contract_Cumulative__c,newPicMap.get(newpicId).Total_Deobligated_Amount__c) !=null)
                   fieldValueMap.put('Budget_Balance_Percent__c',Math.round(getbalancePercentage(newPicMap.get(newpicId).Total_Contract_Cumulative__c,newPicMap.get(newpicId).Total_Deobligated_Amount__c)));
                   oppFieldValuesMap.put(newPicMap.get(newpicId).Project__c, fieldValueMap);
               }
               }
               else{
                   Map<String,Decimal> fieldValueMap = new Map<String,Decimal>();
                   opportunityIdSet.add(newPicMap.get(newpicId).Project__c);
                   if(newPicMap.get(newpicId).Total_Contract_Cumulative__c !=null)
                   fieldValueMap.put('Total_Contract_Cumulative__c',Math.round(newPicMap.get(newpicId).Total_Contract_Cumulative__c));
                   if(newPicMap.get(newpicId).Total_Deobligated_Amount__c!=null)
                   fieldValueMap.put('Total_Deobligated_Amount__c',Math.round(newPicMap.get(newpicId).Total_Deobligated_Amount__c));
                   if(getbalancePercentage(newPicMap.get(newpicId).Total_Contract_Cumulative__c,newPicMap.get(newpicId).Total_Deobligated_Amount__c) !=null)
                   fieldValueMap.put('Budget_Balance_Percent__c',Math.round(getbalancePercentage(newPicMap.get(newpicId).Total_Contract_Cumulative__c,newPicMap.get(newpicId).Total_Deobligated_Amount__c)));
                   oppFieldValuesMap.put(newPicMap.get(newpicId).Project__c, fieldValueMap);
               }
        }
        Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>(getOpportunityRecords(opportunityIdSet));
        for(Id oppId : opportunityMap.keySet()){
            if(opportunityMap.get(oppId).Invoice_Type__c !='Firm Fixed Price'){
                opportunityMap.get(oppId).put('Contract_Expenses_to_Date__c',oppFieldValuesMap.get(oppId).get('Total_Contract_Cumulative__c'));
                opportunityMap.get(oppId).put('Budget_Balance_Remaining__c',oppFieldValuesMap.get(oppId).get('Total_Deobligated_Amount__c'));
                opportunityMap.get(oppId).put('Budget_Balance_Percent__c',oppFieldValuesMap.get(oppId).get('Budget_Balance_Percent__c')); 
            }
        }    
        if(!opportunityMap.values().isEmpty()){
            database.update(opportunityMap.values());
        }
    }
    
    public static Decimal getbalancePercentage(Decimal Contract_Cumulative, Decimal Deobligated_Amount){
        Decimal d = 0.0;
        if((Deobligated_Amount + Contract_Cumulative)>0){
            d = ((Deobligated_Amount/(Deobligated_Amount + Contract_Cumulative))*100);     
        }else{
            d = null;
        }
        
        return d;
    }
    private static Map<Id,Opportunity> getOpportunityRecords(set<Id> oppSet){
        return new Map<Id,Opportunity>([Select Id, Budget_Balance_Remaining__c, Contract_Expenses_to_Date__c,Budget_Balance_Percent__c, Invoice_Type__c
                                        from Opportunity Where Id IN : oppSet]);
    }
    
    public List<Contract_Checklist__c> updateContractFields(List<Contract_Checklist__c> ccList, Project_Invoice_Calculation__c pic){
        List<Contract_Checklist__c> updatedList = new List<Contract_Checklist__c>();
        for(Contract_Checklist__c cc : ccList){
            cc.Cumulative_Amount__c = pic.Total_Contract_Cumulative__c;
            cc.De_Obligated_Amount__c = pic.Total_Deobligated_Amount__c;
            updatedList.add(cc);
        }
        return updatedList;
    }
    private decimal getDecimalvalue(decimal sconvertedvalue){
        decimal dval;
        if(sconvertedvalue==null){
            dval=0;
        }
        else{
            dval=sconvertedvalue;
        }
        return dval;
    } 
    
}   // End of the class