//Future class for updating CommunityProfile based on Reviewer Status
public class UpdateUserInformation {
     @future
   //Method that takes the contact id as parameter from Trigger
    public static void updateUser(id x)
    {
        string text1=Label.Profile_Name_Approved;
        //Below soql query selects the present profileid from user Objects and changes accordingly as per the condition
user u=[select profileid from user where id=:x limit 1];
       system.debug(u);
        Profile p=[select Id from profile where Name=:text1 limit 1];
     u.ProfileId=p.Id;
        database.update(u);
 }
  }