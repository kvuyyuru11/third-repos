@isTest
public class TestCOIReview {
    static testMethod void testCOIController1() {
         Schema.DescribeSObjectResult oppsche = Schema.SObjectType.Opportunity;
    Map<string, schema.RecordtypeInfo> sObjectMap = oppsche.getRecordtypeInfosByName();    
    Id stroppRecordTypeId = sObjectMap.get('Research Awards').getRecordTypeId();
    
        Account a = new Account();
    a.Name = 'Acme';
    insert a;
    contact c= new Contact();
    c.LastName='opttest234h7';
        c.Email='opttest23gh7h6@test.com';
    c.AccountId=a.Id;
    insert c;
    
     opportunity objopp=new opportunity();
    objopp.name='testopp';
    objopp.Contract_Number__c='34567890';
    objopp.StageName='Qualification';
    objopp.closedate=system.today()+5;
    objopp.accountid=a.id;
    objopp.Full_Project_Title__c='testpms';
    objopp.Project_End_Date__c=system.today()+1;
    objopp.recordtypeid=stroppRecordTypeId;
    objopp.Project_Start_Date__c=system.today();
    insert objopp;
         RecordType objRecordType=  [SELECT Id,name FROM RecordType where name =: 'Awardee COI'];
       
        COI_Expertise__c objCOI_Expertise=new COI_Expertise__c();
        objCOI_Expertise.RecordTypeId=objRecordType.id;
        objCOI_Expertise.Related_Project__c=objopp.id;
        upsert objCOI_Expertise;
        ApexPages.StandardController controller =new ApexPages.StandardController(objCOI_Expertise);
          PageReference pageRef =Page.Coireviewer;
        pageRef.getParameters().put('Pid', String.valueOf(objopp.Id));
       pageRef.getParameters().put('coid', String.valueOf(objCOI_Expertise.Id));
        
        ApexPages.currentPage().getParameters().put('Referer', 'abc'+'/'+String.valueOf(objopp.Id)); 
        Test.setCurrentPage(pageRef);
        COIReview objCoiReview=new COIReview(controller);
        objCoiReview.save();
        objCoiReview.cancel();
          objCoiReview.doesOpportunityHavePendingApproval(objCOI_Expertise.Id);

    }
    
}