/**
	* ApprovalRoutingUtil - <description>
	* @author: brian Matthews
	* @version: 1.0
*/

public with sharing class ApprovalRoutingUtil{

	public static void assignApprovers(List<SurveyTaker__c> surveys) {
		
		System.debug('*** RUNNING ASSIGNAPPROVALS');
		
		/* This is the map for the routing key */
		Map<String, List<SurveyTaker__c>> routingKeyToSurveys = new Map<String, List<SurveyTaker__c>>();
		
		System.debug('*** STARTING LOOP');
		/* Loop through the opportunities, adding to the map and set */
		for(SurveyTaker__c s:surveys) {
			String key = s.Survey__c;
			System.debug('*** KEY VALUE: ' + key);
     		if(routingKeyToSurveys.get(key) == null) routingKeyToSurveys.put(key, new List<SurveyTaker__c>());
     		routingKeyToSurveys.get(key).add(s);	
		}
		
		System.debug('*** QUERYING ROUTING TABLE');
		/* Iterate over the approval_routing_rule__c objects with the respective routing key values
		   so we can set the appropriate level assignments on the surveys. */
		for(Approval_Routing_Rule__c rule:[select routing_key__c, Level1__c, Level2__c, Level3__c 
		                                     from Approval_Routing_Rule__c 
		                                     where routing_key__c in :routingKeyToSurveys.keySet()]){
		                                     	
         	for(SurveyTaker__c taker:routingKeyToSurveys.get(rule.routing_key__c)) {
				
				System.debug('*** LEVEL1 - ' + rule.level1__c);
				System.debug('*** LEVEL2 - ' + rule.level2__c);
				System.debug('*** LEVEL3 - ' + rule.level3__c);
				
         		taker.level1__c = rule.level1__c;
         		taker.level2__c = rule.level2__c;
         		taker.level3__c = rule.level3__c;
         	}	
		}
											 
	}
	
}