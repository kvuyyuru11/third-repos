@isTest
public class UpdateUserPermissionTest {
    private static testMethod void updateUser(){
        List<Id> mraIds = new List<Id>();
        RecordType recAcc = [SELECT Id FROM RecordType WHERE DeveloperName = 'Standard_Salesforce_Account' AND sObjectType = 'Account'];
        RecordType recCnt = [SELECT Id FROM RecordType WHERE DeveloperName = 'Standard_Salesforce_Contact' AND sObjectType = 'Contact'];
        Profile p = [SELECT Id FROM Profile WHERE Name = 'PCORI Community Partner'];
        UserRole r = [SELECT Id FROM UserRole WHERE Name = 'PCORI Default Role'];
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.RecordTypeId = recAcc.Id;
        insert acc;
        Contact c = new Contact();
        c.FirstName = 'Test';
        c.LastName = 'Tester';
        c.RecordTypeId = recCnt.Id;
        insert c;
        List<User> usrlst = [SELECT Id, ContactId FROM User WHERE ContactId != null AND FGM_Portal__UserProfile__c != null LIMIT 2];
        List<Merit_Reviewer_Application__c> mralst = new List<Merit_Reviewer_Application__c>();
        for(User usr :usrlst){
            Merit_Reviewer_Application__c mra = new Merit_Reviewer_Application__c();
            mra.Application_Status__c = 'Accepted as Reviewer';
            mra.Reviewer_Name__c = c.Id;
            mra.Reviewer_Role__c = 'Scientist';
            mralst.add(mra);
        }
        insert mralst;
        for(Merit_Reviewer_Application__c mra1 :mralst){
            mraIds.add(mra1.Id);
        }
        UpdateUserPermission.AssignPermission(mraIds);
        
    }
}