/*------------------------------------------------------------------------------------------
 *  Date            Project             Developer                       Justification
 *  03/10/2017  Invoicing-Phase2        Vinayak Sharma                  Developer, REI Systems
 *  03/20/2017  Invoicing-Phase2        Vinayak Sharma                  REI Systems Inc Wrapper class for Invoice Line Item  and use in InvoiceMethods class
 * ---------------------------------------------------------------------------------------------
*/
public class InvoiceWrapper {
    public Invoice__c Invoice { get; set; }
    public list<invoiceLineItemWrapper> InvoiceLineItems { get; set; }
    public Id clonedId {get;set;}
}