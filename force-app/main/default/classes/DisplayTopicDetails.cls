public with sharing class DisplayTopicDetails {

 public Topic__c topicDetails { get; set;}   
 public Id vendor_acct_id{get;set;}
 
 public DisplayTopicDetails()
 {
  topicDetails = new Topic__c();
   
    topicDetails =  [Select Id, 
                            External_Status__c, 
                            LastModifiedDate, 
                            Similar_Topic__c, 
                            Source__c, 
                            Research_Question__c, 
                            Stakeholder_Group__c, 
                            Priority_Population__c, 
                            Entry_Date__c, 
                            Primary_Research_Area__c, 
                            Name, 
                            Email__c, 
                            Disease_Condition__c 
                            From 
                            Topic__c 
                            where 
                            name  =: String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('Id'))];
                            

 }
        
}