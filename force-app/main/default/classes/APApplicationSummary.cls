public class APApplicationSummary
{
    public String fullName {get;set;}
    public String dummy {get;set;}
    public Advisory_Panel_Application__c apa {get;set;}
    public Contact cntct {get;set;}
    public List<Advisory_Panel_Application__c> myApp;
    public contact cnt {get;set;}
    public string a{get;set;}
    public string b{get;set;}
    
   
    public APApplicationSummary()
    {
        // Query for existing application.
        apa = new Advisory_Panel_Application__c();
       
       myApp = [SELECT Id, Name,Contact__c,Degrees__c,Describe_PC_trial_involvement__c,Describe_clinical_trial_involvement__c,Multi_stakeholder_processes_frameworks__c,Describe_multi_stakeholder_involvement__c,Data_related_research_involvement__c,Describe_data_research_involvement__c,Involved_in_Research_Prioritization__c,Describe_research_prioritization_involve__c,Participated_in_Clinical_Trial__c,Describe_Engaging_Patients__c,Patient_centric_clinicals_involvment__c,Describe_PCORI_research_involvement__c,Ever_engaged_in_PCORI_research__c,Engaged_in_PCOR_research__c,engaging_patients_and_other_stakeholders__c,Describe_PCOR_Research__c,Categories_identified_with__c,Relevant_societies_or_organizations__c,Primary_Rare_Disease_Community__c,Primary_Clinical_Trials_Community__c,Which_disease_condition_area_represented__c,Preferred_Advisory_Panel__c,Advisory_Panels_Applied_For__c,Highest_Degree_of_Education__c,Groups_identified_with_or_represented__c, First_Name__c, Last_Name__c, Application_Status_New__c, Abide_by_PCORI_COI_Policy__c, Personal_Statement2__c, Bio__c, Caregiver_categories_identified_with__c, Clinician_categories_identified_with__c, Health_Research_identified_with__c, Hospital_Health_System_identified_with__c, Industry_categories_identified_with__c, Payer_categories_identified_with__c, Policy_Maker_category_you_identify_with__c, Purchaser_categories_identified_with__c, Training_Institution_you_identify_with__c, Permission_to_post_bios_on_website__c, Please_tell_us_anything_else__c, Additional_Supportive_Information__c , What_is_your_highest_level_of_education__c from Advisory_Panel_Application__c where CreatedBy.Id =:userinfo.getUserId() and Application_Status_New__c = 'Draft' order by CreatedDate DESC limit 1];
        user us=[select id,contactid, Lastname from user where id=:userinfo.getUserId()];
         Id cid = us.contactid;
        cnt =[select id,Highest_Level_Of_Education__c,Degrees__c,Accepts_Stipend_s__c,Accepts_Reimbursements__c,Reviewer_Disease_Condition_Expertise__c,Reviewer_Population_Expertise__c from Contact where id=:cid limit 1];
        
        a=cnt.Accepts_Stipend_s__c;
        b=cnt.Accepts_Reimbursements__c;
        
        // Dummy data filler for unmapped questions.
        dummy = 'Dummy Data';
        
        // If application already exists.
        if(myApp.size() > 0)
        {
            // Just grab it all.
            apa = myApp[0];
            
            // Get the contact name.
            cntct = [Select Name from Contact where Id =: apa.Contact__c];
            fullName = cntct.Name;
           
            
            
            // For some reason having this here makes it so that it isn't required to specify needed fields
            // in the application query above.
            apa.Additional_Supportive_Information__c = myApp[0].Additional_Supportive_Information__c;
        }
    }
    
    // Logic for "Previous" button.
    public pagereference Previousmethod ()
    {
        pagereference pg=new pagereference(label.AP_Application_3);
        return pg;
    }
    
    // Logic for "Submit" buton.
    Public pagereference submit ()
    {
     apa.Application_Status__c='Ready for Review';
        apa.Application_Status_New__c='Submitted';
        database.update(apa);
    pagereference pg=new pagereference(label.AP_Application);
        return pg;
       
        
    }
   
    
}