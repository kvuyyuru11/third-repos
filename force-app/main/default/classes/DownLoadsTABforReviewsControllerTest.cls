@isTest
public class DownLoadsTABforReviewsControllerTest
{
    static testMethod void Test() 
    {        
        
        list<contact> cntlist= new list<contact>();
        list<user> userlist= new list<user>();
        list<opportunity> opprtlist= new list<opportunity>();
        list<FC_Reviewer__External_Review__c> rvwslist= new list<FC_Reviewer__External_Review__c>();
        RecordType r1=[Select Id from RecordType where name=:'RA Applications' limit 1];
        RecordType r2=[Select Id from RecordType where name=:'In-Person Review' limit 1];
        RecordType r3=[Select Id from RecordType where name=:'In-Person Preview' limit 1];
        RecordType r4=[Select Id from RecordType where name=:'Online Review - AD' limit 1];
        RecordType r5=[Select Id from RecordType where name=:'LOI Review' limit 1];
        Id profileId1 = [select id from profile where name='PCORI Community Partner'].id;  
        Account a = new Account();
        a.Name = 'Acme1';
        insert a;
        
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;  
        Contact con = new Contact(LastName ='testCon',AccountId = a.Id);  
        cntlist.add(con);  
        Contact con1 = new Contact(LastName ='testCon1',AccountId = a.Id);  
        cntlist.add(con1);
        Contact con2 = new Contact(LastName ='testCon1',AccountId = a.Id);  
        cntlist.add(con2);
        Contact con3 = new Contact(LastName ='testCon2',AccountId = a.Id);  
        cntlist.add(con3);
        insert cntlist;
        User user = new User(alias = 'test123', email='test123fvb@noemail.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,   
                             contactId = con.Id,
                             timezonesidkey='America/New_York', username='testerfvb1@noemail.com');
        userlist.add(user);
        
        
        
        User user1 = new User(alias = 'test1234', email='test1234fvb@noemail.com',
                              emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                              localesidkey='en_US', profileid = profileId1 , country='United States',IsActive = true,   
                              contactId = con1.Id,
                              timezonesidkey='America/New_York', username='testerfvb123@noemail.com');
        userlist.add(user1);
        
        User user2 = new User(alias = 'test145', email='test12345fvb@noemail.com',
                              emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                              localesidkey='en_US', profileid = profileId1 , country='United States',IsActive = true,   
                              contactId = con2.Id,
                              timezonesidkey='America/New_York', username='testerfvb12@noemail.com');
        userlist.add(user2);
        
        User user3 = new User(alias = 'test52', email='test135fvb@noemail.com',
                              emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                              localesidkey='en_US', profileid = profileId1 , country='United States',IsActive = true,   
                              contactId = con3.Id,
                              timezonesidkey='America/New_York', username='testerfvb82@noemail.com');
        userlist.add(user3);
        insert userlist;
        
        
            Cycle__c c = new Cycle__c();
            c.Name = 'testcycle';
            c.COI_Due_Date__c = date.valueof(System.now());
            insert c;
            
            Panel__c p = new Panel__c();
            p.Active__c=true;
            p.name= 'testpanel';
            p.Cycle__c= c.id ;
            p.MRO__c = user2.id;
            p.Panel_Due_Dtae__c=  Date.newInstance(2019,12,20); 
        p.OwnerId=user3.ID;
            insert p;
             Test.startTest();
            opportunity o=new opportunity();
            
            o.Awardee_Institution_Organization__c=a.Id;
            o.RecordTypeId=r1.Id;
            o.Full_Project_Title__c='test';
            o.Project_Name_off_layout__c='test';
            o.Panel__c=p.Id;
            o.CloseDate=Date.today();
            o.Application_Number__c='Ra-1234';
            o.StageName='In Progress';
            o.Name='test';
            o.PI_Name_User__c=user.id;
            o.AO_Name_User__c=user1.id;
            o.OwnerId=user3.id;
            opprtlist.add(o);
            
            opportunity o2 =new opportunity();
            
            o2.Awardee_Institution_Organization__c=a.Id;
            o2.RecordTypeId=r1.Id;
            o2.Full_Project_Title__c='test 2';
            o2.Project_Name_off_layout__c='test 2';
            o2.CloseDate=Date.today();
            o2.Application_Number__c='Ra-5678';
            o2.StageName='In Progress';
            o2.Name='test 2';
              o2.Panel__c=p.Id; 
            o2.PI_Name_User__c=user.id;
            o2.AO_Name_User__c=user1.id;
            o2.OwnerId=user3.id;
            opprtlist.add(o2);
            
            opportunity o3 =new opportunity();
            
            o3.Awardee_Institution_Organization__c=a.Id;
            o3.RecordTypeId=r1.Id;
            o3.Full_Project_Title__c='test 3';
            o3.Project_Name_off_layout__c='test 3';
            o3.CloseDate=Date.today();
            o3.Application_Number__c='Ra-5678';
            o3.StageName='In Progress';
            o3.Name='test 3';
         o3.Panel__c=p.Id;
            o3.PI_Name_User__c=user.id;
            o3.AO_Name_User__c=user1.id;
            o3.OwnerId=user3.id;
            opprtlist.add(o3);
            
            opportunity o4 =new opportunity();
            
            o4.Awardee_Institution_Organization__c=a.Id;
            o4.RecordTypeId=r1.Id;
            o4.Full_Project_Title__c='test 4';
            o4.Project_Name_off_layout__c='test 4';
            o4.CloseDate=Date.today();
            o4.Application_Number__c='Ra-5678';
            o4.StageName='In Progress';
            o4.Name='test 4';
         o4.Panel__c=p.Id;
            o4.PI_Name_User__c=user.id;
            o4.AO_Name_User__c=user1.id;
            o4.OwnerId=user3.id;
            opprtlist.add(o4);
            insert opprtlist;
           
            FC_Reviewer__External_Review__c Fcr1 = new FC_Reviewer__External_Review__c();
            Fcr1.RecordTypeId = r2.id;
            Fcr1.FC_Reviewer__Reviewer_Type__c='In-Person';
            Fcr1.FC_Reviewer__Opportunity__c = o.id;
            Fcr1.Panel__c = p.id;
            Fcr1.FC_Reviewer__Contact__c = con3.id;
            Fcr1.FC_Reviewer__Deadline__c = date.valueof(System.now());
            fcr1.OwnerId=user3.Id;
            rvwslist.add(Fcr1);
            
            FC_Reviewer__External_Review__c Fcr2 = new FC_Reviewer__External_Review__c();
            Fcr2.RecordTypeId = r3.id;
            Fcr2.FC_Reviewer__Reviewer_Type__c='In-Person';
            Fcr2.FC_Reviewer__Opportunity__c = o2.id;
            Fcr2.Panel__c = p.id;
            Fcr2.FC_Reviewer__Contact__c = con3.id;
            Fcr2.FC_Reviewer__Deadline__c = date.valueof(System.now());
        fcr2.OwnerId=user3.Id;
            rvwslist.add(Fcr2);
            
            FC_Reviewer__External_Review__c Fcr3 = new FC_Reviewer__External_Review__c();
            Fcr3.RecordTypeId = r4.id;
            Fcr3.FC_Reviewer__Opportunity__c = o.id;
            Fcr3.Panel__c = p.id;
            Fcr3.FC_Reviewer__Contact__c = con3.id;
            Fcr3.FC_Reviewer__Deadline__c = date.valueof(System.now());
        fcr3.OwnerId=user3.Id;
            rvwslist.add(Fcr3);
            
            FC_Reviewer__External_Review__c Fcr4 = new FC_Reviewer__External_Review__c();
            Fcr4.RecordTypeId = r4.id;
            Fcr4.FC_Reviewer__Opportunity__c = o2.id;
            Fcr4.Panel__c = p.id;
            Fcr4.FC_Reviewer__Contact__c = con3.id;
            Fcr4.FC_Reviewer__Deadline__c = date.valueof(System.now());
        fcr4.OwnerId=user3.Id;
            rvwslist.add(Fcr4);
            insert  rvwslist;           
           
            
            Test.setCurrentPageReference(new PageReference('Page.LinksDownloadingPageInPersonPreview'));
            System.currentPageReference().getParameters().put('Id',Fcr1.Id);
            
        system.runas(user3){
            DownLoadsTABforReviewsController page1 = new DownLoadsTABforReviewsController();
            
            
            List<SelectOption> gOption = page1.getopptoptions(); 
            
            pagereference pRef1 = page1.showlinks();
            System.assertEquals(null, pRef1);
            
            page1.selectedOpt = o.Name;
            pRef1 = page1.showlinks();
            pagereference pExpected1 = new PageReference(Label.LinksDownloadingPageInPersonReview+Fcr2.Id);
            
            System.assertEquals(pExpected1.getAnchor(), pRef1.getAnchor());
            page1.selectedOpt = o2.Name;
            pRef1 = page1.showlinks();
            pExpected1 = new PageReference(Label.LinksDownloadingPageInPersonPreview+Fcr1.Id);
            
            System.assertEquals(pExpected1.getAnchor(), pRef1.getAnchor());
            
            
           
            
            Test.stopTest();
            
            }
        
        
    }
    
    static testMethod void Test1() 
    {        
        
        list<contact> cntlist= new list<contact>();
        list<user> userlist= new list<user>();
        list<opportunity> opprtlist= new list<opportunity>();
        list<FC_Reviewer__External_Review__c> rvwslist= new list<FC_Reviewer__External_Review__c>();
        RecordType r1=[Select Id from RecordType where name=:'RA Applications' limit 1];
        RecordType r2=[Select Id from RecordType where name=:'In-Person Review' limit 1];
        RecordType r3=[Select Id from RecordType where name=:'In-Person Preview' limit 1];
        RecordType r4=[Select Id from RecordType where name=:'Online Review - AD' limit 1];
        RecordType r5=[Select Id from RecordType where name=:'LOI Review' limit 1];
        Id profileId1 = [select id from profile where name='PCORI Community Partner'].id;  
        Account a = new Account();
        a.Name = 'Acme1';
        insert a;
        
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;  
        Contact con = new Contact(LastName ='testCon',AccountId = a.Id);  
        cntlist.add(con);  
        Contact con1 = new Contact(LastName ='testCon1',AccountId = a.Id);  
        cntlist.add(con1);
        Contact con2 = new Contact(LastName ='testCon1',AccountId = a.Id);  
        cntlist.add(con2);
        Contact con3 = new Contact(LastName ='testCon2',AccountId = a.Id);  
        cntlist.add(con3);
        insert cntlist;
        User user = new User(alias = 'test123', email='test123fvb1@noemail.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,   
                             contactId = con.Id,
                             timezonesidkey='America/New_York', username='testerfvb11@noemail.com');
        userlist.add(user);
        
        
        
        User user1 = new User(alias = 'test1234', email='test1234fvb1@noemail.com',
                              emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                              localesidkey='en_US', profileid = profileId1 , country='United States',IsActive = true,   
                              contactId = con1.Id,
                              timezonesidkey='America/New_York', username='testerfvb1231@noemail.com');
        userlist.add(user1);
        
        User user2 = new User(alias = 'test145', email='test12345fvb1@noemail.com',
                              emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                              localesidkey='en_US', profileid = profileId1 , country='United States',IsActive = true,   
                              contactId = con2.Id,
                              timezonesidkey='America/New_York', username='testerfvb121@noemail.com');
        userlist.add(user2);
        
        User user3 = new User(alias = 'test52', email='test135fvb1@noemail.com',
                              emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                              localesidkey='en_US', profileid = profileId1 , country='United States',IsActive = true,   
                              contactId = con3.Id,
                              timezonesidkey='America/New_York', username='testerfvb821@noemail.com');
        userlist.add(user3);
        insert userlist;
        

            Cycle__c c = new Cycle__c();
            c.Name = 'testcycle';
            c.COI_Due_Date__c = date.valueof(System.now());
            insert c;
            
            Panel__c p = new Panel__c();
            p.Active__c=true;
            p.name= 'testpanel';
            p.Cycle__c= c.id ;
            p.MRO__c = user2.id;
            p.Panel_Due_Dtae__c=  Date.newInstance(2019,12,20); 
        p.ownerId=user3.Id;
            insert p;
             Test.startTest();
            opportunity o=new opportunity();
            
            o.Awardee_Institution_Organization__c=a.Id;
            o.RecordTypeId=r1.Id;
            o.Full_Project_Title__c='test';
            o.Project_Name_off_layout__c='test';
            //o.Panel__c=p.Id;
            o.CloseDate=Date.today();
            o.Application_Number__c='Ra-1234';
            o.StageName='In Progress';
            o.Name='test';
            o.PI_Name_User__c=user.id;
            o.AO_Name_User__c=user1.id;
            o.OwnerId=user3.id;
            opprtlist.add(o);
            
            opportunity o2 =new opportunity();
            
            o2.Awardee_Institution_Organization__c=a.Id;
            o2.RecordTypeId=r1.Id;
            o2.Full_Project_Title__c='test 2';
            o2.Project_Name_off_layout__c='test 2';
            o2.CloseDate=Date.today();
            o2.Application_Number__c='Ra-5678';
            o2.StageName='In Progress';
            o2.Name='test 2';
            o2.PI_Name_User__c=user.id;
            o2.AO_Name_User__c=user1.id;
            o2.OwnerId=user3.id;
            opprtlist.add(o2);
            
            opportunity o3 =new opportunity();
            
            o3.Awardee_Institution_Organization__c=a.Id;
            o3.RecordTypeId=r1.Id;
            o3.Full_Project_Title__c='test 3';
            o3.Project_Name_off_layout__c='test 3';
            o3.CloseDate=Date.today();
            o3.Application_Number__c='Ra-5678';
            o3.StageName='In Progress';
            o3.Name='test 3';
            o3.PI_Name_User__c=user.id;
            o3.AO_Name_User__c=user1.id;
            o3.OwnerId=user3.id;
            opprtlist.add(o3);
            
            opportunity o4 =new opportunity();
            
            o4.Awardee_Institution_Organization__c=a.Id;
            o4.RecordTypeId=r1.Id;
            o4.Full_Project_Title__c='test 4';
            o4.Project_Name_off_layout__c='test 4';
            o4.CloseDate=Date.today();
            o4.Application_Number__c='Ra-5678';
            o4.StageName='In Progress';
            o4.Name='test 4';
            o4.PI_Name_User__c=user.id;
            o4.AO_Name_User__c=user1.id;
            o4.OwnerId=user3.id;
            opprtlist.add(o4);
            insert opprtlist;
                    
            FC_Reviewer__External_Review__c Fcr5 = new FC_Reviewer__External_Review__c();
            Fcr5.RecordTypeId = r4.id;
            Fcr5.FC_Reviewer__Opportunity__c = o3.id;
            Fcr5.Panel__c = p.id;
            Fcr5.FC_Reviewer__Contact__c = con3.id;
            Fcr5.FC_Reviewer__Deadline__c = date.valueof(System.now());
        fcr5.OwnerId=user3.Id;
            insert Fcr5;
            
           
            
            System.runAs(user3){
            
            DownLoadsTABforReviewsController page1 = new DownLoadsTABforReviewsController();
            
            
            List<SelectOption> gOption = page1.getopptoptions(); 
            
            pagereference pRef1 = page1.showlinks();
            System.assertEquals(null, pRef1);
            
            
            page1.selectedOpt = o3.Name;
            pRef1 = page1.showlinks();
            pagereference  pExpected1 = new PageReference(Label.LinksDownloadingPageOnlineReviewAD+Fcr5.Id);
            
           System.assertEquals(pExpected1.getAnchor(), pRef1.getAnchor());
            
            
            Test.stopTest();
            
            
        }
        
    }
    
    
    static testMethod void Test2() 
    {        
        
        list<contact> cntlist= new list<contact>();
        list<user> userlist= new list<user>();
        list<opportunity> opprtlist= new list<opportunity>();
        list<FC_Reviewer__External_Review__c> rvwslist= new list<FC_Reviewer__External_Review__c>();
        RecordType r1=[Select Id from RecordType where name=:'RA Applications' limit 1];
        RecordType r2=[Select Id from RecordType where name=:'In-Person Review' limit 1];
        RecordType r3=[Select Id from RecordType where name=:'In-Person Preview' limit 1];
        RecordType r4=[Select Id from RecordType where name=:'Online Review - AD' limit 1];
        RecordType r5=[Select Id from RecordType where name=:'LOI Review' limit 1];
        Id profileId1 = [select id from profile where name='PCORI Community Partner'].id;  
        Account a = new Account();
        a.Name = 'Acme1';
        insert a;
        
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;  
        Contact con = new Contact(LastName ='testCon',AccountId = a.Id);  
        cntlist.add(con);  
        Contact con1 = new Contact(LastName ='testCon1',AccountId = a.Id);  
        cntlist.add(con1);
        Contact con2 = new Contact(LastName ='testCon1',AccountId = a.Id);  
        cntlist.add(con2);
        Contact con3 = new Contact(LastName ='testCon2',AccountId = a.Id);  
        cntlist.add(con3);
        insert cntlist;
        User user = new User(alias = 'test123', email='test123fvb2@noemail.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,   
                             contactId = con.Id,
                             timezonesidkey='America/New_York', username='testerfvb12@noemail.com');
        userlist.add(user);
        
        
        
        User user1 = new User(alias = 'test1234', email='test12342fvb@noemail.com',
                              emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                              localesidkey='en_US', profileid = profileId1 , country='United States',IsActive = true,   
                              contactId = con1.Id,
                              timezonesidkey='America/New_York', username='testerfvb1232@noemail.com');
        userlist.add(user1);
        
        User user2 = new User(alias = 'test145', email='test12345fvb2@noemail.com',
                              emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                              localesidkey='en_US', profileid = profileId1 , country='United States',IsActive = true,   
                              contactId = con2.Id,
                              timezonesidkey='America/New_York', username='testerfvb122@noemail.com');
        userlist.add(user2);
        
        User user3 = new User(alias = 'test52', email='test135fvb2@noemail.com',
                              emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                              localesidkey='en_US', profileid = profileId1 , country='United States',IsActive = true,   
                              contactId = con3.Id,
                              timezonesidkey='America/New_York', username='testerfvb822@noemail.com');
        userlist.add(user3);
        insert userlist;
        
  
            Cycle__c c = new Cycle__c();
            c.Name = 'testcycle';
            c.COI_Due_Date__c = date.valueof(System.now());
            insert c;
            
            Panel__c p = new Panel__c();
            p.Active__c=true;
            p.name= 'testpanel';
            p.Cycle__c= c.id ;
            p.MRO__c = user2.id;
            p.Panel_Due_Dtae__c=  Date.newInstance(2019,12,20); 
        p.ownerId=user3.Id;
            insert p;
              Test.startTest();
            opportunity o=new opportunity();
            
            o.Awardee_Institution_Organization__c=a.Id;
            o.RecordTypeId=r1.Id;
            o.Full_Project_Title__c='test';
            o.Project_Name_off_layout__c='test';
            //o.Panel__c=p.Id;
            o.CloseDate=Date.today();
            o.Application_Number__c='Ra-1234';
            o.StageName='In Progress';
            o.Name='test';
            o.PI_Name_User__c=user.id;
            o.AO_Name_User__c=user1.id;
            o.OwnerId=user3.id;
            opprtlist.add(o);
            
            opportunity o2 =new opportunity();
            
            o2.Awardee_Institution_Organization__c=a.Id;
            o2.RecordTypeId=r1.Id;
            o2.Full_Project_Title__c='test 2';
            o2.Project_Name_off_layout__c='test 2';
            o2.CloseDate=Date.today();
            o2.Application_Number__c='Ra-5678';
            o2.StageName='In Progress';
            o2.Name='test 2';
            o2.PI_Name_User__c=user.id;
            o2.AO_Name_User__c=user1.id;
            o2.OwnerId=user3.id;
            opprtlist.add(o2);
            
            opportunity o3 =new opportunity();
            
            o3.Awardee_Institution_Organization__c=a.Id;
            o3.RecordTypeId=r1.Id;
            o3.Full_Project_Title__c='test 3';
            o3.Project_Name_off_layout__c='test 3';
            o3.CloseDate=Date.today();
            o3.Application_Number__c='Ra-5678';
            o3.StageName='In Progress';
            o3.Name='test 3';
            o3.PI_Name_User__c=user.id;
            o3.AO_Name_User__c=user1.id;
            o3.OwnerId=user3.id;
            opprtlist.add(o3);
            
            opportunity o4 =new opportunity();
            
            o4.Awardee_Institution_Organization__c=a.Id;
            o4.RecordTypeId=r1.Id;
            o4.Full_Project_Title__c='test 4';
            o4.Project_Name_off_layout__c='test 4';
            o4.CloseDate=Date.today();
            o4.Application_Number__c='Ra-5678';
            o4.StageName='In Progress';
            o4.Name='test 4';
            o4.PI_Name_User__c=user.id;
            o4.AO_Name_User__c=user1.id;
            o4.OwnerId=user3.id;
            opprtlist.add(o4);
            insert  opprtlist;
        
             
            FC_Reviewer__External_Review__c Fcr3 = new FC_Reviewer__External_Review__c();
            Fcr3.RecordTypeId = r4.id;
            Fcr3.FC_Reviewer__Opportunity__c = o.id;
            Fcr3.Panel__c = p.id;
            Fcr3.FC_Reviewer__Contact__c = con3.id;
            Fcr3.FC_Reviewer__Deadline__c = date.valueof(System.now());
        fcr3.OwnerId=user3.Id;
            rvwslist.add(Fcr3);
            
            FC_Reviewer__External_Review__c Fcr4 = new FC_Reviewer__External_Review__c();
            Fcr4.RecordTypeId = r4.id;
            Fcr4.FC_Reviewer__Opportunity__c = o2.id;
            Fcr4.Panel__c = p.id;
            Fcr4.FC_Reviewer__Contact__c = con3.id;
            Fcr4.FC_Reviewer__Deadline__c = date.valueof(System.now());
        fcr4.OwnerId=user3.Id;
            rvwslist.add(Fcr4);
            
            FC_Reviewer__External_Review__c Fcr1 = new FC_Reviewer__External_Review__c();
            Fcr1.RecordTypeId = r2.id;
            Fcr1.FC_Reviewer__Reviewer_Type__c='In-Person';
            Fcr1.FC_Reviewer__Opportunity__c = o.id;
            Fcr1.Panel__c = p.id;
            Fcr1.FC_Reviewer__Contact__c = con3.id;
            Fcr1.FC_Reviewer__Deadline__c = date.valueof(System.now());
        fcr1.OwnerId=user3.Id;
            rvwslist.add(Fcr1);
            
            FC_Reviewer__External_Review__c Fcr2 = new FC_Reviewer__External_Review__c();
            Fcr2.RecordTypeId = r3.id;
            Fcr2.FC_Reviewer__Reviewer_Type__c='In-Person';
            Fcr2.FC_Reviewer__Opportunity__c = o2.id;
            Fcr2.Panel__c = p.id;
            Fcr2.FC_Reviewer__Contact__c = con3.id;
            Fcr2.FC_Reviewer__Deadline__c = date.valueof(System.now());
        fcr2.OwnerId=user3.Id;
            rvwslist.add(Fcr2);
            insert rvwslist;
            
          
            
            Test.setCurrentPageReference(new PageReference('Page.LinksDownloadingPageInPersonPreview'));
            System.currentPageReference().getParameters().put('Id',Fcr1.Id);
            
             System.runAs(user3){
            DownLoadsTABforReviewsController page1 = new DownLoadsTABforReviewsController();
            
            
            List<SelectOption> gOption = page1.getopptoptions(); 
            
            pagereference pRef1 = page1.showlinks();
            System.assertEquals(null, pRef1);
            
            page1.selectedOpt = o.Name;
            pRef1 = page1.showlinks();
           pagereference pExpected1 = new PageReference(Label.LinksDownloadingPageInPersonReview+Fcr2.Id);
            
            System.assertEquals(pExpected1.getAnchor(), pRef1.getAnchor());
            page1.selectedOpt = o2.Name;
            pRef1 = page1.showlinks();
            pExpected1 = new PageReference(Label.LinksDownloadingPageInPersonPreview+Fcr1.Id);
            
            System.assertEquals(pExpected1.getAnchor(), pRef1.getAnchor());
            
            
            Test.stopTest();
            
            
        }
        
    }
    
        static testMethod void Test4() 
    {        
        
           list<contact> cntlist= new list<contact>();
        list<user> userlist= new list<user>();
        list<opportunity> opprtlist= new list<opportunity>();
        list<FC_Reviewer__External_Review__c> rvwslist= new list<FC_Reviewer__External_Review__c>();
        RecordType r1=[Select Id from RecordType where name=:'RA Applications' limit 1];
        RecordType r2=[Select Id from RecordType where name=:'In-Person Review' limit 1];
        RecordType r3=[Select Id from RecordType where name=:'In-Person Preview' limit 1];
        RecordType r4=[Select Id from RecordType where name=:'Online Review - AD' limit 1];
        RecordType r5=[Select Id from RecordType where name=:'LOI Review' limit 1];
        Id profileId1 = [select id from profile where name='PCORI Community Partner'].id;  
        Account a = new Account();
        a.Name = 'Acme1';
        insert a;
        
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;  
        Contact con = new Contact(LastName ='testCon',AccountId = a.Id);  
        cntlist.add(con);  
        Contact con1 = new Contact(LastName ='testCon1',AccountId = a.Id);  
        cntlist.add(con1);
        Contact con2 = new Contact(LastName ='testCon1',AccountId = a.Id);  
        cntlist.add(con2);
        Contact con3 = new Contact(LastName ='testCon2',AccountId = a.Id);  
        cntlist.add(con3);
        insert cntlist;
        User user = new User(alias = 'test123', email='test123fvb12@noemail.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,   
                             contactId = con.Id,
                             timezonesidkey='America/New_York', username='testerfvb112@noemail.com');
        userlist.add(user);
        
        
        
        User user1 = new User(alias = 'test1234', email='test1234fvb12@noemail.com',
                              emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                              localesidkey='en_US', profileid = profileId1 , country='United States',IsActive = true,   
                              contactId = con1.Id,
                              timezonesidkey='America/New_York', username='testerfvb12312@noemail.com');
        userlist.add(user1);
        
        User user2 = new User(alias = 'test145', email='test12345fvb12@noemail.com',
                              emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                              localesidkey='en_US', profileid = profileId1 , country='United States',IsActive = true,   
                              contactId = con2.Id,
                              timezonesidkey='America/New_York', username='testerfvb1212@noemail.com');
        userlist.add(user2);
        
        User user3 = new User(alias = 'test52', email='test135fvb12@noemail.com',
                              emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                              localesidkey='en_US', profileid = profileId1 , country='United States',IsActive = true,   
                              contactId = con3.Id,
                              timezonesidkey='America/New_York', username='testerfvb8212@noemail.com');
        userlist.add(user3);
        insert userlist;
        
       
            Cycle__c c = new Cycle__c();
            c.Name = 'testcycle';
            c.COI_Due_Date__c = date.valueof(System.now());
            insert c;
            
            Panel__c p = new Panel__c();
            p.Active__c=true;
            p.name= 'testpanel';
            p.Cycle__c= c.id ;
            p.MRO__c = user2.id;
            p.Panel_Due_Dtae__c=  Date.newInstance(2019,12,20); 
        p.OwnerId=user3.Id;
            insert p;
             Test.startTest();
            opportunity o=new opportunity();
            
            o.Awardee_Institution_Organization__c=a.Id;
            o.RecordTypeId=r1.Id;
            o.Full_Project_Title__c='test';
            o.Project_Name_off_layout__c='test';
            //o.Panel__c=p.Id;
            o.CloseDate=Date.today();
            o.Application_Number__c='Ra-1234';
            o.StageName='In Progress';
            o.Name='test';
            o.PI_Name_User__c=user.id;
            o.AO_Name_User__c=user1.id;
            o.OwnerId=user3.id;
            opprtlist.add(o);
            
            opportunity o2 =new opportunity();
            
            o2.Awardee_Institution_Organization__c=a.Id;
            o2.RecordTypeId=r1.Id;
            o2.Full_Project_Title__c='test 2';
            o2.Project_Name_off_layout__c='test 2';
            o2.CloseDate=Date.today();
            o2.Application_Number__c='Ra-5678';
            o2.StageName='In Progress';
            o2.Name='test 2';
            o2.PI_Name_User__c=user.id;
            o2.AO_Name_User__c=user1.id;
            o2.OwnerId=user3.id;
            opprtlist.add(o2);
            
            opportunity o3 =new opportunity();
            
            o3.Awardee_Institution_Organization__c=a.Id;
            o3.RecordTypeId=r1.Id;
            o3.Full_Project_Title__c='test 3';
            o3.Project_Name_off_layout__c='test 3';
            o3.CloseDate=Date.today();
            o3.Application_Number__c='Ra-5678';
            o3.StageName='In Progress';
            o3.Name='test 3';
            o3.PI_Name_User__c=user.id;
            o3.AO_Name_User__c=user1.id;
            o3.OwnerId=user3.id;
            opprtlist.add(o3);
            
            opportunity o4 =new opportunity();
            
            o4.Awardee_Institution_Organization__c=a.Id;
            o4.RecordTypeId=r1.Id;
            o4.Full_Project_Title__c='test 4';
            o4.Project_Name_off_layout__c='test 4';
            o4.CloseDate=Date.today();
            o4.Application_Number__c='Ra-5678';
            o4.StageName='In Progress';
            o4.Name='test 4';
            o4.PI_Name_User__c=user.id;
            o4.AO_Name_User__c=user1.id;
            o4.OwnerId=user3.id;
            opprtlist.add(o4);
            insert opprtlist;
           
            FC_Reviewer__External_Review__c Fcr5 = new FC_Reviewer__External_Review__c();
            Fcr5.RecordTypeId = r3.id;
            Fcr5.FC_Reviewer__Opportunity__c = o3.id;
            Fcr5.FC_Reviewer__Reviewer_Type__c='In-Person';
            Fcr5.Panel__c = p.id;
            Fcr5.FC_Reviewer__Contact__c = con3.id;
            Fcr5.FC_Reviewer__Deadline__c = date.valueof(System.now());
        fcr5.OwnerId=user3.Id;
            insert Fcr5;
            
           
            Test.setCurrentPageReference(new PageReference('Page.LinksDownloadingPageInPersonPreview'));
            System.currentPageReference().getParameters().put('Id',Fcr5.Id);
            
              System.runAs(user3){
            DownLoadsTABforReviewsController page1 = new DownLoadsTABforReviewsController();
            
            
            List<SelectOption> gOption = page1.getopptoptions(); 
            
            pagereference pRef1 = page1.showlinks();
            System.assertEquals(null, pRef1);
            
            
            page1.selectedOpt = o3.Name;
            pRef1 = page1.showlinks();
            pagereference  pExpected1 = new PageReference(Label.LinksDownloadingPageInPersonPreview+Fcr5.Id);
            
            System.assertEquals(pExpected1.getAnchor(), pRef1.getAnchor());
           Fcr5.RecordTypeId = r2.id;
           update Fcr5;
            
           page1.selectedOpt = o3.Name;
            pRef1 = page1.showlinks();
            pagereference pExpected2 = new PageReference(Label.LinksDownloadingPageInPersonReview+Fcr5.Id);
             System.assertEquals(pExpected2.getAnchor(), pRef1.getAnchor());
            Test.stopTest();
            
            
        }
            
    
        
    }
    
    
}