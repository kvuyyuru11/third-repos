@isTest
public class FlowControllerTest{
    private static testMethod void ER(){
        RecordType rec = [SELECT Id FROM RecordType WHERE sObjectType = 'Engagement_Report__c' AND DeveloperName = 'Engagement_Report'];
        RecordType frec = [SELECT Id FROM RecordType WHERE sObjectType = 'Engagement_Report__c' AND DeveloperName = 'Final_Engagement_Report'];
        Opportunity opp = new Opportunity();
        opp.Name = 'Test';
         opp.Full_Project_Title__c = 'Test';
        opp.Project_Name_off_layout__c='Test';
        opp.StageName = 'Closed';
        opp.CloseDate = Date.newInstance(2016,5,15);
        insert opp;
        FGM_Base__Grantee_Report__c gr = new FGM_Base__Grantee_Report__c();
        gr.FGM_Base__Request__c = opp.Id;
        insert gr;
        Engagement_Report__c er = new Engagement_Report__c();
        er.RecordTypeId = rec.Id;
        er.Grantee_Report__c = gr.Id;
        insert er;
        Engagement_Report__c fer = new Engagement_Report__c();
        fer.RecordTypeId = frec.Id;
        fer.Grantee_Report__c = gr.Id;
        insert fer;
        PageReference pageRef = Page.EngReport_Flow;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', gr.Id);
        FlowController fc = new FlowController();
    }
    private static testMethod void ERBack(){
        RecordType rec = [SELECT Id FROM RecordType WHERE sObjectType = 'Engagement_Report__c' AND DeveloperName = 'Engagement_Report'];
        RecordType frec = [SELECT Id FROM RecordType WHERE sObjectType = 'Engagement_Report__c' AND DeveloperName = 'Final_Engagement_Report'];
        Opportunity opp = new Opportunity();
        opp.Name = 'Test';
         opp.Full_Project_Title__c = 'Test';
        opp.Project_Name_off_layout__c='Test';
        opp.StageName = 'Closed';
        opp.CloseDate = Date.newInstance(2016,5,15);
        insert opp;
        FGM_Base__Grantee_Report__c gr = new FGM_Base__Grantee_Report__c();
        gr.FGM_Base__Request__c = opp.Id;
        insert gr;
        Engagement_Report__c er = new Engagement_Report__c();
        er.RecordTypeId = rec.Id;
        er.Grantee_Report__c = gr.Id;
        insert er;
        Engagement_Report__c fer = new Engagement_Report__c();
        fer.RecordTypeId = frec.Id;
        fer.Grantee_Report__c = gr.Id;
        insert fer;
        PageReference pageRef = Page.EngReport_Flow;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', gr.Id);
        FlowController fc = new FlowController();
        fc.ReturnBack();
    }
    private static testMethod void FER(){
        RecordType rec = [SELECT Id FROM RecordType WHERE sObjectType = 'Engagement_Report__c' AND DeveloperName = 'Engagement_Report'];
        RecordType frec = [SELECT Id FROM RecordType WHERE sObjectType = 'Engagement_Report__c' AND DeveloperName = 'Final_Engagement_Report'];
        RecordType grRec = [SELECT Id FROM RecordType WHERE sObjectType = 'FGM_Base__Grantee_Report__c' AND Name = 'Final Progress Report - RA'];
        Opportunity opp = new Opportunity();
        opp.Name = 'Test';
         opp.Full_Project_Title__c = 'Test';
        opp.Project_Name_off_layout__c='Test';
        opp.StageName = 'Closed';
        opp.CloseDate = Date.newInstance(2016,5,15);
        insert opp;
        FGM_Base__Grantee_Report__c gr = new FGM_Base__Grantee_Report__c();
        gr.FGM_Base__Request__c = opp.Id;
        gr.RecordTypeId = grRec.Id;
        insert gr;
        Engagement_Report__c er = new Engagement_Report__c();
        er.RecordTypeId = rec.Id;
        er.Grantee_Report__c = gr.Id;
        insert er;
        Engagement_Report__c fer = new Engagement_Report__c();
        fer.RecordTypeId = frec.Id;
        fer.Grantee_Report__c = gr.Id;
        insert fer;
        PageReference pageRef = Page.EngReport_Flow;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', gr.Id);
        ApexPages.currentPage().getParameters().put('page', '5');
        FlowController fc = new FlowController();
    }
    private static testMethod void FER4(){
        RecordType rec = [SELECT Id FROM RecordType WHERE sObjectType = 'Engagement_Report__c' AND DeveloperName = 'Engagement_Report'];
        RecordType frec = [SELECT Id FROM RecordType WHERE sObjectType = 'Engagement_Report__c' AND DeveloperName = 'Final_Engagement_Report'];
        RecordType grRec = [SELECT Id FROM RecordType WHERE sObjectType = 'FGM_Base__Grantee_Report__c' AND Name = 'Final Progress Report - RA'];
        Opportunity opp = new Opportunity();
        opp.Name = 'Test';
         opp.Full_Project_Title__c = 'Test';
        opp.Project_Name_off_layout__c='Test';
        opp.StageName = 'Closed';
        opp.CloseDate = Date.newInstance(2016,5,15);
        insert opp;
        FGM_Base__Grantee_Report__c gr = new FGM_Base__Grantee_Report__c();
        gr.FGM_Base__Request__c = opp.Id;
        gr.RecordTypeId = grRec.Id;
        insert gr;
        Engagement_Report__c er = new Engagement_Report__c();
        er.RecordTypeId = rec.Id;
        er.Grantee_Report__c = gr.Id;
        insert er;
        Engagement_Report__c fer = new Engagement_Report__c();
        fer.RecordTypeId = frec.Id;
        fer.Grantee_Report__c = gr.Id;
        insert fer;
        PageReference pageRef = Page.EngReport_Flow;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', gr.Id);
        ApexPages.currentPage().getParameters().put('page', '4');
        FlowController fc = new FlowController();
    }
}