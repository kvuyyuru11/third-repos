/* This class belongs to Consolidated Trigger. This class will updatte Email fields in the IRB whenever Opportunity record is edited and updated*/
public with sharing class UpdateInIRBHelper {

    public UpdateInIRBHelper() {
        
    }
    public void updateEmailIds(List<Opportunity> oplist) { /* This Method will update Email fields in the IRB */
          
           Set<id> idList = new Set<Id>();
        for(Opportunity p : oplist){
           idList.add(p.id);
        }// for loop

        //IRB__c irb; 
                List<IRB__c> irbList = new List<IRB__c>();
                Map<Id, Opportunity> oppmap = new Map<Id, Opportunity>();
                Map<Id, IRB__C> irbmap = new Map<Id, IRB__C>();
                                      
     
            List<IRB__c> irbSOQLList =[SELECT id,Email_PI_Project_Lead_1__c, Email_PI_Project_Lead_2__c, Email_PI_Project_Lead_Designee_1__c, Email_PI_Project_Lead_Designee_2__c, Email_Program_Associate__c, Email_Program_Officer__c, Email_Administrative_Official__c, Email_Contract_Administrator__c, Email_Contract_Coordinator__c,Project_Title__c FROM IRB__c WHERE Project_Title__c IN : idList Limit 50000];
           System.debug('############# irbSOQLList'+irbSOQLList); 

        List<Opportunity> oppSOQLList = [SELECT Id, Name, PI_Name_User__c,PI_Name_User__r.email, PI_Project_Lead_2_Name_New__r.Email, PI_Project_Lead_Designee_1_New__r.Email, PI_Project_Lead_Designee_2_Name_New__r.Email,Program_Associate__r.Email,Program_Officer__r.Email,AO_Name_User__r.Email,Contract_Administrator__r.Email,Contract_Coordinator__r.Email FROM Opportunity where Id IN : idList Limit 50000]; 
             

                for(Opportunity opp: oppSOQLList)
                {
                   oppmap.put(opp.id, opp);

                }
             
                for(IRB__C irbc: irbSOQLList)
                {
                   irbmap.put(irbc.id, irbc);

                }
                
         try{
                for(IRB__c irb : irbSOQLList)
                 {
                            
                    irb.Email_PI_Project_Lead_1__c=oppmap.get(irb.Project_Title__c).PI_Name_User__r.email;
                    irb.Email_PI_Project_Lead_2__c = oppmap.get(irb.Project_Title__c).PI_Project_Lead_2_Name_New__r.Email;
                    irb.Email_PI_Project_Lead_Designee_1__c = oppmap.get(irb.Project_Title__c).PI_Project_Lead_Designee_1_New__r.Email;
                    irb.Email_PI_Project_Lead_Designee_2__c = oppmap.get(irb.Project_Title__c).PI_Project_Lead_Designee_2_Name_New__r.Email;
                    irb.Email_Program_Associate__c = oppmap.get(irb.Project_Title__c).Program_Associate__r.Email;
                    irb.Email_Program_Officer__c = oppmap.get(irb.Project_Title__c).Program_Officer__r.Email;
                    irb.Email_Administrative_Official__c = oppmap.get(irb.Project_Title__c).AO_Name_User__r.Email;
                    irb.Email_Contract_Administrator__c = oppmap.get(irb.Project_Title__c).Contract_Administrator__r.Email;
                    irb.Email_Contract_Coordinator__c = oppmap.get(irb.Project_Title__c).Contract_Coordinator__r.Email;
                   
                   irbList.add(irb);
                  
                                     
                }// for loop
              update irbList;  
           }catch(DmlException e) {
              System.debug('The following exception has occurred: ' + e.getMessage());       
           }Catch(Exception e)  {
               System.debug('The following exception has occurred: ' + e.getMessage());
           }// try catch block   
           
           
      
          }
    
}