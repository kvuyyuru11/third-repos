//This is a Class that routes a user to different MRA views based on Profile
public class RouterClass {
    PageReference pageref;
    ID  recdid;
    string PName = Label.Profile_Name_Candidate;
       profile p=[select id from Profile where name=:PName];
 ID MRCid=p.Id;
    public RouterClass(ApexPages.StandardController controller){}
      //This is a method that routes to the standard page layout detail view or VFpage view based on profile
public pagereference router(){
    //Gets the record id
      recdid =ApexPages.currentPage().getParameters().get('Id');
    //Checks the profile id and routes to the page required
if (UserInfo.getProfileId().equals(MRCid)) 
    {
     pageRef = new PageReference('/apex/MRAPage?id='+recdid);
pageref.setredirect(True);
        }
    else{
     pageRef = new PageReference('/'+recdID+'?nooverride=1');
        pageref.setRedirect(True);
        }
    return pageRef;
}

}