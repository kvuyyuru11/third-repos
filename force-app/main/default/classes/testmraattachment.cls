@istest
public class testmraattachment {   
    public static testmethod void attachmentmethod(){
    User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    Contact con1;
    list<contact> c;
    User user;
    List<Merit_Reviewer_Application__c> myapp1=new list<Merit_Reviewer_Application__c>();
    List<Merit_Reviewer_Application__c> myapp2=new list<Merit_Reviewer_Application__c>();
    System.runAs(usr){
    string streetstyle;
    String citystyle;
    String zipstyle;
    String statestyle1;
       // list<Merit_Reviewer_Application__c> myapp;
    
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
    Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
    insert acnt; 
    con1 = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                              Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
    insert con1;  
    

      user = new User(alias = 'test123', email='test123@noemail.com',
      emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
      localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
      ContactId = con1.Id,
      timezonesidkey='America/New_York', username='tester@noemailgoogle.com');
        
     insert user;
    c = [select id,Age__c,MailingCity ,MailingCountry ,Current_Employer__c,Current_Position_or_Title__c,Federal_Employee__c,Gender__c,Race__c,MailingState,MailingPostalcode,phone,mailingstreet from Contact where id=:user.contactId limit 1];  
 
    }

        //User u = [Select u.IsPortalEnabled, u.id, u.contactid, u.IsActive,u.profile.name From User u where u.isActive=true and  u.profile.name ='PCORI Community Basic' limit 1];
        System.RunAs(user) {
        
            Attachment__c  men1=new Attachment__c();
                 men1.name='Test';
                 men1.ownerid=userinfo.getUserId();
                 //men1.Attachment__c=attach1.id;
                 men1.Contact__c=con1.id;
                 Blob bodyBlob1=Blob.valueOf('Unit Test Attachment Body');
                 men1.File_Name__c='bodyBlob1';
            insert men1;
            
            
       Merit_Reviewer_Application__c a = new Merit_Reviewer_Application__c();           
         for(contact mycon1:c){
         
        a.Reviewer_Name__c=mycon1.id;
       a.Highest_Level_of_Education__c='';
       a.Highest_Degree_of_Education_New__c='';
       a.Number_of_Scientific_Panels__c='';
       a.Served_as_Chair_or_Co_Chair__c='';
       a.Number_of_Publications__c='';
       a.Stakeholder_Communities__c='';
       a.Patient_Communities__c='';
        a.Age__c='21';
        a.City__c='test';
        a.Country__c='United States' ;
        a.Current_Employer__c='test';
        a.Current_Position_or_Title__c='test';
        a.Federal_Employee__c='No';
        a.Gender__c='male';
       //a.Patient_Communities__c='';
        a.Phone__c='1234567890';
        a.Race__c='Test Race';
        a.Reviewer_Role__c='Stakeholder';
        a.State__c='Virginia';
        a.Street_Address__c='electric avenue';
        a.Zip_Code__c='12345';
        a.Application_Status__c='Draft';         
           myapp2.add(a);  
         }
           insert myapp2;
       
        Attachment attach = new Attachment();
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=a.id;
        insert attach;     
            
     for(Merit_Reviewer_Application__c mrr:[select id,Application_Status__c from Merit_Reviewer_Application__c where Id=:a.id]){
        mrr.Application_Status__c='Accepted as Reviewer';         
           myapp1.add(mrr);
            }
           update myapp1;

            Attachment__c  men=new Attachment__c();
                 men.name=attach.Name;
                 men.ownerid=userinfo.getUserId();
                 men.Attachment__c=attach.id;
                 //men.Contact__c=con.id;
                 men.File_Name__c=attach.contenttype;
            insert men;  

    }
  }
    public static testmethod void attachmentmethod1(){
     User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()]; 
            string streetstyle;
          string citystyle;
           string zipstyle;
           string statestyle1;
       // list<Merit_Reviewer_Application__c> myapp;
     Contact con1;
    list<contact> c;
    User user;
    List<Merit_Reviewer_Application__c> myapp1=new list<Merit_Reviewer_Application__c>();
    List<Merit_Reviewer_Application__c> myapp2=new list<Merit_Reviewer_Application__c>();
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();
    System.runAs(usr){
        Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
    insert acnt; 
    con1 = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                              Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
    insert con1;  
    

      user = new User(alias = 'test123', email='test123@noemail.com',
      emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
      localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
      ContactId = con1.Id,
      timezonesidkey='America/New_York', username='tester123@noemailaccenture.com');
        
     insert user;
             c = [select id,Age__c,MailingCity ,MailingCountry ,Current_Employer__c,Current_Position_or_Title__c,Federal_Employee__c,Gender__c,Race__c,MailingState,MailingPostalcode,phone,mailingstreet from Contact where id=:user.contactId limit 1];  
 
    }        
    
        //User u = [Select u.IsPortalEnabled, u.id, u.contactid, u.IsActive,u.profile.name From User u where u.isActive=true and  u.profile.name ='PCORI Community Basic' limit 1];
        System.RunAs(user) {            
       Merit_Reviewer_Application__c a = new Merit_Reviewer_Application__c();           
         for(contact mycon1:c){
         
        a.Reviewer_Name__c=mycon1.id;
       a.Highest_Level_of_Education__c='';
      a.Highest_Degree_of_Education_New__c='';
       a.Number_of_Scientific_Panels__c='';
       a.Served_as_Chair_or_Co_Chair__c='';
       a.Number_of_Publications__c='';
       a.Stakeholder_Communities__c='';
       a.Patient_Communities__c='';
        a.Age__c='21';
        a.City__c='test';
        a.Country__c='United States' ;
        a.Current_Employer__c='test';
        a.Current_Position_or_Title__c='test';
        a.Federal_Employee__c='No';
        a.Gender__c='male';
       //a.Patient_Communities__c='';
        a.Phone__c='1234567890';
        a.Race__c='Test Race';
        a.Reviewer_Role__c='Stakeholder';
        a.State__c='Virginia';
        a.Street_Address__c='electric avenue';
        a.Zip_Code__c='12345';
        a.Application_Status__c='Draft';         
           myapp2.add(a);  
         }
           insert myapp2;
       
        Attachment attach = new Attachment();
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=a.id;
        insert attach;     
            
     for(Merit_Reviewer_Application__c mrr:[select id,Application_Status__c from Merit_Reviewer_Application__c where Id=:a.id]){
        mrr.Application_Status__c='Accepted as Reviewer';         
           myapp1.add(mrr);
            }
           update myapp1;

            Attachment__c  men=new Attachment__c();
                 men.name=attach.Name;
                 men.ownerid=userinfo.getUserId();
                 men.Attachment__c=attach.id;
                 //men.Contact__c=con.id;
                 men.File_Name__c=attach.contenttype;
            insert men;  

    }
  }
}