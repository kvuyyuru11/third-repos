/**
* @author        Abhinav Polsani 
* @date          12/01/2017
* @description   This Controller retrives the sequentiality of P2P forms from Custom Settings and displays it on Page 
Modification Log:
-----------------------------------------------------------------------------------------------------------------
Developer                Date            Description
-----------------------------------------------------------------------------------------------------------------
Abhinav Polsani       12/01/2017      Initial Version
*/
public with sharing class P2PFormsController {

    public PipelineToProposal__c p2pRec;
    private final sObject mysObject;
    public P2PSequentialitySettings__c fieldSetNameStr { get; set;}
    public String fieldSetName { get; set;}
    public static Id p2pRecId { get; set;}
    public PipelineToProposal__c genP2P { get; set;}
    public static Boolean showDetail { get; set;}
    public static Boolean canSubmit { get; set;}
    public static Boolean lockEdit { get; set;}
    public static Boolean t3Check { get; set;}
    public static Boolean t3CompleteCheck { get; set;}
    public static Boolean showPartners { get; set;}
    //Instance variables for PDF view
    public transient String bodyTag {get;set;}
    public transient String renderType {get;set;}   

    /**
    * @author        Abhinav Polsani
    * @date          12/01/2017
    * @description   Constructer of PipelineToProposalController class, gets the fieldset and pushes it to Page.
    * @param         ApexPages.StandardSetController
    * @return        Constructer 
    */
    public P2PFormsController(ApexPages.StandardController stdController) {
        this.mysObject = (sObject)stdController.getRecord();
        p2pRecId = this.mysObject.Id;
        this.p2pRec = (PipelineToProposal__c)stdController.getRecord();
        Id projectId = this.p2pRec.Projects__c;        
        Map<Integer, P2PSequentialitySettings__c> fieldSetMap = getCustomSettings();
        Integer p2pCount = getP2PRecords(projectId);
        t3Check = false;
        t3CompleteCheck = false;        
        if (p2pCount != null && !fieldSetMap.isEmpty()) {
            fieldSetNameStr = getFieldSetName(p2pCount, fieldSetMap);
            if (fieldSetNameStr == null && p2pRecId == null) {
                t3CompleteCheck = true;
                return;
            }
            if (fieldSetNameStr != null &&fieldSetNameStr.Tier_3__c == true) {
                t3Check = true;
            }
        }
        if (p2pRecId == null) {
            showDetail = false;
        }
        if (p2pRecId != null && Schema.PipelineToProposal__c.SObjectType == p2pRecId.getSobjectType()) {
            showDetail = true;
            canSubmit = false; 
            lockEdit = true;
            showPartners = false;           
            string fieldsetNa = [SELECT Id, FieldSetName__c FROM PipelineToProposal__c WHERE Id =:p2pRecId].FieldSetName__c;            
            if (fieldsetNa != null && fieldsetNa != '') {
                fieldSetNameStr = P2PSequentialitySettings__c.getValues(fieldsetNa);
                if (fieldSetNameStr != null) {
                    List<Schema.FieldSetMember> listOfFeilds =  readFieldSet(fieldSetNameStr.FieldSetName__c  , 'PipelineToProposal__c' );
                    genP2P  = getPipelineToProposalDynamiCSOQL(p2pRecId , listOfFeilds );
                    if (genP2P.IsLocked__c == true) {
                        lockEdit = false;
                    }
                    if (fieldSetNameStr.ShowSubmit__c == true) {
                        canSubmit = true;
                    }
                    if (fieldSetNameStr.HavePartners__c == true) {
                        showPartners = true;
                    }
                }
            }
        } else {
            genP2P = p2pRec ;
            genP2P.Name = fieldSetNameStr.FormName__c;
            genP2P.Types__c = fieldSetNameStr.FormName__c;
            genP2P.FieldSetName__c = fieldSetNameStr.FormName__c;
            system.debug('check here ' + genP2P);
        }
    }

    /**
    * @author        Abhinav Polsani
    * @date          12/01/2017
    * @description   This method returns map of sequentiality number and its corresponding setting.
    * @param         None
    * @return        Map<Integer, P2PSequentialitySettings__c>
    */
    public static Map<Integer, P2PSequentialitySettings__c> getCustomSettings() {
        Map<Integer, P2PSequentialitySettings__c> fieldSetMap = new Map<Integer, P2PSequentialitySettings__c>();
        List<P2PSequentialitySettings__c> settingsP2P = P2PSequentialitySettings__c.getAll().values();
        System.debug('Get All Output ' + settingsP2P);
        for (P2PSequentialitySettings__c settings : settingsP2P) {
            fieldSetMap.put(Integer.valueOf(settings.OrderNumber__c), settings);
        }
        return fieldSetMap;
    }
      //Anudeep Changing VF page to a PDF
    public void pdfView() 
    {   
       
        
        //Changing applyBodyTag component to false in apex:page tags output pages   
        bodyTag = 'false';
        
        //Changing RenderAs component in Apex:page tag to render page as pdf
        renderType = 'pdf';      
    }

    /**
    * @author        Abhinav Polsani
    * @date          12/01/2017
    * @description   This method returns map of Form Name and its corresponding setting.
    * @param         None
    * @return        Map<String, P2PSequentialitySettings__c>
    */
    public static Map<String, P2PSequentialitySettings__c> getCustomSettingsByTierName() {
        Map<String, P2PSequentialitySettings__c> fieldSetMap = new Map<String, P2PSequentialitySettings__c>();
        List<P2PSequentialitySettings__c> settingsP2P = P2PSequentialitySettings__c.getAll().values();
        System.debug('Get All Output ' + settingsP2P);
        for (P2PSequentialitySettings__c settings : settingsP2P) {
            fieldSetMap.put(String.valueOf(settings.FormName__c), settings);
        }
        return fieldSetMap;
    }

    /**
    * @author        Abhinav Polsani
    * @date          12/01/2017
    * @description   This method returns all P2P records count which are related to that specific project. 
    * @param         Id 
    * @return        Integer
    */
    public static Integer getP2PRecords(Id pProjectId) {
        return [
            SELECT
                Count()
            FROM
                PipelineToProposal__c
            WHERE
                Projects__c =:pProjectId
        ];
    }

    /**
    * @author        Abhinav Polsani
    * @date          12/01/2017
    * @description   This method returns specific setting which is related to the given sequentiality. 
    * @param         Integer, Map<Integer, P2PSequentialitySettings__c>
    * @return        P2PSequentialitySettings__c
    */
    public static P2PSequentialitySettings__c  getFieldSetName(Integer pCount, Map<Integer, P2PSequentialitySettings__c> pFieldSetMap) {
        if (pFieldSetMap.containsKey(pCount+1)) {
            return pFieldSetMap.get(pCount+1);
        }
        return null;
    }

    /**
    * @author        Abhinav Polsani
    * @date          12/01/2017
    * @description   This method redirects to the Same VF page by passing P2P rec Id  
    * @param         None
    * @return        PageReference
    */
    public PageReference editP2p() {
        if (genP2P.IsLocked__c) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'The Report is already submitted you cannot edit it, please request to un-lock the form'));
            return null;
        } else {
            showDetail = false;
            return new PageReference('/apex/P2PFormsEdit?id=' + genP2P.id);
        }
    }
   
    /**
    * @author        Abhinav Polsani
    * @date          12/01/2017
    * @description   This method redirects to detail page(Same VF page) after saving the record.  
    * @param         None
    * @return        PageReference
    */
     public PageReference savep2p(){
         try {

            upsert genP2P ;  
            showDetail = true;
            pagereference pagref = new PageReference('/apex/P2PFormsDetail?id=' + genP2P.id) ;
            pagref.setredirect(true);    
            return pagref ;
            
        } catch(DMLException ex) {
            system.debug('======>'+ex.getmessage());
            //throw ex;
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, ex.getmessage() ));  
            
           // pagereference pagref = new PageReference(Apexpages.currentPage().getUrl()) ;
            //pagref.getParameters().put('err',ex.getmessage());
            
            //errorMessage = ex.getmessage();
            //urlpage =  Apexpages.currentPage().getUrl() ;
            //pagref.setredirect(False);    
           // return pagref ;
            return null;
            
        }
        return null;
    }
   
   /***
   public pagereference reloadWindow(){
        pagereference pagref = new PageReference(Apexpages.currentPage().getUrl()) ;
        pagref.setredirect(true);    
        return pagref ;
   }**/
   
    
    /**
    * @author        Abhinav Polsani
    * @date          12/01/2017
    * @description   This method takes fieldset name and object name and gets all fields as Schema.FieldSetMember List   
    * @param         String, String 
    * @return        List<Schema.FieldSetMember>
    */
    public static List<Schema.FieldSetMember> readFieldSet(String fieldSetName, String ObjectName) {
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        //system.debug('====>' + DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName));
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
        return fieldSetObj.getFields(); 
    }  
    
    /**
    * @author        Abhinav Polsani
    * @date          12/01/2017
    * @description   This method takes List of fields, recordId and generate a query String and queries on object.      
    * @param         String, String 
    * @return        List<Schema.FieldSetMember>
    */
    private PipelineToProposal__c getPipelineToProposalDynamiCSOQL(String recId , List<Schema.FieldSetMember> listOfFeilds ) {
        try {
            String query = 'SELECT ';
            for (Schema.FieldSetMember f : listOfFeilds ) {
                query += f.getFieldPath() + ', ';
            }
            query += 'Id, Name, Projects__c, Types__c, IsLocked__c, Projects__r.Proceed_To_Tier_3__c FROM PipelineToProposal__c WHERE Id = \''+recId+'\'';
            return Database.query(query);
        } catch (Exception e) {
            system.debug('===>'+ e);
        }
        return null;
    }
    
    /**
    * @author        Abhinav Polsani
    * @date          12/01/2017
    * @description   On hit of cancel button this method directs to related Project.      
    * @param         None 
    * @return        PageReference
    */
    public PageReference CancelPage(){
        return new PageReference('/' + genP2P.Projects__c) ;
    }

    /**
    * @author        Abhinav Polsani
    * @date          01/24/2018
    * @description   When the P2P Form is loaded this method inserts P2P record and directs to detail page.      
    * @param         None 
    * @return        PageReference
    */
    public PageReference directToDetail() {
        //String baseUrl = System.URL.getSalesforceBaseUrl().toExternalForm();
        //String url = '<a href="'baseUrl'+'/'+'this.p2pRec.Projects__c'">here</a>';
        if (t3CompleteCheck) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, 'You have completed all Tier III Reports! Please Click back in browser to return to Project'));
            return null;
        }
        Opportunity opp = [SELECT Proceed_To_Tier_3__c FROM Opportunity WHERE Id =: genP2P.Projects__c];
        if (opp != null && !opp.Proceed_To_Tier_3__c && t3Check) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, 'You have completed all Tier II Reports! Please Click back in browser to return to Project'));
            return null;
        } else {
            insert genP2P;
            pagereference pagref = new PageReference('/apex/P2PFormsDetail?id=' + genP2P.id) ;
            pagref.setredirect(true);    
            return pagref;
        }
        return null;
    }
}