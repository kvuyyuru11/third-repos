public without sharing class listdirect{
     public Merit_Reviewer_Evaluation__c myev{get;set;} 
    public listdirect(ApexPages.StandardController controller){
    }
      public PageReference redirectDefaultLead1(){
        String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        User CurrentUser = [Select id, name, UserRoleId from User where Id = :Userinfo.getUserId()];
     id  recdid =ApexPages.currentPage().getParameters().get('Id'); 
       myev=[select id,name,recordtypeid from Merit_Reviewer_Evaluation__c where id=:recdid];
           RecordType RecType = [Select Id From RecordType  Where SobjectType = 'Merit_Reviewer_Evaluation__c' and DeveloperName = 'Mentor_Evaluation'];
    if(myev.recordtypeid==RecType.Id){
        pagereference pg =new pagereference('/apex/MentorEvaluation?id='+myev.Id);
       pg.setRedirect(True);
        return pg;
    }else{
PageReference newPage1 = new PageReference('/'+myev.Id);
newPage1.setRedirect(true);
newPage1.getParameters().put('nooverride', '1');
return newpage1;
    }
        return null;
        }
   
    }