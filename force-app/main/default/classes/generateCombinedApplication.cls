/*-------------------------------------------------------------------------------------------------------------------------------------------------------------

Code Written by - Pradeep Bokkala
Updated : 10/20/2017
Updates are related to SO-816

Test Class - generateCombinedApplication _Test
Parent Component - Generate PDF -- RA Applications(Custom Link on Opportunity Object)
Used in senario(s) - Used to generate combined PDF for MRO's for 'RA Applications' Recordtype. This class is mainly used
to arrange the documents in the PDF which is a request from MR team

Below are the Key points
1.Attach the Research Plan after the Project template
2.If Research Plan size is greater than 18MB then return a string saying 'Cannot Create PDF'
3.If the Project is a resubmission Project, then add the Prior summary statement which will be uploaded by the CMA to the Notes and
attachment section of the Project with a naming convention which is stored in the variable 'cmaName'
4. If the Project is associated to a D&I PFA then use a different Budget template because D&I budget has 'year 6' instead of 'Peer Review Period'
5. If the Project is associated to a D&I PFA then use a different Budget template because D&I budget has 'Peer Review Period' instead of 'year 6' 
------------------------------------------------------------------------------------------------------------------------------------------------------------*/

global class generateCombinedApplication {
    public generateCombinedApplication() {
        
    }
    webservice static string updateCongaRequestLink(Id opportunityId, string cmaName, string staticUrl, string templateId, string sid, string surl){
        //staticUrl = 'https://composer.congamerge.com?sessionId='+sessionId+'&serverUrl='+serverUrl+'&id='+opportunityFifteenDigitId+'&Qvar0Id=a1C3900000GljpY&Qvar1Id=a1C3900000Gljpi&Qvar2Id=a1C3900000Gljps&Qvar3Id=a1C0x00000064g2&SelectTemplates=0&ReportId=00O390000055dFl,00O390000055dFm,00O39000004yyHC,00O39000004yyH9,00O39000004yyHI,00O39000004yyHF,00O39000004yyHE,00O39000004yyHD,00O39000004yyHB,00O39000004yyHG,00O39000004yyHH,00O39000004yyHA,00O39000004yyH8&DefaultPDF=1&OFN='+outputFileName+'&FPO=1&SC0=1&SC1=Attachments&DS7=1&QMode=SalesforceFile';
        String sessionId = UserInfo.getSessionId();
        String serverUrl = string.valueof(URL.getSalesforceBaseUrl().toExternalForm());
        string outputFileName='';
        boolean researchPlanAdded = false;
        boolean milestoneAdded = false;
        //boolean researchPlanSizeInLimits = false;
        Id researchPlanId;
        String milestoneId='';
        Id priorSummaryStatementId;
        string initialUrl = '';
        string extensionUrl = '';
        string finalUrl = '';
        string opportunityFifteenDigitId = string.valueOf(opportunityId).substring(0,15);
        string sessionServerUrl = 'https://composer.congamerge.com?sessionId=' + sessionId + '&serverUrl=' + serverUrl + '&id=' + opportunityFifteenDigitId;
        List<Opportunity> opportunityList = new List<Opportunity>(getOpportunity(opportunityId));
        List<Attachment> opportunityAttachmentsList = new List <Attachment> (getOpportunityAttachments(opportunityId));
        Map<Id, FGM_Portal__Question_Attachment__c> questionAttachmentsMap = new Map<Id, FGM_Portal__Question_Attachment__c>(getQuestionAttachments(opportunityId));
        
        //Grab the Research Plan to add it between the conga Templates
        
        for (Attachment at : getAttachments(opportunityId)){
            if(questionAttachmentsMap.get(at.ParentId).Question_Text__c == 'Research Plan' || 
               questionAttachmentsMap.get(at.ParentId).Question_Text__c == 'Project Plan'){
                   system.debug('the size of the attachment is ' + at.BodyLength);
                   //if(at.BodyLength < 10000000){
                   researchPlanAdded = true;
                   //researchPlanSizeInLimits = true;
                   researchPlanId = at.Id;
                   //}else{
                   //extensionUrl = 'Research Plan or Project Plan document size is more than 10 MB. Please create the PDF Manually';
                   //}
               }
            if(questionAttachmentsMap.get(at.ParentId).Question_Text__c == 'Milestones'){
               
                milestoneId = String.ValueOf(at.Id)+',';
                milestoneAdded = True;
            }
        }
        
        //Grab the Summary Statement from the Attachements associated to Opportunity
        
        for(Attachment at : opportunityAttachmentsList){
            if(at.Name.contains(cmaName) || at.Name.contains(cmaName + '.pdf')){
                priorSummaryStatementId = at.Id;
            }
        }
        if(!opportunityList.isEmpty()){
            initialUrl = staticUrl.replace('sessionId=','sessionId=' + sid).replace('serverUrl=','serverUrl=' + surl).replace('&id=','&id=' + opportunityFifteenDigitId);
            outputFileName = '&OFN=' + (('MRO_Combined '+opportunityList[0].name+' '+opportunityList[0].Application_Number__c).replace('&',' '));
            extensionUrl = getTemplateIdAndAttachmentId(opportunityList, researchPlanAdded, researchPlanId, priorSummaryStatementId, templateId, milestoneId);
        }
        
        //Return the final string to the custom button
        
        if(extensionUrl == 'Research Plan is Missing in the Applicant Attachments'){
            finalUrl = extensionUrl;
        }
        //else if(extensionUrl == 'Research Plan or Project Plan document size is more than 10 MB. Please create the PDF Manually'){
        //finalUrl = extensionUrl;
        //} 
        else if(extensionUrl == 'Previous Summary Statement is Missing from the Attachments'){
            finalUrl = extensionUrl;
        }
        else{
            finalUrl = initialUrl + extensionUrl + outputFileName;
        }
        return finalUrl;      
    }
    
    private static List<Opportunity> getOpportunity(Id opportunityId){
        return [Select Id, 
                Campaign.Name, 
                D_I_Resubmission__c, 
                App_Resubmission__c, 
                Name, 
                Application_Number__c, 
                Panel__r.Name 
                From 
                Opportunity 
                Where 
                Id =: opportunityId];
    }
    
    private static List<Attachment> getOpportunityAttachments(Id opportunityId){
        return [Select Id,
                Name,
                BodyLength,
                ParentId 
                From 
                Attachment 
                Where 
                ParentId =: opportunityId];
    }
    
    
    private static Map<Id, FGM_Portal__Question_Attachment__c> getQuestionAttachments(Id opportunityId){
        Map<Id, FGM_Portal__Question_Attachment__c> questionAttachmentsMap = new Map<Id, FGM_Portal__Question_Attachment__c>([Select Id, Name, Question_Text__c, FGM_Portal__Opportunity__c from FGM_Portal__Question_Attachment__c where FGM_Portal__Opportunity__c =: opportunityId]);
        return questionAttachmentsMap;
    }
    
    
    private static List<Attachment> getAttachments(Id opportunityId){
        return ([Select Id,
                 Name,
                 BodyLength,
                 ParentId 
                 From 
                 Attachment 
                 Where 
                 ParentId IN (Select Id 
                              From 
                              FGM_Portal__Question_Attachment__c 
                              Where 
                              FGM_Portal__Opportunity__c =: opportunityId)]);
    }
    
    public static string getTemplateIdAndAttachmentId(List<Opportunity> opportunityList, boolean researchPlanAdded, id researchPlanId, id priorSummaryStatementId, string templateId, String milestoneId){
        string constructedUrl = '';
        if(researchPlanAdded){
            if(opportunityList[0].D_I_Resubmission__c == 'Yes' || opportunityList[0].App_Resubmission__c == 'Yes'){
                constructedUrl = constructUrlForResubmission(opportunityList, researchPlanId, priorSummaryStatementId, templateId, milestoneId);
            }else{
                constructedUrl = constructUrlForNonResubmission(opportunityList, researchPlanId, templateId, milestoneId);
            }
            
        }else{
            constructedUrl = 'Research Plan is Missing in the Applicant Attachments';
        }
        return constructedUrl;
    }
    
    
    /*
Construct the URL for Non Resubmission (Includes the Resubmission attachment uploaded by the Applicant on the Quiz) for
three different Applications.
1. D&I
2. SDM
3. Non D&I
*/ 
    public static string constructUrlForResubmission(List<Opportunity> opportunityList, id researchPlanId, id priorSummaryStatementId, string templateId, String milestoneId){
        string resubmissionUrl = '';
        system.debug('the Prior Sunmmary statement Id is '+priorSummaryStatementId);
        if(String.valueOf(priorSummaryStatementId) != null && String.valueOf(priorSummaryStatementId) != ''){
            if(opportunityList[0].Campaign.Name == Label.Dissemination_and_Implementation)
            {    
                resubmissionUrl = '&TemplateId=' + templateId + ',' + researchPlanId + ',' + milestoneId + Label.BudgetTemplate_for_D_I+'&AttachmentId={Qvar1},{Qvar5},{Qvar0},{Qvar2},{Qvar3},' + priorSummaryStatementId;
            }
            else if(opportunityList[0].Campaign.Name.contains('Shared Decision Making'))
            {
                resubmissionUrl = '&TemplateId=' + templateId + ',' + researchPlanId + ',' + milestoneId + Label.BudgetTemplate_for_D_I+'&AttachmentId={Qvar1},{Qvar5},{Qvar4},{Qvar0},{Qvar2},{Qvar3},' + priorSummaryStatementId;
            }
            else{
                resubmissionUrl = '&TemplateId=' + templateId + ',' + researchPlanId + ',' + milestoneId + Label.BudgetTemplate_for_non_D_I+'&AttachmentId={Qvar1},{Qvar0},{Qvar2},{Qvar3},' + priorSummaryStatementId;
            }
        }
        else{
            resubmissionUrl = 'Previous Summary Statement is Missing from the Attachments';
        }
        return resubmissionUrl;
    }
    
    /*
Construct the URL for Non Resubmission (Doesn't include the Resubmission attachment uploaded by the Applicant on the Quiz) for
three different Applications.
1. D&I
2. SDM
3. Non D&I
*/ 
    
    public static string constructUrlForNonResubmission(List<Opportunity> opportunityList, id researchPlanId, string templateId, string milestoneId){
        string nonResubmissionUrl = '';
        if(opportunityList[0].Campaign.Name == Label.Dissemination_and_Implementation)
        {
            nonResubmissionUrl = '&TemplateId=' + templateId + ',' + researchPlanId + ',' + milestoneId + Label.BudgetTemplate_for_D_I+'&AttachmentId={Qvar1},{Qvar5},{Qvar0},{Qvar2},{Qvar3}';
        }
        else if(opportunityList[0].Campaign.Name.contains('Shared Decision Making')){
            nonResubmissionUrl = '&TemplateId=' + templateId + ',' + researchPlanId + ',' + milestoneId + Label.BudgetTemplate_for_D_I+'&AttachmentId={Qvar1},{Qvar5},{Qvar4},{Qvar0},{Qvar2},{Qvar3}';
        }
        else
        {
            nonResubmissionUrl = '&TemplateId=' + templateId + ',' + researchPlanId + ',' + milestoneId + Label.BudgetTemplate_for_non_D_I+'&AttachmentId={Qvar1},{Qvar0},{Qvar2},{Qvar3}';
        }
        return nonResubmissionUrl;
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------
Updates Related to : SO-816
Explanation of what the method below is about to do:
1. Whenever the MRO's click on the Generate PDF link on the RA Application page layout the requirement was to associate a PDF
to the quiz that is associated to the PFA of the Application.

2. Inorder to do this we need to create some records in the Foundation connect Managed package object which will help us in displaying
the PDF on the quiz.

3. The object relation model for the Foundation connect is as below
- Quiz is the Parent of all the objects including Campaign
- Question is a child to Quiz
- Quiz Question is a Junction Object Between Quiz, Question, Related List Controller Objects
- Related List Controller Object (Stores the Data related to quiz tabs) is a parent to Questions Object

4. So Inorder to Attach an attachment to the Templates and Uploads section with a new Question, we need to do the Followinf
- Create a Question (If not Present) with a Type of Attachment
- Create a Quiz Question record to associate the question to the Temlates and Uploads Tab
- Create a Question Attacment Record and Associate it to the Question created in the first Step
- Create an attachment and associate it to the Question Attachment record created in the above step.
---------------------------------------------------------------------------------------------------------------------------------------*/
    
    webService static void updateQuiz(Id OppId){
        
        //Query Opportunity Record
        List<Opportunity> OpportunityList = [select Id,Name,Campaign.EndDate,Campaign.FGM_Portal__Application_Quiz__c,CampaignId,Application_Number__c,PI_Name_User__r.LastName from Opportunity where id=:OppId];
        
        //Query Related List controller records. This is nothing but the Tabs on the quiz
        List<FGM_Portal__Related_List_Controller__c> RelatedList = [select Id,name,FGM_Portal__Quiz__c from FGM_Portal__Related_List_Controller__c where FGM_Portal__Quiz__c=:OpportunityList[0].Campaign.FGM_Portal__Application_Quiz__c AND (Name='Templates & Uploads' OR Name='Templates and Uploads')];
        
        //Query the questions
        List<FGM_Portal__Questions__c> QuestionList = [Select Id,name,FGM_Portal__Question__c,FGM_Portal__Quiz__c,FGM_Portal__Type__c from FGM_Portal__Questions__c where FGM_Portal__Type__c='Attachment' AND FGM_Portal__Quiz__c=:OpportunityList[0].Campaign.FGM_Portal__Application_Quiz__c];
        
        //Query the question attachments associated to the Opportunity
        List<FGM_Portal__Question_Attachment__c> QuestionAttachmentList = [Select Id,name,Question_Text__c from FGM_Portal__Question_Attachment__c where Question_Text__c='Budget Detailed Attachment' AND FGM_Portal__Opportunity__c=:OppId];
        //List<Attachment> AttachmentList = [Select Id,name,Body,ParentId from Attachment where ParentId=:OppId and Name='Budget Detailed Attachment.docx'];
        List<Attachment> budgetQuestionAttachmentList = new List<Attachment>();
        if(!QuestionAttachmentList.isEmpty()){
            budgetQuestionAttachmentList = [Select Id, Name, ParentId from Attachment Where ParentId =: QuestionAttachmentList[0].Id];
        }
        
        String str2 = OpportunityList[0].PI_Name_User__r.LastName+' '+OpportunityList[0].Application_Number__c+' Application Budget.pdf';
        
        List<contentdocumentLink> ContentDocumentList = [Select Id,ContentDocumentId from contentdocumentLink WHERE LinkedEntityId =:OppId AND ContentDocumentId IN (Select Id from ContentDocument where Title LIKE '%Application Budget.pdf') Limit 1];
        
        //Instantiate variables to use them in the code below
        List<ContentVersion> ContentVersionList = new List<ContentVersion>();
        Boolean ProcessAction = false;
        Id BufferQuestionId;
        
        //Verify whether the Budget PDF Content document is generated(Generated during the AO Submission) 
        if(!ContentDocumentList.isEmpty()){
            ContentVersionList = [select id,VersionData,Title,description,pathonclient,ownerid,contentsize from contentversion where islatest=true AND ContentDocumentId =: ContentDocumentList[0].ContentDocumentId];
        }
        
        //Verify whether the Budget question Attachment is Associated to the Opportunity
        for(FGM_Portal__Questions__c q : QuestionList){
            if(q.FGM_Portal__Question__c == 'Budget Detailed Attachment'){
                BufferQuestionId = q.Id;
                ProcessAction = True;
            }
        }
        
        //If there is no question associated 
        if(!ProcessAction && OpportunityList[0].Campaign.EndDate<System.today()){
            
            //Create a question
            FGM_Portal__Questions__c question = new FGM_Portal__Questions__c();
            question.FGM_Portal__Quiz__c = OpportunityList[0].Campaign.FGM_Portal__Application_Quiz__c;
            question.FGM_Portal__Is_Active__c = True;
            question.FGM_Portal__Type__c = 'Attachment';
            question.FGM_Portal__Question__c = 'Budget Detailed Attachment';
            insert question;
            
            //Associate the question to the Quiz
            FGM_Portal__Quiz_Questions__c quizquestion = new FGM_Portal__Quiz_Questions__c();
            quizquestion.FGM_Portal__Question__c = question.Id;
            quizquestion.FGM_Portal__Quiz__c = OpportunityList[0].Campaign.FGM_Portal__Application_Quiz__c;
            quizquestion.FGM_Portal__Tab__c = RelatedList[0].Id;
            insert quizquestion;
            
            //Insert a question attachment and associate it to the Opportunity and the Question
            FGM_Portal__Question_Attachment__c questionAttachment = new FGM_Portal__Question_Attachment__c();
            questionAttachment.FGM_Portal__Question__c = question.Id;
            questionAttachment.FGM_Portal__Opportunity__c = OppId;
            insert questionAttachment;
            
            //Make sure the Budget PDF is generated to the Application
            if(!ContentVersionList.isEmpty()){
                Attachment Atta = new Attachment();
                Atta.Name = str2;
                String VerData = encodingUtil.base64Encode(ContentVersionList[0].versiondata);
                Atta.Body = EncodingUtil.base64Decode(VerData);
                Atta.ParentId = questionAttachment.Id;
                Atta.OwnerId = '00570000002eX5e';
                insert Atta;
            }
        }//end of If condition
        
        
        else if(QuestionAttachmentList.isEmpty() && OpportunityList[0].Campaign.EndDate<System.today()){
            
            //Insert a question Attachment and associate it to the question.
            FGM_Portal__Question_Attachment__c questionAttachment = new FGM_Portal__Question_Attachment__c();
            questionAttachment.FGM_Portal__Question__c = BufferQuestionId;
            questionAttachment.FGM_Portal__Opportunity__c = OppId;
            insert questionAttachment;
            
            if(!ContentVersionList.isEmpty()){
                Attachment Atta = new Attachment();
                Atta.Name = str2;
                String VerData = encodingUtil.base64Encode(ContentVersionList[0].versiondata);
                Atta.Body = EncodingUtil.base64Decode(VerData);
                Atta.ParentId = questionAttachment.Id;
                Atta.OwnerId = '00570000002eX5e';
                insert Atta;
            }
        }//end of else condition
        
        else if(!QuestionAttachmentList.isEmpty() && budgetQuestionAttachmentList.isEmpty()){
            if(!ContentVersionList.isEmpty()){
                Attachment Atta = new Attachment();
                Atta.Name = str2;
                String VerData = encodingUtil.base64Encode(ContentVersionList[0].versiondata);
                Atta.Body = EncodingUtil.base64Decode(VerData);
                Atta.ParentId = QuestionAttachmentList[0].Id;
                Atta.OwnerId = '00570000002eX5e';
                insert Atta;
            }
        }
    }//end of the updateQuiz Method
    
}