/*-------------------------------------------------------------------------------------------------------------------------------------------------------------

Code Written at by - Pradeep Bokkala
Test Class - RRCAUserRecordsRequest_Test
Parent Component - ConsolidatedTrigger
Used in senario(s) - This class is used in RRCA process where we need to deal with User and Contact records creation/updation
------------------------------------------------------------------------------------------------------------------------------------------------------------*/
public without sharing class RRCAUserRecordsRequest{
    
    Public Void ProcessOpportunities(Opportunity Opp, String EmailToProcess){
        
        //Instantiate various Maps and Variables to use them below
        Map<String,String> fieldsMap = new Map<String,String>();
        List<Contact> ContactList = new List<Contact>();
        List<User> UserList = new List<User>(); 
        
        //Decide whether the trigger is looking for PI or AO   
        if(EmailToProcess=='Project_Lead_Email_Address__c'){
            fieldsMap.put(EmailToProcess,EmailToProcess);
            fieldsMap.put('LastName','Project_Lead_Last_Name__c');
            fieldsMap.put('FirstName','Project_Lead_First_Nam__c');
            fieldsMap.put('AssociatingUser','PI_Name_User__c');
            fieldsMap.put('Email',EmailToProcess);
        }
        
         //Decide whether the trigger is looking for PI or AO 
        if(EmailToProcess=='AO_Email__c'){
            fieldsMap.put(EmailToProcess,EmailToProcess);
            fieldsMap.put('LastName','RRCA_AO_Last_Name__c');
            fieldsMap.put('FirstName','RRCA_AO_First_Name__c');
            fieldsMap.put('AssociatingUser','AO_Name_User__c');
            fieldsMap.put('Email',EmailToProcess);
        }
        
        Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RRCA').getRecordTypeId();
        List<sObject> objects = new List<sObject>();
        Boolean ExistingActiveuser=false;
        
        //Use the functionality for RRCA recordtype only
        if(Opp.RecordTypeId == recordTypeId){
        
            //Verify whether the Associated user is not null and Email is not null
            if((Opp.get(EmailToProcess)!=null && Opp.get(EmailToProcess)!='') && (Opp.get(fieldsMap.get('AssociatingUser'))==null || Opp.get(fieldsMap.get('AssociatingUser'))=='')){
                ContactList=[select Id,name,email,Account.Ispartner from contact where email=:(String)Opp.get(EmailToProcess) LIMIT 1];
                
                //If the contact exists then 
                if(ContactList!=null && ContactList.size()>0){
                    UserList=[select Id,name,contactId from user where contactId=:ContactList[0].id];
                    ExistingActiveuser=true;
                }
                
                if(UserList.size()==0 || UserList==null){
                    UserList=[select Id,name,contactId from user where email=:(String)Opp.get(EmailToProcess) AND ContactId=null LIMIT 1];
                }
                
                //If both user and contact Exists then just simply share the Opportunity record
                if(UserList!=null && UserList.size()>0 && ExistingActiveuser){
                    CreateorUpdateRecords(Opp,ContactList[0],UserList[0],'ShareRecordWithUser',fieldsMap);
                    system.debug('Yes User and Yes Contact' + ContactList[0]+UserList[0]);
                }
                
                
                //No User, Yes Contact - If contact exists and user doesnt exist then create User and associate contact to the User
                if(UserList.size()==0 || UserList==null){
                    if(ContactList!=null && ContactList.size()>0){
                        CreateorUpdateRecords(Opp,ContactList[0],null,'CreateUser',fieldsMap);
                        system.debug('No User and Yes Contact'+ContactList[0]);
                    }  
                }
                
                //If both user and contact doesn't exist then create Contact and User record
                if(UserList.size()==0 || UserList==null){
                    if(ContactList.size()==0 || ContactList==null){
                        CreateorUpdateRecords(Opp,null,null,'CreateUserandCreateContact',fieldsMap);
                        system.debug('No user and No Contact');
                    }
                }
                
            }
        }
    }
    

    /*This is the main method which handeles all the logic. It is used to create contact, create users and Provide access to the users 
    on Opportunity*/
    
    Public void CreateorUpdateRecords(Opportunity Opp, Contact cnt, User usr, String Str, Map<String,String> fieldsMap){
        Boolean Contactflag = false;
        Boolean Userflag = false;
        User usr2 = new User();
        contact con2 = new Contact();
        
        //If there contact doesn't exist, create a contact
        if (Str.contains('CreateContact')){
            
            Contact con = new Contact(FirstName=(String)Opp.get(fieldsMap.get('FirstName')),LastName=(String)Opp.get(fieldsMap.get('LastName')),Email=(String)Opp.get(fieldsMap.get('Email')),Community__c='TBD',FGM_Portal__Username__c=((String)Opp.get(fieldsMap.get('Email'))).replace('@',''),FGM_Portal__Password__c='PCORI1234',FGM_Portal__Confirm_Password__c='PCORI1234',FGM_Portal__User_Profile__c='Grantee',FGM_Portal__IsActive__c=true,RecordTypeId=Label.Standard_Contact_Record_Type,OwnerId=Label.RRCA_OwnerId,AccountId=Opp.AccountId);
            insert con;
            con2 = con;
            Contactflag=true;
            system.debug('====>'+con2);
        }
        
        //If user doesn't exist and contact us newly created then create the user and associate the user to the contact
        if(Str.contains('CreateUser') && Contactflag){
            try{
            User u = new User(FirstName=(String)Opp.get(fieldsMap.get('FirstName')),LastName=(String)Opp.get(fieldsMap.get('LastName')),Email=(String)Opp.get(fieldsMap.get('Email')),Username=(String)Opp.get(fieldsMap.get('Email')),ProfileId=Label.MRA_Profile,contactId=con2.Id,languagelocalekey='en_US',IsActive = true,timezonesidkey='America/New_York', emailencodingkey='UTF-8',alias=((String)Opp.get(fieldsMap.get('FirstName'))).trim().substring(0,1)+((String)Opp.get(fieldsMap.get('LastName'))).trim().substring(0,1),localesidkey='en_US');
            insert u;
            usr2 = u;
            Userflag = true;
            system.debug('====>'+u);
            str = 'ShareRecordWithUser';
            }
            Catch(Exception e){
            system.debug('=====>'+e);
            }
            
            
        }
        
        //If user doesn't exist but contact already exists then create a user and associate it with the existing contact
        else if(Str.contains('CreateUser')){
            //Checking whether the account is Partner Enabled
            if(!cnt.Account.Ispartner){
                List<Account> AccountList = new List<Account>([Select Id,IsPartner from Account where id=:cnt.AccountId]);
                AccountList[0].IsPartner = true;
                update AccountList[0];
            }
            
            //Try-catch block to eliminate any issues during user creation like Du-licate username exists
            try{
            User u = new User(FirstName=(String)Opp.get(fieldsMap.get('FirstName')),LastName=(String)Opp.get(fieldsMap.get('LastName')),Email=(String)Opp.get(fieldsMap.get('Email')),Username=(String)Opp.get(fieldsMap.get('Email')),ProfileId=Label.MRA_Profile,contactId=cnt.Id,languagelocalekey='en_US',IsActive = true,timezonesidkey='America/New_York', emailencodingkey='UTF-8',alias=((String)Opp.get(fieldsMap.get('FirstName'))).trim().substring(0,1)+((String)Opp.get(fieldsMap.get('LastName'))).trim().substring(0,1),localesidkey='en_US');
            insert u;
            usr2 = u;
            Userflag =true;
            str ='ShareRecordWithUser';
            system.debug('====>'+u);
            }
             Catch(Exception e){
             
             system.debug('Here is the exception ======>'+e);
             if(((String)(e.getMessage())).contains('Duplicate Username')){
             User u = new User(FirstName=(String)Opp.get(fieldsMap.get('FirstName')),LastName=(String)Opp.get(fieldsMap.get('LastName')),Email=(String)Opp.get(fieldsMap.get('Email')),Username=(String)Opp.get(fieldsMap.get('Email')+'.pcori'),ProfileId=Label.MRA_Profile,contactId=cnt.Id,languagelocalekey='en_US',IsActive = true,timezonesidkey='America/New_York', emailencodingkey='UTF-8',alias=((String)Opp.get(fieldsMap.get('FirstName'))).trim().substring(0,1)+((String)Opp.get(fieldsMap.get('LastName'))).trim().substring(0,1),localesidkey='en_US');
            insert u;
            usr2 = u;
            Userflag =true;
            str ='ShareRecordWithUser';
             }
             }
        }
        
        //Share the record with the user if the user is newly created/already exists
        if(Str.contains('ShareRecordWithUser')){
            system.debug('Here is the final User2'+usr2);
            if(!Userflag){
                usr2 = usr;
            }
            system.debug('Here is the final User2 +second time'+usr2);
            
            //Updating Opportunity Owner to PI of the project so that it he can submit the record to approval process
            if(usr2.Id!=null && fieldsMap.get('Email')=='Project_Lead_Email_Address__c'){
                Opportunity Opp2 = new Opportunity();        
                Opp2.Id=Opp.Id;        
                Opp2.PI_Name_User__c=usr2.Id;
                Opp2.OwnerId=usr2.Id;
                //Opp2.Project_Lead_Title_RRCA__c=usr2.Contact.Current_Position_or_Title__c;
                update Opp2;
            }
            
            //Updating the AO user field to the user who is created/updated in this method
            if(usr2.Id!=null && fieldsMap.get('Email')=='AO_Email__c'){
                Opportunity Opp2 = new Opportunity();        
                Opp2.Id=Opp.Id;        
                Opp2.AO_Name_User__c=usr2.Id;
                //Opp2.AO_Title_RRCA__c=usr2.Contact.Current_Position_or_Title__c;
                update Opp2;
                system.debug('if'+usr2);
                
                /*This logic is commented because it was used to share Opportunity Records but later on during testing a 
                  defect was found where if the user changes the associated account to his contact, then the opportunity
                  share records are being deleted resulting in insufficient previlages error when trying to access the 
                  Opportunity record. So as a workaroud we are inserting Opportunity Team records below instead of 
                  Opportunityshare records*/
                
                /*List<OpportunityShare> OpportunityShareRecords = new List<OpportunityShare>([Select Id from OpportunityShare where UserorGroupId=:usr2.Id AND OpportunityId=:Opp.Id]);
                system.debug('here is the Opportunity'+Opp.Id);
                system.debug('Here is the User Id'+usr2.Id);
                system.debug('here is the opportunityshare'+OpportunityShareRecords);
                system.debug('here is the opportunityshare size'+OpportunityShareRecords.size());
                if(usr2==null){
                opp.adderror('User 2 is null at this point');
                }
                if(OpportunityShareRecords.size()==0){
                    OpportunityShare Ops = new OpportunityShare(OpportunityId=Opp.Id, UserOrGroupId=usr2.Id,OpportunityAccessLevel='Edit');
                    insert Ops;
                    system.debug('Here is the Opportunityshare'+Ops); 
                }*/
                
                //Check for Opportunity Team records 
                List<OpportunityTeamMember> OpportunityTeamMemberList = new List<OpportunityTeamMember>([Select Id,name from OpportunityTeamMember where OpportunityId =: Opp.Id AND UserId =: usr2.Id]);
                
                //If no records found, then Insert new records
                if(OpportunityTeamMemberList.size()==0 && fieldsMap.get('Email')=='AO_Email__c'){
                OpportunityTeamMember otm = new OpportunityTeamMember (OpportunityId = Opp.Id,UserId = usr2.Id,TeamMemberRole = 'Administrative Official',OpportunityAccessLevel='Edit');
                insert otm;
                }
            }
        }
        
        
    }
}