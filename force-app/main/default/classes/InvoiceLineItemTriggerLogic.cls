/*------------------------------------------------------------------------------
 *  Date            Project             Developer                       Justification
 *  04/24/2017   Invoicing-Phase2        Vinayak Sharma, REI Systems     Creation
 *  04/24/2017   Invoicing-Phase2        Vinayak Sharma, REI Systems     This is a Trigger on the InvoiceLineItems which is used to do DML operatoins on InvoiceLineItem object 
 * -----------------------------------------------------------------------------
*/  
public with Sharing  class InvoiceLineItemTriggerLogic extends TriggerHelper  {   
    /* @purpose This method is called in the Before Insert 
      which in turn calls the method to populate the Project Invoice Calculation on 
      the Invoice Line Item objec */
    public override void processBeforeInsert(){
        populatePIC();    
    } 
       
    /* @purpose This method is executed in the Before Update Context  
    *   and in turn calls the methods to Populate the PIC on the Invoice Line Item
    *  and also to Calculate the Percentage
    */
    public override void processBeforeUpdate(){
        updateCumulativeAndPercent();
      //  System.debug('<<<<<<<<<<  The  processBeforeUpdate 0002 is >>>>>>>>>>>');
    } 
    
    /* @purpose This method is called populate the Project Invoice Calculation 
    *  on the Invoice Line Item before the invoice is created 
    */
    private void populatePIC(){
        Set<Id> invoiceIdSet = new Set<Id>(); 
        List<Id> projectIdlst = new List<Id>();
        Map<Id,Id> invProjectIdMap = new Map<Id,Id>(); // Map of Invoice ID to Project ID
        Set<Id> projectInvoiceCal = new Set<Id>();

        Id profileId= userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name; 
         
        for(Invoice_Line_Item__c lineItem : (List<Invoice_Line_Item__c>)Trigger.new){         
                invoiceIdSet.add(lineItem.Invoice__c);    
        } 
                 
        List<Invoice__c> invoiceLst = [Select Id,Project__c from Invoice__c where Id in : invoiceIdSet limit 50000];      
        System.debug('<<<< invoiceLst   >>>> '+invoiceLst);        
        for(Invoice__c inv: invoiceLst){
            projectIdlst.add(inv.Project__c);  
            if(invProjectIdMap.get(inv.Id) == null){  
                invProjectIdMap.put(inv.Id,inv.Project__c);
            }
        } 
        
        // To do make sure we restrict to create only one record of the Project Invoice Calculatin object 
        Map<Id,Project_Invoice_Calculation__c> proInvCalMap = new  Map<Id,Project_Invoice_Calculation__c>();
        // List<Project_Invoice_Calculation__c> proInvCalLst  = new  List<Project_Invoice_Calculation__c>();
        List<Project_Invoice_Calculation__c> proInvCalLst = [Select
            Id,Name,Project__c  from Project_Invoice_Calculation__c where Project__c in:projectIdlst  and  Is_Current_Budget__c =: true];  

        Map<Id,List<Project_Invoice_Calculation__c>> mapProjIdListProjInvObj = new Map<Id,List<Project_Invoice_Calculation__c>>(); 
        
        for(Project_Invoice_Calculation__c projInvCalc: proInvCalLst){ 
            List<Project_Invoice_Calculation__c> projInvCalcObjLst  =  (List<Project_Invoice_Calculation__c>)mapProjIdListProjInvObj.get(projInvCalc.Project__c);
            if(projInvCalcObjLst == null){ 
                  projInvCalcObjLst = new List<Project_Invoice_Calculation__c>();
                  mapProjIdListProjInvObj.put(projInvCalc.Project__c,projInvCalcObjLst);
             }  
             projInvCalcObjLst.add(projInvCalc);  
        }     
        try{ 
            for(Invoice_Line_Item__c lineItem : (List<Invoice_Line_Item__c>)Trigger.new){                
                List<Project_Invoice_Calculation__c>  projInvCalcObjLst =  (List<Project_Invoice_Calculation__c>)mapProjIdListProjInvObj.get((invProjectIdMap.get(lineItem.Invoice__c )));  
                if(Trigger.isBefore && Trigger.isInsert){ 
                   System.debug('<<<< mapProjIdListProjInvObj     >>>> '+projInvCalcObjLst[0] ); 
                   lineItem.PIC__c =   projInvCalcObjLst[0].Id;    
                } 
            }   
        }catch(Exception e){
            System.debug(' The populatePIC method exception Exception   is ' + e.getMessage()); 
            System.debug(' The populatePIC method Line Number Exception is ' + e.getLineNumber()); 
            System.debug(' The populatePIC method Cause Exception is ' + e.getCause());  
        } 
    }
    
    /* @purpose This method is used to 
     *   1) update the Cumulative Expense to Date on Invoice Line Items( reffred via _CED__c in Field API name)
     *      Whenver the Awardee submits the Invoice for Approval
     *   2) This is method is also used to Calculate the Percentage Amount Spent per Budget Dategory  
     */
    
    private  void updateCumulativeAndPercent(){
        system.debug('INVLINEITEM updateCumulativeAndPercent ENTER');
        Set<Id> invoiceIdSet = new Set<Id>(); 
        List<Id> projectIdlst = new List<Id>();
        Map<Id,Id> invProjectIdMap = new Map<Id,Id>(); // Map of Invoice ID to Project ID
        Set<Id> projectInvoiceCal = new Set<Id>();

        Id profileId= userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name; 
         
        for(Invoice_Line_Item__c lineItem : (List<Invoice_Line_Item__c>)Trigger.new){
            if(lineItem.Line_Item_Type__c != System.Label.Firm_Fixed_Price){
                if(lineItem.PIC__c != null){
                    invoiceIdSet.add(lineItem.Invoice__c);        
                }
                   projectIdlst.add(lineItem.ProjectID__c);  
                if(invProjectIdMap.get(lineItem.Invoice__c) == null){  
                    invProjectIdMap.put(lineItem.Invoice__c,lineItem.ProjectID__c);
                }
            }   
        }   
                 
        System.debug('<<<< projectIdlst       >>>>  '+projectIdlst);
        
        // To do make sure we restrict to create only one record of the Project Invoice Calculatin object 
        Map<Id,Project_Invoice_Calculation__c> proInvCalMap = new  Map<Id,Project_Invoice_Calculation__c>();
        // List<Project_Invoice_Calculation__c> proInvCalLst  = new  List<Project_Invoice_Calculation__c>();
        List<Project_Invoice_Calculation__c> proInvCalLst = [Select
            Id,Name,Project__c ,
            Salary_SUP_Cumulative__c ,Salary_SUP_Budget__c, 
            Fringe_Benefits_SUP_Budget__c,  Fringe_Benefits_SUP_Cumulative__c,
            Sub_Total_Personnel_Cost_SUP__c,    Sub_Total_Personnel_Cost_SUP_Cumulative__c,
            Consultant_Costs_SUP_Budget__c  ,Consultant_Costs_SUP_Cumulative__c,
            Supplies_SUP_Budget__c  ,Supplies_SUP_Cumulative__c,
            Travel_SUP_Budget__c    ,Travel_SUP_Cumulative__c,
            Other_Costs_SUP_Budget__c,  Other_Costs_SUP_Cumulative__c,
            Equipment_SUP_Budget__c,    Equipment_SUP_Cumulative__c,
            Consortium_Contractual_Costs_SUP_Budget__c, Consortium_Contractual_Costs_SUP_Cumulat__c,
            Indirect_Costs_SUP_Budget__c,   Indirect_Costs_SUP_Cumulative__c,
            Total_Direct_Cost_SUP_Budget__c,    Total_Direct_Cost_SUP_Cumulative__c,
            
            Salary_TRP_Cumulative__c ,Salary_TRP_Budget__c, 
            Fringe_Benefits_TRP_Budget__c,  Fringe_Benefits_TRP_Cumulative__c,
            Sub_Total_Personnel_Cost_TRP__c,    Sub_Total_Personnel_Cost_TRP_Cumulative__c,
            Consultant_Costs_TRP_Budget__c  ,Consultant_Costs_TRP_Cumulative__c,
            Supplies_TRP_Budget__c  ,Supplies_TRP_Cumulative__c,
            Travel_TRP_Budget__c    ,Travel_TRP_Cumulative__c,
            Other_Costs_TRP_Budget__c,  Other_Costs_TRP_Cumulative__c,
            Equipment_TRP_Budget__c,    Equipment_TRP_Cumulative__c,
            Consortium_Contractual_Costs_TRP_Budget__c, Consortium_Contractual_Costs_TRP_Cumulat__c,
            Indirect_Costs_TRP_Budget__c,   Indirect_Costs_TRP_Cumulative__c,
            Total_Direct_Cost_TRP_Budget__c,    Total_Direct_Cost_TRP_Cumulative__c ,
            
            Salary_PR_Cumulative__c ,Salary_PR_Budget__c, 
            Fringe_Benefits_PR_Budget__c,  Fringe_Benefits_PR_Cumulative__c,
            Sub_Total_Personnel_Cost_PR__c,    Sub_Total_Personnel_Cost_PR_Cumulative__c,
            Consultant_Costs_PR_Budget__c  ,Consultant_Costs_PR_Cumulative__c,
            Supplies_PR_Budget__c  ,Supplies_PR_Cumulative__c,
            Travel_PR_Budget__c    ,Travel_PR_Cumulative__c, 
            Other_Costs_PR_Budget__c,  Other_Costs_PR_Cumulative__c,
            Equipment_PR_Budget__c,    Equipment_PR_Cumulative__c,
            Consortium_Contractual_Costs_PR_Budget__c, Consortium_Contractual_Costs_PR_Cumulat__c,
            Indirect_Costs_PR_Budget__c,   Indirect_Costs_PR_Cumulative__c,
            Total_Direct_Cost_PR_Budget__c,    Total_Direct_Cost_PR_Cumulative__c ,       

            // Firm Fixed price Fields 
            Milestone_Budget__c ,Milestone_Cumulative__c , Milestone_AFR__c
 
            from Project_Invoice_Calculation__c where Project__c in:projectIdlst  and  Is_Current_Budget__c =: true];    
            //System.debug('<<<< proInvCalLst   >>>>   '+proInvCalLst);          

        Map<Id,List<Project_Invoice_Calculation__c>> mapProjIdListProjInvObj = new Map<Id,List<Project_Invoice_Calculation__c>>(); 
        
        for(Project_Invoice_Calculation__c projInvCalc: proInvCalLst){ 
            List<Project_Invoice_Calculation__c> projInvCalcObjLst  =  (List<Project_Invoice_Calculation__c>)mapProjIdListProjInvObj.get(projInvCalc.Project__c);
            if(projInvCalcObjLst == null){ 
                  projInvCalcObjLst = new List<Project_Invoice_Calculation__c>();
                  mapProjIdListProjInvObj.put(projInvCalc.Project__c,projInvCalcObjLst);
             }
            if(projInvCalc.Salary_TRP_Cumulative__c==null){
                projInvCalc.Salary_TRP_Cumulative__c=0;
            }
            if(projInvCalc.Fringe_Benefits_TRP_Cumulative__c==null){
                projInvCalc.Fringe_Benefits_TRP_Cumulative__c=0;
            }
            if(projInvCalc.Consultant_Costs_TRP_Cumulative__c==null){
                projInvCalc.Consultant_Costs_TRP_Cumulative__c=0;
            }
            if(projInvCalc.Supplies_TRP_Cumulative__c==null){
                projInvCalc.Supplies_TRP_Cumulative__c=0;
            }
            if(projInvCalc.Travel_TRP_Cumulative__c==null){
                projInvCalc.Travel_TRP_Cumulative__c=0;
            }
             if(projInvCalc.Other_Costs_TRP_Cumulative__c==null){
                projInvCalc.Other_Costs_TRP_Cumulative__c=0;
            }
             if(projInvCalc.Equipment_TRP_Cumulative__c==null){
                projInvCalc.Equipment_TRP_Cumulative__c=0;
            }
            if(projInvCalc.Consortium_Contractual_Costs_TRP_Cumulat__c==null){
                projInvCalc.Consortium_Contractual_Costs_TRP_Cumulat__c=0;
            }
            if(projInvCalc.Indirect_Costs_TRP_Cumulative__c==null){
                projInvCalc.Indirect_Costs_TRP_Cumulative__c=0;
            }
             if(projInvCalc.Salary_SUP_Cumulative__c==null){
                projInvCalc.Salary_SUP_Cumulative__c=0;
            }
             if(projInvCalc.Fringe_Benefits_SUP_Cumulative__c==null){
                projInvCalc.Fringe_Benefits_SUP_Cumulative__c=0;
            }
            if(projInvCalc.Consultant_Costs_SUP_Cumulative__c==null){
                projInvCalc.Consultant_Costs_SUP_Cumulative__c=0;
            }
            if(projInvCalc.Supplies_SUP_Cumulative__c==null){
                projInvCalc.Supplies_SUP_Cumulative__c=0;
            }
            if(projInvCalc.Travel_SUP_Cumulative__c==null){
                projInvCalc.Travel_SUP_Cumulative__c=0;
            }
            if(projInvCalc.Other_Costs_SUP_Cumulative__c==null){
                projInvCalc.Other_Costs_SUP_Cumulative__c=0;
            }
            if(projInvCalc.Equipment_SUP_Cumulative__c==null){
                projInvCalc.Equipment_SUP_Cumulative__c=0;
            }
             if(projInvCalc.Consortium_Contractual_Costs_SUP_Cumulat__c==null){
                projInvCalc.Consortium_Contractual_Costs_SUP_Cumulat__c=0;
            }
            if(projInvCalc.Indirect_Costs_SUP_Cumulative__c==null){
                projInvCalc.Indirect_Costs_SUP_Cumulative__c=0;
            }
            if(projInvCalc.Salary_PR_Cumulative__c==null){
                projInvCalc.Salary_PR_Cumulative__c=0;
            }
            if(projInvCalc.Fringe_Benefits_PR_Cumulative__c==null){
                projInvCalc.Fringe_Benefits_PR_Cumulative__c=0;
            }
            if(projInvCalc.Consultant_Costs_PR_Cumulative__c==null){
                projInvCalc.Consultant_Costs_PR_Cumulative__c=0;
            }
            if(projInvCalc.Other_Costs_PR_Cumulative__c==null){
                projInvCalc.Other_Costs_PR_Cumulative__c=0;
            }
            if(projInvCalc.Consortium_Contractual_Costs_PR_Cumulat__c==null){
                projInvCalc.Consortium_Contractual_Costs_PR_Cumulat__c=0;
            }
            if(projInvCalc.Indirect_Costs_PR_Cumulative__c==null){
                projInvCalc.Indirect_Costs_PR_Cumulative__c=0;
            }
            if(projInvCalc.Supplies_PR_Cumulative__c==null){
                projInvCalc.Supplies_PR_Cumulative__c=0;
            }
            if(projInvCalc.Travel_PR_Cumulative__c==null){
                projInvCalc.Travel_PR_Cumulative__c=0;
            }
            if(projInvCalc.Equipment_PR_Cumulative__c==null){
                projInvCalc.Equipment_PR_Cumulative__c=0;
            }
            if(projInvCalc.other_Costs_PR_Cumulative__c==null){
                      projInvCalc.other_Costs_PR_Cumulative__c=0;
                  }
             if(projInvCalc.Milestone_Cumulative__c==null){
                      projInvCalc.Milestone_Cumulative__c=0;
                  }
             projInvCalcObjLst.add(projInvCalc);  
        }     
            //System.debug('<<<< mapProjIdProjInvObj     >>>> '+mapProjIdListProjInvObj); 

        try{ 
            for(Invoice_Line_Item__c lineItem : (List<Invoice_Line_Item__c>)Trigger.new){
                
                List<Project_Invoice_Calculation__c>  projInvCalcObjLst =  (List<Project_Invoice_Calculation__c>)mapProjIdListProjInvObj.get((invProjectIdMap.get(lineItem.Invoice__c )));    
                //System.debug(' <<<< The PIC OBject is >>>>> '+ projInvCalcObjLst[0].Id);  
                updateCumulativeBeforeSubmission(lineItem,projInvCalcObjLst,profileName);
                calculatePercentage(lineItem,projInvCalcObjLst,profileName); 
                //System.debug('<<<< lineItem.PIC__c   >>>> '+lineItem.PIC__c);
            }   
        }catch(Exception e){
            System.debug(' The updateCumulativeAndPercent method Message exception is ' + e.getMessage()); 
            System.debug(' The updateCumulativeAndPercent method Line Number exception is ' + e.getLineNumber());  
            System.debug(' The updateCumulativeAndPercent method Cause exception is ' + e.getCause());  
        } 
    }


    /* @vinayak sharma @purpose 
      If the logged in Profile is a Community user the updates the Cumulatives at the Line Item level 
      As the Community user will click on the save button to save Invoice and Invoice Line Items  */
    
    private  void updateCumulativeBeforeSubmission(Invoice_Line_Item__c li,List<Project_Invoice_Calculation__c> picLst,String profileName){
        // If logged in Profile is the community user then update the cumulative

        for(Project_Invoice_Calculation__c pic: picLst){
            try{                 

                if(li.PIC__c == pic.Id) {
                    if(profileName == System.Label.PCORI_Community_Partner && Trigger.isBefore && Trigger.isUpdate){

                         if(li.Line_Item_Type__c == System.Label.Research_Period || li.Line_Item_Type__c == System.Label.Engagement){    
                            li.Salary_CED__c                         =  getDecimalvalue(li.Salary__c) + pic.Salary_TRP_Cumulative__c ;
                            li.Fringe_Benefits_CED__c                =  getDecimalvalue(li.Fringe_Benefits__c) + pic.Fringe_Benefits_TRP_Cumulative__c ; 
                            li.Consultant_Cost_CED__c                =  getDecimalvalue(li.Consultant_Cost__c)  + pic.Consultant_Costs_TRP_Cumulative__c;
                            li.Supplies_CED__c                       =  getDecimalvalue(li.Supplies__c)   + pic.Supplies_TRP_Cumulative__c;
                            li.Travel_CED__c                         =  getDecimalvalue(li.Travel__c)   + pic.Travel_TRP_Cumulative__c;
                            li.Other_Costs_CED__c                    =  getDecimalvalue(li.Other_Costs__c)   + pic.Other_Costs_TRP_Cumulative__c;
                            //li.Research_Related_IP_OP_Costs_CED__c   =  li.Research_Related_Inpatient_and_Outp__c  + pic.Research_Related_I_O_TRP_Cumulative__c;
                            li.Equipment_CED__c                      =  getDecimalvalue(li.Equipment__c)   + pic.Equipment_TRP_Cumulative__c;
                            li.Consortium_Contractual_Costs_CED__c   =  getDecimalvalue(li.Consortium_Contractual_Costs__c)  + pic.Consortium_Contractual_Costs_TRP_Cumulat__c;
                            li.Indirect_Costs_CED__c                 =  getDecimalvalue(li.Indirect_Costs__c)  + pic.Indirect_Costs_TRP_Cumulative__c;
                        } 
                        if(li.Line_Item_Type__c == System.Label.Supplemental_Funding){ 
                            li.Salary_CED__c                         =  getDecimalvalue(li.Salary__c) + pic.Salary_SUP_Cumulative__c ;
                            li.Fringe_Benefits_CED__c                =  getDecimalvalue(li.Fringe_Benefits__c) + pic.Fringe_Benefits_SUP_Cumulative__c ; 
                            li.Consultant_Cost_CED__c                =  getDecimalvalue(li.Consultant_Cost__c)  + pic.Consultant_Costs_SUP_Cumulative__c;
                            li.Supplies_CED__c                       =  getDecimalvalue(li.Supplies__c )  + pic.Supplies_SUP_Cumulative__c;
                            li.Travel_CED__c                         =  getDecimalvalue(li.Travel__c)   + pic.Travel_SUP_Cumulative__c;
                            li.Other_Costs_CED__c                    = getDecimalvalue( li.Other_Costs__c )  + pic.Other_Costs_SUP_Cumulative__c;
                            //li.Research_Related_IP_OP_Costs_CED__c   =  li.Research_Related_Inpatient_and_Outp__c  + pic.Research_Related_I_O_SUP_Cumulative__c;
                            li.Equipment_CED__c                      =  getDecimalvalue(li.Equipment__c)   + pic.Equipment_SUP_Cumulative__c;
                            li.Consortium_Contractual_Costs_CED__c   =  getDecimalvalue(li.Consortium_Contractual_Costs__c)  + pic.Consortium_Contractual_Costs_SUP_Cumulat__c;
                            li.Indirect_Costs_CED__c                 =  getDecimalvalue(li.Indirect_Costs__c)  + pic.Indirect_Costs_SUP_Cumulative__c;
                        }
                        if(li.Line_Item_Type__c == System.Label.Peer_Review){
                            li.Salary_CED__c                         =  getDecimalvalue(li.Salary__c) + pic.Salary_PR_Cumulative__c ;
                            li.Fringe_Benefits_CED__c                =  getDecimalvalue(li.Fringe_Benefits__c) + pic.Fringe_Benefits_PR_Cumulative__c ; 
                            li.Consultant_Cost_CED__c                =  getDecimalvalue(li.Consultant_Cost__c)  + pic.Consultant_Costs_PR_Cumulative__c;
                      //    li.Supplies_CED__c                       =  li.Supplies__c   + pic.Supplies_PR_Cumulative__c;
                      //    li.Travel_CED__c                         =  li.Travel__c   + pic.Travel_PR_Cumulative__c;
                            li.Other_Costs_CED__c                    =  getDecimalvalue(li.Other_Costs__c)   + pic.Other_Costs_PR_Cumulative__c;
                      //    li.Research_Related_IP_OP_Costs_CED__c   =  li.Research_Related_Inpatient_and_Outp__c  + pic.Research_Related_I_O_PR_Cumulative__c;
                      //    li.Equipment_CED__c                      =  li.Equipment__c   + pic.Equipment_PR_Cumulative__c;
                            li.Consortium_Contractual_Costs_CED__c   =  getDecimalvalue(li.Consortium_Contractual_Costs__c)  + pic.Consortium_Contractual_Costs_PR_Cumulat__c;
                            li.Indirect_Costs_CED__c                 =  getDecimalvalue (li.Indirect_Costs__c ) + pic.Indirect_Costs_PR_Cumulative__c;
                       }       
                    }
                } 
                system.debug('INVLINEITEM updateCumulativeBeforeSubmission END Invoice_Line_Item__c '+li);  
            }Catch(Exception e) {
                System.debug(' The updateCumulativeBeforeSubmission method Message exception   is  '+e.getMessage());
                System.debug(' The updateCumulativeBeforeSubmission method LineNumber exception is  '+e.getLineNumber());
                System.debug(' The updateCumulativeBeforeSubmission method cause exception      is  '+e.getCause());
            }
        }
    }      
    //@Purpose: Calculate percent to show in different budget categories 
    private  void calculatePercentage(Invoice_Line_Item__c li,List<Project_Invoice_Calculation__c> picLst,String profileName){
        for(Project_Invoice_Calculation__c pic: picLst){
            try{                 
                if(li.PIC__c == pic.Id) { 

                     if(li.Line_Item_Type__c == System.Label.Research_Period || li.Line_Item_Type__c == System.Label.Engagement){  
                        system.debug('INVLINEITEM calculatePercentage ENTER Salary '+li.Salary__c+ ' ced '+ li.Salary_CED__c+ ' pic '+ pic.Salary_TRP_Budget__c);  
                        li.Salaries_percent__c                    = perCent(pic.Salary_TRP_Budget__c,li.Salary_CED__c,li.Salary__c); 
                        system.debug('INVLINEITEM calculatePercentage ENTER Salaries_percent__c '+li.Salaries_percent__c );                  
                        li.Fringe_percent__c                      = perCent(pic.Fringe_Benefits_TRP_Budget__c,li.Fringe_Benefits_CED__c,li.Fringe_Benefits__c);
                        li.Subtotal_Personal_Cost_percent__c      = perCent(pic.Sub_Total_Personnel_Cost_TRP__c ,li.Subtotal_Personal_Cost_CED__c,li.Subtotal_Personal_Cost__c);
                        li.Consultant_Cost_percent__c             = perCent(pic.Consultant_Costs_TRP_Budget__c ,li.Consultant_Cost_CED__c ,li.Consultant_Cost__c);
                        li.Supplies_percent__c                    = perCent(pic.Supplies_TRP_Budget__c ,li.Supplies_CED__c,li.Supplies__c);
                        li.Travel_percent__c                      = perCent(pic.Travel_TRP_Budget__c ,li.Travel_CED__c,li.Travel__c);
                        li.Other_Costs_percent__c                 = perCent( pic.Other_Costs_TRP_Budget__c,li.Other_Costs_CED__c,li.Other_Costs__c);
                        //li.Research_Related_percent__c            = perCent(pic.Research_Related_I_O_TRP_Budget__c ,li.Research_Related_IP_OP_Costs_CED__c,li.Research_Related_Inpatient_and_Outp__c);
                        li.Equipment_percent__c                   = perCent(pic.Equipment_TRP_Budget__c,li.Equipment_CED__c,li.Equipment__c);
                        li.Consortium_Contractual_Cost_percent__c = perCent(pic.Consortium_Contractual_Costs_TRP_Budget__c ,li.Consortium_Contractual_Costs_CED__c,li.Consortium_Contractual_Costs__c);
                        li.Indirect_Cost_percent__c               = perCent( pic.Indirect_Costs_TRP_Budget__c,li.Indirect_Costs_CED__c,li.Indirect_Costs__c);
                        li.Total_Direct_Cost_percent__c           = perCent(pic.Total_Direct_Cost_TRP_Budget__c ,li.Direct_Costs_CED__c,li.Direct_Cost__c);                   
                    }
                    if(li.Line_Item_Type__c == System.Label.Supplemental_Funding){ 
                        li.Salaries_percent__c                    = perCent(pic.Salary_SUP_Budget__c,li.Salary_CED__c,li.Salary__c);                    
                        li.Fringe_percent__c                      = perCent(pic.Fringe_Benefits_SUP_Budget__c,li.Fringe_Benefits_CED__c,li.Fringe_Benefits__c);
                        li.Subtotal_Personal_Cost_percent__c      = perCent(pic.Sub_Total_Personnel_Cost_SUP__c ,li.Subtotal_Personal_Cost_CED__c,li.Subtotal_Personal_Cost__c);
                        li.Consultant_Cost_percent__c             = perCent(pic.Consultant_Costs_SUP_Budget__c ,li.Consultant_Cost_CED__c ,li.Consultant_Cost__c);
                        li.Supplies_percent__c                    = perCent(pic.Supplies_SUP_Budget__c ,li.Supplies_CED__c,li.Supplies__c);
                        li.Travel_percent__c                      = perCent(pic.Travel_SUP_Budget__c ,li.Travel_CED__c,li.Travel__c);
                        li.Other_Costs_percent__c                 = perCent(pic.Other_Costs_SUP_Budget__c,li.Other_Costs_CED__c,li.Other_Costs__c);
                        //li.Research_Related_percent__c            = perCent(pic.Research_Related_I_O_SUP_Budget__c ,li.Research_Related_IP_OP_Costs_CED__c,li.Research_Related_Inpatient_and_Outp__c);
                        li.Equipment_percent__c                   = perCent(pic.Equipment_SUP_Budget__c,li.Equipment_CED__c,li.Equipment__c);
                        li.Consortium_Contractual_Cost_percent__c = perCent(pic.Consortium_Contractual_Costs_SUP_Budget__c ,li.Consortium_Contractual_Costs_CED__c,li.Consortium_Contractual_Costs__c);
                        li.Indirect_Cost_percent__c               = perCent(pic.Indirect_Costs_SUP_Budget__c,li.Indirect_Costs_CED__c,li.Indirect_Costs__c);
                        li.Total_Direct_Cost_percent__c           = perCent(pic.Total_Direct_Cost_SUP_Budget__c ,li.Direct_Costs_CED__c,li.Direct_Cost__c);    
                    }   
                    if(li.Line_Item_Type__c == System.Label.Peer_Review){
                        li.Salaries_percent__c                    = perCent(pic.Salary_PR_Budget__c,li.Salary_CED__c,li.Salary__c);                    
                        li.Fringe_percent__c                      = perCent(pic.Fringe_Benefits_PR_Budget__c,li.Fringe_Benefits_CED__c,li.Fringe_Benefits__c);
                        li.Subtotal_Personal_Cost_percent__c      = perCent(pic.Sub_Total_Personnel_Cost_PR__c ,li.Subtotal_Personal_Cost_CED__c,li.Subtotal_Personal_Cost__c);
                        li.Consultant_Cost_percent__c             = perCent(pic.Consultant_Costs_PR_Budget__c ,li.Consultant_Cost_CED__c ,li.Consultant_Cost__c);
                        li.Other_Costs_percent__c                 = perCent( pic.Other_Costs_PR_Budget__c,li.Other_Costs_CED__c,li.Other_Costs__c);
                        li.Consortium_Contractual_Cost_percent__c = perCent(pic.Consortium_Contractual_Costs_PR_Budget__c ,li.Consortium_Contractual_Costs_CED__c,li.Consortium_Contractual_Costs__c);
                        li.Indirect_Cost_percent__c               = perCent( pic.Indirect_Costs_PR_Budget__c,li.Indirect_Costs_CED__c,li.Indirect_Costs__c);
                        li.Total_Direct_Cost_percent__c           = perCent(pic.Total_Direct_Cost_PR_Budget__c ,li.Direct_Costs_CED__c,li.Direct_Cost__c);    
                    }
                } 
                system.debug('INVLINEITEM calculatePercentage END Invoice_Line_Item__c '+li);  
            }catch (Exception e){    
                System.debug(' The calculatePercentage method Message exception   is  '+e.getMessage());
                System.debug(' The calculatePercentage method LineNumber exception is  '+e.getLineNumber());
                System.debug(' The calculatePercentage method cause exception      is  '+e.getCause()); 
                System.debug(' The calculatePercentage method Trace exception    is  '+e.getStackTraceString());
            }
        }   
    }  
    
    //@Purpose: Returns percent to show in different budget categories  
    private Decimal perCent(Decimal budget,Decimal cumulative,Decimal current){
        system.debug('INVLINEITEM perCent ENTER');
        Integer Scale = 1;

        Decimal percent = (budget  == null || budget ==0  ) ? 0.0  : 
        (((cumulative ==null||cumulative==0 )&&(current!=null && current!= 0 ))?((current/budget)*100).setScale(Scale):((cumulative/budget)*100).setScale(Scale));


        System.debug('The Percentage is now >>>'+  percent);
        return percent;
    }    
    private decimal getDecimalvalue(decimal sconvertedvalue){
                decimal dval;
                if(sconvertedvalue==null){
                  dval=0;
                }
                else{
                    dval=sconvertedvalue;
                }
                return dval;
            } 
}