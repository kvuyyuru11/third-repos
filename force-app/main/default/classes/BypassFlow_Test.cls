@isTest
private class BypassFlow_Test {
	
	@isTest static void testBypassFlow() {
		//run immediate action
		List<BypassFlow.BypassFlowInput> inpList = new List<BypassFlow.BypassFlowInput>();
		BypassFlow.immediateAction(inpList);
		//nothing happens

		//give input then run again
		BypassFlow.BypassFlowInput inp = new BypassFlow.BypassFlowInput();
		inp.debugMsg = 'Running Bypass Flow Test Class';
		inpList.add(inp);
		BypassFlow.immediateAction(inpList);
		//not sure how to assert a debug log was created

		//test null debug message
		inp.debugMsg = null;
		BypassFlow.immediateAction(inpList);
	}
	
}