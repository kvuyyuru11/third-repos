public class TopicGenerationTriggerHandlerClass {
    public TopicGenerationTriggerHandlerClass(){}
    public void insertTop(Topic__c t) { 
        Topic__c tp=[select Email__c from Topic__c where id=:t.Id];
        if(tp.Email__c!= Null){
            list<Contact> ct= new list<Contact>();
            ct= [select id,Email, AccountId,RecordTypeId from Contact where Email=:tp.Email__c];
            if(ct.size()>0){    
                for(Contact cnt:ct)
                {
                    system.debug('id founde1' + cnt.Email+tp.Email__c);
                    if(cnt.Email==tp.Email__c){
                        tp.Contact__c= cnt.id;
                        tp.Account__c= cnt.AccountId;     
                    }
                }
                update tp;
            }     
        }
    }
}