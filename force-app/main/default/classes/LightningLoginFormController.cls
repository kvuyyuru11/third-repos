global class LightningLoginFormController {

  

    @AuraEnabled
    public static String login(String username, String password) {
        try{
            
            ApexPages.PageReference lgn = Site.login(username, password, null);
            aura.redirect(lgn);
            return null;
        }
        catch (Exception ex) {
             AuraHandledException e=new AuraHandledException(ex.getMessage());
            e.SetMessage(ex.getMessage());
            system.debug('ex.getMessage()'+e);
            throw e;           
        }
    }
}