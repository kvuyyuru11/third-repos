//This is a Class that Prevents the dependent triggers from firing.
public class recursiveTrigger {
	public static boolean run = true;
    public static boolean disableTrigger = false;
    public static boolean run1 = true;
    public static boolean runOnce(){
    	if(run){
    		run=false;
     		return true;
    	}
        else{
        return run;
    	}
    }
    public static boolean runOnceOpp(){
    	if(run1){
    		run1=false;
     		return true;
    	}
        else{
        return run1;
    	}
    }
}