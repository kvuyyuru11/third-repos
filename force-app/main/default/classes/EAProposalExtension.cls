/****************************************************************************************************
*Description:           This is the extension used by the EAProposalWebform for lead entry
*                                         
*                       
*
*Required Class(es):    EAProposalExtension_Test
*
*Organization: 
*
*Revision   |Date       |Author             |AdditionalNotes
*====================================================================================================
*   1.0     07/02/2014   Justin Padilla     Initial Version
*****************************************************************************************************/
public class EAProposalExtension {

    public string FirstName{get;set;}
    public string LastName{get;set;}
    public string Org{get;set;}
    public string Email{get;set;}
    public string Phone{get;set;}
    public string Info{get;set;}
    public string Street{get;set;}
    public string City{get;set;}
    public string State{get;set;}
    public string ZipCode{get;set;}
    public string Country{get;set;}
    public string ProjectName{get;set;}
    public string text1=Label.Single_Community_Profile;
    private Id recordTypeId{get;set;}
    
    public boolean Submitted{get;set;}
    
    public EAProposalExtension(Apexpages.StandardController controller)
    {
        Submitted = false;
        for (RecordType rt: [SELECT Id from RecordType where DeveloperName = 'EA_Proposal_Webform' and SObjectType = 'Lead'])
        {
            recordTypeId = rt.Id;
        }
        //Info = string.valueOf(Site.isRegistrationEnabled());
        Info = string.valueOf(UserInfo.getProfileId());
    }
    
    public void Submit()
    {
        boolean error = false;
        if (FirstName == null || FirstName == '')
        {
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'First Name Required'));
        }
        if (LastName == null || LastName == '')
        {
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Last Name Required'));
        }
        if (Org == null || Org == '')
        {
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Organization Required'));
        }
        if (Email == null || Email == '')
        {
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Email Required'));
        }
         /*String err = 'We cannot complete your request at this time because your email address has already been registered with the PCORI Community. Please submit a ticket <a href="https://help.pcori.org/hc/en-us/requests/new" target="_blank">here</a> to have the issue resolved.';
    list<user> usr=[select username,profileID, email from user];
        Profile p1=[select Id from profile where Name=:text1 limit 1];
        for(user ur:usr){
             if(ur.Email==email && (ur.ProfileId==p1.Id)){
                 ApexPages.Message msgErr = new ApexPages.Message(ApexPages.Severity.ERROR, err);
                 ApexPages.addmessage(msgErr);
        
error = true;
             }
        }*/
        
        if (Phone == null || Phone == '')
        {
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Phone Required'));
        }
        if (Street == null || Street == '')
        {
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Street Required'));
        }
        if (City == null || City == '')
        {
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'City Required'));
        }
        if (State == null || State == '')
        {
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'State Required'));
        }
        if (ZipCode == null || ZipCode == '')
        {
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Zip Required'));
        }
        if (Country == null || Country == '')
        {
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Country Required'));
        }
        if (ProjectName == null || ProjectName == '')
        {
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Project Name Required'));
        }
        if (!error)
        {
            try
            {
                //Set the owner to the Grantee Portal User
                Id ProfileId;
                for (Profile p: [SELECT Id FROM Profile WHERE Name ='Grantee Portal User'])
                {
                    ProfileId = p.Id;
                }
                User validUser;
                for (User u: [SELECT Id FROM User WHERE IsActive = true AND ProfileId =: ProfileId LIMIT 1])
                {
                    validUser = u;
                }
                if (validUser != null)
                {
                        Lead l = new Lead();
                        l.FirstName = FirstName;
                        l.LastName = LastName;
                        l.Company = Org;
                        l.Email = Email;
                        l.Phone = Phone;
                        l.RecordTypeId = recordTypeId;
                        l.OwnerId = validUser.Id;
                        l.Straight_to_Proposal__c = true;
                        l.Street = Street;
                        l.City = City;
                        l.State = State;
                        l.PostalCode = ZipCode;
                        l.Country = Country;
                        l.Project_Name__c = ProjectName;
                        insert(l);
                        Submitted = true;
                        if (test.isRunningTest())Throw new MyException('Added for additional code coverage');
                }
            }
            catch (Exception ex)
            {
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Error: '+ex.getMessage()));
            }
        }
    }
    public class MyException extends Exception {}
}