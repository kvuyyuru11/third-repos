public without sharing class srchcntController {  
   public list <contact> cnt {get;set;} 
   public list<list<SObject>> cntListofList {get;set;} 
	 
   public string searchstring {get;set;}  
  
   public srchcntController(ApexPages.StandardController controller) {  
       contact con = (contact)controller.getRecord();
   }  
   public void search(){  
       
       Set<String> statusSet= new set<String>();
statusSet.add('PCORI Reviewer');
//statusSet.add('Do Not Invite');
statusSet.add('New Reviewer');
       statusSet.add('Approved Pending Federal Authorization');
      string searchquery='FIND : searchstring IN ALL FIELDS RETURNING Contact(FirstName,LastName,Personal_Statement__c,Reviewer_Role__c,Reviewer_Status__c where reviewer_status__c  IN :statusSet limit 250)';
       cntListofList = search.query(searchQuery);  
        cnt = cntListofList[0];
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'The number of records for this search is' +  ' '+cnt.size()));
       if(cnt.size()>200){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'There might potentially be more than 200 records which are showing up now. Please refine search.'));
       }
        if(cnt == null){
	    cnt = new list <contact>();
        }		
   }  
   public void clear(){  
   cnt.clear();  
   }  
 }