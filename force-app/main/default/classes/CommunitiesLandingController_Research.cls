public with sharing class CommunitiesLandingController_Research {
    
  
 
    // ZT - 06/18/14. Adapted from Eugene Vabishchevich's article (http://cloudcatamaran.com/2013/11/customer-community-customization-part-1/)
    // Code we will invoke on page load.
    public PageReference forwardToStartPage() 
    {
        //String communityUrl = 'https://dev-pcori.cs1.force.com/engagement/';
        String communityurl=Label.Portal_URL;
        String customHomePage = Label.Community_Research_Page_Home_Label;        //_ui/core/chatter/ui/ChatterPage';
        
        if (UserInfo.getUserType().equals('Guest')) 
        {
            return new PageReference(communityUrl + '/' + Label.Community_Research_Page_Label);
        } 
        else 
        {
            return new PageReference(communityUrl + customHomePage);
        }
    }
      
    public CommunitiesLandingController_Research() {}
}