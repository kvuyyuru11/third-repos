@isTest
private class ActionGridDeleteForNegotiation_Test {
	
	@isTest static void testActionGridDeleteForNegotiation() {
		Id r1 =Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RA Applications').getRecordTypeId();
		Id rKeyPersonnel =Schema.SObjectType.Key_Personnel_Costs__c.getRecordTypeInfosByName().get('Key Personnel').getRecordTypeId();
		Id rConsultantCost =Schema.SObjectType.Key_Personnel_Costs__c.getRecordTypeInfosByName().get('Consultant Cost').getRecordTypeId();
		Id rSubcontractorDirect =Schema.SObjectType.Key_Personnel_Costs__c.getRecordTypeInfosByName().get('Subcontractor Direct').getRecordTypeId();
		
		Id pcoriCommunityPartID = [SELECT Id FROM Profile WHERE name = 'PCORI Community Partner'].Id;

		Account.SObjectType.getDescribe().getRecordTypeInfos();

		Account a = new Account();
		a.Name = 'Acme1';
		insert a;

		Contact con = new Contact(LastName = 'testCon', AccountId = a.Id);
		Contact con1 = new Contact(LastName = 'testCon1', AccountId = a.Id);
		Contact con2 = new Contact(LastName = 'testCon1', AccountId = a.Id);
		Contact con3 = new Contact(LastName = 'testCon2', AccountId = a.Id);

		List<Contact> cons = new List<Contact>();
		cons.add(con);
		cons.add(con1);
		cons.add(con2);
		cons.add(con3);
		insert cons;
		
		User user = new User(Alias = 'test123', Email = 'test123fvb@noemail.com',
							 Emailencodingkey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',    
							 LocaleSidKey = 'en_US', ProfileId = pcoriCommunityPartID , Country='United States',IsActive = true,   
							 ContactId = con.Id, CommunityNickname = 'test12',
							 TimeZoneSidKey='America/New_York', UserName = 'testerfvb1@noemail.com');

		User user1 = new User(Alias = 'test1234', Email = 'test1234fvb@noemail.com',
							  Emailencodingkey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',    
							  LocaleSidKey = 'en_US', ProfileId = pcoriCommunityPartID , Country='United States',IsActive = true,   
							  ContactId = con1.Id, CommunityNickname = 'test12345',
							  TimeZoneSidKey='America/New_York', UserName = 'testerfvb123@noemail.com');

		User user2 = new User(Alias = 'test145', Email = 'test12345fvb@noemail.com',
							  Emailencodingkey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',    
							  LocaleSidKey = 'en_US', ProfileId = pcoriCommunityPartID , Country='United States',IsActive = true,   
							  ContactId = con2.Id, CommunityNickname = 'test14',
							  TimeZoneSidKey='America/New_York', UserName = 'testerfvb12@noemail.com');
		
		User user3 = new User(Alias = 'test52', Email = 'test135fvb@noemail.com',
							  Emailencodingkey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',    
							  LocaleSidKey = 'en_US', ProfileId = pcoriCommunityPartID , Country='United States',IsActive = true,   
							  ContactId = con3.Id, CommunityNickname = 'test5',
							  TimeZoneSidKey = 'America/New_York', UserName = 'testerfvb82@noemail.com');

		List<User> users = new List<User>();
		users.add(user);
		users.add(user1);
		users.add(user2);
		users.add(user3);
		insert users;
		
		Cycle__c c = new Cycle__c();
		c.Name = 'testcycle';
		c.COI_Due_Date__c = date.valueof(System.now());
		insert c;

		Campaign camp = new Campaign();
		camp.Name = 'ResearchCamp';
		camp.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('PCORI Portal').getRecordTypeId();
		camp.Cycle__c = c.Id;
		camp.IsActive = true;
		camp.FGM_Portal__Visibility__c = 'Public';
		camp.Status = 'In Progress';
		camp.StartDate = System.today();
		camp.EndDate = System.today() + 7;
		camp.FGM_Base__Board_Meeting_Date__c = System.today() + 14;

		insert camp;
		
		opportunity o = new opportunity();
		
		o.Awardee_Institution_Organization__c=a.Id;
		o.CampaignId = camp.Id;
		o.RecordTypeId = r1;
		o.Full_Project_Title__c='test';
		o.Project_Name_off_layout__c='test';
		o.CloseDate = Date.today() + 14;
		o.Application_Number__c='Ra-1234';
		o.StageName = 'Pending PI Application';
		o.External_Status__c = 'Draft';
		o.Name='test';
		o.PI_Name_User__c = user.id;
		o.AO_Name_User__c = user1.id;
		o.Project_Start_Date__c = system.Today();
		o.Bypass_Flow__c = true;
		
		insert o;

		//create a list of 10 milestones to delete later in web method
		List<FGM_Base__Benchmark__c> milestones = new List<FGM_Base__Benchmark__c>();
		for(Integer i=0; i<10; i++) {
			FGM_Base__Benchmark__c milestone = new FGM_Base__Benchmark__c();
			milestone.RecordTypeId = Schema.SObjectType.FGM_Base__Benchmark__c.getRecordTypeInfosByName().get('Milestone - Deliverable - RA').getRecordTypeId();
			milestone.Milestone_Status__c = 'Not Completed';
			milestone.FGM_Base__Due_Date__c = System.today() + 7;
			milestone.Milestone_Name__c = 'Unit Test ' + i;
			milestone.Milestone_ID2__c = 'A';
			milestone.FGM_Base__Request__c = o.Id;
			milestone.Bypass_Flow__c = true;
			milestones.add(milestone);
		}

		//insert milestones for testing
		insert milestones;

		//populate id string
		List<String> mIds = new List<String>();
		for(FGM_Base__Benchmark__c milestone : milestones) {
			mIds.add(milestone.id);
		}

		Test.startTest();

		//insure the milestones have been inserted, then delte them and assert that they have been deleted.
		System.assert(![SELECT Id FROM FGM_Base__Benchmark__c WHERE Id IN : mIds].isEmpty());
		ActionGridDeleteForNegotiation.deleteMilestoneIds(mIds);
		System.assert([SELECT Id FROM FGM_Base__Benchmark__c WHERE Id IN : mIds].isEmpty());

		//test handling of a list of ids that was previously deleted
		ActionGridDeleteForNegotiation.deleteMilestoneIds(mIds);

		//make sure that web method can work with an emtpy list
		mIds = new List<String>();
		ActionGridDeleteForNegotiation.deleteMilestoneIds(mIds);

		Test.stopTest();

	}
	
}