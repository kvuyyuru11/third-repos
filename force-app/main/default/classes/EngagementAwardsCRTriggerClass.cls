public class EngagementAwardsCRTriggerClass {

    public EngagementAwardsCRTriggerClass(){}
    public void insertCnt(Opportunity opt1)
{
     opportunity o=[select Project_Lead_First_Nam__c,Project_Lead_Last_Name__c,RecordTypeId, AccountId, Name, Project_Lead_Email_Address__c from Opportunity where id=:opt1.Id];
    Contact[] ct= [select id from Contact where Email=:o.Project_Lead_Email_Address__c ORDER BY CreatedDate ASC]; 
    //Contact cnt=[select id from Contact where email=:o.Project_Lead_Email_Address__c];
    Contact_Roles__c crs= new Contact_Roles__c();
     Contact c= new Contact();
    Id OpntRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('FoundationConnect - Portal').getRecordTypeId();
    
    if(o.RecordTypeId == OpntRecordTypeId){
    if(ct.size()==0){
              c.FirstName=o.Project_Lead_First_Nam__c;
              c.LastName=o.Project_Lead_Last_Name__c;
              c.AccountId=o.AccountId;
              c.Email=o.Project_Lead_Email_Address__c;
              database.insert(c);
           }
    
    if(ct.size()>0) {
        for(Contact ct1:ct){
               crs.Engagement_Award__c=o.Id;
               crs.Contact__c=ct1.Id;
               crs.Role__c='Project Lead';
               crs.PCORI_Department__c='Engagement';
               crs.PCORI_Program_Area__c='Engagement Awards';
               
    } database.insert(crs);
    }
       }  
}
}