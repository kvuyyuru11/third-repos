/***
 * An apex page controller that supports self registration of users in communities that allow self registration
 */
public with sharing class CommunitiesSelfRegController_Research {

    public String OrganizationName {get;set;}
    public String firstName {get; set;}
    public String lastName {get; set;}
    public String email {get; set;}
    public String phone {get; set;}
    public String password {get; set {password = value == null ? value : value.trim(); } }
    public String confirmPassword {get; set { confirmPassword = value == null ? value : value.trim(); } }
    public String communityNickname {get; set { communityNickname = value == null ? value : value.trim(); } }
    public boolean checkbox01 {get; set;}

    
    public CommunitiesSelfRegController_Research() {}
    
    private boolean isValidPassword() {
        return string.valueof(password).equals(string.valueof(confirmPassword));
    }
    public boolean confirmAgreement() {
    if(!checkbox01) {
        return false;
    } else {
        return true;
    }
}

    public PageReference registerUser() {
           // it's okay if password is null - we'll send the user a random password in that case
        if (!isValidPassword()) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.site.passwords_dont_match);
            ApexPages.addMessage(msg);
            return null;
        }    
        if(!confirmAgreement()) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Acknowledgement of PCORI’s Privacy Policy and Terms of Use is required to proceed.'));
        return null;

        }
        String err = 'We cannot complete your request at this time because the email address you entered may be in use by a current user in the PCORI Community. If you would like more information, please submit a ticket <a href="https://help.pcori.org/hc/en-us/requests/new" target="_blank">here</a>.';
        list<user> usr=[select username, email from user where email=:email];
        if(usr.isempty()){
             
                 ApexPages.Message msgErr = new ApexPages.Message(ApexPages.Severity.ERROR, err);
                 ApexPages.addmessage(msgErr);
        
            
        }

       String profileId = Label.MRA_Profile; // To be filled in by customer.
        String roleEnum = null; // To be filled in by customer.
        String accountId = Label.MRA_Account; // To be filled in by customer.
        
        String userName = email;

        User u = new User();
        u.Username = userName;
        u.Email = email;
        u.FirstName = firstName;
        u.LastName = lastName;
        u.CommunityNickname = communityNickname;
        u.ProfileId = profileId;
        u.phone=phone;
        
            String userId = Site.createPortalUser(u, accountId, password);
          
             if (userId != null) { 
            if (password != null && password.length() > 1) {
                return Site.login(userName, password, ApexPages.currentPage().getParameters().get('startURL'));
            }
            else {
                PageReference page = System.Page.CommunitiesSelfRegConfirm_Research;
                page.setRedirect(true);
                return page;
            }
        }
        return null;
    }
}