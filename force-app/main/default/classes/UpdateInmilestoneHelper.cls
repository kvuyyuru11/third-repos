/* This class belongs to Consolidated Trigger. This class will updatte Email fields in the Milestone whenever Opportunity record is edited and updated*/

public with sharing class UpdateInmilestoneHelper {

    public UpdateInMilestoneHelper() {
        
    }
    public void updateEmailIdsinMilestone(List<Opportunity> oplist) { /* This method will update email fields in Milestone*/
          
           Set<id> idList = new Set<Id>();
        for(Opportunity p : oplist){
           idList.add(p.id);
        }// for loop

                       
                Map<Id, Opportunity> oppmap = new Map<Id, Opportunity>();
                List<FGM_Base__Benchmark__c> mstList = new List<FGM_Base__Benchmark__c>();
                Map<Id, FGM_Base__Benchmark__c> mstmap = new Map<Id, FGM_Base__Benchmark__c>();
                                                          

        List<FGM_Base__Benchmark__c> mstSOQLList =[SELECT id,Email_PI_Project_Lead_1__c, Email_Administrative_Official__c, Email_PI_Project_Lead_Designee_2__c, Email_Program_Associate__c, Email_PI_Project_Lead_2__c, Email_PI_Project_Lead_Designee_1__c, Email_Program_Officer__c, Email_Contract_Administrator__c, Email_Contract_Coordinator__c, FGM_Base__Request__c FROM FGM_Base__Benchmark__c WHERE FGM_Base__Request__c IN : idList LIMIt 50000];
            

        List<Opportunity> oppSOQLList = [SELECT Id, Name, PI_Name_User__c,PI_Name_User__r.email, PI_Project_Lead_2_Name_New__r.Email, PI_Project_Lead_Designee_1_New__r.Email, PI_Project_Lead_Designee_2_Name_New__r.Email,Program_Associate__r.Email,Program_Officer__r.Email,AO_Name_User__r.Email,Contract_Administrator__r.Email,Contract_Coordinator__r.Email FROM Opportunity where Id IN : idList Limit 50000]; 

               
                for(Opportunity opp: oppSOQLList)
                {
                   oppmap.put(opp.id, opp);

                }
                
                for(FGM_Base__Benchmark__c mst: mstSOQLList)
                {
                   mstmap.put(mst.id, mst);

                }
           try{     
                for(FGM_Base__Benchmark__c mst : mstSOQLList)
                 {
                            
                    mst.Email_PI_Project_Lead_1__c=oppmap.get(mst.FGM_Base__Request__c).PI_Name_User__r.email;
                    mst.Email_PI_Project_Lead_2__c = oppmap.get(mst.FGM_Base__Request__c).PI_Project_Lead_2_Name_New__r.Email;
                    mst.Email_PI_Project_Lead_Designee_1__c = oppmap.get(mst.FGM_Base__Request__c).PI_Project_Lead_Designee_1_New__r.Email;
                    mst.Email_PI_Project_Lead_Designee_2__c =oppmap.get(mst.FGM_Base__Request__c).PI_Project_Lead_Designee_2_Name_New__r.Email;
                    mst.Email_Program_Associate__c = oppmap.get(mst.FGM_Base__Request__c).Program_Associate__r.Email;
                    mst.Email_Program_Officer__c = oppmap.get(mst.FGM_Base__Request__c).Program_Officer__r.Email;
                    mst.Email_Administrative_Official__c = oppmap.get(mst.FGM_Base__Request__c).AO_Name_User__r.Email;
                    mst.Email_Contract_Administrator__c = oppmap.get(mst.FGM_Base__Request__c).Contract_Administrator__r.Email;
                    mst.Email_Contract_Coordinator__c = oppmap.get(mst.FGM_Base__Request__c).Contract_Coordinator__r.Email;
                   
                    mstList.add(mst);
                  
                                     
                }// for loop
                update mstList;
                
            }catch(DmlException e) {
                System.debug('The following exception has occurred: ' + e.getMessage());       
            }Catch(Exception e)  {
                System.debug('The following exception has occurred: ' + e.getMessage());
            }// try catch block
           
           
      
          }//end updateEmailIdsinMilestone Method
    
}//end UpdateInmilestoneHelper