/* This class belongs to Consolidated Trigger. This class will updatte Email fields in the Progress Report whenever Opportunity record is edited and updated*/

public with sharing class UpdateInProgressReportHelper 
{
    public void updateEmailIdsinProgressReport(List<Opportunity> oplist) {   /* This method will update the email fields in the progress report */
        
        //Object used to store report data when PI AO CO or PO changes
        FGM_Base__Grantee_Report__c r = new FGM_Base__Grantee_Report__c(); 
    
        //List used for updating progress reports that need PI AO CO or PO changes
        List<FGM_Base__Grantee_Report__c> prList = new List<FGM_Base__Grantee_Report__c>(); 
        Set<id> idList = new Set<Id>();
        for(Opportunity p : oplist)
        {
           idList.add(p.id);
        }
                                                          
        Map<Id, Opportunity> OpportunityMap = new Map<id, Opportunity>([SELECT Id, Name, PI_Name_User__c,PI_Name_User__r.email, 
        PI_Project_Lead_2_Name_New__r.Email, PI_Project_Lead_Designee_1_New__r.Email, PI_Project_Lead_Designee_2_Name_New__r.Email, 
        Program_Associate__r.Email,Program_Officer__r.Email,AO_Name_User__r.Email,Contract_Administrator__r.Email,
        Contract_Coordinator__r.Email, Engagement_Officer__r.Email FROM Opportunity where id IN : idList]);
        
        List<FGM_Base__Grantee_Report__c> prSOQLList =[SELECT id,Email_PI_Project_Lead_1__c, Email_PI_Project_Lead_2__c, 
        Email_PI_Project_Lead_Designee_1__c, Email_PI_Project_Lead_Designee_2__c, Email_Program_Associate__c, Email_Program_Officer__c, 
        Email_Administrative_Official__c, Email_Contract_Administrator__c, Email_Contract_Coordinator__c, Email_Engagement_Officer__c, 
        FGM_Base__Request__c,FGM_Base__Status__c FROM FGM_Base__Grantee_Report__c WHERE FGM_Base__Request__c IN : idList];
        
        System.debug('oppSQL: ' + OpportunityMap);
        System.debug('prSQL: ' + prSOQLList);
        
                 
            for(FGM_Base__Grantee_Report__c pr : prSOQLList) 
            {
                if((pr.FGM_Base__Status__c!=null && pr.FGM_Base__Status__c!='')&&(pr.FGM_Base__Status__c=='Open' || pr.FGM_Base__Status__c=='Returned')){
                pr.Email_PI_Project_Lead_1__c=OpportunityMap.get(pr.FGM_Base__Request__c).PI_Name_User__r.email;
                pr.Email_PI_Project_Lead_2__c = OpportunityMap.get(pr.FGM_Base__Request__c).PI_Project_Lead_2_Name_New__r.Email;
                pr.Email_PI_Project_Lead_Designee_1__c = OpportunityMap.get(pr.FGM_Base__Request__c).PI_Project_Lead_Designee_1_New__r.Email;
                pr.Email_PI_Project_Lead_Designee_2__c = OpportunityMap.get(pr.FGM_Base__Request__c).PI_Project_Lead_Designee_2_Name_New__r.Email;
                pr.Email_Program_Associate__c = OpportunityMap.get(pr.FGM_Base__Request__c).Program_Associate__r.Email;
                pr.Email_Program_Officer__c = OpportunityMap.get(pr.FGM_Base__Request__c).Program_Officer__r.Email;
                pr.Email_Administrative_Official__c = OpportunityMap.get(pr.FGM_Base__Request__c).AO_Name_User__r.Email;
                pr.Email_Contract_Administrator__c = OpportunityMap.get(pr.FGM_Base__Request__c).Contract_Administrator__r.Email;
                pr.Email_Contract_Coordinator__c = OpportunityMap.get(pr.FGM_Base__Request__c).Contract_Coordinator__r.Email;
                pr.Email_Engagement_Officer__c = OpportunityMap.get(pr.FGM_Base__Request__c).Engagement_Officer__r.Email;
                prList.add(pr);
                
                }
            }
            
            if(!prList.isEmpty())
            {
                System.debug('prList: ' + prList);
                update prList;
            }
      
          //end updateEmailIdsinMilestone Method
    }

}