@isTest

public class SFProjectsListTest
{
    public static testMethod void testConstructor(){
        Id rvwRecordTypeId3 = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('RA Supplemental Funding').getRecordTypeId();
        
        list<Account> actlist=TestDataFactory.getAccountTestRecords(1);
        list<opportunity> opplist=TestDataFactory.getresearchawardsopportunitytestrecords(1, 7, 1);        
        List<Opportunity> sfList = new List<Opportunity>();
        for(Integer i = 0; i < 5; i++){
            Opportunity r = new Opportunity();
            r.RecordTypeId = rvwRecordTypeId3;
            r.Parent_Contract__c= opplist[0].Id;
            r.Application_Number__c='abcd1234'+i;
            r.StageName = 'Programmatic Review';
            r.Name='TestSFP'+i;
            r.AccountId=actlist[0].Id;
            r.CloseDate=DATE.TODAY();
            sfList.add(r);
        }        
        SFProjectsList rlEmpty = new SFProjectsList(new ApexPages.StandardController(opplist[0]));
        insert sfList;
        SFProjectsList rlWithRecordss = new SFProjectsList(new ApexPages.StandardController(opplist[0]));
    }
}