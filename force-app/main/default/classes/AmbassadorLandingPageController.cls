/****************************************************************************************************
*Description:           This Class is the controller for the AmbassadorLandingPage and ActiveAmbassadorLandingPage 
                        Visualforforce Pages
*
*
*Test Class        :    AmbassadorLandingPageController_Test
*
*****************************************************************************************************/
public class AmbassadorLandingPageController {
        public Boolean showSubmitButton {get;set;}
        public Boolean showChatter{get;set;}
        Public User us;
        Public Contact[] con;
        Public List<Ambassador__c> AmbList = new List<Ambassador__c>();
        Public Map<string, Id> Dmap = new Map<string, Id>();
        Public List<Document> FirstDocument = new List<Document>();
        List<Document> secondDocument = new List<Document>();
    public id pacgfc {get;set;}
        String AmbassadorFolderName;
             
        public AmbassadorLandingPageController() {
        showChatter=true;
              list<CollaborationGroup> cg = [select id, Name,CollaborationType,OwnerId,NetworkId from CollaborationGroup where name LIKE:Label.Pcori_Ambassadors_Group_Name AND NetworkID!=NULL LIMIT 1];
            if(!cg.isempty()) 
            pacgfc=cg[0].id;
              us=[select id,contactid from user where id=:userinfo.getUserId()];
              con= [select id from contact where Id=:us.contactid Limit 1];
              if(con.size()>0)
              AmbList = [Select Id from Ambassador__c where Contact__c=:con[0].Id];
              showSubmitButton = false;
              if(AmbList.size()==0 || AmbList==null){
              showSubmitButton = true;
              }
             else{
             showSubmitButton = false;
             }
     }
     
     public pageReference redirectToAmbassador(){
     
         try{
             PageReference pageReferenceAP = new pageReference(Label.Ambassador_Form); 
             pageReferenceAP.setRedirect(true);
             return pageReferenceAP;       
         }
          catch(exception ee){
              throw ee;
              
          }
     }

    /* public pageReference RedirectToAmbform(){
       
         if(AmbList.isEmpty()){
             PageReference pageReferenceAmb = new PageReference(label.Ambassador_Form);
             pageReferenceAmb.setRedirect(true);
             return pageReferenceAmb;  
         }             
         else{
             PageReference pageReferenceAmb1 = new PageReference(label.Ambassador_Landing_Page);
             pageReferenceAmb1.setRedirect(true);
             return pageReferenceAmb1;  
         }          
     }*/

     public Map<string, Id> getListofDocuments(){
     AmbassadorFolderName = label.Ambassador_Documents_Folder;
     FirstDocument = [SELECT Id, Name, FolderId, Folder.Name FROM Document WHERE Folder.Name =: AmbassadorFolderName ORDER BY Name ASC NULLS LAST];
     
         for(Document d:FirstDocument){
             secondDocument.add(d);
         }
              
         for(Document dP:secondDocument){
             Dmap.put(dp.Name, dp.Id);
         }
         return Dmap; 
     }

 }