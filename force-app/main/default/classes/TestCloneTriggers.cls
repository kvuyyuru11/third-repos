@isTest
public class TestCloneTriggers 
{
    static testmethod void reportRejector ()
    {
        Test.startTest();
        List<Account> accts = ObjectCreator.getAccts('Jimbo', 10);
        insert accts;
        
        List<User> users = ObjectCreator.getUsers(10, 'Donnie');
        insert users;
        
        List<Opportunity> opps = ObjectCreator.getOpps(10, 'CoolioHoolio');
        insert opps;
        
        List<FGM_Base__Grantee_Report__c> repInserts = ObjectCreator.getReports(1);
        insert repInserts;
        
        for(FGM_Base__Grantee_Report__c repUps: repInserts)
        {
            repUps.FGM_Base__Status__c = 'Returned';
            repUps.RejectClone__c = TRUE;
        }        
        update repInserts;        
        Test.stopTest();
    }
    
}