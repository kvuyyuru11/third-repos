@isTest
  
public class srchcntControllertest{

	public static testMethod void testsrchcntControllertest() { 
		
		Account acc = new Account();
		acc.Name = 'Test';
        acc.Community__c='Stakeholder';
		insert acc;
		
		Contact con = new Contact();
		con.FirstName = 'FirstTest';
		con.LastName = 'LastTest';
		con.Personal_Statement__c = 'Test';
		con.Reviewer_Role__c = 'Patient';
		con.Reviewer_Status__c = 'Applicant';
		con.Accountid = acc.id;
        con.Community__c='Patient';
		insert con;
		
		Test.startTest();
		
		ApexPages.StandardController stdController = new ApexPages.StandardController(con);
		//cntsearchcontroller controllerObj = new cntsearchcontroller(stdController);
		srchcntController cObj = new srchcntController(stdController);
        cObj.searchstring = 'LastTest';
		cObj.search();
		cObj.clear();
        
       
		Test.stopTest();
		
	}
}