@isTest
private class ReviewsClassTest
{
    static testMethod void Test() 
    {
        
        RecordType r1 = [Select Id from RecordType where name=:'RA Applications' limit 1];
        Account a = new Account();
        a.Name = 'Acme';
        insert a;
        
        RecordType r2 = [SELECT id from RecordType where name=:'PCORI Portal' limit 1];
        Campaign camp = new Campaign();
        camp.Name = 'Acme Test';
        camp.RecordTypeId = r2.Id;
        camp.Online_Review_Resources__c = 'Google*https://www.google.com; Gmail*https://www.gmail.com;';
        camp.In_Person_Review__c = 'Bing*https://www.bing.com;';
        insert camp;
        
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;  
        Contact con = new Contact(LastName ='testCon',AccountId = a.Id);  
        insert con;
        
        User user = new User(alias = 'test123', email='test123fvb@noemail.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,   
                             ContactId = con.Id,
                             timezonesidkey='America/New_York', username='testerfvb@noemail.com');
        insert user;
        
        
        Id profileId1 = [select id from profile where name='PCORI Community Partner'].id; 
        Contact con1 = new Contact(LastName ='testCon',AccountId = a.Id);  
        insert con1;  
        
        User user1 = new User(alias = 'test1234', email='test123fvb1@noemail.com',
                              emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                              localesidkey='en_US', profileid = profileId1 , country='United States',IsActive = true,     
                              ContactId = con1.Id,
                              timezonesidkey='America/New_York', username='testerfvb1@noemail.com');
        insert user1;
        
        Schema.DescribeSObjectResult oppsche = Schema.SObjectType.Opportunity;
        Map<string, schema.RecordtypeInfo> sObjectMap = oppsche.getRecordtypeInfosByName();   
        Id stroppRecordTypeId = sObjectMap.get('RA Applications').getRecordTypeId(); 
        opportunity o = new opportunity();
        
        o.Awardee_Institution_Organization__c = a.Id;
        o.RecordTypeId = r1.Id;
        o.Full_Project_Title__c = 'test';
        o.Project_Name_off_layout__c = 'test';
        //o.Panel__c=p.Id;
        o.CloseDate=Date.today();
        o.Application_Number__c = 'Ra-1234';
        o.StageName = 'In Progress';
        o.Name = 'test';
        o.PI_Name_User__c = user.id;
        o.AO_Name_User__c = user1.id;
        o.CampaignId = camp.Id;
        
        insert o;
               
        FC_Reviewer__External_Review__c Fcr1 = new FC_Reviewer__External_Review__c();
        Fcr1.FC_Reviewer__Opportunity__c = o.id;
        insert Fcr1;
        
        Test.startTest();
        
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(Fcr1);
        ApexPages.currentPage().getParameters().put('Id',Fcr1.id);
        
        ReviewsClass p1 = new ReviewsClass();
            
        FC_Reviewer__External_Review__c Fcr2 = p1.rvs;
        System.assertEquals(Fcr1.id,Fcr2.id);

        System.assertEquals(Fcr1.id, p1.rvs.id);

        String baseUR = 'www.google.com';
        p1.baseURL='www.google.com';
        System.assertEquals(baseUR,p1.baseURL);
        
        System.assertEquals(1, p1.InPersonReview.size());
        System.assertEquals(2, p1.OnlineReview.size());
        
        System.assertEquals('https://www.bing.com', p1.InPersonReview.get('Bing'));
        
        System.assertEquals('https://www.google.com', p1.OnlineReview.get('Google'));
        //System.assertEquals('https://www.gmail.com', p1.OnlineReview.get('Gmail'));
        
        Test.stopTest();
    }
}