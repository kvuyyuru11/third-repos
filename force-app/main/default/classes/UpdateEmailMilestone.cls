/* This class belongs to the trigger MilestoneEmailUpdate. 
This class will update Email Fields in the Milestone only when the Milestone Object is edited or created.*/

Public with sharing class UpdateEmailMilestone{

//Construtor
    Public UpdateEmailMilestone(){
    }

   Public void UpdateEmailonMilestone (List<FGM_Base__Benchmark__c> msList){ /* This method will update email fields in the Milestone Object*/
   
    Set <Id> parentIds = new set<Id>();
    for(FGM_Base__Benchmark__c ms : msList) {
        parentIds.add(ms.FGM_Base__Request__c);
    }
      
    List<Opportunity> oppSOQLList =[SELECT Id, Name, PI_Name_User__c,PI_Name_User__r.email, PI_Project_Lead_2_Name_New__r.Email, PI_Project_Lead_Designee_1_New__r.Email, PI_Project_Lead_Designee_2_Name_New__r.Email,Program_Associate__r.Email,Program_Officer__r.Email,AO_Name_User__r.Email,Contract_Administrator__r.Email,Contract_Coordinator__r.Email FROM Opportunity where id IN : parentIds LIMIT 50000]; 

    List<FGM_Base__Benchmark__c> msSOQLList =[SELECT id,Email_PI_Project_Lead_1__c, Email_PI_Project_Lead_2__c, Email_PI_Project_Lead_Designee_1__c, Email_PI_Project_Lead_Designee_2__c, Email_Administrative_Official__c, Email_Program_Associate__c, Email_Program_Officer__c, Email_Contract_Administrator__c, Email_Contract_Coordinator__c, FGM_Base__Request__c FROM FGM_Base__Benchmark__c WHERE FGM_Base__Request__c IN : parentIds LIMIT 50000];
          // System.debug('############# irbSOQLList'+irbSOQLList); 

   //Map<Id, IRB__C> irbmap = new Map<Id, IRB__C>();
   Map<Id, Opportunity> oppmap = new Map<Id, Opportunity>();

  for(Opportunity opp: oppSOQLList)
  {
     oppmap.put(opp.id, opp);

  }
 try{ 
  for(FGM_Base__Benchmark__c ms:msList)
  {     
            ms.Email_PI_Project_Lead_1__c = oppmap.get(ms.FGM_Base__Request__c).PI_Name_User__r.email;
            ms.Email_PI_Project_Lead_2__c = oppmap.get(ms.FGM_Base__Request__c).PI_Project_Lead_2_Name_New__r.Email;
            ms.Email_PI_Project_Lead_Designee_1__c = oppmap.get(ms.FGM_Base__Request__c).PI_Project_Lead_Designee_1_New__r.Email;
            ms.Email_PI_Project_Lead_Designee_2__c = oppmap.get(ms.FGM_Base__Request__c).PI_Project_Lead_Designee_2_Name_New__r.Email;
            ms.Email_Administrative_Official__c = oppmap.get(ms.FGM_Base__Request__c).AO_Name_User__r.Email;
            ms.Email_Program_Associate__c = oppmap.get(ms.FGM_Base__Request__c).Program_Associate__r.Email;
            ms.Email_Program_Officer__c = oppmap.get(ms.FGM_Base__Request__c).Program_Officer__r.Email;
            ms.Email_Contract_Administrator__c = oppmap.get(ms.FGM_Base__Request__c).Contract_Administrator__r.Email;
            ms.Email_Contract_Coordinator__c = oppmap.get(ms.FGM_Base__Request__c).Contract_Coordinator__r.Email;
                      
        }// end for
   }catch(DmlException e) {
           System.debug('The following exception has occurred: ' + e.getMessage());       
   }Catch(Exception e)  {
           System.debug('The following exception has occurred: ' + e.getMessage());
   }// try catch block
    
       
    }//end UpdateEmailonMilestone

}//end UpdateEmailMilestone