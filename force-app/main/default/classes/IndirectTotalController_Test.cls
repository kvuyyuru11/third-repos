@IsTest
public class IndirectTotalController_Test {
    public static testmethod void IndirectTotalController_TestMethod_1() {
        //list<Campaign> cmplist = [select Id,name from campaign where name='Symptom Management for Patients with Advanced Illness' LIMIT 1];
        Account acc = new account(Name = 'Opt Out', Community__c = 'TBD');
        insert acc;
        Opportunity Opp = new Opportunity(Name='Total calculator Test Opportuity',AccountId=acc.Id,StageName='Qualification',closedate=system.today()+5,
                                          Full_Project_Title__c='testpms',Project_Name_off_layout__c='testpms',Project_End_Date__c=system.today()+1
                                         );
        insert Opp;
        campaign cmp = new campaign(Name='Symptom Management for Patients with Advanced Illness');
        insert cmp;
        Budget__c bgt = new Budget__c(Associated_App_Project__c=Opp.Id,Scientific_Travel_Costs_Year_1__c=10001);
        insert bgt;
        ApexPages.currentPage().getHeaders().put('Referer','id='+cmp.id+'&');
        IndirectTotalController ITC = new IndirectTotalController();
        ITC.appId=Opp.Id;
        ITC.grabListData();
        ITC.grabId();
        ITC.grabListDataReload();
        ITC.saveBudget();
        
    }
    public static testmethod void IndirectTotalController_TestMethod_2() {
        Account acc = new account(Name = 'Opt Out', Community__c = 'TBD');
        insert acc;
        Opportunity Opp = new Opportunity(Name='Total calculator Test Opportuity',AccountId=acc.Id,StageName='Qualification',closedate=system.today()+5,
                                          Full_Project_Title__c='testpms',Project_Name_off_layout__c='testpms',Project_End_Date__c=system.today()+1,
                                          External_Status__c='Submitted');
        insert Opp;
        
        campaign cmp = new campaign(Name='Medication-Assisted Treatment (MAT) Delivery for Pregnant Women with SUD');
        insert cmp;
        Budget__c bgt = new Budget__c(Associated_App_Project__c=Opp.Id,Scientific_Travel_Costs_Year_1__c=10001);
        insert bgt;
        ApexPages.currentPage().getHeaders().put('Referer','id='+cmp.id+'&');
        IndirectTotalController ITC = new IndirectTotalController();
        ITC.appId=Opp.Id;
        ITC.grabListData();
    }
    public static testmethod void IndirectTotalController_TestMethod_3() {
        Account acc = new account(Name = 'Opt Out', Community__c = 'TBD');
        insert acc;
        Opportunity Opp = new Opportunity(Name='Total calculator Test Opportuity',AccountId=acc.Id,StageName='Qualification',closedate=system.today()+5,
                                          Full_Project_Title__c='testpms',Project_Name_off_layout__c='testpms',Project_End_Date__c=system.today()+1,
                                          External_Status__c='Submitted');
        insert Opp;        
        campaign cmp = new campaign(Name='Pragmatic Clinical Studies to Evaluate Patient-Centered Outcomes');
        insert cmp;
        Budget__c bgt = new Budget__c(Associated_App_Project__c=Opp.Id,Scientific_Travel_Costs_Year_1__c=10001);
        insert bgt;
        ApexPages.currentPage().getHeaders().put('Referer','id='+cmp.id+'&');
        IndirectTotalController ITC = new IndirectTotalController();
        ITC.appId=Opp.Id;
        ITC.grabListData();
    }
    public static testmethod void IndirectTotalController_TestMethod_4() {
        Account acc = new account(Name = 'Opt Out', Community__c = 'TBD');
        insert acc;
        Opportunity Opp = new Opportunity(Name='Total calculator Test Opportuity',AccountId=acc.Id,StageName='Qualification',closedate=system.today()+5,
                                          Full_Project_Title__c='testpms',Project_Name_off_layout__c='testpms',Project_End_Date__c=system.today()+1,
                                          External_Status__c='Submitted');
        insert Opp;        
        campaign cmp = new campaign(Name='Dissemination and Implementation');
        insert cmp;
        Budget__c bgt = new Budget__c(Associated_App_Project__c=Opp.Id,Scientific_Travel_Costs_Year_1__c=10001);
        insert bgt;
        ApexPages.currentPage().getHeaders().put('Referer','id='+cmp.id+'&');
        IndirectTotalController ITC = new IndirectTotalController();
        ITC.appId=Opp.Id;
        ITC.grabListData();
    }
    
}