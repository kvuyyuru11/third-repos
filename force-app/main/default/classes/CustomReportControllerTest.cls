@isTest
public class CustomReportControllerTest{
    private static testMethod void testReviewer(){
        RecordType recOpp = [SELECT Id FROM RecordType WHERE DeveloperName = 'Research_Awards' AND sObjectType = 'Opportunity'];
        RecordType recAcc = [SELECT Id FROM RecordType WHERE DeveloperName = 'Standard_Salesforce_Account' AND sObjectType = 'Account'];
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.RecordTypeId = recAcc.Id;
        insert acc;
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = recOpp.Id;
        opp.AccountId = acc.Id;
        opp.Contract_Number__c = '03423';
        opp.Name = 'Test';
        opp.StageName = 'In Progress';
        opp.Application_Number__c = '12213';
        opp.Full_Project_Title__c = 'Project Test';
        opp.CloseDate = Date.newInstance(2016, 12, 22);
        insert opp;
        Cycle__c cy = new Cycle__c();
        cy.Name = 'Test';
        cy.COI_Due_Date__c = Date.newInstance(2016,12,11);
        insert cy;
        Panel__c p = new Panel__c();
        p.Name = 'Test';
        p.Cycle__c = cy.Id;
        p.Panel_Due_Dtae__c=  Date.newInstance(2019,12,20); 
        insert p;
        FC_Reviewer__External_Review__c rev = new FC_Reviewer__External_Review__c();
        //rev.FC_Reviewer__Status__c = 'Review Complete';
        rev.FC_Reviewer__Opportunity__c = opp.Id;
        rev.Panel__c = p.Id;
        insert rev;
        CustomReportController cr = new CustomReportController(new ApexPages.StandardController(p));
    }
}