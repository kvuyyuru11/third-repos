/*------------------------------------------------------------------------------------------
 *  Date            Project             Developer                       Justification
 *  03/19/2017  Invoicing-Phase2        Vinayak Sharma, REI Systems Inc   Creation
 *  03/19/2017  Invoicing-Phase2        Vinayak Sharma, REI Systems Inc This class is working as a Wrapper to be used in creating the Data which needs to be displayed on the User Interface
 * ---------------------------------------------------------------------------------------------
*/

public class InvoiceLineItemWrapper {
    
    public Invoice_Line_Item__c lineItem { get; set; }

    public List<Invoice_Line_Item__c> lineItems { get; set; }
    public String LineItemTitle { get; set; }
    
    public decimal Consultant_Costs_Budget { get; set; }
    public decimal Consultant_Costs_Cumulative { get; set; }   

    public decimal Supplies_Budget { get; set; }
    public decimal Supplies_Cumulative { get; set; }      

    public decimal Travel_Budget { get; set; }
    public decimal Travel_Cumulative { get; set; }       

    public decimal Other_Costs_Budget { get; set; }
    public decimal Other_Costs_Cumulative { get; set; }    
    
    public decimal Equipment_Budget { get; set; }
    public decimal Equipment_Cumulative { get; set; }   
    
    //public decimal Research_Related_I_O_Budget { set; get; }       
    //public decimal Research_Related_I_O_Cumulative { set; get; } 

    public decimal Consortium_Contractual_Costs_Budget{ set; get; }       
    public decimal Consortium_Contractual_Costs_Cumulative{ set; get; }    
    
    public decimal indirect_Costs_Budget { set; get; }       
    public decimal indirect_Costs_Cumulative { set; get; }     
    
    public decimal fringe_Benefits_Budget{ set; get; }       
    public decimal fringe_Benefits_Cumulative { set; get; } 
    
    public decimal salary_Budget{ set; get; }       
    public decimal salary_Cumulative { set; get; }
                        
    public InvoiceLineItemWrapper() {
    }
        
   public InvoiceLineItemWrapper(String title, Invoice_Line_Item__c li, 
                                        
                                        decimal salary_Budget, decimal salary_Cumulative,
                                        decimal fringe_Benefits_Budget, decimal fringe_Benefits_Cumulative,
                                        decimal consultant_Costs_Budget, decimal consultant_Costs_Cumulative,
                                        decimal supplies_Budget, decimal supplies_Cumulative,
                                        decimal Travel_Budget, decimal Travel_Cumulative,
                                        decimal Other_Costs_Budget, decimal Other_Costs_Cumulative,
                                        decimal Equipment_Budget, decimal Equipment_Cumulative, 
                                        decimal Consortium_Contractual_Costs_Budget, decimal Consortium_Contractual_Costs_Cumulative, 
                                        decimal indirect_Costs_Budget, decimal indirect_Costs_Cumulative
                                        )
     {
        this.LineItemTitle = title;
        
        this.Consultant_Costs_Budget = consultant_Costs_Budget;
        this.Consultant_Costs_Cumulative = consultant_Costs_Cumulative ;

        this.Supplies_Budget = supplies_Budget;
        this.Supplies_Cumulative = supplies_Cumulative ;

        this.Travel_Budget  = Travel_Budget  ;
        this.Travel_Cumulative = Travel_Cumulative  ;
        
        this.Other_Costs_Budget  = Other_Costs_Budget  ;
        this.Other_Costs_Cumulative = Other_Costs_Cumulative ;        

        this.Equipment_Budget = Equipment_Budget;
        this.Equipment_Cumulative = Equipment_Cumulative;  
        
        //this.Research_Related_I_O_Budget  = Research_Related_I_O_Budget ;
        //this.Research_Related_I_O_Cumulative = Research_Related_I_O_Cumulative ;

        this.Consortium_Contractual_Costs_Budget = Consortium_Contractual_Costs_Budget;
        this.Consortium_Contractual_Costs_Cumulative = Consortium_Contractual_Costs_Cumulative;
        
        this.indirect_Costs_Budget = indirect_Costs_Budget;
        this.indirect_Costs_Cumulative = indirect_Costs_Cumulative;
        
        this.fringe_Benefits_Budget= fringe_Benefits_Budget;
        this.fringe_Benefits_Cumulative = fringe_Benefits_Cumulative ;

        this.salary_Budget = salary_Budget;
        this.salary_Cumulative = salary_Cumulative ;
                                
        this.lineItem = li; 
        lineItems = new List<Invoice_Line_Item__c>();
        lineItems.add(lineItem); 
        
    }   
    
    public InvoiceLineItemWrapper(String title, Invoice_Line_Item__c li){
        this.LineItemTitle = title;  
        lineItems = new List<Invoice_Line_Item__c>();
        lineItems.add(lineItem);       
    }
    
}