// AI-1233 | RAr3_C3_011 | 17190
// By: Luis M. Rocha in SADC
// Description: This class utilizes the sharing keyword so that it may only display remediations shared with the current
// user. It also only displayes remediations with the following statuses. 
// 'Pending Remediation Plan' 
//'Submitted' 
//'Rejected - Needs Revision' 
//'Pending Final Remediation Plan' 
//'Submitted Final Remediation Plan' 
//'Rejected Final Plan – Additional Information Requested' 
//If the query does not return any remediations. (I.E. No remediations shared with this user or none matching the criteria)
//The controller will return a null list so that the vfp will not generate a table. 
public with sharing class RemediationList {
    @TestVisible private Opportunity opp;
    //public List<Audit__c> auditlst {get;set;}
    public List<Remediation__c> remediationList {get;set;}
    public RemediationList(ApexPages.StandardController ctr){
        this.opp= (Opportunity)ctr.getRecord();
        if(opp.Id != null){
			//Query for record matching criteria.
            remediationList = [SELECT Id, Name, RecordTypeId, 
                               Initiating_Dept_Director_NoCL_Approval__c, Status__c, 
                               Remediation_Status__c FROM Remediation__c
                               WHERE Project__c = :opp.Id AND (
                                   Remediation_Status__c =: Label.Pending_Remediation_Plan OR
                                   Remediation_Status__c =: Label.Submitted OR
                                   Remediation_Status__c =: Label.Rejected_Needs_Revision OR
                                   Remediation_Status__c =: Label.Pending_Final_Remediation_Plan OR
                                   Remediation_Status__c =: Label.Submitted_Final_Remediation_Plan OR
                                   Remediation_Status__c =: Label.Rejected_Final_Plan_Additional_Information_Requested OR
                                   Remediation_Status__c =: Label.Rejected_Additional_Information_Requested
                                   
                               
                               )];
        }
        if(RemediationList.size() == 0){
            //Updated code to return a null list if the list is empty so that it will not generate a page on the page layout. 
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No remediations to display'));
            RemediationList = null;
        }
    }
}