@isTest
private class BudgetPageRouterController_Test {
	
	@isTest static void testBudgetPageRouterController() {
		Id r1 =Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Research Awards').getRecordTypeId();
		
		Id pcoriCommunityPartID = [SELECT Id FROM Profile WHERE name = 'PCORI Community Partner'].Id;

		Account.SObjectType.getDescribe().getRecordTypeInfos();

		Account a = new Account();
		a.Name = 'Acme1';
		insert a;

		Contact con = new Contact(LastName = 'testCon', AccountId = a.Id);
		Contact con1 = new Contact(LastName = 'testCon1', AccountId = a.Id);
		Contact con2 = new Contact(LastName = 'testCon1', AccountId = a.Id);
		Contact con3 = new Contact(LastName = 'testCon2', AccountId = a.Id);

		List<Contact> cons = new List<Contact>();
		cons.add(con);
		cons.add(con1);
		cons.add(con2);
		cons.add(con3);
		insert cons;
		
		User user = new User(Alias = 'test123', Email = 'test123fvb@noemail.com',
							 Emailencodingkey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',    
							 LocaleSidKey = 'en_US', ProfileId = pcoriCommunityPartID , Country='United States',IsActive = true,   
							 ContactId = con.Id, CommunityNickname = 'test12',
							 TimeZoneSidKey='America/New_York', UserName = 'testerfvb1@noemail.com');

		User user1 = new User(Alias = 'test1234', Email = 'test1234fvb@noemail.com',
							  Emailencodingkey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',    
							  LocaleSidKey = 'en_US', ProfileId = pcoriCommunityPartID , Country='United States',IsActive = true,   
							  ContactId = con1.Id, CommunityNickname = 'test12345',
							  TimeZoneSidKey='America/New_York', UserName = 'testerfvb123@noemail.com');

		User user2 = new User(Alias = 'test145', Email = 'test12345fvb@noemail.com',
							  Emailencodingkey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',    
							  LocaleSidKey = 'en_US', ProfileId = pcoriCommunityPartID , Country='United States',IsActive = true,   
							  ContactId = con2.Id, CommunityNickname = 'test14',
							  TimeZoneSidKey='America/New_York', UserName = 'testerfvb12@noemail.com');
		
		User user3 = new User(Alias = 'test52', Email = 'test135fvb@noemail.com',
							  Emailencodingkey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',    
							  LocaleSidKey = 'en_US', ProfileId = pcoriCommunityPartID , Country='United States',IsActive = true,   
							  ContactId = con3.Id, CommunityNickname = 'test5',
							  TimeZoneSidKey = 'America/New_York', UserName = 'testerfvb82@noemail.com');

		List<User> users = new List<User>();
		users.add(user);
		users.add(user1);
		users.add(user2);
		users.add(user3);
		insert users;
		
		Cycle__c c = new Cycle__c();
		c.Name = 'testcycle';
		c.COI_Due_Date__c = date.valueof(System.now());
		insert c;
		
		opportunity o = new opportunity();
		
		o.Awardee_Institution_Organization__c=a.Id;
		o.RecordTypeId = r1;
		o.Full_Project_Title__c='test';
		o.Project_Name_off_layout__c='test';
		o.CloseDate = Date.today();
		o.Application_Number__c='Ra-1234';
		o.StageName = 'Executed';
		o.Name='test';
		o.PI_Name_User__c = user.id;
		o.AO_Name_User__c = user1.id;
		o.Project_Start_Date__c = system.Today();
		
		insert o;
		
		Budget__c b = new Budget__c();
		b.Associated_App_Project__c= o.id;
		b.Bypass_Flow__c = true;
		insert b;

		Test.startTest();

		BudgetPageRouterController router = new BudgetPageRouterController();

		router.projectAppId = o.Id;
		router.budgetId = b.Id;
		router.router();

		router = new BudgetPageRouterController();
		router.budgetId = b.Id;
		router.router();


		System.runAs(user) {
			router = new BudgetPageRouterController();

			router.budgetId = b.Id;

			router.router();
			System.assert(router.projectAppId != null);
		}

		router = new BudgetPageRouterController();
		router.projectAppId = o.Id;
		System.assert(router.router().getParameters().get('showHeader') == 'false');

		ApexPages.currentPage().getParameters().put('Id',b.Id);
		ApexPages.currentPage().getParameters().put('retURL','/engagement/' + o.Id);
		router = new BudgetPageRouterController();
		PageReference page = router.router();
		System.assert(page.getParameters().get('retURL') == '/engagement/' + o.Id);
		System.assert(router.budgetId != null);

		Test.stopTest();

	}
	
	
}