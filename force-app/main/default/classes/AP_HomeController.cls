/****************************************************************************************************
*Description:           Controler class for the Application page HOME / LANDING PAGE vfp
*                       
*
*Required Class(es):    N/A
*
*Organization: PCORI / Accenture
*Revision   |Date       |Author             |AdditionalNotes
*====================================================================================================
*   1.0      11/19/2015     Trey arose          Initial Version
*   1.01     1/10/2016      Anirudh Sharma      Updated for Pcori-Portal
*   1.02     1/10/2016      Daniel Haro         Bug fixes
*   1.03     1/15/2016      Chris Holby         Changed APList logic
*****************************************************************************************************/  


global with sharing class AP_HomeController {
    
    public boolean showbutton1{get;set;}
    public boolean showbutton2{get;set;}
    public boolean showbutton3{get;set;}
    public boolean showbutton4{get;set;}
    
    
    
    public list<Advisory_Panel_Application__c> APlist{get;set;} 
    public list<Advisory_Panel_Application__c> APAList{get;set;}
  
    //constructor 
    global AP_HomeController(){    
       
         //Query contact ID from current Portal User  
        User u=[select id,contactid from User where id=:UserInfo.getUserId() limit 1];
        
        //Create Advisory Panel Applications list with columns for Portal User
        APlist = [select id, Name, Application_Status_new__c,Date_Submitted__c, 
                  CreatedDate, Contact__r.Firstname, Contact__r.Lastname, 
                  Advisory_Panel_Application_Cycle__c, Advisory_Panels_Applied_For__c 
                  from Advisory_Panel_Application__c 
                  //Find all Advisory Panel Applications associated to the contact of the current Portal User
                  where Contact__c=:u.contactid
                  //More recently created applications appear at top of list          
                  order by CreatedDate DESC ];
                  system.debug('******'+APlist);
        
        
        system.debug('the size of Aplist is' + Aplist.size());
        showbutton1=true;
       //  system.assert(false, 'size is:' + aplist.size() +   'status' + APlist[0].Application_Status_New__c);
        for(Advisory_Panel_Application__c a:aplist){
          
           if(APlist.size()>0 && APlist[0].Application_Status_New__c=='Draft'){
                showbutton1=false;
               showbutton2=true;
               showbutton3=false;
               showbutton4=false;
            } 
            
            if(APlist[0].Application_Status_New__c=='Submitted'){
                showbutton3=true;
               showbutton1=false;
                showbutton2=false;
                showbutton4=false;
            } 
            
             if(APlist[0].Application_Status_New__c=='Withdrawn'){
                  Showbutton4=true;
        Showbutton1=true;
        Showbutton2=false;
        Showbutton3=false;
            } 
           if(APlist[0].Application_Status_New__c=='Not Selected'){
                  Showbutton4=true;
        Showbutton1=true;
        Showbutton2=false;
        Showbutton3=false;
        }
          if(APlist[0].Application_Status_New__c=='Selected'){
                  Showbutton4=true;
        Showbutton1=true;
        Showbutton2=false;
        Showbutton3=false;
        }
          if(APlist[0].Application_Status_New__c=='Under Review'){
                  Showbutton4=false;
        Showbutton1=false;
        Showbutton2=false;
        Showbutton3=true;
        }
        }
        
    }
    
 
    
    public pagereference withdrawmethod() {
       User u=[select id,contactid from User where id=:UserInfo.getUserId() limit 1];
        Advisory_Panel_Application__c  APlist1 = [select id, Name, Application_Status_New__c, CreatedDate, Contact__r.Firstname, Contact__r.Lastname from Advisory_Panel_Application__c where contact__c=:u.contactid  order by CreatedDate DESC limit 1];
        Aplist1.Application_Status_new__c='Withdrawn';
        database.update(Aplist1);
        pagereference pg=new pagereference('/Ap_Home');
        return pg;
      

    }
       public pagereference SApplication() {
            
        pagereference pg=new pagereference(label.AP_Application_Start);
           
        return pg;
          }
    //used to submit the application 
        Public pagereference cloneApplication(){
        List<Advisory_Panel_Application__c> APA = new List<Advisory_Panel_Application__c>();
        Advisory_Panel_Application__c APA1 = new Advisory_Panel_Application__c();
         User u=[select id,contactid from User where id=:UserInfo.getUserId() limit 1];
        Advisory_Panel_Application__c APA1obj = [select Id,Name,Gender_del__c,Previous_involvement_with_PCORI__c,Currently_employed_by_the_federal_govt__c,Current_Position_or_Title__c,Current_Employer__c,Race_Ethnicity__c,Are_you_Hispanic_Latino__c,Age_in_Years__c,Additional_Supportive_Information__c,Please_tell_us_anything_else__c,Permission_to_post_bios_on_website__c,Bio__c,Personal_Statement2__c,Which_disease_condition_area_represented__c,Advisory_Panels_Applied_For__c,Preferred_Advisory_Panel__c  ,Categories_identified_with__c,Caregiver_categories_identified_with__c,Clinician_categories_identified_with__c,Groups_identified_with_or_represented__c,Hospital_Health_System_identified_with__c,Purchaser_categories_identified_with__c,Payer_categories_identified_with__c,Industry_categories_identified_with__c,Health_Research_identified_with__c,Policy_Maker_category_you_identify_with__c,Training_Institution_you_identify_with__c,Primary_Clinical_Trials_Community__c,Primary_Rare_Disease_Community__c,Involved_in_Research_Prioritization__c,Describe_research_prioritization_involve__c,engaging_patients_and_other_stakeholders__c,Ever_engaged_in_PCORI_research__c,Describe_PC_trial_involvement__c,Relevant_societies_or_organizations__c,Patient_centric_clinicals_involvment__c,Describe_clinical_trial_involvement__c,Participated_in_Clinical_Trial__c,Development_of_Processes_or_Framework__c,Data_related_research_involvement__c,Describe_data_research_involvement__c,Multi_stakeholder_processes_frameworks__c,Please_describe_your_involvement__c,Describe_Engaging_Patients__c, Application_Status_New__c,Engaged_in_PCOR_research__c,Describe_PCOR_Research__c,Describe_PCORI_research_involvement__c,Describe_multi_stakeholder_involvement__c,Degrees__c,What_is_your_highest_level_of_education__c,CreatedDate,Contact__c,Last_Name__c,Abide_by_PCORI_COI_Policy__c, Able_to_attend_a_one_to_two_day_meeting__c from Advisory_Panel_Application__c where Contact__c=:u.contactid order by CreatedDate DESC limit 1];
        APA1.Application_Status_New__c = 'Draft';
        //APA1.Application_Status_new__c='Withdrawn';    
            system.debug('The status is ####' + APA1.Application_Status_New__c);
        APA1.Abide_by_PCORI_COI_Policy__c = APA1obj.Abide_by_PCORI_COI_Policy__c;
       APA1.Contact__c=APA1obj.Contact__c;
        APA1.Last_Name__c=APA1obj.Last_Name__c;
            APA1.Gender_del__c=APA1obj.Gender_del__c;
            
         APA1.Able_to_attend_a_one_to_two_day_meeting__c=APA1obj.Able_to_attend_a_one_to_two_day_meeting__c;
         //APA1.Accept_Compensation__c=APA1obj.Accept_Compensation__c;
         //APA1.Travel_Reimbursement__c=APA1obj.Travel_Reimbursement__c;
        APA1.What_is_your_highest_level_of_education__c=APA1obj.What_is_your_highest_level_of_education__c; 
        APA1.Age_in_Years__c=APA1obj.Age_in_Years__c;
            APA1.Degrees__c=APA1obj.Degrees__c;
            APA1.Are_you_Hispanic_Latino__c=APA1obj.Are_you_Hispanic_Latino__c;
            APA1.Race_Ethnicity__c=APA1obj.Race_Ethnicity__c;
            APA1.Current_Employer__c=APA1obj.Current_Employer__c;
            APA1.Current_Position_or_Title__c=APA1obj.Current_Position_or_Title__c;
            APA1.Currently_employed_by_the_federal_govt__c=APA1obj.Currently_employed_by_the_federal_govt__c;
            APA1.Previous_involvement_with_PCORI__c=APA1obj.Previous_involvement_with_PCORI__c;
         APA1.Describe_multi_stakeholder_involvement__c=APA1obj.Describe_multi_stakeholder_involvement__c;
         APA1.Describe_PCOR_Research__c=APA1obj.Describe_PCOR_Research__c;
            APA1.Describe_PCORI_research_involvement__c=APA1obj.Describe_PCORI_research_involvement__c;
            APA1.Engaged_in_PCOR_research__c=APA1obj.Engaged_in_PCOR_research__c;
            APA1.Describe_Engaging_Patients__c=APA1obj.Describe_Engaging_Patients__c;
            APA1.Please_describe_your_involvement__c=APA1obj.Please_describe_your_involvement__c;
            APA1.Multi_stakeholder_processes_frameworks__c=APA1obj.Multi_stakeholder_processes_frameworks__c;
            APA1.Describe_data_research_involvement__c=APA1obj.Describe_data_research_involvement__c;
            APA1.Data_related_research_involvement__c=APA1obj.Data_related_research_involvement__c;
            APA1.Development_of_Processes_or_Framework__c=APA1obj.Development_of_Processes_or_Framework__c;
            APA1.Participated_in_Clinical_Trial__c=APA1obj.Participated_in_Clinical_Trial__c;
            APA1.Describe_clinical_trial_involvement__c=APA1obj.Describe_clinical_trial_involvement__c;
            APA1.Patient_centric_clinicals_involvment__c=APA1obj.Patient_centric_clinicals_involvment__c;
            APA1.Relevant_societies_or_organizations__c=APA1obj.Relevant_societies_or_organizations__c;
            APA1.Describe_PC_trial_involvement__c=APA1obj.Describe_PC_trial_involvement__c;
            APA1.Describe_PCORI_research_involvement__c=APA1obj.Describe_PCORI_research_involvement__c;
            APA1.Ever_engaged_in_PCORI_research__c=APA1obj.Ever_engaged_in_PCORI_research__c;
            APA1.engaging_patients_and_other_stakeholders__c=APA1obj.engaging_patients_and_other_stakeholders__c;
            APA1.Describe_research_prioritization_involve__c=APA1obj.Describe_research_prioritization_involve__c;
            APA1.Involved_in_Research_Prioritization__c=APA1obj.Involved_in_Research_Prioritization__c;
            APA1.Primary_Rare_Disease_Community__c=APA1obj.Primary_Rare_Disease_Community__c;
            APA1.Primary_Clinical_Trials_Community__c=APA1obj.Primary_Clinical_Trials_Community__c;
            APA1.Training_Institution_you_identify_with__c=APA1obj.Training_Institution_you_identify_with__c;
            APA1.Policy_Maker_category_you_identify_with__c=APA1obj.Policy_Maker_category_you_identify_with__c;
            APA1.Health_Research_identified_with__c=APA1obj.Health_Research_identified_with__c;
            APA1.Industry_categories_identified_with__c=APA1obj.Industry_categories_identified_with__c;
            APA1.Payer_categories_identified_with__c=APA1obj.Payer_categories_identified_with__c;
            APA1.Purchaser_categories_identified_with__c=APA1obj.Purchaser_categories_identified_with__c;
            APA1.Hospital_Health_System_identified_with__c=APA1obj.Hospital_Health_System_identified_with__c;
            APA1.Groups_identified_with_or_represented__c=APA1obj.Groups_identified_with_or_represented__c;
            APA1.Clinician_categories_identified_with__c=APA1obj.Clinician_categories_identified_with__c;
            APA1.Caregiver_categories_identified_with__c=APA1obj.Caregiver_categories_identified_with__c;
            APA1.Categories_identified_with__c=APA1obj.Categories_identified_with__c;
            APA1.Preferred_Advisory_Panel__c  =APA1obj.Preferred_Advisory_Panel__c;
            APA1.Advisory_Panels_Applied_For__c=APA1obj.Advisory_Panels_Applied_For__c;
            APA1.Which_disease_condition_area_represented__c=APA1obj.Which_disease_condition_area_represented__c;
            APA1.Personal_Statement2__c=APA1obj.Personal_Statement2__c;
            APA1.Bio__c=APA1obj.Bio__c;
            APA1.Permission_to_post_bios_on_website__c=APA1obj.Permission_to_post_bios_on_website__c;
            APA1.Please_tell_us_anything_else__c=APA1obj.Please_tell_us_anything_else__c;
            APA1.Additional_Supportive_Information__c=APA1obj.Additional_Supportive_Information__c;
    
            
        APA.add(APA1);
        if(APA.size()>0 && APA!=null)
        
         {
         insert APA1;
         }
          return new pagereference('/Ap_Application');
         }
  
    }