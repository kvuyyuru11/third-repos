//This is a TriggerHandler Class that handles the methods on MRA Object
public class MRATriggerClass {
Id contactid;
String emailid;
String text1=Label.Expired;
String text2=Label.Not_Accepted_as_Reviewer;
String text3=Label.Applicant_Not_Accepted;
String text4=Label.Accepted_as_Reviewer;
String text5=Label.New_Reviewer;
String text6=Label.Scientific;
String text7=Label.Patient;
String text8=Label.Stakeholder;
String text9=Label.Applicant;
string text10=Label.Conditionally_Accepted;
string text11=Label.Approved_Pending_Federal_Authorization;    
    
public mraTriggerClass(){}
     
    /*public static void getComments(Merit_Reviewer_Application__c mraIDSet){
     List<Merit_Reviewer_Application__c> mrApps=[Select c.Id, (Select Id, IsPending, ProcessInstanceId, TargetObjectId, StepStatus, OriginalActorId, ActorId, RemindersSent, Comments, IsDeleted, CreatedDate, CreatedById, SystemModstamp From ProcessSteps) 
                      From Merit_Reviewer_Application__c c where id=:mraIDSet];
    List<Merit_Reviewer_Application__c> updateMRAList = new List<Merit_Reviewer_Application__c>();
  if (mrApps.size()>0)
  {
     for(Merit_Reviewer_Application__c mrAObj : mrApps){
         Merit_Reviewer_Application__c mrTemp=mrAObj;
         String commentsStr='';
         String str= '';
         for (ProcessInstanceHistory ps : mrTemp.ProcessSteps)
         {
            commentsStr+='\nComment from user ' + ps.ActorId + ' : ' + ps.comments;
             system.debug('===commentsStr==='+commentsStr);
             if(ps.comments !=null){
                 str = ps.comments;
             }
         }
         system.debug('===see the comments===='+str); 
         mrAObj.comments__c= str;
         updateMRAList.add(mrAObj);
     }
  }
        if(updateMRAList != null && !updateMRAList.isEmpty()) {
            update updateMRAList;
        }
       
}*/
    //This method updates the details on Contact Object from MRA Object which are entered from flow.
public void updatecnt(Merit_Reviewer_Application__c u)
    {
     system.debug('The trigger.new is:' + u.ownerid);
        //Below is the soql query to get the email of the user who is entering the details on flow
     User cntId1= [Select contactId from User where id=:u.ownerid ];
     //emailid=string.valueof(emailofuser.email);
    system.debug('The Email id is:' + emailid);
 //Below is the soql query to get field values from the contact and once we get values from flow copy those values onto the queried fields on contact
   Contact c;
   if(cntId1.ContactId != null){
       c = [select id,Email,Name,Served_as_Chair_or_Co_Chair__c,Reviewer_Methodological_Expertise__c,Reviewer_Healthcare_Expertise__c,Reviewer_Population_Expertise__c,Current_Employer__c,Reviewer_Disease_Condition_Expertise__c,Specific_Population_Expertise__c,Specific_Methodological_Expertise__c,Specific_Healthcare_Expertise__c,Current_Position_or_Title__c,Specific_Disease_Condition_Expertise__c,Healthcare_Expertise__c,Reviewer_Status__c,Country__c,Age__c,Race__c,Disease_Condition_Expertise__c,Federal_Employee__c,Population_Expertise__c,Preferred_Program__c,Reviewer_Role__c,Gender_Research__c,Methodologic_Expertise__c from Contact where ID=:cntId1.ContactId limit 1];
   }
   
    if(c != null){
        system.Debug('The contacts details are:' + c);
             // c.Current_Employer__c = u.Current_Employer__c; 
              //c.Current_Position_or_Title__c = u.Current_Position_or_Title__c; 
              
        //c.Gender_Research__c=u.Gender__c;
        c.Reviewer_Disease_Condition_Expertise__c=u.Expertise_in_Diseases_Conditions_New__c;
        c.Federal_Employee__c=u.Federal_Employee__c;
        c.Reviewer_Healthcare_Expertise__c=u.Healthcare_Expertise_New__c;
        c.Patient_Communities__c=u.patient_coommunities__c;
        c.Reviewer_Population_Expertise__c=u.Population_Expertise_New__c;
        c.Preferred_Program__c=u.Program_Preference__c;
        c.Specific_Disease_Condition_Expertise__c=u.Disease_Condition_Ranking__c;
        c.Specific_Healthcare_Expertise__c=u.Healthcare_Ranking__c;
        c.Specific_Methodological_Expertise__c=u.Methodologic_Ranking__c;
        c.Specific_Population_Expertise__c=u.Population_Ranking__c;
        c.Reviewer_Role__c=u.Reviewer_Role__c;
        c.Stakeholder_Communities__c=u.StakeholderCommunities_New__c;
        c.Reviewer_Methodological_Expertise__c=u.Methodological_Expertise__c;
        c.Country__c=u.Country__c;
        c.Reviewer_Status__c=text9;
        c.Served_as_Chair_or_Co_Chair__c=u.Served_as_Chair_or_Co_Chair__c;
        c.Highest_Level_Of_Education__c=u.Highest_Level_of_Education__c;
        c.MRA_Application_Submitted__c=true;
        
        database.update(c);
    }
    
     /****Updating name from the CommunitiesmeritReviewerController****/  
        
    /*Merit_Reviewer_Application__c mra= new Merit_Reviewer_Application__c();
    mra=[select reviewer_name__c from Merit_Reviewer_Application__C where id=:u.id];
     mra=[select reviewer_name__c from Merit_Reviewer_Application__C where ownerid=:u.ownerid];
    mra.Reviewer_Name__c= c.id; 
    database.update(mra);*/
}
    
    //This method updates the Reviewer status on Contact Object based on the Application Status of MRA Object
    public void updatecntReviewerstatus(Merit_Reviewer_Application__c crs)
    {
        //Below is the Soql query to get the fields from MRA Object
      
        Merit_Reviewer_Application__c mra=[select Reviewer_Name__c,Application_Status__c,(select comments from processsteps) from Merit_Reviewer_Application__c where id=:crs.Id limit 1];
   
                 String str= '';
         for (ProcessInstanceHistory ps : mra.ProcessSteps)
         {
             if(ps.comments !=null){
                 str = ps.comments;
             }
         }
    //From the Reviewer Name above we get the reviewer status of that Contact Record and assign new Reviewer status value based on condition
      // User cntId= [Select contactId from User where id=:crs.ownerid ];
        Contact c= [select Reviewer_Status__c,Merit_Review_Approval_Comments__c from contact where id=:mra.Reviewer_Name__c limit 1];
  if(crs.Application_Status__c.equals(text1) || crs.Application_Status__c.equals(text2)){
         c.Reviewer_Status__c=text3;
  c.Merit_Review_Approval_Comments__c=str;
            }
        
        else if(crs.Application_Status__c.equals(text4)){
            c.Reviewer_Status__c=text5;
        c.Merit_Review_Approval_Comments__c=str;
            system.debug(c.reviewer_status__c);
        }
        database.update(c);
         }

    //This method updates Reviewer Role on Contact Object which is on MRA Object
    public void updatecntReviewerrole(Merit_Reviewer_Application__c lmra)
    {
        //Below is the Soql query to get the fields from MRA Object
        list<Merit_Reviewer_Application__c> mra=[select Reviewer_Name__c,Reviewer_Role__c from Merit_Reviewer_Application__c where id=:lmra.Id limit 1];
    system.debug(mra);
    Id contactid;
     User cntId= [Select contactId from User where id=:lmra.ownerid ];
   /* for (Merit_Reviewer_Application__c m:mra)
       contactid=m.Reviewer_Name__c;
    system.debug(contactid);*/
        //From the Reviewer Name above we get the reviewer role of the Contact Record and assign Reviewer Role value based on condition
    
     /***Quering the contact records using the user contactid to update role in contact******/
         Contact c;
         if(cntId.contactid != null){
             c = [select id,Reviewer_Role__c from contact where id=:cntId.contactid limit 1];
             system.debug('the reviwer role bug*******************************************************'+c);
     if(lmra.Reviewer_Role__c.equals(text6) ||lmra.Reviewer_Role__c.equals(text7)||lmra.Reviewer_Role__c.equals(text8))
    c.Reviewer_Role__c=lmra.Reviewer_Role__c;
    database.update(c);
         }
        
    }


    }