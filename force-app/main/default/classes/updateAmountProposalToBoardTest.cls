@isTest
    private class updateAmountProposalToBoardTest
    {
        static testMethod void Test1() 
        {
            Id r1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Research Awards').getRecordTypeId();
            
            Id pcoriCommunityPartID = [SELECT Id FROM Profile WHERE name = 'PCORI Community Partner'].Id;
            Id cmaId = [SELECT Id FROM Profile WHERE name = 'CMA Operations' LIMIT 1].Id;

            Account.SObjectType.getDescribe().getRecordTypeInfos();

            Account a = new Account();
            a.Name = 'Acme1';
            insert a;

            Contact con = new Contact(LastName = 'testCon', AccountId = a.Id);
            Contact con1 = new Contact(LastName = 'testCon1', AccountId = a.Id);

            List<Contact> cons = new List<Contact>();
            cons.add(con);
            cons.add(con1);
            insert cons;
            
            User user = new User(Alias = 'test123', Email = 'test123fvb@noemail.com',
                                    Emailencodingkey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',    
                                    LocaleSidKey = 'en_US', ProfileId = pcoriCommunityPartID , Country='United States',IsActive = true,   
                                    ContactId = con.Id, CommunityNickname = 'test12',
                                    TimeZoneSidKey='America/New_York', UserName = 'testerfvb1@noemail.com');

            User user1 = new User(Alias = 'test1234', Email = 'test1234fvb@noemail.com',
                                    Emailencodingkey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',    
                                    LocaleSidKey = 'en_US', ProfileId = pcoriCommunityPartID , Country='United States',IsActive = true,   
                                    ContactId = con1.Id, CommunityNickname = 'test12345',
                                    TimeZoneSidKey='America/New_York', UserName = 'testerfvb123@noemail.com');

            List<User> users = new List<User>();
            users.add(user);
            users.add(user1);
            insert users;
            
            Cycle__c c = new Cycle__c();
            c.Name = 'testcycle';
            c.COI_Due_Date__c = date.valueof(System.now());
            insert c;

            Campaign camp = new Campaign();
            camp.Name = System.Label.Symptom_Management_for_Patients_with_Advanced_Illness;
            camp.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('PCORI Portal').getRecordTypeId();
            camp.Cycle__c = c.Id;
            camp.IsActive = true;
            camp.FGM_Portal__Visibility__c = 'Public';
            camp.Status = 'In Progress';
            camp.StartDate = System.today();
            camp.EndDate = System.today() + 7;
            camp.FGM_Base__Board_Meeting_Date__c = System.today() + 14;

            insert camp;
            
            opportunity o1 = new opportunity();
            o1.Awardee_Institution_Organization__c=a.Id;
            o1.CampaignId = camp.Id;
            o1.RecordTypeId = r1;
            o1.Full_Project_Title__c='test';
            o1.Project_Name_off_layout__c='test';
            o1.CloseDate = Date.today() + 14;
            o1.Application_Number__c='Ra-1234';
            o1.StageName = 'Application Awarded';
            o1.External_Status__c = '';
            o1.Name='test';
            o1.PI_Name_User__c = user.id;
            o1.AO_Name_User__c = user1.id;
            o1.Project_Start_Date__c = system.Today();
            o1.Bypass_Flow__c = true;

            opportunity o2 = new opportunity();
            o2.Awardee_Institution_Organization__c=a.Id;
            o2.CampaignId = camp.Id;
            o2.RecordTypeId = r1;
            o2.Full_Project_Title__c='test';
            o2.Project_Name_off_layout__c='test';
            o2.CloseDate = Date.today() + 14;
            o2.Application_Number__c='Ra-1234';
            o2.StageName = 'Application Awarded';
            o2.External_Status__c = '';
            o2.Name='test';
            o2.PI_Name_User__c = user.id;
            o2.AO_Name_User__c = user1.id;
            o2.Project_Start_Date__c = system.Today();
            o2.Bypass_Flow__c = true;

            opportunity o3 = new opportunity();
            o3.Awardee_Institution_Organization__c=a.Id;
            o3.CampaignId = camp.Id;
            o3.RecordTypeId = r1;
            o3.Full_Project_Title__c='test';
            o3.Project_Name_off_layout__c='test';
            o3.CloseDate = Date.today() + 14;
            o3.Application_Number__c='Ra-1234';
            o3.StageName = 'Application Awarded';
            o3.External_Status__c = '';
            o3.Name='test';
            o3.PI_Name_User__c = user.id;
            o3.AO_Name_User__c = user1.id;
            o3.Project_Start_Date__c = system.Today();
            o3.Bypass_Flow__c = true;
            
            List<Opportunity> oppList = new List<Opportunity>();
            oppList.add(o1);
            oppList.add(o2);
            oppList.add(o3);
            insert oppList;

            Test.startTest();
            
            List<Budget__c> listOfBudgets = new List<Budget__c>();

            Budget__c b1 = new Budget__c();
            b1.Associated_App_Project__c = o1.id;
            b1.Total_Prime_Indirect_Costs_Year_1__c = 1000;
            b1.Applicable_Rate_Year_1__c = 0;
            listOfBudgets.add(b1);
            
            Budget__c b2 = new Budget__c();
            b2.Associated_App_Project__c = o1.id;
            b2.Total_Prime_Indirect_Costs_Year_1__c = 1400;
            listOfBudgets.add(b2);
                        
            Budget__c b3 = new Budget__c();
            b3.Associated_App_Project__c = o1.id;
            b3.Total_Prime_Indirect_Costs_Year_1__c = 1300;
            listOfBudgets.add(b3);

            insert listOfBudgets;

            //update and assert final grand total is set up correctly
            b1.recalculateFormulas();
            b2.recalculateFormulas();
            b3.recalculateFormulas();
            System.assertEquals(1000,b1.Final_Grand_Total__c);
            System.assertEquals(1400,b2.Final_Grand_Total__c);
            System.assertEquals(1300,b3.Final_Grand_Total__c);
            
            b1.Budget_Status__c = 'Application Budget';
            b2.Budget_Status__c = 'PIR Approved Budget';
            b3.Budget_Status__c = 'Approved for Contract';

            update listOfBudgets;

            //refresh o1
            o1 = [SELECT Id, Application_Amount__c, Amount_Proposed_to_Board__c, Contracted_Budget__c, Contract_Execution_Amount_Current__c FROM Opportunity WHERE Id = : o1.id];

            System.assertEquals(1000,o1.Application_Amount__c);
            System.assertEquals(1400,o1.Amount_Proposed_to_Board__c);
            System.assertEquals(1300,o1.Contracted_Budget__c);
            System.assertEquals(1300,o1.Contract_Execution_Amount_Current__c);

            listOfBudgets.clear();

            b3.Budget_Status__c = 'Executed';
            listOfBudgets.add(b3);

            o3.Contract_Execution_Amount_Current__c = null;

            Budget__c b7 = new Budget__c();
            b7.Associated_App_Project__c= o3.id;
            b7.Budget_Status__c= 'Approved for Supplemental Funding';
            b7.Supplemental_Funding_Flag__c = true;
            b7.Total_Prime_Indirect_Costs_Year_1__c = 1000;
            listOfBudgets.add(b7);

            Budget__c b8 = new Budget__c();
            b8.Associated_App_Project__c= o1.id;
            b8.Budget_Status__c = 'Approved for Supplemental Funding';
            b8.Supplemental_Funding_Flag__c = true;
            b8.Total_Prime_Indirect_Costs_Year_1__c = 200;
            listOfBudgets.add(b8);

            //update and assert final grand total is set up correctly
            b7.recalculateFormulas();
            b8.recalculateFormulas();
            System.assertEquals(1000,b7.Final_Grand_Total__c);
            System.assertEquals(200,b8.Final_Grand_Total__c);

            upsert listOfBudgets;

            //refresh o1
            o1 = [SELECT Id, Application_Amount__c, Amount_Proposed_to_Board__c, Contracted_Budget__c, Contract_Execution_Amount_Current__c FROM Opportunity WHERE Id = : o1.id];

            System.assertEquals(1500,o1.Contract_Execution_Amount_Current__c);

            Budget__c b4 = new Budget__c();
            b4.Associated_App_Project__c= o1.id;
            b4.Total_Prime_Indirect_Costs_Year_1__c = 2000;
            
            b4.recalculateFormulas();
            System.assertEquals(2000,b4.Final_Grand_Total__c);

            insert b4;

            listOfBudgets.clear();

            b3.Budget_Status__c = 'Archived';
            listOfBudgets.add(b3);
            b4.Budget_Status__c = 'Approved for Contract';
            listOfBudgets.add(b4);

            update listOfBudgets;

            //refresh o1
            o1 = [SELECT Id, Application_Amount__c, Amount_Proposed_to_Board__c, Contracted_Budget__c, Contract_Execution_Amount_Current__c FROM Opportunity WHERE Id = : o1.id];

            System.assertEquals(2200,o1.Contract_Execution_Amount_Current__c);

            //test bypass flow
            Budget__c b6 = new Budget__c();
            b6.Associated_App_Project__c= o2.id;
            b6.Bypass_Flow__c = true;

            insert b6;

            Test.stopTest();

        }
    }