/**
* @author        Abhinav Polsani 
* @date          01/21/2017
* @description   This test class targets P2PFormsController
Modification Log:
-----------------------------------------------------------------------------------------------------------------
Developer                Date            Description
-----------------------------------------------------------------------------------------------------------------
Abhinav Polsani       01/21/2017      Initial Version
*/
@isTest
private class P2PFormsControllerTest {
	
	public static testMethod void testDataCreation() {
		Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Pipeline to Proposal').getRecordTypeId();
		Opportunity opp = new Opportunity(Name = 'TestName', Closedate = System.today(), RecordTypeId = recordTypeId, StageName = 'In Progress',
			                              Proceed_To_Tier_3__c = true);
		insert opp;
		PipelineToProposal__c ptop = new PipelineToProposal__c(Projects__c = opp.Id, FieldSetName__c = 'Tier I Final Report', Name = 'TestName');
		insert ptop;
		PipelineToProposal__c ptop1 = new PipelineToProposal__c(Projects__c = opp.Id, FieldSetName__c = 'Tier II Final Report', Name = 'TestName1');
		insert ptop1;
		PipelineToProposal__c ptop2 = new PipelineToProposal__c(Projects__c = opp.Id, FieldSetName__c = 'Tier III Final Report', Name = 'TestName1');
		P2PSequentialitySettings__c pSettings = new P2PSequentialitySettings__c(OrderNumber__c = 1, FormName__c = 'Tier I Final Report',
			                                        	FieldSetName__c = 'Tier_I_Final_Report', Name = 'Tier I Final Report');
		insert pSettings;
		//Create List Custom Settings
		ApexPages.StandardController sc = new ApexPages.StandardController(ptop);
		P2PFormsController p2p = new P2PFormsController(sc);
		p2p.genP2P = ptop2;
		p2p.fieldSetName = 'Tier III Final Report';
		p2p.directToDetail();
		List<ApexPages.Message> msgList = ApexPages.getMessages();
		for (ApexPages.Message msg :  ApexPages.getMessages()) {
    		System.assertEquals('You have completed all Tier III Reports! Please Click back in browser to return to Project', msg.getSummary());
    		System.assertEquals(ApexPages.Severity.INFO, msg.getSeverity()); 
		}
		p2p.savep2p();
		Map<String, P2PSequentialitySettings__c> settingsMap = P2PFormsController.getCustomSettingsByTierName();
		//p2p.reloadWindow();
		p2p.CancelPage();
		p2p.editP2p();
		ptop.isLocked__c = true;
		update ptop;
		ptop1.isLocked__c = true;
		update ptop1;
		p2p.pdfView();
	}	
}