public class redirectReportController {
    /* controller and VF page made by Matt Hoffman
    This controller class handles the redirecting of a report when it is being viewed. Progress reports have different VF pages for each type, and since you can only override 
    the standard view page to one VF page, we make a page (RedirectReportTypeLandingPage) with this controller so that it can then auto-navigate to the appropriate VF page 
    based on the report's record type.
    This is accomplished by having an empty VF page except for 1 action function that calls the PageReference function to redirect to the correct VF page, and 1 javascript 
    function that is called when the page loads that calls the action function.
    This class works by trying to find the progress report id that is passed in the URL and querying for it. If the progress report is of a type we dont have a page for or
    some sort of error happens along the way, the controller will send the user to the opportunity standard detail page as a last resort.   
    */
    
    ////////// BEGIN VARIABLE DECLARATION ////////////
    
    // the progress report that is being looked at 
    public FGM_Base__Grantee_Report__c gRecordInserts {get;set;}   
    
    // booleans for each valid report record type
    public boolean isInterimRA {get;set;}   
    public boolean isInterimEA {get;set;}    
    public boolean isFinalRA {get;set;}   
    public boolean isFinalEA {get;set;}
    public boolean isNonStandardInterim {get; set;}
    public boolean isNonStandardFinal {get; set;}
    
    // stores the user's profile name
    public String userProf {get;set;}
   
    // stores the portal profile name
    public String portalProfileName;

    
    ////////// END VARIABLE DECLARATION ///////////
    
    ////////// BEGIN CONSTRUCTORS //////////////
    
    public redirectReportController(ApexPages.StandardController controller) {
        callCommonFunctions();
    }
    
    public redirectReportController() {
        callCommonFunctions();
    }
    
    // constructor used for test classes
    public redirectReportController(String repId) {
        
    }
    
    
    //////////// END CONSTRUCTORS //////////////
    
    
    // "constructor function"
    public void callCommonFunctions() {
        
        // initialize all booleans to false
        setBooleansFalse ();
        
        // pull id variable from url parameter
        String RepId = ApexPages.currentPage().getParameters().get('id');                     
            
        // query for the progress report's information using the parameter
        List<FGM_Base__Grantee_Report__c> GranteeReportList = [SELECT FGM_Base__Request__c, FGM_Base__Request__r.Networktype__c, recordTypeId, id, RecordTypeName__c, FGM_Base__Request__r.Program_R2__c FROM FGM_Base__Grantee_Report__c WHERE Id = :RepId];              
        
        // get the report's information if the list is not empty
        if (!GranteeReportList.isEmpty()) {
            gRecordInserts = GranteeReportList[0];
            
            // function that matches the found progress report's type and sets the correspond boolean to true
            setReportTypeBoolean();
        }
        
        // get the user's profile name
        List<Profile> uprof = [SELECT name from Profile WHERE Id =: UserInfo.getProfileId().substring(0,15)];      
        if (!uprof.isEmpty()) {
            userProf = uprof[0].name;
        }
        
        // set the portal profile name
        portalProfileName = 'PCORI Community Partner';
    }
    
    
    // sets all booleans to false
    public void setBooleansFalse () {
        isInterimRA = isInterimEA = isFinalRA = isFinalEA = isNonStandardInterim = isNonStandardFinal =  false;
    }
    
    
    // tries to match the report's record type to the known record types and sets the corresponding boolean to true if found
    public void setReportTypeBoolean () {
        if (gRecordInserts.RecordTypeName__c == 'Interim_Progress_Report_RA' && gRecordInserts.FGM_Base__Request__r.Networktype__c == 'Non Standard') {
           isNonStandardInterim = true;
        } else if (gRecordInserts.RecordTypeName__c == 'Final_Progress_Report_RA' && gRecordInserts.FGM_Base__Request__r.Networktype__c == 'Non Standard') {
           isNonStandardFinal = true;
        } else if (gRecordInserts.RecordTypeName__c == 'Final_Progress_Report_EA') {
            isFinalEA = true;
        }
        else if (gRecordInserts.RecordTypeName__c == 'Final_Progress_Report_RA') {
            isFinalRA = true;
        }
        else if (gRecordInserts.RecordTypeName__c == 'Interim_Progress_Report_RA') {
            isInterimRA = true;
        }
        else if (gRecordInserts.RecordTypeName__c == 'Interim_Progress_Report_EA') {
            isInterimEA = true;
        }
    }
    
    
    // called by the action function to actually send to the correct page. sends to the opportunity/project standard detail page if none are found
    public PageReference sendToPage () {
        String r;                    
            //interim RA
            if (isInterimRA) {
                if(userProf == portalProfileName) {
                    r = '/InterimProgressReportOutputRA?id='+gRecordInserts.Id;
                }
                else {
                    r = '/apex/InterimProgressReportOutputRA?id='+gRecordInserts.Id;
                }
            }
            
            // interim EA
            if (isInterimEA) {
                if(userProf == portalProfileName) {
                    r = '/InterimProgressReportOutputEA?id='+gRecordInserts.Id;
                }
                else {
                    r = '/apex/InterimProgressReportOutputEA?id='+gRecordInserts.Id;
                }
            }
            
             // final RA
            if (isFinalRA) {
                if(userProf == portalProfileName) {
                    if(gRecordInserts.FGM_Base__Request__r.Program_R2__c == 'Dissemination and Implementation'){
                        r = '/FinalProgressReportOutputDIRA?id='+gRecordInserts.Id;
                    }else{
                        r = '/FinalProgressReportOutputRA?id='+gRecordInserts.Id;
                    }
                }
                else {
                    if(gRecordInserts.FGM_Base__Request__r.Program_R2__c == 'Dissemination and Implementation'){
                        r = '/apex/FinalProgressReportOutputDIRA?id='+gRecordInserts.Id;
                    }else{
                        r = '/apex/FinalProgressReportOutputRA?id='+gRecordInserts.Id;
                    }
                }
            }
            
            // final EA
            if (isFinalEA) {
                if(userProf == portalProfileName) {
                    r = '/FinalProgressReportOutputEA?id='+gRecordInserts.Id;
                }
                else {
                    r = '/apex/FinalProgressReportOutputEA?id='+gRecordInserts.Id;
                }
            } 

            if (isNonStandardFinal) {
                if(userProf == portalProfileName) {
                    r = '/FinalProgressReportOutput_NonStandard?id='+gRecordInserts.Id;
                }
                else {
                    r = '/apex/FinalProgressReportOutput_NonStandard?id='+gRecordInserts.Id;
                }
            } 

            if (isNonStandardInterim) {
                if(userProf == portalProfileName) {
                    r = '/InterimProgressReportOutput_NonStandard?id='+gRecordInserts.Id;
                }
                else {
                    r = '/apex/InterimProgressReportOutput_NonStandard?id='+gRecordInserts.Id;
                }
            }

        
        // failsafe. if r is not yet set, send to opportunity standard detail page
        if (r == null) {
            r = '/006/o';
        }
        
        PageReference pr = new PageReference(r);
        pr.setRedirect(true);
        return pr;
    }    
}