/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestPCORITopicPages {

    static testMethod void testTopicPage() {
    	
        TopicSearchNewControllerExtension c = new TopicSearchNewControllerExtension();       
         
        c.topicVals.Disease_Condition_1__c = 'Cancer';
        c.topicVals.Disease_Condition_Subtopic_1__c = 'Breast Cancer';
        
        c.getTopicSearchResults();
        c.clearFields();
        
        c.topicVals.Health_Topic__c = 'Care Coordination';
        c.getTopicSearchResults();
        c.clearFields();
        
        c.populationSelected = 'Low-income Groups';
        c.getTopicSearchResults();
        c.clearFields();        
        
        c.stakeholderSelected = 'Clinician/Patient Communication';    
        c.getTopicSearchResults();
        c.clearFields();       
        
        c.topicVals.Keywords__c = 'Communication';
        c.getTopicSearchResults();
        c.clearFields();      
        
        c.topicVals.External_Status__c =  'SOC Approved';
        c.getTopicSearchResults();
        c.clearFields(); 
        
        c.topicVals.Reason__c = '1001';
        c.getTopicSearchResults();
        c.clearFields();        
        
        List<SelectOption> loption1 = c.getPriorityPopulation();
 		List<SelectOption> loption2 = c.getStakeholdersGroup();               
    }
    
    static testMethod void testTopicSummaryForm() {

        
  		Topic__c t = new Topic__c();
     
        Apexpages.StandardController controller = new Apexpages.StandardController(t);
        TopicSummaryController p = new TopicSummaryController(controller);   
        
        p.clearFields();
        
        p.diseaseSelected = 'Pain';
        p.otherDisease = 'Heartbreak';
        
        p.topic.Submission_Page_Research_Question__c = 'Submission_Page_Research_Question__c';
        
        p.topic.Submission_Page_Question__c = 'Submission_Page_Question__c';
        
        p.populationSelected.add('Women');
        p.otherPopulation = 'otherPopulation ';
        
        p.StudyDesignSelected.add('Prospective cohort (Observational)');
        p.otherStudyDesign = 'otherStudyDesign';
        
        p.stakeholderSelected = 'Patients/Consumers';
        
        p.email1 = 'sm@gmail.com';
        p.topic.Submission_Page_Email__c = 'sm@gmail.com';
        
        PageReference page = p.submit();
        
		List<SelectOption> po1 = p.getPriorityPopulation();     
		List<SelectOption> po2 = p.getStakeholdersGroup();  		
		List<SelectOption> po3 = p.getDiseaseCondition();		   
 		List<SelectOption> po4 = p.getStudyDesign();       
 		List<SelectOption> po5 = p.getContacted();    
 		Boolean b = TopicSummaryController.validateEmail('sm@gmail.com');
 		
 		    
    }    
    
    
}