/*
    Author     : Vijaya Kumar Sripathi
    Date       : 12th Dec 2017
    Name       : PublicationsEmailsUpdate 
    Description: This class will update Email Fields in the Publications object when the Publication is edited or created.
Last Modified by : Kalyan Vuyyuru
Last Modified description: Handled the exception in case if publication updated without project to not throw null pointer exception.
*/
Public without Sharing class PublicationsEmailsUpdate
{
    //Constructor
    Public PublicationsEmailsUpdate(){
    }

   Public static void PublicationNameEmailUpdate(List<Publication__c> lstPublications)
   { /* This method will update name fields and email fields in Publication object*/
   
        Set <Id> parentIds = new set<Id>();
        for(Publication__c objPub : lstPublications) {
            if(objPub.Project_Publication_del__c!=null){
            parentIds.add(objPub.Project_Publication_del__c);
                }
        }
          
       if(!parentIds.isempty()){
        List<Opportunity> oppSOQLList =[SELECT Id, Name, PI_Name_User__c,PI_Name_User__r.email, PI_Project_Lead_2_Name_New__r.Email, PI_Project_Lead_Designee_1_New__r.Email, PI_Project_Lead_Designee_2_Name_New__r.Email,Program_Associate__r.Email,Program_Officer__r.Email,AO_Name_User__r.Email,Contract_Administrator__r.Email,Contract_Coordinator__r.Email FROM Opportunity where id IN : parentIds Limit 50000 ]; 
    
        List<Publication__c> lstPubSOQLList =[SELECT Id, PI_Lead_Name__c, PI_Email__c, AO_Name__c , AO_Email__c, Program_Associate_Name__c , Program_Associate_Email__c, Program_Officer_Name__c , Program_Officer_Email__c FROM Publication__c WHERE Project_Publication_del__c IN : parentIds Limit 50000];
           
        Map<Id, Publication__c> mapPublications = new Map<Id, Publication__c>();
        Map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>();
        Set <Id> UserIds = new set<Id>();    
    
       for(Opportunity opp: oppSOQLList)
       {
           mapOpp.put(opp.id, opp);
           UserIds.add(mapOpp.get(opp.id).PI_Name_User__c);
           UserIds.add(mapOpp.get(opp.id).AO_Name_User__c);
           UserIds.add(mapOpp.get(opp.id).Program_Associate__c);
           UserIds.add(mapOpp.get(opp.id).Program_Officer__c);
       }//end For loop
           
       List<User> lstUsers = [SELECT Id, Name, FirstName, LastName FROM User WHERE Id IN : UserIds Limit 50000];
       Map<Id, User> mapUser = new Map<Id, User>();
           
       for(User usr: lstUsers)
       {
           mapUser.put(usr.id, usr);       
       }//end For loop    
            	
		for(Publication__c objPub:lstPublications)
        {
            if (mapUser.get(mapOpp.get(objPub.Project_Publication_del__c).PI_Name_User__c) != null)            
            	objPub.PI_Lead_Name__c = mapUser.get(mapOpp.get(objPub.Project_Publication_del__c).PI_Name_User__c).FirstName + ' ' + mapUser.get(mapOpp.get(objPub.Project_Publication_del__c).PI_Name_User__c).LastName;
            if (mapOpp.get(objPub.Project_Publication_del__c).PI_Name_User__r.email != null)
                objPub.PI_Email__c = mapOpp.get(objPub.Project_Publication_del__c).PI_Name_User__r.email;
            if (mapUser.get(mapOpp.get(objPub.Project_Publication_del__c).AO_Name_User__c) != null)
                objPub.AO_Name__c = mapUser.get(mapOpp.get(objPub.Project_Publication_del__c).AO_Name_User__c).FirstName + ' ' + mapUser.get(mapOpp.get(objPub.Project_Publication_del__c).AO_Name_User__c).LastName;                
            if (mapOpp.get(objPub.Project_Publication_del__c).AO_Name_User__r.Email != null)
                objPub.AO_Email__c = mapOpp.get(objPub.Project_Publication_del__c).AO_Name_User__r.Email;
            if (mapUser.get(mapOpp.get(objPub.Project_Publication_del__c).Program_Associate__c) != null)
                objPub.Program_Associate_Name__c = mapUser.get(mapOpp.get(objPub.Project_Publication_del__c).Program_Associate__c).FirstName + ' ' + mapUser.get(mapOpp.get(objPub.Project_Publication_del__c).Program_Associate__c).LastName;                
            if (mapOpp.get(objPub.Project_Publication_del__c).Program_Associate__r.Email != null)
                objPub.Program_Associate_Email__c = mapOpp.get(objPub.Project_Publication_del__c).Program_Associate__r.Email;
            if (mapUser.get(mapOpp.get(objPub.Project_Publication_del__c).Program_Officer__c) != null)
                objPub.Program_Officer_Name__c = mapUser.get(mapOpp.get(objPub.Project_Publication_del__c).Program_Officer__c).FirstName + ' ' + mapUser.get(mapOpp.get(objPub.Project_Publication_del__c).Program_Officer__c).LastName;                
            if (mapOpp.get(objPub.Project_Publication_del__c).Program_Officer__r.Email != null)
                objPub.Program_Officer_Email__c = mapOpp.get(objPub.Project_Publication_del__c).Program_Officer__r.Email;                      
		}//end for loop
       }//end of if loop  
    }// end PublicationNameEmailUpdate
    
}// end PublicationsEmailsUpdate