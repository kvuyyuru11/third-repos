public without sharing class FCcustomLink{

    public Boolean showpageblock { get; set; }

    public String usrProfileName;
    public List<PermissionSet> permissionsetname;
    public boolean showlink{get;set;}
    public boolean showlink1{get;set;}
    string text3=Label.New_Reviewer;
    
    Public FCcustomLink(){
    
    showlink1=true;
    showlink=false;
    showpageblock=false;
    Id myID3 = userinfo.getUserId();
    user u=[select contactid from user where id=:myID3];
   
    list<contact> c= [select Reviewer_Status__c from Contact where id=:u.ContactId];
    
    if(c.size()>0){
    
        for(contact con: c ){
            system.debug('convalue+++'+con);
            system.debug('convalue Reviewer_Status__c+++'+con.Reviewer_Status__c);
            
             if(con.Reviewer_Status__c!=null && con.Reviewer_Status__c!='' && con.Reviewer_Status__c.equals(text3)){
             showlink=true;
             showlink1=false;
             }
    
         }
    }
    
    
   }
   
    public PageReference target1() {
         string url1 ='/Communitiescustomlogin';
       pagereference p=new pagereference(url1);
        return p;
        
    }

    
     public PageReference target2() {
         string url2='https://www.pcori.org/funding-opportunities/merit-review-process/general-merit-reviewer-resources';
       pagereference p1=new pagereference(url2);
       
        return p1;
    }
      public PageReference target() {
         string url3='http://www.pcori.org/get-involved/review-funding-applications/become-reviewer';
       pagereference p=new pagereference(url3);
        return p;
        
    }
    public PageReference target3() {
         string url4='http://www.pcori.org/get-involved/review-funding-applications';
       pagereference p=new pagereference(url4);
        return p;
    }
    
      public PageReference target4() {
         string url4='http://www.pcori.org/get-involved/review-funding-applications/reviewer-resources';
       pagereference p=new pagereference(url4);
        return p;
    }
      public PageReference target5() {
         string url4='http://www.pcori.org/get-involved/review-funding-applications/reviewer-resources/reviewer-faqs';
       pagereference p=new pagereference(url4);
        return p;
    }
      public PageReference target6() {
         string url4='http://www.pcori.org/get-involved/review-funding-applications/reviewer-resources/meet-our-reviewers';
       pagereference p=new pagereference(url4);
        return p;
    }
 }