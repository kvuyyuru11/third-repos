/*
Application Inventory: AI-0856
Requirement Id: 16906
Code updated with custom labels for hardcoded values on  03/06/2017 by Elizabeth Bryant
*/
public with sharing class FilteredAudit{
    @TestVisible private Opportunity opp;
    public List<Audit__c> auditlst {get;set;}
    public FilteredAudit(ApexPages.StandardController ctr){
        this.opp= (Opportunity)ctr.getRecord();
        if(opp.Id != null){
            auditlst = [SELECT Id, Name, RecordType.Name, Audit_Type__c, Status__c, Initiating_Department__c FROM Audit__c 
            WHERE Project__c = :opp.Id  AND RecordType.Name =: Label.Site_Visit AND (Status__c =: Label.Sent_to_Awardee OR Status__c =: Label.Submitted OR Status__c =: Label.Finalized)];
            if(auditlst.size() == 0){
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No audits to display'));
                //AI-1233 | RAr3_C3_011 | 17190
                //Updated code to return a null list if the list is empty so that it will not generate a page on the page layout. 
            	auditlst = null;
            }
            
        }
    }
}