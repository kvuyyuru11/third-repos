/*
    06/18/2014 ZT - Adapted from Eugene Vabishchevich's article (http://cloudcatamaran.com/2013/11/customer-community-customization-part-1/)
    08/18/2017 Eric Viveiros - Accenture Federal - Added startURL to allow redirecting from external links
*/

global class CommunitiesCustomLoginController {
    global String username {get; set;}
    global String password {get; set;}
    public string baseURL{get; set;}
    public string redirectPage{get; set;}
    
    global CommunitiesCustomLoginController () {
        baseUrl=apexpages.currentpage().geturl();
        system.debug('****'+baseUrl);
        redirectPage = System.currentPageReference().getParameters().get('startURL');
    
    }
    
    global PageReference login() {
        if(baseUrl.contains(Label.COIBoardofDirectors)){
            redirectPage= Label.COIBODpage; 
        }
        return Site.login(username, password, redirectPage); 
    } 
}