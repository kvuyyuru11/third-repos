global class batchUpdateUserNameforMRonContact implements Database.Batchable<sObject> {
    global String query='';
    public batchUpdateUserNameforMRonContact(String query){
        this.query=query;
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    Map<Id,string> contactidusernamemap = new  Map<Id,string>();
    List<Contact> contactlisttoupdate= new List<Contact>();
    
    global void execute(Database.BatchableContext BC, List<User> scope) {
        for(User u : scope)
            
        {
            contactidusernamemap.put(u.contactId, u.username); 
            
        }
        list<Contact> cntlist=[select Id,User_Name_for_MR__c from Contact where Id IN:contactidusernamemap.keySet()];
        for(Contact c:cntlist){
            c.User_Name_for_MR__c=contactidusernamemap.get(c.Id);
            contactlisttoupdate.add(c);
            
        }
        database.update(contactlisttoupdate);
    }   
    
    global void finish(Database.BatchableContext BC) {
    }
    
    
    
}