public class CreateReviewRecords {
    public static void FluxxLOIReviews (Set<Id> LOIs) {
        List<Fluxx_LOI_Review_Form__c> reviewForms = new List<Fluxx_LOI_Review_Form__c>();

        //Select Id, Fluxx_Request_Id__c, PFA__c, Principal_Investigator__c, Project_Title__c From Fluxx_LOI__c where id in :LOIs]        
        for ( Fluxx_LOI__c[] flois: [Select Id, PFA_Type__c, PFA__c From Fluxx_LOI__c where id in :LOIs] ) {     
            for (Fluxx_LOI__c loi: flois)   {       
                integer i = 1;
                string rev ;
                string recTypeId;               //Review record type id
                
                rev = 'Primary' ;       //Reviewer label
                
                //Set up review record type id based on the PFA Type.
                    if (loi.PFA_Type__c == 'Broad' || loi.PFA_Type__c == '') {
                        recTypeId = Label.Fluxx_LOI_Reviews_Broads;
                    } else {
                        if (loi.PFA_Type__c == 'Pragmatic') {
                            recTypeId = Label.Fluxx_LOI_Reviews_Pragmatic_Studies;
                        } else {
                            if (loi.PFA_Type__c == 'Targeted') {
								if (loi.PFA__c == 'APDTO') {
                                	recTypeId = Label.Fluxx_LOI_Reviews_Targeted;                                
                            	} else {
                                	if (loi.PFA__c == 'CDR') {
                                            recTypeId = Label.Fluxx_LOI_Reviews_CDR_Targeted;
                                    }  else {
                                        if (loi.PFA__c == 'AD') {
                                            recTypeId = Label.Fluxx_LOI_Reviews_AD_Targeted;
                                        } else {
                                            if (loi.PFA__c == 'IHS') {
                                                recTypeId = Label.Fluxx_LOI_Reviews_IHS_Targeted;
                                            }  
                                        } //end  (loi.PFA__c == 'AD')                              
                                    } //end (loi.PFA__c == 'CDR')                                                              
                         		} //end (loi.PFA__c == 'APDTO')                    
                            } //end (loi.PFA_Type__c == 'Targeted')                        
                        } // end (loi.PFA_Type__c == 'Pragmatic')
                    } //end (loi.PFA_Type__c == 'Broad' || loi.PFA_Type__c == '')
                
                do {    //create 2 review record for each LOI in the list returned
                    if (i==2) {
                      rev = 'Secondary' ;
                    }
                    Fluxx_LOI_Review_Form__c review = new Fluxx_LOI_Review_Form__c( Salesforce_LOI_Number__c = loi.Id,                                                                                    
                                                                                    Reviewer_Label__c = rev,
                                                                                   RecordTypeId = recTypeId
                                                                                  );    
                    reviewForms.add(review);
                    i++;
                } while (i < 3);                
             }  //inner for loop           
        } //end for loop
        system.debug('******* New review records: ' + json.serializePretty(reviewForms));
        try {
            insert reviewForms;
        } catch (DmlException e) {
            system.debug('******* An error occured while saving to database: ' + e);
        }
    } //end FluxxLOIReviews method

}