/**
	* newEventControllerTest - <description>
	* Created by BrainEngine Cloud Studio
	* @author: brian Matthews
	* @version: 1.0
*/

@isTest
private class newEventControllerTest {
	static testMethod void myUnitTest() {
		
	 	PageReference pageRef = Page.EventWizardStep1;
	  	Test.setCurrentPageReference(pageRef);
		
		newEventController cls = new newEventController();
		cls.getEvent();
		cls.step1();
		cls.step2();
		cls.step3();
		cls.step4();
		cls.cancel();
		cls.save();
	  	
	}
}