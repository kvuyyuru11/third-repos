public with sharing class SFProjectsList {
   Public Opportunity opp;
    
    public List<Opportunity> sfprojectsList {get;set;}
    public SFProjectsList(ApexPages.StandardController ctr){
        this.opp= (Opportunity)ctr.getRecord();
        if(opp.Id != null){
      //Query for record matching criteria.
            sfprojectsList = [SELECT Id, Name, RecordTypeId,StageName,Contract_Number__c,Program__c,PI_Name_User__c ,Parent_Contract__c,Project_Start_Date__c, CloseDate
                                
                                FROM Opportunity
                               WHERE Parent_Contract__c  = :opp.Id  AND(
                                  StageName=: Label.Programmatic_Review OR
                                  StageName=: Label.Application_Declined OR
                                  StageName=: Label.Supplemental_Funding_Awarded
                                  
                                   
                                   
                               
                               )];
        }
        if(sfprojectsList.size() == 0){
            //Updated code to return a null list if the list is empty so that it will not generate a page on the page layout. 
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No remediations to display'));
            sfprojectsList = null;
        }
    }
}