public without sharing class BypassFlow {

	@InvocableMethod
	public static void immediateAction(List<BypassFlowInput> inputs) {
		if(inputs.isEmpty()) {
			return;
		}
		for(BypassFlowInput input: inputs) {
			if(input.debugMsg == null) {
				continue;
			}
			System.debug(input.debugMsg);
		}
	}

	public class BypassFlowInput {
		@InvocableVariable
		public String debugMsg;
	}
}