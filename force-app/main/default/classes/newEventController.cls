/**
	* newEventController  - <description>
	* @author: brian Matthews
	* @version: 1.0
*/

public class newEventController {

	Event__c event;
	private String rowId = '';
	public Event__c getEvent() {
      if(event == null) event = new Event__c();
      return event;
   }
	
	public PageReference step1() {
      return Page.EventWizardStep1;
   }

   public PageReference step2() {
      return Page.EventWizardStep2;
   }

   public PageReference step3() {
      return Page.EventWizardStep3;
   }
   
   public PageReference step4() {
      return Page.EventWizardStep4;
   }
   
   // This method cancels the wizard, and returns the user to the 
    public PageReference cancel() {
		PageReference eventPage = new ApexPages.StandardController(event).view();
		eventPage.setRedirect(true);
		return eventPage; 
    }
	
	public PageReference save() {

		Database.insert(event);
		rowId = event.Id;
		//send them to the confirmation page
		PageReference eventPage = Page.EventWizardConfirmation;
		//eventPage.setRedirect(true);
		return eventPage;

      	//PageReference eventPage = new ApexPages.StandardController(event).view();
      	//eventPage.setRedirect(true);
      	//return eventPage;
		
   }
	
	public String eventUrl {
    	get  { return rowId;  }
    	set;
  	}

}