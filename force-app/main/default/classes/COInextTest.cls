@isTest
public class  COInextTest{
    static testMethod void testCOInextTestMethod(){
        List<User> usrlist=TestDataFactory.getpartnerUserTestRecords(1,2);
        Id recordTypeId = Schema.SObjectType.COI_Expertise__c.getRecordTypeInfosByName().get('General Record Type').getRecordTypeId();         
        COI_Expertise__c ce =new COI_Expertise__c();
        ce.RecordTypeId=recordTypeId;
        ce.Reviewer_Name__c=usrlist[0].contactId;
        ce.Active__c=true;
        ce.OwnerId=usrlist[0].Id;
        
        insert ce;
        //COI_Expertise__c ce = [select id from COI_Expertise__c limit 1];
        COInext cn = new COInext(new ApexPages.StandardController(ce));
        Test.startTest();
        cn.recId = ce.id;
        cn.getFundingApplications();
        cn.getlistOfObject();
        Test.StopTest();
    }
    
    
}