@isTest
private class BudgetPageController_Test {
	
	@isTest static void testExternalBudgetForApplicationPhase() {
		Id r1 =Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RA Applications').getRecordTypeId();
		Id rKeyPersonnel =Schema.SObjectType.Key_Personnel_Costs__c.getRecordTypeInfosByName().get('Key Personnel').getRecordTypeId();
		Id rConsultantCost =Schema.SObjectType.Key_Personnel_Costs__c.getRecordTypeInfosByName().get('Consultant Cost').getRecordTypeId();
		Id rSubcontractorDirect =Schema.SObjectType.Key_Personnel_Costs__c.getRecordTypeInfosByName().get('Subcontractor Direct').getRecordTypeId();
		
		Id pcoriCommunityPartID = [SELECT Id FROM Profile WHERE name = 'PCORI Community Partner'].Id;
		Id cmaId = [SELECT Id FROM Profile WHERE name = 'CMA Operations' LIMIT 1].Id;

		Account.SObjectType.getDescribe().getRecordTypeInfos();

		Account a = new Account();
		a.Name = 'Acme1';
		insert a;

		Contact con = new Contact(LastName = 'testCon', AccountId = a.Id);
		Contact con1 = new Contact(LastName = 'testCon1', AccountId = a.Id);
		Contact con2 = new Contact(LastName = 'testCon1', AccountId = a.Id);
		Contact con3 = new Contact(LastName = 'testCon2', AccountId = a.Id);

		List<Contact> cons = new List<Contact>();
		cons.add(con);
		cons.add(con1);
		cons.add(con2);
		cons.add(con3);
		insert cons;
		
		User user = new User(Alias = 'test123', Email = 'test123fvb@noemail.com',
							 Emailencodingkey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',    
							 LocaleSidKey = 'en_US', ProfileId = pcoriCommunityPartID , Country='United States',IsActive = true,   
							 ContactId = con.Id, CommunityNickname = 'test12',
							 TimeZoneSidKey='America/New_York', UserName = 'testerfvb1@noemail.com');

		User user1 = new User(Alias = 'test1234', Email = 'test1234fvb@noemail.com',
							  Emailencodingkey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',    
							  LocaleSidKey = 'en_US', ProfileId = pcoriCommunityPartID , Country='United States',IsActive = true,   
							  ContactId = con1.Id, CommunityNickname = 'test12345',
							  TimeZoneSidKey='America/New_York', UserName = 'testerfvb123@noemail.com');

		User user2 = new User(Alias = 'test145', Email = 'test12345fvb@noemail.com',
							  Emailencodingkey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',    
							  LocaleSidKey = 'en_US', ProfileId = pcoriCommunityPartID , Country='United States',IsActive = true,   
							  ContactId = con2.Id, CommunityNickname = 'test14',
							  TimeZoneSidKey='America/New_York', UserName = 'testerfvb12@noemail.com');
		
		User user3 = new User(Alias = 'test52', Email = 'test135fvb@noemail.com',
							  Emailencodingkey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',    
							  LocaleSidKey = 'en_US', ProfileId = pcoriCommunityPartID , Country='United States',IsActive = true,   
							  ContactId = con3.Id, CommunityNickname = 'test5',
							  TimeZoneSidKey = 'America/New_York', UserName = 'testerfvb82@noemail.com');

		List<User> users = new List<User>();
		users.add(user);
		users.add(user1);
		users.add(user2);
		users.add(user3);
		insert users;
		
		Cycle__c c = new Cycle__c();
		c.Name = 'testcycle';
		c.COI_Due_Date__c = date.valueof(System.now());
		insert c;

		Campaign camp = new Campaign();
		camp.Name = System.Label.Symptom_Management_for_Patients_with_Advanced_Illness;
		camp.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('PCORI Portal').getRecordTypeId();
		camp.Cycle__c = c.Id;
		camp.IsActive = true;
		camp.FGM_Portal__Visibility__c = 'Public';
		camp.Status = 'In Progress';
		camp.StartDate = System.today();
		camp.EndDate = System.today() + 7;
		camp.FGM_Base__Board_Meeting_Date__c = System.today() + 14;

		insert camp;
		
		opportunity o = new opportunity();
		
		o.Awardee_Institution_Organization__c=a.Id;
		o.CampaignId = camp.Id;
		o.RecordTypeId = r1;
		o.Full_Project_Title__c='test';
		o.Project_Name_off_layout__c='test';
		o.CloseDate = Date.today() + 14;
		o.Application_Number__c='Ra-1234';
		o.StageName = 'Pending PI Application';
		o.External_Status__c = 'Draft';
		o.Name='test';
		o.PI_Name_User__c = user.id;
		o.AO_Name_User__c = user1.id;
		o.Project_Start_Date__c = system.Today();
		o.Bypass_Flow__c = true;
		
		insert o;

		Test.startTest();

		//System.runAs(user){
			ApexPages.currentPage().getParameters().put('appId', o.Id);
			ApexPages.currentPage().getParameters().put('retURL', '/engagement/' + o.Id);
			BudgetPageController bp = new BudgetPageController();

			System.assertEquals(camp.name,bp.pfa.Name);
			System.assert(bp.isEditable);
			System.assert(bp.isApplication);
			System.assert(!bp.isProject);
			System.assert(!bp.isSupplemental);
			System.assertEquals(bp.years.size(),Integer.valueOf(System.Label.Max_Years_Budget_Covers));

			//Key Personnel Page
			bp.costCategorySelect1 = 'Key Personnel'; //change drop down navigation on top to 'Key Personnel'
			bp.setCostCategorySelect1(); //switch to project personnel page and save
			System.assertEquals('Key Personnel',bp.costCategory);

			System.assertNotEquals('Error',bp.infoHeader); //error should not be shown
			System.assert(![SELECT Id FROM Budget__c WHERE Id = : bp.budget.Id LIMIT 1].isEmpty()); //check budget successfully created

			//use the hide the info confirm method
			bp.hideInfoConfirm();

			//set bypass flow to true for budget
			bp.budget.Bypass_Flow__c = true;
			update bp.budget;

			for(Integer i=1;i<=6;i++) {
				//year i
				bp.currentYear = i;

				bp.addRow(); //addRow button pressed
				Integer rowIndex = bp.budgetLineItemsMap.get(bp.costCategory).get(bp.currentYear).values().size() - 1;
				BudgetLineItemDecorator decoratedKeyCostBudgetLine = bp.budgetLineItemsMap.get(bp.costCategory).get(bp.currentYear).values().get(rowIndex);
				Key_Personnel_Costs__c keyCostBudgetLine = decoratedKeyCostBudgetLine.lineItem; //get the line item that we are about to edit
				keyCostBudgetLine.Name = 'Henry Winkler';
				keyCostBudgetLine.Role_On_Project__c = 'Principal Investigator 1';
				keyCostBudgetLine.Percent_Effort__c = 75.0;
				keyCostBudgetLine.Calendar_Months__c = 6;
				keyCostBudgetLine.Inst_Base_Salary__c = 10;
				keyCostBudgetLine.Salary_Requested__c = 50;
				keyCostBudgetLine.Fringe_Benefits__c = 20;

				//test that the date is correctly caught as not being set
				System.assert(!bp.hasValidStartOrEndDate(bp.currentYear));
			}

			//Test Navigation with errors; Navigation order depends on Key_Personnel_Costs__c.Cost_Category__c picklist order
			if(bp.firstPage.equals('Key Personnel')) {
				bp.nextPage(); //next button pressed
				System.assertEquals('Key Personnel',bp.costCategory);
			}
			else {
				bp.prevPage(); //next button pressed
				System.assertEquals('Key Personnel',bp.costCategory);
			}
			System.assert(bp.showNavConfirm); //Insure the popup asking if the user wishes to continue to the next page when there are errors is visible
			bp.hideNavConfirm(); //cancel going to the next page since there are errors

			//test save for year with invalid date error
			bp.infoHeader = null; //clear out the infoHeader
			bp.saveForYear();
			System.assertEquals('Error',bp.infoHeader); //error should be shown

			//set the budget date for all years
			bp.budget.Start_Date_Year_1__c = System.today();
			bp.budget.End_Date_Year_1__c = System.today() + 30;
			bp.budget.Start_Date_Year_2__c = System.today();
			bp.budget.End_Date_Year_2__c = System.today() + 30;
			bp.budget.Start_Date_Year_3__c = System.today();
			bp.budget.End_Date_Year_3__c = System.today() + 30;
			bp.budget.Start_Date_Year_4__c = System.today();
			bp.budget.End_Date_Year_4__c = System.today() + 30;
			bp.budget.Start_Date_Year_5__c = System.today();
			bp.budget.End_Date_Year_5__c = System.today() + 30;
			bp.budget.Start_Date_Year_6__c = System.today();
			bp.budget.End_Date_Year_6__c = System.today() + 30;

			bp.saveAllRows(); //Save All button pressed
			System.assertNotEquals('Error',bp.infoHeader); //error should not be shown

			//set year back to 1
			bp.currentYear = 1;

			//test copy selected to all with nothing selected
			bp.copySelectedToAllYears();
			System.assertEquals('No Items Selected For Year 1',bp.infoHeader);

			//test delete selected for year with nothing selected
			bp.showDeleteAllConfirm(); //run show delete all confirm method
			bp.deleteAllSelectedForYear();
			System.assertEquals('No Items Selected For Year 1',bp.infoHeader);
			bp.hideDeleteAllConfirm(); //run the hide delete all confirm method

			//test delete all selected with nothing selected
			bp.showDeleteConfirm(); //run show delete confirm method
			bp.deleteAllSelected();
			System.assertEquals('No Items Selected',bp.infoHeader);
			bp.hideDeleteConfirm(); //run the hide delete confirm method;

			bp.addRow(); //addRow button pressed
			Integer rowIndex = bp.budgetLineItemsMap.get(bp.costCategory).get(bp.currentYear).values().size() - 1;
			BudgetLineItemDecorator decoratedKeyCostBudgetLine = bp.budgetLineItemsMap.get(bp.costCategory).get(bp.currentYear).values().get(rowIndex);
			Key_Personnel_Costs__c keyCostBudgetLine = decoratedKeyCostBudgetLine.lineItem; //get the line item that we are about to edit
			keyCostBudgetLine.Name = 'Stewart Derby';
			keyCostBudgetLine.Key__c = false;
			keyCostBudgetLine.Role_On_Project__c = 'Principal Investigator 1';
			keyCostBudgetLine.Percent_Effort__c = 75.0;
			keyCostBudgetLine.Calendar_Months__c = 6;
			keyCostBudgetLine.Inst_Base_Salary__c = 10;
			keyCostBudgetLine.Salary_Requested__c = 50;
			keyCostBudgetLine.Fringe_Benefits__c = 20;
			bp.saveForYear(); //save for year button pressed
			System.assertNotEquals('Error',bp.infoHeader); //error should not be shown
			System.assert(![SELECT Id FROM Key_Personnel_Costs__c WHERE Id = : keyCostBudgetLine.Id LIMIT 1].isEmpty()); //check line item created successfuly
			
			//Test Navigation without errors; Navigation order depends on Key_Personnel_Costs__c.Cost_Category__c picklist order
			if(bp.firstPage.equals('Key Personnel')) {
				bp.nextPage(); //next button pressed
				System.assertNotEquals('Key Personnel',bp.costCategory);
				bp.prevPage(); //previous button pressed
				System.assertEquals('Key Personnel',bp.costCategory);
			}
			else {
				bp.prevPage(); //next button pressed
				System.assertNotEquals('Key Personnel',bp.costCategory);
				bp.nextPage(); //previous button pressed
				System.assertEquals('Key Personnel',bp.costCategory);
			}
			System.assertNotEquals('Error',bp.infoHeader); //error should not be shown

			bp.costCategorySelect2 = 'Consultant Cost'; //change drop down navigation on top to 'Consultant Cost'
			bp.setCostCategorySelect2();
			bp.currentYear = 1;
			bp.addRow(); //addRow button pressed
			rowIndex = bp.budgetLineItemsMap.get(bp.costCategory).get(bp.currentYear).values().size() - 1;
			decoratedKeyCostBudgetLine = bp.budgetLineItemsMap.get(bp.costCategory).get(bp.currentYear).values().get(rowIndex);
			Key_Personnel_Costs__c consultantCostBudgetLine = decoratedKeyCostBudgetLine.lineItem; //get the line item that we are about to edit
			consultantCostBudgetLine.Description__c = 'Test class';
			consultantCostBudgetLine.Hourly_Unit_Rate__c = 10;
			consultantCostBudgetLine.Total_1__c = 100;
			bp.saveForYear(); //save for year button pressed
			System.assertNotEquals('Error',bp.infoHeader); //error should not be shown

			//test copy to all years (copying from year 1)
			decoratedKeyCostBudgetLine.isSelected = true;
			Integer rowsYear2 = bp.budgetLineItemsMap.get(bp.costCategory).get(2).values().size();
			Integer rowsYear3 = bp.budgetLineItemsMap.get(bp.costCategory).get(3).values().size();
			Integer rowsYear4 = bp.budgetLineItemsMap.get(bp.costCategory).get(4).values().size();
			Integer rowsYear5 = bp.budgetLineItemsMap.get(bp.costCategory).get(5).values().size();
			Integer rowsYear6 = bp.budgetLineItemsMap.get(bp.costCategory).get(6).values().size();
			bp.copySelectedToAllYears();
			System.assertEquals(rowsYear2 + 1, bp.budgetLineItemsMap.get(bp.costCategory).get(2).values().size());
			System.assertEquals(rowsYear3 + 1, bp.budgetLineItemsMap.get(bp.costCategory).get(3).values().size());
			System.assertEquals(rowsYear4 + 1, bp.budgetLineItemsMap.get(bp.costCategory).get(4).values().size());
			System.assertEquals(rowsYear5 + 1, bp.budgetLineItemsMap.get(bp.costCategory).get(5).values().size());
			System.assertEquals(rowsYear6 + 1, bp.budgetLineItemsMap.get(bp.costCategory).get(6).values().size());

			//test faulty data and proceed on error functionality
			bp.addRow(); //addRow button
			rowIndex = bp.budgetLineItemsMap.get(bp.costCategory).get(bp.currentYear).values().size() - 1;
			decoratedKeyCostBudgetLine = bp.budgetLineItemsMap.get(bp.costCategory).get(bp.currentYear).values().get(rowIndex);
			consultantCostBudgetLine = decoratedKeyCostBudgetLine.lineItem;
			consultantCostBudgetLine.Description__c = ''; //empty description
			consultantCostBudgetLine.Hourly_Unit_Rate__c = 10;
			consultantCostBudgetLine.Total_1__c = 100;
			if(bp.firstPage.equals('Consultant Cost')) {
				bp.nextPage();
			}
			else {
				bp.prevPage();
			}
			System.assert(bp.showNavConfirm = true); //popup asking if you want to navigate away without saving should be shown
			System.assertEquals('Error',bp.infoHeader); //error message should be set
			
			bp.setProceed(); //procceed to next page despite error
			System.assertNotEquals('Consultant Cost',bp.costCategory); //no longer on consultant cost page

			//select faulty line item
			decoratedKeyCostBudgetLine.isSelected = true;
			String deletedId = decoratedKeyCostBudgetLine.lineItem.Id;
			//go to back to consultant cost where the faulty line is
			bp.costCategory = 'Consultant Cost';
			//delete faulty line item
			bp.deleteAllSelectedForYear();
			System.assertNotEquals('Error',bp.infoHeader); //error should not be shown
			System.assert([SELECT Id FROM Key_Personnel_Costs__c WHERE Id = : decoratedKeyCostBudgetLine.lineItem.Id LIMIT 1].isEmpty()); //check line item deleted successfuly

			//go to scientific travel cost category to test travel validation
			bp.costCategory = 'Programmatic Travel';
			bp.addRow(); //addRow button
			rowIndex = bp.budgetLineItemsMap.get(bp.costCategory).get(bp.currentYear).values().size() - 1;
			decoratedKeyCostBudgetLine = bp.budgetLineItemsMap.get(bp.costCategory).get(bp.currentYear).values().get(rowIndex);
			Key_Personnel_Costs__c scienceCostBudgetLine = decoratedKeyCostBudgetLine.lineItem;
			scienceCostBudgetLine.Description__c = 'Science Travel';
			scienceCostBudgetLine.Total_1__c = 10001; //over max travel cost limit
			System.assertEquals(System.Label.Symptom_Management_for_Patients_with_Advanced_Illness,bp.pfa.Name);
			//test validate travel method
			bp.saveForYear(); //test save before running validate travel method
			//System.assertEquals('Error',bp.infoHeader);
			bp.hideTravelConfirm(); //hide popup notifying about travel cost being too high
			//fix travel total and validate again
			scienceCostBudgetLine.Total_1__c = 1001; //over max travel cost limit
			System.assert(bp.hasValidTravelTotal);

			//make sure the lastPage property is correct
			List<SelectOption> costCategories = bp.getPageOptions();
			System.assertEquals(costCategories.get(costCategories.size()-1).getValue(),bp.lastPage);
			
			//go to Budget Summary
			bp.costCategory = 'Budget Summary';

			System.assert(!bp.showJustification);
			System.assert(!bp.isSubmittable);

			System.assert(bp.budgetSummaryList != null);
			System.assertEquals(bp.openBudgetPDF().getUrl(),System.Label.Communities_EA_URL + '/BudgetPDFpage?id=' + bp.projectApp.Id);

			//test save on summary notes;
			bp.saveSummaryNotes();

			//go to total prime indirect
			bp.costCategory = 'Total Prime Indirect';

			//test save on total prime indirect
			bp.saveTotalPrimeIndirect();

			//test save that should fail on total prime indirect
			bp.budget.Total_Prime_Indirect_Costs_Year_1__c = null;
			bp.setIsChanged();
			bp.saveTotalPrimeIndirect();
			System.assertEquals('Error',bp.infoHeader);

			o.External_Status__c = null;
			o.StageName = 'Pending AO Approval';
			bp.budget.budget_status__c = 'Application Budget';
			System.Assert(!bp.isEditable);

			//test there are no exceptions when using navigation page in a non editable context
			if(bp.LastPage.equals('Budget Summary')) {
				bp.prevPage();
			}
			else {
				bp.nextPage();
			}
		//}

		Test.stopTest();

	}


	@isTest static void testExternalBudgetForProjectPhase() {
		Id r1 =Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Research Awards').getRecordTypeId();
		Id rKeyPersonnel =Schema.SObjectType.Key_Personnel_Costs__c.getRecordTypeInfosByName().get('Key Personnel').getRecordTypeId();
		Id rConsultantCost =Schema.SObjectType.Key_Personnel_Costs__c.getRecordTypeInfosByName().get('Consultant Cost').getRecordTypeId();
		Id rSubcontractorDirect =Schema.SObjectType.Key_Personnel_Costs__c.getRecordTypeInfosByName().get('Subcontractor Direct').getRecordTypeId();
		
		Id pcoriCommunityPartID = [SELECT Id FROM Profile WHERE name = 'PCORI Community Partner'].Id;
		Id cmaId = [SELECT Id FROM Profile WHERE name = 'CMA Operations' LIMIT 1].Id;

		Account.SObjectType.getDescribe().getRecordTypeInfos();

		Account a = new Account();
		a.Name = 'Acme1';
		insert a;

		Contact con = new Contact(LastName = 'testCon', AccountId = a.Id);
		Contact con1 = new Contact(LastName = 'testCon1', AccountId = a.Id);
		Contact con2 = new Contact(LastName = 'testCon1', AccountId = a.Id);
		Contact con3 = new Contact(LastName = 'testCon2', AccountId = a.Id);

		List<Contact> cons = new List<Contact>();
		cons.add(con);
		cons.add(con1);
		cons.add(con2);
		cons.add(con3);
		insert cons;
		
		User user = new User(Alias = 'test123', Email = 'test123fvb@noemail.com',
								Emailencodingkey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',    
								LocaleSidKey = 'en_US', ProfileId = pcoriCommunityPartID , Country='United States',IsActive = true,   
								ContactId = con.Id, CommunityNickname = 'test12',
								TimeZoneSidKey='America/New_York', UserName = 'testerfvb1@noemail.com');

		User user1 = new User(Alias = 'test1234', Email = 'test1234fvb@noemail.com',
								Emailencodingkey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',    
								LocaleSidKey = 'en_US', ProfileId = pcoriCommunityPartID , Country='United States',IsActive = true,   
								ContactId = con1.Id, CommunityNickname = 'test12345',
								TimeZoneSidKey='America/New_York', UserName = 'testerfvb123@noemail.com');

		User user2 = new User(Alias = 'test145', Email = 'test12345fvb@noemail.com',
								Emailencodingkey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',    
								LocaleSidKey = 'en_US', ProfileId = pcoriCommunityPartID , Country='United States',IsActive = true,   
								ContactId = con2.Id, CommunityNickname = 'test14',
								TimeZoneSidKey='America/New_York', UserName = 'testerfvb12@noemail.com');
		
		User user3 = new User(Alias = 'test52', Email = 'test135fvb@noemail.com',
								Emailencodingkey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',    
								LocaleSidKey = 'en_US', ProfileId = pcoriCommunityPartID , Country='United States',IsActive = true,   
								ContactId = con3.Id, CommunityNickname = 'test5',
								TimeZoneSidKey = 'America/New_York', UserName = 'testerfvb82@noemail.com');

		List<User> users = new List<User>();
		users.add(user);
		users.add(user1);
		users.add(user2);
		users.add(user3);
		insert users;
		
		Cycle__c c = new Cycle__c();
		c.Name = 'testcycle';
		c.COI_Due_Date__c = date.valueof(System.now());
		insert c;

		Campaign camp = new Campaign();
		camp.Name = System.Label.Symptom_Management_for_Patients_with_Advanced_Illness;
		camp.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('PCORI Portal').getRecordTypeId();
		camp.Cycle__c = c.Id;
		camp.IsActive = true;
		camp.FGM_Portal__Visibility__c = 'Public';
		camp.Status = 'In Progress';
		camp.StartDate = System.today();
		camp.EndDate = System.today() + 7;
		camp.FGM_Base__Board_Meeting_Date__c = System.today() + 14;

		insert camp;
		
		opportunity o = new opportunity();
		
		o.Awardee_Institution_Organization__c=a.Id;
		o.CampaignId = camp.Id;
		o.RecordTypeId = r1;
		o.Full_Project_Title__c='test';
		o.Project_Name_off_layout__c='test';
		o.CloseDate = Date.today() + 14;
		o.Application_Number__c='Ra-1234';
		o.StageName = 'Application Awarded';
		o.External_Status__c = '';
		o.Name='test';
		o.PI_Name_User__c = user.id;
		o.AO_Name_User__c = user1.id;
		o.Project_Start_Date__c = system.Today();
		o.Bypass_Flow__c = true;
		
		insert o;

		Budget__c b = new Budget__c();
		b.Associated_App_Project__c= o.id;
		b.Budget_Status__c = 'Working Budget - Pending Submission';
		b.Bypass_Flow__c = true;
		insert b;

		//add an existing line item
		Key_Personnel_Costs__c keyCostBudgetLine = new Key_Personnel_Costs__c();
		keyCostBudgetLine.Budget__c = b.id;
		keyCostBudgetLine.RecordTypeId = rKeyPersonnel;
		keyCostBudgetLine.Cost_Category__c = 'Key Personnel';
		keyCostBudgetLine.Year__c = 1;
		keyCostBudgetLine.Name = 'Henry Winkler';
		keyCostBudgetLine.Key__c = false;
		keyCostBudgetLine.Role_On_Project__c='Principal Investigator 1';
		keyCostBudgetLine.Percent_Effort__c =75.0;
		keyCostBudgetLine.Calendar_Months__c = 6;
		keyCostBudgetLine.Inst_Base_Salary__c = 10;
		keyCostBudgetLine.Salary_Requested__c = 50;
		keyCostBudgetLine.Fringe_Benefits__c = 20;

		Test.startTest();

		//System.runAs(user){
			ApexPages.currentPage().getParameters().put('budgetId', b.Id);
			ApexPages.currentPage().getParameters().put('retURL', '/engagement/' + o.Id);
			BudgetPageController bp = new BudgetPageController();

			System.assertEquals(camp.name,bp.pfa.Name);
			System.assert(!bp.isApplication);
			System.assert(bp.isProject);
			System.assert(!bp.isSupplemental);
			System.assertEquals(bp.years.size(),Integer.valueOf(System.Label.Max_Years_Budget_Covers));

			//currently opportunity status is 'Application Awarded'
			System.assert(bp.isEditable);
			bp.projectApp.StageName = 'Executed';
			System.assert(bp.isEditable);
			bp.projectApp.StageName = 'Programmatic Review';
			bp.budget.Budget_Status__c = 'Requesting Further Edits';
			System.assert(bp.isEditable);
			bp.budget.Budget_Status__c = null;
			bp.projectApp.StageName = 'Pending PI Application';
			System.assert(bp.hasPeerReviewPeriod);
			System.assert(bp.isEditable);

			//put opportunity status back to 'Application Awarded' and budget status to 'Working Budget - Pending Submission'
			bp.projectApp.StageName = 'Application Awarded';
			bp.budget.Budget_Status__c = 'Working Budget - Pending Submission';

			//Key Personnel Page
			bp.costCategorySelect1 = 'Key Personnel'; //change drop down navigation on top to 'Key Personnel'
			bp.setCostCategorySelect1(); //switch to project personnel page and save
			System.assertEquals('Key Personnel',bp.costCategory);
			System.assertNotEquals('Error',bp.infoHeader); //error should not be shown

			//use the hide the info confirm method
			bp.hideInfoConfirm();

			for(Integer i=1;i<=6;i++) {
				//year i
				bp.currentYear = i;

				bp.addRow(); //addRow button pressed
				Integer rowIndex = bp.budgetLineItemsMap.get(bp.costCategory).get(bp.currentYear).values().size() - 1;
				BudgetLineItemDecorator decoratedKeyCostBudgetLine = bp.budgetLineItemsMap.get(bp.costCategory).get(bp.currentYear).values().get(rowIndex);
				keyCostBudgetLine = decoratedKeyCostBudgetLine.lineItem; //get the line item that we are about to edit
				keyCostBudgetLine.Name = 'Henry Winkler';
				keyCostBudgetLine.Role_On_Project__c = 'Principal Investigator 1';
				keyCostBudgetLine.Percent_Effort__c = 75.0;
				keyCostBudgetLine.Calendar_Months__c = 6;
				keyCostBudgetLine.Inst_Base_Salary__c = 10;
				keyCostBudgetLine.Salary_Requested__c = 50;
				keyCostBudgetLine.Fringe_Benefits__c = 20;

				//test that the date is correctly caught as not being set
				System.assert(!bp.hasValidStartOrEndDate(bp.currentYear));
			}

			//Test Navigation with errors; Navigation order depends on Key_Personnel_Costs__c.Cost_Category__c picklist order
			if(bp.firstPage.equals('Key Personnel')) {
				bp.nextPage(); //next button pressed
				System.assertEquals('Key Personnel',bp.costCategory);
			}
			else {
				bp.prevPage(); //next button pressed
				System.assertEquals('Key Personnel',bp.costCategory);
			}
			System.assert(bp.showNavConfirm); //Insure the popup asking if the user wishes to continue to the next page when there are errors is visible
			bp.hideNavConfirm(); //cancel going to the next page since there are errors

			//test save for year with invalid date error
			bp.infoHeader = null; //clear out the infoHeader
			bp.saveForYear();
			System.assertEquals('Error',bp.infoHeader); //error should be shown

			//set the budget date for all years
			bp.budget.Start_Date_Year_1__c = System.today();
			bp.budget.End_Date_Year_1__c = System.today() + 30;
			bp.budget.Start_Date_Year_2__c = System.today();
			bp.budget.End_Date_Year_2__c = System.today() + 30;
			bp.budget.Start_Date_Year_3__c = System.today();
			bp.budget.End_Date_Year_3__c = System.today() + 30;
			bp.budget.Start_Date_Year_4__c = System.today();
			bp.budget.End_Date_Year_4__c = System.today() + 30;
			bp.budget.Start_Date_Year_5__c = System.today();
			bp.budget.End_Date_Year_5__c = System.today() + 30;
			bp.budget.Start_Date_Year_6__c = System.today();
			bp.budget.End_Date_Year_6__c = System.today() + 30;

			bp.saveAllRows(); //Save All button pressed
			System.assertNotEquals('Error',bp.infoHeader); //error should not be shown

			//set year back to 1
			bp.currentYear = 1;

			//test copy selected to all with nothing selected
			bp.copySelectedToAllYears();
			System.assertEquals('No Items Selected For Year 1',bp.infoHeader);

			//test delete selected for year with nothing selected
			bp.showDeleteAllConfirm(); //run show delete all confirm method
			bp.deleteAllSelectedForYear();
			System.assertEquals('No Items Selected For Year 1',bp.infoHeader);
			bp.hideDeleteAllConfirm(); //run the hide delete all confirm method

			//test delete all selected with nothing selected
			bp.showDeleteConfirm(); //run show delete confirm method
			bp.deleteAllSelected();
			System.assertEquals('No Items Selected',bp.infoHeader);
			bp.hideDeleteConfirm(); //run the hide delete confirm method;

			bp.addRow(); //addRow button pressed
			Integer rowIndex = bp.budgetLineItemsMap.get(bp.costCategory).get(bp.currentYear).values().size() - 1;
			BudgetLineItemDecorator decoratedKeyCostBudgetLine = bp.budgetLineItemsMap.get(bp.costCategory).get(bp.currentYear).values().get(rowIndex);
			keyCostBudgetLine = decoratedKeyCostBudgetLine.lineItem; //get the line item that we are about to edit
			keyCostBudgetLine.Name = 'Stewart Derby';
			keyCostBudgetLine.Key__c = false;
			keyCostBudgetLine.Role_On_Project__c = 'Principal Investigator 1';
			keyCostBudgetLine.Percent_Effort__c = 75.0;
			keyCostBudgetLine.Calendar_Months__c = 6;
			keyCostBudgetLine.Inst_Base_Salary__c = 10;
			keyCostBudgetLine.Salary_Requested__c = 50;
			keyCostBudgetLine.Fringe_Benefits__c = 20;
			bp.saveForYear(); //save for year button pressed
			System.assertNotEquals('Error',bp.infoHeader); //error should not be shown
			System.assert(![SELECT Id FROM Key_Personnel_Costs__c WHERE Id = : keyCostBudgetLine.Id LIMIT 1].isEmpty()); //check line item created successfuly
			
			//Test Navigation without errors; Navigation order depends on Key_Personnel_Costs__c.Cost_Category__c picklist order
			if(bp.firstPage.equals('Key Personnel')) {
				bp.nextPage(); //next button pressed
				System.assertNotEquals('Key Personnel',bp.costCategory);
				bp.prevPage(); //previous button pressed
				System.assertEquals('Key Personnel',bp.costCategory);
			}
			else {
				bp.prevPage(); //next button pressed
				System.assertNotEquals('Key Personnel',bp.costCategory);
				bp.nextPage(); //previous button pressed
				System.assertEquals('Key Personnel',bp.costCategory);
			}
			System.assertNotEquals('Error',bp.infoHeader); //error should not be shown

			bp.costCategorySelect2 = 'Consultant Cost'; //change drop down navigation on top to 'Consultant Cost'
			bp.setCostCategorySelect2();
			bp.currentYear = 1;
			bp.addRow(); //addRow button pressed
			rowIndex = bp.budgetLineItemsMap.get(bp.costCategory).get(bp.currentYear).values().size() - 1;
			decoratedKeyCostBudgetLine = bp.budgetLineItemsMap.get(bp.costCategory).get(bp.currentYear).values().get(rowIndex);
			Key_Personnel_Costs__c consultantCostBudgetLine = decoratedKeyCostBudgetLine.lineItem; //get the line item that we are about to edit
			consultantCostBudgetLine.Description__c = 'Test class';
			consultantCostBudgetLine.Hourly_Unit_Rate__c = 10;
			consultantCostBudgetLine.Total_1__c = 100;
			bp.saveForYear(); //save for year button pressed
			System.assertNotEquals('Error',bp.infoHeader); //error should not be shown

			//test copy to all years (copying from year 1)
			decoratedKeyCostBudgetLine.isSelected = true;
			Integer rowsYear2 = bp.budgetLineItemsMap.get(bp.costCategory).get(2).values().size();
			Integer rowsYear3 = bp.budgetLineItemsMap.get(bp.costCategory).get(3).values().size();
			Integer rowsYear4 = bp.budgetLineItemsMap.get(bp.costCategory).get(4).values().size();
			Integer rowsYear5 = bp.budgetLineItemsMap.get(bp.costCategory).get(5).values().size();
			Integer rowsYear6 = bp.budgetLineItemsMap.get(bp.costCategory).get(6).values().size();
			bp.copySelectedToAllYears();
			System.assertEquals(rowsYear2 + 1, bp.budgetLineItemsMap.get(bp.costCategory).get(2).values().size());
			System.assertEquals(rowsYear3 + 1, bp.budgetLineItemsMap.get(bp.costCategory).get(3).values().size());
			System.assertEquals(rowsYear4 + 1, bp.budgetLineItemsMap.get(bp.costCategory).get(4).values().size());
			System.assertEquals(rowsYear5 + 1, bp.budgetLineItemsMap.get(bp.costCategory).get(5).values().size());
			System.assertEquals(rowsYear6 + 1, bp.budgetLineItemsMap.get(bp.costCategory).get(6).values().size());

			//test faulty data and proceed on error functionality
			bp.addRow(); //addRow button
			rowIndex = bp.budgetLineItemsMap.get(bp.costCategory).get(bp.currentYear).values().size() - 1;
			decoratedKeyCostBudgetLine = bp.budgetLineItemsMap.get(bp.costCategory).get(bp.currentYear).values().get(rowIndex);
			consultantCostBudgetLine = decoratedKeyCostBudgetLine.lineItem;
			consultantCostBudgetLine.Description__c = ''; //empty description
			consultantCostBudgetLine.Hourly_Unit_Rate__c = 10;
			consultantCostBudgetLine.Total_1__c = 100;
			if(bp.firstPage.equals('Consultant Cost')) {
				bp.nextPage();
			}
			else {
				bp.prevPage();
			}
			System.assert(bp.showNavConfirm = true); //popup asking if you want to navigate away without saving should be shown
			System.assertEquals('Error',bp.infoHeader); //error message should be set
			
			bp.setProceed(); //procceed to next page despite error
			System.assertNotEquals('Consultant Cost',bp.costCategory); //no longer on consultant cost page

			//select faulty line item
			decoratedKeyCostBudgetLine.isSelected = true;
			String deletedId = decoratedKeyCostBudgetLine.lineItem.Id;
			//go to back to consultant cost where the faulty line is
			bp.costCategory = 'Consultant Cost';
			//delete faulty line item
			bp.deleteAllSelectedForYear();
			System.assertNotEquals('Error',bp.infoHeader); //error should not be shown
			System.assert([SELECT Id FROM Key_Personnel_Costs__c WHERE Id = : decoratedKeyCostBudgetLine.lineItem.Id LIMIT 1].isEmpty()); //check line item deleted successfuly

			//go to scientific travel cost category to test travel validation
			bp.costCategory = 'Programmatic Travel';
			bp.addRow(); //addRow button
			rowIndex = bp.budgetLineItemsMap.get(bp.costCategory).get(bp.currentYear).values().size() - 1;
			decoratedKeyCostBudgetLine = bp.budgetLineItemsMap.get(bp.costCategory).get(bp.currentYear).values().get(rowIndex);
			Key_Personnel_Costs__c scienceCostBudgetLine = decoratedKeyCostBudgetLine.lineItem;
			scienceCostBudgetLine.Description__c = 'Science Travel';
			scienceCostBudgetLine.Total_1__c = 10001; //over max travel cost limit
			System.assertEquals(System.Label.Symptom_Management_for_Patients_with_Advanced_Illness,bp.pfa.Name);
			//test validate travel method
			bp.saveAllRows(); //test save all before running validate travel method
			//System.assert(!bp.hasValidTravelTotal);
			bp.hideTravelConfirm(); //hide popup notifying about travel cost being too high
			//fix travel total and validate again
			scienceCostBudgetLine.Total_1__c = 1000; //over max travel cost limit
			System.assert(bp.hasValidTravelTotal);

			//make sure the lastPage property is correct
			List<SelectOption> costCategories = bp.getPageOptions();
			System.assertEquals(costCategories.get(costCategories.size()-1).getValue(),bp.lastPage);

			//go to total prime indirect
			bp.costCategory = 'Total Prime Indirect';
			bp.setIsChanged(); //pretend something was changed

			//test save on total prime indirect
			bp.saveTotalPrimeIndirect();

			//test save on navigate works
			if(bp.firstPage.equals('Consultant Cost')) {
				bp.nextPage();
				System.assert(!bp.showNavConfirm);
				bp.prevPage();
			}
			else {
				bp.prevPage();
				System.assert(!bp.showNavConfirm);
				bp.nextPage();
			}

			//test save that should fail on total prime indirect
			bp.budget.Total_Prime_Indirect_Costs_Year_1__c = null;
			bp.setIsChanged();
			bp.saveTotalPrimeIndirect();
			System.assertEquals('Error',bp.infoHeader);
			
			//go to Budget Summary
			bp.costCategory = 'Budget Summary';

			System.assert(bp.showJustification);

			//test is submittable
			System.assert(bp.isSubmittable);
			//check with other budget status
			bp.budget.Budget_Status__c = 'Requesting Further Edits';
			System.assert(bp.isSubmittable);

			System.assert(bp.budgetSummaryList != null);
			System.assertEquals(bp.openBudgetPDF().getUrl(),System.Label.Communities_EA_URL + '/BudgetPDFpage?id=' + bp.projectApp.Id);

			//test save on summary notes;
			bp.budget.Summary_Notes_Awardee__c = 'Test summary notes';
			bp.setIsChanged();
			bp.saveSummaryNotes();
			System.assertNotEquals('Error',bp.infoHeader);

			//test save on navigate for budget summary
			if(bp.LastPage.equals('Budget Summary')) {
				bp.prevPage();
				System.assert(!bp.showNavConfirm);
			}
			else {
				bp.nextPage();
				System.assert(!bp.showNavConfirm);
			}

			bp.costCategory = 'Budget Summary'; //back to budget summary
			bp.showSubmitConfirm();
			System.assert(!bp.showInfoConfirm);
			bp.submitForReview();
			System.assertEquals('Working Budget - Submitted',bp.budget.Budget_Status__c);
			bp.hideSubmitConfirm();

			Test.stopTest();

		//}

	}
	
}