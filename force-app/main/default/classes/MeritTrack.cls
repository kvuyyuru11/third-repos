public with sharing class MeritTrack {
    public string Reviewerrole{get;set;}
    public String strInput {get;set;}
    public String phonestyle{ get; set; }
    public String PIstyle{ get; set; }
    public String streetstyle{ get; set; }
    public String citystyle{ get; set; }
    public String zipstyle{ get; set; }
    public String statestyle{ get; set; }
    public String citystyle1{ get; set; }
    public String authorstyle{ get; set; }
    public String zipstyle1{ get; set; }
    public String statestyle1{ get; set; }
    public String citystyle2{ get; set; }
    public String zipstyle2{ get; set; }
    public String statestyle2{ get; set; }
     public String orgstyle{ get; set; }
    public String otherorgstyle{ get; set; }
    public String orgstyle11{ get; set; }
    public String otherspecify{ get; set; }
    public String otherspecifyorg{ get; set; }
    public contact mycon{get;set;}
    public boolean other2{get;set;}
    public boolean other3{get;set;}
    public boolean pso{get;set;}
    public String statestyle11{ get; set; }
    
    

 public list<Merit_Reviewer_Application__c> MRlist1{get;set;}
 public  Merit_Reviewer_Application__c mra{get;set;}
 public  Merit_Reviewer_Application__c myApp;
 public  MeritTrack (){
     mra=new  Merit_Reviewer_Application__c();
     if(userinfo.getUserId()!=null&&userinfo.getUserId()!=''){
       user us=[select id,contactid from user where id=:userinfo.getUserId()];
        mycon= [select id,Accountid from contact where Id=:us.contactid];   
      myApp= [select id,Application_Status__c,Name, Reviewer_Role__c,Age__c,Application_Reassignment_Notes__c,
              Highest_Level_of_Education__c,Highest_Degree_of_Education_New__c,Number_of_Scientific_Panels__c,
              Served_as_Chair_or_Co_Chair__c,Number_of_Publications__c,Are_you_a_PI_Co_PI_Site_PI__c,Number_of_authored_Publications__c,Organizational_Institutional_Affiliation__c,Other_Org_Inst_Affiliation__c,patient_coommunities__c,StakeholderCommunities_New__c
              from Merit_Reviewer_Application__c where OwnerId=:userinfo.getUserId() AND rejected__c=false limit 1];
        mra.Reviewer_Role__c=myApp.Reviewer_Role__c; 
        mra.Highest_Level_of_Education__c=myApp.Highest_Level_of_Education__c;
       mra.Highest_Degree_of_Education_New__c= myApp.Highest_Degree_of_Education_New__c;
        mra.Number_of_Scientific_Panels__c= myApp.Number_of_Scientific_Panels__c;
        mra.Served_as_Chair_or_Co_Chair__c= myApp.Served_as_Chair_or_Co_Chair__c;
        mra.Number_of_Publications__c= myApp.Number_of_Publications__c; 
        mra.StakeholderCommunities_New__c=  myApp.StakeholderCommunities_New__c;
        mra.patient_coommunities__c=  myApp.patient_coommunities__c;
         mra.Organizational_Institutional_Affiliation__c=myApp.Organizational_Institutional_Affiliation__c;
       mra.Other_Org_Inst_Affiliation__c=myApp.Other_Org_Inst_Affiliation__c;
         mra.Number_of_authored_Publications__c=myApp.Number_of_authored_Publications__c;
         mra.Are_you_a_PI_Co_PI_Site_PI__c=myApp.Are_you_a_PI_Co_PI_Site_PI__c;
         }
}
 
   public List<SelectOption> getstates()
{
          List<SelectOption> option = new List<SelectOption>();
                
           Schema.DescribeFieldResult fieldResult =
           Merit_Reviewer_Application__c.Highest_Degree_of_Education_New__c.getDescribe();
           List<Schema.PicklistEntry> pl = fieldResult.getPicklistValues();
                
           for( Schema.PicklistEntry ff : pl)
          {
              option.add(new SelectOption(ff.getlabel(),ff.getValue()));
           }       
   return option;
}

   public pagereference Nextmethod1(){ 
         myApp.Reviewer_Role__c=mra.Reviewer_Role__c;
       String HLOE=String.valueOf(mra.Highest_Level_of_Education__c);
         integer x=0;
         phonestyle='';
         PIstyle='';
         streetstyle='';
         citystyle='';
         zipstyle='';
         statestyle='';
         orgstyle='';
         citystyle1='';
         zipstyle1='';
         statestyle1='';
         citystyle2='';
         zipstyle2='';
         statestyle2='';
       otherorgstyle='';
       authorstyle='';
     if(myApp.Reviewer_Role__c=='Scientist'){ 
       if(mra.Highest_Level_of_Education__c=='--None--'){
      phonestyle= label.css_value;
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields'));  
      }
      if(mra.Highest_Level_of_Education__c!=Label.Professional && mra.Highest_Level_of_Education__c!=Label.Masters){
      phonestyle= label.css_value;
           ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Dear applicant, an advanced research degree is required for Scientific Reviewer applications. PCORI Scientist Reviewers must also lead research activities (i.e., PI, Co-PI, Site PI, etc. on a current or recent grant/contract) and/or significantly contribute to multiple publications in peer-reviewed scientific journals in the past 5 years.'));
      }
         
      if(mra.Highest_Degree_of_Education_New__c==''||mra.Highest_Degree_of_Education_New__c==null){
      
      streetstyle= label.css_value;
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields'));
      }
             if(mra.Highest_Degree_of_Education_New__c!=null && mra.Highest_Degree_of_Education_New__c.length()>7){
       streetstyle= label.css_value;
                 x=1;
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Degree cannot be greater than 7 characters'));
      }
         
      if(mra.Number_of_Scientific_Panels__c==''|| mra.Number_of_Scientific_Panels__c==null){
      citystyle= label.css_value; 
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields'));  
      }   
      if(mra.Served_as_Chair_or_Co_Chair__c==''|| mra.Served_as_Chair_or_Co_Chair__c==null){
      zipstyle= label.css_value; 
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields'));
      } 
      if(mra.Number_of_Publications__c==''|| mra.Number_of_Publications__c==null){
      statestyle= label.css_value; 
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields')); 
      } 
          if((mra.Number_of_Publications__c!='0')&&(mra.Number_of_authored_Publications__c==''||mra.Number_of_authored_Publications__c==null)){
              x=1;
      authorstyle= label.css_value; 
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields')); 
      } 
            if(mra.Are_you_a_PI_Co_PI_Site_PI__c==''||mra.Are_you_a_PI_Co_PI_Site_PI__c==null){
      PIstyle= label.css_value; 
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields')); 
      } 
           String err = 'Dear Applicant, Scientist Reviewers must have significantly contributed to multiple publications in peer-reviewed scientific journals in the past 5 years and/or lead research activities on a current or recent research grant/contract. Qualifications for all reviewer roles are found <a href="http://www.pcori.org/get-involved/become-merit-reviewer/reviewer-qualifications" target="_blank">here</a>, including Patient and Stakeholder Reviewer roles which are equally important to the Merit Review process.'; 
               
               String err1='While reviewers play a critical role in helping PCORI to realize its mission, reviewing applications is just one of many ways that you can contribute to the advancement of patient-centered outcomes research, as well as help patients and those who care for them make more informed health care decisions. Learn more about other opportunities to get involved at PCORI <a href="http://www.pcori.org/get-involved" target="_blank">here</a>.';
               


         if(mra.Number_of_Publications__c=='0'&&mra.Are_you_a_PI_Co_PI_Site_PI__c=='No'){
              
              
            x=1;
      statestyle= label.css_value; 
              
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,err));
                  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,err1));
              
      } 
          
         if(mra.Organizational_Institutional_Affiliation__c==''|| mra.Organizational_Institutional_Affiliation__c==null){
      orgstyle=label.css_value1; 
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields'));
      } 
         if(mra.Organizational_Institutional_Affiliation__c!='' && mra.Organizational_Institutional_Affiliation__c!=null && mra.Organizational_Institutional_Affiliation__c.Contains('Other') && (mra.Other_Org_Inst_Affiliation__c==''|| mra.Other_Org_Inst_Affiliation__c==null)){
         otherorgstyle=label.css_value2;
             x=1;
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields')); 
         }
      if(mra.Highest_Degree_of_Education_New__c=='Other (please specify)'&& (otherspecify==null || otherspecify=='')){
      statestyle11= label.css_value; 
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields'));
          return null;
      } 
        
       if(mra.Highest_Level_of_Education__c!=''&&mra.Highest_Level_of_Education__c!=null&&mra.Highest_Degree_of_Education_New__c!=null&&mra.Highest_Degree_of_Education_New__c!=''
         &&mra.Number_of_Scientific_Panels__c!=''&& mra.Number_of_Scientific_Panels__c!=null&&mra.Served_as_Chair_or_Co_Chair__c!=''&& mra.Served_as_Chair_or_Co_Chair__c!=null
          &&mra.Number_of_Publications__c!=''&& mra.Number_of_Publications__c!=null&&mra.Are_you_a_PI_Co_PI_Site_PI__c!=null &&mra.Organizational_Institutional_Affiliation__c!=''&&mra.Organizational_Institutional_Affiliation__c!=null&& x==0&&(mra.Highest_Level_of_Education__c==Label.Professional
          ||mra.Highest_Level_of_Education__c==Label.Masters)) {
      myApp.Highest_Degree_of_Education_New__c=mra.Highest_Degree_of_Education_New__c;  
      myApp.Highest_Level_of_Education__c=mra.Highest_Level_of_Education__c;
      myApp.Number_of_Scientific_Panels__c=mra.Number_of_Scientific_Panels__c;
              myApp.Are_you_a_PI_Co_PI_Site_PI__c=mra.Are_you_a_PI_Co_PI_Site_PI__c;
      myApp.Served_as_Chair_or_Co_Chair__c=mra.Served_as_Chair_or_Co_Chair__c;
      myApp.Number_of_Publications__c=mra.Number_of_Publications__c; 
              if(mra.Number_of_Publications__c=='0'){
                   myApp.Number_of_authored_Publications__c='';
              }
               if(mra.Number_of_Publications__c!='0'){
                   myApp.Number_of_authored_Publications__c= mra.Number_of_authored_Publications__c;
              }
              otherorgstyle='';
              myApp.Organizational_Institutional_Affiliation__c=mra.Organizational_Institutional_Affiliation__c;
               if(mra.Organizational_Institutional_Affiliation__c.Contains('Other') && (mra.Other_Org_Inst_Affiliation__c!=''|| mra.Other_Org_Inst_Affiliation__c!=null)){
         myApp.Other_Org_Inst_Affiliation__c=mra.Other_Org_Inst_Affiliation__c;
         }
         
         if(!mra.Organizational_Institutional_Affiliation__c.Contains('Other')){
         myApp.Other_Org_Inst_Affiliation__c='';
         }
         myApp.patient_coommunities__c='';
         myApp.StakeholderCommunities_New__c='';       
        pagereference pg=new pagereference(label.link_for_merit_trac); 
      
             
            try{
            update  myApp;
      }catch(Exception e){
 
      }
       return pg;
    }
         //else{
         //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Dear applicant, An advanced educational degree (i.e. MA, PhD, MD, MPH, etc) is required for Scientific Reviewer applications.'));
        // return null;
    //}   

    }
       if(myApp.Reviewer_Role__c=='Stakeholder'){ 
            system.debug('Testing for update***************'+MRlist1); 
       if(mra.Highest_Level_of_Education__c==''|| mra.Highest_Level_of_Education__c==null){
      citystyle1= label.css_value;
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields'));
      }
      if(mra.Highest_Degree_of_Education_New__c==''||mra.Highest_Degree_of_Education_New__c==null){
      system.debug('when not none ****************************************************************'+mra.Highest_Degree_of_Education__c);
      zipstyle1= label.css_value;
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields'));
      }
      if(mra.StakeholderCommunities_New__c==''|| mra.StakeholderCommunities_New__c==null){
      statestyle1= 'border-size:1px; border-color:red;border-style:solid;'; 
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields')); 
      } 
         
           if(mra.Highest_Degree_of_Education_New__c!=null && mra.Highest_Degree_of_Education_New__c.length()>7){
       zipstyle1= label.css_value;
                 x=1;
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Degree cannot be greater than 7 characters'));
      }
      
       if(mra.Highest_Level_of_Education__c!=''&&mra.Highest_Level_of_Education__c!=null&&mra.Highest_Degree_of_Education_New__c!=null
         &&mra.StakeholderCommunities_New__c!=''&& mra.StakeholderCommunities_New__c!=null&& x==0) {
      
        myApp.Highest_Level_of_Education__c=mra.Highest_Level_of_Education__c;
        myApp.Highest_Degree_of_Education_New__c=mra.Highest_Degree_of_Education_New__c;
        myApp.StakeholderCommunities_New__c=mra.StakeholderCommunities_New__c;
         myApp.patient_coommunities__c=''; 
      myApp.Number_of_Scientific_Panels__c='';
      myApp.Served_as_Chair_or_Co_Chair__c='';
      myApp.Number_of_Publications__c='';
             myApp.Organizational_Institutional_Affiliation__c='';
             myApp.Other_Comments__c='';
             myApp.Number_of_authored_Publications__c='';
             myApp.Are_you_a_PI_Co_PI_Site_PI__c='';
        pagereference pg=new pagereference(label.link_for_merit_trac);  
             try{
       update  myApp;
      }catch(Exception e){
      }
      return pg;
     }    
   }
       if(myApp.Reviewer_Role__c=='Patient'){ 
           if(mra.Highest_Level_of_Education__c==''|| mra.Highest_Level_of_Education__c==null){
      citystyle2= label.css_value;
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields'));
      }
      if(mra.Highest_Degree_of_Education_New__c==''||mra.Highest_Degree_of_Education_New__c==null){
      system.debug('when not none ****************************************************************'+mra.Highest_Degree_of_Education__c);
      zipstyle2= label.css_value;
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields'));
      }
      if(mra.patient_coommunities__c==''|| mra.patient_coommunities__c==null){
      statestyle2= 'border-size:1px; border-color:red;border-style:solid;'; 
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields')); 
      } 
     
                if(mra.Highest_Degree_of_Education_New__c!=null && mra.Highest_Degree_of_Education_New__c.length()>7){
        zipstyle2= label.css_value;
                 x=1;
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Degree cannot be greater than 7 characters'));
      }
      
       if(mra.Highest_Level_of_Education__c!=''&&mra.Highest_Level_of_Education__c!=null&&mra.Highest_Degree_of_Education_New__c!=null
         &&mra.patient_coommunities__c!=''&& mra.patient_coommunities__c!=null&& x==0) {
      

        myApp.Highest_Level_of_Education__c=mra.Highest_Level_of_Education__c;
        myApp.Highest_Degree_of_Education_New__c=mra.Highest_Degree_of_Education_New__c;
        myApp.patient_coommunities__c=mra.patient_coommunities__c;
             system.debug('*********'+ mra.patient_coommunities__c);
         myApp.StakeholderCommunities_New__c=''; 
      myApp.Number_of_Scientific_Panels__c='';
      myApp.Served_as_Chair_or_Co_Chair__c='';
      myApp.Number_of_Publications__c=''; 
             myApp.Organizational_Institutional_Affiliation__c='';
             myApp.Other_Comments__c='';
              myApp.Number_of_authored_Publications__c='';
             myApp.Are_you_a_PI_Co_PI_Site_PI__c='';
        pagereference pg=new pagereference(label.link_for_merit_trac);
      
             try{
       update  myApp;
      }catch(Exception e){
      }
      return pg;         
    }     
  }
       return null;
 }
  public pagereference Nextmethod() {
     myApp.Reviewer_Role__c=mra.Reviewer_Role__c;
      strInput =mra.Reviewer_Role__c;
      system.debug('Strinput for data*******************'+myApp.Reviewer_Role__c);
      system.debug('Testing for updateeeeeee***************'+mra.Reviewer_Role__c);
        if(myApp.Reviewer_Role__c=='Scientist'){ 
         system.debug('Testing for update***************'+MRlist1);  
         pagereference pg=new pagereference(label.link_MRA_Scientist);
         update  myApp;
         return pg;
        }
         if(myApp.Reviewer_Role__c=='Stakeholder'){         
      //system.debug('Strinput for data roleeeeeeeeee*******************role'+  mrr.Reviewer_Role__c);
          pagereference pg=new pagereference(label.Link_Stakeholder);
          update  myApp;
          system.debug('Strinput for data*******************role'+  strInput);
          return pg;
        }
       if(myApp.Reviewer_Role__c=='Patient'){ 
         pagereference pg=new pagereference(label.Link_Patpage);
         update  myApp;
         return pg;
        } 
 
    return null;
   }
  
  
    public pagereference previousmethod(){
          system.debug('Strinput for data*******************role'+  mra.Reviewer_Role__c);
          PageReference pageRef= new PageReference(label.Link_landing_page); 
          pageRef.setRedirect(true);          
         return pageRef; 
   }
   public pagereference other1() {
      if(mra.Highest_Degree_of_Education_New__c=='Other (please specify)'){
       other2=true;
     }
        else{
        other2=false;
            statestyle11='';
     }
  return null;
 }
  
    
     
    

}