/*
    Author     : Vijaya Kumar Sripathi
    Date       : 10th Mar 2017
    Name       : ShareProjectsWithKeyProjectPersonnelTest 
    Description: ShareProjectsWithKeyProjectPersonnel Trigger Test class
*/
@isTest
public class ShareProjectsWithKeyProjectPersonnelTest
{
    private static testMethod void TestTrigger()
    {
        User testPI1usr;
        User testPI2usr;
        User testPID1usr;
        User testPID2usr;
        User testCMusr;
        User testAOusr;
        User testFAusr;
        
        User testPI1usr1;
        User testPI2usr1;
        User testPID1usr1;
        User testPID2usr1;
        User testCMusr1;
        User testAOusr1;
        User testFAusr1;
        
        RecordType recOpp = [SELECT Id FROM RecordType WHERE sObjectType = 'Opportunity' AND Name = 'Research Awards'];
        RecordType recAcc = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Standard Salesforce Account'];
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        System.runAs (thisUser)
        {
            Profile p = [SELECT Id FROM Profile WHERE Name='Science Program Operations'];
            UserRole r = [SELECT Id FROM UserRole WHERE Name='Science Method Effectiveness Research Team'];
            
            testPI1usr = new User(alias = 'PI', email='PI1@pcori.com',
                                emailencodingkey='UTF-8', lastname='Lead1',
                                languagelocalekey='en_US',
                                localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
                                timezonesidkey='America/New_York',
                                username='PI1@pcori.com');
            insert testPI1usr;
            
            testPI2usr = new User(alias = 'PI2', email='PI2@pcori.com',
                emailencodingkey='UTF-8', lastname='Lead2',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
                timezonesidkey='America/New_York',
                username='PI2@pcori.com');
            insert testPI2usr;
            
            testPID1usr = new User(alias = 'PID1', email='PID1@pcori.com',
                emailencodingkey='UTF-8', lastname='Designee1',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
                timezonesidkey='America/New_York',
                username='DPI1@pcori.com');
            insert testPID1usr;
            
            testPID2usr = new User(alias = 'PID2', email='DPI2@pcori.com',
                emailencodingkey='UTF-8', lastname='Designee2',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
                timezonesidkey='America/New_York',
                username='PID2@pcori.com');
            insert testPID2usr;
            
            testCMusr = new User(alias = 'AO', email='AO@pcori.com',
                emailencodingkey='UTF-8', lastname='Admin',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
                timezonesidkey='America/New_York',
                username='AO@pcori.com');
            insert testCMusr;
            
            testAOusr = new User(alias = 'CM', email='CM@pcori.com',
                emailencodingkey='UTF-8', lastname='Manager',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
                timezonesidkey='America/New_York',
                username='CM@pcori.com');
            insert testAOusr;
            
            testFAusr = new User(alias = 'FA', email='FA@pcori.com',
                emailencodingkey='UTF-8', lastname='Finance',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
                timezonesidkey='America/New_York',
                username='FA@pcori.com');
            insert testFAusr;
            
            testPI1usr1 = new User(alias = 'PI', email='PI1@pcori1.com',
                                emailencodingkey='UTF-8', lastname='Lead1',
                                languagelocalekey='en_US',
                                localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
                                timezonesidkey='America/New_York',
                                username='PI1@pcori1.com');
            insert testPI1usr1;
            
            testPI2usr1 = new User(alias = 'PI2', email='PI2@pcori1.com',
                emailencodingkey='UTF-8', lastname='Lead2',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
                timezonesidkey='America/New_York',
                username='PI2@pcori1.com');
            insert testPI2usr1;
            
            testPID1usr1 = new User(alias = 'PID1', email='PID1@pcori1.com',
                emailencodingkey='UTF-8', lastname='Designee1',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
                timezonesidkey='America/New_York',
                username='DPI1@pcori1.com');
            insert testPID1usr1;
            
            testPID2usr1 = new User(alias = 'PID2', email='DPI2@pcori1.com',
                emailencodingkey='UTF-8', lastname='Designee2',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
                timezonesidkey='America/New_York',
                username='PID2@pcori1.com');
            insert testPID2usr1;
            
            testCMusr1 = new User(alias = 'AO', email='AO@pcori1.com',
                emailencodingkey='UTF-8', lastname='Admin',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
                timezonesidkey='America/New_York',
                username='AO@pcori1.com');
            insert testCMusr1;
            
            testAOusr1 = new User(alias = 'CM', email='CM@pcori1.com',
                emailencodingkey='UTF-8', lastname='Manager',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
                timezonesidkey='America/New_York',
                username='CM@pcori1.com');
            insert testAOusr1;
            
            testFAusr1 = new User(alias = 'FA', email='FA@pcori1.com',
                emailencodingkey='UTF-8', lastname='Finance',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
                timezonesidkey='America/New_York',
                username='FA@pcori1.com');
            insert testFAusr1;
            
            Account acc = new Account();
            acc.RecordTypeId = recAcc.Id;
            acc.Name = 'Test Acc';
            insert acc;
            
            Opportunity opp = new Opportunity();
            opp.RecordTypeId = recOpp.Id;
            opp.Awardee_Institution_Organization__c = acc.Id;
            opp.Full_Project_Title__c = 'test0';
            opp.Project_Name_off_layout__c='test0';
            opp.Name = 'Test';
            opp.CloseDate = Date.newInstance(2016, 12, 31);
            opp.Application_Number__c = '45125';
            opp.StageName = 'In Progress';
            opp.PI_Name_User__c = testPI1usr.Id;    
            opp.PI_Project_Lead_2_Name_New__c = testPI2usr.Id;
            opp.PI_Project_Lead_Designee_1_New__c = testPID1usr.Id;
            opp.PI_Project_Lead_Designee_2_Name_New__c = testPID2usr.Id;
            opp.AO_Name_User__c = testAOusr.Id;
            opp.Contract_Manager__c = testCMusr.Id;
            opp.Financial_Contact__c = testFAusr.Id;
            insert opp;
            
            Opportunity op = [SELECT AO_Name_User__c FROM Opportunity WHERE Id = :opp.Id];
            op.PI_Name_User__c = testPI1usr1.Id;    
            op.PI_Project_Lead_2_Name_New__c = testPI2usr1.Id;
            op.PI_Project_Lead_Designee_1_New__c = testPID1usr1.Id;
            op.PI_Project_Lead_Designee_2_Name_New__c = testPID2usr1.Id;
            op.AO_Name_User__c = testAOusr1.Id;
            op.Contract_Manager__c = testCMusr1.Id;
            op.Financial_Contact__c = testFAusr1.Id;
            update op;
        }
    }
}