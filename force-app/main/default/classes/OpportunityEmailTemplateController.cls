/* VisualForce Email Template Component Controller; Created for AI-1252 By Eric Viveiros Accenture SADC
   The following controller allows the use of cross referenced objects in an email template, while
   still allowing the use of a SalesForce letterhead. This controller can be used for more than one
   email template using the Opportunity object for the merge fields.
   See the VisualForce component EmailContractActivationNotification for an example of its use.
   Insure that any fields used in subsequent components are added to the opportunity query.
*/
public with sharing class OpportunityEmailTemplateController {

	public String letterHead{
		get;
		set {
			try{
				letterHead = [SELECT Value FROM BrandTemplate WHERE DeveloperName = : value].Value;
			}
			catch(Exception e) {
				throw new OpportunityEmailTemplateException('There was an error with the specified template developer name or no name was specified');
			}
		}
	}

	public Opportunity opp{get; private set;}

	public Id opportunityId{
		get;
		set {
			opportunityId = value;
			try{
				this.opp = [SELECT Name, Application_Number__c, PI_Name_User__r.Name,
								   Program_Officer__r.Name, Program_Officer__r.Title,
								   Program_Officer__r.Phone, Program_Officer__r.MobilePhone,
								   Program_Officer__r.Email, Contract_Administrator__r.Name,
								   Contract_Administrator__r.Title, Contract_Administrator__r.Phone,
								   Contract_Administrator__r.MobilePhone, Contract_Administrator__r.Email,
								   AO_Name_User__r.Name
							FROM Opportunity WHERE Id = : value];
			}
			catch (Exception e) {
				//Exception when the email template is initially pulled up in setup; It doesn't have an opportunity id until send preview is setup
			}
			finally {
				if(this.opp == null) {
					this.opp = new Opportunity();
				}
			}
		}
	}

	public String bodyCssStyle{
		get{
			return getStyle('body');
		}
	}
	
	public String headerCssStyle{
		get{
			return getStyle('header');
		}
	}

	public String accent1CssStyle{
		get{
			return getStyle('accent1');
		}
	}
	
	public String mainCssStyle{
		get{
			return getStyle('main');
		}
	}

	public String accent2CssStyle{
		get{
			return getStyle('accent2');
		}
	}

	public String footerCssStyle{
		get{
			return getStyle('footer');
		}
	}

	public String accent3CssStyle{
		get{
			return getStyle('accent3');
		}
	}

	public String headerImgSrc{
		get{
			return getImgSrc('headerImage');
		}
	}

	public String footerImgSrc{
		get{
			return getImgSrc('footerImage');
		}
	}

	public OpportunityEmailTemplateController() {

	}

	private String getStyle(String bLabel){
		String styleString = '';

		if(String.isBlank(this.letterHead)) {
			return null;
		}
		
		//get style element with bLabel attribute
		Pattern styleElementPattern = Pattern.compile('<style[^>]+bLabel\\s*=\\s*"' + bLabel + '"[^>]*>'); 
		Matcher styleMatch = styleElementPattern.matcher(this.letterHead);

		if(!styleMatch.find()) {
			return null;
		}

		styleString += this.getStyleAttribute(styleMatch.group(0),'\\b(background-color\\s*=\\s*"[^"]+)"');

		styleString += this.getStyleAttribute(styleMatch.group(0),'\\b(bEditID\\s*=\\s*"[^"]+)"');

		styleString += this.getStyleAttribute(styleMatch.group(0),'\\b(color\\s*=\\s*"[^"]+)"');

		styleString += this.getStyleAttribute(styleMatch.group(0),'\\b(vertical-align\\s*=\\s*"[^"]+)"');

		styleString += this.getStyleAttribute(styleMatch.group(0),'\\b(height\\s*=\\s*"[^"]+)"');

		styleString += this.getStyleAttribute(styleMatch.group(0),'\\b(text-align\\s*=\\s*"[^"]+)"');

		return  styleString.removeStart(' ') + ' bLabel:' + bLabel + ';';
	}

	/* gets the css formatted property for the  style attribute specified in 'regEx'
	   for the style element provided in 'searchStr'
	*/
	private String getStyleAttribute(String searchStr, string regEx) {
		Pattern p = Pattern.compile(regEx);
		Matcher pm = p.matcher(searchStr);
		if(pm.find()) {
			return ' ' + pm.group(1).replaceFirst('\\s*=\\s*"',':') + ';';
		}
		else {
			return '';
		}
	}

	private String getImgSrc(String bLabel){
		if(String.isBlank(this.letterHead)) {
			return null;
		}

		Pattern imgElementPattern = Pattern.compile('<img\\s*.+\\bbLabel="' + bLabel + '"[^>]+>[^>]+\\[\\s*([^\\]]+)\\s*\\][^>]+>\\s*(?=<\\/\\s*img\\s*>)');
		Matcher imgMatch = imgElementPattern.matcher(this.letterHead);

		if(!imgMatch.find()) {
			return null;
		}
		else {
			return URL.getSalesforceBaseUrl().toExternalForm() + imgMatch.group(1) ;
		}

	}

	public class OpportunityEmailTemplateException Extends Exception{

	}
}