global class Scheduler_class implements Schedulable{

    public static String sched = '0 00 00 * * ?';  //Every Day at Midnight 

    global static String scheduleMe() {
        string batchclassname='';
    if(Test.isRunningTest()){
        batchclassname='UpdateConstantContactAccountswithoutNames'+datetime.now();
    }
    else{
        batchclassname='UpdateConstantContactAccountswithoutNames';
    }
        Scheduler_class SC = new Scheduler_class(); 
        return System.schedule(batchclassname, sched, SC);
    }

    global void execute(SchedulableContext sc) {

        batchContactAccountNameUpdate b1 = new batchContactAccountNameUpdate();
        ID batchprocessid = Database.executeBatch(b1,50);           
    }
}