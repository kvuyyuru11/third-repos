global with sharing class CommunitiesCustomLoginController_Rsch {
    global String username {get; set;}
    global String password {get; set;}
    global CommunitiesCustomLoginController_Rsch () {}
    global PageReference login() {
        return Site.login(username, password, null); 
    } 
}