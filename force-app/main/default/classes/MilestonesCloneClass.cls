Global without sharing class  MilestonesCloneClass
{

    webService static List<FGM_Base__Benchmark__c> milestonesClone(List<String> milestoneIds)
    {
        Set<Id> mids = new set<id>();
        List<FGM_Base__Benchmark__c> midlisttoinsert = new List<FGM_Base__Benchmark__c>();
        Map<String, Schema.SobjectField> fieldMap;
        String queryFields;
        String idsString;
        String query;

        if(milestoneIds != null && !milestoneIds.isEmpty()) {

            //SELECT *
            fieldMap = Schema.SobjectType.FGM_Base__Benchmark__c.fields.getMap();
            queryFields = '';
            //build query string, don't forget the ',' at the end when using
            for(String devName : fieldMap.keySet()) {
                queryFields += devName + ', ';
            }
            //build idsString
            idsString = '';
            for(string milestoneId : milestoneIds) {
                idsString += '\'' + milestoneId + '\', ';
            }
            idsString = idsString.removeEnd(', ');
            //final query
            query = 'SELECT ' + queryFields + 'FGM_Base__Request__r.Parent_Contract__c FROM FGM_Base__Benchmark__c WHERE Id IN (' + idsString + ')';

            for(FGM_Base__Benchmark__c mil : database.query(query)) {
                FGM_Base__Benchmark__c  newmil = new FGM_Base__Benchmark__c ();
                newmil = mil.clone(false, true, false, false);

                if(mil.FGM_Base__Request__r.Parent_Contract__c != null)
                {
                    newmil.FGM_Base__Request__c = mil.FGM_Base__Request__r.Parent_Contract__c;
                }
                else
                {
                    newmil.FGM_Base__Request__c = mil.FGM_Base__Request__c;
                }
                
                midlisttoinsert.add(newmil);

            }

            if(!midlisttoinsert.isEmpty())
            {
                insert midlisttoinsert;
            }

        }

        return midlisttoinsert;
    }
}