/**
 * This class contains unit tests for validating the behavior of Apex class:SurveyAttachmentUploadController
 * 
 * Created by Himanshu Kalra  Date: 05/30/2017
 */
    @isTest
    public class TestSurveyAttachmentUploadController{
    //@Purpose: Test coverage If Attachment is there
    public static testMethod void doUploadTest(){
      Speakers_Bureau__c speakBur = new Speakers_Bureau__c();
        speakBur.Organizer_Contact_First_Name__c = 'Hari';
        speakBur.Organizer_Contact_Last_Name__c = 'Madhav'; 
        speakBur.Event_Title__c = 'Death';
        insert speakBur;

        PageReference pageref = Page.SpeakerBureauSurvey;
        Test.setCurrentPage(pageref);
        ApexPages.currentPage().getParameters().put('SBID', speakBur.id );  
        SurveyAttachmentUploadController controller = new SurveyAttachmentUploadController();

        controller = new SurveyAttachmentUploadController();

        controller.Attachment.Name = 'unit test Attachment';
        controller.Attachment.Body = Blob.valueOf('unit test Attachment Body');
        controller.upload();

        List<Attachment> attachments = [SELECT ID, Name FROM Attachment WHERE parent.id= :speakBur.id];
        System.assertEquals(1, attachments.size());
    }
    //@Purpose: Test coverage If no Attachment 
    public static testMethod void failUploadTest(){
        SurveyAttachmentUploadController controller = new SurveyAttachmentUploadController();
        PageReference pagerefC = controller.upload();
    }   
}