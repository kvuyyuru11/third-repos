/**
* @author        Abhinav Polsani 
* @date          04/25/2018
* @description   This class locks/Unlocks the P2P Tier A records based on isSubmitted field.   
Modification Log:
-----------------------------------------------------------------------------------------------------------------
Developer                Date            Description
-----------------------------------------------------------------------------------------------------------------
Abhinav Polsani       04/25/2018      Initial Version
*/
public with sharing class P2P_TierATriggerHelper {

    /**
    * @author        Abhinav Polsani
    * @date          04/25/2018
    * @description   This method checks which records need to be locked/unlocked 
    * @param         List<P2P_TierA__c> pNewList, Map<Id, P2P_TierA__c> pOldMap
    * @return        void 
    */
    public static void AutomateP2PRecords(List<P2P_TierA__c> pNewList, Map<Id, P2P_TierA__c> pOldMap) {
        if (pNewList.isEmpty() || pOldMap.isEmpty()) {
            return;
        }
        List<P2P_TierA__c> p2pLockList = new List<P2P_TierA__c>();
        List<P2P_TierA__c> p2pUnlockList = new List<P2P_TierA__c>();
        for (P2P_TierA__c p2pRec : pNewList) {
            if (pOldMap.containsKey(p2pRec.Id) && p2pRec.IsLocked__c  != pOldMap.get(p2pRec.Id).IsLocked__c ) {
                if (p2pRec.IsLocked__c  == true && LABEL.ApexRecordLockSwitch == 'True') {
                    p2pLockList.add(p2pRec);
                } else {
                    p2pUnlockList.add(p2pRec);
                }
            }
        }
        if (!p2pLockList.isEmpty()) {
            lockP2PRecords(p2pLockList);    
        }
        if (!p2pUnlockList.isEmpty()) {
            unlockP2PRecords(p2pUnlockList);
        }
    }

    /**
    * @author        Abhinav Polsani
    * @date          04/25/2018
    * @description   This method unlocks the records which were previously locked 
    * @param         List<P2P_TierA__c> pUnlockList
    * @return        void 
    */
    public static void unlockP2PRecords(List<P2P_TierA__c> pUnlockList) {
        Approval.UnLockResult[] lrList = Approval.Unlock(pUnlockList, false);
        for (Approval.UnLockResult lr : lrList) {
            if (lr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully Unlocked p2p tier a rec with ID: ' + lr.getId());
            } else {
                // Operation failed, so get all errors                
                for (Database.Error err : lr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('p2p fields that affected this error: ' + err.getFields());
                }
            }
        }
    }

    /**
    * @author        Abhinav Polsani
    * @date          04/25/2018
    * @description   This method locks the records which were previously unlocked 
    * @param         List<P2P_TierA__c> pLockList
    * @return        void 
    */
    public static void lockP2PRecords(List<P2P_TierA__c> pLockList) {
        Approval.LockResult[] lrList = Approval.lock(pLockList, false);
        for (Approval.LockResult lr : lrList) {
            if (lr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully locked p2p tier a rec with ID: ' + lr.getId());
            } else {
                // Operation failed, so get all errors                
                for (Database.Error err : lr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('p2p fields that affected this error: ' + err.getFields());
                }
            }
        }
    }
    
        }