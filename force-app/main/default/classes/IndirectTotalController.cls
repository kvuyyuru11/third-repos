public class IndirectTotalController 
{
    public boolean isBudgetLocked {get; set;}
    Public List<Budget__c> budgetItems {get; set;}
    Public Boolean noAppId{get;set;}
    public Boolean totalPrimeRow{get;set;}
    public Budget__c budgetPrime{get;set;}
    public String appId{get;set;}
    public Integer travelBudget{get;set;}
    public string PFAName;
    public Decimal Total1{get;set;}
    public Decimal Total2{get;set;}
    public Decimal Total3{get;set;}
    public Decimal Total4{get;set;}
    public Decimal Total5{get;set;}
    public Decimal Total6{get;set;}
    public Decimal Total7{get;set;}
    public Decimal Total8{get;set;}
    public Decimal Total9{get;set;}
    public Decimal Total10{get;set;}
    public Decimal Total11{get;set;}
    public Decimal Total12{get;set;}
    public Decimal Total13{get;set;}
    public list<Opportunity> OppList;
    public String YearValue{get;set;}
     
     public IndirectTotalController()
     {
         noAppId = false;
         travelBudget = 0;
          try{
        String campId=ApexPages.currentPage().getHeaders().get('Referer').substringbetween('id=','&');
        Campaign cmp=[select Name from Campaign where Id=:campId];
        PFAName=cmp.Name;
        }Catch(exception e){
        system.debug('===>'+e);
        }
      
     }
     
     public pageReference grabListDataReload()
     {      
         grabListData();
         System.debug('reloading form');
         return null;
     }
   
     public void saveBudget()
     {
         if (budgetPrime != null)
         {
             system.debug('Budget: ' + budgetPrime);
             upsert budgetPrime;
         }
         else
         {
             system.debug('Null object'); 
             //system.debug(budgetItems);
         }
         grabListDataReload();
     }
     
     public void grabListData()
     {
         
         budgetItems = [SELECT Id, Associated_App_Project__c, Name, 
                        End_Date_Year_1__c, End_Date_Year_2__c, End_Date_Year_3__c, End_Date_Year_4__c, End_Date_Year_5__c, End_Date_Year_6__c,
                        Start_Date_Year_1__c, Start_Date_Year_2__c, Start_Date_Year_3__c, Start_Date_Year_4__c, Start_Date_Year_5__c, Start_Date_Year_6__c,
                        Consultant_Costs_Total__c, Consultant_Costs_Year_1__c, Consultant_Costs_Year_2__c, Consultant_Costs_Year_3__c, 
                        Consultant_Costs_Year_4__c, Consultant_Costs_Year_5__c, Consultant_Costs_Year_6__c,
                        Direct_Subcontractor_Costs_Total__c, Direct_Subcontractor_Costs_Year_1__c, Direct_Subcontractor_Costs_Year_2__c,
                        Direct_Subcontractor_Costs_Year_3__c, Direct_Subcontractor_Costs_Year_4__c, Direct_Subcontractor_Costs_Year_5__c,
                        Direct_Subcontractor_Costs_Year_6__c,
                        Equipment_Costs_Total__c, Equipment_Costs_Year_1__c, Equipment_Costs_Year_2__c, Equipment_Costs_Year_3__c,
                        Equipment_Costs_Year_4__c, Equipment_Costs_Year_5__c, Equipment_Costs_Year_6__c,
                        Grand_Total_All_Years__c, Grand_Total_Year_1__c, Grand_Total_Year_2__c, Grand_Total_Year_3__c,
                        Grand_Total_Year_4__c, Grand_Total_Year_5__c, Grand_Total_Year_6__c,
                        Final_Grand_Total__c,
                        Other_Expenses_Total__c, Other_Expenses_Year_1__c, Other_Expenses_Year_2__c, Other_Expenses_Year_3__c, 
                        Other_Expenses_Year_4__c, Other_Expenses_Year_5__c, Other_Expenses_Year_6__c,
                        Personnel_Costs_Total__c, Personnel_Costs_Year_1__c, Personnel_Costs_Year_2__c, Personnel_Costs_Year_3__c,
                        Personnel_Costs_Year_4__c, Personnel_Costs_Year_5__c, Personnel_Costs_Year_6__c,
                        Programmatic_Travel_Costs_Total__c, Programmatic_Travel_Costs_Year_1__c, Programmatic_Travel_Costs_Year_2__c,
                        Programmatic_Travel_Costs_Year_3__c, Programmatic_Travel_Costs_Year_4__c, Programmatic_Travel_Costs_Year_5__c, 
                        Programmatic_Travel_Costs_Year_6__c,
                        Scientific_Travel_Costs_Total__c, Scientific_Travel_Costs_Year_1__c, Scientific_Travel_Costs_Year_2__c,
                        Scientific_Travel_Costs_Year_3__c, Scientific_Travel_Costs_Year_4__c, Scientific_Travel_Costs_Year_5__c,                        
                        Scientific_Travel_Costs_Year_6__c,
                        Subcontractor_Indirect_Costs_Total__c, Subcontractor_Indirect_Costs_Year_1__c, Subcontractor_Indirect_Costs_Year_2__c,
                        Subcontractor_Indirect_Costs_Year_3__c, Subcontractor_Indirect_Costs_Year_4__c, Subcontractor_Indirect_Costs_Year_5__c,                         
                        Subcontractor_Indirect_Costs_Year_6__c,
                        SubtotalDirectCosts_Total__c, Subtotal_Direct_Costs_Year_1__c, Subtotal_Direct_Costs_Year_2__c, Subtotal_Direct_Costs_Year_3__c,
                        Subtotal_Direct_Costs_Year_4__c, Subtotal_Direct_Costs_Year_5__c, Subtotal_Direct_Costs_Year_6__c,
                        Supplies_Costs_Total__c, Supplies_Costs_Year_1__c, Supplies_Costs_Year_2__c, Supplies_Costs_Year_3__c,
                        Supplies_Costs_Year_4__c, Supplies_Costs_Year_5__c, Supplies_Costs_Year_6__c,
                        Total_Direct_Costs_Total__c, Total_Direct_Costs_Year_1__c, Total_Direct_Costs_Year_2__c, Total_Direct_Costs_Year_3__c,
                        Total_Direct_Costs_Year_4__c, Total_Direct_Costs_Year_5__c, Total_Direct_Costs_Year_6__c,
                        Total_Prime_Indirect_Costs_Total__c, Total_Prime_Indirect_Costs_Year_1__c, Total_Prime_Indirect_Costs_Year_2__c,
                        Total_Prime_Indirect_Costs_Year_3__c, Total_Prime_Indirect_Costs_Year_4__c, Total_Prime_Indirect_Costs_Year_5__c, 
                        Total_Prime_Indirect_Costs_Year_6__c,
                        Applicable_Rate_Year_1__c, Applicable_Rate_Year_2__c, Applicable_Rate_Year_3__c, Applicable_Rate_Year_4__c,
                        Applicable_Rate_Year_5__c, Applicable_Rate_Year_6__c
                        FROM Budget__c WHERE Associated_App_Project__c = :appId];
                        
         System.Debug('budgetItems:: ' + budgetItems);
         
          OppList = new List<Opportunity>([Select Id,name,Campaign.Name from Opportunity where Id=:appId Limit 1]);
         if(!OppList.isempty()){
          try{
        if(OppList[0].Campaign.Name==Label.Dissemination_and_Implementation){
        YearValue = 'Year 6';
        }else{
        YearValue = 'Peer Review Period';
        }
        }Catch(exception e){
        system.debug('===>'+e);
        YearValue = 'Peer Review Period';
        }
         }
         if(budgetItems.isEmpty())
         {
         }
         else
         {
             budgetPrime = budgetItems[0];
             totalPrimeRow = true;
             
             //Instantiate variables to use them on them to calculate the totals and display them on the page
             integer i=1;
             Total1=0;
             Total2=0;
             Total3=0;
             Total4=0;
             Total5=0;
             Total6=0;
             Total7=0;
             Total8=0;
             Total9=0;
             Total10=0;
             Total11=0;
             Total12=0;
             Total13=0;
             
             //Iterate through the list of BudgetLineItem records and grab totals depending on the cost categoery and Year
             while(i<7){
             if(budgetItems[0].get('Personnel_Costs_Year_'+i+'__c')!=null){
             system.debug('here is the value'+budgetItems[0].get('Personnel_Costs_Year_'+i+'__c'));
             system.debug('here is the value'+Double.valueOf(budgetItems[0].get('Personnel_Costs_Year_'+i+'__c')));
             Total1 += Integer.valueOf(budgetItems[0].get('Personnel_Costs_Year_'+i+'__c'));
             }
             if(budgetItems[0].get('Consultant_Costs_Year_'+i+'__c')!=null){
             Total2 += Integer.valueOf(budgetItems[0].get('Consultant_Costs_Year_'+i+'__c'));
             }
             if(budgetItems[0].get('Supplies_Costs_Year_'+i+'__c')!=null){
             Total3 += Integer.valueOf(budgetItems[0].get('Supplies_Costs_Year_'+i+'__c'));
             }
             if(budgetItems[0].get('Scientific_Travel_Costs_Year_'+i+'__c')!=null){
             Total4 += Integer.valueOf(budgetItems[0].get('Scientific_Travel_Costs_Year_'+i+'__c'));
             }
             if(budgetItems[0].get('Programmatic_Travel_Costs_Year_'+i+'__c')!=null){
             Total5 += Integer.valueOf(budgetItems[0].get('Programmatic_Travel_Costs_Year_'+i+'__c'));
             }
             if(budgetItems[0].get('Programmatic_Travel_Costs_Year_'+i+'__c')!=null){
             Total6 += Integer.valueof(budgetItems[0].get('Other_Expenses_Year_'+i+'__c'));
             }
             if(budgetItems[0].get('Equipment_Costs_Year_'+i+'__c')!=null){
             Total7 += Integer.valueof(budgetItems[0].get('Equipment_Costs_Year_'+i+'__c'));
             }
             if(budgetItems[0].get('Equipment_Costs_Year_'+i+'__c')!=null){
             Total8 += Integer.valueof(budgetItems[0].get('Direct_Subcontractor_Costs_Year_'+i+'__c'));
             }
             if(budgetItems[0].get('Equipment_Costs_Year_'+i+'__c')!=null){
             Total9 += Integer.valueof(budgetItems[0].get('Subtotal_Direct_Costs_Year_'+i+'__c'));
             }
             if(budgetItems[0].get('Subcontractor_Indirect_Costs_Year_'+i+'__c')!=null){
             Total10 += Integer.valueof(budgetItems[0].get('Subcontractor_Indirect_Costs_Year_'+i+'__c'));
             }
             if(budgetItems[0].get('Total_Direct_Costs_Year_'+i+'__c')!=null){
             Total11 += Integer.valueof(budgetItems[0].get('Total_Direct_Costs_Year_'+i+'__c'));
             }
             if(budgetItems[0].get('Total_Prime_Indirect_Costs_Year_'+i+'__c')!=null){
             Total12 += Integer.valueof(budgetItems[0].get('Total_Prime_Indirect_Costs_Year_'+i+'__c'));
             }
             if(budgetItems[0].get('Grand_Total_Year_'+i+'__c')!=null){
             Total13 += Integer.valueof(budgetItems[0].get('Grand_Total_Year_'+i+'__c'));
             }
             i++;
             
             }
             
             noAppId = true;
             
             // see if this budget should be locked by looking at the opportunity's submit date field
             Opportunity theOpp = [SELECT External_Status__c FROM Opportunity where id =: appId limit 1];
             system.debug('status:' + theOpp.External_Status__c);
             if (theOpp.External_Status__c != 'Submitted')
                 isBudgetLocked = false; 
             else
                 isBudgetLocked = true;
         } 
         
         travelBudget = Integer.valueOf(budgetPrime.Scientific_Travel_Costs_Total__c);
         system.debug('Travel Budget'+travelBudget);
         system.debug('PFA name'+PFAName);
         
         //Depending on the Campaign Names associated to the Application display different error messages with different links to PDF
         if(travelBudget>10000 && PFAName==label.Symptom_Management_for_Patients_with_Advanced_Illness)
         {
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You have exceeded the scientific travel cap. Review budget guidelines. <a target="_blank" href="http://www.pcori.org/sites/default/files/PCORI-PFA-2017-Cycle-2-Targeted-Application-Guidelines.pdf">PCORI Application Guidelines</a>  <a target="_blank" href="http://www.pcori.org/sites/default/files/PCORI-Cost-Considerations-Description-of-Allowable-Direct-Costs-Under-PCORI-Award.pdf">PCORI Cost Considerations</a>'));
         }
         if(travelBudget>10000 && PFAName==label.Medication_Assisted_Treatment_MAT_Delivery_for_Pregnant_Women_with_SUD)
         {
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You have exceeded the scientific travel cap. Review budget guidelines. <a target="_blank" href="http://www.pcori.org/sites/default/files/PCORI-PFA-2017-Cycle-2-Targeted-Application-Guidelines.pdf">PCORI Application Guidelines</a>  <a target="_blank" href="http://www.pcori.org/sites/default/files/PCORI-Cost-Considerations-Description-of-Allowable-Direct-Costs-Under-PCORI-Award.pdf">PCORI Cost Considerations</a>'));
         }
         if(travelBudget>10000 && PFAName==label.Pragmatic_Clinical_Studies_to_Evaluate_Patient_Centered_Outcomes)
         {
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You have exceeded the scientific travel cap. Review budget guidelines. <a target="_blank" href="http://www.pcori.org/sites/default/files/PCORI-PFA-2017-Cycle-2-Pragmatic-Studies-Application-Guidelines.pdf">PCORI Application Guidelines</a>  <a target="_blank" href="http://www.pcori.org/sites/default/files/PCORI-Cost-Considerations-Description-of-Allowable-Direct-Costs-Under-PCORI-Award.pdf">PCORI Cost Considerations</a>'));
         }
         //if(travelBudget>10000 && PFAName==label.Dissemination_and_Implementation)
         //{
             //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You have exceeded the scientific travel cap. Review budget guidelines. <a target="_blank" href="http://www.pcori.org/sites/default/files/PCORI-PFA-2017-Cycle-2-Dissemination-Implementation-Application-Guidelines.pdf">PCORI Application Guidelines</a>  <a target="_blank" href="http://www.pcori.org/sites/default/files/PCORI-Cost-Considerations-Description-of-Allowable-Direct-Costs-Under-PCORI-Award.pdf">PCORI Cost Considerations</a>'));
         //}
         System.debug('Budget Total::: ' + budgetPrime.Scientific_Travel_Costs_Total__c);
     }
     
     
     
     public PageReference grabId()
     {
         system.debug('appId::: '+appId);
         if(appId != null && appId != '')
         {
             grabListData();
         }

         return null;
     }
     
}