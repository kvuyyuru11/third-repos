/*------------------------------------------------------------------------------------------------------------------------------------
Code Written by SADC PCORI Team: Matt Hoffman
Written During Development Phase 2: June/2016 - September/2016
--------------------------------------------------------------------------------------------------------------------------------------*/

public class Lead_MasterClass {

    public static void beforeUpdate(object[] newTrigger, object[] oldTrigger)
    {
        List<Lead> newLeadList = (List<Lead>) newTrigger;
        List<Lead> oldLeadList = (List<Lead>) oldTrigger;
        
        leadManualShareWithKP_Contacts_Update(newLeadList, oldLeadList);
        
        // NOT USED. Alternative to using Lookups. Not 100% tested
        //leadManualShareWithKP_Contacts_Update_UsingEmails(newLeadList, oldLeadList);
    }
    
    
    public static void afterInsert(object[] newTrigger)
    {
        List<Lead> newLeadList = (List<Lead>) newTrigger;
        
        leadManualShareWithKP_Contacts_Insert(newLeadList);
        
        // NOT USED. Alternative to using Lookups. Not 100% tested
        //leadManualShareWithKP_Contacts_Insert_UsingEmails(newLeadList);
    }
    
    
    public static void after(object[] newTrigger)
    {
        List<Lead> newLeadList = (List<Lead>) newTrigger;
        
        RA_LOI_ReviewCreation(newLeadList);
    }
    
    /*-----------------------------------------------------------------------------------------------------------------------------------
    Code Written at by SADC PCORI Team: Matt Hoffman
    Written During Development Phase 2: June/2016 - September/2016
    Application ID: AI-0792
    Requirement ID: RAr2_C2_010
    
    Note: This trigger is Bulkified.
    This trigger ensures that every LOI (assuming the correct LOI record type) that is in the 'Pending LOI Review' status has two LOI Reviews related to it. 
    The record types of the LOI Reviews are also set, assuming the Program field is not empty. If it is, the record types will not be set.
    ----------------------------------------------------------------------------------------------------------------------------------------*/
    static void RA_LOI_ReviewCreation(List<Lead> leadList)
    {
        ///////////////////////////////////////// Begin Trigger Setup ///////////////////////////////////
        
        List<Fluxx_LOI_Review_Form__c> insertList = new List<Fluxx_LOI_Review_Form__c>();   
         list<Id> cmpIdlst = new List<Id> (); 
        
        for(Lead ld:leadList){
           cmpIdlst.add(ld.PFA__c); 
        }
        
        //Code added October 25th, 2016 by SADC Team: Luis Rocha 
        // AI-1123: This code was updated to take in different record types dynamically. 

        //The next block of code gathers the record types for LOI Reviews, 
        // Programs, and the list of allowed review record types(In custom settings > 

        //LOI_Reviews_RT) the allowed list of record types also includes the
        // number of records to create for each record type. 
        /**** Gathering of Record Types ***/
        
        //Get all record types for the LOI Review
        List<RecordType>  recordTypeList = [SELECT id, Name, DeveloperName FROM recordType 
                                            WHERE isActive =: TRUE AND SobjectType =:  'Fluxx_LOI_Review_Form__c' LIMIT 50000];
        
        //From Matt's code in Dev 3
        Map<String, ID> ProgramString_toID = new Map<String, ID>();
        for(Schema.RecordTypeInfo recInfo : Schema.getGlobalDescribe().get('Fluxx_LOI_Review_Form__c').getDescribe().getRecordTypeInfosByName().values()){
             ProgramString_toID.put(recInfo.getName(),recInfo.getRecordTypeId());
             system.debug(recInfo.getName() + ', ' + recInfo.getRecordTypeId());
        }
        
        //Get a list of all programs
        List<Campaign> cmpgnList = [SELECT Id, LOI_Record_Type_Name__c ,Program__r.Name, Name FROM Campaign where Id IN:cmpIdlst];
        
        //Get list of allowed LOI Reviews recordTypes (In custom settings > LOI_Reviews_RT__c). 
        //Map contains (Name, Number of Record types to create). 
        List<LOI_Reviews_RT__c> rtAllowedList = new List<LOI_Reviews_RT__c>();
        rtAllowedList = LOI_Reviews_RT__c.getall().values();
        Map<String,Integer> rtAllowedMap = new Map<String,Integer>();
        for(LOI_Reviews_RT__c lst: rtAllowedList)
        {
            rtAllowedMap.put(lst.Name, (Integer) lst.number_of_loi_reviews_to_create__c);
        } //This is a map so to use the .contains function
       /**** End: Gathering of Record Types ***/ 

        // get all flux reviews and put in a map. only need 1 per LOI
        List<Fluxx_LOI_Review_Form__c> allReviewsList = [SELECT id, Lead_LOI__c FROM Fluxx_LOI_Review_Form__c where Lead_LOI__c IN : leadList LIMIT 50000];
        Map<Id, Id> reviewsMap = new Map<Id, Id>();
        for (Fluxx_LOI_Review_Form__c review : allReviewsList) 
        {
            reviewsMap.put(review.Lead_LOI__c, review.Id);
        }
        //System.debug('list size: ' + allReviewsList.size());   
        ///////////////////////////////////////// End Trigger Setup ////////////////////////////////////
        ///////////////////////////////////////// Begin Trigger Loop ///////////////////////////////////
        
        for (Lead theLead : leadList)
        {
            //System.debug('status: ' + theLead.Status);
            //System.debug('record type: ' + theLead.RecordTypeDevName__c);
            if ((theLead.Status == 'Pending LOI Review') && (theLead.RecordTypeDevName__c == 'RA_LOI' || theLead.RecordTypeDevName__c == 'RA_LOI_Methods'))
            {   
                boolean needsReviewsAssigned = true;
                if (Trigger.isUpdate)
                {
                    if (reviewsMap.containsKey(theLead.id)){needsReviewsAssigned = false;}
                }
                
                // if this Lead has no reviews, then add 2 reaccviews of the right record type.
                // use program field to get record type for reviews. if empty, no record type is set for the reviews.
                if (needsReviewsAssigned)
                {
                    //Code update: AI-1123
                    String idOfLOIReviewRecordType;
                    //Get the record's type id.
                    ////If programList is empty then return the the 'Master' record type's ID
                    if (cmpgnList.isEmpty()){idOfLOIReviewRecordType  = ProgramString_toID.get('Master'); }
                    else
                    {
                        idOfLOIReviewRecordType  = getProgramName(theLead.Program__c,cmpgnList);
                    }
                    
                    String idOfRecordType = getRecordTypeId(idOfLOIReviewRecordType, recordTypeList);
                    if(rtAllowedMap.containsKey(idOfLOIReviewRecordType))
                    {    
                        Integer number_reviews_to_create = rtAllowedMap.get(idOfLOIReviewRecordType);
                        //iterate and create review records and add them to the insert list.   
                        for(Integer index = 0; index < number_reviews_to_create; index++)
                        {
                            insertList.add(createReviewRecord( idOfRecordType, theLead.id));
                        }
                        //insertList.add(createReviewRecord( idOfRecordType, theLead.id));
                    }
                }
                else 
                {
                    //System.debug('No reviews needed to be created');
                }
            }
            ///////////////////////////////////////// End Trigger Loop ///////////////////////////////////
        }
        insert insertList;
    }
    
    //*************************************************************************************
    // GetRecordTypeId by SADC Team : Luis Rocha
    // Params: RecordName the name of the record type, rtList contains list of all record types
    // Parses through list and returns the id of the matching record type name. If no record
    // types are found with that name the code will return the first record type on the list. 
    //*************************************************************************************/
    static string getRecordTypeId(String recordName, List<RecordType> rtList)
    {
        for(RecordType rt : rtList)
        {
            if(recordName == rt.DeveloperName)
            {
                return rt.id;
            }
        }
        return rtList[0].id;
    }
    //************************************************************************************
    // GetProgramName by SADC Team : Luis Rocha
    // Searches the list of programs and returns the correct program that has the matching
    // record type name. 
    static string getProgramName(String rn, List<campaign> pl)
    {
        for(campaign prog : pl)
        {
            if(rn == prog.Program__r.Name)
            {
                String matchedName = prog.LOI_Record_Type_Name__c;
                return matchedName; //Returns name i.e. (Addressing Disperities)
            }
        }
        //If the list is empty return null
        if(pl.isEmpty()) { return null; }
        //OTherwise return first on the list. 
        else { return pl[0].id; }       
    }
    
    static Fluxx_LOI_Review_Form__c createReviewRecord (Id recordTypeID, ID leadID)
    {
        Fluxx_LOI_Review_Form__c review = new Fluxx_LOI_Review_Form__c();
        review.Lead_LOI__c = leadID;   
        if (!String.isBlank(recordTypeID))
            review.RecordTypeID = recordTypeID;

        return review;
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    /*--------------------------------------------------------------------------------------------------------
    Code Written at by SADC PCORI Team: Matt Hoffman
    Written During Development Phase 2: June/2016 - September/2016
    Application ID: AI-0615
    Requirement ID: RAr2_C1_007
    
    Note: LeadManualShareWithKP_Update is the update version of this trigger and is much more complex. Both 
    versions are bulkified.
    This trigger shares an LOI/Lead with up to 5 users in the Contacts/Key Personnel section, such as the PI, 
    AO, D1, D2, and FO using the LeadShare object.
    ----------------------------------------------------------------------------------------------------------*/
    
    static void leadManualShareWithKP_Contacts_Insert(List<Lead> newLeadList)
    {
        List<LeadShare> insertList = new List<LeadShare>();
    
        for (Lead theLead : newLeadList)
        {
            if (theLead.RecordTypeDevName__c == 'RA_LOI' || theLead.RecordTypeDevName__c == 'RA_LOI_Methods' || theLead.RecordTypeDevName__c == 'FoundationConnect_Portal' )
            {        
                Map<Id, LeadShare> mapOfKP_toLeadShareID_thisLead = new Map<Id, LeadShare>();
                
                // PI
                if (!String.isBlank(theLead.Principal_Investigator__c))
                {
                    if ((!mapOfKP_toLeadShareID_thisLead.containsKey(theLead.Principal_Investigator__c)) && (theLead.Principal_Investigator__c != theLead.ownerid))
                    {
                        LeadShare ls = new LeadShare();
                        ls = JobSharing.manualShareRead(theLead.id, theLead.Principal_Investigator__c);
                        if (ls != null)
                        {
                            insertList.add(ls);
                            mapOfKP_toLeadShareID_thisLead.put(theLead.Principal_Investigator__c, ls);
                        }
                    }
                }
                
                // AO
                if (!String.isBlank(theLead.Administrative_Official__c))
                {
                    if ((!mapOfKP_toLeadShareID_thisLead.containsKey(theLead.Administrative_Official__c)) && (theLead.Administrative_Official__c != theLead.ownerid))
                    {
                        LeadShare ls = new LeadShare();
                        ls = JobSharing.manualShareRead(theLead.id, theLead.Administrative_Official__c);
                        if (ls != null)
                        {
                            insertList.add(ls);
                            mapOfKP_toLeadShareID_thisLead.put(theLead.Administrative_Official__c, ls);
                        }
                    }
                }
                
                // PI Designee 1
                if (!String.isBlank(theLead.PI_Designee_1__c))
                {
                    if ((!mapOfKP_toLeadShareID_thisLead.containsKey(theLead.PI_Designee_1__c)) && (theLead.PI_Designee_1__c != theLead.ownerid))
                    {
                        LeadShare ls = new LeadShare();
                        ls = JobSharing.manualShareRead(theLead.id, theLead.PI_Designee_1__c);
                        if (ls != null)
                        {
                            insertList.add(ls);
                            mapOfKP_toLeadShareID_thisLead.put(theLead.PI_Designee_1__c, ls);
                        }
                    }
                }
                
                
                // PI Designee 2
                if (!String.isBlank(theLead.PI_Designee_2__c))
                {
                    if ((!mapOfKP_toLeadShareID_thisLead.containsKey(theLead.PI_Designee_2__c)) && (theLead.PI_Designee_2__c != theLead.ownerid))
                    {
                        LeadShare ls = new LeadShare();
                        ls = JobSharing.manualShareRead(theLead.id, theLead.PI_Designee_2__c);
                        if (ls != null)
                        {
                            insertList.add(ls);
                            mapOfKP_toLeadShareID_thisLead.put(theLead.PI_Designee_2__c, ls);
                        }
                    }
                }   
                
                // Financial Officer
                if (!String.isBlank(theLead.Financial_Officer__c))
                {
                    if ((!mapOfKP_toLeadShareID_thisLead.containsKey(theLead.Financial_Officer__c)) && (theLead.Financial_Officer__c != theLead.ownerid))
                    {
                        LeadShare ls = new LeadShare();
                        ls = JobSharing.manualShareRead(theLead.id, theLead.Financial_Officer__c);
                        if (ls != null)
                        {
                            insertList.add(ls);
                            mapOfKP_toLeadShareID_thisLead.put(theLead.Financial_Officer__c, ls);
                        }
                    }
                } 
            }
        }
        insert insertList;
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /*----------------------------------------------------------------------------------------------------------------------------------------
    Code Written at by SADC PCORI Team: Matt Hoffman
    Written During Development Phase 2: June/2016 - September/2016
    Application ID: AI-0615
    Requirement ID: RAr2_C1_007
    
    Note: LeadManualShareWithKP is the insert version of this trigger. Both versions are bulkified.
    This trigger shares an LOI/Lead with up to 5 users in the Contacts/Key Personnel section, such as the PI, AO, D1, D2, and FO using the LeadShare object.
    This trigger is complex since it needs to be bulkified and because of some unusual use-cases that could cause sharing inconsistencies. 
    For example, if the same user is entered into two or more of the fields, we have to check before trying to add a new LeadShare record. Also, when we
    go to remove a rule, we have to make sure this user is not in one of the other fields. In addition, we have to deal with the scenario of users moving around 
    between the different fields with difference circumstances. 
    To accomplish this, we use a couple of 2-D maps-- One that serves as a "local database" so that we can keep track of who is gaining/loseing LeadShare 
    privledges, and another map that holds trigger.old info since trigger.new will need to look at it.
    ---------------------------------------------------------------------------------------------------------------------------------------*/
    
    static void leadManualShareWithKP_Contacts_Update(List<Lead> newLeadList, List<Lead> oldLeadList)
    {
        ///////////////////////////////////////// Begin Trigger Setup ///////////////////////////////////
    
        // Map used for the old values of the record before the update was requested
        Map<Id, Map<string, Id>> oldTriggerMap = new Map<Id, Map<string, Id>>();
        // Lead Id, ao/pi/fo navigation-key, ao/pi/fo user ID
        
        // Main map. serves as the temp database of LeadShares. changes during runetime
        Map<Id, Map<Id, LeadShare>> leadShareMap = new Map<Id, Map<Id, LeadShare>>();
        // Lead Id, user/group Id, LeadShare (null if there isnt one)
        
        List<LeadShare> deleteList = new List<LeadShare>();
        
        Map<Id, Map<Id, LeadShare>> insertMap = new Map<Id, Map<Id, LeadShare>>();
        // Lead Id, userOrGroup Id, LeadShare
        List<LeadShare> insertList = new List<LeadShare>();
        
        
        List<LeadShare> allCurrentLeadShares = [SELECT id, userorgroupid, leadid, RowCause FROM LeadShare where (LeadId IN : newLeadList OR LeadId IN : newLeadList) ORDER BY LeadId ASC LIMIT 50000];
        for (LeadShare theShareRule : allCurrentLeadShares)
        {
            Map<Id, LeadShare> mapOfKP_thisLead;
            if (leadShareMap.containsKey(theShareRule.LeadId))
            {
                mapOfKP_thisLead = new Map<Id, LeadShare>(leadShareMap.get(theShareRule.LeadId));
            }
            else
            {
                mapOfKP_thisLead = new Map<Id, LeadShare>();
            }
            
            // make sure the lead doesn't already have this user as shared/about to be shared.
            // this is for the purpose of ensuring that multiple rules aren't added if the same user is entered twice.
            if (!mapOfKP_thisLead.containsKey(theShareRule.UserOrGroupId)) 
            {
                mapOfKP_thisLead.put(theShareRule.UserOrGroupId, theShareRule);
                leadShareMap.put(theShareRule.LeadId, mapOfKP_thisLead);
            }        
        }
        
        // get all the old id's of the key personnel
        for (Lead theLead : oldLeadList)
        { 
            // get trigger.old fields
            Map <string, Id> mapOfKeyPersonnel = new Map <string, Id>();
          
            mapOfKeyPersonnel.put('old_ao_userID', theLead.Principal_Investigator__c);
            mapOfKeyPersonnel.put('old_pi_userID', theLead.Administrative_Official__c);
            mapOfKeyPersonnel.put('old_d1_userID', theLead.PI_Designee_1__c);
            mapOfKeyPersonnel.put('old_d2_userID', theLead.PI_Designee_2__c);
            mapOfKeyPersonnel.put('old_fo_userID', theLead.Financial_Officer__c);
            
            oldTriggerMap.put(theLead.Id, mapOfKeyPersonnel);
        }
        
        ///////////////////////////////////////// End Trigger Setup ////////////////////////////////////
        ///////////////////////////////////////// Begin Trigger Loop ///////////////////////////////////
        
        for (Lead theLead : newLeadList)
        {
            if (theLead.RecordTypeDevName__c == 'RA_LOI' || theLead.RecordTypeDevName__c == 'RA_LOI_Methods' || theLead.RecordTypeDevName__c == 'FoundationConnect_Portal')
            {
                // get values trying to be saved
                Id newPI_id = theLead.Principal_Investigator__c;
                Id newAO_id = theLead.Administrative_Official__c;
                Id newD1_id = theLead.PI_Designee_1__c;
                Id newD2_id = theLead.PI_Designee_2__c;
                Id newFO_id = theLead.Financial_Officer__c  ;
                
                Map<string, Id> oldLeadMap = new Map<string, Id>(oldTriggerMap.get(theLead.Id));
                //oldLeadMap = mainMap.get(theLead.Id);
                
                Id oldPI_id = oldLeadMap.get('old_ao_userID');
                Id oldAO_id = oldLeadMap.get('old_pi_userID');
                Id oldD1_id = oldLeadMap.get('old_d1_userID');
                Id oldD2_id = oldLeadMap.get('old_d2_userID');
                Id oldFO_id = oldLeadMap.get('old_fo_userID');
                
                // get the LeadShare rules for this Lead
                Map<Id, LeadShare> mapOfKP_toLeadShareID_thisLead;
                if (leadShareMap.containsKey(theLead.Id))
                {
                    mapOfKP_toLeadShareID_thisLead = new Map<Id, LeadShare>(leadShareMap.get(theLead.id));
                }
                else
                {
                    mapOfKP_toLeadShareID_thisLead = new Map<Id, LeadShare>();
                }
                
                // below, we see if the old KP is not equal to the old KP AND make sure they are not null. If so, this means this field changed and we need to
                // delete the old manual sharing and create a new one.
                
                
                // PI ///////////////////////////////////////////////////////////////////////////////////////////////
                // see if we need to do anything. If they are the same, do nothing.
                system.debug('here is the new PI '+newPI_id+' here is the old PI '+oldPI_id);
                if (newPI_id != oldPI_id)
                {
                    LeadShare theLeadShare = Null;
                    
                    // see if there is was a user is this spot previously or if its the first time being filled. We need to remove the previous person's access.
                    if (oldPI_id != Null)
                    {
                        // get id of manual sharing record
                        if (mapOfKP_toLeadShareID_thisLead.containsKey(oldPI_id)) 
                        {
                            // this is the current record for this user's access to this particular lead
                            theLeadShare = mapOfKP_toLeadShareID_thisLead.get(oldPI_id);
                        }
                    }
                    
                    // see if we need to remove the sharing record
                    if (theLeadShare != Null)
                    {
                        // make sure that this is not the Owner rule
                        if (theLeadShare.RowCause != 'Owner')
                        {
                            // before we remove the rule, we have to make sure that this user is not in another Key Personnel field
                            if ((oldPI_id != oldAO_id) && (oldPI_id != oldD1_id) && (oldPI_id != oldD2_id) && (oldPI_id != oldFO_id))
                            {
                                //remove this record (add it to the delete list) AND remove it from the main map.
                                deleteList.add(theLeadShare);
                                mapOfKP_toLeadShareID_thisLead.remove(oldPI_id);
                            } 
                        }                           
                    }
                    
                    // create the new manual sharing record
                    if (newPI_id != Null)
                    {
                        LeadShare tempLeadShare = new LeadShare();
                        
                        // call static function that returns a LeadShare record by passing in the LeadID and the user/group ID
                        tempLeadShare = JobSharing.manualShareRead(theLead.id, newPI_id);
                        
                        // before we put this record into the potential insert list and main map, make sure this user doesnt already 
                        // have access to this lead. We have to do this so that we don't give the same user multiple share records for the same lead
                        if (!mapOfKP_toLeadShareID_thisLead.containsKey(newPI_id) && tempLeadShare != Null)
                        {
                            mapOfKP_toLeadShareID_thisLead.put(newPI_id, tempLeadShare);
                            insertList.add(tempLeadShare);
                        }                               
                    }  
                    // Update the main map
                    //leadShareMap.put(theLead.Id, mapOfKP_toLeadShareID_thisLead);
                }
                
                
                // AO /////////////////////////////////////////////////////////////////////////////////////////////////////
                // see if we need to do anything. If they are the same, do nothing.
                if (newAO_id != oldAO_id)
                {
                    LeadShare theLeadShare = Null;
                    
                    // see if there is was a user is this spot previously or if its the first time being filled. We need to remove the previous person's access.
                    if (oldAO_id != Null)
                    {
                        // get id of manual sharing record
                        if (mapOfKP_toLeadShareID_thisLead.containsKey(oldAO_id)) 
                        {
                            // this is the current record for this user's access to this particular lead
                            theLeadShare = mapOfKP_toLeadShareID_thisLead.get(oldAO_id);
                        }
                    }
                    
                    // see if we need to remove the sharing record
                    if (theLeadShare != Null)
                    {
                        // make sure that this is not the Owner rule
                        if (theLeadShare.RowCause != 'Owner')
                        {
                            // before we remove the rule, we have to make sure that this user is not in another Key Personnel field
                            if ((oldAO_id != newPI_id) && (oldAO_id != oldD1_id) && (oldAO_id != oldD2_id) && (oldAO_id != oldFO_id))
                            {
                                //remove this record (add it to the delete list) AND remove it from the main map.
                                deleteList.add(theLeadShare);
                                mapOfKP_toLeadShareID_thisLead.remove(oldAO_id);
                            } 
                        }                           
                    }
                    
                    // create the new manual sharing record
                    if (newAO_id != Null)
                    {
                        LeadShare tempLeadShare = new LeadShare();
                        
                        // call static function that returns a LeadShare record by passing in the LeadID and the user/group ID
                        tempLeadShare = JobSharing.manualShareRead(theLead.id, newAO_id);
                        
                        // before we put this record into the potential insert list and main map, make sure this user doesnt already 
                        // have access to this lead. We have to do this so that we don't give the same user multiple share records for the same lead
                        if (!mapOfKP_toLeadShareID_thisLead.containsKey(newAO_id) && tempLeadShare != Null)
                        {
                            mapOfKP_toLeadShareID_thisLead.put(newAO_id, tempLeadShare);
                            insertList.add(tempLeadShare);
                        }                               
                    }  
                    // Update the main map
                    //leadShareMap.put(theLead.Id, mapOfKP_toLeadShareID_thisLead);
                }
        
                
                // D1 /////////////////////////////////////////////////////////////////////////////////////////////////////
                // see if we need to do anything. If they are the same, do nothing.
                if (newD1_id != oldD1_id)
                {
                    LeadShare theLeadShare = Null;
                    
                    // see if there is was a user is this spot previously or if its the first time being filled. We need to remove the previous person's access.
                    if (oldD1_id != Null)
                    {
                        // get id of manual sharing record
                        if (mapOfKP_toLeadShareID_thisLead.containsKey(oldD1_id)) 
                        {
                            // this is the current record for this user's access to this particular lead
                            theLeadShare = mapOfKP_toLeadShareID_thisLead.get(oldD1_id);
                        }
                    }
                    
                    // see if we need to remove the sharing record
                    if (theLeadShare != Null)
                    {
                        // make sure that this is not the Owner rule
                        if (theLeadShare.RowCause != 'Owner')
                        {
                            // before we remove the rule, we have to make sure that this user is not in another Key Personnel field
                            if ((oldD1_id != newPI_id) && (oldD1_id != newAO_id) && (oldD1_id != oldD2_id) && (oldD1_id != oldFO_id))
                            {
                                //remove this record (add it to the delete list) AND remove it from the main map.
                                deleteList.add(theLeadShare);
                                mapOfKP_toLeadShareID_thisLead.remove(oldD1_id);
                            } 
                        }                           
                    }
                    
                    // create the new manual sharing record
                    if (newD1_id != Null)
                    {
                        LeadShare tempLeadShare = new LeadShare();
                        
                        // call static function that returns a LeadShare record by passing in the LeadID and the user/group ID
                        tempLeadShare = JobSharing.manualShareRead(theLead.id, newD1_id);
                        
                        // before we put this record into the potential insert list and main map, make sure this user doesnt already 
                        // have access to this lead. We have to do this so that we don't give the same user multiple share records for the same lead
                        if (!mapOfKP_toLeadShareID_thisLead.containsKey(newD1_id) && tempLeadShare != Null)
                        {
                            mapOfKP_toLeadShareID_thisLead.put(newD1_id, tempLeadShare);
                            insertList.add(tempLeadShare);
                        }                               
                    }  
                    // Update the main map
                    //leadShareMap.put(theLead.Id, mapOfKP_toLeadShareID_thisLead);
                }
                
                
                // D2 /////////////////////////////////////////////////////////////////////////////////////////////////////
                // see if we need to do anything. If they are the same, do nothing.
                if (newD2_id != oldD2_id)
                {
                    LeadShare theLeadShare = Null;
                    
                    // see if there is was a user is this spot previously or if its the first time being filled. We need to remove the previous person's access.
                    if (oldD2_id != Null)
                    {
                        // get id of manual sharing record
                        if (mapOfKP_toLeadShareID_thisLead.containsKey(oldD2_id)) 
                        {
                            // this is the current record for this user's access to this particular lead
                            theLeadShare = mapOfKP_toLeadShareID_thisLead.get(oldD2_id);
                        }
                    }
                    
                    // see if we need to remove the sharing record
                    if (theLeadShare != Null)
                    {
                        // make sure that this is not the Owner rule
                        if (theLeadShare.RowCause != 'Owner')
                        {
                            // before we remove the rule, we have to make sure that this user is not in another Key Personnel field
                            if ((oldD2_id != newPI_id) && (oldD2_id != newAO_id) && (oldD2_id != newD1_id) && (oldD2_id != oldFO_id))
                            {
                                //remove this record (add it to the delete list) AND remove it from the main map.
                                deleteList.add(theLeadShare);
                                mapOfKP_toLeadShareID_thisLead.remove(oldD2_id);
                            } 
                        }                           
                    }
                    
                    // create the new manual sharing record
                    if (newD2_id != Null)
                    {
                        LeadShare tempLeadShare = new LeadShare();
                        
                        // call static function that returns a LeadShare record by passing in the LeadID and the user/group ID
                        tempLeadShare = JobSharing.manualShareRead(theLead.id, newD2_id);
                        
                        // before we put this record into the potential insert list and main map, make sure this user doesnt already 
                        // have access to this lead. We have to do this so that we don't give the same user multiple share records for the same lead
                        if (!mapOfKP_toLeadShareID_thisLead.containsKey(newD2_id) && tempLeadShare != Null)
                        {
                            mapOfKP_toLeadShareID_thisLead.put(newD2_id, tempLeadShare);
                            insertList.add(tempLeadShare);
                        }                               
                    }  
                    // Update the main map
                    //leadShareMap.put(theLead.Id, mapOfKP_toLeadShareID_thisLead);
                }
                
                
                // Financial Officer ///////////////////////////////////////////////////////////////////////////////////////////////
                // see if we need to do anything. If they are the same, do nothing.
                if (newFO_id != oldFO_id)
                {
                    LeadShare theLeadShare = Null;
                    
                    // see if there is was a user is this spot previously or if its the first time being filled. We need to remove the previous person's access.
                    if (oldFO_id != Null)
                    {
                        // get id of manual sharing record
                        if (mapOfKP_toLeadShareID_thisLead.containsKey(oldFO_id)) 
                        {
                            // this is the current record for this user's access to this particular lead
                            theLeadShare = mapOfKP_toLeadShareID_thisLead.get(oldFO_id);
                        }
                    }
                    
                    // see if we need to remove the sharing record
                    if (theLeadShare != Null)
                    {
                        // make sure that this is not the Owner rule
                        if (theLeadShare.RowCause != 'Owner')
                        {
                            // before we remove the rule, we have to make sure that this user is not in another Key Personnel field
                            if ((oldFO_id != newPI_id) && (oldFO_id != newAO_id) && (oldFO_id != newD1_id) && (oldFO_id != newD2_id))
                            {
                                //remove this record (add it to the delete list) AND remove it from the main map.
                                deleteList.add(theLeadShare);
                                mapOfKP_toLeadShareID_thisLead.remove(oldFO_id);
                            } 
                        }                           
                    }
                    
                    // create the new manual sharing record
                    if (newFO_id != Null)
                    {
                        LeadShare tempLeadShare = new LeadShare();
                        
                        // call static function that returns a LeadShare record by passing in the LeadID and the user/group ID
                        tempLeadShare = JobSharing.manualShareRead(theLead.id, newFO_id);
                        
                        // before we put this record into the potential insert list and main map, make sure this user doesnt already 
                        // have access to this lead. We have to do this so that we don't give the same user multiple share records for the same lead
                        if (!mapOfKP_toLeadShareID_thisLead.containsKey(newFO_id) && tempLeadShare != Null)
                        {
                            mapOfKP_toLeadShareID_thisLead.put(newFO_id, tempLeadShare);
                            insertList.add(tempLeadShare);
                        }                               
                    }  
                    // Update the main map
                    //leadShareMap.put(theLead.Id, mapOfKP_toLeadShareID_thisLead);
                }       
                leadShareMap.put(theLead.Id, mapOfKP_toLeadShareID_thisLead);
            }
        ///////////////////////////////////////// End Trigger Loop ///////////////////////////////////
        }
        delete deleteList;
        insert insertList;
    }     
}