/*
* Apex Class : PIRRelatedListPageControllerTestClass
* Author : Kalyan Vuyyuru (PCORI)
* Version History : 1.0
* Description : This is a class that is writen to test the PIRRelatedListPageController to ensure all the PIR's with Letter Closed status will be returned in this class.
*/
@IsTest
public class PIRRelatedListPageControllerTestClass {
    
    public static testmethod void pirrltestmethod(){
        list<opportunity> oplist= TestDataFactory.getopportunitytestrecordstotestresearchawardssharing(1, 7, 1);
        PIR__c p= new PIR__c();
        p.Application__c=oplist[0].Id;
        p.PIR_Response_Deadline__c=system.today();
        p.PIR_Status__c='draft';
        insert p;
        p.PIR_Status__c='Letter Closed';
        update p;
        
        PIRRelatedListPageController prpc= new PIRRelatedListPageController(new ApexPages.StandardController(oplist[0]));
        Test.startTest();
        prpc.recid=oplist[0].Id;
        prpc.getPIRS();
system.assertEquals(prpc.getPIRS().size(), 1);
        Test.stopTest();
    }

}