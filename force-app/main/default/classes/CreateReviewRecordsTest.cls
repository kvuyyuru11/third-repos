@isTest
public class CreateReviewRecordsTest {
    private static testMethod void FluxxLOIReviewsTest(){
        Set<Id> LOIs = new Set<Id>();
        List<Fluxx_LOI__c> fllst = new List<Fluxx_LOI__c>();
        Fluxx_LOI__c fl = new Fluxx_LOI__c();
        fl.Fluxx_Request_Id__c = 'Test1';
        fl.PFA_Type__c = 'Broad';
        fl.PFA__c = 'AD';
        fllst.add(fl);

        Fluxx_LOI__c f1 = new Fluxx_LOI__c();
        f1.Fluxx_Request_Id__c = 'Test2';
        f1.PFA_Type__c = 'Pragmatic';
        f1.PFA__c = 'AD';
        fllst.add(f1);
        
        Fluxx_LOI__c f2 = new Fluxx_LOI__c();
        f2.Fluxx_Request_Id__c = 'Test3';
        f2.PFA_Type__c = 'Targeted';
        f2.PFA__c = 'APDTO';
        fllst.add(f2);
        
        Fluxx_LOI__c f3 = new Fluxx_LOI__c();
        f3.Fluxx_Request_Id__c = 'Test4';
        f3.PFA_Type__c = 'Targeted';
        f3.PFA__c = 'CDR';
        fllst.add(f3);
        
        Fluxx_LOI__c f4 = new Fluxx_LOI__c();
        f4.Fluxx_Request_Id__c = 'Test5';
        f4.PFA_Type__c = 'Targeted';
        f4.PFA__c = 'IHS';
        fllst.add(f4);
        
        Fluxx_LOI__c f5 = new Fluxx_LOI__c();
        f5.Fluxx_Request_Id__c = 'Test5';
        f5.PFA_Type__c = 'Targeted';
        f5.PFA__c = 'AD';
        fllst.add(f5);
        
        insert fllst;
        
        for(Fluxx_LOI__c f :fllst){
            LOIs.add(f.Id);
        }
        CreateReviewRecords.FluxxLOIReviews(LOIs);
    }
}