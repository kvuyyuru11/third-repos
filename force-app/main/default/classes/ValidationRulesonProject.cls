//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//This is developed by SADC team - Sunitha Maddala//////////////////////////////////////////////
//Application Inventory # AI-1219, Client Requirement ID RAr3_C2_015B, Requirement  17235///////
//Upon status change to ‘Executed’ validation rules put in place to require Contract specific fields. 
//Code updated with custom labels for all hardcoded values on 03/06/2017 by Elizabeth Bryant
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Public With Sharing Class ValidationRulesonProject{
    
    public  ValidationRulesonProject() {}
    public static List<Contract_Start_Date__mdt> lstdates = [Select Contract_Start_Date__c FROM Contract_Start_Date__mdt];
      Id MouParentrcdtypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('MOU').getRecordTypeId();
      Id MourelatedrcdtypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('MOU - Related Project').getRecordTypeId();

    Public void ValidationRules(List<Opportunity> OppList)
    {
        set<Id> OpportunityIds = new set<Id>();
        for(Opportunity oppids: opplist)
        {
            OpportunityIds.add(oppids.id);    
        }
        
        for(Opportunity opp: OppList)
        {
            if(opp.StageName.equals(Label.Executed)&& opp.Project_Start_Date__c >= lstdates[0].Contract_Start_Date__c && opp.recordtypeId!=MouParentrcdtypeId && opp.recordtypeId!=MourelatedrcdtypeId )
            {
                if(opp.Full_Project_Title__c == null)
                { 
                    opp.Full_Project_Title__c.adderror(Label.Contract_Related_Field_Error);
                }
                if(opp.Project_Start_Date__c == null)
                { 
                    opp.Project_Start_Date__c.adderror(Label.Contract_Related_Field_Error);
                } 
                if(opp.Project_End_Date__c == null)
                { 
                    opp.Project_End_Date__c.adderror(Label.Contract_Related_Field_Error);
                }
                if(opp.Program_Officer__c == null)
                { 
                    opp.Program_Officer__c.adderror(Label.Contract_Related_Field_Error);
                }
                if(opp.Contract_Administrator__c == null)
                { 
                    opp.Contract_Administrator__c.adderror(Label.Contract_Related_Field_Error);
                }
                if(opp.AO_Name_User__c == null)
                { 
                    opp.AO_Name_User__c.adderror(Label.Contract_Related_Field_Error);
                }
                if(opp.Contracted_Budget__c == null)
                { 
                    opp.Contracted_Budget__c.adderror(Label.Contract_Related_Field_Error);
                }
                if(opp.Contract_Execution_Date__c == null)
                { 
                    opp.Contract_Execution_Date__c.adderror(Label.Contract_Related_Field_Error);
                }
                if(opp.FGM_Base__Award_Date__c == null)
                { 
                    opp.FGM_Base__Award_Date__c.adderror(Label.Contract_Related_Field_Error);
                }
                //Update made as per Zendesk Ticket 139053 by Pradeep Bokkala
                if(opp.Program__c ==null)
                {
                    opp.Program__c.adderror(Label.Contract_Related_Field_Error);
                }
            }
        }
    }
}