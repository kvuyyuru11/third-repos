@IsTest
public without sharing class CommunitiesSelfRegControllerTest {
    
    public static testmethod void testmethod1(){
        Test.startTest();
        CommunitiesSelfRegController c= new CommunitiesSelfRegController();
        c.getCheckemail();
        c.firstName='Test User';
        c.lastName='Last Test';
        c.email='Testuserlastuser'+system.now()+'@yopmail.com';
        c.communityNickname='Testuserk';
        c.password='pcori1234';
        c.confirmPassword='pcori123';
        c.checkbox01=false;
        c.registerUser();
        test.stopTest();
    }
    
    public static testmethod void testmethod2(){
        Test.startTest();
        CommunitiesSelfRegController c= new CommunitiesSelfRegController();
        c.getCheckemail();
        c.firstName='Test User';
        c.lastName='Last Test';
        c.email='Testuserlastuser'+system.now()+'@yopmail.com';
        c.communityNickname='Tesla';
        c.password='pcori123';
        c.confirmPassword='pcori123';
        c.checkbox01=true;
        c.registerUser();
        test.stopTest();
    }
    
    public static testmethod void testmethod3(){
        Test.startTest();
        CommunitiesSelfRegController c= new CommunitiesSelfRegController();
        c.getCheckemail();
        c.firstName='Test User';
        c.lastName='Last Test';
        c.email='Testuserlastuser2'+system.now()+'@yopmail.com';
        c.communityNickname='Testuserlastuser2'+system.now()+'@yopmail.com';
        c.password='pcori123';
        c.confirmPassword='pcori123';
        c.checkbox01=true;
        c.registerUser();
        test.stopTest();
    }
    
    public static testmethod void testmethod4(){
        Test.startTest();
        Account a = new Account(Name='Test Big Co');
        Insert a;
        
        Contact cnt = new Contact(LastName='Last Test', FirstName='Test User', Email='Testuserlastuser2@yopmail.com', AccountId=a.Id);
        Insert cnt;
        CommunitiesSelfRegController c= new CommunitiesSelfRegController();
        c.getCheckemail();
        c.firstName='Test User';
        c.lastName='Last Test';
        c.email='Testuserlastuser2@yopmail.com';
        c.communityNickname='Testuserlastuser2@yopmail.com';
        c.password='pcori123';
        c.confirmPassword='pcori123';
        c.checkbox01=false;
        c.registerUser();
        Test.stopTest();
        
    }
    
    public static testmethod void testmethod5(){
        Test.startTest();
        CommunitiesSelfRegController c= new CommunitiesSelfRegController();
        c.getCheckemail();
        c.firstName='Test User';
        c.lastName='Last Test';
        c.email='Testuserlastuser2'+system.now()+'@yopmail.com';
        c.communityNickname='Testuserlastuser2'+system.now()+'@yopmail.com';
        c.password='pcori123';
        c.confirmPassword='pcori123';
        c.checkbox01=true;
        c.registerUser();
        test.stopTest();
        
    }
    
    public static testmethod void testmethod6() {
        Test.startTest();
        CommunitiesSelfRegController controller = new CommunitiesSelfRegController();
        controller.firstName = 'testFirstName';
        controller.lastName = 'testLastName';
        controller.email = 'test@force.com';
        controller.communityNickname = 'test@force.com';
        controller.password = 'abcd1234';
        controller.confirmPassword = 'abcd1234';
        controller.checkbox01=true;
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();
        Account ac = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId) ;
        insert ac; 
        Id recordTypeId1 = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Standard Salesforce Contact').getRecordTypeId();
        Contact con = new Contact(LastName ='testLastName',AccountId = ac.Id,FirstName='testFirstName',Email='test@force.com',recordtypeid=recordTypeId1);  
        insert con;  
        
        
        
        RecordType RecType3 = [Select Id From RecordType  Where SobjectType = 'Contact' and DeveloperName = 'Standard_Salesforce_Contact'];
        for(contact c:[select id,accountid,firstname, lastname, email from contact where (Recordtypeid =: RecType3.Id ) and ( id=:con.Id)]){
            if(c.Email==controller.email && c.LastName==controller.lastName && c.FirstName==controller.firstName){
                system.debug(controller.registerUser());
                controller.registerUser();
                system.debug(controller.confirmAgreement());
            }
        }
        Test.stopTest();
    }
    
    
}