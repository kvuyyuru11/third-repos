@isTest
public class RecruitmentTrigger_Test {

    public static testMethod void RecruitmentInsert() {

insert RecRec();
}
    public static testMethod list<recruitment__c>  RecRec(){
        list<recruitment__c> lst =new list<recruitment__c>();
        recruitment__c objrecruitment =new recruitment__c();
        objrecruitment.Project__c=OppId();
        objrecruitment.name='Test';
        objrecruitment.Report_Date__c=system.today();
        lst.add(objrecruitment);
        return lst; 
    }
    public static testMethod id  OppId(){
    User testPI1usr1;
    User testAOusr1;
    RecordType recOpp = [SELECT Id FROM RecordType WHERE sObjectType = 'Opportunity' AND Name = 'Research Awards'];
    RecordType recAcc = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Standard Salesforce Account'];
    User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    
System.runAs (thisUser)
        {
    
    Profile p = [SELECT Id FROM Profile WHERE Name='Science Program Operations'];
    UserRole r = [SELECT Id FROM UserRole WHERE Name='Science Method Effectiveness Research Team'];
    
    testPI1usr1 = new User(alias = 'PI', email='PI1@pcori1.com',
                                emailencodingkey='UTF-8', lastname='Lead1',
                                languagelocalekey='en_US',
                                localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
                                timezonesidkey='America/New_York',
                                username='PI1@pcori1.com');
            insert testPI1usr1;
            
            testAOusr1 = new User(alias = 'CM', email='CM@pcori1.com',
                emailencodingkey='UTF-8', lastname='Manager',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
                timezonesidkey='America/New_York',
                username='CM@pcori1.com');
            insert testAOusr1;
            }
        opportunity opps = new opportunity();
         
         opps.name= 'test opp';
         opps.Project_Name_off_layout__c = 'test opp';
         //opps.closedate=  Date.newInstance(2017,12,31);
         opps.StageName = 'Programmatic Review';
         opps.PI_Name_User__c = testPI1usr1.Id;
         opps.AO_Name_User__c = testAOusr1.Id;
                  
         //opps.PI_Name_User__c = UserInfo.Getuserid();
         //opps.AO_Name_User__c = UserInfo.Getuserid();
         opps.Contract_Number__c='34567890';
         //opps.StageName='Qualification';
         opps.closedate=system.today()+5;
         //opps.accountid=a.id;
         opps.Full_Project_Title__c='testpms';
        opps.Total_Cumulative_Actual_Enrolled__c=20;
        //o.Project_Name__c='testpm';
        opps.Project_End_Date__c=system.today()+1;
        insert opps;
        return opps.id;
       
    }
}