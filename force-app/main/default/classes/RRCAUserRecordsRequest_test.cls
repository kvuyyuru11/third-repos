@IsTest
public class RRCAUserRecordsRequest_test{
public static testmethod void RRCAUserRecordsRequest_testMethod1(){
RRCAUserRecordsRequest urr = new RRCAUserRecordsRequest();
Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RRCA').getRecordTypeId();
Account Acc = new Account(Name='Test Account',Community__c = 'TBD');
Insert Acc;
Acc.IsPartner = true;
update Acc;
Opportunity Opp = new Opportunity(AccountId=Acc.Id,Name='RRCA Project 1',Stagename='In-Process',Project_Lead_Email_Address__c='rrca505@test.com',CloseDate=system.today(),recordtypeId=recordTypeId,Project_Lead_First_Nam__c='FirstNameTest',Project_Lead_Last_Name__c='LastNameTest');
insert Opp;
Opp.Total_Project_Budget_EA__c = 123;
Opp.Project_Summary_to_be_made_public__c = 'Test';
Opp.Project_Lead_Salutation__c = 'Mr.';
Opp.Project_Lead_Degree__c = 'AAS';
Opp.Primary_Group_Identification__c = 'Patient/Consumer';
Opp.Previous_involvement_with_PCORI_RRCA__c = 'Joined a PCORI email list';
Opp.Outcomes_projected__c = 'Test';
Opp.Organization_s_Financial_Status__c = 'Test';
Opp.Current_Organizational_Budget_Total__c = 123;
Opp.Objectives__c = 'Test';
Opp.Methods__c = 'Test';
Opp.Justification__c = 'Test';
Opp.Describe_project_lead_s_previous_exp__c = 'test';
Opp.Proj_Lead_Funding_Sources_RRCA__c = 'PCORI';
Opp.Organization_s_EIN_Number__c = '123';
Opp.Describe_Unique_Capabilities__c = 'test';
Opp.Administrative_Official_Salutation__c = 'Mr.';
Opp.Project_Start_Date__c = date.today();
Opp.AO_Email__c= 'rrcaao505@test.com';
Opp.RRCA_AO_First_Name__c = 'AOFirstNameTest';
Opp.RRCA_AO_Last_Name__c = 'AOLastTest';
Opp.I_am_Authorized_To_Submit__c = True;
Opp.Amount_Requested_from_PCORI__c = 123;
Opp.Background__c = 'Test';
Opp.Budget_Description__c = 'Test';
Opp.Collaborator_Partner_Orgs_and_Role__c = 'Test';
Opp.Project_End_Date__c = system.today();
update Opp;
}
public static testmethod void RRCAUserRecordsRequest_testMethod2(){
 // User usr = [Select IsPortalEnabled,AccountId, name, id, contactid, IsActive,profile.name From User u where isActive=true and profile.name ='PCORI Community Partner' limit 1];           
         User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
    Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
    insert acnt;
    acnt.ispartner=true;
    update acnt; 
    Contact con = new Contact(LastName ='testCon',email='testeruio505@noemail.com',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                              Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
    insert con;  
      User usr = new User(alias = 'test123', email='testeruio505@noemail.com',
      emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
      localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
      ContactId = con.Id,
      timezonesidkey='America/New_York', username='testeruio505@noemail.com');
        
     insert usr;
RRCAUserRecordsRequest urr = new RRCAUserRecordsRequest();
Id recordTypeId2 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RRCA').getRecordTypeId();
/*Account Acc = new Account(Name='Test Account',Community__c = 'TBD');
Insert Acc;
Acc.IsPartner = true;
update Acc;*/
Opportunity Opp = new Opportunity(AccountId=acnt.Id,Name='RRCA Project 1',Stagename='In-Process',Project_Lead_Email_Address__c='testeruio505@noemail.com',CloseDate=system.today(),recordtypeId=recordTypeId2,Project_Lead_First_Nam__c='FirstNameTest',Project_Lead_Last_Name__c='LastNameTest');
insert Opp;
Opp.Total_Project_Budget_EA__c = 123;
Opp.Project_Summary_to_be_made_public__c = 'Test';
Opp.Project_Lead_Salutation__c = 'Mr.';
Opp.Project_Lead_Degree__c = 'AAS';
Opp.Primary_Group_Identification__c = 'Patient/Consumer';
Opp.Previous_involvement_with_PCORI_RRCA__c = 'Joined a PCORI email list';
Opp.Outcomes_projected__c = 'Test';
Opp.Organization_s_Financial_Status__c = 'Test';
Opp.Current_Organizational_Budget_Total__c = 123;
Opp.Objectives__c = 'Test';
Opp.Methods__c = 'Test';
Opp.Justification__c = 'Test';
Opp.Describe_project_lead_s_previous_exp__c = 'test';
Opp.Proj_Lead_Funding_Sources_RRCA__c = 'PCORI';
Opp.Organization_s_EIN_Number__c = '123';
Opp.Describe_Unique_Capabilities__c = 'test';
Opp.Administrative_Official_Salutation__c = 'Mr.';
Opp.Project_Start_Date__c = date.today();
Opp.AO_Email__c= 'abc505@nomail.com';
Opp.RRCA_AO_First_Name__c = 'AOFirstNameTest';
Opp.RRCA_AO_Last_Name__c = 'AOLastTest';
Opp.I_am_Authorized_To_Submit__c = True;
Opp.Amount_Requested_from_PCORI__c = 123;
Opp.Background__c = 'Test';
Opp.Budget_Description__c = 'Test';
Opp.Collaborator_Partner_Orgs_and_Role__c = 'Test';
Opp.Project_End_Date__c = system.today();
update Opp;
}

public static testmethod void RRCAUserRecordsRequest_testMethod3(){
 // User usr = [Select IsPortalEnabled,AccountId, name, id, contactid, IsActive,profile.name From User u where isActive=true and profile.name ='PCORI Community Partner' limit 1];           
         User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
    Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
    insert acnt;
    //acnt.ispartner=true;
    //update acnt; 
    Contact con = new Contact(LastName ='testCon',email='testeruio505@noemail.com',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                              Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
    insert con;  
     /* User usr = new User(alias = 'test123', email='testeruio505@noemail.com',
      emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
      localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
      ContactId = con.Id,
      timezonesidkey='America/New_York', username='testeruio505@noemail.com');
        
     insert usr;*/
RRCAUserRecordsRequest urr = new RRCAUserRecordsRequest();
Id recordTypeId2 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RRCA').getRecordTypeId();
/*Account Acc = new Account(Name='Test Account',Community__c = 'TBD');
Insert Acc;
Acc.IsPartner = true;
update Acc;*/
Opportunity Opp = new Opportunity(AccountId=acnt.Id,Name='RRCA Project 1',Stagename='In-Process',Project_Lead_Email_Address__c='testeruio505@noemail.com',CloseDate=system.today(),recordtypeId=recordTypeId2,Project_Lead_First_Nam__c='FirstNameTest',Project_Lead_Last_Name__c='LastNameTest');
insert Opp;
Opp.Total_Project_Budget_EA__c = 123;
Opp.Project_Summary_to_be_made_public__c = 'Test';
Opp.Project_Lead_Salutation__c = 'Mr.';
Opp.Project_Lead_Degree__c = 'AAS';
Opp.Primary_Group_Identification__c = 'Patient/Consumer';
Opp.Previous_involvement_with_PCORI_RRCA__c = 'Joined a PCORI email list';
Opp.Outcomes_projected__c = 'Test';
Opp.Organization_s_Financial_Status__c = 'Test';
Opp.Current_Organizational_Budget_Total__c = 123;
Opp.Objectives__c = 'Test';
Opp.Methods__c = 'Test';
Opp.Justification__c = 'Test';
Opp.Describe_project_lead_s_previous_exp__c = 'test';
Opp.Proj_Lead_Funding_Sources_RRCA__c = 'PCORI';
Opp.Organization_s_EIN_Number__c = '123';
Opp.Describe_Unique_Capabilities__c = 'test';
Opp.Administrative_Official_Salutation__c = 'Mr.';
Opp.Project_Start_Date__c = date.today();
Opp.AO_Email__c= 'abc_505@nomail.com';
//Opp.AO_Email__c='testing_user0525@yopmail.com';
Opp.RRCA_AO_First_Name__c = 'AOFirstNameTest';
Opp.RRCA_AO_Last_Name__c = 'AOLastTest';
Opp.I_am_Authorized_To_Submit__c = True;
Opp.Amount_Requested_from_PCORI__c = 123;
Opp.Background__c = 'Test';
Opp.Budget_Description__c = 'Test';
Opp.Collaborator_Partner_Orgs_and_Role__c = 'Test';
Opp.Project_End_Date__c = system.today();
update Opp;
}
}