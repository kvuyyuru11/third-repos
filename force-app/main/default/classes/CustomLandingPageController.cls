//This controller is used in Custom Landing Page  and it is used to redirect to Pages

public class CustomLandingPageController{
    public boolean isReviewer {get;set;} 
    public list<Advisory_Panel_Application__c> APlist{get;set;} 
    public boolean button1{get;set;}
    public boolean button2{get;set;}
    public boolean button4{get;set;}
    public boolean buttoncoi{get;set;}
    public boolean buttonappl{get;set;}
    public boolean Text1{get;set;}
    public boolean Text2{get;set;}
    public boolean Text3{get;set;}
    public boolean Text6{get;set;}
    public boolean Text7{get;set;}
    public boolean Text8{get;set;}
    public boolean Text9{get;set;}
    public boolean P2Pbutton1{get;set;}
    
    // public boolean link{get;set;}
    //public boolean button3{get;set;}
    public PermissionSet meCadidatePset{get;set;}
    public PermissionSet mrApprovePSet{get;set;}
    public PermissionSet mrEngagementPSet{get;set;}
    public PermissionSet APPSet{get;set;}
    string text4 = label.Redirect_to_MR_Application;
    string text5 = label.Redirect_to_Engagement;
    
    list<Permissionsetassignment> psalist= new list<Permissionsetassignment>();
    Id devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Pipeline to Proposal').getRecordTypeId();
    Id devRecordTypeId2 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('P2P Tier A').getRecordTypeId();
    RecordType rt=[Select Id from RecordType where name=:Label.COI_General_Record_Type limit 1];
    
    // string text6 = label.Redirect_to_Link;
    // here we initialize and define the permissionsets and and get the conact of the current user and check for reviewer status 
    public CustomLandingPageController()
    {
        //giveGranteePermissionSetIfNeeded();
        // new functionality. see if they are a merit reviewer
        isReviewer = userIsMeritReviewer();
        meCadidatePset = new PermissionSet();
        mrApprovePSet = new PermissionSet();
        mrEngagementPSet = new PermissionSet();
        APPSet = new PermissionSet();
        Id myID3 = userinfo.getUserId();
        string mrCadidatePSetName=Label.MR_Candidate_CCL;
        meCadidatePset =[SELECT Id,IsOwnedByProfile,Label FROM PermissionSet where Name=:mrCadidatePSetName limit 1];
        string mrApprovedPSetName=Label.MR_Approved;
        mrApprovePSet =[SELECT Id,IsOwnedByProfile,Label FROM PermissionSet where Name=:mrApprovedPSetName limit 1];
        string mrEngagementPSetName=Label.Engagement_Awards;
        mrEngagementPSet =[SELECT Id,IsOwnedByProfile,Label FROM PermissionSet where Name=:mrEngagementPSetName limit 1];
        string APPSetName=Label.AP_Contact;
        APPSet =[SELECT Id,IsOwnedByProfile,Label FROM PermissionSet where Name=:APPSetName limit 1];
        user usr=[select contactid from user where id=:myID3 limit 1];
        contact con=[select Reviewer_Status__c,Panel_Active_Inactive__c from Contact where id=:usr.ContactId limit 1];
        list<COI_Expertise__c> ciept=[select id,Submitted__c,Recordtypeid,COI_Due_Date__c from COI_Expertise__c where Reviewer_Name__c=:con.Id AND RecordTypeId=:rt.Id AND Active__c=TRUE AND Panel_R2__r.Active__c = TRUE order by Submitted__c desc];
        text9=true;
        list<FC_Reviewer__External_Review__c> extrappl=[select id,Panel__c,FC_Reviewer__Contact__c from FC_Reviewer__External_Review__c where FC_Reviewer__Contact__c=:con.Id AND Panel__r.Active__c = TRUE];
        
        if(con.Reviewer_Status__c==Label.PCORI_Reviewer||con.Reviewer_Status__c==Label.New_Reviewer||con.Reviewer_Status__c==Label.Do_Not_Invite||con.Reviewer_Status__c==Label.Approved_Pending_Federal_Authorization){
            button1=true;
            
            if(ciept.isEmpty()){
                
                text9=true;
                text2=true;
                text3=false;
                text6=false;
                
            }
            if(!ciept.isEmpty()){
                
                for(COI_Expertise__c ce:ciept){
                    if(ce.RecordTypeId==rt.Id){
                        if(ce.Submitted__c==false){
                            
                            buttoncoi=true;
                            text9=true;
                            buttonappl=false;
                            text7=true;
                            text8=false;
                            text6=false;
                            text2=false;
                            text3=false;
                            if(ce.COI_Due_Date__c<system.today()){
                            text6=true;
                            buttoncoi=false;
                            text7=false;
                            }
                            /* if(con.Panel_Active_Inactive__c==true){

text9=true;
text7=false;
text8=false;
text6=true;
text2=false;
text3=false;
buttonappl=false;
buttoncoi=false;
}*/
                            
                            
                            
                        }
                        if(ce.Submitted__c==true){
                            text1=false;
                            text7=false;
                            text9=false;
                            text2=false;
                            text8=true;
                            text6=false;
                            text3=false;
                            buttonappl=false;
                            
                            /*if(con.Panel_Active_Inactive__c==true){

text9=true;
text7=false;
text8=false;
text6=true;
text2=false;
text3=false;
buttonappl=false;
}*/
                            
                            /* if(con.Panel_Active_Inactive__c==false){

text1=false;
text7=false;
text9=false;
text2=false;
text8=true;
text6=false;
text3=false;
buttonappl=false;
}*/
                            
                        }                          }
                    
                }
            }                   
            if(!extrappl.isEmpty()){
                text9=false;
                text2=false;
                text3=true;
                text6=false;
                text7=false;
                text8=false;
                buttoncoi=false;
                buttonappl=true;
            }
            
            
            
            
            button4=false;
            
            
            
            
        }   
        
        //Checking the Reviewer Status of user
        //if(!Label.New_Reviewer.equals(con.Reviewer_Status__c))
        else if(con.Reviewer_Status__c!=Label.PCORI_Reviewer||con.Reviewer_Status__c!=Label.New_Reviewer)
        {
            button1=true;
            buttoncoi=false;
            button4=true;
            Text1=true;
            Text2=false;
            text3=false;
            // link=true;
            // button3=false;
        }    }
    //this method is to redirect to corresponding page when we click on the link    
    public PageReference redirectToVfPage()
    {
        try{
            Id myID1 = userinfo.getUserId();
            string permissionsetid = meCadidatePset.id ;
            
            //string strQuery = 'select id from PermissionsetAssignment where PermissionSetId = \'' + String.escapeSingleQuotes(permissionsetid) + '\' AND AssigneeId = \'' + String.escapeSingleQuotes(myID1) + '\'';
            List<PermissionSetAssignment> lstPsa = [select id from PermissionsetAssignment where PermissionSetId =: permissionsetid AND AssigneeId =:myID1];
            
            if(lstPsa.size() == 0)
            {
                PermissionSetAssignment psa1 = new PermissionsetAssignment(PermissionSetId= permissionsetid, AssigneeId = myID1);
                database.insert(psa1);
            }
            
            // Schema.DescribeSObjectResult anySObjectSchema = Merit_Reviewer_Application__c.SObjectType.getDescribe();
            //String ObjectIdPrefix = anySObjectSchema.getKeyPrefix();
            
            //This  will redirect to the Merit Reviewer Applications VF Tab.
            
            string url1='/servlet/servlet.Integration?lid=01r700000006kAZ&ic=1/home/home.jsp';
            PageReference pageReference = new PageReference(url1);
            // PageReference pageReference = new PageReference('/a1b/o');
            //pageReference.getparameters().put('lid',text4);
            pageReference.setRedirect(true);
            return pageReference;
        }
        catch (exception b)
        {
            throw b;
        }
    }
    
    
    public void giveGranteePermissionSetIfNeeded()
    {
        List<PermissionSet> granteePermSet = [SELECT id FROM PermissionSet WHERE Name =: 'Community_Grantee_Permission_set_User_Login_Licence'];
        Id granteePermSetID = granteePermSet[0].id;
        
        List<PermissionSetAssignment> communityPermAssignmentsList = [SELECT AssigneeId FROM PermissionSetAssignment 
                                                                      WHERE PermissionSetId =: granteePermSetID
                                                                      AND AssigneeId =: userinfo.getUserId()];
        if (communityPermAssignmentsList.isEmpty())
        {
            PermissionSetAssignment permAssign = new PermissionSetAssignment();
            permAssign.AssigneeId = userinfo.getUserId();
            permAssign.PermissionSetId = granteePermSetID;
            insert permAssign;           
        }
        
    }
    
    
    public PageReference redirectToVfPage1()
    {
        try{    
            Id myID2 = userinfo.getUserId();
            string permissionsetid = mrEngagementPSet.id; //'0PS1800000004Pw';
            
            string strQuery = 'select id from PermissionsetAssignment where PermissionSetId = \'' + String.escapeSingleQuotes(permissionsetid) + '\' AND AssigneeId = \'' + String.escapeSingleQuotes(myID2) + '\'';
            List<PermissionSetAssignment> lstPsa = [select id from PermissionsetAssignment where PermissionSetId =: permissionsetid AND AssigneeId =:myID2];
            
            if(lstPsa.size() == 0)
            {
                PermissionSetAssignment psa1 = new PermissionsetAssignment(PermissionSetId= permissionsetid, AssigneeId = myID2 );
                database.insert(psa1);
            }
            
            
            
            
            /* string url3='/servlet/servlet.Integration?lid=01r180000008b74&ic=1/home/home.jsp';
// string url1='/servlet/servlet.Integration?lid=01r180000008b74&ic=1/home/home.jsp';
PageReference pageReference1 = new PageReference(url3);
// pgRef1.getparameters().put('lid',text3);
pageReference1.setRedirect(true); */
            
            // This will redircet to the Foundation Connect VF Tab.
            string url2='/servlet/servlet.Integration?lid=01r700000006gSn&ic=1/home/home.jsp';
            // string url1='/servlet/servlet.Integration?lid=01r180000008b74&ic=1/home/home.jsp';
            /* Id fid=userinfo.getUserId();
User ur=[select id,contactid,Email from User where id=:fid];
Contact c = [SELECT Id,FGM_Portal__Username__c,FGM_Portal__Password__c,FGM_Portal__Confirm_Password__c,FGM_Portal__IsActive__c,FGM_Portal__User_Profile__c FROM Contact WHERE Id =: ur.ContactId limit 1];
if(c.FGM_Portal__Username__c==null && c.FGM_Portal__Password__c==null){
c.FGM_Portal__Username__c = ur.Email.replace('@','');
c.FGM_Portal__Password__c = FCUtility.GeneratePassword(ur);
c.FGM_Portal__Confirm_Password__c = c.FGM_Portal__Password__c;
c.FGM_Portal__IsActive__c = true;
c.FGM_Portal__User_Profile__c = Label.Grantee;

database.update(c);
}*/
            PageReference pageReference = new PageReference(url2);
            // pgRef1.getparameters().put('lid',text5);
            pageReference.setRedirect(true); 
            return pageReference ;
            
        }
        catch (exception c)
        {
            throw c;
        }
    }
    
    //This Method is to Redircet to the PCORI help Link
    /* public pageReference redirectToLink()
{
try{
// string url = 'http://www.pcori.org/get-involved/review-funding-applications/become-reviewer';
PageReference pgRef5 = new PageReference(text6); 
return pgRef5 ;
}
catch(exception h)
{
throw h;
}
} */
    //This Method is to Redirect to the Merit Reviewer Approved Candidate Profile.
    public pageReference redirectToMyProfile()
    {
        
        Id myID3 = userinfo.getUserId();
        try{
            APlist = [select id, Name, Application_Status__c, CreatedDate, Contact__r.Firstname, Contact__r.Lastname from Advisory_Panel_Application__c where CreatedBy.Id =:userinfo.getUserId() order by CreatedDate DESC limit 1];
            string permissionsetid = meCadidatePset.id ;//'0PS1800000004Py';
            string approvedpersetid = mrApprovePSet.id ;//'0PS1800000004Px';
            PageReference PageReference = null;
            
            
            // Deleting  PermissionsetAssignment which we assign user for the first time  
            //string strQuery = 'select id from PermissionsetAssignment where PermissionSetId = \'' + String.escapeSingleQuotes(permissionsetid) + '\' AND AssigneeId = \'' + String.escapeSingleQuotes(myID3) + '\'';
            List<PermissionSetAssignment> removePermissionSetlst = [select id from PermissionsetAssignment where PermissionSetId =: permissionsetid AND AssigneeId =:myID3];
            database.delete(removePermissionSetlst);
            
            // Assigning  Approved PermissionSet 
            //string strApprovedQuery = 'select id from PermissionsetAssignment where PermissionSetId = \'' + String.escapeSingleQuotes(approvedpersetid) + '\' AND AssigneeId = \'' + String.escapeSingleQuotes(myID3) + '\'';
            List<PermissionSetAssignment> lstPsa = [select id from PermissionsetAssignment where PermissionSetId =: approvedpersetid AND AssigneeId =:myID3];
            
            if(lstPsa.size() == 0)
            {
                PermissionSetAssignment psa1 = new PermissionsetAssignment(PermissionSetId= approvedpersetid, AssigneeId = myID3);
                database.insert(psa1);
            }
            
            
            
            PageReference pgRef6 = new PageReference(label.Open_Contact); 
            return pgRef6 ;
        }
        catch(exception a)
        {
            throw a;
        }
    }
    public pageReference redirectToMyProfileAP()
    {
        Id myID3 = userinfo.getUserId();
        try{
            string permissionsetid = meCadidatePset.id ;//'0PS1800000004Py';
            string APpersetid = APPSet.id ;
            PageReference PageReference = null;
            
            
            // Deleting  PermissionsetAssignment which we assign user for the first time  
            //string strQuery = 'select id from PermissionsetAssignment where PermissionSetId = \'' + String.escapeSingleQuotes(permissionsetid) + '\' AND AssigneeId = \'' + String.escapeSingleQuotes(myID3) + '\'';
            List<PermissionSetAssignment> removePermissionSetlst = [select id from PermissionsetAssignment where PermissionSetId =: permissionsetid AND AssigneeId =:myID3];
            database.delete(removePermissionSetlst);
            
            // Assigning  Approved PermissionSet 
            //string strApprovedQuery = 'select id from PermissionsetAssignment where PermissionSetId = \'' + String.escapeSingleQuotes(approvedpersetid) + '\' AND AssigneeId = \'' + String.escapeSingleQuotes(myID3) + '\'';
            List<PermissionSetAssignment> lstPsa = [select id from PermissionsetAssignment where PermissionSetId =: APpersetid AND AssigneeId =:myID3];
            
            if(lstPsa.size() == 0)
            {
                PermissionSetAssignment psa1 = new PermissionsetAssignment(PermissionSetId= APpersetid, AssigneeId = myID3);
                database.insert(psa1);
            }
            
            
            
            PageReference pgRef6 = new PageReference(label.Open_Contact); 
            return pgRef6 ;
        }
        catch(exception a)
        {
            throw a;
        }
    }
    
    //This is to Redirect to COI & Expertise Recent Applications Page.
    public PageReference redirectToCOIPage1()
    {
        Id myID3 = userinfo.getUserId();
        
        try
        {
            string permissionsetid = meCadidatePset.id ;//'0PS1800000004Py';
            string approvedpersetid = mrApprovePSet.id ;//'0PS1800000004Px';
            PageReference PageReference = null;
            
            
            // Deleting  PermissionsetAssignment which we assign user for the first time  
            //string strQuery = 'select id from PermissionsetAssignment where PermissionSetId = \'' + String.escapeSingleQuotes(permissionsetid) + '\' AND AssigneeId = \'' + String.escapeSingleQuotes(myID3) + '\'';
            List<PermissionSetAssignment> removePermissionSetlst = [select id from PermissionsetAssignment where PermissionSetId =: permissionsetid AND AssigneeId =:myID3];
            database.delete(removePermissionSetlst);
            
            // Assigning  Approved PermissionSet 
            //string strApprovedQuery = 'select id from PermissionsetAssignment where PermissionSetId = \'' + String.escapeSingleQuotes(approvedpersetid) + '\' AND AssigneeId = \'' + String.escapeSingleQuotes(myID3) + '\'';
            List<PermissionSetAssignment> lstPsa = [select id from PermissionsetAssignment where PermissionSetId =: approvedpersetid AND AssigneeId =:myID3];
            
            if(lstPsa.size() == 0)
            {
                PermissionSetAssignment psa1 = new PermissionsetAssignment(PermissionSetId= approvedpersetid, AssigneeId = myID3);
                database.insert(psa1);
            }
            
            
            PageReference pageReference2 = new PageReference(label.COI_Expertise);
            pageReference2.setRedirect(true);
            return pageReference2;
        }
        catch(exception ex)
        {
            throw ex;
        }
    }
    
    
    public pageReference redirectToAP()
    {
        try{
            PageReference pageReferenceAP = new PageReference(label.AP_Application);
            pageReferenceAP.setRedirect(true);
            return pageReferenceAP;       
        }
        catch(exception ee)
        {
            throw ee;
        }
    }
    
    //For Release 2 code was updated. 
    //08/26/2016 - Changes by Luis Rocha
    public pageReference redirectToRA()
    {
        // NEW
        List<Contact> contactList = [SELECT Id FROM Contact WHERE OwnerId =: UserInfo.getUserId()];
        List<Id> contactId_List = new List<Id>();
        for (Contact theContact : contactList)
        {
            contactId_List.add(theContact.id);
        }
        system.debug('contactID_List size: ' + contactId_List.size());
        String s = Label.RATabId;
        assignCustomerCommunityLoginPermissionSetToCommunityUser(contactId_List);
        assignGranteePermission();
        PageReference pageReferenceAP = new pageReference('/servlet/servlet.Integration?lid=' + s + '&ic=1/home/home.jsp');
        
        // END NEW
        try{
            
            pageReferenceAP.setRedirect(true);
            return pageReferenceAP;       
        }
        catch(exception ee)
        {
            throw ee;
        }
        /*OLD CODE BEFORE 08/26/2016
try{
PageReference pageReferenceAP = new pageReference('/'+'006?fcf='+Label.Redirect_to_Research_Awards_List_view); 
pageReferenceAP.setRedirect(true);
return pageReferenceAP;       
}
catch(exception ee)
{
throw ee;
return null;
}
*/
    }
    public pageReference redirectToDI()
    {
        // NEW
        List<Contact> contactList = [SELECT Id FROM Contact WHERE OwnerId =: UserInfo.getUserId()];
        List<Id> contactId_List = new List<Id>();
        for (Contact theContact : contactList)
        {
            contactId_List.add(theContact.id);
        }
        system.debug('contactID_List size: ' + contactId_List.size());
        String s = Label.DILandingPage;
        assignCustomerCommunityLoginPermissionSetToCommunityUser(contactId_List);
        assignGranteePermission();
        PageReference pageReferenceAP = new pageReference('/servlet/servlet.Integration?lid=' + s + '&ic=1/home/home.jsp');
        
        // END NEW
        try{
            
            pageReferenceAP.setRedirect(true);
            return pageReferenceAP;       
        }
        catch(exception ee)
        {
            throw ee;
        }
        /*OLD CODE BEFORE 08/26/2016
try{
PageReference pageReferenceAP = new pageReference('/'+'006?fcf='+Label.Redirect_to_Research_Awards_List_view); 
pageReferenceAP.setRedirect(true);
return pageReferenceAP;       
}
catch(exception ee)
{
throw ee;
return null;
}
*/
    }
    public pageReference redirectToInfrastructure()
    {
        // NEW
        List<Contact> contactList = [SELECT Id FROM Contact WHERE OwnerId =: UserInfo.getUserId()];
        List<Id> contactId_List = new List<Id>();
        for (Contact theContact : contactList)
        {
            contactId_List.add(theContact.id);
        }
        system.debug('contactID_List size: ' + contactId_List.size());
        String s = Label.InfrastructureLandingPage;
        
        assignCustomerCommunityLoginPermissionSetToCommunityUser(contactId_List);
        assignGranteePermission();
        PageReference pageReferenceAP = new pageReference('/servlet/servlet.Integration?lid=' + s + '&ic=1/home/home.jsp');
        
        // END NEW
        try{
            
            pageReferenceAP.setRedirect(true);
            return pageReferenceAP;       
        }
        catch(exception ee)
        {
            throw ee;
        }
        
    }
      /**
    * @author        Anudeep Buddineni
    * @date          05/08/2018
    * @description   This method Checks which recordtype P2P project is being shared with the external user and pass the parameters in the URL accordingly
    */
    public pageReference redirectToPtoP()
    {        
     Set<Id> Opp  = new Set<Id>();
        Set<Id> Opp2 = new Set<Id>();
        Id p2pTierART =Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('P2P Tier A').getRecordTypeId();
        Id p2pRT =Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Pipeline to Proposal').getRecordTypeId();
        List<OpportunityShare> opptyShare = [SELECT OpportunityAccessLevel, OpportunityId, Opportunity.RecordtypeId from OpportunityShare WHERE UserOrGroupId =: UserInfo.getUserId() AND 
                                            (Opportunity.RecordtypeId =:p2pTierART OR Opportunity.RecordtypeId =:p2pRT)]; 
        for (OpportunityShare os:opptyShare) {
            if (os.Opportunity.RecordtypeId == p2pRT) {
                Opp.add(os.Id);
            } else if (os.Opportunity.RecordtypeId == p2pTierART) {
                opp2.add(os.Id);
            }
        }
        system.debug('opp2'+opp2);  
        system.debug('opp'+opp); 
        String s = Label.PtoPLandingPage;
        system.debug('Opp.size()'+Opp.size());               
            
            if (!Opp.isEmpty() && Opp.Size()>0 && Opp2.isEmpty()) {
                system.debug('entered opp loop');
                PageReference pageReferenceP2P = new pageReference('/PtoPLandingPage?lid='+s+'&recType=P2P');             
                pageReferenceP2P.setRedirect(true);
                return pageReferenceP2P;  
            } else if (!Opp2.isEmpty() && Opp2.Size()>0 && Opp.isEmpty() ) {
                system.debug('TEst tierA PI');
                PageReference pageReferenceP2Pa = new pageReference('/PtoPLandingPage?lid='+s+'&recType=P2PTierA');             
                pageReferenceP2Pa.setRedirect(true);
                return pageReferenceP2Pa;      
            } else if (!Opp2.isEmpty() && !Opp.isEmpty()) {
                system.debug('TEst tierA PI');
                PageReference pageReferenceP2Pa = new pageReference('/PtoPLandingPage?lid='+s+'&recType=P2PBothTiers');             
                pageReferenceP2Pa.setRedirect(true);
                return pageReferenceP2Pa;      
            } else if (Opp2.isEmpty() && Opp.isEmpty()) {
                PageReference pageReferenceP2Pa = new pageReference('/PtoPLandingPage?lid='+s+'&recType=NoP2Precs');             
                pageReferenceP2Pa.setRedirect(true);
                return pageReferenceP2Pa;    
            }
    
        return null;
    }
    /* public pageReference RedirectToAmbform()
{
user us=[select id,contactid from user where id=:userinfo.getUserId() Limit 1];
Contact  con= [select id from contact where Id=:us.contactid Limit 1];
List<Ambassador__c> Amblist = [select Id,Status__c from Ambassador__c Where Contact__c=:con.Id];
if(Amblist.size()>0 && Amblist!=null){
if(Amblist[0].Status__c == 'Active'){

try{
PageReference pageReferenceAP = new PageReference('/apex/AmbassadorActiveLandingPage');
pageReferenceAP.setRedirect(true);
return pageReferenceAP;       
}
catch(exception ee)
{
throw ee;
}


}else{
try{
PageReference pageReferenceAP = new PageReference(label.Ambassador_Landing_Page);
pageReferenceAP.setRedirect(true);
return pageReferenceAP;       
}
catch(exception ee)
{
throw ee;
}
}}
return null;
}

boolean userIsMeritReviewer()
{   
// get main permission set
List<PermissionSet> permissionList = [SELECT id FROM PermissionSet WHERE name =: 'Merit_Reviewer_Approved_PCL'];
if (!permissionList.isEmpty())
{
System.debug('p set found');
List<PermissionSetAssignment> permAssignments = [SELECT Id FROM PermissionSetAssignment 
WHERE PermissionSetID =: permissionList[0].id
AND AssigneeId =: UserInfo.getUserId()];
if (!permAssignments.isEmpty())
{
// they have the permission set. can enable them as a reviewer.
return true;
}
}
return false;
}
*/
    public pageReference RedirectToAmbform()
    {
        user us=[select id,contactid from user where id=:userinfo.getUserId() Limit 1];
        Contact  con= [select id from contact where Id=:us.contactid Limit 1];
        List<Ambassador__c> Amblist = [select Id,Status__c from Ambassador__c Where Contact__c=:con.Id];
        if(Amblist.size()>0 && Amblist!=null){
            if(Amblist[0].Status__c == 'Active'){
                
                try{
                    PageReference pageReferenceAP = new PageReference('/apex/AmbassadorActiveLandingPage');
                    pageReferenceAP.setRedirect(true);
                    return pageReferenceAP;       
                }
                catch(exception ee)
                {
                    throw ee;
                }
                
                
            }else{
                try{
                    PageReference pageReferenceAP = new PageReference(label.Ambassador_Landing_Page);
                    pageReferenceAP.setRedirect(true);
                    return pageReferenceAP;       
                }
                catch(exception ee)
                {
                    throw ee;
                }
            }}
        else{
            try{
                PageReference pageReferenceAP = new PageReference(label.Ambassador_Landing_Page);
                pageReferenceAP.setRedirect(true);
                return pageReferenceAP;       
            }
            catch(exception ee)
            {
                throw ee;
            }
        }
    }
    
    boolean userIsMeritReviewer()
    {   
        // get main permission set
        List<PermissionSet> permissionList = [SELECT id FROM PermissionSet WHERE name =: 'Merit_Reviewer_Approved_PCL'];
        if (!permissionList.isEmpty())
        {
            System.debug('p set found');
            List<PermissionSetAssignment> permAssignments = [SELECT Id FROM PermissionSetAssignment 
                                                             WHERE PermissionSetID =: permissionList[0].id
                                                             AND AssigneeId =: UserInfo.getUserId()];
            if (!permAssignments.isEmpty())
            {
                // they have the permission set. can enable them as a reviewer.
                return true;
            }
        }
        return false;
    }    
    public void redirecttorvwportalap(){
        List<PermissionSet> rvwrPermSet = [SELECT id FROM PermissionSet WHERE Name =: 'Community_Reviewer_Permission_set_User_Login_Licence'];
        
        List<PermissionSetAssignment> currentAssigns1 = [SELECT ID, AssigneeId FROM PermissionSetAssignment 
                                                         WHERE permissionSetId =: rvwrPermSet[0].id
                                                         AND assigneeId =: userInfo.getUserId()];
        system.debug('888888'+currentAssigns1);
        if (currentAssigns1.size() == 0)
        {
            system.debug('******'+'came inside');
            PermissionSetAssignment psa = new PermissionSetAssignment();
            psa.PermissionSetId = rvwrPermSet[0].id;
            psa.AssigneeId = UserInfo.getUserId();
            psalist.add(psa);
        }
        
        List<PermissionSet> rvwrPermSet1 = [SELECT id FROM PermissionSet WHERE Name =: 'Community_Grantee_Permission_set_User_Login_Licence'];
        
        List<PermissionSetAssignment> currentAssigns2 = [SELECT ID, AssigneeId FROM PermissionSetAssignment 
                                                         WHERE permissionSetId =: rvwrPermSet1[0].id
                                                         AND assigneeId =: userInfo.getUserId()];
        system.debug('888888'+currentAssigns2);
        if (currentAssigns2.size() == 0)
        {
            system.debug('******'+'came inside');
            PermissionSetAssignment psa1 = new PermissionSetAssignment();
            psa1.PermissionSetId = rvwrPermSet1[0].id;
            psa1.AssigneeId = UserInfo.getUserId();
            psalist.add(psa1);
        }
        database.insert(psalist);
    }
    
    public void assignGranteePermission()
    {
        List<PermissionSet> granteePermSet = [SELECT id FROM PermissionSet WHERE Name =: 'Community_Grantee_Permission_set_User_Login_Licence'];
        
        List<PermissionSetAssignment> currentAssigns = [SELECT ID, AssigneeId FROM PermissionSetAssignment 
                                                        WHERE permissionSetId =: granteePermSet[0].id
                                                        AND assigneeId =: userInfo.getUserId()];
        
        if (currentAssigns.isEmpty())
        {
            PermissionSetAssignment psa = new PermissionSetAssignment();
            psa.PermissionSetId = granteePermSet[0].id;
            psa.AssigneeId = UserInfo.getUserId();
            insert psa;
        }
    }
    
    
    //This code was added from FC2TEST
    //By Luis Rocha on 08/26/2016
    // Customer_Community_Login_User_Modified_PCL
    public static void assignCustomerCommunityLoginPermissionSetToCommunityUser(List<Id> contactIdList){
        system.debug('===contactIdList==='+contactIdList);
        Map<Id,User> contactIdAndUserMap = new Map<Id,User>();
        PermissionSet ps = null;
        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();
        
        if(!contactIdList.isEmpty()){
            //system.debug('Im not empty!');
            ps = [select Id, Name from PermissionSet where Name='Customer_Community_Login_User_Modified_PCL'];
            for(User u : [select Id, ContactId, (select PermissionSetId from PermissionSetAssignments) from User where ContactId in :contactIdList]){
                contactIdAndUserMap.put(u.ContactId,u);
            }
        }
        
        for(Id contactId : contactIdAndUserMap.keySet()){
            if(contactIdAndUserMap.containsKey(contactId)){
                Boolean assignedAlreadyFlag = false;
                for(PermissionSetAssignment psaExisted : contactIdAndUserMap.get(contactId).PermissionSetAssignments){
                    //system.debug('2nd for loop!');
                    if(psaExisted.PermissionSetId == ps.Id){
                        //system.debug('Im already assigned!');
                        assignedAlreadyFlag = true;
                        break;
                    }
                }
                system.debug('===assignedAlreadyFlag==='+assignedAlreadyFlag);
                if(!assignedAlreadyFlag){
                    PermissionSetAssignment psa = new PermissionSetAssignment();
                    psa.PermissionSetId = ps.Id;
                    psa.AssigneeId = contactIdAndUserMap.get(contactId).Id;
                    psaList.add(psa);
                }
            }
        }
        if(!psaList.isEmpty()){
            try{
                system.debug('===psaList==='+psaList);
                Database.insert(psaList);
            }catch(Exception e){
                
            }
        }
    }
    /**
    * @author        Abhinav Polsani(EA Migration)
    * @date          03/26/2018
    * @description   returns nothing
    * @param         None
    * @return        PageReference 
    */
    public PageReference redirectToReviewerCommunity() {
            return null;       
    }
    
}