public class P2PFormRedirectionController {
   public List<SelectOption> Recordtypes {get;set;}
    public string Recordtypevalue {get;set;}
    public boolean ishowNorow{get;set;}
     @testvisible private id projid{get;set;}
     public P2PFormRedirectionController(ApexPages.StandardController controller){
          string sReturl=ApexPages.currentPage().getParameters().get('Pid');
         projid=sReturl;
         Recordtypes=getItems();
         if(Recordtypes.size()==0){
             ishowNorow=false;
         }
         else{
             ishowNorow=true;
         }
     }


 
  public List<SelectOption> getItems() {
      map<string,id> mpexistingRecord=new map<string,id>();
      for(P2P_TierA__c objP2P_TierA:[select id,RecordType.Name from P2P_TierA__c where Projects__c=:projid]) {
          mpexistingRecord.put(objP2P_TierA.RecordType.Name,objP2P_TierA.id);
      }
    List<SelectOption> options = new List<SelectOption>();
      Map <String,Schema.RecordTypeInfo> recordTypeMap = P2P_TierA__c.sObjectType.getDescribe().getRecordTypeInfosByName();
      if(recordTypeMap.size()>0){
         
      }
    for(string srecname : recordTypeMap.keyset())
    {
        if(srecname!='Master'){
        if(!mpexistingRecord.containskey(srecname)){
            if(recordTypeMap.get(srecname).isActive()){
             options.add(new SelectOption(recordTypeMap.get(srecname).getRecordTypeId(),srecname));
            }
        }
        }
    }
    return options;
  }
    public pageReference Save(){
        opportunity objopp=[select id,Name from opportunity where id=:projid];
        
         PageReference pr=new PageReference('/' +label.P2PTierAObjectid+'/e?retURL=%2F'+projid+'&RecordType='+Recordtypevalue+'&'+system.label.P2P_ProjectLookip+'='+objopp.name+'&'+system.label.P2P_ProjectLookip+'_lkid='+projid);
     pr.setRedirect(true);
   return pr; 
    }
    public pageReference cancel(){
        
         PageReference pr=new PageReference('/'+projid);
     pr.setRedirect(true);
   return pr; 
    }
}