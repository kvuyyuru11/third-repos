public with sharing class BudgetListComponentController {
	//parameters to be passed in
	public Id oppId {get;set;}
	public Id retId {get;set;}
	public Boolean displayArchived {get;set;}

	public List<Budget__c> budgetlst {get;set;}

	public BudgetListComponentController(){
		/* Attributes don't get set until after this executes
		   so all initiation logic happens in the init method
		   when the component loads
		*/
	}

	/* Component has <apex:outputText rendered='{!init}'/>
	   at the top of the page.
	   When the component loads the following logic will run
	*/
	public Boolean init {
		get {
			if(this.oppId != null){

				if(this.showArchived()) {
					this.budgetlst = [SELECT Id, Name, Budget_Status__c FROM Budget__c Where Associated_App_Project__c = :this.oppId];
				}
				else {
					this.budgetlst = [SELECT Id, Name, Budget_Status__c FROM Budget__c Where Associated_App_Project__c = :this.oppId AND Budget_Status__c != 'Archived'];
				}

				//Updated code to return a null list if the list is empty so that it will not generate a page on the page layout. 
				if(this.budgetlst.size() == 0){
					this.budgetlst = null;
				}

			}
			//we don't want to display the output text so we return false
			return false;
		}
    }

	//this private method can be updated if additional logic is need to determine if archived budgets should be shown
	private Boolean showArchived() {
		return this.displayArchived;
	}
}