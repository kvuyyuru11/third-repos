@isTest
public class LOI_Review_LookupChangeTest {
        
    private static testMethod void testUpdateReviewer1(){
        List<Account> accts = ObjectCreator.getAccts('Jimbo', 10);
        insert accts;
        
        List<User> users = ObjectCreator.getUsers(10, 'Donnie');
        insert users;
        RecordType RA_LOI_ID = [SELECT Id FROM RecordType WHERE sObjectType = 'Lead' AND DeveloperName = 'RA_LOI'];
        Id campRecId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Standard Campaign').getRecordTypeId();
        Id FLRF_ID = Schema.SObjectType.Fluxx_LOI_Review_Form__c.getRecordTypeInfosByName().get('AD LOI Review').getRecordTypeId();
        Campaign camp = new Campaign();
        camp.Name = 'Test';
        camp.RecordTypeId = campRecId;
        camp.PFA_Type__c = 'Broad';
        insert camp;
        
        Lead lead1 = new Lead(recordTypeID = RA_LOI_ID.Id, Principal_Investigator__c = users[0].id, Administrative_Official__c = users[1].id,
                              Status = 'Draft', Project_Lead_Last_Name_Clone__c = 'testing', Company = 'testin Comp', PFA__c = camp.Id,
                                  LastName = 'BoJack', FirstName = 'Horseman', Zendesk__organization__c  = accts[0].id, Review_Roll_up_1__c = '2');
        insert lead1;
        
        Fluxx_LOI_Review_Form__c fl1 = new Fluxx_LOI_Review_Form__c();
        fl1.COI__c = 'No';
        fl1.Lead_LOI__c = lead1.Id;
        fl1.Review_Completed__c = true;
        fl1.Final_Decision__c = 'Not Invite';
        fl1.RecordTypeId = FLRF_ID;
        fl1.Programmatically_Responsive__c = 'No';
        fl1.If_No_can_it_move_to_alternate_PFA__c = 'No';
        fl1.Reviewer_Label_RA__c = 'Reviewer 1';
        fl1.Review_Number__c = 2;
        fl1.Includes_CEA__c = 'No';
        fl1.Includes_comparators__c = 'No';
        insert fl1;
        
        List<Lead> leads = new List<Lead>();
        Lead lead = new Lead(recordTypeID = RA_LOI_ID.Id, Principal_Investigator__c = users[0].id, Administrative_Official__c = users[1].id,
                                  Status = 'Draft', Project_Lead_Last_Name_Clone__c = 'testing', Company = 'testin Comp', LOI_Review2__c = fl1.Id, 
                                    LOI_Review1__c = fl1.Id, LOI_Review3__c = fl1.Id, Review_Roll_up_2__c = '2', Review_Roll_up_3__c = '2',
                                  LastName = 'BoJack', FirstName = 'Horseman', Zendesk__organization__c  = accts[0].id, Review_Roll_up_1__c = '2');
        leads.add(lead);
        insert leads;
        LOI_Review1_LookupChange.updateReviewer1(leads);
    }
    
    /*private static testMethod void testUpdateReviewer2(){
        List<Account> accts = ObjectCreator.getAccts('Jimbo', 10);
        insert accts;
        
        List<User> users = ObjectCreator.getUsers(10, 'Donnie');
        insert users;
        RecordType RA_LOI_ID = [SELECT Id FROM RecordType WHERE sObjectType = 'Lead' AND DeveloperName = 'RA_LOI'];
        Id campRecId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Standard Campaign').getRecordTypeId();
        Id FLRF_ID = Schema.SObjectType.Fluxx_LOI_Review_Form__c.getRecordTypeInfosByName().get('AD LOI Review').getRecordTypeId();
        Campaign camp = new Campaign();
        camp.Name = 'Test';
        camp.RecordTypeId = campRecId;
        camp.PFA_Type__c = 'Broad';
        insert camp;
        
        Lead lead1 = new Lead(recordTypeID = RA_LOI_ID.Id, Principal_Investigator__c = users[0].id, Administrative_Official__c = users[1].id,
                              Status = 'Draft', Project_Lead_Last_Name_Clone__c = 'testing', Company = 'testin Comp', PFA__c = camp.Id,
                                  LastName = 'BoJack', FirstName = 'Horseman', Zendesk__organization__c  = accts[0].id, Review_Roll_up_1__c = '2');
        insert lead1;
        
        Fluxx_LOI_Review_Form__c fl1 = new Fluxx_LOI_Review_Form__c();
        fl1.COI__c = 'No';
        fl1.Lead_LOI__c = lead1.Id;
        fl1.Review_Completed__c = true;
        fl1.Final_Decision__c = 'Not Invite';
        fl1.Programmatically_Responsive__c = 'No';
        fl1.If_No_can_it_move_to_alternate_PFA__c = 'Yes';
        fl1.Reviewer_Label_RA__c = 'Reviewer 1';
        fl1.Review_Number__c = 2;
        fl1.Includes_CEA__c = 'No';
        fl1.Includes_comparators__c = 'No';
        insert fl1;
        
        
        
        List<Lead> leads = new List<Lead>();*/
        /*
        Lead lead = new Lead(recordTypeID = RA_LOI_ID.Id, Principal_Investigator__c = users[0].id, Administrative_Official__c = users[1].id,
                                  Status = 'Draft', Project_Lead_Last_Name_Clone__c = 'testing', Company = 'testin Comp', LOI_Review2__c = fl1.Id, 
                                    LOI_Review1__c = fl1.Id, LOI_Review3__c = fl1.Id, Review_Roll_up_2__c = '2',
                                  LastName = 'BoJack', FirstName = 'Horseman', Zendesk__organization__c  = accts[0].id);
        */
        /*lead1.LOI_Review1__c = fl1.id;
        lead1.LOI_Review2__c = fl1.id;
        lead1.LOI_Review3__c = fl1.id;
        lead1.Review_Roll_up_1__c = '1';
        lead1.Review_Roll_up_2__c = '1';
        lead1.Review_Roll_up_3__c = '1';
        lead1.RecordTypeId = RA_LOI_ID.id;
        leads.add(lead1);
        try {
        update leads;
        LOI_Review2_LookupChange.updateReviewer2(leads);

        } catch (Exception e) {
           system.debug('+++++'+e); 
        } 
    }
    */
    
        private static testMethod void testUpdateReviewer2(){
        List<Account> accts = ObjectCreator.getAccts('Jimbo', 10);
        insert accts;
        
        List<User> users = ObjectCreator.getUsers(10, 'Donnie');
        insert users;
        RecordType RA_LOI_ID = [SELECT Id FROM RecordType WHERE sObjectType = 'Lead' AND DeveloperName = 'RA_LOI'];
        Id campRecId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Standard Campaign').getRecordTypeId();
        Id FLRF_ID = Schema.SObjectType.Fluxx_LOI_Review_Form__c.getRecordTypeInfosByName().get('AD LOI Review').getRecordTypeId();
        Campaign camp = new Campaign();
        camp.Name = 'Test';
        camp.RecordTypeId = campRecId;
        camp.PFA_Type__c = 'Broad';
        insert camp;
        
        Lead lead1 = new Lead(recordTypeID = RA_LOI_ID.Id, Principal_Investigator__c = users[0].id, Administrative_Official__c = users[1].id,
                              Status = 'Draft', Project_Lead_Last_Name_Clone__c = 'testing', Company = 'testin Comp', PFA__c = camp.Id,
                                  LastName = 'BoJack', FirstName = 'Horseman', Zendesk__organization__c  = accts[0].id, Review_Roll_up_1__c = '2');
        insert lead1;
        
        Fluxx_LOI_Review_Form__c fl1 = new Fluxx_LOI_Review_Form__c();
        fl1.COI__c = 'No';
        fl1.Lead_LOI__c = lead1.Id;
        fl1.RecordTypeId = FLRF_ID;
        fl1.Review_Completed__c = true;
        fl1.Final_Decision__c = 'Not Invite';
        fl1.Programmatically_Responsive__c = 'No';
        fl1.If_No_can_it_move_to_alternate_PFA__c = 'Yes';
        fl1.Reviewer_Label_RA__c = 'Reviewer 1';
        fl1.Review_Number__c = 2;
        fl1.Includes_CEA__c = 'No';
        fl1.Includes_comparators__c = 'No';
        insert fl1;
        
        
        
        List<Lead> leads = new List<Lead>();
        /*
        Lead lead = new Lead(recordTypeID = RA_LOI_ID.Id, Principal_Investigator__c = users[0].id, Administrative_Official__c = users[1].id,
                                  Status = 'Draft', Project_Lead_Last_Name_Clone__c = 'testing', Company = 'testin Comp', LOI_Review2__c = fl1.Id, 
                                    LOI_Review1__c = fl1.Id, LOI_Review3__c = fl1.Id, Review_Roll_up_2__c = '2',
                                  LastName = 'BoJack', FirstName = 'Horseman', Zendesk__organization__c  = accts[0].id);
        */
        lead1.LOI_Review1__c = fl1.id;
        lead1.LOI_Review2__c = fl1.id;
        lead1.LOI_Review3__c = fl1.id;
        lead1.Review_Roll_up_1__c = '1';
        lead1.Review_Roll_up_2__c = '1';
        lead1.Review_Roll_up_3__c = '1';
        lead1.RecordTypeId = RA_LOI_ID.id; 
        leads.add(lead1);
        try {
        //update leads;
        LOI_Review2_LookupChange.updateReviewer2(leads);

        } catch (Exception e) {
           system.debug('+++++'+e); 
        } 
    }
    
    private static testMethod void testUpdateReviewer3(){
                List<Account> accts = ObjectCreator.getAccts('Jimbo', 10);
        insert accts;
        
        List<User> users = ObjectCreator.getUsers(10, 'Donnie');
        insert users;
        RecordType RA_LOI_ID = [SELECT Id FROM RecordType WHERE sObjectType = 'Lead' AND DeveloperName = 'RA_LOI'];
        Id campRecId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Standard Campaign').getRecordTypeId();
        Id FLRF_ID = Schema.SObjectType.Fluxx_LOI_Review_Form__c.getRecordTypeInfosByName().get('AD LOI Review').getRecordTypeId();
        Campaign camp = new Campaign();
        camp.Name = 'Test';
        camp.RecordTypeId = campRecId;
        camp.PFA_Type__c = 'Broad';
        insert camp;
        
        Lead lead1 = new Lead(recordTypeID = RA_LOI_ID.Id, Principal_Investigator__c = users[0].id, Administrative_Official__c = users[1].id,
                              Status = 'Draft', Project_Lead_Last_Name_Clone__c = 'testing', Company = 'testin Comp', PFA__c = camp.Id,
                                  LastName = 'BoJack', FirstName = 'Horseman', Zendesk__organization__c  = accts[0].id, Review_Roll_up_1__c = '2');
        insert lead1;
        
        Fluxx_LOI_Review_Form__c fl1 = new Fluxx_LOI_Review_Form__c();
        fl1.COI__c = 'No';
        fl1.Lead_LOI__c = lead1.Id;
        fl1.Review_Completed__c = true;
        fl1.Final_Decision__c = 'Not Invite';
        fl1.RecordTypeId = FLRF_ID;
        fl1.Programmatically_Responsive__c = 'No';
        fl1.If_No_can_it_move_to_alternate_PFA__c = 'No';
        fl1.Reviewer_Label_RA__c = 'Reviewer 1';
        fl1.Review_Number__c = 2;
        fl1.Includes_CEA__c = 'No';
        fl1.Includes_comparators__c = 'No';
        insert fl1;
        
        List<Lead> leads = new List<Lead>();
        Lead lead = new Lead(recordTypeID = RA_LOI_ID.Id, Principal_Investigator__c = users[0].id, Administrative_Official__c = users[1].id,
                                  Status = 'Draft', Project_Lead_Last_Name_Clone__c = 'testing', Company = 'testin Comp', LOI_Review2__c = fl1.Id, 
                                    LOI_Review1__c = fl1.Id, LOI_Review3__c = fl1.Id, Review_Roll_up_2__c = '2', Review_Roll_up_3__c = '2',
                                  LastName = 'BoJack', FirstName = 'Horseman', Zendesk__organization__c  = accts[0].id, Review_Roll_up_1__c = '2');
        leads.add(lead);
        insert leads;
        LOI_Review3_LookupChange.updateReviewer3(leads);
    }
}