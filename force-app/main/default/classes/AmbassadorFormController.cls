/****************************************************************************************************
*Description:           This Class is the controller for the AmbassadorForm Visualforforce Page
*
*
*Test Class        :    AmbassadorFormController_Test
*
*****************************************************************************************************/

public without sharing class AmbassadorFormController{

    public Id deleteRecordId{get;set;}
    
    
    
    public String servedasaPCORIAdvisoryPanel { get; set; }

    public String servedasaPCORIPeerReviewer { get; set; }

    public String servedasaPCORIMeritReviewer { get; set; }

    public String primaryRole3OtherStyle { get; set; }

    public String primaryRole2OtherStyle { get; set; }
    
    public String mentorprgrmStyle {get; set;}

    public String primaryRole1OtherStyle { get; set; }

    public String enterShortBioStyle { get; set; }

    public String whereDoYouCurrentlyLiveOtherStyle { get; set; }

    public String whereDoYouCurrentlyLiveStyle { get; set; }

    public String populationRepresentationOtherStyle { get; set; }

    public String populationRepresentationStyle { get; set; }

    public String therapeuticAreasofInterestOtherStyle { get; set; }

    public String therapeuticAreasofInterestStyle { get; set; }

    public String researchPhaseExperienceOtherStyle { get; set; }

    public String researchPhaseExperienceStyle { get; set; }

    public String involvementWithPcoriStyle { get; set; }

    public String primaryStakeholderOtherStyle { get; set; }

    public String primaryStakeholderStyle { get; set; }

    public String lastNameStyle { get; set; }

    public String firstNameStyle { get; set; }
    
    Public Boolean canUpload {get; set;}
    
    public Ambassador__c amb{get;set;}  
    
    Public User u;
    
    
   
    //public transient Attachment userAttachment{get;set;}
    
    public transient Attachment userAttachment{
  get {
      if (userAttachment == null)
        userAttachment = new Attachment();
      return userAttachment ;
    }
  set;
  }
    
    public AmbassadorFormController(){
    amb= new Ambassador__c();
    
    u = [Select Id, ContactId from User where Id=:UserInfo.getUserId()];
    if(getattachmentList().isEmpty()){
        canUpload=True;
    }else{
        canUpload=False;
    }    
    }
   
    public pagereference submit(){
        resetAllStrings();
        if(!validations()){
        return null;
        }else{
        Ambassador__c amb2 = new Ambassador__c();
        amb2.First_Name__c = amb.First_Name__c;
        amb2.Last_Name__c = amb.Last_Name__c;
        amb2.Primary_Stakeholder_Type__c = amb.Primary_Stakeholder_Type__c;
        amb2.Participate_in_Mentor_Program__c= amb.Participate_in_Mentor_Program__c;
        
        if(amb.Primary_Stakeholder_Type__c == 'Other')
        amb2.Primary_Stakeholder_Type_Other__c= amb.Primary_Stakeholder_Type_Other__c;
        
        
        for(integer i=1;i<4;i++){
        amb2.put('Organizational_Affiliation_'+i+'__c', amb.get('Organizational_Affiliation_'+i+'__c'));
        
        if(amb.get('Organizational_Affiliation_'+i+'__c') !=null && amb.get('Organizational_Affiliation_'+i+'__c')!='')
        amb2.put('Primary_Role_'+i+'__c' , amb.get('Primary_Role_'+i+'__c'));
        
        if(amb2.get('Primary_Role_'+i+'__c') == 'Other')
        amb2.put('Primary_Role_'+i+'_Other__c' , amb.get('Primary_Role_'+i+'_Other__c'));
        
        }
        
        amb2.Involvment_with_PCORI__c = amb.Involvment_with_PCORI__c;
        
        amb2.Research_Phase_Experience__c = amb.Research_Phase_Experience__c;
        
        if(amb.Research_Phase_Experience__c.contains('Other'))
        amb2.Research_Phase_Experience_Other__c = amb.Research_Phase_Experience_Other__c;
        
        if(amb.Project_1_Type__c!=null || amb.Project_1_Type__c!=''){
          amb2.Project_1_Type__c = amb.Project_1_Type__c;
          amb2.Project_1_Name__c = amb.Project_1_Name__c;
          amb2.Name_of_Project_1_PI_PL__c = amb.Name_of_Project_1_PI_PL__c;
          amb2.Role_on_Project_1__c = amb.Role_on_Project_1__c;
            if(amb.Role_on_Project_1__c!=null && amb.Role_on_Project_1__c!=''){
              amb2.Open_Role_On_Project_1__c = amb.Open_Role_On_Project_1__c;  
            }  
        }
        if(amb.Project_2_Type__c!=null || amb.Project_2_Type__c!=''){
          amb2.Project_2_Type__c = amb.Project_2_Type__c;
          amb2.Project_2_Name__c = amb.Project_2_Name__c;
          amb2.Name_of_Project_2_PI_PL__c = amb.Name_of_Project_2_PI_PL__c;
          amb2.Role_on_Project_2__c = amb.Role_on_Project_2__c;
            if(amb.Role_on_Project_2__c!=null && amb.Role_on_Project_2__c!=''){
              amb2.Open_Role_On_Project_2__c = amb.Open_Role_On_Project_2__c;  
            }  
        }
        if(amb.Project_3_Type__c!=null || amb.Project_3_Type__c!=''){
          amb2.Project_3_Type__c = amb.Project_3_Type__c;
          amb2.Project_3_Name__c = amb.Project_3_Name__c;
          amb2.Name_of_Project_3_PI_PL__c = amb.Name_of_Project_3_PI_PL__c;
          amb2.Role_on_Project_3__c = amb.Role_on_Project_3__c;
            if(amb.Role_on_Project_3__c!=null && amb.Role_on_Project_3__c!=''){
              amb2.Open_Role_On_Project_3__c = amb.Open_Role_On_Project_3__c;  
            }  
        }
        
        amb2.Enter_Short_Bio__c = amb.Enter_Short_Bio__c;
        
        amb2.Therapeutic_Areas_of_Interest__c= amb.Therapeutic_Areas_of_Interest__c;
        
        if(amb.Therapeutic_Areas_of_Interest__c.contains('Other or non-disease specific'))
        amb2.Therapeutic_Areas_of_Interest_Other__c=amb.Therapeutic_Areas_of_Interest_Other__c;
        
        amb2.Population_Representation__c= amb.Population_Representation__c;
        
        if(amb.Population_Representation__c.contains('Other (Please specify)'))
        amb2.Population_Representation_Other__c = amb.Population_Representation_Other__c;
        
        amb2.Where_do_you_currently_live__c= amb.Where_do_you_currently_live__c;
        
        if(amb2.Where_do_you_currently_live__c.contains('Other'))
        amb2.Where_do_you_currently_live_Other__c = amb.Where_do_you_currently_live_Other__c;
        
        amb2.Served_as_a_PCORI_Merit_Reviewer__c = amb.Served_as_a_PCORI_Merit_Reviewer__c;
        amb2.served_as_a_PCORI_Peer_Reviewer__c = amb.served_as_a_PCORI_Peer_Reviewer__c;
        amb2.served_on_a_PCORI_Advisory_Panel__c = amb.served_on_a_PCORI_Advisory_Panel__c;
        amb2.contact__c = u.contactId;
        amb2.Submitted__c = true; 
        amb2.Status__c = Label.Submitted;  
        insert amb2;
        attach(amb2.Id);    
        PageReference pageRef= new PageReference(label.AmbassadorsubmitbuttonLandingPage); 
        pageRef.setRedirect(true);
        return pageRef;
        }       
    }
    
    public void attach(Id ambassadorId){
       List<Attachment> InsertList = new List<Attachment>();
       List<Attachment> deleteList = new List<Attachment>();
        for(Attachment a :  getattachmentList()){
            Attachment att = new Attachment(Name=a.Name, Body=a.Body, ParentId=ambassadorId);
            InsertList.add(att);
            deleteList.add(a);
        }
        if(InsertList.size()>0 && InsertList!=null)
            insert InsertList;
        if(deleteList.size()>0 && deleteList!=null)
            delete deleteList;
    }
      
    public boolean validations(){
    boolean validForSubmission = True;
    if(checkResponse(amb.First_Name__c)==''){
    validForSubmission = False;
    firstNameStyle = label.css_value2;
    }
    if(checkResponse(amb.Last_Name__c)==''){
    validForSubmission = False;
    lastNameStyle = label.css_value2;
    }
    if(checkResponse(amb.Primary_Stakeholder_Type__c)==''){
    validForSubmission = False;
    primaryStakeholderStyle = label.css_value2;
    }
    if(checkResponse(amb.Participate_in_Mentor_Program__c)==''){
    validForSubmission = False;
    mentorprgrmStyle = label.css_value2;
    }
    if(checkResponse(amb.Primary_Stakeholder_Type__c).contains('Other')){
    if(checkResponse(amb.Primary_Stakeholder_Type_Other__c)==''){
    validForSubmission = False;
    primaryStakeholderOtherStyle = label.css_value2;
    }
    }
    if(checkResponse(amb.Involvment_with_PCORI__c)==''){
    validForSubmission = False;
    involvementWithPcoriStyle = label.css_value2;
    }
    if(checkResponse(amb.Research_Phase_Experience__c)==''){
    validForSubmission = False;
    researchPhaseExperienceStyle = label.css_value2;
    }
    if(checkResponse(amb.Research_Phase_Experience__c).contains('Other')){
    if(checkResponse(amb.Research_Phase_Experience_Other__c)==''){
    validForSubmission = False;
    researchPhaseExperienceOtherStyle = label.css_value2;
    }
    }
    if(checkResponse(amb.Enter_Short_Bio__c)==''){
    validForSubmission = False;
    enterShortBioStyle = label.css_value2;
    }
    if(checkResponse(amb.Therapeutic_Areas_of_Interest__c)==''){
    validForSubmission = False;
    therapeuticAreasofInterestStyle= label.css_value2;
    }
    if(checkResponse(amb.Therapeutic_Areas_of_Interest__c).contains('Other or non-disease specific')){
    if(checkResponse(amb.Therapeutic_Areas_of_Interest_Other__c)==''){
    validForSubmission = False;
    therapeuticAreasofInterestOtherStyle = label.css_value2;
    }
    }
    if(checkResponse(amb.Population_Representation__c)==''){
    validForSubmission = False;
    populationRepresentationStyle = label.css_value2;
    }
    if(checkResponse(amb.Population_Representation__c).contains('Other (Please specify)')){
    if(checkResponse(amb.Population_Representation_Other__c)==''){
    validForSubmission = False;
    populationRepresentationOtherStyle = label.css_value2;
    }
    }
    if(checkResponse(amb.Where_do_you_currently_live__c)==''){
    validForSubmission = False;
    whereDoYouCurrentlyLiveStyle = label.css_value2;
    }
    if(checkResponse(amb.Served_as_a_PCORI_Merit_Reviewer__c)==''){
    validForSubmission = False;
    servedasaPCORIMeritReviewer = label.css_value2;
    }
    if(checkResponse(amb.served_as_a_PCORI_Peer_Reviewer__c)==''){
    validForSubmission = False;
    servedasaPCORIPeerReviewer = label.css_value2;
    }
    if(checkResponse(amb.served_on_a_PCORI_Advisory_Panel__c)==''){
    validForSubmission = False;
    servedasaPCORIAdvisoryPanel = label.css_value2;
    }
    if(checkResponse(amb.Where_do_you_currently_live__c).contains('Other')){
    if(checkResponse(amb.Where_do_you_currently_live_Other__c)==''){
    validForSubmission = False;
    whereDoYouCurrentlyLiveOtherStyle = label.css_value2;
    }
    }
    if(checkResponse(amb.Primary_Role_1__c).contains('Other')){
    if(checkResponse(amb.Primary_Role_1_Other__c)==''){
    validForSubmission = False;
    primaryRole1OtherStyle = label.css_value2;
    }
    }
    if(checkResponse(amb.Primary_Role_2__c).contains('Other')){
    if(checkResponse(amb.Primary_Role_2_Other__c)==''){
    validForSubmission = False;
    primaryRole2OtherStyle = label.css_value2;
    }
    }
    if(checkResponse(amb.Primary_Role_3__c).contains('Other')){
    if(checkResponse(amb.Primary_Role_3_Other__c)==''){
    validForSubmission = False;
    primaryRole3OtherStyle = label.css_value2;
    }
    }
    return validForSubmission;
    }
    
    public String checkResponse(String str){
    if(string.ValueOf(str)==null || string.valueOf(str)==''){
    str='';
    }
    return str;
    }
    
    public void resetAllStrings(){
    servedasaPCORIAdvisoryPanel = '';
    servedasaPCORIPeerReviewer= '';
    servedasaPCORIMeritReviewer = '';
    enterShortBioStyle = '';
    whereDoYouCurrentlyLiveOtherStyle = '';
    whereDoYouCurrentlyLiveStyle = '';
    populationRepresentationOtherStyle = '';
    populationRepresentationStyle = '';
    therapeuticAreasofInterestOtherStyle = '';
    therapeuticAreasofInterestStyle = '';
    researchPhaseExperienceOtherStyle = '';
    researchPhaseExperienceStyle = '';
    involvementWithPcoriStyle = '';
    primaryStakeholderOtherStyle = '';
    primaryStakeholderStyle = '';
    lastNameStyle = '';
    firstNameStyle = '';
    mentorprgrmStyle= '';
    }
    
    public void uploadAttachment()
    {   system.debug('+++++ '+userAttachment);
        if(userAttachment!=null){
        if(userAttachment.ContentType == 'image/jpeg' || userAttachment.ContentType == 'image/pjpeg'){
            if(u.ContactId != null){
            attachment a = new attachment(name=userAttachment.Name,body=userAttachment.Body,ParentId=u.contactId,Description='Ambassador Form Attachment');
            insert a;
        }
        }
        else{
        system.debug('+++++ '+userAttachment.ContentType);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please upload a JPEG format file'));
        }   
        }
        if(getattachmentList().isEmpty()){
        canUpload=True;
    }else{
        canUpload=False;
    }
    }
    
    public List<attachment> getattachmentList(){
         List<Attachment> attachmentList = new List<Attachment>();
        if(u.contactId != null){
            attachmentList = [select Id, body, Name, Description from Attachment where ParentId=:u.contactId and Description='Ambassador Form Attachment'];
        }
        if(attachmentList.isEmpty()){
        canUpload=True;
    }else{
        canUpload=False;
    }
        return attachmentList;
    }
    
     public void deleteAttachment(){
         try{
             delete [Select Id from Attachment where Id=:deleteRecordId Limit 1];
         }catch(exception e){
             system.debug('Here is the deletion error '+e.getCause());
         }
         if(getattachmentList().isEmpty()){
        canUpload=True;
    }else{
        canUpload=False;
    }   
    }
  
}