@IsTest
public class Tasks_Test {
    private static testMethod void getTask(){        
        list<User> usrs    = TestDataFactory.getpartnerUserTestRecords(1,5);
        list<User> ussrs    = TestDataFactory.getstandardUserTestRecords(2);
        list<Account> acnt = TestDataFactory.getAccountTestRecords(1);
        RecordType rec = [SELECT Id FROM RecordType WHERE Name = 'Research Awards'];         
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = rec.Id;
        opp.Program_Officer__c = usrs[0].Id;
        opp.Contract_Administrator__c = ussrs[0].Id;
        opp.Contract_Coordinator__c = ussrs[0].Id;
        opp.PI_Name_User__c = ussrs[0].Id;
        opp.AO_Name_User__c = ussrs[0].Id;
        opp.Contract_Number__c = '32432432';
        opp.Contract_Execution_Date__c = Date.newInstance(2016, 07, 01);
        opp.Contracted_Budget__c = 223213123;
        opp.Name = 'Tst';
        opp.Full_Project_Title__c = 'Test';
        opp.AccountId = acnt[0].Id;
        opp.Program__c = 'Addressing Disparities';
        opp.PFA__c = 'Asthma';
        opp.PFA_Type__c = 'Broad';
        opp.Priority_Area__c = 'Pilot Projects';
        opp.StageName = 'Set Up';
        opp.Cycle__c = 'PPP';
        opp.Project_Start_Date__c = Date.newInstance(2016, 04, 21);
        opp.CloseDate = Date.newInstance(2016, 12, 21);
        opp.Application_Amount__c = 3432432;
        opp.Application_Number__c = '233';
        insert opp;
        Opportunity opp1 = [SELECT Id FROM Opportunity WHERE Name = 'Tst' LIMIT 1];
        FGM_Base__Benchmark__c bench = new FGM_Base__Benchmark__c();
        bench.Milestone_Name__c = 'Tst';
        bench.Milestone_Type__c = 'IRB';
        bench.Milestone_Status__c = 'Not Completed';
        bench.Milestone_ID2__c = 'Test';
        bench.FGM_Base__Due_Date__c = Date.newInstance(2016, 12, 30);
        bench.FGM_Base__Request__c = opp1.Id;
        insert bench;
        for(Integer i = 0; i < 10; i++){
            Task t = new Task();
            t.Subject = 'Test' + i;
            t.Status = 'Not Started';
            t.OwnerId = usrs[0].Id;
            t.Start_Date__c = Date.newInstance(2014, 07, 10);
            t.ActivityDate = Date.newInstance(2017, 07, 10);
            t.Priority = 'Medium';
            insert t;
        }
        
        
        Tasks__c tl= new Tasks__c();
        tl.TasksList__c=5;
        tl.Name='Number of tasks';
        insert tl;
        ApexPages.StandardController sc = new ApexPages.StandardController(bench);
        Tasks tsk = new Tasks(sc);
    }
    private static testMethod void getNext(){
        
        list<User> usrs    = TestDataFactory.getpartnerUserTestRecords(1,5);
        list<User> ussrs    = TestDataFactory.getstandardUserTestRecords(2);
        list<Account> acnt = TestDataFactory.getAccountTestRecords(1);
        RecordType rec = [SELECT Id FROM RecordType WHERE Name = 'Research Awards'];         
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = rec.Id;
        opp.Program_Officer__c = ussrs[0].Id;
        opp.Contract_Administrator__c = ussrs[0].Id;
        opp.Contract_Coordinator__c = ussrs[0].Id;
        opp.PI_Name_User__c = ussrs[0].Id;
        opp.AO_Name_User__c = ussrs[0].Id;
        opp.Contract_Number__c = '32432432';
        opp.Contract_Execution_Date__c = Date.newInstance(2016, 07, 01);
        opp.Contracted_Budget__c = 223213123;
        opp.Name = 'Tst';
        opp.Full_Project_Title__c = 'Test';
        opp.AccountId = acnt[0].Id;
        opp.Program__c = 'Addressing Disparities';
        opp.PFA__c = 'Asthma';
        opp.PFA_Type__c = 'Broad';
        opp.Priority_Area__c = 'Pilot Projects';
        opp.StageName = 'Set Up';
        opp.Cycle__c = 'PPP';
        opp.Project_Start_Date__c = Date.newInstance(2016, 04, 21);
        opp.CloseDate = Date.newInstance(2016, 12, 21);
        opp.Application_Amount__c = 3432432;
        opp.Application_Number__c = '233';
        insert opp;
        FGM_Base__Benchmark__c bench = new FGM_Base__Benchmark__c();
        bench.Milestone_Name__c = 'Tst';
        bench.Milestone_Type__c = 'IRB';
        bench.Milestone_Status__c = 'Not Completed';
        bench.Milestone_ID2__c = 'Test';
        bench.FGM_Base__Due_Date__c = Date.newInstance(2016, 12, 30);
        bench.FGM_Base__Request__c = opp.Id;
        insert bench;
        for(Integer i = 0; i < 10; i++){
            Task t = new Task();
            t.Subject = 'Test' + i;
            t.Status = 'Not Started';
            t.OwnerId = usrs[0].Id;
            t.Start_Date__c = Date.newInstance(2014, 07, 10);
            t.ActivityDate = Date.newInstance(2017, 07, 10);
            t.Priority = 'Medium';
            insert t;
        }
        Tasks__c tl= new Tasks__c();
        tl.TasksList__c=5;
        tl.Name='Number of tasks';
        insert tl;
        ApexPages.StandardController sc = new ApexPages.StandardController(bench);
        Tasks tsk = new Tasks(sc);
        tsk.next();
        Tasks tsk1 = new Tasks(sc);
        tsk1.page = 5;
        tsk1.next();
        tsk1.previous();
        Tasks tsk3 = new Tasks(sc);
        tsk3.page = 0;
        tsk3.previous();
    }
}