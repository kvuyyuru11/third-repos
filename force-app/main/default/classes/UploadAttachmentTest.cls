@isTest
public class UploadAttachmentTest {
    static testMethod void uploadTest() {        
        list<User> usrs    = TestDataFactory.getpartnerUserTestRecords(1,5);
        list<contact> con  = TestDataFactory.getContactTestRecords(1,5);
        System.runAs(usrs[0]) {               
            ApexPages.StandardController ctrl = new ApexPages.StandardController(con[0]);
            UploadAttachmentController NAC = new UploadAttachmentController(ctrl);
            NAC.description = 'txt';
            NAC.fileBody = Blob.Valueof('New Text Document'); 
            NAC.fileName='test';
            NAC.saveStandardAttachment(con[0].id);
            Attachment att = [Select id from attachment LIMIT 1];
            NAC.processUpload(); 
            NAC.saveCustomAttachment(); 
            NAC.back();
        }
    }
}