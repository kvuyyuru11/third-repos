@IsTest
public class AmbassadorFormController_Test{
    public static testmethod void AmbassadorFormController_TestMethod1() { 
        // Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        // User user = [Select Id, name,ContactId,IsActive from user where ProfileId=:profileId And ContactId!=null And AccountId!=null And IsActive=True Limit 1];
        list<User> usrs    = TestDataFactory.getpartnerUserTestRecords(1, 15); 
        System.runAs(usrs[0]){
            AmbassadorFormController AFC = new AmbassadorFormController();
            //Ambassador__c amb = new Ambassador__c();            
            AFC.amb.Enter_Short_Bio__c='strsdsd';
            AFC.amb.Participate_in_Mentor_Program__c='No';
            AFC.amb.Research_Funding_of_Interest__c='Assessment of Prevention, Diagnosis, and Treatment';
            AFC.amb.Healthcare_Issues_of_Interest__c='Armed forces and family members';
            AFC.amb.Name_and_Date_of_PCORI_Event__c='asdasda';
            AFC.amb.Serve_as_Individual_Organizational_Rep__c='Individual';
            AFC.amb.Do_you_accept_the_PCORI_Ambassador_Progr__c='Yes';
            AFC.amb.Conditions_Diseases_are_of_Interest__c='Allergies and Immune Disorders';
            AFC.amb.Contact__c=usrs[0].contactId; 
            AFC.amb.Submitted__c=true; 
            AFC.amb.Status__c='Submitted';
            AFC.amb.Affiliated_Organization__c='Individual';
            insert AFC.amb;
            system.debug('here is the ambassador record '+ AFC.amb);
            system.assertequals(AFC.amb.Enter_Short_Bio__c,'strsdsd');
            AFC.submit();
            AFC.uploadAttachment();
            
        }        
    }
    
    public static testmethod void AmbassadorFormController_TestMethod2() { 
        // Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        // User user = [Select Id, name,ContactId,IsActive from user where ProfileId=:profileId And ContactId!=null And AccountId!=null And IsActive=True Limit 1];
        list<User> usrs    = TestDataFactory.getpartnerUserTestRecords(1, 15);        
        System.runAs(usrs[0]){
            AmbassadorFormController AFC = new AmbassadorFormController();
            //Ambassador__c amb = new Ambassador__c();            
            AFC.amb.Enter_Short_Bio__c='aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
            AFC.amb.Research_Funding_of_Interest__c='';
            AFC.amb.Healthcare_Issues_of_Interest__c='';
            AFC.amb.Name_and_Date_of_PCORI_Event__c='';
            AFC.amb.Serve_as_Individual_Organizational_Rep__c='';
            AFC.amb.Do_you_accept_the_PCORI_Ambassador_Progr__c='';
            AFC.amb.Conditions_Diseases_are_of_Interest__c='';
            AFC.amb.Contact__c=usrs[0].contactId; 
            AFC.amb.Submitted__c=true; 
            AFC.amb.Status__c='';
            AFC.amb.Affiliated_Organization__c='';
            AFC.amb.Participate_in_Mentor_Program__c='';
            insert AFC.amb;
            system.debug('here is the ambassador record '+ AFC.amb);            
            AFC.submit();            
        }        
    }
    
    public static testmethod void AmbassadorFormController_TestMethod3() { 
        //Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        //User user = [Select Id, name,ContactId,IsActive from user where ProfileId=:profileId And ContactId!=null And AccountId!=null And IsActive=True Limit 1];
        list<User> usrs    = TestDataFactory.getpartnerUserTestRecords(1, 15);         
        System.runAs(usrs[0]){
            AmbassadorFormController AFC = new AmbassadorFormController();
            //Ambassador__c amb = new Ambassador__c();            
            AFC.amb.Enter_Short_Bio__c=null;
            AFC.amb.Research_Funding_of_Interest__c='';
            AFC.amb.Healthcare_Issues_of_Interest__c='';
            AFC.amb.Name_and_Date_of_PCORI_Event__c='';
            AFC.amb.Serve_as_Individual_Organizational_Rep__c='';
            AFC.amb.Do_you_accept_the_PCORI_Ambassador_Progr__c='';
            AFC.amb.Conditions_Diseases_are_of_Interest__c='';
            AFC.amb.Contact__c=usrs[0].contactId; 
            AFC.amb.Submitted__c=true; 
            AFC.amb.Status__c='';
            AFC.amb.Affiliated_Organization__c='';
            AFC.amb.Participate_in_Mentor_Program__c='';
            insert AFC.amb;
            system.debug('here is the ambassador record '+ AFC.amb);            
            AFC.submit();            
        }        
    }
    
    public static testmethod void AmbassadorFormController_TestMethod4() { 
        //Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        // User user = [Select Id, name,ContactId,IsActive from user where ProfileId=:profileId And ContactId!=null And AccountId!=null And IsActive=True Limit 1];
        list<User> usrs    = TestDataFactory.getpartnerUserTestRecords(1, 15);         
        System.runAs(usrs[0]){
            AmbassadorFormController AFC = new AmbassadorFormController();
            //Ambassador__c amb = new Ambassador__c();           
            AFC.amb.First_Name__c = 'Test 1';
            AFC.amb.Last_Name__c = 'Test 1';
            AFC.amb.Primary_Stakeholder_Type__c = 'Other';
            AFC.amb.Primary_Stakeholder_Type_Other__c = '';
            AFC.amb.Involvment_with_PCORI__c = '0-2 years';
            AFC.amb.Research_Phase_Experience__c = 'Other';
            AFC.amb.Research_Phase_Experience_Other__c = '';
            AFC.amb.Enter_Short_Bio__c = 'Test';
            AFC.amb.Therapeutic_Areas_of_Interest__c = 'Other or non-disease specific';
            AFC.amb.Therapeutic_Areas_of_Interest_Other__c = '';
            AFC.amb.Population_Representation__c = 'Other (Please specify)';
            AFC.amb.Population_Representation_Other__c = '';
            AFC.amb.Where_do_you_currently_live__c = 'Other';
            AFC.amb.Where_do_you_currently_live_Other__c = '';
            AFC.amb.Primary_Role_1__c = 'Other';
            AFC.amb.Primary_Role_1_Other__c = 'Test';
            AFC.amb.Primary_Role_2__c = 'Other';
            AFC.amb.Primary_Role_2_Other__c = 'Test';
            AFC.amb.Primary_Role_3__c = 'Other';
            AFC.amb.Primary_Role_3_Other__c = 'Test';
            AFC.amb.Participate_in_Mentor_Program__c='No';
            AFC.amb.Contact__c=usrs[0].contactId;
            insert AFC.amb;
            AFC.submit();            
        }
                
    } 
    public static testmethod void AmbassadorFormController_TestMethod5() { 
        //Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        // User user = [Select Id, name,ContactId,IsActive from user where ProfileId=:profileId And ContactId!=null And AccountId!=null And IsActive=True Limit 1];
        list<User> usrs    = TestDataFactory.getpartnerUserTestRecords(1, 15);         
        System.runAs(usrs[0]){
            AmbassadorFormController AFC = new AmbassadorFormController();
            //Ambassador__c amb = new Ambassador__c();           
            AFC.amb.First_Name__c = 'test';
            AFC.amb.Last_Name__c = 'test';
            AFC.amb.Primary_Stakeholder_Type__c = 'Other';
            AFC.amb.Primary_Stakeholder_Type_Other__c = 'test';
            AFC.amb.Organizational_Affiliation_1__c = 'test';
            AFC.amb.Organizational_Affiliation_2__c = 'test';
            AFC.amb.Organizational_Affiliation_3__c = 'test';
     
            AFC.amb.Involvment_with_PCORI__c = '0-2 years';
            AFC.amb.Research_Phase_Experience__c = 'Other';
            AFC.amb.Research_Phase_Experience_Other__c = 'test';
            AFC.amb.Enter_Short_Bio__c = 'test';
            AFC.amb.Therapeutic_Areas_of_Interest__c = 'Other or non-disease specific';
            AFC.amb.Therapeutic_Areas_of_Interest_Other__c = '0-2 years';
            AFC.amb.Population_Representation__c = 'Other (Please specify)';
            AFC.amb.Population_Representation_Other__c = 'test';
            AFC.amb.Where_do_you_currently_live__c = 'Other';
            AFC.amb.Where_do_you_currently_live_Other__c = 'test';
            AFC.amb.Served_as_a_PCORI_Merit_Reviewer__c = 'Yes';
            AFC.amb.Served_as_a_PCORI_Peer_Reviewer__c = 'Yes';
            AFC.amb.Participate_in_Mentor_Program__c='No';
            AFC.amb.Served_on_a_PCORI_Advisory_Panel__c = 'Advisory Panel on Clinical Effectiveness and Decision Science';
            
            AFC.submit();
            AFC.amb.Where_do_you_currently_live__c = 'Other';
            AFC.amb.Primary_Role_1__c = 'Other';
            AFC.amb.Primary_Role_2__c = 'Other';
            AFC.amb.Primary_Role_3__c = 'Other';
            AFC.submit();
            AFC.deleteAttachment();            
        }
    }
}