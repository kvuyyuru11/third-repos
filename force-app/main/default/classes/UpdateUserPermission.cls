public class UpdateUserPermission {
    @InvocableMethod
    public static void AssignPermission(List<Id> mraIds){
        Set<Id> userSet = new Set<Id>();
        Set<Id> cSet = new Set<Id>();
        
        List<Merit_Reviewer_Application__c> mralst = [SELECT Id, Reviewer_Name__c FROM Merit_Reviewer_Application__c WHERE Id IN :mraIds];
        for(Merit_Reviewer_Application__c mra: mralst){
            cSet.add(mra.Reviewer_Name__c);
        }
        List<Contact> clst = [SELECT Id, FGM_Portal__User_Profile__c FROM Contact WHERE Id IN :cSet];
        for(Contact c :clst){
            userSet.add(c.Id);
        }
        List<User> usrlst = [SELECT Id, FGM_Portal__UserProfile__c FROM User WHERE ContactId IN :userSet AND isActive = true];
        List<Id> Ids = new List<Id>();
        for(User usr :usrlst){
            if(usr.FGM_Portal__UserProfile__c != null){
                usr.FGM_Portal__UserProfile__c = usr.FGM_Portal__UserProfile__c + ';Reviewer';
            }
            else {
                usr.FGM_Portal__UserProfile__c = 'Reviewer';
            }
            Ids.add(usr.Id);
        }
        update usrlst;
        AssignPermissionSet.assign(Ids);
    }
}