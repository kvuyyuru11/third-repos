/**
	* ContactSearchController - <description>
	* @author: Rainmaker
	* @version: 1.0
*/

public with sharing class AccountSearchController{
	
	private final Account accnt;
	// the soql without the order and limit
  private String soql {get;set;}
  public boolean enabled {get;set;}
  public string searchText{get;set;}
  // the collection of contacts to display
  public List<Account> accounts {get;set;}
 
  // the current sort direction. defaults to asc
  public String sortDir {
    get  { if (sortDir == null) {  sortDir = 'asc'; } return sortDir;  }
    set;
  }
 
  // the current field to sort by. defaults to last name
  public String sortField {
    get  { if (sortField == null) {sortField = 'name'; } return sortField;  }
    set;
  }
 
  // format the soql for display on the visualforce page
  public String debugSoql {
    get { return soql + ' order by ' + sortField + ' ' + sortDir + ' limit 20'; }
    set;
  }
 
  // init the controller and display some sample data when the page loads
 /* public ContactSearchController() {
    soql = 'select firstname, lastname, account.name from contact where account.name != null';
    runQuery();
  }*/
  
   public AccountSearchController(ApexPages.StandardController stdController) {
		 this.accnt = (Account)stdController.getRecord();
	   
	   enabled = true;
	   soql = 'Select A.Id, A.Name, A.BillingCity, A.BillingState, A.BillingStreet, A.BillingPostalCode from Account A where name != null';
    	runQuery();
    }
 
  // toggles the sorting of query from asc<-->desc
  public void toggleSort() {
    // simply toggle the direction
    sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
    // run the query again
    runQuery();
  }
 
  // runs the actual query
  public void runQuery() {
 
    try {
      accounts = Database.query(soql + ' order by ' + sortField + ' ' + sortDir + ' limit 20');
    } catch (Exception e) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'An error has occured. Please try again.'));
    }
 
  }
  
  public PageReference createNew()
	{
		PageReference pageRef = new PageReference('/001/e?retURL=%2F001%2Fo&nooverride=1&acc2='+ searchText);
		return pageRef;
	}
  
  public PageReference createNewA()
	{
		PageReference pageRef = new PageReference('/001/e?retURL=%2F001%2Fo&nooverride=1');
		//&name_lastcon2='+ searchLast + '&name_firstcon2=' + searchFirst);
		return pageRef;
	}
 
  // runs the search with parameters passed via Javascript
  public PageReference runSearch() {
 
    /*String firstName = Apexpages.currentPage().getParameters().get('firstname');
    String lastName = Apexpages.currentPage().getParameters().get('lastname');*/
    String accountName = Apexpages.currentPage().getParameters().get('accountName');
 
    soql = 'Select A.Id, A.Name, A.BillingCity, A.BillingState, A.BillingStreet, A.BillingPostalCode from Account A where name != null';
    /*if (!firstName.equals(''))
      soql += ' and firstname LIKE \''+String.escapeSingleQuotes(firstName)+'%\'';
    if (!lastName.equals(''))
      soql += ' and lastname LIKE \''+String.escapeSingleQuotes(lastName)+'%\'';*/
    if (!accountName.equals('')){
	  
		  if(accountName.contains('*')){
			  try{
				  System.debug('***** LOOP');
				  List<String> fields = accountName.split('\\*', 0);
				  System.debug('***** LOOP: ' + fields);
				  if(fields.size() > 0){
					  for(String str : fields){
						  if((str != null) || (str != '')){
							System.debug('***** LOOP str: ' + str);
					  		soql += ' and account.name LIKE \'%'+String.escapeSingleQuotes(str)+'%\'';
						  }
					  }
				  }
				  System.debug('***** LOOP SOQL: ' + SOQL);
			  }catch(Exception ex){
			  	System.debug('***** LOOP ERROR: ' + ex.getMessage());
			  }
			    
		  }else{
			  soql += ' and account.name LIKE \''+String.escapeSingleQuotes(accountName)+'%\'';  
		  }
      
  }   
    // run the query again
    runQuery();
	  
	enabled = false;
 
    return null;
  }

	

}