@isTest
public class postRefreshBatch_Test {
    static testmethod void testConstructor(){
        postrefreshbatch.createAccounts();
        postrefreshbatch.createCampaign();    
    } 
    @isTest
    static void testUpdateToCurrentEnvironment()
    {
        Id orgId = UserInfo.getOrganizationId();
        Id sandboxId = UserInfo.getOrganizationId();
 
        Test.startTest();
            Test.testSandboxPostCopyScript(new PostRefreshBatch(), orgId, sandboxId, 'sandbox');
        Test.stopTest();
 
       
 
        System.assertEquals(2, [Select count() from Account], 'Two Account Records should be created');
        System.assertEquals(1, [Select count() from Campaign], 'One CampaignRecord should be created');
    }
}