public class RAPrincipalInvestigatorHandlerClass {

    public  RAPrincipalInvestigatorHandlerClass(){}
    
//    public void insertCnt(Research_Application__c ra) {

      public void insertCnt(Opportunity ra) {
        list<Contact_Roles__c> ContactRolelst = new list<Contact_Roles__c>();
        Contact_Roles__c ContactRole = new Contact_Roles__c();  
//        ContactRole.Contact__c = ra.Principal_Investigator1__c;
        ContactRole.Principal_Investigator__c = ra.PI_Name_User__c;
//        ContactRole.Role__c ='Principal Investigator';
        ContactRole.Role__C = 'Principal Investigator (PI)';
        ContactRole.PCORI_Department__c ='Science';
        ContactRole.Engagement_Award__c = ra.ID;
        database.insert(ContactRole);
    }
    
}