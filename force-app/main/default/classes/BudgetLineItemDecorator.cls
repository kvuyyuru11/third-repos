public with sharing class BudgetLineItemDecorator {

	public Key_Personnel_Costs__c lineItem {get;set;}
	
	public Boolean isSelected {
		get {
			if(isSelected == null) {
				return false;
			} else {
				return isSelected;
			}
		}
		set;
	}

	public BudgetLineItemDecorator(Key_Personnel_Costs__c budgetLineItem) {
		this.lineItem = budgetLineItem;
	}

}