@IsTest
public class RRCSAverageReviewScoreController_test{    
    
    public static testmethod void RRCSAverageReviewScoreControllertest1() {
        Account acc = new account(Name='Test Account', Community__c='TBD');
        insert acc;
        Schema.DescribeSObjectResult oppsche = Schema.SObjectType.Opportunity;
        Map<string, schema.RecordtypeInfo> sObjectMap = oppsche.getRecordtypeInfosByName();
        Id stroppRecordTypeId = sObjectMap.get('RRCA Unlocked').getRecordTypeId();
        
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id sysprofileId = [select id from profile where name='System Administrator - Accenture'].id;
        
        opportunity o=new opportunity();
        o.name='testopp';
        o.StageName='Qualification';
        o.closedate=system.today()+5;
        o.accountid=acc.id;
        o.Full_Project_Title__c='testpms';
        o.Project_Name_off_layout__c='testpms';
        //o.Project_Name__c='testpm';
        o.Project_End_Date__c=system.today()+1;
        o.recordtypeid=stroppRecordTypeId;
        insert o;
        
        User user2 = new User(alias = 'test123', email='user345t@sgkmail234.com',
                              emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                              localesidkey='en_US', profileid = sysprofileId , country='United States',IsActive = true,     
                              timezonesidkey='America/New_York', username='user345t@sgkmail234.com');
        
        insert user2;
        
        
        Attachment a= new Attachment();
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        
        a.Body=bodyBlob;
        a.Name='RRCS-IPDMA : Test';
        a.ParentId=o.Id;
        insert a;
        
        FC_Reviewer__External_Review__c er = new FC_Reviewer__External_Review__c();
        er.FC_Reviewer__Opportunity__c = o.id;
        er.recordtypeId='01239000000HwBg';
        er.Topic_Value_Score__c='1 - Poor';
        er.Viable_IPD_MA_Collaboration_Score__c='2 - Fair'; 
        er.Project_Plan_and_Timeline_Score__c='1 - Poor';
        er.Personnel_Collaborators_Score__c='4 - Outstanding';
        er.Past_Performance_Score__c='4 - Outstanding';
        er.Budget_Cost_Proposal_Score__c='3 - Good';
        er.I_Do_Not_Have_Conflict_with_Application__c=true;
        er.Review_Complete__c = True;
        er.Internal_Reviewer__c = '00539000004H9El';
        insert er;
        
        FC_Reviewer__External_Review__c er2 = new FC_Reviewer__External_Review__c();
        er2.FC_Reviewer__Opportunity__c = o.id;
        er2.recordtypeId='01239000000HwBg';
        er2.Topic_Value_Score__c='1 - Poor';
        er2.Viable_IPD_MA_Collaboration_Score__c='2 - Fair'; 
        er2.Project_Plan_and_Timeline_Score__c='1 - Poor';
        er2.Personnel_Collaborators_Score__c='4 - Outstanding';
        er2.Past_Performance_Score__c='4 - Outstanding';
        er2.Budget_Cost_Proposal_Score__c='3 - Good';
        er2.I_Do_Not_Have_Conflict_with_Application__c=true;
        er2.Review_Complete__c = True;
        er2.Internal_Reviewer__c = '00570000002dmiQ';
        insert er2;
        
        apexpages.currentpage().getParameters().put('id',o.id);
        apexpages.standardcontroller stdcont = new apexpages.standardcontroller(o);
        
        RRCSAverageReviewScoreController avgc= new RRCSAverageReviewScoreController(stdcont);
    }
}