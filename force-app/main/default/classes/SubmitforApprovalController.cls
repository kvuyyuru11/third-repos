/*
* Class : SubmitforApprovalController
* Author : Kalyan Vuyyuru (PCORI)
* Version History : 1.0
* Creation : 5/3/2017
* Last Modified By: Kalyan Vuyyuru (PCORI)
* Last Modified Date: 5/24/2017
* Modification Description:Just aligned the text and ensured coverage is proper
* Description : This Class submits the RRCA application for approval to the AO. This class is fired by the custom submit for approval button on RRCA opportunity.
*/
global class SubmitforApprovalController {
  webservice static void submitRecords(Id oprtID){
        List<Opportunity> OppList = [select Id,PI_Name_User__c from Opportunity where Id=:oprtID];
        if(OppList!=null && OppList.size()>0){
            //Instantiate new approval object
        Approval.ProcessSubmitRequest aprov = new Approval.ProcessSubmitRequest();
        //Set the objectId that the approval process   
        aprov.setObjectId(OppList[0].Id);
        //Setting approval comments
        aprov.setComments('Submitted for approval. Please approve.');
            aprov.setProcessDefinitionNameOrId('RRCA_AO_Approval');
        //Setting the submitterId of the approvalObject
        aprov.SubmitterId = OppList[0].PI_Name_User__c;
        //Sending approval object to the approval process
        Approval.ProcessResult result = Approval.process(aprov);
            }}
       
        }