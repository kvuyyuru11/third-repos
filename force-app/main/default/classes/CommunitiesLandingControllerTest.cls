/**
* An apex page controller that takes the user to the right start page based on credentials or lack thereof
*/
@IsTest 
public class CommunitiesLandingControllerTest {
    
    public static testmethod void testCommunitiesLandingController() {
        // Instantiate a new controller with all parameters in the page
        CommunitiesLandingController controller = new CommunitiesLandingController();
        list<user> usrlist=TestDataFactory.getpartnerUserTestRecords(1,1);      
        System.runAs(usrlist[0]) {           
            controller.forwardToStartPage();
        }        
    }    
    
    public static testmethod void testCommunitiesLandingController1() {
        // Instantiate a new controller with all parameters in the page
        CommunitiesLandingController controller = new CommunitiesLandingController();
        
        list<user> usrlist=TestDataFactory.getpartnerUserTestRecords(1,1);
        contact c=[select Id,Registered__c from contact where Id=:usrlist[0].contactId];
        c.Registered__c=true;
        update c;
        System.runAs(usrlist[0]) {           
            controller.forwardToStartPage();
        }        
    }      
}