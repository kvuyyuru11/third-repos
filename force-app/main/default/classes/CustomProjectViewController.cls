//Controller and VF Page (MyProjectCustomView) written by Matt Gerry on 10/27/2016 at Accenture SADC Facility.
//This was created to satisfy requirementId 17105 in the PCORI Toolkit

Public with sharing Class CustomProjectViewController 
{    
    //Get list of views available from our custom setting projectViews object
    public List<ProjectViews__c> viewsList{get;set;}
    
    //Get list of ListViews
    public List<ListView> listIds;
    
    //Used to Id our currently chosen list view
    public String listNum{get;set;}
    
    //Used to set our listView
    public String selectedView{get;set;}
    
    //String used to check which record type page we're on
    public String recCheck;
    
    //Used to populate project type select list
    public List<SelectOption> listOptions{get;set;}

    //Constructor
    public CustomProjectViewController(ApexPages.StandardController controller)
    {
        //Grabbing all available list views from our custom settings Project views object
        viewsList = ProjectViews__c.getall().values();
        
        //Grabbing the recType variable from the URL
        recCheck = ApexPages.currentPage().getParameters().get('recType');
        
        //Instantiating our List of SelectOptions
        listOptions = new List<SelectOption>();  
        
        //Setting up our default list selected based on the URL variables
        if(recCheck == 'research')
        {
            listNum = 'My Research Awards Projects';
        }
        else if(recCheck == 'engage')
        {
            listNum = 'My Engagement Awards Projects';
        }
        //P2P code block Anudeep 
         else if(recCheck == 'P2P')
        {
            listNum = 'Pipeline to Proposal Awards';
        }
         else if(recCheck == 'P2PTierA')
        {
            listNum = 'P2P Tier A';
        }
        else if(recCheck == 'DI'){
             listNum = 'D&I Awards';
        }
        else if(recCheck == 'Infrastructure'){
             listNum = 'Infrastructure Awards';
        }
        else
        {
            listNum = 'All Projects';
        }
        
        //Grabbing all available lists for the recType
        listAll();
    }
    
    //Used to grab all lists available based on whether it is an egagement project or research project
    public void listAll()
    {
        //Grabbing ListView Id based on the selected list name
        listIds = [SELECT Id FROM ListView WHERE SobjectType = 'Opportunity' AND Name = :listNum];
        String viewId = listIds[0].Id;
        selectedView = viewId.substring(0,15);
        
        //Clearing our list view options and refilling them based on our recCheck URL variable
        listOptions.clear();
        if(recCheck == 'engage')
        {
            for(ProjectViews__c pv: viewsList)
            {
                if(pv.ViewLocation__c != 2 && pv.ViewLocation__c != 4 && pv.ViewLocation__c != 5 && pv.ViewLocation__c != 6 && pv.ViewLocation__c != 7)
                {
                    listOptions.add(new SelectOption(pv.OppType__c, pv.OppType__c));
                }
            }
        }
        else if(recCheck == 'DI')
        {
            for(ProjectViews__c pv: viewsList)
            {
                if(pv.ViewLocation__c != 2 && pv.ViewLocation__c != 1 && pv.ViewLocation__c != 5 && pv.ViewLocation__c != 6 && pv.ViewLocation__c != 7)
                {
                    listOptions.add(new SelectOption(pv.OppType__c, pv.OppType__c));
                }
            }
        }
        else if(recCheck == 'Infrastructure')
        {
            for(ProjectViews__c pv: viewsList)
            {
                if(pv.ViewLocation__c != 2 && pv.ViewLocation__c != 1 && pv.ViewLocation__c != 4 && pv.ViewLocation__c != 6 && pv.ViewLocation__c != 7)
                {
                    listOptions.add(new SelectOption(pv.OppType__c, pv.OppType__c));
                }
            }
        }
            //P2P Code block #Anudeep 
         else if(recCheck == 'P2P')
        {
            for(ProjectViews__c pv: viewsList)
            {
                if(pv.ViewLocation__c != 2 && pv.ViewLocation__c != 1 && pv.ViewLocation__c != 4 && pv.ViewLocation__c != 5 && pv.ViewLocation__c != 7)
                {
                    listOptions.add(new SelectOption(pv.OppType__c, pv.OppType__c));
                }
            }
        }
         else if(recCheck == 'P2PTierA')
        {
            for(ProjectViews__c pv: viewsList)
            {
                if(pv.ViewLocation__c != 2 && pv.ViewLocation__c != 1 && pv.ViewLocation__c != 4 && pv.ViewLocation__c != 5&& pv.ViewLocation__c != 6)
                {
                    listOptions.add(new SelectOption(pv.OppType__c, pv.OppType__c));
                }
            }
        }
         else if(recCheck =='research'){
             for(ProjectViews__c pv: viewsList)
            {
                if(pv.ViewLocation__c != 5 && pv.ViewLocation__c != 1 && pv.ViewLocation__c != 4 && pv.ViewLocation__c != 7 && pv.ViewLocation__c != 6)
                {
                    listOptions.add(new SelectOption(pv.OppType__c, pv.OppType__c));
                }
            }
        }
        else
        {
            for(ProjectViews__c pv: viewsList)
            {
               // if(pv.ViewLocation__c != 1)
               // {
                    listOptions.add(new SelectOption(pv.OppType__c, pv.OppType__c));
                //}
            }
        }
    }
}