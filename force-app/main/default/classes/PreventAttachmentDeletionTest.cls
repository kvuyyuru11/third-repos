/*
* Class : PreventAttachmentDeletionTest
* Author : Kalyan Vuyyuru (PCORI)
* Version History : 3.0
* Creation : 1/6/2017
* Last Modified By: Kalyan Vuyyuru (PCORI)
* Last Modified Date: 9/17/2018
* Modification Description:Ensured the Trigger has full coverage and also ensuring the new introduced check for one attachment per template is covered
*/
@isTest
public class PreventAttachmentDeletionTest {
    
    public static  testMethod  void Testattdel(){
        
        list<user> usrlist=TestDataFactory.getpartnerUserTestRecords(1, 1);
        list<opportunity> opplist=TestDataFactory.getopportunitytestrecords(1, 7, 1);
        opplist[0].ownerID=usrlist[0].Id;
        update opplist[0];
        Attachment attachTest = new Attachment(Name='Test');
        attachTest.body = Blob.valueOf('');
        attachTest.ParentID = opplist[0].Id;
        attachTest.OwnerID = usrlist[0].Id;
        insert attachTest;
        
        system.runAs(usrlist[0]){
            try{   
                delete attachTest;
            }catch (exception e){
            }       
        }
    }
    
    public static  testMethod  void TestInvoiceattdelwithoutdraft(){
        list<user> usrlist=TestDataFactory.getpartnerUserTestRecords(1, 1);
        list<user> stusrlist=TestDataFactory.getstandardUserTestRecords(4);
        list<opportunity> opplist=TestDataFactory.getopportunitytestrecords(1, 7, 1);
        opplist[0].ownerID=usrlist[0].Id;
        opplist[0].Contract_Administrator__c=stusrlist[2].ID;
        opplist[0].Program_Officer__c=stusrlist[3].ID;
        update opplist[0];
         KeyValueStore__c k1= new KeyValueStore__c();
        k1.Boolean__c=true;
        k1.Name='ProjectInvoiceCalculation';
        insert k1;
        Project_Invoice_Calculation__c p= new Project_Invoice_Calculation__c();
        p.Project__c=opplist[0].Id;
        insert p;
        KeyValueStore__c k= new KeyValueStore__c();
        k.Boolean__c=true;
        k.Name='InvoiceTrigger';
        insert k;
        test.startTest();
        Invoice__c inv= new Invoice__c();
        inv.ownerId=usrlist[0].Id;
        inv.Project__c=opplist[0].Id;
        inv.Invoice_Status_External__c='Submitted - Under Review';
        inv.Invoice_Status_Internal__c='CMA Review';
        inv.Finance_Reviewer__c=stusrlist[0].Id;
        inv.Contracts_Administrator__c=stusrlist[1].Id;
        inv.Version__c=0;
        insert inv;
        Attachment attachTest = new Attachment(Name='Test');
        attachTest.body = Blob.valueOf('');
        attachTest.ParentID = inv.Id;
        attachTest.OwnerID = usrlist[0].Id;
        insert attachTest;
        system.runAs(usrlist[0]){
            try{   
                delete attachTest;
            }catch (exception e){
            }       
        }
        test.stopTest();
    }
    
    public static  testMethod  void TestInvoiceattdelwithdraft(){
        list<user> usrlist=TestDataFactory.getpartnerUserTestRecords(1, 1);
        list<user> stusrlist=TestDataFactory.getstandardUserTestRecords(4);
        list<opportunity> opplist=TestDataFactory.getopportunitytestrecords(1, 7, 1);
        opplist[0].ownerID=usrlist[0].Id;
        opplist[0].Contract_Administrator__c=stusrlist[2].ID;
        opplist[0].Program_Officer__c=stusrlist[3].ID;
        update opplist[0];
        KeyValueStore__c k1= new KeyValueStore__c();
        k1.Boolean__c=true;
        k1.Name='ProjectInvoiceCalculation';
        insert k1;
        Project_Invoice_Calculation__c p= new Project_Invoice_Calculation__c();
        p.Project__c=opplist[0].Id;
        insert p;
        KeyValueStore__c k= new KeyValueStore__c();
        k.Boolean__c=true;
        k.Name='InvoiceTrigger';
        insert k;
        test.startTest();
        Invoice__c inv= new Invoice__c();
        inv.ownerId=usrlist[0].Id;
        inv.Project__c=opplist[0].Id;
        inv.Invoice_Status_External__c='Draft';
        inv.Invoice_Status_Internal__c='CMA Review';
        inv.Finance_Reviewer__c=stusrlist[0].Id;
        inv.Contracts_Administrator__c=stusrlist[1].Id;
        inv.Version__c=0;
        insert inv;
        
        Attachment attachTest = new Attachment(Name='Test');
        attachTest.body = Blob.valueOf('');
        attachTest.ParentID = inv.Id;
        attachTest.OwnerID = usrlist[0].Id;
        insert attachTest;
        
        system.runAs(usrlist[0]){
            try{   
                delete attachTest;
            }catch (exception e){
            }       
        }
        test.stopTest();
    }
    
    public static  testMethod  void Testattmethodafterinsert(){
        list<user> usrlist=TestDataFactory.getpartnerUserTestRecords(1, 1);
        list<opportunity> opplist=TestDataFactory.getopportunitytestrecords(1, 7, 1);
        opplist[0].ownerID=usrlist[0].Id;
        update opplist[0];
        system.runAs(usrlist[0]){
            Attachment attachTest = new Attachment(Name='Test');
            attachTest.body = Blob.valueOf('');
            attachTest.ParentID = opplist[0].Id;
            attachTest.OwnerID = usrlist[0].Id;
            insert attachTest;
            
        }
    }
    
    public static  testMethod  void Testattadditioninquiz(){
        list<user> usrlist=TestDataFactory.getpartnerUserTestRecords(1, 1);
        list<opportunity> opplist=TestDataFactory.getopportunitytestrecords(1, 7, 1);
        opplist[0].ownerID=usrlist[0].Id;
        update opplist[0];
        FGM_Portal__Question_Attachment__c f= new FGM_Portal__Question_Attachment__c();
        f.OwnerId=usrlist[0].Id;
        f.FGM_Portal__Opportunity__c=opplist[0].Id;
        f.Question_Text__c='People and Places';
        insert f;
        system.runAs(usrlist[0]){
            Attachment attachTest = new Attachment(Name='Test');
            attachTest.body = Blob.valueOf('');
            attachTest.ParentID = f.ID;
            attachTest.OwnerID = usrlist[0].Id;
            insert attachTest;
            try{
                Attachment attachTest2 = new Attachment(Name='Test2');
                attachTest2.body = Blob.valueOf('');
                attachTest2.ParentID = f.ID;
                attachTest2.OwnerID = usrlist[0].Id;
                insert attachTest2;
            }catch(exception e){}
            
        }
    }
    
}