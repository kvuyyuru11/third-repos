/*
* Apex Class : ChangeOwnerofRRCSrecords
* Author : Kalyan Vuyyuru (PCORI)
* Version History : 1.0
* Description : This Class is used to calculate the Average In-Person Score when the Calculate Average In-Person Score button is clicked on the Panel.
*/

global class CalculateInpersonAvgscores {
    
     webService static void calculatewocallingreviewscoresclass(String pId){
        
        list<opportunity> opprtlist=[select Id,Average_In_Person_Score__c from opportunity where Panel__c=:pId];
        
        if(!opprtlist.isEmpty() && opprtlist.size()>0){
            
            Map<Id, list<FC_Reviewer__External_Review__c>> reviewsandoptmap = new Map<Id, list<FC_Reviewer__External_Review__c>>();
            list<opportunity> opportunitieslisttoupdate= new list<opportunity>();
            double totalInPersonScore = 0;
            double totalInPersonReviews = 0;
            double newInPersonScore = 0;
            
            List<FC_Reviewer__External_Review__c> reviewslistforopportunities = [SELECT id, name, Overall_Score__c, RecordTypeDevName__c, FC_Reviewer__Status__c,FC_Reviewer__Opportunity__c
                                                                                 FROM FC_Reviewer__External_Review__c
                                                                                 WHERE FC_Reviewer__Opportunity__c IN: opprtlist];
            
            for(FC_Reviewer__External_Review__c reviewrcrd:reviewslistforopportunities){
                if(!reviewsandoptmap.containsKey(reviewrcrd.FC_Reviewer__Opportunity__c)) {
                    list<FC_Reviewer__External_Review__c> temp = new list<FC_Reviewer__External_Review__c>();
                    temp.add(reviewrcrd);
                    reviewsandoptmap.put(reviewrcrd.FC_Reviewer__Opportunity__c,temp);
                }
                else
                {
                    list<FC_Reviewer__External_Review__c> temp = reviewsandoptmap.get(reviewrcrd.FC_Reviewer__Opportunity__c);
                    temp.add(reviewrcrd);
                    reviewsandoptmap.put(reviewrcrd.FC_Reviewer__Opportunity__c,temp);
                }
            }
            
            for (Opportunity theOpp : opprtlist){
                
                for (FC_Reviewer__External_Review__c relatedReview : reviewsandoptmap.get(theOpp.Id))
                {
                    if (relatedReview.FC_Reviewer__Status__c != 'Inactive Review')
                    {
                        // figure out if we need to make it a in-person or online vote. if it isnt in-person, it is one of the online types
                        if (relatedReview.RecordTypeDevName__c == 'In_Person_Review' || relatedReview.RecordTypeDevName__c == 'In_Person_Preview')
                        {
                            // its in-person
                            if ((relatedReview.FC_Reviewer__Status__c == 'Review Submitted' || relatedReview.FC_Reviewer__Status__c == 'Discussed') && !String.isBlank(relatedReview.Overall_Score__c)) 
                            {
                                totalInPersonReviews++;
                                totalInPersonScore += integer.valueof(relatedReview.Overall_Score__c.trim());
                            }
                        }
                        
                    }       
                    
                }
                
                if (totalInPersonReviews != 0)
                {
                    newInPersonScore = ((totalInPersonScore / totalInPersonReviews) * 10);
                }
                
                Opportunity oppToUpdate = theOpp.clone(false, true);        
                oppToUpdate.Id = theOpp.id;
                if(newInPersonScore!=null){
                    oppToUpdate.Average_In_Person_Score__c = newInPersonScore;
                }
                
                if((oppToUpdate.Average_In_Person_Score__c != theOpp.Average_In_Person_Score__c)){
                    opportunitieslisttoupdate.add(oppToUpdate); 
                }
                totalInPersonScore = 0;
                totalInPersonReviews = 0;
            }
            database.update(opportunitieslisttoupdate);
        }
        
    }

}