//Class contains all the budget line item trigger handler classes
public class BudgetLineItemTriggerHandler {

    /* The following trigger is not ideal.
       However, SalesForce imposes a 10-25 rollup-summary field limit,
       making this trigger the only way to compute the sum of the budget line
       item fields.
    */
    public class CalculateBudgetTotals implements Triggers.HandlerInterface {

        //CostCategory to Field Mapping
        Map<String,String> costField = new Map<String,String>();

        public CalculateBudgetTotals() {
            this.costField.put('Key Personnel','Personnel_Costs_Year_');
            this.costField.put('Consultant Cost','Consultant_Costs_Year_');
            this.costField.put('Supplies','Supplies_Costs_Year_');
            this.costField.put('Scientific Travel','Scientific_Travel_Costs_Year_');
            this.costField.put('Programmatic Travel','Programmatic_Travel_Costs_Year_');
            this.costField.put('Other Expenses','Other_Expenses_Year_');
            this.costField.put('Equipment','Equipment_Costs_Year_');
            this.costField.put('Subcontractor Direct','Direct_Subcontractor_Costs_Year_');
            this.costField.put('Subcontractor Indirect','Subcontractor_Indirect_Costs_Year_');
        }

        private String getCostCategoryField(String costCategory, Decimal year) {
            return this.costField.get(costCategory) + year + '__c';
        }

        private String getBenefitsTotalField(Decimal year) {
            return 'PersonnelBudget_Year' + year + '_BT__c';
        }

        private String getSalaryTotalField(Decimal year) {
            return 'PersonnelBudget_Year' + year + '_ST__c';
        }

        public void handle() {

            //if the clone budget trigger is running don't run this trigger or the totals will be doubled
            if(BudgetTriggerHandler.cloningLineItems == true) {
                return;
            }

            Set<Id> budgetIds = new Set<Id>();
            List<Key_Personnel_Costs__c> changedLineItemList = new List<Key_Personnel_Costs__c>();
            String queryFields = '';
            String query;
            Map<Id, Budget__c> budgetMap;

            //following section gets the list of line items that have been changed
            //also gets the data needed to query the budgets that need to be updated
            if(!trigger.isDelete) {
                for(sObject o: trigger.new) {
                    Key_Personnel_Costs__c changedLineItem = (Key_Personnel_Costs__c) o;
                    String costCategory = changedLineItem.Cost_Category__c;
                    Decimal year = changedLineItem.Year__c;
                    String updateField = this.getCostCategoryField(costCategory, year);

                    if(changedLineItem.Bypass_Flow__c) {
                        continue;
                    }

                    //fields to be queried
                    if(!queryFields.containsIgnoreCase(updateField)) {
                        queryFields += updateField + ', ';
                        if(costCategory.equals('Key Personnel')) {
                            queryFields += getBenefitsTotalField(year) + ', ';
                            queryFields += getSalaryTotalField(year) + ', ';
                        }
                    }

                    changedLineItemList.add(changedLineItem);

                    budgetIds.add(changedLineItem.Budget__c);
                }

            }
            else {
                for(sObject o: trigger.old) {
                    Key_Personnel_Costs__c oldLineItem = (Key_Personnel_Costs__c) o;
                    //oldLineItem is read only so we need changedLineItem as an editable lineItem
                    Key_Personnel_Costs__c changedLineItem = new Key_Personnel_Costs__c();
                    String costCategory = oldLineItem.Cost_Category__c;
                    Decimal year = oldLineItem.Year__c;
                    String updateField = this.getCostCategoryField(costCategory, year);

                    if(oldLineItem.Bypass_Flow__c) {
                        continue;
                    }

                    //fields to be queried
                    if(!queryFields.containsIgnoreCase(updateField)) {
                        queryFields += updateField + ', ';
                        if(costCategory.equals('Key Personnel')) {
                            queryFields += getBenefitsTotalField(year) + ', ';
                            queryFields += getSalaryTotalField(year) + ', ';
                        }
                    }

                    changedLineItem.Budget__c = oldLineItem.Budget__c;
                    changedLineItem.Cost_Category__c = oldLineItem.Cost_Category__c;
                    changedLineItem.Year__c = oldLineItem.Year__c;

                    //make old values negative for key personnel cost category
                    if(oldLineItem.Salary_Requested__c != null) {
                        changedLineItem.Salary_Requested__c = oldLineItem.Salary_Requested__c * -1;
                    }
                    if(oldLineItem.Fringe_Benefits__c != null) {
                        changedLineItem.Fringe_Benefits__c = oldLineItem.Fringe_Benefits__c * -1;
                    }
                    changedLineItem.recalculateFormulas(); //for field Total__c which is a formula using above fields

                    //make old values negative for all other cost categories
                    if(oldLineItem.Total_1__c != null) {
                        changedLineItem.Total_1__c = oldLineItem.Total_1__c * -1;
                    }

                    changedLineItemList.add(changedLineItem);

                    budgetIds.add(oldLineItem.Budget__c);
                }
            }

            if(budgetIds.isEmpty()){
                return;
            }

            //query to get budgets that need to be updated; note the FOR UPDATE will lock these objects
            queryFields = queryFields.removeEnd(', ');
            query = 'SELECT Id, ' + queryFields + ' FROM Budget__C WHERE Id IN :budgetIds FOR UPDATE';
            budgetMap = new Map<Id, Budget__c>( (List<Budget__c>) Database.query(query));

            for(Key_Personnel_Costs__c changedLineItem : changedLineItemList) {
                Key_Personnel_Costs__c originalLineItem;
                Budget__c budget = budgetMap.get(changedLineItem.Budget__c);
                String costCategory = changedLineItem.Cost_Category__c;
                Decimal year = changedLineItem.Year__c;
                String categoryYearTotalField = this.getCostCategoryField(costCategory, year);
                Decimal changedAmount = 0;

                //instantiate originalLineItem values
                if(trigger.isUpdate) {
                    originalLineItem = (Key_Personnel_Costs__c) trigger.oldMap.get(changedLineItem.Id);
                }
                else {
                    //oldMap doesn't exist so create a fake originalLineItem
                    originalLineItem = new Key_Personnel_Costs__c();
                    originalLineItem.Total_1__c = 0;
                    originalLineItem.Salary_Requested__c = 0;
                    originalLineItem.Fringe_Benefits__c = 0;
                    originalLineItem.recalculateFormulas(); //for field Total__c which is a formula using above fields
                }

                //initialize cost category total field for the year if it is null
                if(budget.get(categoryYearTotalField) == null) {
                    budget.put(categoryYearTotalField, 0);
                }

                //Specific logic for key personnel cost category
                if(costCategory.equals('Key Personnel')) {
                    String benefitsTotalField = getBenefitsTotalField(year);
                    String salaryTotalField = getSalaryTotalField(year);
                    
                    //initialize benefits total field if null
                    if(budget.get(benefitsTotalField) == null) {
                        budget.put(benefitsTotalField, 0);
                    }

                    //initialize salary total field if null
                    if(budget.get(salaryTotalField) == null) {
                        budget.put(salaryTotalField, 0);
                    }
                    
                    //add new amount to old for key personnel cost category for the year
                    changedAmount = changedLineItem.Total__c - originalLineItem.Total__c;
                    budget.put(categoryYearTotalField, (Decimal) budget.get(categoryYearTotalField) + changedAmount);
                    budget.put(benefitsTotalField,
                        (Decimal) budget.get(benefitsTotalField) +
                        changedLineItem.Fringe_Benefits__c - originalLineItem.Fringe_Benefits__c);
                    budget.put(salaryTotalField,
                        (Decimal) budget.get(salaryTotalField) +
                        changedLineItem.Salary_Requested__c - originalLineItem.Salary_Requested__c);
                }
                //logic for all other cost categories
                else {
                    //add new amount to old for all other cost categories for the year
                    changedAmount = changedLineItem.Total_1__c - originalLineItem.Total_1__c;
                    budget.put(categoryYearTotalField, (Decimal) budget.get(categoryYearTotalField) + changedAmount);
                }
            }

            update budgetMap.values();
        }

    }

    /* Trigger Handler Class updates the comma separated list of Awardee changed fields
    */
    public without sharing class UpdateAwardeeChangedFields implements Triggers.HandlerInterface {

        //Mapping of all Key_Personnel_Costs__c fields
        private final Id PCORI_COMMUNITY_PARTNER_ID = System.Label.PCORI_Community_Partner_Id;
        private final Set<String> FIELD_SET = Key_Personnel_Costs__c.sObjectType.getDescribe().fields.getMap().keyset();

        private Set<String> fieldsToSkip;

        public void handle() {

            //create a set of known fields to skip over
            this.setFieldsToSkip();
			
            for(sObject o : trigger.new) {
                //Get new record
                Key_Personnel_Costs__c newLineItem = (Key_Personnel_Costs__c) o;

                if(newLineItem.Bypass_Flow__c) {
                    continue;
                }

                //Insert
				if(trigger.isInsert) {
                    this.handleInsert(newLineItem);
				}
                //Update
				else if(trigger.isUpdate) {
                    this.handleUpdate(newLineItem);
				}

            }
			
        }

        private void setFieldsToSkip() {
            this.fieldsToSkip = new Set<String>();
            this.fieldsToSkip.add('awardee_changed_fields__c'); //the field containing changed fields
            this.fieldsToSkip.add('isdeleted');                 //standard field; auto populated to false on insert
            this.fieldsToSkip.add('budget__c');                 //lookup
            this.fieldsToSkip.add('year__c');                   //should be auto populated
            this.fieldsToSkip.add('cost_category__c');          //should be auto populated
            this.fieldsToSkip.add('peer_review__c');            //Not changed by PCORI Community Partner user
        }

        private void handleInsert(Key_Personnel_Costs__c newLineItem){
            //if not the PCORI Community Partner user skip handling insert
            if(UserInfo.getProfileId() != PCORI_COMMUNITY_PARTNER_ID) {
                return;
            }

            //initial awardee changed fields
            newLineItem.Awardee_Changed_Fields__c = '';

            //add all fields that aren't blank or in field skip list
            for(String fieldName : this.FIELD_SET) {
                //Skip any fields we don't need to add
                if(this.fieldsToSkip.contains(fieldName)) {
                    continue;
                }
                newLineItem.Awardee_Changed_Fields__c += '\'' + fieldName + '\',';
            }

            //Remove trailing comma
            newLineItem.Awardee_Changed_Fields__c = newLineItem.Awardee_Changed_Fields__c.removeEnd(',');

        }

        private void handleUpdate(Key_Personnel_Costs__c newLineItem){
            //old line item data
            Key_Personnel_Costs__c oldLineItem = (Key_Personnel_Costs__c) trigger.oldMap.get(newLineItem.Id);

            if(String.isNotBlank(newLineItem.Awardee_Changed_Fields__c) && newLineItem.Awardee_Changed_Fields__c.equals('Remove Highlighting')) {
                newLineItem.Awardee_Changed_Fields__c = '';
                return;
            }

            //Get initial Awardee_Changed_Fields__c
            if(String.isNotBlank(oldLineItem.Awardee_Changed_Fields__c)) {
                    //for when any user makes a field change after the awardee has already made changes
                    newLineItem.Awardee_Changed_Fields__c = oldLineItem.Awardee_Changed_Fields__c + ',';
            } else {
                newLineItem.Awardee_Changed_Fields__c = '';
            }
            
            //Loop through all fields and check for changes
            for(String fieldName : this.FIELD_SET) {
                //Skip any fields we don't need to check for history changes
                if(this.fieldsToSkip.contains(fieldName)) {
                    continue;
                }
                //update awardee changed list if there is a field changed
                if(newLineItem.get(fieldName) != oldLineItem.get(fieldName))
                {
                    //Remove field if changed by someone else other than PCORI Community Partner user
                    if(UserInfo.getProfileId() != PCORI_COMMUNITY_PARTNER_ID) {
                        newLineItem.Awardee_Changed_Fields__c = newLineItem.Awardee_Changed_Fields__c.remove('\'' + fieldName + '\',');
                        //remove Total_1__c for Personnel Costs Category total updates
                        if( ( fieldName.equalsIgnoreCase('fringe_benefits__c') || fieldName.equalsIgnoreCase('salary_requested__c') )) {
                            newLineItem.Awardee_Changed_Fields__c = newLineItem.Awardee_Changed_Fields__c.remove('\'total_1__c\',');
                        }
                        //remove Description__c for Costs Categories that have a name updated
                        else if( ( fieldName.equalsIgnoreCase('name') || fieldName.equalsIgnoreCase('subcontractor_name__c') )) {
                            newLineItem.Awardee_Changed_Fields__c = newLineItem.Awardee_Changed_Fields__c.remove('\'description__c\',');
                        }
                    }
                    //Add field if changed by PCORI Community Partner user and it isn't already in the list
                    else if(!newLineItem.Awardee_Changed_Fields__c.containsIgnoreCase('\'' + fieldName + '\'')) {
                        newLineItem.Awardee_Changed_Fields__c += '\'' + fieldName + '\',';
                        //add Total_1__c for Personnel Costs Category total updates
                        if( ( fieldName.equalsIgnoreCase('fringe_benefits__c') || fieldName.equalsIgnoreCase('salary_requested__c') ) && !newLineItem.Awardee_Changed_Fields__c.containsIgnoreCase('\'total_1__c\'')) {
                            newLineItem.Awardee_Changed_Fields__c += '\'total_1__c\',';
                        }
                        //add Description__c for Costs Categories that have a name updated
                        else if( ( fieldName.equalsIgnoreCase('name') || fieldName.equalsIgnoreCase('subcontractor_name__c') ) && !newLineItem.Awardee_Changed_Fields__c.containsIgnoreCase('\'description__c\'')) {
                            newLineItem.Awardee_Changed_Fields__c += '\'description__c\',';
                        }
                    }
                }
            }
            
            //Remove trailing comma
			newLineItem.Awardee_Changed_Fields__c = newLineItem.Awardee_Changed_Fields__c.removeEnd(',');
        }
    }

}