/****************************************************************************************************
*Description:           Controler class for the portal Application page 2 vfp
*                       
*
*Required Class(es):    N/A
*
*Organization: Rainmaker-LLC
*Revision   |Date       |Author             |AdditionalNotes
*====================================================================================================
*   1.0     04/04/2014       Anirudh Sharma          Initial Version
*   1.01     1/10/2016      Daniel Haro       Bug fixes
*****************************************************************************************************/




public with sharing class AP_Application2Controller {
    public  Advisory_Panel_Application__c apa{get;set;}
    public List<Advisory_Panel_Application__c> myApp;
    public contact mycon{get;set;}
    public boolean test2{get;set;}
    public boolean test4{get;set;}
    public boolean testMode;

    
    
    ////MultiSelectPickList Variables
    public SelectOption[] selectedOptions1 { get; set; }
    public SelectOption[] allOptions1 { get; set; }
    public SelectOption[] selectedOptions2 { get; set; }
    public SelectOption[] allOptions2 { get; set; }
    public Boolean checkCount1{get;set;}
    public Boolean Identify{get;set;}
    public String values{get;set;}
    public String message { get; set; } 
    public String getValues{get;set;}
   
    public Boolean SaveFailed;
    public String holddata {get;set;}
    public Boolean validateMsg1 {get;set;}
    public Boolean showCareGiver{get;set;}
    public Boolean showClinical{get;set;}
    public Boolean showHospital{get;set;}
    public Boolean showPurchaser{get;set;}
    public Boolean showPayer{get;set;}
    public Boolean showIndustry{get;set;}
    public Boolean showPolicy{get;set;}
    public Boolean showIndustryHC{get;set;}
    public Boolean showTraining{get;set;}
    public String education{get;set;}
    public string degree{get;set;}
    Public boolean tyu {get;set;}
    public Advisory_Panel_Application__c pap {get;set;}
    public Contact cnt;
    public string style1{get;set;}
    public string style2{get;set;}
    public string style3{get;set;}
    public string style4{get;set;}
    public string style5{get;set;}
    public string style6{get;set;}
    public string style7{get;set;}
    public string style8{get;set;}
    public string style9{get;set;}
    public string style10{get;set;}
    public string style11{get;set;}
    public string style12{get;set;}
    public string style13{get;set;}
    public string style14{get;set;}
    public string style15{get;set;}
    public string style16{get;set;}
    public string style17{get;set;}
    public string style999{get;set;}
    public string style18{get;set;}
    public string style19{get;set;}
    public string style20{get;set;}
    public string style21{get;set;}
    public string style22{get;set;}
    public string style23{get;set;}
    public string style24{get;set;}
    public string style25{get;set;}
    public string style26{get;set;}
    public string style27{get;set;}
    public string style28{get;set;}
    public string style29{get;set;}   
    public string style30{get;set;} 
    public string style31{get;set;} 
    public string style32{get;set;} 
    public string style33{get;set;}
    public string style34{get;set;}
    public boolean z{get;set;} 
    public boolean c{get;set;}
    public boolean d{get;set;}
    public boolean wq{get;set;}
    public boolean re{get;set;}
    public boolean yt{get;set;}
    public boolean iu{get;set;}
    public boolean po{get;set;}
    public integer x;

   user us=[select id,contactid, Lastname from user where id=:userinfo.getUserId() limit 1];
    
    
     public AP_Application2Controller(){ 
         style1='';
         style2='';
         style3=''; 
         style4='';
         style5='';
         style6='';
         style7='';
         style8='';
         style9='';
         style10='';
         style11='';
         style12='';
         style13='';
         style14='';
         style15='';
         style16='';
         style17='';
         style18='';
         style19='';
         style20='';
         style21='';
         style22='';
         style23='';
         style24='';
         style25='';
         style26='';
         style27='';
         style28='';
         style29='';
         style30='';
         style31='';
         style32='';
         style33='';
         style34='';
         
         Id cid = us.contactid; //User queried in line 75. 
       
      mycon=[select id,Highest_Level_Of_Education__c,Degrees__c,Reviewer_Population_Expertise__c,Reviewer_Disease_Condition_Expertise__c from Contact where id=:cid limit 1];
   
     SaveFailed = false; 
     validateMsg1  = false;
     showCareGiver = true;
     showClinical = true;
     showHospital= true;
     showPurchaser= true;
     showPayer= true;
     showIndustry= false;
     showPolicy= false;
     showIndustryHC = false;
     showTraining = false;
     //blockid=false;
     //z=false; 
       
       apa = new Advisory_Panel_Application__c ();
         
                myApp= [SELECT Id,What_is_your_highest_level_of_education__c,Describe_multi_stakeholder_involvement__c,Describe_PCOR_Research__c,Engaged_in_PCOR_research__c,
                Describe_Engaging_Patients__c,Please_describe_your_involvement__c,Multi_stakeholder_processes_frameworks__c,Describe_data_research_involvement__c,
                Data_related_research_involvement__c,Development_of_Processes_or_Framework__c,Participated_in_Clinical_Trial__c,Describe_clinical_trial_involvement__c,
                Patient_centric_clinicals_involvment__c,Relevant_societies_or_organizations__c,Describe_PC_trial_involvement__c,Ever_engaged_in_PCORI_research__c,engaging_patients_and_other_stakeholders__c,Describe_research_prioritization_involve__c,
                Involved_in_Research_Prioritization__c,Primary_Rare_Disease_Community__c,Primary_Clinical_Trials_Community__c,Training_Institution_you_identify_with__c,
                Policy_Maker_category_you_identify_with__c,Health_Research_identified_with__c,Industry_categories_identified_with__c,Payer_categories_identified_with__c,
                Purchaser_categories_identified_with__c,Describe_PCORI_research_involvement__c,Hospital_Health_System_identified_with__c,Groups_identified_with_or_represented__c,Clinician_categories_identified_with__c,
                Caregiver_categories_identified_with__c,Other_categories__c,Categories_identified_with__c,Ranking_of_Advisory_Panels_applied_for__c,Advisory_Panels_Applied_For__c,
                Which_disease_condition_area_represented__c , Able_to_attend_a_one_to_two_day_meeting__c, Contact__c, Last_Name__c, Application_Status_New__c, 
                Abide_by_PCORI_COI_Policy__c,Degrees__c,Preferred_Advisory_Panel__c  from Advisory_Panel_Application__c 
                where CreatedBy.Id =:userinfo.getUserId() and Application_Status_New__c = 'Draft' order by CreatedDate DESC limit 1];

                
    if (myApp.size() > 0)
        {
            apa = myApp[0];
        }
        
         if(apa.Preferred_Advisory_Panel__c!=null){
         z=true;
         }
        if(apa.Primary_Clinical_Trials_Community__c!=null){
        c=true;
        } 
        if(apa.Primary_Rare_Disease_Community__c!=null){
        d=true;
        }
        if(apa.Patient_centric_clinicals_involvment__c!=null)
        wq=true;
        if(apa.Participated_in_Clinical_Trial__c!=null)
        re=true;
        if(apa.Multi_stakeholder_processes_frameworks__c!=null)
        yt=true;
        if(apa.Data_related_research_involvement__c!=null)
        iu=true;
        if(apa.Involved_in_Research_Prioritization__c!=null)
        po=true;
  
     selectedOptions1 = new List<SelectOption>();
        

   
 }//end of Constructor
 
   
 
    
    
    public String getValues()
   {
   return values;
   }
    
    //used to extract data from multi picklist
    
    public Void Identify()
        
    {
         List <String> listValues= new list <String>();
        //listvalues.add(apa.Advisory_Panels_Applied_For__c.split(';'));
        
        String temp = apa.Categories_identified_with__c;
      if(temp != null) 
        for(String value : temp.split(';'))
            listValues.add(value);
        
        values = 'list size: ' + listValues.size();
        Boolean a;
        //System.assert(true,listValues.size());
        if(listValues.size() > 0)
        for( String x : listValues)
        {
            If(x.equals('Patient/Consumer'))
                 a= true;
              
            If(x.equals('Caregiver/Family Member of Patient'))
                 showCareGiver = true;
            else showCareGiver = false;
            If(x.equals('Patient/Consumer'))
                 a= true;
               
            If(x.equals('Patient/Caregiver Advocate/Advocacy Org'))
            {
                showCareGiver = true;}
            else {showCareGiver = false;}
            If(x.equals('Clinician'))
            {showClinical = true;}
            else {showClinical = false;}
            If(x.equals('Hospital/Health System'))
            {showHospital = true;}
            else {showHospital = false;}
            If(x.equals('Purchaser'))
            {showPurchaser = true;}
            else  {showPurchaser = false;}
            If(x.equals('Payer'))
            {showPayer = true;}
            else {showPayer = false;}
          
            If(x.equals('Industry'))
                 showIndustry = true;
           else   showIndustry = false;
            
            If(x.equals('Health Research'))
                 showIndustryHC = true;
            else showIndustryHC = false;
            If(x.equals('Policy Maker'))
                 showPolicy = true;
            else showPolicy = false;
            If(x.equals('Training Institution'))
                 showTraining = true;
            else  showTraining = false;
        }
        
    }
    
    public void checkCount1()
        
    {
        Integer set1 = 0;
        z=false;
        c=false;
        d=false;
        wq=false;
        re=false;
        yt=false;
        iu=false;
        po=false;
            
                   if(apa.Advisory_Panels_Applied_For__c != null) 
           set1 += apa.Advisory_Panels_Applied_For__c.split(';').size();
           system.debug('set1 is'+set1);
          
        if( set1 >1){
            
           validateMsg1 = true;
            tyu = true;
        }
        else
        {
            validateMsg1 = false;
             tyu = false;
        }
        Integer set2=0;
        if(apa.Advisory_Panels_Applied_For__c != null){ 
         set2 = apa.Advisory_Panels_Applied_For__c.split(';').size();
         }
       if(set2>=2){
          z=true;
        }
        else{
        apa.Ranking_of_Advisory_Panels_applied_for__c='';
        }
        if(apa.Advisory_Panels_Applied_For__c!=null){
        if(apa.Advisory_Panels_Applied_For__c.contains('Clinical Trials')){
        c=true;
        }else{
        apa.Primary_Clinical_Trials_Community__c='';
        }        
        }
        if(apa.Advisory_Panels_Applied_For__c!=null){
        if(apa.Advisory_Panels_Applied_For__c.contains('Rare Disease')){
        d=true ;
        }else{
        apa.Primary_Rare_Disease_Community__c='';
        }
        }
        if(apa.Advisory_Panels_Applied_For__c!=null){
        if(apa.Advisory_Panels_Applied_For__c.contains('Clinical Trials')||apa.Advisory_Panels_Applied_For__c.contains('Patient Engagement')){
        wq=true;
        }else{
        apa.Patient_centric_clinicals_involvment__c='';
        }
        
        if(apa.Advisory_Panels_Applied_For__c.contains('Clinical Trials')||apa.Advisory_Panels_Applied_For__c.contains('Patient Engagement')){
        re=true;
        }else{
        apa.Participated_in_Clinical_Trial__c='';
         }
      
        if(apa.Advisory_Panels_Applied_For__c.contains('Patient Engagement')){
        yt=true;
        }else{
              apa.Multi_stakeholder_processes_frameworks__c='';
             }
      
        if(apa.Advisory_Panels_Applied_For__c.contains('Patient Engagement')){
        iu=true;
        }else{
              apa.Data_related_research_involvement__c='';
             }  
     
        //if(apa.Advisory_Panels_Applied_For__c.contains('Improving Healthcare Systems')||apa.Advisory_Panels_Applied_For__c.contains('APDTO')){
        if(apa.Advisory_Panels_Applied_For__c.contains('CEDS')||apa.Advisory_Panels_Applied_For__c.contains('HDDR')){
        po=true;
        }else{
              apa.Involved_in_Research_Prioritization__c='';
             } 
             }
     
    }
    
    //used to navigate to next page and save application data
    public pagereference Nextmethod() {
      
          
       
        style1='';
        style2='';
        style3=''; 
        style4='';
        style5='';
        style6='';
        style7='';
        style8='';
        style9='';
        style10='';
        style11='';
        style12='';
        style13='';
        style14='';
        style15='';
        style16='';
        style17='';
        style18='';
        style19='';
        style20='';
        style21='';
        style22='';
        style23='';
        style24='';
        style25='';
        style26='';
        style27='';
        style28='';
        style29='';
        style30='';
        style31='';
        style32='';
        style33='';
        style34='';
        x=0;
       
      
       if(apa.Advisory_Panels_Applied_For__c!=null)
        // if(apa.Advisory_Panels_Applied_For__c.contains('APDTO')||apa.Advisory_Panels_Applied_For__c.contains('Improving Healthcare Systems'))
         if(apa.Advisory_Panels_Applied_For__c.contains('CEDS')||apa.Advisory_Panels_Applied_For__c.contains('HDDR'))
        if(String.isBlank(apa.Involved_in_Research_Prioritization__c)){
            style1=label.css_value; 
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.')); 
      x=1;
         }
          if(apa.Advisory_Panels_Applied_For__c!=null)
         if(apa.Advisory_Panels_Applied_For__c.contains('Patient Engagement'))
        if(String.isblank(apa.Data_related_research_involvement__c)){
            style2=label.css_value; 
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.')); 
 x=1;
         }
        
    
         if(apa.Advisory_Panels_Applied_For__c!=null)
         if(apa.Advisory_Panels_Applied_For__c.contains('Patient Engagement'))
         if(String.isblank(apa.Multi_stakeholder_processes_frameworks__c)){
            style3=label.css_value; 
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
             x=1;
         }
        
         
         if(apa.Advisory_Panels_Applied_For__c!=null)
         if(apa.Advisory_Panels_Applied_For__c.contains('Clinical Trials')||apa.Advisory_Panels_Applied_For__c.contains('Patient Engagement'))
         if(String.isblank(apa.Participated_in_Clinical_Trial__c)){
            style4=label.css_value; 
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
             x=1;
         }
         
         if(apa.Advisory_Panels_Applied_For__c!=null)
         if(apa.Advisory_Panels_Applied_For__c.contains('Clinical Trials')||apa.Advisory_Panels_Applied_For__c.contains('Patient Engagement'))
         if(String.isBlank(apa.Patient_centric_clinicals_involvment__c)){
            style5=label.css_value; 
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
            x=1;
         }
         
         if(String.isblank(apa.Ever_engaged_in_PCORI_research__c)){
            style6=label.css_value; 
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
             x=1;
         }
        if(String.isBlank(apa.engaging_patients_and_other_stakeholders__c)){
            style7=label.css_value; 
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
            x=1;
        }
          if(String.isblank(apa.Engaged_in_PCOR_research__c)){
            style8=label.css_value; 
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
            x=1;
        }
        system.debug('qwerty'+apa);
        if(apa.Advisory_Panels_Applied_For__c!=null){
         if((apa.Advisory_Panels_Applied_For__c.contains('Rare Disease'))&&(String.isBlank(apa.Primary_Rare_Disease_Community__c))){
            style9='border-size:1px; border-color:red;border-style:solid;';
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
             x=1;
        }system.debug('qwerty2'+apa);
        }if(apa.Advisory_Panels_Applied_For__c!=null){
        if((apa.Advisory_Panels_Applied_For__c.contains('Clinical Trials'))&&(String.isBlank(apa.Primary_Clinical_Trials_Community__c))){
            style10='border-size:1px; border-color:red;border-style:solid;';
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
             x=1;
        }}
         if(String.isBlank(apa.Categories_identified_with__c)){
            style11='border-size:1px; border-color:red;border-style:solid;';
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
            x=1;
        }
         if(String.isblank(apa.Advisory_Panels_Applied_For__c)){
            style12='border-size:1px; border-color:red;border-style:solid;'; 
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
             x=1;
        }
        if(apa.Involved_in_Research_Prioritization__c!=null)
        if((apa.Involved_in_Research_Prioritization__c.equals('Yes'))&&(String.isblank(apa.Describe_research_prioritization_involve__c))){
            style13=label.css_value; 
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
             x=1;
        }
        if(apa.Data_related_research_involvement__c!=null)
          if((apa.Data_related_research_involvement__c.equals('Yes'))&&(String.isBlank(apa.Describe_data_research_involvement__c))){
            style14=label.css_value; 
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
             x=1;
        }
        if(apa.Multi_stakeholder_processes_frameworks__c!=null)
        if((apa.Multi_stakeholder_processes_frameworks__c.equals('Yes'))&&(String.isblank(apa.Describe_multi_stakeholder_involvement__c))){
            style15=label.css_value; 
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
             x=1;
        }
        if(apa.Participated_in_Clinical_Trial__c!=null)
         if((apa.Participated_in_Clinical_Trial__c.equals('Yes'))&&(String.isBlank(apa.Describe_clinical_trial_involvement__c))){
            style16=label.css_value; 
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
             x=1;
        }
      if(apa.Patient_centric_clinicals_involvment__c!=null)
         if((apa.Patient_centric_clinicals_involvment__c.equals('Yes'))&&(String.isblank(apa.Describe_PC_trial_involvement__c))){
            style17=label.css_value; 
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
             x=1;
        }
       if(apa.Ever_engaged_in_PCORI_research__c!=null)
         if((apa.Ever_engaged_in_PCORI_research__c.equals('Yes'))&&(String.isBlank(apa.Describe_PCORI_research_involvement__c))){
            style18=label.css_value; 
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
             x=1;
       }
        if(apa.engaging_patients_and_other_stakeholders__c!=null)
          if((apa.engaging_patients_and_other_stakeholders__c.equals('Yes'))&&(String.isBlank(apa.Describe_Engaging_Patients__c))){
            style19=label.css_value; 
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
             x=1;
        }
        if(apa.Engaged_in_PCOR_research__c!=null)
          if((apa.Engaged_in_PCOR_research__c.equals('Yes'))&&(String.isBlank(apa.Describe_PCOR_Research__c))){
            style20=label.css_value; 
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
             x=1;
        }
        if(apa.Categories_identified_with__c!=null){
         if((apa.Categories_identified_with__c.contains('Policy Maker'))&&(String.isblank(apa.Policy_Maker_category_you_identify_with__c))){
            style21='border-size:1px; border-color:red;border-style:solid;';
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
             x=1;
        
        }
        //if(apa.Categories_identified_with__c!=null){
        if((apa.Categories_identified_with__c.contains('Clinician'))&&(String.isblank(apa.Clinician_categories_identified_with__c))){
            style22='border-size:1px; border-color:red;border-style:solid;';
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
             x=1;
        }
        
        if((apa.Categories_identified_with__c.contains('Hospital/Health System'))&&(String.isBlank(apa.Hospital_Health_System_identified_with__c))){
            style23='border-size:1px; border-color:red;border-style:solid;';
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
             x=1;
        }
         if((apa.Categories_identified_with__c.contains('Caregiver/Family Member of Patient'))&&(String.isBlank(apa.Caregiver_categories_identified_with__c))){
            style24='border-size:1px; border-color:red;border-style:solid;'; 
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
             x=1;
        }
         if((apa.Categories_identified_with__c.contains('Purchaser'))&&(String.isBlank(apa.Purchaser_categories_identified_with__c))){
            style25='border-size:1px; border-color:red;border-style:solid;';
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
             x=1;
        }
          if((apa.Categories_identified_with__c.contains('Payer'))&&(String.isblank(apa.Payer_categories_identified_with__c))){
            style26='border-size:1px; border-color:red;border-style:solid;'; 
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
             x=1;
        }
         if((apa.Categories_identified_with__c.contains('Industry'))&&(String.isblank(apa.Industry_categories_identified_with__c))){
            style27='border-size:1px; border-color:red;border-style:solid;';
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
             x=1;
        }
          if((apa.Categories_identified_with__c.contains('Health Research'))&&(String.isBlank(apa.Health_Research_identified_with__c))){
            style28='border-size:1px; border-color:red;border-style:solid;'; 
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
             x=1;
        }
          if((apa.Categories_identified_with__c.contains('Training Institution'))&&(String.isBlank(apa.Training_Institution_you_identify_with__c))){
            style29='border-size:1px; border-color:red;border-style:solid;';
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
             x=1;
        }}
        if(String.isBlank(mycon.Highest_Level_Of_Education__c)){
            style30=label.css_value; 
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.')); 
      x=1;
         }
        /*if(mycon.Degrees__c==''||mycon.Degrees__c==Null){
            style31=label.css_value; 
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.')); 
      x=1;
         }*/
         if(String.isBlank(mycon.Degrees__c)){
            style31='border-size:1px; border-color:red;border-style:solid;';
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.')); 
      x=1;
         }
         if(String.isBlank(mycon.Reviewer_Population_Expertise__c)){
            style32='border-size:1px; border-color:red;border-style:solid;';
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.')); 
      x=1;
         }
         if(String.isBlank(mycon.Reviewer_Disease_Condition_Expertise__c)){
            style33='border-size:1px; border-color:red;border-style:solid;'; 
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.')); 
      x=1;
         }
         if(apa.Advisory_Panels_Applied_For__c!=null){
         if(apa.Advisory_Panels_Applied_For__c.split(';').size()>=2){
         if(String.isBlank(apa.Preferred_Advisory_Panel__c)){
            style34='border-size:1px; border-color:red;border-style:solid;'; 
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please rank your order of preference for the panels on which you would like to serve')); 
      x=1;
         }}}
       
        
        
         List<string>abc = new List <string>();
        if(apa.Advisory_Panels_Applied_For__c != null)
        {abc=apa.Advisory_Panels_Applied_For__c.split(';');}
         if(abc.size()>2){
         style12='border-size:1px; border-color:red;border-style:solid;';
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'You can apply to a maximum of two Advisory Panels. Please remove all Advisory Panel selections and re-select up to two Advisory Panels to ensure Panel-specific questions appear'));
         return null;
         }
         if(x==1){
            return null;
        } else{
        
        saveMethod(); 
        pagereference pg=new pagereference(label.AP_Application_3);
        return pg;
        }
    
    }
    
      public pagereference Previousmethod() {
        //saveMethod();  
         
          
        pagereference pg=new pagereference(label.AP_Application_Start);
        return pg;
        
    }

    //update application data without navigation
     public pagereference saveMethod() {
         style12='';
        
         user us=[select id,contactid, Lastname from user where id=:userinfo.getUserId() limit 1];
         Id cid = us.contactid;
         //mycon= [select id,Accountid,Highest_Level_Of_Education__c,Highest_Degree1__c,Reviewer_Population_Expertise__c,Reviewer_Disease_Condition_Expertise__c from contact where Id=:us.contactid]; 
        //myApp = [SELECT Id, Able_to_attend_a_one_to_two_day_meeting__c, Contact__c, Last_Name__c, Application_Status_New__c, Abide_by_PCORI_COI_Policy__c from Advisory_Panel_Application__c where CreatedBy.Id =:userinfo.getUserId() and Application_Status__c = 'Draft' order by CreatedDate DESC limit 1];
       
         
         
         contact cnt = [select id,Degrees__c,Gender_Research__c,Advisory_Panel_Applied_To__c,Exact_Age__c,Salutation,Reviewer_Population_Expertise__c,Reviewer_Disease_Condition_Expertise__c,Hispanic_Latino__c,Race__c,Current_Employer__c,Department,Highest_Level_Of_Education__c,Federal_Employee__c from Contact where id=:cid limit 1];
        
         
         cnt.Highest_Level_Of_Education__c=mycon.Highest_Level_Of_Education__c;
         cnt.Degrees__c =mycon.Degrees__c;
         cnt.Reviewer_Population_Expertise__c=mycon.Reviewer_Population_Expertise__c;
         cnt.Reviewer_Disease_Condition_Expertise__c=mycon.Reviewer_Disease_Condition_Expertise__c; 
         cnt.Advisory_Panel_Applied_To__c=APA.Advisory_Panels_Applied_For__c;
         update cnt;
        
        if(myApp.size()==0)
        {
        
            Advisory_Panel_Application__c saveAPA = new Advisory_Panel_Application__c ();
           // saveAPA.Abide_by_PCORI_COI_Policy__c = apa.Abide_by_PCORI_COI_Policy__c;
           // saveAPA.Able_to_attend_a_one_to_two_day_meeting__c = apa.Able_to_attend_a_one_to_two_day_meeting__c;
            saveAPA.Contact__c = us.contactId;
            saveAPA.Last_Name__c = us.Lastname;
             
                   saveAPA.What_is_your_highest_level_of_education__c = mycon.Highest_Level_Of_Education__c;
                 saveAPA.Degrees__c = mycon.Degrees__c;
                  saveAPA.Groups_identified_with_or_represented__c =mycon.Reviewer_Population_Expertise__c;
                  saveAPA.Which_disease_condition_area_represented__c = mycon.Reviewer_Disease_Condition_Expertise__c;
            //saveAPA.What_is_your_highest_level_of_education__c = apa.What_is_your_highest_level_of_education__c;
           //saveAPA.Application_Status_new__c = 'Draft';
            //saveAPA.SurveyGizmo_ID__c = '12345';
           List<string>abc = new List <string>();
            if(apa.Advisory_Panels_Applied_For__c != null)
            abc=apa.Advisory_Panels_Applied_For__c.split(';');
         if(abc.size()>2){
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'You can apply only 2 panels at a time'));
         }else{insert saveAPA;
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Your application has been saved as a draft.',''));
              }            
            
         
        }
         else{
             
            myApp[0].Contact__c = cnt.id;
            myApp[0].Last_Name__c = us.Lastname;
            myApp[0].Abide_by_PCORI_COI_Policy__c= apa.Abide_by_PCORI_COI_Policy__c;
            myApp[0].Able_to_attend_a_one_to_two_day_meeting__c= apa.Able_to_attend_a_one_to_two_day_meeting__c;
             myApp[0].What_is_your_highest_level_of_education__c = mycon.Highest_Level_Of_Education__c;
                  myApp[0].Degrees__c = mycon.Degrees__c;
              
                  myApp[0].Groups_identified_with_or_represented__c =mycon.Reviewer_Population_Expertise__c;
                  myApp[0].Which_disease_condition_area_represented__c = mycon.Reviewer_Disease_Condition_Expertise__c;
                  myApp[0].Advisory_Panels_Applied_For__c = apa.Advisory_Panels_Applied_For__c;
                  //myApp[0].Ranking_of_Advisory_Panels_applied_for__c = apa.Ranking_of_Advisory_Panels_applied_for__c;
                  myApp[0].Categories_identified_with__c = apa.Categories_identified_with__c;
                   myApp[0].Caregiver_categories_identified_with__c = apa.Caregiver_categories_identified_with__c;
                        myApp[0].Clinician_categories_identified_with__c = apa.Clinician_categories_identified_with__c;
                        myApp[0].Hospital_Health_System_identified_with__c = apa.Hospital_Health_System_identified_with__c; 
                        myApp[0].Purchaser_categories_identified_with__c = apa.Purchaser_categories_identified_with__c;
                        myApp[0].Payer_categories_identified_with__c = apa.Payer_categories_identified_with__c;
             myApp[0].Other_categories__c=apa.Other_categories__c;
             
             if(apa.Involved_in_Research_Prioritization__c!=null)
             if(apa.Involved_in_Research_Prioritization__c.equals('')||apa.Involved_in_Research_Prioritization__c.equals('No'))         
             apa.Describe_research_prioritization_involve__c='';
             if(apa.Data_related_research_involvement__c!=null)
             if(apa.Data_related_research_involvement__c.equals('')||apa.Data_related_research_involvement__c.equals('No'))
              apa.Describe_data_research_involvement__c=''; 
                if(apa.Multi_stakeholder_processes_frameworks__c!=null)
             if(apa.Multi_stakeholder_processes_frameworks__c.equals('')||apa.Multi_stakeholder_processes_frameworks__c.equals('No'))
                 apa.Describe_multi_stakeholder_involvement__c='';
             if(apa.Participated_in_Clinical_Trial__c!=null)
             if(apa.Participated_in_Clinical_Trial__c.equals('')||apa.Participated_in_Clinical_Trial__c.equals('No'))
                 apa.Describe_clinical_trial_involvement__c='';
              if(apa.Patient_centric_clinicals_involvment__c!=null)
             if(apa.Patient_centric_clinicals_involvment__c.equals('')||apa.Patient_centric_clinicals_involvment__c.equals('No'))
                 apa.Describe_PC_trial_involvement__c='';
              if(apa.Ever_engaged_in_PCORI_research__c!=null)
            if(apa.Ever_engaged_in_PCORI_research__c.equals('')||apa.Ever_engaged_in_PCORI_research__c.equals('No'))
                 apa.Describe_PCORI_research_involvement__c='';
             if(apa.engaging_patients_and_other_stakeholders__c!=null)
             if(apa.engaging_patients_and_other_stakeholders__c.equals('')||apa.engaging_patients_and_other_stakeholders__c.equals('No'))
                 apa.Describe_Engaging_Patients__c='';
             if(apa.Engaged_in_PCOR_research__c!=null)
             if(apa.Engaged_in_PCOR_research__c.equals('')||apa.Engaged_in_PCOR_research__c.equals('No'))
                 apa.Describe_PCOR_Research__c='';
             if(apa.Categories_identified_with__c!=null)
             if(!(apa.Categories_identified_with__c.contains('Caregiver/Family Member of Patient'))){
                apa.Caregiver_categories_identified_with__c='';
             }
             if(apa.Categories_identified_with__c!=null)
             if(!(apa.Categories_identified_with__c.contains('Clinician')))
                 apa.Clinician_categories_identified_with__c='';
             if(apa.Categories_identified_with__c!=null)
             if(!(apa.Categories_identified_with__c.contains('Hospital/Health System')))
                 apa.Hospital_Health_System_identified_with__c='';
             if(apa.Categories_identified_with__c!=null)
             if(!(apa.Categories_identified_with__c.contains('Purchaser')))
                    apa.Purchaser_categories_identified_with__c='';
             if(apa.Categories_identified_with__c!=null)
             if(!(apa.Categories_identified_with__c.contains('Payer')))
                 apa.Payer_categories_identified_with__c='';
             if(apa.Categories_identified_with__c!=null)
             if(!(apa.Categories_identified_with__c.contains('Industry')))
                 apa.Industry_categories_identified_with__c='';
             if(apa.Categories_identified_with__c!=null)
             if(!(apa.Categories_identified_with__c.contains('Health Research')))
               apa.Health_Research_identified_with__c='';
              if(apa.Categories_identified_with__c!=null)
             if(!(apa.Categories_identified_with__c.contains('Policy Maker')))
            apa.Policy_Maker_category_you_identify_with__c='';
             if(apa.Categories_identified_with__c!=null)
             if(!(apa.Categories_identified_with__c.contains('Training Institution')))
             apa.Training_Institution_you_identify_with__c='';
             
           /* if(myApp[0]!=null && apa.Advisory_Panels_Applied_For__c!=null)
            {
            List<string>abc=apa.Advisory_Panels_Applied_For__c.split(';');
         if(abc.size()>2){
             
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'You can apply only 2 panels at a time'));
         }
         else{
            update myApp[0];
            }
            
            }*/
            update myApp[0];
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Your application has been saved as a draft.',''));
            
        }
         
         
         
        
        
        /*Pagereference pg1 = new Pagereference('/' + saveAPA.id);
        pg1.setredirect(true);
        return pg1;*/
        
       return null;
    
    }
    
    
     /*public PageReference saveSet1() {
        message = 'Selected Contacts: ';
        Boolean first = true;
        for ( SelectOption so : selectedOptions1 ) {
            if (!first) {
                message += ', ';
            }
            message += so.getLabel() + ' (' + so.getValue() + ')';
            first = false;
        }
        
        return null;       
    }*/
    public void test1(){
    if(apa.Involved_in_Research_Prioritization__c.equals('Yes')){
    test2=true;
    }else{test2=false;}
    }
    
    
     
}