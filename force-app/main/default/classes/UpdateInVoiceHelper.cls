public without sharing  class UpdateInVoiceHelper {
public UpdateInVoiceHelper() {
        
    }
    // When AO/CMA changes at project level respective details invoices will be updated
     public void updateInvoiceDetails(map<id,Opportunity> oplist) {
         try{
         list<invoice__c> objInvoice=new  list<invoice__c>();
         for(invoice__c obj:[select id,Contracts_Administrator__c,Program_Officer__c,Project__c from invoice__c where Project__c in: oplist.keyset() and ((Invoice_Status_Internal__c='Finance Review') or(Invoice_Status_Internal__c= 'CMA Review') or (Invoice_Status_Internal__c='Program Officer Review' )or (Invoice_Status_external__c ='Draft' ))]){
             opportunity objopp=oplist.get(obj.Project__c);
             obj.Contracts_Administrator__c=objopp.Contract_Administrator__c;
             obj.Program_Officer__c=objopp.Program_Officer__c;
             objInvoice.add(obj);
         }
         if(objInvoice.size()>0){
             update objInvoice;
         }
         }
         catch(exception e){
             
         }
     }
    //Updates invoice checklist .In DetailInvoice controller as sharing rule applies it is updated here
     public boolean updateInvoiceCheckList(Invoice_Check_List__c invchk) {
         try{
            
             if(invchk!=null){
               upsert invchk;
                 
                  Invoice__c objinvupdate=new invoice__c();
                  if(string.isNotBlank(invchk.Contains_Budget__c) && string.isNotBlank(invchk.ContractNumber__c) && string.isNotBlank(invchk.Cumulative_Amount__c)&&
                      string.isNotBlank(invchk.Labor_Detail__c)&& string.isNotBlank(invchk.Match_Expenses__c)&& string.isNotBlank(invchk.Executed_Budget__c)&&
                      string.isNotBlank(invchk.Period_of_Performance__c)&& string.isNotBlank(invchk.Personnel_overrun__c) && string.isNotBlank(invchk.Prior_Period_Invoices__c)&&
                      string.isNotBlank(invchk.Travel_overrun__c)&& string.isNotBlank(invchk.Unbudgeted_Expenses__c) && string.isNotBlank(invchk.PCORI_online_Billing_Period__c))
                  {
                     
                    objinvupdate.Invoice_HasChecklist__c=true;
                    objinvupdate.id=invchk.invoice__c;
                    update objinvupdate;
                      return true;
                      }
                 else{
                    
                      objinvupdate.Invoice_HasChecklist__c=False;
                    objinvupdate.id=invchk.invoice__c;
                    update objinvupdate;
                   return false;
                 }

             }
             else{
                  return false;
             }
               
         }
         catch(exception e){
              return false;
         }
     }
    //Updates invoice checklist .In DetailInvoice controller as sharing rule applies it is updated here
    public void updateNotes(id pid,string notes){
        list<invoice__c> lstinv=new list<invoice__c>();
           for(invoice__c obj:[select id,Award_Level_notes_text__c from invoice__c where Project__c=:pid]){
               obj.Award_Level_notes_text__c=notes;
               lstinv.add(obj);
           }
           system.debug('==lstinv'+lstinv);
           update lstinv;
    }
     public void processRecord(invoice__c objin) {
        Approval.UnlockResult unlockedRersult = Approval.unlock(objin.id);
        // Iterate through each returned result
        if (unlockedRersult.isSuccess()) {
            // Operation was successful, so get the ID of the record that was processed
           
        }
        else {
            // Operation failed, so get all errors                
            for(Database.Error err : unlockedRersult.getErrors()) {
                
            }
        }
      
        
    }   
    
}