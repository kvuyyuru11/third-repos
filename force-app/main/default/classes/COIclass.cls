//This is a class that performs the business logic on the Conflict of interest page
public class COIclass {
    public COI_Expertise__c coi{get;set;}
    Public Id Panelid;
    string text1=Label.Unclear_if_COI_Exists;
    string text2=Label.Other;
    string NotValid;
    string CheckValidation;
    public string myid{get;set;}
    public String online{get;set;}
    public String other1{get;set;}
    public boolean ErrorMsg1{get;set;}
    public boolean ErrorMsg2{get;set;}
    public boolean ErrorMsg3{get;set;}
    public boolean ErrorMsg4{get;set;}
    public String Expertise{get;set;}
    public String PFA{get;set;}
    public boolean displayMessage1{get;set;}
    public boolean showadditionaltext{get;set;}
    public string textforPFA{get;set;}
    public string programname{get;private set;}
   RecordType rt1=[Select Id from RecordType where name=:Label.COI_PFA_Record_Type limit 1];
   Id myId3 = userinfo.getUserId();
        User U=[select id,ContactId from User where Id=:myId3];
        Contact c=[select id,PFA__c,Reviewer_Status__c from Contact where id=:u.ContactId];
    
     public COIclass(ApexPages.StandardController controller) {
      
        ErrorMsg1=false;
        Errormsg2=false;
        Errormsg3=false;
        Errormsg4=false;
        displayMessage1=false;
        showadditionaltext=false;
      
        
        //Gets the particular page id
        myID=ApexPages.currentPage().getParameters().get('id');
         
         COI_Expertise__c cets=[select id,Panel_R2__c, Panel_R2__r.Program_Lookup__r.name,Panel_R2__r.PFA__r.name  from COI_Expertise__c where id=:myID ];
         programname = cets.Panel_R2__r.PFA__r.name;
         Panelid=cets.Panel_R2__c;
         EditCOI();
        
    }
    
    //Below Items1 method is used for the Confict of Interest Question
    public List<SelectOption> getItems1() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('No','No-The reviewer has no COI with this application.')); 
        options.add(new SelectOption('Personal','Yes-The reviewer has a personal COI: The reviewer or his/her close relative has a significant personal relationship with the principal investigator or a key personnel.'));
        options.add(new SelectOption('Professional','Yes-The reviewer has a professional COI: The reviewer or his/her close relative has a significant professional relationship (employment related or not) with the principle investigator or key personnel.')); 
        options.add(new SelectOption('Institutional','Yes-The reviewer has an institutional COI: The reviewer or his/her close relative is employed by or seeking employment at the applicant entity, or the reviewer may receive professional gain or advancement as a direct result of the application funding decision.')); 
        options.add(new SelectOption('Financial','Yes-The reviewer has a financial COI: The reviewer or his/her close relative could receive a financial benefit as a result of the application funding decision.')); 
        options.add(new SelectOption('Unclear if COI Exists','Unclear if COI Exists.')); 
        return options; 
    } 
      //Below values method is used for the Expertise Question
    public List<SelectOption> getvalues() {
        List<SelectOption> options = new List<SelectOption>(); 
       Schema.DescribeFieldResult fieldResult = COI_Expertise__c.Expertise_Level__c.getDescribe();
   List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
   for( Schema.PicklistEntry f : ple)
   {
      options.add(new SelectOption(f.getLabel(), f.getValue()));
   } 
        return options; 
    }
    
    
    //This is a custom buttom logic method which needs to happen when submit button clicked on COI page
    public PageReference submit() {
        //The below soql query gets the COI recordid and assigns the values selected on the page to the object.
        coi= [Select id from COI_Expertise__c where id=:myid limit 1 ];
        coi.Conflict_of_Interest__c=online;
        coi.If_unclear_if_COI_exists_please_explain__c=other1;
        coi.Expertise_Level__c=expertise;
        coi.PFA_Level_COI__c=PFA;
        CheckValidation = validateValues();
        if(checkvalidation=='true')
        {
            return null;
        }
        else if(checkvalidation=='false'){
          
            coi.Submitted__c=true;
            database.update(coi);
 
        }
        
        
              Schema.DescribeSObjectResult anySObjectSchema = COI_Expertise__c.SObjectType.getDescribe();
String objectIdPrefix = anySObjectSchema.getKeyPrefix();
        string url='/apex/COIandExpertiseCustomView?id='+Panelid;
PageReference pageReference = new PageReference(url);
pageReference.setRedirect(true);
return pageReference;             
    }
    
    //This is a method that enforces validation rules on the COI Page.
    public string validateValues(){
          
            ErrorMsg2 = false;
            ErrorMsg3=false;
            ErrorMsg4=false;
            NotValid= 'false';
        
         if(online == text1 && (other1=='' || other1==null)){
            errormsg2=true;
            NotValid='true';
        }
        
        if(online == text1 && (other1=='' || other1==null) && (Expertise==null||Expertise=='')){
            errormsg2=true;
            errormsg3=true;
            NotValid='true';
        }
        
         if((Expertise==null||Expertise=='') && (online==null||online=='')){
            errormsg3=true;
            errormsg4=true;
            NotValid='true';
        }
         if(online==null||online==''){
            errormsg4=true;
            NotValid='true';
        }
         if(Expertise==null||Expertise==''){
            errormsg3=true;
            NotValid='true';
        }
       
         
        return NotValid;
    }


    
    
    public void updateValues(){
        
      
        if(online!= text1)
        {
          
            
            displayMessage1=false;
            other1='';
        }
        
        if(online == text1)
        {
            
            displayMessage1=true;
           
        }
        
       
         if(other1!=null){
ErrorMsg2=false;
        }
    }
    
  
    
   
    
     public void EditCOI(){
        COI_Expertise__c coi = new COI_Expertise__c();
        if(myId!=null){
            coi= [Select id,Conflict_of_Interest__c,Reviewer_Rationale__c,PFA_Level_COI__c,Submitted__c,If_unclear_if_COI_exists_please_explain__c,Expertise_Level__c from COI_Expertise__c where id=:myid limit 1 ];
            
            if(coi.Submitted__c==true){
               online=coi.Conflict_of_Interest__c;
               PFA=coi.PFA_Level_COI__c;
                other1=coi.If_unclear_if_COI_exists_please_explain__c;
                expertise=coi.Expertise_Level__c;  
            }
           
        }
    }
    
}