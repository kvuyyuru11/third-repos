/*----------------------------------------------------------------------------------------------------------------------------------------
Code Written at by SADC PCORI Team: Matt Hoffman
Written During Development Phase 2: June/2016 - September/2016
Application ID: AI-0908
Requirement ID: RAr2_C4_042

This class is only called by the Process Builder "Calculate Review Score". When a review is saved that is completed, this class is called
to recalculate the score for its Opportunity. This should not need bulkification, but can be changed to do so.

*Last Modified by Kalyan as initially this class was a process builder invocable class but due to some issues we have moved the process builder logic
to be called from trigger as it would be bulkified and also as it was process builder initially the code here was not bulkified with having soql inside for loop
which has been ensured that it is taken into maps to avoid hitting governor limits.

The portion modified by me is only to ensure I get rid of soql inside for loop and then did not touch any of the actual operation will mention below clearly what was touched from my end.
* Modified By: Kalyan Vuyyuru
* Modified Date: 06/06/2018
* Modified Desc:Modified the code to ensure the calculating of In person score works on the custom label Proceed with Trigger when it is set to true and also
removed the updating of Update worker which inturn updates opportunity and directly updating the opportunity to improve performance and score calculation correctly.
---------------------------------------------------------------------------------------------------------------------------------------*/
public class CalculateReviewScores {
    
    
    public static void calculateScore(List<Opportunity> oppList)
    {
        List<Updater_Worker__c> insertList = new List<Updater_Worker__c>();
        Map<Id, Id> insertMap = new Map<Id, Id>();
        Map<Id, list<FC_Reviewer__External_Review__c>> reviewsandoptmap = new Map<Id, list<FC_Reviewer__External_Review__c>>();
        list<opportunity> opportunitiestowork= new list<opportunity>();
        list<opportunity> opportunitieslisttoupdate= new list<opportunity>();
        double totalInPersonScore = 0;
        double totalOnlineScore = 0;
        double totalInPersonReviews = 0;
        double totalOnlineReviews = 0;
        double newInPersonScore = 0;
        double newOnlineScore = 0;
        
        boolean missingOnlineScore = false;
        //Kalyan modified code starts here
        List<FC_Reviewer__External_Review__c> reviewslistforopportunities = [SELECT id, name, Overall_Score__c, RecordTypeDevName__c, FC_Reviewer__Status__c,FC_Reviewer__Opportunity__c
                                                                             FROM FC_Reviewer__External_Review__c
                                                                             WHERE FC_Reviewer__Opportunity__c IN: oppList];
        
        for(FC_Reviewer__External_Review__c reviewrcrd:reviewslistforopportunities){
            if(!reviewsandoptmap.containsKey(reviewrcrd.FC_Reviewer__Opportunity__c)) {
                list<FC_Reviewer__External_Review__c> temp = new list<FC_Reviewer__External_Review__c>();
                temp.add(reviewrcrd);
                reviewsandoptmap.put(reviewrcrd.FC_Reviewer__Opportunity__c,temp);
            }
            else
            {
                list<FC_Reviewer__External_Review__c> temp = reviewsandoptmap.get(reviewrcrd.FC_Reviewer__Opportunity__c);
                temp.add(reviewrcrd);
                reviewsandoptmap.put(reviewrcrd.FC_Reviewer__Opportunity__c,temp);
            }
        }
        
        //opportunitiestowork=[select Id,Average_In_Person_Score__c, Average_Online_Review_Score__c, All_Reviews_Completed__c from opportunity where ID IN:reviewsandoptmap.keySet()];
        //Kalyan Modified code ends here
        for (Opportunity theOpp : oppList){
            
            for (FC_Reviewer__External_Review__c relatedReview : reviewsandoptmap.get(theOpp.Id))
            {
                // if this review is inactive, skip it
                if (relatedReview.FC_Reviewer__Status__c != 'Inactive Review')
                {
                    // figure out if we need to make it a in-person or online vote. if it isnt in-person, it is one of the online types
                    if (relatedReview.RecordTypeDevName__c == 'In_Person_Review' || relatedReview.RecordTypeDevName__c == 'In_Person_Preview')
                    {
                        if(System.label.ProceedwithTrigger=='true'){
                        // its in-person
                        if ((relatedReview.FC_Reviewer__Status__c == 'Review Submitted' || relatedReview.FC_Reviewer__Status__c == 'Discussed') && !String.isBlank(relatedReview.Overall_Score__c)) 
                        {
                            totalInPersonReviews++;
                            totalInPersonScore += integer.valueof(relatedReview.Overall_Score__c.trim());
                        }
                            }
                    }
                    else
                    {
                        // its online  
                        if (relatedReview.FC_Reviewer__Status__c == 'Review Complete' && !String.isBlank(relatedReview.Overall_Score__c))
                        {
                            totalOnlineReviews++;
                            totalOnlineScore += integer.valueof(relatedReview.Overall_Score__c.trim());  
                        }
                        // this just should be else
                        else if (relatedReview.FC_Reviewer__Status__c != 'Review Complete' || String.isBlank(relatedReview.Overall_Score__c))
                        {
                            missingOnlineScore = true;
                            System.debug('this review is online and not complete: ' + relatedReview.id);
                        }
                    }
                }               
            }
            
            if (totalOnlineReviews != 0)
            {
                newOnlineScore = ((totalOnlineScore / totalOnlineReviews) * 10);
            }
            else
            {
                newOnlineScore = null;
            }
            
            if (totalInPersonReviews != 0)
            {
                newInPersonScore = ((totalInPersonScore / totalInPersonReviews) * 10);
            }
            
            Opportunity oppToUpdate = theOpp.clone(false, true);        
            oppToUpdate.Id = theOpp.id;
            if(newInPersonScore!=null && newInPersonScore!=0){
            oppToUpdate.Average_In_Person_Score__c = newInPersonScore;
            }
            
            if (missingOnlineScore)
            {                   
                // one or more online review are not complete. set the scores to 0
                oppToUpdate.Average_Online_Review_Score__c = null;
                oppToUpdate.All_Reviews_Completed__c = 'No';                 
            }
            else
            {
                // update online scores
                if(newOnlineScore!=null){
                oppToUpdate.Average_Online_Review_Score__c = newOnlineScore;
                }else{
                oppToUpdate.Average_Online_Review_Score__c = newOnlineScore;
                }
                oppToUpdate.All_Reviews_Completed__c = 'Yes';
            }
            if ((oppToUpdate.Average_Online_Review_Score__c != theOpp.Average_Online_Review_Score__c )||
                (oppToUpdate.All_Reviews_Completed__c != theOpp.All_Reviews_Completed__c )||
                ((oppToUpdate.Average_In_Person_Score__c != theOpp.Average_In_Person_Score__c) && (System.label.ProceedwithTrigger=='true')))
            {
                opportunitieslisttoupdate.add(oppToUpdate); 
                // dont actually want to update opp through apex, which causes a lot of stuff to happen. instead, update through process builder
                //Updater_Worker__c newWorker = new Updater_Worker__c();
               // newWorker.Opportunity__c = theOpp.id;
                //newWorker.Operation_Number__c = 2;
               // newWorker.New_Reviews_Completed__c = oppToUpdate.All_Reviews_Completed__c;
                //newWorker.New_Online_Score__c = oppToUpdate.Average_Online_Review_Score__c;
                //newWorker.New_InPerson_Score__c = oppToUpdate.Average_In_Person_Score__c;
                //System.debug('making worker');
                //insertList.add(newWorker);
                
               // insertMap.put(theOpp.id, theOpp.id);
            }
           // else 
           // {
                //System.debug('no need for worker!');
           // }
             totalInPersonScore = 0;
            totalInPersonReviews = 0;
            totalOnlineScore=0;
            totalOnlineReviews=0;
        }
        update opportunitieslisttoupdate;
    }
}