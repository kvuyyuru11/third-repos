public with sharing class P2P_TriggerHelper {

	public static void updateP2PTierNameOnInsert(List<PipelineToProposal__c> pNewList) {
		if (pNewList.isEmpty()) {
			return;
		}
		Set<Id> projectIdSet = new Set<Id>();
		for (PipelineToProposal__c p2p : pNewList) {
			if (p2p.Projects__c != null) {
				projectIdSet.add(p2p.Projects__c);
			}
		}
		Map<Id, List<PipelineToProposal__c>> projP2pMap = getProjects(projectIdSet);
		Map<Integer, P2PSequentialitySettings__c> fieldSetMap = P2PFormsController.getCustomSettings();
		for (PipelineToProposal__c p2p : pNewList) {
			if (projP2pMap.get(p2p.Projects__c) == null) {
				System.debug('+++++++++++');
				p2p.Name = fieldSetMap.get(1).FormName__c;
				p2p.Types__c = fieldSetMap.get(1).FormName__c;
			} else if (fieldSetMap.get(Integer.valueOf(projP2pMap.get(p2p.Projects__c).size())) != null) {
				   	p2p.Name = fieldSetMap.get(Integer.valueOf(projP2pMap.get(p2p.Projects__c).size() + 1)).FormName__c;
					p2p.Types__c = fieldSetMap.get(Integer.valueOf(projP2pMap.get(p2p.Projects__c).size() + 1)).FormName__c;
			} 
		}
	}

	public static Map<Id, List<PipelineToProposal__c>> getProjects(Set<Id> pIdSet) {
		Map<Id, List<PipelineToProposal__c>> projP2pMap = new Map<Id, List<PipelineToProposal__c>>();
		List<PipelineToProposal__c> p2List;
		List<PipelineToProposal__c> p2pList = [SELECT Id, Name, Types__c, Projects__c FROM PipelineToProposal__c WHERE Projects__c IN:pIdSet];
		for (PipelineToProposal__c p2p : p2pList) {
			if (projP2pMap.get(p2p.Projects__c) != null) {
				p2List = projP2pMap.get(p2p.Projects__c);
				p2List.add(p2p);
			} else {
				p2List = new List<PipelineToProposal__c>();
				p2List.add(p2p);
				projP2pMap.put(p2p.Projects__c, p2List);
			}
		} 
		return projP2pMap;
	}

	public static void avoidDeletionOfP2P(List<PipelineToProposal__c> pOldList) {
		if (pOldList.isEmpty()) {
			return;
		}
		for (PipelineToProposal__c p2p:pOldList) {
			p2p.Id.addError('You will not be able to delete records');
		}
	}
}