public class apdcclass {

    
    public apdcclass(){
        
 
    }
    
   /* public void updatecnt(Advisory_Panel_Application__c u)
    {
        Advisory_Panel_Application__c cnt=[select Contact__c from Advisory_Panel_Application__c where id=:u.Id];
       
        contact c=[select Personal_Statement__c from Contact where id=:cnt.Contact__c];
        if(c.Personal_Statement__c==null){
        c.Personal_Statement__c=u.Personal_Statement2__c;
        }
        
        database.update(c);
    }*/
    
    public void updatecnt(List<Advisory_Panel_Application__c> panelAppList)
    {
        Map<Id,Advisory_Panel_Application__c> panelAppMap = new Map<Id,Advisory_Panel_Application__c>([select Id, Contact__c from Advisory_Panel_Application__c where id in :panelAppList]);
        Set<Id> contactIdSet = new Set<Id>();
        for(Advisory_Panel_Application__c tempPanel : panelAppMap.values()){
            contactIdSet.add(tempPanel.Contact__c);
        }
        Map<Id,contact> contactMap = new Map<Id,Contact>([select Id, Personal_Statement__c from Contact where id in :contactIdSet]);
        
        for(Advisory_Panel_Application__c tempPanel : panelAppList){
            if(contactMap.containsKey(panelAppMap.get(tempPanel.Id).Contact__c)){
                if(contactMap.get(panelAppMap.get(tempPanel.Id).Contact__c).Personal_Statement__c == null){
                    contactMap.get(panelAppMap.get(tempPanel.Id).Contact__c).Personal_Statement__c = tempPanel.Personal_Statement2__c;
                }
            }
        }
        database.update(contactMap.values());
    }
    }