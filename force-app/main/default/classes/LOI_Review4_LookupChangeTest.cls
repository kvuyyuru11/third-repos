@isTest
public class LOI_Review4_LookupChangeTest {

    public static testMethod void testLOIReview4Field(){
    List<Account> accts = ObjectCreator.getAccts('Jimbo', 10);
    insert accts;
        
    List<User> users = ObjectCreator.getUsers(10, 'Donnie');
    insert users;

    
     Id RA_LOI_ID = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('RA LOI').getRecordTypeId();
    Id campRecId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('PCORI Portal').getRecordTypeId();
    Id FLRF_ID = Schema.SObjectType.Fluxx_LOI_Review_Form__c.getRecordTypeInfosByName().get('AD LOI Review').getRecordTypeId();

    Campaign camp = new Campaign();
    camp.Name = 'Test';
    camp.RecordTypeId = campRecId;
    camp.PFA_Type__c = 'Broad';
    insert camp;
        
    Lead lead1 = new Lead(recordTypeID = RA_LOI_ID, Principal_Investigator__c = users[0].id, Administrative_Official__c = users[1].id,
                              Status = 'Draft', Project_Lead_Last_Name_Clone__c = 'testing', Company = 'testin Comp', PFA__c = camp.Id,
                                  LastName = 'BoJack', FirstName = 'Horseman', Zendesk__organization__c  = accts[0].Id);
    insert lead1;
        

    Test.startTest();

    Fluxx_LOI_Review_Form__c fl1 = new Fluxx_LOI_Review_Form__c();
    fl1.COI__c = 'No';
    fl1.Lead_LOI__c = lead1.Id;
    fl1.RecordTypeId = FLRF_ID;
    fl1.Reviewer_Label_RA__c = 'Reviewer 1';
    insert fl1;

    Fluxx_LOI_Review_Form__c fl2 = new Fluxx_LOI_Review_Form__c();
    fl2.COI__c = 'No';
    fl2.Lead_LOI__c = lead1.Id;
    fl2.RecordTypeId = FLRF_ID;
    fl2.Reviewer_Label_RA__c = 'Reviewer 2';
    insert fl2;

    Fluxx_LOI_Review_Form__c fl3 = new Fluxx_LOI_Review_Form__c();
    fl3.COI__c = 'No';
    fl3.Lead_LOI__c = lead1.Id;
    fl3.RecordTypeId = FLRF_ID;
    fl3.Reviewer_Label_RA__c = 'Reviewer 3';
    insert fl3;


    Fluxx_LOI_Review_Form__c fl4 = new Fluxx_LOI_Review_Form__c();
    fl4.COI__c = 'No';
    fl4.Lead_LOI__c = lead1.Id;
    fl4.RecordTypeId = FLRF_ID;
    fl4.Reviewer_Label_RA__c = 'Reviewer 4';
    insert fl4;
        
        
    Lead l = [select id,LOI_Review4__c,Review_Roll_up_4__c from Lead where id=: lead1.id ];
    System.assertEquals(l.LOI_Review4__c, fl4.Id);

    lead1.Review_Roll_up_4__c='3';
    update lead1;
    Lead l1 = [select id,LOI_Review4__c,Review_Roll_up_4__c from Lead where id=: lead1.id ];
    System.assertEquals(l1.LOI_Review4__c,fl3.Id);

    lead1.Review_Roll_up_4__c='6';
    update lead1;
    Lead l2 = [select id,LOI_Review4__c,Review_Roll_up_4__c from Lead where id=: lead1.id ];
    System.assertEquals(l2.LOI_Review4__c,null);
    Test.stopTest();
  
    }

}