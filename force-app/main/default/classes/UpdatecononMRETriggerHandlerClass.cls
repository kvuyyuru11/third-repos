//This is a trigger handler class that updates contact object fields based on Merit Reviewer Evaluation object updates
public class UpdatecononMRETriggerHandlerClass {
    
    string text1=Label.Mentor_Evaluation;
    string text2=Label.PCORI_Reviewer;
    string text3=Label.New_Reviewer;
    string text4=Label.Yes_completed_merit_review_and_participated_in_person;
    string text5=Label.Yes_but_participated_by_phone_during_merit_review;
     //String recordTypeName = text1; 
  //Map<String,Schema.RecordTypeInfo> rtMapByName = //Schema.SObjectType.Merit_Reviewer_Evaluation__c.getRecordTypeInfosByName();
  //Schema.RecordTypeInfo rtInfo =  rtMapByName.get(recordTypeName);
  //id recordTypeId = rtInfo.getRecordTypeId();
    
    public UpdatecononMRETriggerHandlerClass(){}
    
    public void updatecontact(Merit_Reviewer_Evaluation__c mtre){
        
         Merit_Reviewer_Evaluation__c mre = [Select Id,Reviewer_Name__c,recordtypeID,Panelist_complete_merit_review_cycle__c From Merit_Reviewer_Evaluation__c Where Id =:mtre.Id];
   Id contactid;
        RecordType RecType = [Select Id From RecordType  Where SobjectType = 'Merit_Reviewer_Evaluation__c' and DeveloperName = 'Reviewer_Evaluation'];
    List<Contact> cont = new List<Contact>();
   
       if(mre.recordtypeID==RecType.Id){
       contactid=mre.Reviewer_Name__c;
   system.debug(contactid);
     //From the above got Reviewer Name, below we query the reviewer status of that Contact Record and assign value based on below condition
      Contact cnt = [ Select Reviewer_Status__c From Contact Where Id = :Contactid];
    system.debug(cnt);
       
    if((mre.Panelist_complete_merit_review_cycle__c==(text4) && cnt.Reviewer_Status__c==(text3)) || (mre.Panelist_complete_merit_review_cycle__c==(text5) && cnt.Reviewer_Status__c==(text3))){
  cnt.Reviewer_Status__c=text2;
        cont.add(cnt);
               }
       }
 update cont;
       
   }
        
        
        
        
        
        
        
        
    }