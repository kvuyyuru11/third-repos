@istest
public class MRATRIGGERTEST {
 
    private static testmethod void MRAmethod(){
     string phonestyle;
     string streetstyle;
     string citystyle;
     string zipstyle;
     string statestyle1;
       // list<Merit_Reviewer_Application__c> myapp;
     list<Merit_Reviewer_Application__c> myapp1=new list<Merit_Reviewer_Application__c>();
     list<Merit_Reviewer_Application__c> myapp2=new list<Merit_Reviewer_Application__c>();
                RecordType recAcc = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Standard Salesforce Account'];

    User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1];
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
    Account acnt = new Account();
        acnt.RecordTypeId = recAcc.Id;
        acnt.Name = 'Test Acc';
        insert acnt;
    Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                              Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
    insert con;  
      User user = new User(alias = 'test123', email='test123xhk9@noemail.com',
      emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
      localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
      ContactId = con.Id,
      timezonesidkey='America/New_York', username='test123xhk9@noemail.com');
        
     insert user;
    list<contact> c;
             c = [select id,Age__c,MailingCity ,MailingCountry ,Current_Employer__c,Current_Position_or_Title__c,Federal_Employee__c,Gender__c,Race__c,MailingState,MailingPostalcode,phone,mailingstreet from Contact where id=:user.contactId limit 1];  
 
        //User u = [Select u.IsPortalEnabled, u.id, u.contactid, u.IsActive,u.profile.name From User u where u.isActive=true and  u.profile.name ='PCORI Community Partner' limit 1];
        System.RunAs(user) {
            Test.startTest();
               Merit_Reviewer_Application__c a = new Merit_Reviewer_Application__c();           
             for(contact mycon1:c){
         
                 a.Reviewer_Name__c=mycon1.id;
       a.Highest_Level_of_Education__c='';
      a.Highest_Degree_of_Education_New__c='';
       a.Number_of_Scientific_Panels__c='';
       a.Served_as_Chair_or_Co_Chair__c='';
       a.Number_of_Publications__c='';
       a.StakeholderCommunities_New__c='';
       a.patient_coommunities__c='';
       a.Age__c='21';
       a.City__c='test';
       a.Country__c='United States' ;
       a.Current_Employer__c='test';
       a.Current_Position_or_Title__c='test';
       a.Federal_Employee__c='No';
       a.Gender__c='male';
       //a.Patient_Communities__c='';
       a.Phone__c='1234567890';
       a.Race__c='Test Race';
        a.Reviewer_Role__c='Stakeholder';
        a.State__c='Virginia';
        a.Street_Address__c='electric avenue';
        a.Zip_Code__c='12345';
        a.Application_Status__c='Accepted as Reviewer';         
               myapp2.add(a);  
             }
           insert myapp2;
             system.debug('the log of record before************************************************************'+a.Reviewer_Role__c);
       /*  myapp=[select id,Application_Status__c,Name, Reviewer_Role__c,Age__c,Application_Reassignment_Notes__c,
              Highest_Level_of_Education__c,Highest_Degree_of_Education__c,Number_of_Scientific_Panels__c,
              Served_as_Chair_or_Co_Chair__c,Number_of_Publications__c,patient_coommunities__c,StakeholderCommunities_New__c
              from Merit_Reviewer_Application__c where Id=:a.id]; 
             system.debug('the log of record after************************************************************'+myapp);*/
            for(Merit_Reviewer_Application__c mrr:[select id,Application_Status__c,Name, Reviewer_Role__c,Age__c,Application_Reassignment_Notes__c,
              Highest_Level_of_Education__c,Highest_Degree_of_Education_New__c,Number_of_Scientific_Panels__c,
              Served_as_Chair_or_Co_Chair__c,Number_of_Publications__c,patient_coommunities__c,StakeholderCommunities_New__c
              from Merit_Reviewer_Application__c where Id=:a.id]){
        mrr.Reviewer_Role__c='Stakeholder'; 
        mrr.Highest_Level_of_Education__c='Master’s degree (MA, MS, MENG, MSW, etc.)';
        mrr.Highest_Degree_of_Education_New__c='RDH';
        mrr.Number_of_Scientific_Panels__c='test class1';
        mrr.Served_as_Chair_or_Co_Chair__c='test class1';
        mrr.Number_of_Publications__c='21-50';
        mrr.StakeholderCommunities_New__c='Clinician/Clinician Association';
        mrr.patient_coommunities__c='Patient, Consumer';
                       myapp1.add(mrr);
            }
           update myapp1;
        PageReference myVfPage = Page.MRAPage;
        Test.setCurrentPage(myVfPage);
        ApexPAges.StandardController sc = new ApexPages.StandardController(a);
        MeritTrack testController = new MeritTrack();
        testController .phonestyle='border-size:1px; border-color:red;border-style:Solid';
        testController .streetstyle='border-size:1px; border-color:red;border-style:Solid';
        testController .citystyle='border-size:1px; border-color:red;border-style:Solid';
        testController .zipstyle='border-size:1px; border-color:red;border-style:Solid';
        testController .statestyle1='border-size:1px; border-color:red;border-style:Solid';
        testController .citystyle1='border-size:1px; border-color:red;border-style:Solid';
        testController .zipstyle1='border-size:1px; border-color:red;border-style:Solid';
        testController .statestyle1='border-size:1px; border-color:red;border-style:Solid';
        testController .citystyle2='border-size:1px; border-color:red;border-style:Solid';
        testController .zipstyle2='border-size:1px; border-color:red;border-style:Solid';
        testController .statestyle2='border-size:1px; border-color:red;border-style:Solid';
        testController .statestyle11='border-size:1px; border-color:red;border-style:Solid'; 
        testController .Reviewerrole='Stakeholder';
        testController .otherspecify='';  
        testController.Nextmethod1();
        testController.Nextmethod();
        testController.previousmethod();
        testController.getstates(); 
        testController.other1();    
          // system.debug('the log of record after************************************************************'+  testController.Nextmethod1().a.Reviewer_Role__c);
    Test.stopTest();
        }
     
    }
}