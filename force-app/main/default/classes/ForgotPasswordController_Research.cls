/**
 * An apex page controller that exposes the site forgot password functionality
 */
public with sharing class ForgotPasswordController_Research {
    public String username {get; set;}   
       
    public ForgotPasswordController_Research() {}
    
    public PageReference forgotPassword() {
        boolean success = Site.forgotPassword(username);
        if(username.contains('@')){
        PageReference pr = Page.ForgotPasswordConfirm_Research;
        pr.setRedirect(true);
        
        if (success) {              
            return pr;
        }
            }
        else
        {
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please verify that you are entering the correct username in the form of an email address (username@company.com)');
            ApexPages.addMessage(msg);
            return null;
            }
        return null;
    }
}