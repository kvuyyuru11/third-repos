// This is a test class for the future class to cover it 100%
@isTest
public class FCfutureclasstest {
    
    public static testMethod void createFCdetails(){
        test.startTest();
        Profile Profile1 = [SELECT Id, Name FROM Profile WHERE Name = 'PCORI Community Partner'];
       
        Account ac = new Account(name ='henry') ;
        insert ac; 
        ac.ispartner=true;
        update ac;
       
        Contact con = new Contact(LastName ='testCon',AccountId = ac.Id, Email='testXVI@nomail.com',Community__c='Patient');
        insert con;  
                  
        User u = new User(alias = 'test123', email='test123dcv456@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = Profile1.id, country='United States',IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', username='test123dcv456@noemail.com');
       
        insert u;
        
        
        system.runAs(u){
        
            
        
        FCfutureclass.createFcdetails();
        test.stopTest();
        }
        
    }

}