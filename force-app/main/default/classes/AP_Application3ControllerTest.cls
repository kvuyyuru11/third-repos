@isTest
public  with sharing class AP_Application3ControllerTest {
public static testMethod void Mytest(){
  PageReference pageRef = new pagereference(label.AP_Application_3); //replace with your VF page name
  Test.setCurrentPage(pageRef);
    
  // String profileId = Label.CommunityAdminAccount;
         Profile Profile1 = [SELECT Id, Name FROM Profile WHERE Name = 'PCORI Community Partner'];
         String profileId = Profile1.id;
      String accountId = Label.CommunityAdminProfile;
      
      //Profile CProfile = [SELECT Id, Name FROM Profile WHERE Name ='Customer Community Login User'];
      Profile AProfile = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator'];
      //User u = [SELECT Id FROM User WHERE ProfileId =:AProfile.Id AND isActive = true LIMIT 1];
      User u = [SELECT Id FROM User WHERE ProfileId =:AProfile.Id AND isActive = true AND UserRoleId!=null LIMIT 1];
      User temp = new User();
      User user2 = new User();
    
    system.runAs(u){ 
    Account account = new Account(Name='Test');
        insert(account);
        Contact contact = new Contact(FirstName='Rainmaker', LastName='Test', AccountId=account.Id);
        insert(contact);
        temp.Email = 'testingghj654der@test.com';
        temp.ContactId = contact.Id;
        temp.UserName = 'testingghj654der@test.com';
        temp.LastName = 'Padilla';
        temp.Alias = 'jpadille';
        temp.CommunityNickName = 'jpadilla';
        temp.TimeZoneSidKey = 'GMT';
        temp.LocaleSidKey = 'en_US';
        temp.EmailEncodingKey = 'ISO-8859-1';
        temp.ProfileId = profileId;
        temp.LanguageLocaleKey = 'en_US';
       
        String userId = Site.createPortalUser(temp, accountId, 'Testing');
        
        Contact contact1 = new Contact(FirstName='Rainmaker1', LastName='Test1', AccountId=account.Id,Reviewer_Status__c='New Reviewer');
        insert(contact1); 
    
    }
   test.startTest(); 
   
    System.runAs(temp)
    {
               Contact tempContact = [select firstName,LastName,accountID from Contact where LastName = 'Test' limit 1];

  Advisory_Panel_Application__c myApp = new Advisory_Panel_Application__c();
         Advisory_Panel_Application__c tempApp = new Advisory_Panel_Application__c();
        tempApp.Contact__c = tempContact.id;
        tempApp.Application_Status_New__c = 'Draft';  
        tempApp.Preferred_Advisory_Panel__c = 'test';
        tempApp.Primary_Clinical_Trials_Community__c = 'test';
        tempApp.Primary_Rare_Disease_Community__c = 'test';
        tempApp.Patient_centric_clinicals_involvment__c = 'test';
        tempApp.Participated_in_Clinical_Trial__c = 'test';
        tempApp.Multi_stakeholder_processes_frameworks__c = 'test';
        tempApp.Data_related_research_involvement__c = 'test';
        tempApp.Involved_in_Research_Prioritization__c = 'test';
        tempApp.Bio__c = 'testing';
        tempApp.Personal_Statement2__c = 'testing';
        tempApp.Please_tell_us_anything_else__c = 'testing';
        tempApp.Additional_Supportive_Information__c = 'testing';
       // tempApp.
        database.insert(tempApp);
  
    
AP_Application3Controller testCon = new AP_Application3Controller();
    
    testCon.supportingDocuments1 = '';
    testCon.supportingDocuments2 = '';    
        testCon.supportingDocuments3 = '';
        testCon.supportingDocuments4 = '';
        testCon.supportingDocuments5 = '';
    testCon.apa = new Advisory_Panel_Application__c();    
    testCon.apa.Personal_Statement2__c = 'I like to test';
    testCon.apa.Bio__c = 'I like to test';   
    testCon.apa.Bio__c = 'I like to test';  
    testCon.apa.Permission_to_post_bios_on_website__c = 'YES';
    testCon.apa.Please_tell_us_anything_else__c = 'I love to test';
    testCon.apa.Additional_Supportive_Information__c = 'I love to test';
    //testCon.apa.
        
    testCon.Previousmethod();
    testCon.saveMethod();
        
        
        
    testCon.Nextmethod();
        
      
        
        
        testCon.getBioPermissions();
        
        
        
        
  //End Test Instruction for additional code coverage   
  //
         testCon.apa.Permission_to_post_bios_on_website__c = 'Yes';
        testCon.checkCount2();   
        testCon.apa.Personal_Statement2__c = '';
       testCon.apa.Permission_to_post_bios_on_website__c = '';
        testCon.x = 1;
        testCon.Nextmethod();
        testCon.biography = null;
        testCon.r = null;
        testCon.personalStatement = null;
        
        testCon.myApp.clear();
        testCon.saveMethod();
        testCon.biography = null;
        testCon.r = null;
        testCon.apa.Personal_Statement2__c = '';
        testCon.apa.Permission_to_post_bios_on_website__c = '';
        for(integer x = 0 ; x < 151 ; x++)
          testCon.apa.Bio__c = testCon.apa.Bio__c + '1234567890';
        for(integer x = 0 ; x < 341; x++)
           testCon.apa.Personal_Statement2__c = testCon.apa.Personal_Statement2__c + '1234567890';
       
         for(integer x = 0 ; x < 101; x++)
           testCon.apa.Please_tell_us_anything_else__c = testCon.apa.Please_tell_us_anything_else__c + '1234567890';
        
          for(integer x = 0 ; x < 1801; x++)
           testCon.apa.Additional_Supportive_Information__c = testCon.apa.Additional_Supportive_Information__c + '1234567890';
    
//        testCon.myApp[0] = testCon.apa;
                  //  system.assert(false, ' '+ testCon.apa.Bio__c.length());

        testCon.Nextmethod();
        testCon.saveMethod();
        testCon.Previousmethod();
        
        
        testCon.apa.Permission_to_post_bios_on_website__c = 'yes';
        testCon.apa.Bio__c = '';
        testCon.nextmethod();
        testCon.saveMethod();

        testCon.Previousmethod();
      //  Additional_Supportive_Information__c
        
   //testCon.updateMe();
        
      
    }  
     test.stopTest();
}
   
}