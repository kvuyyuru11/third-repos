public class Review_ServiceClass {
    
   public Review_ServiceClass() {}
    
    public static void updateReviwerDeadline(List<FC_Reviewer__External_Review__c> review){
    	Id pId = null;
        Datetime oDate = null;
        Id pRecordtypeId = null;
        set<Id> RecordtypeNames = new set<Id>();
        
        	
        	
        if(review.size()>0){ 
        	for(FC_Reviewer__External_Review__c setFRECR : review){
            	pId = setFRECR.FC_Reviewer__Opportunity__c;
                oDate = setFRECR.FC_Reviewer__Deadline__c;
                pRecordtypeId = setFRECR.RecordTypeId;
        	}
            
            For(RecordType rt : [Select Id, Name from RecordType where Name LIKE '%Online%']){
                RecordtypeNames.add(rt.Id);
            }
       		//Checking if Application Date is Null
        	if(oDate == null){
              if(RecordtypeNames.contains(pRecordtypeId)){
        				List<Opportunity> project = [SELECT Id, Online_Review_Deadline__c,Panel_MRO_Lookup_Formula__c from Opportunity where Id =: pId];
       
        				for(FC_Reviewer__External_Review__c updateFRECR: review){
            				updateFRECR.FC_Reviewer__Deadline__c = project[0].Online_Review_Deadline__c;
                            //Kalyan change of modifying starts here and just adding this line of code to ensure manual crerated reviews also have the panel MRO populated
                            //will change this class to bulkified later when assigned to finish the task.
                            updateFRECR.Panel_MRO__c=project[0].Panel_MRO_Lookup_Formula__c;
                            //Kalyan change of modifying code ends here.
        			}
             	}
        		
    		}   
        }
    }
}