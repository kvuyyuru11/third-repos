public  class TopicSummaryController {
    
public Topic__c topic { get; set;}   
public Contact c { get; set;}

public Id vendor_acct_id{get;set;}
public  String diseaseSelected;

public  List<String> populationSelected{get;set;}
public  String stakeholderSelected{get;set;}
public  List<String> StudyDesignSelected{get;set;}

public  String researchQuestion{get;set;}
public  String importantQuestion{get;set;}
public  String email1{get;set;}
public  String email2{get;set;}
public  String contactedSelected{get;set;}

public  String contactEmail{get;set;}
public  String scribeEmail{get;set;}

 
public boolean IsSaved {get;set;}

public  String otherDisease{get;set;}
public  String otherPopulation{get;set;}
public  String otherStudyDesign{get;set;}

public boolean showOtherDisease{get;set;}
public boolean showContact{get;set;}

//Submission_Page_Specific_Population__c
 
    public TopicSummaryController(Apexpages.Standardcontroller stdController)
    {
  
        StudyDesignSelected = new List<String>();
        populationSelected = new List<String>();
         
        topic = new Topic__c();
        isSaved = false;
        showOtherDisease = false;
        researchQuestion='';
        topic.Submission_Page_Email__c = '';
        email1 = topic.Submission_Page_Email__c;
        contactedSelected = 'No';
        System.debug('Test Save - In Controller issaved == ' + isSaved);
        contactEmail = null;
        System.debug('Test Email - In Controller  == ' + topic.Submission_Page_Email__c + ' email1 --> '  + email1);
        showContact = false;
        
    }
    
    public List<SelectOption> getDiseaseCondition()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        Schema.DescribeFieldResult fieldResult = Topic__c.Submission_Page_Disease_Condition__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple)
        {
            if(f.getValue() == 'Other (please specify)') continue;
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
            options.add(new SelectOption('Other (please specify)', 'Other (please specify)'));
            return options;
    }   
    
    public List<SelectOption> getPriorityPopulation()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        Schema.DescribeFieldResult fieldResult = Topic__c.Submission_Page_Specific_Population__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple)
        {
            if(f.getValue() == 'Other (please specify)') continue;
            options.add(new SelectOption(f.getValue(), f.getValue()));
        }       
            options.add(new SelectOption('Other (please specify)', 'Other (please specify)'));
            return options;
    }    
    
    public List<SelectOption> getStudyDesign()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        Schema.DescribeFieldResult fieldResult = Topic__c.Submission_Page_Study_Design__c.getDescribe();
        //Schema.DescribeFieldResult fieldResult = Topic__c.Study_Design__c.getDescribe();
        
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple)
        {
            if(f.getValue() == 'Other (please specify)') continue;
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        } 
            options.add(new SelectOption('Other (please specify)', 'Other (please specify)'));
            return options;
    }   
    
    public List<SelectOption> getStakeholdersGroup()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        Schema.DescribeFieldResult fieldResult = Topic__c.Submission_Page_Stakeholder__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
            return options;
    }   
    
    public void clearFields(){
        
        diseaseSelected = null;
        otherDisease = null;
        otherPopulation = null;
        StudyDesignSelected = new List<String>();
        otherStudyDesign = null;
        contactedSelected = 'No';
        email1 = null;
        stakeholderSelected = null;
        populationSelected = new List<String>();
        researchQuestion = null;        
        importantQuestion = null;
        
        //contacted = 'No';
        
    }
    
    public List<SelectOption> getContacted() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Yes','Yes'));
        options.add(new SelectOption('No','No'));

        return options;
    }   
    
    
    public PageReference submit() {
        
        System.debug('Topic selected researchQuestion ---> ' + researchQuestion);
        
        System.debug('Topic selected diseaseSelected ---> '  + diseaseSelected );
        System.debug('Topic selected populationSelected ---> '  + populationSelected );
        System.debug('Topic selected Study Design selected ---> '  + StudyDesignSelected);          
        System.debug('Topic stakeholderSelected selected ---> '  +stakeholderSelected );
        
        System.debug('Topic  otherDisease ---> '  + otherDisease);
        System.debug('Topic  otherPopulation ---> '  + otherPopulation);
        System.debug('Topic  otherStudyDesign ---> '  + otherStudyDesign);
        
        System.debug('Topic  contactedSelected ---> '  + contactedSelected);
        
        
        PageReference page = null;
        
        boolean error = false;
        
        System.debug('Test Save - In Save issaved == ' + isSaved);
        
        if (isSaved)
        {
            error = true;
            
            String errMsg = 'Thanks! Research Question has already been saved!';
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,errMsg));
        }       
        
        if(contactedSelected == 'Yes'){     
            
            showContact = true;
            if (contactEmail != null){
                
                boolean emailGood = validateEmail(contactEmail);
                if(!emailGood){
                    error = true;
            
                    String errMsg = 'Contact email is invalid!';
                    Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,errMsg));                   
                }
                
            }
                        
            
        } else {
            showContact = false;
            topic.Email__c = null;
            topic.Organization__c = null;
            topic.Street_Address__c = null;
            topic.Address_Line_2__c = null;
            topic.City__c = null;
            topic.Zip_Postal_Code__c = null;
            topic.Country__c = null;
        }
        
        if (topic.Submission_Page_Research_Question__c == null || topic.Submission_Page_Research_Question__c == '')
        {
            error = true;
            
            String errMsg = 'Research Question is Required';
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,errMsg));
        }       
        
        if (topic.Submission_Page_Email__c != null && topic.Submission_Page_Email__c != ''){
            
        System.debug('Test Email - In Submit  == ' + topic.Submission_Page_Email__c + ' email1 --> '  + email1);
            
        
            if( email1 != topic.Submission_Page_Email__c){
                error = true;
                
                String errMsg = 'Email Addresses Do Not Match';
                errMsg = errMsg + ' ' + email1 + ' ' + topic.Submission_Page_Email__c;
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,errMsg));
            }           
        }
        
        if (stakeholderSelected == null || stakeholderSelected == '')
        {
            error = true;
            
            String errMsg = 'Stakeholder Group is Required' ;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,errMsg));
        }       
        
        if (!error)
        {       
            try
            {
                topic.Submission_Page_Disease_Condition__c = diseaseSelected;
                topic.Submission_Page_Stakeholder__c = stakeholderSelected;
        
                //topic.Submission_Page_Research_Question__c = researchQuestion;
                //topic.Submission_Page_Question__c = importantQuestion;
        
                if(contactedSelected == 'Yes')
                    topic.Submission_Page_Contacted__c = true;
                else
                    topic.Submission_Page_Contacted__c = false;
        
                                             
                if(topic.Submission_Page_Disease_Condition__c == 'Other (please specify)')
                    topic.Submission_Page_Other_Disease_Condition__c = otherDisease;
                else
                    topic.Submission_Page_Other_Disease_Condition__c = null;
                    
//              Population Priority                 
                
                if(!populationSelected.isEmpty()){
                    
                    for(String s: populationSelected){
                        
                        if(s==null) continue;
                        
                        if(topic.Submission_Page_Specific_Population__c == null) 
                            topic.Submission_Page_Specific_Population__c =  s + ';';
                        else
                            topic.Submission_Page_Specific_Population__c = topic.Submission_Page_Specific_Population__c + s + ';';
                        
                        
                        if(s == 'Other (please specify)'){
                            topic.Submission_Page_Other_Priority_Pop__c = otherPopulation;
                        }else{
                            topic.Submission_Page_Other_Priority_Pop__c = null;                     
                        }
                    }                   

                }   
                                
//              Population Priority 


//              Study Design                
                
                if(!StudyDesignSelected.isEmpty()){
                    
                    for(String s: StudyDesignSelected){
                        
                        if(topic.Submission_Page_Study_Design__c == null) 
                            topic.Submission_Page_Study_Design__c =  s + ';';
                        else
                            topic.Submission_Page_Study_Design__c = topic.Submission_Page_Study_Design__c + s + ';';

                        if(s == 'Other (please specify)'){
                            topic.Submission_Page_Other_Study_Design__c = otherStudyDesign;
                        }else{
                            topic.Submission_Page_Other_Study_Design__c = null;                     
                        }
                    }                   

                }   
                                
//              Study Design

/*
                                    
                if(topic.Submission_Page_Study_Design__c == 'Other (please specify)')
                    topic.Submission_Page_Other_Study_Design__c = otherStudyDesign;
                else
                    topic.Submission_Page_Other_Study_Design__c = null;     
                    
                if(StudyDesignSelected <> null)
                    topic.Submission_Page_Study_Design__c = StudyDesignSelected;
                else
                    topic.Submission_Page_Study_Design__c = null;       
                                        
*/                                      
                topic.Source__c = 'Web';
                topic.Entry_Date__c = System.now();         
    
    
                System.debug(' Inserting Topic');
                
                //insert topic;
                
                List<Topic__c> newTopicList = new List<Topic__c>();
                newTopicList.add(topic);
                
                Integer i = 0;
                Database.SaveResult[] lsr = Database.insert(newTopicList, false);
                String newTopicId = '';
                Topic__c newTopic = new Topic__c();                      
                
                for(Database.SaveResult sr:lsr){
                    if(!sr.isSuccess()){
                        Database.Error err = sr.getErrors()[0];
                        System.debug(LoggingLevel.INFO,'-->***** Insert Contact Error: ' + err.getMessage());
                        
                    }else{
                         newTopicId = newTopicList.get(i).Id;
                         System.debug('--> ***** newTopic Id: ' + newTopicList.get(i).Id);
                         newTopic = [SELECT name, id FROM Topic__c WHERE id =: newTopicId];
                         System.debug('--> ***** newTopic Id name: ' + newTopic.name);
                         
                    }
                    
                }               

                topic = new Topic__c();
                
                clearFields();
                
                String errMsg = 'Thanks! Research Question has been submitted! Please use the following Topic ID to track the status of your topic ' + newTopic.name;
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,errMsg));                             

            }
            catch (Exception ex)
            {
                
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Error: '+ex.getMessage()));
                
                
            }
        }           
    
    
        return null;
    } 
    
    public String getMultiList(List<String> stringList)
    {
        String returnString = '';
        
        for(String s: stringList){
            
            returnString = returnString + s + ';';
        }
          
        return returnString;
    }   
    
    
     public String getDiseaseSelected()
    {
          
        return diseaseSelected;
    }

    public void setDiseaseSelected(String selected)
    {   
        System.debug('Topic2 selected diseaseSelected ---> '  + selected );
        if(selected == 'Pain')
        showOtherDisease = true;
        else
        showOtherDisease = false;
        diseaseSelected = selected;      
    }    
    
 /*   
    public List<String> getStudyDesignSelected()
    {
          
        return StudyDesignSelected;
    }

    public void setStudyDesignSelected(String selected)
    {   
        StudyDesignSelected = selected;      
    }   
 */
            
 public static Boolean validateEmail(String email) {

    Boolean res = true;
    
    

    String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'; 
    // source: <a href="http://www.regular-expressions.info/email.html" target="_blank">http://www.regular-expressions.info/email.html</a>
    Pattern MyPattern = Pattern.compile(emailRegex);

    Matcher MyMatcher = MyPattern.matcher(email);

    if (!MyMatcher.matches())

        res = false;

    return res;

    }
    
    
}