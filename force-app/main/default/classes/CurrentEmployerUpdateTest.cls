@isTest
public class CurrentEmployerUpdateTest {
    public static testmethod void CurrentEmp1(){
        CurrentEmployerUpdate ce = new CurrentEmployerUpdate();      
        list<user> usrlist=TestDataFactory.getpartnerUserTestRecords(1,1);        
        //Written By Vijay 
        Account acnt2 = new Account(name ='Vijay Test Account',ownerId = usrlist[0].Id);
        insert acnt2;        
        System.runAs(usrlist[0]){
            ce.employerchange='Employer found';
            ce.searchTerm='Vijay Test Account';
            ce.title='';
            ce.searchtermstyle='';
            ce.curposstyle='';
            ce.savecurrent();
            ce.cancel();
            ce.searchTerm='Farmers Insurance Company Test';
            ce.title='dev';
            ce.Organization='accenture';
            ce.searchtermstyle='border-size:1px; border-color:red;border-style:Solid';
            ce.curposstyle='border-size:1px; border-color:red;border-style:Solid';
            CurrentEmployerUpdate.searchMovie('Apple');
            ce.savecurrent();            
        }
    }
    
    public static testmethod void CurrentEmp5(){
        CurrentEmployerUpdate ce = new CurrentEmployerUpdate();
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt;
        
        //Written By Vijay 
        Account acnt1 = new Account(name ='Patient Unassociated Individuals',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId); 
        insert acnt1;
        acnt1.IsPartner=true;
        update acnt1;
        
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA',Reviewer_Role__c='');  
        insert con;
        
        Contact con1 = new Contact(LastName ='testCon1',AccountId = acnt1.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                   Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA',Reviewer_Role__c='Patient');  
        insert con1;
        User user = new User(alias = 'test123', email='test123y6@noemail.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con1.Id,
                             timezonesidkey='America/New_York', username='test123y6@noemail.com');  
        insert user;
        
        System.RunAs(user) {
            ce.employerchange='Unemployed'; 
            ce.savecurrent();
        }}
    
    public static testmethod void CurrentEmp2(){
        CurrentEmployerUpdate ce = new CurrentEmployerUpdate(); 
        
        list<user> usrlist=TestDataFactory.getpartnerUserTestRecords(1,1); 
        System.runAs(usrlist[0]){
            ce.employerchange='Unemployed';
            //ce.savecurrent(); 
            Contact con =[select Id,Community__c,Reviewer_Role__c from Contact c where c.Id=:usrlist[0].ContactId];
            con.Community__c='Patient';
            con.Reviewer_Role__c='';
            update con;
            //ce.savecurrent();
            con.Community__c='Stakeholder';
            update con;
            //ce.savecurrent();
            con.Reviewer_Role__c='';
            con.Community__c='';
            update con;
            //ce.savecurrent();
            con.Reviewer_Role__c='Stakeholder';
            update con;
            //ce.savecurrent();
            con.Reviewer_Role__c='Patient';
            update con;
            //ce.savecurrent();
            con.Reviewer_Role__c='Scientist';
            update con;
            //ce.savecurrent();
            List<SelectOption> testoptions6 = new List<SelectOption>{};
                testoptions6=ce.getItemsState();
            system.debug(testoptions6);
            system.assertequals(51,testoptions6.size());
            List<SelectOption> testoptions5 = new List<SelectOption>{};
                testoptions5=ce.getItemsCountry();
            system.debug(testoptions5);
            system.assertequals(239,testoptions5.size());
            List<SelectOption> testoptions3 = new List<SelectOption>{};
                testoptions3=ce.getItemsCurrentEmployer();
            system.debug(testoptions3);
            system.assertequals(3,testoptions3.size());
        }
    }
    
    public static testmethod void CurrentEmp3(){
        CurrentEmployerUpdate ce = new CurrentEmployerUpdate();
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        list<user> usrlist=TestDataFactory.getpartnerUserTestRecords(1,1); 
        System.runAs(usrlist[0]){
            ce.employerchange='Employer not found';
            ce.empcity='';
            ce.empcitystyle='';
            ce.empcountry='';
            ce.empcurrentpos='';
            ce.empcurposstyle='';
            ce.emplist=true;
            ce.empname='';
            ce.empnamestyle='';
            ce.empstatelist='';
            ce.empstatetext='';
            ce.empstatetextstyle='';
            ce.empstreet='';
            ce.empzip='';
            ce.empzipcode='';
            ce.empzipstyle='';
            ce.mailingaddress1='';
            ce.saveemp();
            ce.showstate();
            ce.empcity='City';
            ce.empcitystyle='border-size:1px; border-color:red;border-style:Solid';
            ce.empcountry='USA';
            ce.empcurrentpos='border-size:1px; border-color:red;border-style:Solid';
            ce.empcurposstyle='border-size:1px; border-color:red;border-style:Solid';
            ce.emplist=true;
            ce.empname='Test Account';
            ce.empnamestyle='border-size:1px; border-color:red;border-style:Solid';
            ce.empstatelist='list';
            ce.empstatetextstyle='border-size:1px; border-color:red;border-style:Solid';
            ce.empstreet='street';
            ce.empzip='12344';
            ce.empzipcode='12345';
            ce.empzipstyle='border-size:1px; border-color:red;border-style:Solid';
            ce.mailingaddress1='street test city test';
            ce.saveemp();
            ce.showstate();
            ce.empname='Test Acc';
            ce.empcountry='USA';
            ce.empstatetext='test';
            ce.empstatelist='test';
            ce.saveemp();
            ce.showstate();
            
        }
    }
    
    public static testmethod void CurrentEmp4(){
        CurrentEmployerUpdate ce = new CurrentEmployerUpdate();
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();        
        
        list<user> usrlist=TestDataFactory.getpartnerUserTestRecords(1,1); 
        
        
        System.runAs(usrlist[0]){
            ce.employerchange='Employer not found';
            ce.empcity='City';
            ce.empcitystyle='border-size:1px; border-color:red;border-style:Solid';
            ce.empcountry='USA';
            ce.empcurrentpos='border-size:1px; border-color:red;border-style:Solid';
            ce.empcurposstyle='border-size:1px; border-color:red;border-style:Solid';
            ce.emplist=true;
            ce.empname='Test Account';
            ce.empnamestyle='border-size:1px; border-color:red;border-style:Solid';
            ce.empstatelist='list';
            ce.empstatetextstyle='border-size:1px; border-color:red;border-style:Solid';
            ce.empstreet='street';
            ce.empzip='12344';
            ce.empzipcode='12345';
            ce.empzipstyle='border-size:1px; border-color:red;border-style:Solid';
            ce.mailingaddress1='street test city test';
            ce.empname='Test Acc';
            ce.empcountry='India';
            ce.empstatetext='test';
            ce.empstatelist='test';
            ce.saveemp();
            ce.showstate();
            
        }
    }
    
    public static testmethod void CurrentEmp6(){
        CurrentEmployerUpdate ce = new CurrentEmployerUpdate();
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt;
        
        Account acnt1 = new Account(name ='Scientist Unassociated Individuals',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId); 
        insert acnt1;
        acnt1.IsPartner=true;
        update acnt1;
        
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA',Reviewer_Role__c='');  
        insert con;
        
        Contact con1 = new Contact(LastName ='testCon1',AccountId = acnt1.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                   Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA',Reviewer_Role__c='Scientist');  
        insert con1;
        User user = new User(alias = 'test123', email='test123y6@noemail.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con1.Id,
                             timezonesidkey='America/New_York', username='test123y6@noemail.com');  
        insert user;
        
        System.RunAs(user) {
            ce.employerchange='Unemployed'; 
            ce.savecurrent();
        }} 
    
    public static testmethod void CurrentEmp7(){
        CurrentEmployerUpdate ce = new CurrentEmployerUpdate();
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt;
        
        Account acnt1 = new Account(name ='Stakeholder Unassociated Individuals',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId); 
        insert acnt1;
        acnt1.IsPartner=true;
        update acnt1;
        
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA',Reviewer_Role__c='');  
        insert con;
        
        Contact con1 = new Contact(LastName ='testCon1',AccountId = acnt1.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                   Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA',Reviewer_Role__c='Stakeholder');  
        insert con1;
        User user = new User(alias = 'test123', email='test123y6@noemail.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con1.Id,
                             timezonesidkey='America/New_York', username='test123y6@noemail.com');  
        insert user;
        
        System.RunAs(user) {
            ce.employerchange='Unemployed'; 
            ce.savecurrent();
        }} 
    
    public static testmethod void CurrentEmp8(){
        CurrentEmployerUpdate ce = new CurrentEmployerUpdate();
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt;
        
        Account acnt1 = new Account(name ='Patient Unassociated Individuals',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId); 
        insert acnt1;
        acnt1.IsPartner=true;
        update acnt1;
        
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA',Reviewer_Role__c='');  
        insert con;
        
        Contact con1 = new Contact(LastName ='testCon1',AccountId = acnt1.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                   Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA',Community__c='Patient');  
        insert con1;
        User user = new User(alias = 'test123', email='test123y6@noemail.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con1.Id,
                             timezonesidkey='America/New_York', username='test123y6@noemail.com');  
        insert user;
        
        System.RunAs(user) {
            ce.employerchange='Unemployed'; 
            ce.savecurrent();
        }} 
    
    public static testmethod void CurrentEmp9(){
        CurrentEmployerUpdate ce = new CurrentEmployerUpdate();
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt;
        
        Account acnt1 = new Account(name ='Scientist Unassociated Individuals',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId); 
        insert acnt1;
        acnt1.IsPartner=true;
        update acnt1;
        
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA',Reviewer_Role__c='');  
        insert con;
        
        Contact con1 = new Contact(LastName ='testCon1',AccountId = acnt1.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                   Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA',Community__c='Scientist');  
        insert con1;
        User user = new User(alias = 'test123', email='test123y6@noemail.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con1.Id,
                             timezonesidkey='America/New_York', username='test123y6@noemail.com');  
        insert user;
        
        System.RunAs(user) {
            ce.employerchange='Unemployed'; 
            ce.savecurrent();
        }} 
    
    public static testmethod void CurrentEmp10(){
        CurrentEmployerUpdate ce = new CurrentEmployerUpdate();
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt;
        
        Account acnt1 = new Account(name ='Stakeholder Unassociated Individuals',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId); 
        insert acnt1;
        acnt1.IsPartner=true;
        update acnt1;
        
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA',Reviewer_Role__c='');  
        insert con;
        
        Contact con1 = new Contact(LastName ='testCon1',AccountId = acnt1.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                   Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA',community__c='Other Stakeholder');  
        insert con1;
        User user = new User(alias = 'test123', email='test123y6@noemail.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con1.Id,
                             timezonesidkey='America/New_York', username='test123y6@noemail.com');  
        insert user;
        
        System.RunAs(user) {
            ce.employerchange='Unemployed'; 
            ce.savecurrent();
        }} 
    
    public static testmethod void CurrentEmp11(){
        CurrentEmployerUpdate ce = new CurrentEmployerUpdate();
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt;
        
        Account acnt1 = new Account(name ='Patient Unassociated Individuals',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId); 
        insert acnt1;
        acnt1.IsPartner=true;
        update acnt1;
        
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA',Reviewer_Role__c='');  
        insert con;
        
        Contact con1 = new Contact(LastName ='testCon1',AccountId = acnt1.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                   Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA',Reviewer_Role__c='',community__c='');  
        insert con1;
        User user = new User(alias = 'test123', email='test123y6@noemail.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con1.Id,
                             timezonesidkey='America/New_York', username='test123y6@noemail.com');  
        insert user;
        
        System.RunAs(user) {
            ce.employerchange='Unemployed'; 
            ce.savecurrent();
        }
    } 
    
}