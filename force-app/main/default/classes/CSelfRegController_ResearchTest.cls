/**
* An apex page controller that supports self registration of users in communities that allow self registration
*/
@IsTest
public with sharing class CSelfRegController_ResearchTest {   
    public static testmethod void testCommunitiesSelfRegController() {
        test.startTest();
        CommunitiesSelfRegController_Research controller = new CommunitiesSelfRegController_Research();
        controller.firstName = 'FirstName';
        controller.lastName = 'LastName';
        controller.email = 'test@force.com';
        controller.communityNickname = 'test';
        controller.password = 'abcd1234';
        controller.confirmPassword = 'abcd1234';        
        controller.checkbox01=true;
        system.debug(controller.registerUser());
        controller.registerUser();
        system.debug(controller.confirmAgreement());        
        test.stopTest();
    }   
    
    public static testmethod void testCommunitiesSelfRegController1() {
        test.startTest();
        CommunitiesSelfRegController_Research controller = new CommunitiesSelfRegController_Research();
        controller.firstName = 'FirstName';
        controller.lastName = 'LastName';
        controller.email = 'test@force.com';
        controller.communityNickname = 'test';
        controller.password = 'abcd1234@';
        controller.confirmPassword = 'abcd1';        
        controller.checkbox01=true;
        system.debug(controller.registerUser());
        controller.registerUser();
        system.debug(controller.confirmAgreement());
        test.stopTest();        
    }    
    
    public static testmethod void testCommunitiesSelfRegController3() {
        test.startTest();
        CommunitiesSelfRegController_Research controller = new CommunitiesSelfRegController_Research();
        controller.firstName = 'FirstName';
        controller.lastName = 'LastName';
        controller.email = 'test@force.com';
        controller.communityNickname = 'test';
        controller.password = 'abcd1234';
        controller.confirmPassword = 'abcd1234';
        controller.checkbox01=false;
        system.debug(controller.registerUser());
        controller.registerUser();
        system.debug(controller.confirmAgreement());
        test.stopTest();
        
    }    
}