/*
* Code made by Kalyan and/or Z
* Application Inventory: AI-1059
Client Requirement ID: RAr2_C4_022
*/

public  without sharing class CommunitiesLandingController_Reviewer{
    
    
    public PageReference forwardToStartPage()
    {
        
        //String communityUrl =Label.Reviewer_Community;       
        String  communityUrl = System.Url.getSalesforceBaseUrl().toExternalForm() + '/reviewer/';
        String loginpage= Label.Community_Reviewer_Page_Label;
        
        //Dev 3
        //string customhomepage='/FC_Reviewer__RPCommunityDashboard?sfdc.tabName=01r4C000000087u'; 
        
        //Production id of tab updated 10/22/2016 - Luis Rocha SADC
        string customhomepage=Label.Community_Reviewer_Page_Home_Label;
        
        //Id myId=userinfo.getUserId();
        
       // User u=[select contactId from User where id=:myId limit 1];
        
       // Contact c=[select Registered__c from Contact where id=:u.ContactId limit 1];
        
        if (UserInfo.getUserType().equals('Guest')) {
            system.debug('entered***');
            return new PageReference(communityUrl + loginpage);
        } else {
            
            return new PageReference(communityUrl + customhomepage);     
        }
        
    }
    
}