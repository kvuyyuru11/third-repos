@isTest
private class BudgetLineItemDecorator_Test {
	
	@isTest static void testBudgetLineItemDecorator() {
		Id r1 =Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Research Awards').getRecordTypeId();
		Id rKeyPersonnel =Schema.SObjectType.Key_Personnel_Costs__c.getRecordTypeInfosByName().get('Key Personnel').getRecordTypeId();
		
		Id pcoriCommunityPartID = System.Label.CommunityAdminAccount;

		Account.SObjectType.getDescribe().getRecordTypeInfos();

		Account a = new Account();
		a.Name = 'Acme1';
		insert a;

		Contact con = new Contact(LastName = 'testCon', AccountId = a.Id);
		Contact con1 = new Contact(LastName = 'testCon1', AccountId = a.Id);
		Contact con2 = new Contact(LastName = 'testCon1', AccountId = a.Id);
		Contact con3 = new Contact(LastName = 'testCon2', AccountId = a.Id);

		List<Contact> cons = new List<Contact>();
		cons.add(con);
		cons.add(con1);
		cons.add(con2);
		cons.add(con3);
		insert cons;
		
		User user = new User(alias = 'test123', email='test123fvb@noemail.com',
							 emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
							 localesidkey='en_US', profileid = pcoriCommunityPartID , country='United States',IsActive = true,   
							 contactId = con.Id,
							 timezonesidkey='America/New_York', username='testerfvb1@noemail.com');

		User user1 = new User(alias = 'test1234', email='test1234fvb@noemail.com',
							  emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
							  localesidkey='en_US', profileid = pcoriCommunityPartID , country='United States',IsActive = true,   
							  contactId = con1.Id,
							  timezonesidkey='America/New_York', username='testerfvb123@noemail.com');

		User user2 = new User(alias = 'test145', email='test12345fvb@noemail.com',
							  emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
							  localesidkey='en_US', profileid = pcoriCommunityPartID , country='United States',IsActive = true,   
							  contactId = con2.Id,
							  timezonesidkey='America/New_York', username='testerfvb12@noemail.com');
		
		User user3 = new User(alias = 'test52', email='test135fvb@noemail.com',
							  emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
							  localesidkey='en_US', profileid = pcoriCommunityPartID , country='United States',IsActive = true,   
							  contactId = con3.Id,
							  timezonesidkey='America/New_York', username='testerfvb82@noemail.com');

		List<User> users = new List<User>();
		users.add(user);
		users.add(user1);
		users.add(user2);
		users.add(user3);
		insert users;
		
		Cycle__c c = new Cycle__c();
		c.Name = 'testcycle';
		c.COI_Due_Date__c = date.valueof(System.now());
		insert c;
		
		opportunity o = new opportunity();
		
		o.Awardee_Institution_Organization__c=a.Id;
		o.RecordTypeId = r1;
		o.Full_Project_Title__c='test';
		o.Project_Name_off_layout__c='test';
		o.CloseDate = Date.today();
		o.Application_Number__c='Ra-1234';
		o.StageName = 'Executed';
		o.Name='test';
		o.PI_Name_User__c = user.id;
		o.AO_Name_User__c = user1.id;
		o.Project_Start_Date__c = system.Today();
		
		insert o;
		
		Budget__c b = new Budget__c();
		b.Associated_App_Project__c= o.id;
		b.Bypass_Flow__c = true;
		insert b;
		
		Key_Personnel_Costs__c keyCostBudgetLine = new Key_Personnel_Costs__c();
		keyCostBudgetLine.Budget__c = b.id;
		keyCostBudgetLine.RecordTypeId = rKeyPersonnel;
		keyCostBudgetLine.Cost_Category__c = 'Key Personnel';
		keyCostBudgetLine.Year__c = 1;
		keyCostBudgetLine.Name = 'Henry Winkler';
		keyCostBudgetLine.Key__c = false;
		keyCostBudgetLine.Role_On_Project__c='Principal Investigator 1';
		keyCostBudgetLine.Percent_Effort__c =75.0;
		keyCostBudgetLine.Calendar_Months__c=6;
		keyCostBudgetLine.Inst_Base_Salary__c=10;
		keyCostBudgetLine.Salary_Requested__c=50;
		keyCostBudgetLine.Fringe_Benefits__c=20;

		Test.startTest();

		BudgetLineItemDecorator budlineWcheck = new BudgetLineItemDecorator(keyCostBudgetLine);

		//test true when selected
		budlineWcheck.isSelected = true;
		System.assert(budlineWcheck.isSelected);

		//test false when unselected
		budlineWcheck.isSelected = false;
		System.assert(!budlineWcheck.isSelected);

		//test false when null
		budlineWcheck.isSelected = null;
		System.assert(!budlineWcheck.isSelected);

		Test.stopTest();

	}

}