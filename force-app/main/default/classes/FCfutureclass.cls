// This is a future class used in the custom landing Page controller to assign the Permission Set(for Engagement)
global class FCfutureclass {
   
  
    @future
    //This is the method where it fetches the permission set id and then assigns it to the user clicking engagement awards button
    public static void createFCdetails(){
          string mrEngagementPSetName=Label.Engagement_Awards;
       PermissionSet mrEngagementPSet =[SELECT Id,IsOwnedByProfile,Label FROM PermissionSet where Name=:mrEngagementPSetName limit 1];
     
        Id myID2 = userinfo.getUserId();
        string permissionsetid = mrEngagementPSet.id; //'0PS1800000004Pw';

       // string strQuery = 'select id from PermissionsetAssignment where PermissionSetId = \'' + String.escapeSingleQuotes(permissionsetid) + '\' AND AssigneeId = \'' + String.escapeSingleQuotes(myID2) + '\'';
       //Soql query to check how many Permission Sets of Enagagement Awards are present for the user or reviewer.
        List<PermissionSetAssignment> lstPsa = [select id from PermissionsetAssignment where PermissionSetId =: permissionsetid AND AssigneeId =:myID2];
    
        if(lstPsa.size() == 0)
        {
            PermissionSetAssignment psa1 = new PermissionsetAssignment(PermissionSetId= permissionsetid, AssigneeId = myID2 );
            database.insert(psa1);
        }
    }

}