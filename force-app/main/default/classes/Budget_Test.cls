@isTest
public class Budget_Test 
{
  
    /*static testMethod void TestBudget()
    {
        
        
       
        Campaign cmp=New Campaign();
        cmp.Name='Symptom Management for Patients with Advanced Illness';
        insert cmp;
        ApexPages.currentPage().getHeaders().put('referer', 'id='+cmp.Id+'&');
        BudgetControllerComp bc = new BudgetControllerComp();
        List<String> budgTypes = new List<String>();
        List<String> costCats = new List<String>();
        List<Key_Personnel_Costs__c> lineInserts = new List<Key_Personnel_Costs__c>();
        Integer counter = 1;
        
        costCats.add('Key Personnel');
        costCats.add('Consultant Cost');
        costCats.add('Supplies');
        costCats.add('Scientific Travel');
        costCats.add('Programmatic Travel');
        costCats.add('Other Expenses');
        costCats.add('Equipment');
        costCats.add('Subcontractor Direct');
        costCats.add('Subcontractor Indirect');
        //costCats.add('Total Prime Indirect');
        //costCats.add('Budget Summary');
        
        
        budgTypes.add('KeyPer');
        budgTypes.add('BudgetSum');
        budgTypes.add('Consultant');
        budgTypes.add('Equipment');
        budgTypes.add('Indirect');
        budgTypes.add('Other');
        budgTypes.add('Programmatic');
        budgTypes.add('Science');
        budgTypes.add('Subcontractor');
        budgTypes.add('Supplies');
        budgTypes.add('TotalPrime');
        
        
        List<User> usList = ObjectCreator.getUsers(1, 'TestBoi');
        insert usList;
        List<Account> acList = ObjectCreator.getAccts('JohnTester', 1);
        insert acList;
        
     User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
         Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
    insert acnt;
        acnt.IsPartner=true;
        update acnt;
                          RecordType r1=[Select Id from RecordType where name=:'RA Applications' limit 1];
        Cycle__c cyc=new Cycle__c(Name='test2016', COI_Due_Date__c = Date.newInstance(2018,12,31));
       insert cyc;
       Panel__c pan= new Panel__c(name='test',Cycle__c=cyc.id,MRO__c='00570000002dmiQ');
        insert pan;

         Opportunity o= new opportunity();

o.Awardee_Institution_Organization__c=acnt.Id;
o.RecordTypeId=r1.id;
o.Full_Project_Title__c='test';
      o.Project_Name_off_layout__c='test';
o.Panel__c=pan.Id;
o.CloseDate=Date.today();
o.Application_Number__c='Ra-1234';
o.StageName='In Progress';
o.Name='test';
       o.PI_Name_User__c='00570000002dmiQ';
          o.AO_Name_User__c='005700000047Ydq';
       // o.Bypass_Flow__c=true;
insert o;
        Budget__c budgInsert = new Budget__c();
        
       // If (opList.size()>0)
       // {
        budgInsert = new Budget__c(Associated_App_Project__c = o.Id, Consultant_Costs_Year_1__c = 10, Consultant_Costs_Year_2__c = 10, Consultant_Costs_Year_3__c = 10,
                                            Consultant_Costs_Year_4__c = 10, Consultant_Costs_Year_5__c = 10, Consultant_Costs_Year_6__c = 10, Equipment_Costs_Year_1__c = 10, Equipment_Costs_Year_2__c = 10,
                                            Equipment_Costs_Year_3__c = 10, Equipment_Costs_Year_4__c = 10, Equipment_Costs_Year_5__c = 10, Equipment_Costs_Year_6__c = 10, Subcontractor_Indirect_Costs_Year_1__c =10,
                                            Subcontractor_Indirect_Costs_Year_2__c =10, Subcontractor_Indirect_Costs_Year_3__c = 10, Subcontractor_Indirect_Costs_Year_4__c =10, Subcontractor_Indirect_Costs_Year_5__c =10,
                                            Subcontractor_Indirect_Costs_Year_6__c =10, Other_Expenses_Year_1__c =10, Other_Expenses_Year_2__c =10, Other_Expenses_Year_3__c =10, Other_Expenses_Year_4__c = 10, 
                                            Other_Expenses_Year_5__c = 10, Other_Expenses_Year_6__c = 10, Programmatic_Travel_Costs_Year_1__c =10, Programmatic_Travel_Costs_Year_2__c = 10, Programmatic_Travel_Costs_Year_3__c = 10,
                                            Programmatic_Travel_Costs_Year_4__c = 10, Programmatic_Travel_Costs_Year_5__c = 10, Programmatic_Travel_Costs_Year_6__c = 10, Scientific_Travel_Costs_Year_1__c = 10, Scientific_Travel_Costs_Year_2__c = 10,
                                            Scientific_Travel_Costs_Year_3__c = 10, Scientific_Travel_Costs_Year_4__c = 10, Scientific_Travel_Costs_Year_5__c = 10, Scientific_Travel_Costs_Year_6__c = 10, Direct_Subcontractor_Costs_Year_1__c =10,
                                            Direct_Subcontractor_Costs_Year_2__c = 10, Direct_Subcontractor_Costs_Year_3__c = 10, Direct_Subcontractor_Costs_Year_4__c = 10, Direct_Subcontractor_Costs_Year_5__c =10, Direct_Subcontractor_Costs_Year_6__c =10,
                                            Supplies_Costs_Year_1__c = 10, Supplies_Costs_Year_2__c = 10, Supplies_Costs_Year_3__c =10, Supplies_Costs_Year_4__c =10, Supplies_Costs_Year_5__c =10, Supplies_Costs_Year_6__c =10);
       // budgInsert.Associated_App_Project__c = o.Id;
        insert budgInsert;
       // }
        
        Integer i = 0;
        
        for(String cats: costCats)
        {
            Key_Personnel_Costs__c perBudg = new Key_Personnel_Costs__c();
            perBudg.Budget__c = budgInsert.Id;
            perBudg.Role_On_Project__c = 'Principal Investigator 1';
            perBudg.Total_Indirect_Costs__c = 10;
            perBudg.Description__c = 'test';
            perBudg.Applicable_Rate__c = 10;
            perBudg.Subcontractor_Name__c = 'test boi';
            perBudg.Hourly_Unit_Rate__c = 10;
            perbudg.Total_1__c = 10;
            perbudg.Start_Date__c = Date.today();
            perbudg.End_Date__c = Date.today();
            perBudg.Key__c = true;
            perbudg.Percent_Effort__c = 10;
            perbudg.Calendar_Months__c = 10;
            perbudg.Inst_Base_Salary__c = 10;
            perbudg.Salary_Requested__c = 10;
            perbudg.Fringe_Benefits__c = 10;
            perbudg.Cost_Category__c = cats;
            perbudg.Year__c = counter;
            perbudg.Name = 'test' + i;
            lineInserts.add(perbudg);
            counter++;
            i++;
          if(counter > 6)
            {
                counter = 1;
            }
        }
        
        insert lineInserts;
       
      system.debug('lineInserts ::: '+lineInserts);

        counter = 1;
        bc.curUrl = 'KeyPer';
        bc.costCat='Key Personnel';
        bc.appId = budgInsert.Associated_App_Project__c;
        
        bc.createNewBudget();
        
        for(String bTypes: budgTypes)
        {
          bc.appId = budgInsert.Associated_App_Project__c;
            bc.curUrl = bTypes;
      
            bc.grabCostCat();
            bc.GetSubTotalVal();
        }
        
        
        bc.grabId();
        bc.grabListDataReload();
        List<String> items = new List<String>();
        items.add('hambone');
        items.add('true');
        items.add('Principal Investigator 1');
        items.add('1');
        items.add('1');
        items.add('1');
        items.add('1');
        items.add('1');
        test.startTest();
        budgetControllerComp.grabAndSaveTable(items, '1', 'Insert', 'Key Personnel', bc.appId);
        budgetControllerComp.copyRows(items, '1', 'Key Personnel', bc.appId, 'Insert');
        test.stopTest();
        items.clear();
        
        items.add('MattyG');
        items.add('12');
        //test.startTest();
        budgetControllerComp.grabAndSaveTable(items, '1', 'Insert', 'Equipment', bc.appId);
        budgetControllerComp.copyRows(items, '1', 'Equipment', bc.appId, 'Insert');
        budgetControllerComp.grabAndSaveTable(items, '1', 'Insert', 'Subcontractor Direct', bc.appId);
        budgetControllerComp.copyRows(items, '1', 'Subcontractor Direct', bc.appId, 'Insert');
        //test.stopTest();
        items.clear();
        
        items.add('MattyG');
        items.add('1');
        items.add('1');
        test.startTest();
        budgetControllerComp.grabAndSaveTable(items, '1', 'Insert', 'Consultant Cost', bc.appId);
        budgetControllerComp.copyRows(items, '1', 'Consultant Cost', bc.appId, 'Insert');
        test.stopTest();
        
        items.clear();
        
        items.add('MattyG');
        items.add('1');
        
        test.startTest();
        budgetControllerComp.grabAndSaveTable(items, '1', 'Insert', 'Consultant Cost', bc.appId);
        budgetControllerComp.copyRows(items, '1', 'Consultant Cost', bc.appId, 'Insert');
        test.stopTest();
        
        items.clear();
        
        items.add(lineInserts[0].Id);
        items.add('hambone');
        items.add('true');
        items.add('Principal Investigator 1');
        items.add('12');
        items.add('12');
        items.add('12');
        items.add('12');
        items.add('12');
        
        test.startTest();
        budgetControllerComp.grabAndSaveTable(items, '1', 'Update', 'Key Personnel', bc.appId);
        //budgetControllerComp.copyRows(items, '1', 'Key Personnel', bc.appId, 'Update');
        budgetControllerComp.budgetUpdates('100', '100', '100', 'Key Personnel', bc.appId, '1', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017');
        budgetControllerComp.budgetUpdates('100', '100', '100', 'Key Personnel', bc.appId, '2', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017');
        budgetControllerComp.budgetUpdates('100', '100', '100', 'Key Personnel', bc.appId, '3', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017');
        budgetControllerComp.budgetUpdates('100', '100', '100', 'Key Personnel', bc.appId, '4', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017');
        budgetControllerComp.budgetUpdates('100', '100', '100', 'Key Personnel', bc.appId, '5', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017');
        budgetControllerComp.budgetUpdates('100', '100', '100', 'Key Personnel', bc.appId, '6', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017', '12/13/2017');
        test.stopTest();   
        List<String> deletes = new List<String>();
        deletes.add(lineInserts[0].Id);
        budgetControllerComp.deleteRows(deletes);
        apexpages.currentpage().getParameters().put('id',o.id);
        BudgetPDFpageController bpdf = new BudgetPDFpageController();
        bpdf.getKeyPersonnel();
        bpdf.getConsultantCost();
        bpdf.getSupplies();
        bpdf.getScientificTravel();
        bpdf.getProgrammaticTravel();
        bpdf.getOtherExpenses();
        bpdf.getEquipment();
        bpdf.getSubcontractorDirect();
        bpdf.getSubcontractorIndirect();
        
    }*/
    
}