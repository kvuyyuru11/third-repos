@isTest
public class AutoFillNumberAndLabelTest {
    private static testMethod void Autofill(){
        User usr;
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser){
            Profile p = [SELECT Id FROM Profile WHERE Name='Science Program Operations'];
            UserRole r = [SELECT Id FROM UserRole WHERE Name='Science Method Effectiveness Research Team'];
            usr = new User(alias = 'jonna', email='jonnasmith@acme.com',
                emailencodingkey='UTF-8', lastname='Smith',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
                timezonesidkey='America/New_York',
                username='jonnasmith@acme.com');
            insert usr;
            
            RecordType recAcc = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Standard Salesforce Account'];
            Account acc = new Account();
            acc.RecordTypeId = recAcc.Id;
            acc.Name = 'Test Acc';
            insert acc;
            list<user> urslist=Testdatafactory.getstandardUserTestRecords(3);
            RecordType recLead = [SELECT Id FROM RecordType WHERE sObjectType = 'Lead' AND Name = 'RA LOI'];
            Lead lead = new Lead();
            lead.RecordTypeId = recLead.Id;
            lead.Company = 'Test';
            lead.LastName = 'Tester';
            lead.External_Status__c = 'Draft';
            lead.Status = 'Draft';
            lead.Principal_Investigator__c = usr.Id;
            lead.Administrative_Official__c = usr.Id;
            lead.Zendesk__organization__c = acc.Id;
            lead.Principal_Investigator__c = urslist[0].Id;
            lead.Administrative_Official__c = urslist[1].Id;
            insert lead;
            
            RecordType recFlux = [SELECT Id FROM RecordType WHERE sObjectType = 'Fluxx_LOI_Review_Form__c' AND Name = 'AD LOI Review'];
            Fluxx_LOI_Review_Form__c fluxx = new Fluxx_LOI_Review_Form__c();
            fluxx.COI__c = 'Yes';
            fluxx.RecordTypeId = recFlux.Id;
            fluxx.Lead_LOI__c = lead.Id;
            fluxx.Reviewer_Label_RA__c = 'Reviewer 1';
            insert fluxx;
        }
        
    }
}