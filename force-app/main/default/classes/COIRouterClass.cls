//This is a Class that routes a user to different COI views based on Profile
public class COIRouterClass {
    PageReference pageref;
    Id recdid;
    string recordtypeid;
    string PName = Label.Single_Community_Profile;
    profile p=[select id from Profile where name=:PName];
    Id MRAid=p.id; 
    string ent;
    string returl;
 public COIRouterClass(ApexPages.StandardController controller){}
    //This is a method that routes to the standard page layout detail view or VFpage view based on profile

   public pagereference router3(){
    //Gets the record id
    
    recdid =ApexPages.currentPage().getParameters().get('Id');
    recordtypeid=ApexPages.currentPage().getParameters().get('RecordType');
    ent=ApexPages.currentPage().getParameters().get('ent');
    returl=ApexPages.currentPage().getParameters().get('Referer');
    RecordType objRecordType=  [SELECT Id,name FROM RecordType where name =: 'Awardee COI'];
    RecordType gnrlrecordtype=  [SELECT Id,name FROM RecordType where name =: 'General Record Type'];
    COI_Expertise__c objCOI_Expertise= new   COI_Expertise__c();
    if(recdid!=null){
       objCOI_Expertise=[ select id,RecordTypeId,Related_Project__c from COI_Expertise__c where id=:recdID ];
    }
    string url1;
    if (UserInfo.getProfileId().equals(MRAid) &&  objCOI_Expertise.RecordTypeId==gnrlrecordtype.Id) 
    {
       
         url1 = '/apex/COIPage?id='+recdid;
        
               
            
        }
        else if(UserInfo.getProfileId().equals(MRAid) &&  objCOI_Expertise.RecordTypeId==objRecordType.Id){
            
             url1='/apex/COIReviewer?Pid='+objCOI_Expertise.Related_Project__c+'&coid='+recdID;   
        }
       
   else{
            string objId = COI_Expertise__c.SObjectType.getDescribe().getKeyPrefix();
        if(recdid!=null){
            url1='/'+recdid+'?nooverride=1 &retURL='+recdid;
        }
        else{
             url1='/'+objId+'/e?nooverride=1'+'&RecordType='+recordtypeid;
        }
   }
       
     pageRef = new PageReference(url1);
        pageref.setRedirect(True);
      
        
    return pageRef;
}
}