public without sharing class AutoCompleteControllerHelper {

       
        //Assigining MeritReviewer candidate Permissionset
        @future
        public static void assignMeritReviewPermissionSet(Id Uid){
        PermissionSet meCadidatePset = new PermissionSet();
        string mrCadidatePSetName=Label.MR_Candidate_CCL;
        meCadidatePset =[SELECT Id,IsOwnedByProfile,Label FROM PermissionSet where Name=:mrCadidatePSetName limit 1];
          try{
        Id myID1 = Uid;
        string permissionsetid = meCadidatePset.id;
        List<PermissionSetAssignment> lstPsa = [select id from PermissionsetAssignment where PermissionSetId =: permissionsetid AND AssigneeId =:myID1];
        
        if(lstPsa.size() == 0)
        {
           PermissionSetAssignment psa1 = new PermissionsetAssignment(PermissionSetId= permissionsetid, AssigneeId = myID1);
           database.insert(psa1);
        }
        }
        catch(exception e){
        system.debug('Exception is caused and it is '+ e);
        }
        }
        
        //Assigining AdvisoryPanel Permissionset
        @future
        public static void assignAdvisoryPanelPermissionSet(Id Uid){
        PermissionSet APPSet = new PermissionSet();
        string APPSetName=Label.AP_Contact;
        Id myID2 = Uid;
        try{
        
        APPSet =[SELECT Id,IsOwnedByProfile,Label FROM PermissionSet where Name=:APPSetName limit 1];
        List<PermissionSetAssignment> lstPsa = [select id from PermissionsetAssignment where PermissionSetId =: APPSet.id AND AssigneeId =:myID2];
        
                if(lstPsa.size() == 0)
                {
                   PermissionSetAssignment psa1 = new PermissionsetAssignment(PermissionSetId= APPSet.id, AssigneeId = myID2);
                   database.insert(psa1);
                }
          }      
           catch(exception e)
           {
               system.debug('Exception is caused and it is '+ e);
           }     

       }
       
       //Assigining Engagement Awards Permissionset
         @future
        public static void assignEngagementAwardsPermissionSet(Id Uid){
        PermissionSet mrEngagementPSet = new PermissionSet();
        string mrEngagementPSetName=Label.Engagement_Awards;
        Id myID3 = Uid;
        try{
        mrEngagementPSet =[SELECT Id,IsOwnedByProfile,Label FROM PermissionSet where Name=:mrEngagementPSetName limit 1];
        List<PermissionSetAssignment> EngPsa = [select id from PermissionsetAssignment where PermissionSetId =: mrEngagementPSet.Id AND AssigneeId =:myID3];
        
                if(EngPsa.size() == 0)
                {
                   PermissionSetAssignment psa1 = new PermissionsetAssignment(PermissionSetId= mrEngagementPSet.Id, AssigneeId = myID3);
                   database.insert(psa1);
                }
          }      
           catch(exception e)
           {
               system.debug('Exception is caused and it is '+ e);
           }     

       }
       
       //Assigining Research Awards Permissionset
    @future
    public static void assignResearchAwardsPermissionSet(Id Uid){
        PermissionSet mrResearchPSet = new PermissionSet();
        string mrResearchPSetName=Label.Research_Awards;
        Id myID3 = Uid;
        try{
            mrResearchPSet =[SELECT Id,IsOwnedByProfile,Label FROM PermissionSet where Name=:mrResearchPSetName limit 1];
            List<PermissionSetAssignment> EngPsa = [select id from PermissionsetAssignment where PermissionSetId =: mrResearchPSet.Id AND AssigneeId =:myID3];
            
            if(EngPsa.size() == 0)
            {
                PermissionSetAssignment psa1 = new PermissionsetAssignment(PermissionSetId= mrResearchPSet.Id, AssigneeId = myID3);
                database.insert(psa1);
            }
        }      
        catch(exception e)
        {
            system.debug('Exception is caused and it is '+ e);
        }     
        
    }          
       
     

}