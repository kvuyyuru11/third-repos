/*
* Class : AP_CreateReviewersPanelHandler
* Author : Sathish Kumar (Accenture)
* Version History : 1.0
* Creation : 02/09/2015
* Last Modified By: Sathish Kumar (Accenture)
* Last Modified Date: 01/22/2015
* Modification Description: Updated Commenting
* Description : This class takes care of the inserting the Advisory Panel Application review records. It's referenced in the AP_CreatePanelReviewers trigger.
*/
public without Sharing  class  AP_CreateReviewersPanelHandler {
    public static boolean recursiveFlag = false;
    /**
This is a method which inserts Tier 1 Advisory Panel Application review records for each panel 
*/
    public static void createTier1( Advisory_Panel_Application__c adp, Id recordTypeId, Id uid,String panel){
        String tier1name=Label.Tier_1_name;
        String tier1name1=Label.Tier_1_name2;
        try{
            List<Advisory_Panel_Application_Review__c> aap = new List<Advisory_Panel_Application_Review__c>();
            Advisory_Panel_Application_Review__c ap3= new Advisory_Panel_Application_Review__c();
            ap3.Review_Name__c= tier1name; 
            ap3.Advisory_Panel_Application__c=adp.Id;
            ap3.Advisory_Panel__c= panel;
            ap3.RecordTypeId=recordTypeId; 
            ap3.OwnerId=uid;
            aap.add(ap3);
            Advisory_Panel_Application_Review__c ap2= new Advisory_Panel_Application_Review__c();
            ap2.Review_Name__c=tier1name1;
            ap2.Advisory_Panel_Application__c=adp.id;
            ap2.Advisory_Panel__c=panel;
            ap2.RecordTypeId=recordTypeId;
            ap2.OwnerId=uid;
            aap.add(ap2);
          Database.insert(aap);
        }
        catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
        }
    }
    /**
This is a method which inserts Tier 2 Advisory Panel Application review records for each panel
*/
    public static void createTier2( Advisory_Panel_Application_Review__c apr, Advisory_Panel_Application_Review__c apr1,Id recordTypeId, Id uid,String Panel){
        String tier2name=Label.Tier_2_name;
        String tier2name1=Label.Tier_2_name1;
        try{
            List<Advisory_Panel_Application_Review__c> aap = new List<Advisory_Panel_Application_Review__c>();
            Advisory_Panel_Application_Review__c ap3= new Advisory_Panel_Application_Review__c();
            ap3.Review_Name__c=tier2name; 
            ap3.Advisory_Panel_Application__c=apr.Advisory_Panel_Application__c;
            ap3.Advisory_Panel__c=Panel;
            ap3.RecordTypeId=recordTypeId;
            ap3.OwnerId=uid; 
            ap3.Tier1Reviewer1__c =apr.Owner.Name;
            ap3.Tier1Reviewer2__c =apr1.Owner.Name;
             ap3.Tier_1_Tags__c=apr.Tier_1_Tag__c + '/' + apr1.Tier_1_Tag__c;
            aap.add(ap3);
            Advisory_Panel_Application_Review__c ap2= new Advisory_Panel_Application_Review__c();
            ap2.Review_Name__c=tier2name1;
            ap2.Advisory_Panel_Application__c=apr.Advisory_Panel_Application__c;
            ap2.Advisory_Panel__c=Panel;
            ap2.RecordTypeId=recordTypeId;
            ap2.OwnerId=uid;
            ap2.Tier1Reviewer1__c =apr.Owner.Name;
            ap2.Tier1Reviewer2__c =apr1.Owner.Name;
             ap2.Tier_1_Tags__c=apr.Tier_1_Tag__c + '/' + apr1.Tier_1_Tag__c;
            aap.add(ap2);
            Database.insert(aap);
              /*ap2 = [Select id, Review_Name__c,Advisory_Panel_Application__c,Advisory_Panel__c,RecordTypeId,OwnerId 
                   from Advisory_Panel_Application_Review__c where Review_Name__c =: 'tier1name1'];
             ap3 = [Select id, Review_Name__c,Advisory_Panel_Application__c,Advisory_Panel__c,RecordTypeId,OwnerId 
                   from Advisory_Panel_Application_Review__c where Review_Name__c =: 'tier1name'];
            update ap2;*/
            
        }
        catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
        }
    }
    
    @future
    public static void assignPermissionSetToCommunityUser(List<Id> applicationPanelIdList){
        system.debug('===applicationPanelIdList==='+applicationPanelIdList);
        Set<Id> contactIdSet = new Set<Id>();
        Map<Id,User> contactIdAndUserMap = new Map<Id,User>();
        PermissionSet ps = null;
        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();
        String ApPCL = Label.AP_Contact;
        List<Advisory_Panel_Application__c> applicationList = [select Id, Application_Status__c, Contact__c from Advisory_Panel_Application__c where Id in : applicationPanelIdList];
        
        for(Advisory_Panel_Application__c advisoryapplication : applicationList){
            if(advisoryapplication.Application_Status__c.equalsIgnoreCase('Advisory Panelist')){
                contactIdSet.add(advisoryapplication.Contact__c);
            }
        }
        
        if(!contactIdSet.isEmpty()){
            ps = [select Id, Name from PermissionSet where Name=:ApPCL];
            for(User u : [select Id, ContactId, (select PermissionSetId from PermissionSetAssignments) from User where ContactId in :contactIdSet]){
                contactIdAndUserMap.put(u.ContactId,u);
            }
        }
        
        for(Advisory_Panel_Application__c advisorypanel : applicationList){
            if(advisorypanel.Application_Status__c.equalsIgnoreCase('Advisory Panelist')){
                if(contactIdAndUserMap.containsKey(advisorypanel.Contact__c)){
                    Boolean assignedAlreadyFlag = false;
                    for(PermissionSetAssignment psaExisted : contactIdAndUserMap.get(advisorypanel.Contact__c).PermissionSetAssignments){
                        if(psaExisted.PermissionSetId == ps.Id){
                            assignedAlreadyFlag = true;
                            break;
                        }
                    }
                    system.debug('===assignedAlreadyFlag==='+assignedAlreadyFlag);
                    if(!assignedAlreadyFlag){
                        PermissionSetAssignment psa = new PermissionSetAssignment();
                        psa.PermissionSetId = ps.Id;
                        psa.AssigneeId = contactIdAndUserMap.get(advisorypanel.Contact__c).Id;
                        psaList.add(psa);
                    }
                }
            }
        }
        if(!psaList.isEmpty()){
            try{
                system.debug('===psaList==='+psaList);
                Database.insert(psaList);
            }catch(Exception e){
            
            }
        }
    }
}