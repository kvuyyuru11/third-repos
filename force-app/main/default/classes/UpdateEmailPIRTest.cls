@isTest
public class UpdateEmailPIRTest {
    private static testMethod void UpdateEmailonPIRTest(){
        RecordType recOpp = [SELECT Id FROM RecordType WHERE DeveloperName = 'Research_Awards' AND sObjectType = 'Opportunity'];
        RecordType recAcc = [SELECT Id FROM RecordType WHERE DeveloperName = 'Standard_Salesforce_Account' AND sObjectType = 'Account'];
        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.RecordTypeId = recAcc.Id;
        insert acc;
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = recOpp.Id;
        opp.AO_Name_User__c = usr.Id;
        opp.PI_Name_User__c = usr.Id;
        opp.PI_Project_Lead_Designee_1_New__c = usr.Id;
        opp.AccountId = acc.Id;
        opp.Contract_Number__c = '03423';
        opp.Name = 'Test';
        opp.StageName = 'In Progress';
        opp.Application_Number__c = '12213';
        opp.Full_Project_Title__c = 'Project Test';
        opp.CloseDate = Date.newInstance(2016, 12, 22);
        insert opp;
        List<PIR__c> pirlst = new List<PIR__c>();
        PIR__c pir = new PIR__c();
        pir.Application__c = opp.Id;
        pir.Administrative_Official_Email_v2__c = 'pirtest@testaccent.com';
        pir.PIR_Response_Deadline__c = DateTime.newInstance(2016, 12, 31);
        pirlst.add(pir);
        insert pirlst;
        UpdateEmailPIR updEmail = new UpdateEmailPIR();
        updEmail.UpdateEmailonPIR(pirlst);
    }
}