@IsTest
public class AmbassadorLandingPageController_Test{
    public static testmethod void AmbassadorLandingPageController_TestMethod1() {        
        list<Account> acnt = TestDataFactory.getAccountTestRecords(1);
        list<contact> con  = TestDataFactory.getContactTestRecords(1,5);
        list<User> usrs    = TestDataFactory.getpartnerUserTestRecords(1,5);  
        User usr1 = [Select Id, Name, ContactId from User Where Id=:usrs[0].Id];
        System.runAs(usrs[0]){
            system.debug('here is te user'+usr1.ContactId);
            AmbassadorLandingPageController ALC = new AmbassadorLandingPageController();
            ALC.redirectToAmbassador();
        }
    }
    
    public static testmethod void AmbassadorLandingPageController_TestMethod2() {        
        list<Account> acnt = TestDataFactory.getAccountTestRecords(1);
        list<contact> con  = TestDataFactory.getContactTestRecords(1,1);
        list<User> usrs    = TestDataFactory.getpartnerUserTestRecords(1,1);  
        Ambassador__c amb = new Ambassador__c();
        amb.contact__c = con[0] .Id;
        amb.Status__c = 'Submitted';
        insert amb;
        User usr1 = [Select Id, Name, ContactId from User Where Id=:usrs[0].Id];
        System.runAs(usrs[0]){
            system.debug('here is te user'+usr1.ContactId);
            AmbassadorLandingPageController ALC = new AmbassadorLandingPageController();
            ALC.redirectToAmbassador();
            ALC.getListofDocuments();
        }
    }
}