@isTest
public class Lead_MasterClass_Test {
    @testSetup
    private static void testDataCreation() {
        List<Account> accts = TestDataFactory.getAccountTestRecords(1);
        
        List<User> users = TestDataFactory.getpartnerUserTestRecords(1, 30);
        
        FGM_Base__Program__c program = new FGM_Base__Program__c(Name = 'TestProgram', FGM_Base__Active__c = True, LOI_Record_Type_Name__c = 'IHS_LOI_Review');
        insert program;
        Campaign camp = new Campaign(Name='Test', Program__c=program.Id,LOI_Record_Type_Name__c='IHS_LOI_Review');
        insert camp;
        LOI_Reviews_RT__c loiSetting = new LOI_Reviews_RT__c(Name ='IHS_LOI_Review', number_of_loi_reviews_to_create__c = 2);
        Insert loiSetting;
        List<Lead> leadList = new List<Lead>();
        Id RA_LOI_ID = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('RA LOI').getRecordTypeId();
        
        for (integer i=0; i<users.size()-4; i++) {
            
            leadList.add(new Lead(recordTypeID = RA_LOI_ID, Principal_Investigator__c = users[i].Id, Administrative_Official__c = users[i+1].Id,
                                  Project_Lead_Last_Name_Clone__c = 'testing', Company = 'testin Comp', Status = 'Draft',
                                  LastName = 'BoJack' + i, FirstName = 'Horseman', Zendesk__organization__c  = accts[0].Id, Awardee_Institution_Organization__c = accts[0].Id,
                                  PFA__c=camp.Id, PI_Designee_1__c = users[i+2].Id, PI_Designee_2__c = users[i+3].Id, Financial_Officer__c = users[i+4].Id));
            
        }
        insert leadList;
    }
    
    private static testMethod void testForLeadShare() {
        Lead leadRec = [SELECT Id, Principal_Investigator__c, Administrative_Official__c, External_Status__c, Status FROM Lead LIMIT 1];
        List<User> users = ObjectCreator.getUsers(5, 'Ched');
        insert users;
        Lead le = new Lead(Id = leadRec.Id);
        le.Principal_Investigator__c = users[0].Id;
        le.Administrative_Official__c = users[1].Id;
        le.PI_Designee_1__c = users[2].Id;
        le.PI_Designee_2__c = users[3].Id;
        le.Financial_Officer__c = users[4].Id;
        le.External_Status__c = 'Draft';
        recursivetrigger.run = true;
        update le;
        System.assertEquals(1, [SELECT count() FROM LeadShare Where UserorGroupId =:users[0].Id], 'A LeadShare Record should be created for this user');
        System.assertEquals(1, [SELECT count() FROM LeadShare Where UserorGroupId =:users[1].Id], 'A LeadShare Record should be created for this user');
        System.assertEquals(1, [SELECT count() FROM LeadShare Where UserorGroupId =:users[2].Id], 'A LeadShare Record should be created for this user');
        System.assertEquals(1, [SELECT count() FROM LeadShare Where UserorGroupId =:users[3].Id], 'A LeadShare Record should be created for this user');
        System.assertEquals(1, [SELECT count() FROM LeadShare Where UserorGroupId =:users[4].Id], 'A LeadShare Record should be created for this user');
        
        
    }
    
    private static testMethod void testForBulkRec() {
        List<Lead> leadList = new List<Lead>(); 
        List<User> users = ObjectCreator.getUsers(2, 'bogus');
        insert users;
        for (Lead le : [SELECT Id, Principal_Investigator__c, Administrative_Official__c FROM Lead LIMIT 200]) {
            le.Principal_Investigator__c = users[0].Id;
            le.Administrative_Official__c = users[1].Id;
            leadList.add(le); 
        }
        recursivetrigger.run = true;
        update leadList;
        System.assertEquals(26, [SELECT count() FROM LeadShare Where UserorGroupId =:users[0].Id], '200 LeadShare Record should be created for this user');
  
    }
    
    private static testMethod void testForLOIReviewCreation() {
         Lead leadRec = [SELECT Id,Status, Principal_Investigator__c, Administrative_Official__c FROM Lead LIMIT 1];
         Lead le = new Lead(Id = leadRec.Id);
         le.Status = 'Pending LOI Review';
         update le;
         System.assertEquals(2, [SELECT count() FROM Fluxx_LOI_Review_Form__c Where Lead_LOI__c =:le.Id], 'Two LOI Review Records should be created for this LOI');
        
    }
    
    
    /*private static testMethod void testForLeadStatus() {
        List<Lead> leadUpdateList = new List<Lead>();
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Lead.Status.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple) {
            if (pickListVal.getLabel() == 'Draft' || pickListVal.getLabel() == 'Ready for Review' || pickListVal.getLabel() == 'Pending LOI Review' || pickListVal.getLabel() == 'Invited to Apply' || 
                pickListVal.getLabel() == 'Not Invited' || pickListVal.getLabel() == 'Withdrawn' || pickListVal.getLabel() == 'Closed')
                pickListValuesList.add(pickListVal.getLabel());
        }
        system.debug('Here is the Picklist Values'+pickListValuesList);
        integer j=0;
        for(Lead le : [SELECT Id, Principal_Investigator__c, Administrative_Official__c, External_Status__c, Status FROM Lead]) {
           le.Status = pickListValuesList[j];
           leadUpdateList.add(le);
           j++;
           if(j < pickListValuesList.size()) {
               j = 0;
           }
        }
        System.debug('here it is'+leadUpdateList);
        update leadUpdateList;
        System.debug('here it is'+leadUpdateList);
         //System.assertEquals('Invited to Apply', le.External_Status__c, 'Two LOI Review Records should be created for this LOI');
        
        
    }*/
    
    private static testMethod void testForLeadStatus() {
        List<Lead> leadList = [Select id, Name, Status, External_Status__c from Lead];
        List<Lead> leadUpdateList = new List<Lead>();
        leadList[0].Status = 'Draft';
        leadUpdateList.add(leadList[0]);
        leadList[1].Status = 'Ready for Review';
        leadUpdateList.add(leadList[1]);
        leadList[2].Status = 'Pending LOI Review';
        leadUpdateList.add(leadList[2]);
        leadList[3].Status = 'Invited to Apply';
        leadUpdateList.add(leadList[3]);
        leadList[4].Status = 'Not Invited';
        leadUpdateList.add(leadList[4]);
        leadList[5].Status = 'Withdrawn';
        leadUpdateList.add(leadList[5]);
        leadList[6].Status = 'Closed';
        leadUpdateList.add(leadList[6]);
        recursiveTrigger.run = true;
        update leadUpdateList;
        System.assertEquals('Draft', [Select External_Status__c FROM Lead where Status = 'Draft' LIMIT 1].External_Status__c, 'The External Status of this LOI has been changed');
        System.assertEquals('Submitted', [Select External_Status__c FROM Lead where Status = 'Ready for Review' LIMIT 1].External_Status__c, 'The External Status of this LOI has been changed');
        System.assertEquals('Under Review', [Select External_Status__c FROM Lead where Status = 'Pending LOI Review' LIMIT 1].External_Status__c, 'The External Status of this LOI has been changed');
        System.assertEquals('Invited to Apply', [Select External_Status__c FROM Lead where Status = 'Invited to Apply' LIMIT 1].External_Status__c, 'The External Status of this LOI has been changed');
        System.assertEquals('Not Invited', [Select External_Status__c FROM Lead where Status = 'Not Invited' LIMIT 1].External_Status__c, 'The External Status of this LOI has been changed');
        System.assertEquals('Withdrawn', [Select External_Status__c FROM Lead where Status = 'Withdrawn' LIMIT 1].External_Status__c, 'The External Status of this LOI has been changed');
        System.assertEquals('Closed', [Select External_Status__c FROM Lead where Status = 'Closed' LIMIT 1].External_Status__c, 'The External Status of this LOI has been changed');


    }
 
}