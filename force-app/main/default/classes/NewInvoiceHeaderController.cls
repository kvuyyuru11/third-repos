/*------------------------------------------------------------------------------
 *  Date      Project             Developer             Justification
 *  04-10-17  Invoicing-Phase2       Himanshu Kalra, M&S Consulting  Creation
 *  04-10-17  Invoicing-Phase2       Himanshu Kalra, M&S Consulting  To Allow Awardee to create Invoice with header values after saving take Awardee to InvoiceDetailPage and includes Custom Validations
 *  
 * -----------------------------------------------------------------------------
*/ 
public with sharing class NewInvoiceHeaderController {
     
    public NewInvoiceHeaderController thisClass { get { return this; } set; }
    // holds the Project Invoice Type for display 
    public String Project_InvoiceType{ get; set; }
    // holds the Project Type for display 
    public String Project_Type{ get; set; }
    // holds the boolean value to display warning message 
    public boolean showWarning  { get; set; } 
    // holds the boolean value to stop user to Save Invoice If VF displays error message 
    public boolean stopSave  { get; set; } 
    // holds the Contract Number for display 
    public string contractNumber{get;set;}
    // holds the Institution Name for display 
    public string institutionName{get; set;}
    // holds the PI name for display 
    public string Principal_Investigator_Name{get; set;}
    // holds the Project Start Date for display 
    public string ProjectStartDate{get; set;}
    // holds the Project End Date for display 
    public string ProjectEndDate{get; set;}
    // holds the Project End Date in Date type
    public Date ProjectEndDateD{get; set;}
    // holds the Project Start Date in Date type
    public Date ProjectStartDateD{get; set;}
    public id proj_pid;
    private Opportunity opp;
    
    ApexPages.StandardController invController;
    
    // holds the Invoice record being created
    public Invoice__c invoice{get;set;}
    // holds the boolean Project Missing boolean value
    public Boolean isProjectMissing {get; set;}
    public Boolean isBudgetMissing {get;set;}
    
    //@Purpose: Constructor to call getProjectDetails method
    public NewInvoiceHeaderController(ApexPages.StandardController controller) {
        isBudgetMissing = true;
        invoice = (Invoice__c)controller.getRecord(); 
        // hide all the warnings
        showWarning = false;
        //showWarning1  = false;
        stopSave= false;
        this.invController = controller;
        
        getProjectDetails();
        invoice.version__c = 1;
        invoice.Invoice_Status_External__c = 'Draft';
        invoice.Invoice_Status_Internal__c = '';
    }
    //@Purpose: Fetches Project field values on load in Visualforce page 
    public void getProjectDetails() {     
        List<Opportunity> opp_lst  = [SELECT
                    Account_Name__c, Invoice_Type__c,contract_number__c, PI_Name_User__c, Project_Start_Date__c, 
                    Project_End_Date__c, PI_Name_User__r.FirstName,PI_Name_User__r.LastName,
                    ( Select Is_Current_Budget__c ,
                    Consultant_Costs_TRP_Cumulative__c, Consultant_Costs_SUP_Cumulative__c, Consultant_Costs_PR_Cumulative__c,                                     
                    Supplies_TRP_Cumulative__c, Supplies_SUP_Cumulative__c, Supplies_PR_Cumulative__c,Supplies_PR_Available_Funds__c, 
                    Travel_TRP_Cumulative__c, Travel_SUP_Cumulative__c, Travel_PR_Cumulative__c, Other_Costs_TRP_Cumulative__c,  
                    Other_Costs_SUP_Cumulative__c, Other_Costs_PR_Cumulative__c,
                    Equipment_TRP_Cumulative__c, Equipment_SUP_Cumulative__c, Equipment_PR_Cumulative__c,
                    Research_Related_I_O_TRP_Cumulative__c, Research_Related_I_O_SUP_Cumulative__c, Research_Related_I_O_PR_Cumulative__c,
                    Consortium_Contractual_Costs_TRP_Cumulat__c, Consortium_Contractual_Costs_SUP_Cumulat__c, Consortium_Contractual_Costs_PR_Cumulat__c,
                    Indirect_Costs_TRP_Cumulative__c, Indirect_Costs_SUP_Cumulative__c, Indirect_Costs_PR_Cumulative__c,
                    fringe_Benefits_TRP_Cumulative__c, fringe_Benefits_SUP_Cumulative__c, fringe_Benefits_PR_Cumulative__c,
                    salary_TRP_Cumulative__c, salary_SUP_Cumulative__c, salary_PR_Cumulative__c,                                      
                    Consortium_Contractual_Costs_TRP_AvFunds__c ,Consultant_Costs_TRP_Available_Funds__c, Equipment_TRP_Available_Funds__c,
                    Fringe_Benefits_TRP_Available_Funds__c, Indirect_Costs_TRP_Available_Funds__c,
                    Other_Costs_TRP_Available_Funds__c,  Salary_TRP_Available_Funds__c,
                    Supplies_TRP_Available_Funds__c, Travel_TRP_Available_Funds__c, 
                    Consortium_Contractual_Costs_SUP_AvFunds__c,  Consultant_Costs_SUP_Available_Funds__c,
                    Equipment_SUP_Available_Funds__c, Fringe_Benefits_SUP_Available_Funds__c,
                    Indirect_Costs_SUP_Available_Funds__c, Other_Costs_SUP_Available_Funds__c,
                    Research_Related_I_O_SUP_Available_Funds__c,Salary_SUP_Available_Funds__c,Supplies_SUP_Available_Funds__c,    
                    Consortium_Contractual_Costs_PR_AvFunds__c,Research_Related_I_O_TRP_Available_Funds__c, Consultant_Costs_PR_Available_Funds__c,                                
                    Equipment_PR_Available_Funds__c, Travel_PR_Available_Funds__c, Fringe_Benefits_PR_Available_Funds__c,                                
                    Indirect_Costs_PR_Available_Funds__c, Other_Costs_PR_Available_Funds__c,Research_Related_I_O_PR_Available_Funds__c,                                                               
                    Salary_PR_Available_Funds__c, Travel_SUP_Available_Funds__c from calculation__r ORDER BY Name desc) calculation
                FROM
                    Opportunity
                WHERE 
                    id = : invoice.Project__c]; 
                
        opp = opp_lst[0]; 
               proj_pid=opp.PI_Name_User__c;
        List<Project_Invoice_Calculation__c> lstProInvCal = opp.calculation__r;
        //System.debug('The list of the Items is '+lstProInvCal);
        for(Project_Invoice_Calculation__c projInv: (List<Project_Invoice_Calculation__c>)opp.calculation__r){
            if(projInv.Is_Current_Budget__c == true){
                isBudgetMissing = false;
              //  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.Budget_Missing));
                break; 
            }
        }

        if(!String.isBlank(opp.contract_number__c)){ 
            contractNumber = opp.contract_number__c;
        }
        if(!String.isBlank(opp.Account_Name__c)){
            institutionName = opp.Account_Name__c;
        }
        if(!String.isBlank(opp.Invoice_Type__c)) {
            Project_InvoiceType = opp.Invoice_Type__c;
        }  
        List<user> users = [SELECT FirstName, LastName FROM User WHERE ID = :opp.PI_Name_User__c];

        if(users.size() > 0){
            Principal_Investigator_Name = users[0].firstName + ' ' + users[0].lastName;
        }
        
        if(opp.PI_Name_User__r != null && !String.isBlank(opp.PI_Name_User__r.FirstName) && 
            !String.isBlank(opp.PI_Name_User__r.LastName)) 
            Principal_Investigator_Name = opp.PI_Name_User__r.FirstName + ' ' + opp.PI_Name_User__r.LastName;
        
        if(opp.Project_Start_Date__c != null){  
                ProjectStartDate = opp.Project_Start_Date__c.format();
                ProjectStartDateD = opp.Project_Start_Date__c;
        }
        if(opp.Project_End_Date__c != null) {
            ProjectEndDate = opp.Project_End_Date__c.format();
            ProjectEndDateD = opp.Project_End_Date__c;
        }
        return;
    }    
    //@Purpose: Awardee Invoice Custom Validations 
    public void BillingPeriodDateValidation(){
        showWarning = false; 
        stopSave = false;

        if(invoice.End_of_Billing_Period__c != null && ProjectEndDateD !=null && invoice.End_of_Billing_Period__c > ProjectEndDateD){
          ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Warning, System.Label.Expenses_fall_outside_Project_End_Date);
          ApexPages.AddMessage(myMsg);
        }
        // Awardee Validation: Pre-award phase represents the beginning of the contract lifecycle, awardee should not invoice from more than three months before the project starts.
        if(opp.Project_Start_Date__c!=null && invoice.Start_of_Billing_Period__c!=null){
            integer diffDays =  invoice.Start_of_Billing_Period__c.daysBetween(opp.Project_Start_Date__c); 
            //system.debug('diffDays '+diffDays);
            if(invoice.Start_of_Billing_Period__c != null && diffDays >92 && opp.Project_Start_Date__c > invoice.Start_of_Billing_Period__c){
                //system.debug('diffDays greater show error');
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, System.Label.Invoice_Start_of_Billing_period_ahead_Actual_Contract_start_date);
                ApexPages.AddMessage(myMsg);
                stopSave = true;         
            }
        }
        //Awardee validation: End of Billing Period date is before the Start of Billing Period date
        if(invoice.Start_of_Billing_Period__c != null && invoice.End_of_Billing_Period__c != null && invoice.Start_of_Billing_Period__c > invoice.End_of_Billing_Period__c ) {
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, System.Label.End_of_Billing_Period_date_before_Start_of_Billing_Period_date);
           ApexPages.AddMessage(myMsg);  
            stopSave = true;
        }
        // Awardee Validatiion:  for End date as future date
        if(invoice.End_of_Billing_Period__c !=null && invoice.End_of_Billing_Period__c > System.Date.today()){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Warning, System.Label.End_of_Billing_Date_is_in_the_future);
            ApexPages.AddMessage(myMsg); 
        }
            
        //Awardee alidation: Start and End of Billing Period should not overlap with prior billing periods of Submitted Invoices
        List<invoice__c> invoices = [select id from invoice__c where Project__c = :invoice.project__c and 
                                            (
                                                (Start_of_Billing_Period__c <= :invoice.Start_of_Billing_Period__c  and end_of_billing_period__c >=:invoice.Start_of_Billing_Period__c) Or
                                                (Start_of_Billing_Period__c <= :invoice.End_of_Billing_Period__c and End_of_billing_period__c >=:invoice.End_of_Billing_Period__c ) or
                                                (Start_of_Billing_Period__c >= :invoice.Start_of_Billing_Period__c  and End_of_billing_period__c <= :invoice.End_of_Billing_Period__c)
                                            ) limit 50000 ];
                                            
        system.debug('INVOICE SIZE '+invoices.size());                                
        if(invoices.size() > 0) {
            //debugVal = String.ValueOf(invoices.size());
            showWarning = true;
            //system.debug('INVOICE SIZE showWarning '+showWarning);  
        }
        //system.debug('INVOICE stopSave '+stopSave);
    }     
    //@Purpose: Create the Invoice and than create the line item records based on Invoice Type
    public PageReference createInvoiceAndInvLineItem(){
       // system.debug('INVOICE stopSave SAVE'+stopSave);
        try { 
            invoice__share objinvshare=new invoice__share();
            showWarning = false;
            Insert invoice;
            if((invoice!=null)&&(invoice.id!=null)){
                objinvshare.ParentId=invoice.id;
                objinvshare.UserOrGroupId=proj_pid;
                objinvshare.AccessLevel = 'Read';
               Database.SaveResult jobShareInsertResult = Database.insert(objinvshare,false);

            }
            if(Project_InvoiceType== System.Label.Cost_Reimbursable_Research_Award){
                addLineItem(invoice, System.Label.Research_Period);
                addLineItem(invoice, System.Label.Peer_Review);
                addLineItem(invoice, System.Label.Supplemental_Funding);
            }else if(Project_InvoiceType== System.Label.Cost_Reimbursable_Engagement_Awards){
                //system.debug('INVOICE Engagement');
                addLineItem(invoice, System.Label.Engagement);
            } 
            else if(Project_InvoiceType== System.Label.Firm_Fixed_Price){
                //system.debug('INVOICE Firm_Fixed_Price');
                addLineItem(invoice, System.Label.Milestone);
            }
            else if(Project_InvoiceType== System.Label.Hybrid ){
                //system.debug('INVOICE Hybrid RA');
                addLineItem(invoice, System.Label.Research_Period);
                addLineItem(invoice, System.Label.Supplemental_Funding);
                addLineItem(invoice, System.Label.Peer_Review_Hybrid);
            }       
            PageReference awardeeEditPage = new  PageReference('/apex/NewInvoiceDetail?id=' + invoice.id+'&edit=false'+'&prof=awardee');
            return awardeeEditPage;
        }catch(Exception e){
                System.debug('Exception type caught: ' + e.getTypeName());    
                System.debug('Message: ' + e.getMessage());    
                System.debug('Cause: ' + e.getCause());    // returns null
                System.debug('Line number: ' + e.getLineNumber());    
                System.debug('Stack trace: ' + e.getStackTraceString());      
            // returning NULL so the user can stay on the page   
            return null;
        }       
    }
    //@Purpose: This method called from createInvoiceAndInvLineItem Method to create Line item 
    private void addLineItem(Invoice__c invoice, String lineItemType) {
        List<Invoice_Line_Item__c> invLineItem_lst = new  list<Invoice_Line_Item__c>(); 
        Invoice_Line_Item__c ilic = new  Invoice_Line_Item__c();
        ilic.invoice__c = invoice.id;
        ilic.line_item_type__c = lineItemType;
        //System.debug('lineItemType HK '+lineItemType); 
        
        if(lineItemType == System.Label.Research_Period || lineItemType == System.Label.Engagement){
           // system.debug('INVOICE Hybrid RA Research_Period');
            ilic.Consortium_Contractual_Costs_CED__c = opp.calculation__r[0].Consortium_Contractual_Costs_TRP_Cumulat__c;   
            ilic.Consortium_Contractual_Costs_AFR__c = opp.calculation__r[0].Consortium_Contractual_Costs_TRP_AvFunds__c;            
            ilic.Consultant_Cost_CED__c = opp.calculation__r[0].Consultant_Costs_TRP_Cumulative__c;                        
            ilic.Consultant_Cost_AFR__c = opp.calculation__r[0].Consultant_Costs_TRP_Available_Funds__c;
            ilic.Equipment_CED__c = opp.calculation__r[0].Equipment_TRP_Cumulative__c;                        
            ilic.Equipment_AFR__c = opp.calculation__r[0].Equipment_TRP_Available_Funds__c;
            ilic.Fringe_Benefits_CED__c = opp.calculation__r[0].Fringe_Benefits_TRP_Cumulative__c;                          
            ilic.Fringe_Benefits_AFR__c = opp.calculation__r[0].Fringe_Benefits_TRP_Available_Funds__c;
            ilic.Indirect_Costs_CED__c = opp.calculation__r[0].Indirect_Costs_TRP_Cumulative__c;                         
            ilic.Indirect_Costs_AFR__c = opp.calculation__r[0].Indirect_Costs_TRP_Available_Funds__c;
            ilic.Other_Costs_CED__c = opp.calculation__r[0].Other_Costs_TRP_Cumulative__c;                
            ilic.Other_Costs_AFR__c = opp.calculation__r[0].Other_Costs_TRP_Available_Funds__c;
            ilic.Research_Related_IP_OP_Costs_CED__c = opp.calculation__r[0].Research_Related_I_O_TRP_Cumulative__c;     
            ilic.Research_Related_IP_OP_Costs_AFR__c = opp.calculation__r[0].Research_Related_I_O_TRP_Available_Funds__c;
            ilic.Salary_CED__c = opp.calculation__r[0].Salary_TRP_Cumulative__c;                    
            ilic.Salary_AFR__c = opp.calculation__r[0].Salary_TRP_Available_Funds__c;
            ilic.Supplies_CED__c = opp.calculation__r[0].Supplies_TRP_Cumulative__c;                               
            ilic.Supplies_AFR__c = opp.calculation__r[0].Supplies_TRP_Available_Funds__c;
            ilic.Travel_CED__c = opp.calculation__r[0].Travel_TRP_Cumulative__c;                        
            ilic.Travel_AFR__c = opp.calculation__r[0].Travel_TRP_Available_Funds__c;            
        }
        else if(lineItemType == System.Label.Supplemental_Funding){
            //system.debug('INVOICE Hybrid RA Supplemental_Funding');
            ilic.Consortium_Contractual_Costs_CED__c = opp.calculation__r[0].Consortium_Contractual_Costs_SUP_Cumulat__c;   
            ilic.Consortium_Contractual_Costs_AFR__c = opp.calculation__r[0].Consortium_Contractual_Costs_SUP_AvFunds__c;            
            ilic.Consultant_Cost_CED__c = opp.calculation__r[0].Consultant_Costs_SUP_Cumulative__c;   
            ilic.Consultant_Cost_AFR__c = opp.calculation__r[0].Consultant_Costs_SUP_Available_Funds__c;
            ilic.Equipment_CED__c = opp.calculation__r[0].Equipment_SUP_Cumulative__c;                                        
            ilic.Equipment_AFR__c = opp.calculation__r[0].Equipment_SUP_Available_Funds__c;
            ilic.Fringe_Benefits_CED__c =opp.calculation__r[0].Fringe_Benefits_SUP_Cumulative__c;                            
            ilic.Fringe_Benefits_AFR__c = opp.calculation__r[0].Fringe_Benefits_SUP_Available_Funds__c;
            ilic.Indirect_Costs_CED__c = opp.calculation__r[0].Indirect_Costs_SUP_Cumulative__c;                              
            ilic.Indirect_Costs_AFR__c = opp.calculation__r[0].Indirect_Costs_SUP_Available_Funds__c;
            ilic.Other_Costs_CED__c = opp.calculation__r[0].Other_Costs_SUP_Cumulative__c;                                   
            ilic.Other_Costs_AFR__c = opp.calculation__r[0].Other_Costs_SUP_Available_Funds__c;
            ilic.Research_Related_IP_OP_Costs_CED__c = opp.calculation__r[0].Research_Related_I_O_SUP_Cumulative__c;         
            ilic.Research_Related_IP_OP_Costs_AFR__c = opp.calculation__r[0].Research_Related_I_O_SUP_Available_Funds__c;
            ilic.Salary_CED__c = opp.calculation__r[0].Salary_SUP_Cumulative__c;                                              
            ilic.Salary_AFR__c = opp.calculation__r[0].Salary_SUP_Available_Funds__c;
            ilic.Supplies_CED__c = opp.calculation__r[0].Supplies_SUP_Cumulative__c;                                         
            ilic.Supplies_AFR__c = opp.calculation__r[0].Supplies_SUP_Available_Funds__c;
            ilic.Travel_CED__c = opp.calculation__r[0].Travel_SUP_Cumulative__c;     
            ilic.Travel_AFR__c = opp.calculation__r[0].Travel_SUP_Available_Funds__c;   
        }
        else  if(lineItemType == System.Label.Peer_Review){
            ilic.Consortium_Contractual_Costs_CED__c = opp.calculation__r[0].Consortium_Contractual_Costs_PR_Cumulat__c;     
            ilic.Consortium_Contractual_Costs_AFR__c = opp.calculation__r[0].Consortium_Contractual_Costs_PR_AvFunds__c;            
            ilic.Consultant_Cost_CED__c = opp.calculation__r[0].Consultant_Costs_PR_Cumulative__c;                           
            ilic.Consultant_Cost_AFR__c = opp.calculation__r[0].Consultant_Costs_PR_Available_Funds__c;
            ilic.Equipment_CED__c = opp.calculation__r[0].Equipment_PR_Cumulative__c;                                        
            ilic.Equipment_AFR__c = opp.calculation__r[0].Equipment_PR_Available_Funds__c;
            ilic.Fringe_Benefits_CED__c = opp.calculation__r[0].Fringe_Benefits_PR_Cumulative__c;                            
            ilic.Fringe_Benefits_AFR__c = opp.calculation__r[0].Fringe_Benefits_PR_Available_Funds__c;
            ilic.Indirect_Costs_CED__c = opp.calculation__r[0].Indirect_Costs_PR_Cumulative__c;                              
            ilic.Indirect_Costs_AFR__c = opp.calculation__r[0].Indirect_Costs_PR_Available_Funds__c;
            ilic.Other_Costs_CED__c = opp.calculation__r[0].Other_Costs_PR_Cumulative__c;                                   
            ilic.Other_Costs_AFR__c = opp.calculation__r[0].Other_Costs_PR_Available_Funds__c;
            ilic.Research_Related_IP_OP_Costs_CED__c = opp.calculation__r[0].Research_Related_I_O_PR_Cumulative__c;         
            ilic.Research_Related_IP_OP_Costs_AFR__c = opp.calculation__r[0].Research_Related_I_O_PR_Available_Funds__c;
            ilic.Salary_CED__c = opp.calculation__r[0].Salary_PR_Cumulative__c;                                              
            ilic.Salary_AFR__c = opp.calculation__r[0].Salary_PR_Available_Funds__c;
            ilic.Supplies_CED__c = opp.calculation__r[0].Supplies_PR_Cumulative__c;                                         
            ilic.Supplies_AFR__c = opp.calculation__r[0].Supplies_PR_Available_Funds__c;
            ilic.Travel_CED__c = opp.calculation__r[0].Travel_PR_Cumulative__c;                                               
            ilic.Travel_AFR__c = opp.calculation__r[0].Travel_PR_Available_Funds__c;
        }
        if(lineItemType ==  System.Label.Milestone){
            ilic.Milestone__c = 1;
            ilic.Milestone_Name1__c = '';
            ilic.Milestone_Number1__c =  ''; 
         }    
        invLineItem_lst.add(ilic);
        //System.debug(' The line Item is >>>>>>'+ ilic);
        Database.Saveresult[] lstSaveRes = Database.insert(invLineItem_lst, false); 
        for(Database.SaveResult saveRes : lstSaveRes){
            if(saveRes.isSuccess() == false){
                system.debug('*********saveRes.getErrors() : '+saveRes.getErrors());
            }
        }
    }    
}