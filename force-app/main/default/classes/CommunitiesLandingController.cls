/**
 * An apex page controller that takes the user to the right start page based on credentials or lack thereof
 */
public without sharing class CommunitiesLandingController {
    
    /* Original Code
        // Code we will invoke on page load.
        public PageReference forwardToStartPage() {
            return Network.communitiesLanding();
        }
    */
 
    // ZT - 06/18/14. Adapted from Eugene Vabishchevich's article (http://cloudcatamaran.com/2013/11/customer-community-customization-part-1/)
    // Code we will invoke on page load.
    // Below is the method that redirects the users based on the first time login or a regular login process.
    public PageReference forwardToStartPage() {
        
        //String communityUrl = 'https://developer2-pcori.cs23.force.com/engagement';
        String communityUrl =Label.Portal_URL;       
      String onetimeContactInfoPage = Label.PCORI_Portal_Landing_Page;
        String loginpage=label.PCORI_Portal_login_page;
        string customhomepage=Label.PCORI_Portal_custom_landing_page;
       Id myId=userinfo.getUserId();
    User u=[select contactId from User where id=:myId limit 1];
       Contact c=[select Registered__c from Contact where id=:u.ContactId limit 1];
    if (UserInfo.getUserType().equals('Guest')) {
        return new PageReference(communityUrl + loginpage);
    } else {
      
        if(c.Registered__c==true){
             return new PageReference(communityUrl+'/s'); 
        }
        
        return new PageReference(communityUrl+'/s/contactinformationpage');
        }
       
    }
    
    //This is the Constructor for the landing page of the communities
    public CommunitiesLandingController() {
       
    }
}