/*
* Apex Class : ContentDocumentLinkTestClass
* Author : Kalyan Vuyyuru (PCORI)
* Version History : 1.0
* Description : This is a TestClass to test autoPopulateLink on after insert to share files to the chatter group "Files Sharing Group"
*/
@IsTest
public with sharing class ContentDocumentLinkTestClass {
@TestSetup static void makeData(){
CollaborationGroup cg = new CollaborationGroup(Name = 'Files Sharing Group to test', CollaborationType = 'Public');
insert cg;
ContentVersion cv=new Contentversion();
cv.title='ABC';
cv.PathOnClient ='test';
cv.versiondata=EncodingUtil.base64Decode('This is version data');
insert cv;
ContentVersion cv1=[SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: cv.Id];
system.assertNotEquals(NULL, cv1.ContentDocumentId);
}
static testmethod void methodtotestifcontentdocumentlinkinsertsharesfilestodelete(){
    set<Id> linkedentityidsset = new set<Id>();
    list<Id> usermemberidsset = new list<Id>();
    ContentVersion cv1=[SELECT Id, ContentDocumentId FROM ContentVersion Limit 1];
        list<opportunity> opplist=Testdatafactory.getopportunitytestrecords(1, 7, 1);
    recursiveTrigger.run=true;
    ContentDocumentLink cdtl = new ContentDocumentLink(ContentDocumentId=cv1.ContentDocumentId,LINKEDENTITYID =opplist[0].Id,SHARETYPE='V');
insert cdtl;
    
    List<ContentDocumentLink> lstContentDocumentlink = [SELECT ContentDocumentId, LINKEDENTITYID
                        FROM ContentDocumentLink WHERE Id=:cdtl.Id];
    

    for(ContentDocumentLink cdl:lstContentDocumentlink ){
        linkedentityidsset.add(cdl.LinkedEntityId);
    }
system.assertNotEquals(linkedentityidsset, null);
    system.assertEquals(linkedentityidsset.size(), lstContentDocumentlink.size());
    
    list<CollaborationGroupMember> FilessharingGroupMember = [SELECT MemberId From CollaborationGroupMember WHERE CollaborationGroupId IN:linkedentityidsset];
    if(!FilessharingGroupMember.isempty()){
    for (CollaborationGroupMember cgm:FilessharingGroupMember){
        usermemberidsset.add(cgm.MemberId);
    }
    system.assertNotEquals(usermemberidsset, null);
    list<user> usrlst=[select Id from user where id IN:usermemberidsset];
    system.runAs(usrlst[0]){
        delete lstContentDocumentlink;
        system.assertEquals(lstContentDocumentlink, NULL);
        }
    }
}
    static testmethod void methodtotestifcontentdocumentlinkinsertdoesnotallowotherstodeletefilesthansharedgroup(){
    set<Id> linkedentityidsset = new set<Id>();
    list<Id> usermemberidsset = new list<Id>();
    ContentVersion cv1=[SELECT Id, ContentDocumentId FROM ContentVersion Limit 1];
        list<opportunity> opplist=Testdatafactory.getopportunitytestrecords(1, 7, 1);
    recursiveTrigger.run=true;
    ContentDocumentLink cdtl = new ContentDocumentLink(ContentDocumentId=cv1.ContentDocumentId,LINKEDENTITYID =opplist[0].Id,SHARETYPE='V');
insert cdtl;
    
    List<ContentDocumentLink> lstContentDocumentlink = [SELECT ContentDocumentId, LINKEDENTITYID
                        FROM ContentDocumentLink WHERE Id=:cdtl.Id];
    

    for(ContentDocumentLink cdl:lstContentDocumentlink ){
        linkedentityidsset.add(cdl.LinkedEntityId);
    }
system.assertNotEquals(linkedentityidsset, null);
    system.assertEquals(linkedentityidsset.size(), lstContentDocumentlink.size());
    
    list<CollaborationGroupMember> FilessharingGroupMember = [SELECT MemberId From CollaborationGroupMember WHERE CollaborationGroupId IN:linkedentityidsset];
    if(!FilessharingGroupMember.isempty()){
    for (CollaborationGroupMember cgm:FilessharingGroupMember){
        usermemberidsset.add(cgm.MemberId);
    }
    system.assertNotEquals(usermemberidsset, null);       
    User MRUser = [select id from User where profile.name='Merit Reviewer Management' and userroleid != null and IsActive = true limit 1];
    system.runAs(MRUser){
        delete lstContentDocumentlink;
        system.assertNOTEquals(lstContentDocumentlink, NULL);
        }
    }
}
    
        static testmethod void methodtotestifcontentdocumentlinkinsertwillnotshareiflinkedentityidnotopp(){
    set<Id> linkedentityidsset = new set<Id>();
    list<Id> usermemberidsset = new list<Id>();
    ContentVersion cv1=[SELECT Id, ContentDocumentId FROM ContentVersion Limit 1];
        list<opportunity> opplist=Testdatafactory.getresearchawardsopportunitytestrecordsforInvoices(1, 7, 1);
            Audit__c a= new Audit__c();
            a.Project__c=opplist[0].Id;
            insert a;
    recursiveTrigger.run=true;
    ContentDocumentLink cdtl = new ContentDocumentLink(ContentDocumentId=cv1.ContentDocumentId,LINKEDENTITYID =a.Id,SHARETYPE='V');
insert cdtl;
    
    List<ContentDocumentLink> lstContentDocumentlink = [SELECT ContentDocumentId, LINKEDENTITYID
                        FROM ContentDocumentLink WHERE Id=:cdtl.Id];
    

    for(ContentDocumentLink cdl:lstContentDocumentlink ){
        linkedentityidsset.add(cdl.LinkedEntityId);
    }
system.assertNotEquals(linkedentityidsset, null);
    system.assertEquals(linkedentityidsset.size(), lstContentDocumentlink.size());
    
    list<CollaborationGroupMember> FilessharingGroupMember = [SELECT MemberId From CollaborationGroupMember WHERE CollaborationGroupId IN:linkedentityidsset];
system.assertEquals(FilessharingGroupMember.isempty(), TRUE);
}
}