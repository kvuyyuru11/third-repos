public class RRCSAverageReviewScoreController {
 
    public decimal averagevalue1{get;set;}
    public decimal averagevalue2{get;set;}
    public decimal averagevalue3{get;set;}
    public decimal averagevalue4{get;set;}
    public decimal averagevalue5{get;set;}
    public decimal averagevalue6{get;set;}

     public RRCSAverageReviewScoreController(ApexPages.StandardController controller) {
     decimal i=0;
    Map<String,decimal>averagemap = new Map<String,decimal>();
    List<FC_Reviewer__External_Review__c> ReviewsList = new List<FC_Reviewer__External_Review__c>();
    Id ParentId = apexpages.currentpage().getParameters().get('Id');
    ReviewsList = [select Id,Topic_Value_Score__c,Viable_IPD_MA_Collaboration_Score__c,Project_Plan_and_Timeline_Score__c,
                   Personnel_Collaborators_Score__c,Past_Performance_Score__c,Budget_Cost_Proposal_Score__c 
                   from FC_Reviewer__External_Review__c where FC_Reviewer__Opportunity__c=:ParentId AND Review_Complete__c=True];
                   
     for(FC_Reviewer__External_Review__c rer : ReviewsList){
     
     
    
     if(rer.Topic_Value_Score__c!=null){
     if(averagemap.get('Average Topic Value Score')==null){
     averagemap.put('Average Topic Value Score',decimal.valueof(rer.Topic_Value_Score__c.substring(0,1)));
     }else{
     i=averagemap.get('Average Topic Value Score')+decimal.valueof(rer.Topic_Value_Score__c.substring(0,1));
     averagemap.put('Average Topic Value Score',i);
     }
     system.debug('=====>rer'+averagemap.get('Average Topic Value Score'));
     }
     
     
     
     if(rer.Viable_IPD_MA_Collaboration_Score__c!=null){
     if(averagemap.get('Average Viable IPD MA Collaboration Criteria')==null){
     averagemap.put('Average Viable IPD MA Collaboration Criteria',decimal.valueof(rer.Viable_IPD_MA_Collaboration_Score__c.substring(0,1)));
     }else{
     i=averagemap.get('Average Viable IPD MA Collaboration Criteria')+decimal.valueof(rer.Viable_IPD_MA_Collaboration_Score__c.substring(0,1));
     averagemap.put('Average Viable IPD MA Collaboration Criteria',i);
     }
     }
     
     system.debug('=====>'+averagemap.get('Average Viable IPD MA Collaboration Criteria'));
     
     if(rer.Project_Plan_and_Timeline_Score__c!=null){
     if(averagemap.get('Average Project Plan and Timeline Criteria')==null){
     averagemap.put('Average Project Plan and Timeline Criteria',decimal.valueof(rer.Project_Plan_and_Timeline_Score__c.substring(0,1)));
     }else{
     i=averagemap.get('Average Project Plan and Timeline Criteria')+decimal.valueof(rer.Project_Plan_and_Timeline_Score__c.substring(0,1));
     averagemap.put('Average Project Plan and Timeline Criteria',i);
     system.debug('=====>'+averagemap.get('Average Project Plan and Timeline Criteria'));
     }
     }
     
     system.debug('=====>'+averagemap.get('Average Project Plan and Timeline Criteria'));
     
     if(rer.Personnel_Collaborators_Score__c!=null){
     if(averagemap.get('Average Personnel and Collaborators Criteria')==null){
     averagemap.put('Average Personnel and Collaborators Criteria',decimal.valueof(rer.Personnel_Collaborators_Score__c.substring(0,1)));
     }else{
     i=averagemap.get('Average Personnel and Collaborators Criteria')+decimal.valueof(rer.Personnel_Collaborators_Score__c.substring(0,1));
     averagemap.put('Average Personnel and Collaborators Criteria',i);
     }
     }
     
     system.debug('=====>'+averagemap.get('Average Personnel and Collaborators Criteria'));
     
     if(rer.Past_Performance_Score__c!=null){
     if(averagemap.get('Average Past Performance Score')==null){
     averagemap.put('Average Past Performance Score',decimal.valueof(rer.Past_Performance_Score__c.substring(0,1)));
     }else{
     i=averagemap.get('Average Past Performance Score')+decimal.valueof(rer.Past_Performance_Score__c.substring(0,1));
     averagemap.put('Average Past Performance Score',i);
     }
     }
     
system.debug('=====>'+averagemap.get('Average Past Performance Score'));

          if(rer.Budget_Cost_Proposal_Score__c!=null){
     if(averagemap.get('Average Budget/Cost Proposal Criteria')==null){
     averagemap.put('Average Budget/Cost Proposal Criteria',decimal.valueof(rer.Budget_Cost_Proposal_Score__c.substring(0,1)));
     }else{
     i=averagemap.get('Average Budget/Cost Proposal Criteria')+decimal.valueof(rer.Budget_Cost_Proposal_Score__c.substring(0,1));
     averagemap.put('Average Budget/Cost Proposal Criteria',i);
     }
     }
     
     system.debug('=====>'+averagemap);
         
     }
     if(averagemap.get('Average Topic Value Score')!=null){
     averagevalue1 = averagemap.get('Average Topic Value Score')/ReviewsList.size();
     }
     if(averagemap.get('Average Viable IPD MA Collaboration Criteria')!=null){
     averagevalue2 = averagemap.get('Average Viable IPD MA Collaboration Criteria')/ReviewsList.size();
     }if(averagemap.get('Average Project Plan and Timeline Criteria')!=null){
     averagevalue3 = averagemap.get('Average Project Plan and Timeline Criteria')/ReviewsList.size();
     }if(averagemap.get('Average Personnel and Collaborators Criteria')!=null){
     averagevalue4 = averagemap.get('Average Personnel and Collaborators Criteria')/ReviewsList.size();
     }if(averagemap.get('Average Past Performance Score')!=null){
     averagevalue5 = averagemap.get('Average Past Performance Score')/ReviewsList.size();
     }if(averagemap.get('Average Budget/Cost Proposal Criteria')!=null){
     averagevalue6 = averagemap.get('Average Budget/Cost Proposal Criteria')/ReviewsList.size(); 
     }

}
}