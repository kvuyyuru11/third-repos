/*
* Class : ReviewsTriggerTest
* Author : Kalyan Vuyyuru (PCORI)
* Version History : 1.0
* Description : This is a test class to ensure ReviewsTrigger is covered in all scenarios for different context operations that are carried over
for different processes that are involved accessing the reviews object.
*/
@IsTest
public class ReviewsTriggerTest {
    /*Method to check if we assign reviewers and check the create online reviews checkbox if reviews are created and also asserting
the list size when bulk created and also asserting the sharing to ensure sharing is working fine.*/
    static testmethod void TestMethod1totestSharingoninsertandupdateforcontacts (){ 
        list<Account> acnt=TestDataFactory.getAccountTestRecords(1);
        list<contact> con=TestDataFactory.getContactTestRecords(1,15);
        list<User> usrs= TestDataFactory.getpartnerUserTestRecords(1, 15);
        list<opportunity> opp=TestDataFactory.getopportunitytestrecords(1,45,25);
        list<opportunity> opptoupdate= new list<opportunity>();
        for(opportunity o:[select Id, Create_Online_Reviews__c from opportunity where id IN:opp]){
            o.Create_Online_Reviews__c=true;
            opptoupdate.add(o);
        }
        
        
        Test.startTest();
        recursiveTrigger.run1=true;
        update opptoupdate;
        
        list<FC_Reviewer__External_Review__c> rvslistinserted=[select Id,FC_Reviewer__Contact__c from FC_Reviewer__External_Review__c where FC_Reviewer__Opportunity__c IN :opptoupdate];
        List<FC_Reviewer__External_Review__share> reviewShares = [SELECT id, ParentId, UserOrGroupId From FC_Reviewer__External_Review__share where ParentId IN :rvslistinserted];
        
        set<Id> userset= new set<Id>();
        set<Id> contset= new set<Id>();
        set<Id> reviewshareuserset= new set<Id>();
        set<Id> reviewshareusersetafterupdate1= new set<Id>();
        set<Id> reviewshareusersetafterupdate2= new set<Id>();
        for (FC_Reviewer__External_Review__c r:rvslistinserted ){
            contset.add(r.FC_Reviewer__Contact__c);
        }
        List<User>  usrlist=[select id,contactId from User where contactid IN : contset];
        for(User u:usrlist){
            userset.add(u.Id);
        }
        for(FC_Reviewer__External_Review__share rv:reviewShares){
            reviewshareuserset.add(rv.UserOrGroupId);
        }
        system.assertEquals(true, reviewshareuserset.containsAll(userset));
        system.assertEquals(opptoupdate.size()*5, rvslistinserted.size());
        //system.assertEquals(true, !reviewshareuserset.contains(usrs[14].Id));
        
        ///rvslistinserted[3].FC_Reviewer__Contact__c=usrs[14].contactId;
        // rvslistinserted[4].FC_Reviewer__Contact__c=usrs[9].contactId;
        // rvslistinserted[2].FC_Reviewer__Contact__c=null;
        rvslistinserted[1].FC_Reviewer__Opportunity__c=null;
        rvslistinserted[0].FC_Reviewer__Opportunity__c=opp[8].Id;
        recursiveTrigger.run=true;
        update rvslistinserted;
        
        // List<FC_Reviewer__External_Review__share> reviewSharesafterupdate1 = [SELECT id, ParentId, UserOrGroupId From FC_Reviewer__External_Review__share where ParentId=:rvslistinserted[2].Id];
        //for(FC_Reviewer__External_Review__share rv1:reviewSharesafterupdate1){
        // reviewshareusersetafterupdate1.add(rv1.UserOrGroupId);
        //}
        // List<FC_Reviewer__External_Review__share> reviewSharesafterupdate2 = [SELECT id, ParentId, UserOrGroupId From FC_Reviewer__External_Review__share where ParentId=:rvslistinserted[3].Id];
        //for(FC_Reviewer__External_Review__share rv2:reviewSharesafterupdate2){
        // reviewshareusersetafterupdate2.add(rv2.UserOrGroupId);
        //}
        
        //system.assertEquals(true, !reviewshareusersetafterupdate1.contains(usrs[2].Id));
        //system.assertEquals(true, reviewshareusersetafterupdate2.contains(usrs[14].Id));
        
        test.stopTest();
    }
    
    
    
    
    /*Method to check if we click the move to inperson button on panel is working fine and also asserting the number of rvwslist created
will be equal to that of the COI with active status and conflict of interest indicated as No
*/ 
    static testmethod void TestMethod2totestSharingoninsertforreviewswhenmovedtoinperson (){ 
        
        list<panel__c> pnl=TestDataFactory.getpanelwithoppandpanelassgnandcoiTestRecords(1,20,10,1,7);
        
        list<COI_Expertise__c> coilist=[select id,Active__c,Conflict_of_Interest__c from COI_Expertise__c where Panel_R2__c=:pnl[0].Id];
        list<COI_Expertise__c> coilistnew = new list<COI_Expertise__c>();
        for(COI_Expertise__c c:coilist){
            c.Conflict_of_Interest__c='No';
            coilistnew.add(c);
        }
        update coilistnew;
        test.startTest();
        CreateInPersonReview.createInPersonRecord(pnl[0].Id);
      
        Id rvwRecordTypeId1 = Schema.SObjectType.FC_Reviewer__External_Review__c.getRecordTypeInfosByName().get(Label.InPersonReview).getRecordTypeId();
        Id rvwRecordTypeId2 = Schema.SObjectType.FC_Reviewer__External_Review__c.getRecordTypeInfosByName().get(Label.InPersonPreview).getRecordTypeId();    
        List<FC_Reviewer__External_Review__c> rlist = [SELECT Id,FC_Reviewer__Contact__c,FC_Reviewer__Opportunity__c FROM FC_Reviewer__External_Review__c WHERE Panel__c = :pnl[0].Id  AND (RecordTypeId = :rvwRecordTypeId2 OR RecordTypeId = :rvwRecordTypeId1)];
        List<COI_Expertise__c> coilst2 = [SELECT Id, Related_Project__c, Reviewer_Name__c, Research_Application__c FROM COI_Expertise__c WHERE Panel_R2__c = :pnl[0].Id  AND Active__c = true AND Conflict_of_Interest__c = 'No'];
        system.assertEquals(coilst2.size(), rlist.size());
        system.assertEquals(coilst2.size(), 70);
        delete rlist[5];
        CreateInPersonReview.createInPersonRecord(pnl[0].Id);
        CreateInPersonReview.createInPersonRecord(pnl[0].Id);
        system.assertEquals(CreateInPersonReview.createInPersonRecord(pnl[0].Id), 'Review records were already created');
        test.stopTest();
    }
    
    
     static testmethod void TestMethod2totestSharingoninsertforreviewswhenmovedtoinpersonadminbutton (){ 
        
        list<panel__c> pnl=TestDataFactory.getpanelwithoppandpanelassgnandcoiTestRecords(1,20,10,1,7);
        
        list<COI_Expertise__c> coilist=[select id,Active__c,Conflict_of_Interest__c from COI_Expertise__c where Panel_R2__c=:pnl[0].Id];
        list<COI_Expertise__c> coilistnew = new list<COI_Expertise__c>();
        for(COI_Expertise__c c:coilist){
            c.Conflict_of_Interest__c='No';
            coilistnew.add(c);
        }
        update coilistnew;
        test.startTest();
        CreateInPersonReviewforAdmin.createInPersonRecord(pnl[0].Id);
      
        Id rvwRecordTypeId1 = Schema.SObjectType.FC_Reviewer__External_Review__c.getRecordTypeInfosByName().get(Label.InPersonReview).getRecordTypeId();
        Id rvwRecordTypeId2 = Schema.SObjectType.FC_Reviewer__External_Review__c.getRecordTypeInfosByName().get(Label.InPersonPreview).getRecordTypeId();    
        List<FC_Reviewer__External_Review__c> rlist = [SELECT Id,FC_Reviewer__Contact__c,FC_Reviewer__Opportunity__c FROM FC_Reviewer__External_Review__c WHERE Panel__c = :pnl[0].Id  AND (RecordTypeId = :rvwRecordTypeId2 OR RecordTypeId = :rvwRecordTypeId1)];
        List<COI_Expertise__c> coilst2 = [SELECT Id, Related_Project__c, Reviewer_Name__c, Research_Application__c FROM COI_Expertise__c WHERE Panel_R2__c = :pnl[0].Id  AND Active__c = true AND Conflict_of_Interest__c = 'No'];
        system.assertEquals(coilst2.size(), rlist.size());
        system.assertEquals(coilst2.size(), 70);
        delete rlist[5];
        CreateInPersonReviewforAdmin.createInPersonRecord(pnl[0].Id);
        CreateInPersonReviewforAdmin.createInPersonRecord(pnl[0].Id);
        system.assertEquals(CreateInPersonReviewforAdmin.createInPersonRecord(pnl[0].Id), 'Review records were already created');
        test.stopTest();
    }
    
    
        static testmethod void TestMethod2totestSharingoninsertforreviewswhenmovedtoinpersonadminbuttonandsharing (){ 
        
        list<panel__c> pnl=TestDataFactory.getpanelwithoppandpanelassgnandcoiTestRecords(1,20,10,1,7);
        
        list<COI_Expertise__c> coilist=[select id,Active__c,Conflict_of_Interest__c from COI_Expertise__c where Panel_R2__c=:pnl[0].Id];
        list<COI_Expertise__c> coilistnew = new list<COI_Expertise__c>();
        for(COI_Expertise__c c:coilist){
            c.Conflict_of_Interest__c='No';
            coilistnew.add(c);
        }
        update coilistnew;
        test.startTest();
        CreateInPersonReviewforAdmin.createInPersonRecord(pnl[0].Id);
        ShareApplicationsforInPersonPreview.callsharingreviewsclass(pnl[0].Id);
        Id rvwRecordTypeId1 = Schema.SObjectType.FC_Reviewer__External_Review__c.getRecordTypeInfosByName().get(Label.InPersonReview).getRecordTypeId();
        Id rvwRecordTypeId2 = Schema.SObjectType.FC_Reviewer__External_Review__c.getRecordTypeInfosByName().get(Label.InPersonPreview).getRecordTypeId();    
        List<FC_Reviewer__External_Review__c> rlist = [SELECT Id,FC_Reviewer__Contact__c,FC_Reviewer__Opportunity__c FROM FC_Reviewer__External_Review__c WHERE Panel__c = :pnl[0].Id  AND (RecordTypeId = :rvwRecordTypeId2 OR RecordTypeId = :rvwRecordTypeId1)];
        
            List<COI_Expertise__c> coilst2 = [SELECT Id, Related_Project__c, Reviewer_Name__c, Research_Application__c FROM COI_Expertise__c WHERE Panel_R2__c = :pnl[0].Id  AND Active__c = true AND Conflict_of_Interest__c = 'No'];
        system.assertEquals(coilst2.size(), rlist.size());
        system.assertEquals(coilst2.size(), 70);
        delete rlist[5];
        CreateInPersonReviewforAdmin.createInPersonRecord(pnl[0].Id);
        CreateInPersonReviewforAdmin.createInPersonRecord(pnl[0].Id);
        system.assertEquals(CreateInPersonReviewforAdmin.createInPersonRecord(pnl[0].Id), 'Review records were already created');
        test.stopTest();
    }
    
    /*Method to check if we click the move to inperson button on panel is working fine and also asserting the number of rvwslist created
will be equal to that of the COI with active status and conflict of interest indicated as No
*/    
    static testmethod void TestMethod2totestSharingoninsertforreviewswhenmovedtoinpersonwithYesCOI (){ 
        
        list<panel__c> pnl=TestDataFactory.getpanelwithoppandpanelassgnandcoiTestRecords(1,20,10,1,2);
        list<COI_Expertise__c> coilist=[select id,Active__c,Conflict_of_Interest__c from COI_Expertise__c where Panel_R2__c=:pnl[0].Id];
        coilist[0].Conflict_of_Interest__c='No';
        coilist[1].Conflict_of_Interest__c='No';
        coilist[2].Conflict_of_Interest__c='No';
        coilist[3].Conflict_of_Interest__c='No';
        coilist[4].Conflict_of_Interest__c='No';
        coilist[5].Conflict_of_Interest__c='No';
        coilist[6].Conflict_of_Interest__c='No';
        coilist[7].Conflict_of_Interest__c='Professional';
        coilist[8].Conflict_of_Interest__c='No';
        coilist[9].Conflict_of_Interest__c='No';
        coilist[10].Conflict_of_Interest__c='Personal';
        coilist[11].Conflict_of_Interest__c='No';
        coilist[12].Conflict_of_Interest__c='No';
        coilist[13].Conflict_of_Interest__c='No';
        coilist[14].Conflict_of_Interest__c='Institutional';
        coilist[15].Conflict_of_Interest__c='No';
        coilist[16].Conflict_of_Interest__c='No';
        coilist[17].Conflict_of_Interest__c='Financial';
        coilist[18].Conflict_of_Interest__c='No';
        coilist[19].Conflict_of_Interest__c='No';
        
        update coilist;
        CreateInPersonReview.createInPersonRecord(pnl[0].Id);
        Id rvwRecordTypeId1 = Schema.SObjectType.FC_Reviewer__External_Review__c.getRecordTypeInfosByName().get(Label.InPersonReview).getRecordTypeId();
        Id rvwRecordTypeId2 = Schema.SObjectType.FC_Reviewer__External_Review__c.getRecordTypeInfosByName().get(Label.InPersonPreview).getRecordTypeId();    
        List<FC_Reviewer__External_Review__c> rlist = [SELECT Id,FC_Reviewer__Contact__c,FC_Reviewer__Opportunity__c FROM FC_Reviewer__External_Review__c WHERE Panel__c = :pnl[0].Id  AND (RecordTypeId = :rvwRecordTypeId2 OR RecordTypeId = :rvwRecordTypeId1)];
        List<COI_Expertise__c> coilst2 = [SELECT Id, Related_Project__c, Reviewer_Name__c, Research_Application__c FROM COI_Expertise__c WHERE Panel_R2__c = :pnl[0].Id  AND Active__c = true AND Conflict_of_Interest__c = 'No'];
        system.assertEquals(coilst2.size(), 16);
        system.assertEquals(coilst2.size(), rlist.size());
        
    }
    
    /*Method to ensure even the sharing works for leads if any and then sharing is removed if lead gets updated accordingly.
* We dont know id this scenario actually exists but as it was in old trigger we ensured even this also exists and wrote this method
* purely for code coverage no asserting checks done here.
*/ 
    static testmethod void TestMethod3totestSharingoninsertandupdateforleads (){
        
        list<FC_Reviewer__External_Review__c> rvslisttoinsert = new  list<FC_Reviewer__External_Review__c>();
        list<FC_Reviewer__External_Review__c> rvslisttoupdate = new list<FC_Reviewer__External_Review__c>();
        list<contact> contactlisttoinsert = new list<contact>();
        list<user> userlisttoinsert = new list<user>();
        list<lead> leadlisttoinsert= new list<lead>();
        
        list<Account> acnt=TestDataFactory.getAccountTestRecords(1);
        list<contact> con=TestDataFactory.getContactTestRecords(1,5);
        list<User> usrs= TestDataFactory.getpartnerUserTestRecords(1, 5);
        list<lead> ld=TestDataFactory.getleadtestrecords(1,5,5);
        test.startTest();
        Id r3 = Schema.SObjectType.FC_Reviewer__External_Review__c.getRecordTypeInfosByName().get('Online Review').getRecordTypeId();
        
        FC_Reviewer__External_Review__c rvw1= new FC_Reviewer__External_Review__c();
        rvw1.FC_Reviewer__Deadline__c=DATE.today();
        rvw1.FC_Reviewer__Lead__c=ld[0].Id;
        rvw1.FC_Reviewer__Status__c='Ready to Review';
        rvw1.RecordTypeId=r3;
        rvslisttoinsert.add(rvw1);
        FC_Reviewer__External_Review__c rvw2= new FC_Reviewer__External_Review__c();
        rvw2.FC_Reviewer__Deadline__c=DATE.today();
        rvw2.FC_Reviewer__Lead__c=ld[1].Id;
        rvw2.FC_Reviewer__Status__c='Ready to Review';
        rvw2.RecordTypeId=r3;
        rvslisttoinsert.add(rvw2);
        FC_Reviewer__External_Review__c rvw3= new FC_Reviewer__External_Review__c();
        rvw3.FC_Reviewer__Deadline__c=DATE.today();
        rvw3.FC_Reviewer__Lead__c=ld[2].Id;
        rvw3.FC_Reviewer__Status__c='Ready to Review';
        rvw3.RecordTypeId=r3;
        rvslisttoinsert.add(rvw3);
        FC_Reviewer__External_Review__c rvw4= new FC_Reviewer__External_Review__c();
        rvw4.FC_Reviewer__Deadline__c=DATE.today();
        rvw4.FC_Reviewer__Lead__c=ld[3].Id;
        rvw4.FC_Reviewer__Contact__c=usrs[3].contactId;
        rvw4.FC_Reviewer__Status__c='Ready to Review';
        rvw4.RecordTypeId=r3;
        rvslisttoinsert.add(rvw4);
        FC_Reviewer__External_Review__c rvw5= new FC_Reviewer__External_Review__c();
        rvw5.FC_Reviewer__Deadline__c=DATE.today();
        rvw5.FC_Reviewer__Lead__c=ld[4].Id;
        rvw5.FC_Reviewer__Contact__c=usrs[4].contactId;
        rvw5.FC_Reviewer__Status__c='Ready to Review';
        rvw5.RecordTypeId=r3;
        rvslisttoinsert.add(rvw5);
        insert rvslisttoinsert;
        rvslisttoinsert[0].FC_Reviewer__Lead__c=ld[4].Id;
        rvslisttoinsert[1].FC_Reviewer__Lead__c=ld[3].Id;
        rvslisttoinsert[2].FC_Reviewer__Lead__c=ld[1].Id;
        rvslisttoinsert[3].FC_Reviewer__Lead__c=null;
        rvslisttoinsert[4].FC_Reviewer__Lead__c=ld[0].Id;
        recursiveTrigger.run=true;
        update rvslisttoinsert;
        test.stopTest();
    }
    
    /*Method to check that if initally online review was completed and then at later phase if coi was indicated in in person review phase
then sharing will be removed on opportunity so that the review record is no more visible in the dashboard and if reviewer trying 
to update in same session then validation will kick in not allowing to update or submit and also asserting the same below to ensure sharing 
is properly removed.
*/
    static testmethod void TestMethod4totestRRCSfunctionalitycoiforonlineandinpersonreviews (){
        
        Id recordTypeId2 = Schema.SObjectType.FC_Reviewer__External_Review__c.getRecordTypeInfosByName().get('Online Review - AD').getRecordTypeId();         
        Id recordTypeId3 = Schema.SObjectType.FC_Reviewer__External_Review__c.getRecordTypeInfosByName().get('In-Person Review').getRecordTypeId();         
        
        list<Account> acnt=TestDataFactory.getAccountTestRecords(1);
        list<contact> con=TestDataFactory.getContactTestRecords(1,7);
        list<User> usrs= TestDataFactory.getpartnerUserTestRecords(1, 7);        
        list<opportunity> opp=TestDataFactory.getopportunitytestrecords(1,7,1);
        list<panel__c> pnl=TestDataFactory.getpaneltestrecords(1);
        set<Id> reviewshareusersetafterupdate1= new set<Id>();
        set<Id> reviewshareusersetafterupdate2= new set<Id>();
        set<Id> oppsharesafterupdate= new set<Id>();
        test.startTest();
        FC_Reviewer__External_Review__c rr1= new FC_Reviewer__External_Review__c();
        rr1.FC_Reviewer__Contact__c=usrs[5].ContactId;
        rr1.FC_Reviewer__Opportunity__c=opp[0].Id;
        rr1.RecordTypeId=recordTypeId2;
        rr1.Panel__c=pnl[0].Id;
        insert rr1;
        List<FC_Reviewer__External_Review__share> reviewSharesafterinsert = [SELECT id, ParentId, UserOrGroupId From FC_Reviewer__External_Review__share where ParentId=:rr1.Id];
        for(FC_Reviewer__External_Review__share rv1:reviewSharesafterinsert){
            reviewshareusersetafterupdate1.add(rv1.UserOrGroupId);
        }
       // system.assertEquals(reviewshareusersetafterupdate1.contains(usrs[5].Id), true);
        FC_Reviewer__External_Review__c rr2= new FC_Reviewer__External_Review__c();
        rr2.FC_Reviewer__Contact__c=usrs[5].ContactId;
        rr2.Contact_Copied_due_to_COI__c=usrs[5].ContactId;
        rr2.FC_Reviewer__Opportunity__c=opp[0].Id;
        rr2.RecordTypeId=recordTypeId3;
        rr2.Panel__c=pnl[0].Id;
        insert rr2;
        recursiveTrigger.run=true;
        rr2.Conflict_of_Interest__c='Personal';
        update rr2;
        List<FC_Reviewer__External_Review__share> reviewSharesafterinsertandupdate2 = [SELECT id, ParentId, UserOrGroupId From FC_Reviewer__External_Review__share where ParentId=:rr2.Id];
        for(FC_Reviewer__External_Review__share rv2:reviewSharesafterinsertandupdate2){
            reviewshareusersetafterupdate2.add(rv2.UserOrGroupId);
        }
        
        List<opportunityteammember> oppshares = [SELECT id, OpportunityId, UserId From opportunityteammember where OpportunityId=:opp[0].Id];
        for(opportunityteammember oppps:oppshares){
            oppsharesafterupdate.add(oppps.UserId);
        }
        // system.assertEquals(reviewshareusersetafterupdate1.contains(usrs[5].Id), true);
        //system.assertEquals(reviewshareusersetafterupdate2.contains(usrs[5].Id), true);
        system.assertEquals(!oppsharesafterupdate.contains(usrs[5].Id), true);
        test.stopTest();
        
    }
    
    /*Method for RRCS to ensure reviews on insert will send email to internal reviewer and assign permission set could not assert as assigning permission
* set will be done in future*/
    static testmethod void TestMethod5totestRRCSfunctionalityoninsertandupdate (){
        
        list<Account> acnt=TestDataFactory.getAccountTestRecords(1);     
        list<user> usr=TestDataFactory.getstandardUserTestRecords(2);  
        
        Test.startTest();
        Id r2 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RRCA').getRecordTypeId();
        
        Opportunity o= new opportunity();
        o.Awardee_Institution_Organization__c=acnt[0].Id;
        o.RecordTypeId=r2;
        o.Full_Project_Title__c='test';
        o.Project_Name_off_layout__c='test';
        o.CloseDate=Date.today();
        o.StageName='In-Process';
        o.Name='test';
        insert o;
        Attachment a= new Attachment();
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        
        a.Body=bodyBlob;
        a.Name='RRCS-IPDMA : Test';
        a.ParentId=o.Id;
        insert a;
        
        Id r4 = Schema.SObjectType.FC_Reviewer__External_Review__c.getRecordTypeInfosByName().get('Research Related Conference Awards Review').getRecordTypeId();
        FC_Reviewer__External_Review__c rr1= new FC_Reviewer__External_Review__c();
        rr1.Internal_Reviewer__c = usr[0].Id;
        rr1.OwnerId = usr[1].Id;
        rr1.FC_Reviewer__Opportunity__c = o.Id;
        rr1.RecordTypeId=r4;
        rr1.FC_Reviewer__Status__c='Review In Progress';
        insert rr1;
        FC_Reviewer__External_Review__c rr2= new FC_Reviewer__External_Review__c();
        rr2.Internal_Reviewer__c = usr[0].Id;
        rr2.OwnerId = usr[1].Id;
        rr2.FC_Reviewer__Opportunity__c = o.Id;
        rr2.RecordTypeId=r4;
        rr2.FC_Reviewer__Status__c='Review In Progress';
        insert rr2;
        recursiveTrigger.run=true;
        rr1.I_Do_Not_Have_Conflict_with_Application__c=true;
        rr1.Budget_Cost_Proposal_Score__c='3 - Good';
        rr1.Past_Performance_Score__c='3 - Good';
        rr1.Personnel_Collaborators_Score__c='3 - Good';
        rr1.Project_Plan_and_Timeline_Score__c='3 - Good';
        rr1.Topic_Value_Score__c='3 - Good';
        rr1.Viable_IPD_MA_Collaboration_Score__c='3 - Good';
        rr1.Review_Complete__c=true;
        update rr1;
        recursiveTrigger.run=true;
        rr1.I_Do_Not_Have_Conflict_with_Application__c=false;
        rr1.I_Do_Have_a_Conflict_with_Application__c=true;
        rr1.If_so_please_describe__c='test';
        update rr1;
        test.stopTest();
    }
    
    /*Method for RRCS to ensure reviews on delete will delete permission ifthe internal reviewer has no more reviews assigned. could not assert as deleting permission
* set will be done in future*/
    static testmethod void TestMethod6totestRRCSfunctionalitytodeleteandchangeinternalrvwrforrrcs(){
        
        
        list<Account> acnt=TestDataFactory.getAccountTestRecords(1);     
        list<user> usr=TestDataFactory.getstandardUserTestRecords(3); 
        
        
        
        Id r2 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RRCA').getRecordTypeId();
        
        Opportunity o= new opportunity();
        o.Awardee_Institution_Organization__c=acnt[0].Id;
        o.RecordTypeId=r2;
        o.Full_Project_Title__c='test';
        o.Project_Name_off_layout__c='test';
        o.CloseDate=Date.today();
        o.StageName='In-Process';
        o.Name='test';
        insert o;
        Attachment a= new Attachment();
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        
        a.Body=bodyBlob;
        a.Name='RRCS-IPDMA : Test';
        a.ParentId=o.Id;
        insert a;
        Test.startTest();
        Id r4 = Schema.SObjectType.FC_Reviewer__External_Review__c.getRecordTypeInfosByName().get('Research Related Conference Awards Review').getRecordTypeId();
        FC_Reviewer__External_Review__c rr1= new FC_Reviewer__External_Review__c();
        rr1.Internal_Reviewer__c = usr[0].Id;
        rr1.OwnerId = usr[2].Id;
        rr1.FC_Reviewer__Opportunity__c = o.Id;
        rr1.RecordTypeId=r4;
        rr1.FC_Reviewer__Status__c='Review In Progress';
        insert rr1;
        recursiveTrigger.run=true;
        rr1.Internal_Reviewer__c=usr[1].Id;
        update rr1;
       // delete rr1;
        test.stopTest();
    }
    
    /*Method for RRCS to ensure reviews on insert will send email to internal reviewer and assign permission set and also updating the
* internal reviewer will assign permission set to the new internal reviewer and ensure old reviewer gets deleted the permission set if he has 
* no more revioews assigned. Could not assert as assigning permission set will be done in future*/
    static testmethod void TestMethod7totestRRCSfunctionalityoninsertandupdate (){
        
        
        list<Account> acnt=TestDataFactory.getAccountTestRecords(1);     
        list<user> usr=TestDataFactory.getstandardUserTestRecords(2); 
        
        
        Test.startTest();
        Id r2 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RRCA').getRecordTypeId();
        
        Opportunity o= new opportunity();
        o.Awardee_Institution_Organization__c=acnt[0].Id;
        o.RecordTypeId=r2;
        o.Full_Project_Title__c='test';
        o.Project_Name_off_layout__c='test';
        o.CloseDate=Date.today();
        o.StageName='In-Process';
        o.Name='test';
        insert o;
        Attachment a= new Attachment();
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        
        a.Body=bodyBlob;
        a.Name='RRCS-IPDMA : Test';
        a.ParentId=o.Id;
        insert a;
        
        Id r4 = Schema.SObjectType.FC_Reviewer__External_Review__c.getRecordTypeInfosByName().get('Research Related Conference Awards Review').getRecordTypeId();
        FC_Reviewer__External_Review__c rr2= new FC_Reviewer__External_Review__c();
        rr2.Internal_Reviewer__c = usr[0].Id;
        rr2.OwnerId = usr[1].Id;
        rr2.FC_Reviewer__Opportunity__c = o.Id;
        rr2.RecordTypeId=r4;
        rr2.FC_Reviewer__Status__c='Review In Progress';
        insert rr2;
        recursiveTrigger.run=true;
        rr2.I_Do_Not_Have_Conflict_with_Application__c=true;
        rr2.Budget_Cost_Proposal_Score__c='3 - Good';
        rr2.Past_Performance_Score__c='3 - Good';
        rr2.Personnel_Collaborators_Score__c='3 - Good';
        rr2.Project_Plan_and_Timeline_Score__c='3 - Good';
        rr2.Topic_Value_Score__c='3 - Good';
        rr2.Viable_IPD_MA_Collaboration_Score__c='3 - Good';
        rr2.Review_Complete__c=true;
        update rr2;
        test.stopTest();
    }
    
    public static testmethod void TestMethod8totestbeforeUpdateofReviewsforHistory (){
       
        list<FC_Reviewer__External_Review__c> rvws=TestDataFactory.getReviewTestRecords(10,10,10,10);  
        list<FC_Reviewer__External_Review__c> rvws1= new list<FC_Reviewer__External_Review__c>();
        
        for(FC_Reviewer__External_Review__c r:rvws){
            r.MRO_Action__c='Revisions Out';
            rvws1.add(r);
        }
         test.startTest();
        
        update rvws1;
        test.stopTest();
    }
    
        public static testmethod void TestMethod9beforeUpdateofReviewsforHistory (){
       
        list<FC_Reviewer__External_Review__c> rvws=TestDataFactory.getReviewTestRecords(10,10,10,10);  
        list<FC_Reviewer__External_Review__c> rvws1= new list<FC_Reviewer__External_Review__c>();
        
        for(FC_Reviewer__External_Review__c r:rvws){
            r.Criterion_1_Strengths__c='';
r.Criterion_2_Strengths__c='';
r.Criterion_3_Strengths__c='';
r.Criterion_4_Strengths__c='';
r.Criterion_5_Strengths_r2__c='';
r.Criterion_6_Strengths__c='';
r.Criterion_1_Weaknesses__c='';
r.Criterion_2_Weaknesses__c='';
r.Criterion_3_Weaknesses__c='';
r.Criterion_4_Weaknesses__c='';
r.Criterion_5_Weaknesses__c='';
r.Criterion_6_Weaknesses__c='';
r.Over_all_Comments__c='';
            r.MRO_Action__c='Revisions Out';
            rvws1.add(r);
        }
         test.startTest();
        
        update rvws1;
        test.stopTest();
    }
    
}