/* This test class creates, inserts and updates opportunity, IRB, TASK, Milestone, Progress Report */

@istest
public class UpdateEmailsTest{

       static testMethod void InsertTestData() {

            
           List<Opportunity> oppList = New List<Opportunity>();
           integer i = 0;
          // Creating  a new test user           
           Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator - Accenture']; //''Standard User'];
           User u1 = new User(Alias = 'standt', Email='standarduse12r@testorg.com',
           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
           LocaleSidKey='en_US', ProfileId = p.Id,
           TimeZoneSidKey='America/Los_Angeles', UserName='standarduser768@testorg.com');
           
           User u2 = new User(Alias = 'standt12', Email='standarduser34565@testorg.com',
           EmailEncodingKey='UTF-8', LastName='Testing3434', LanguageLocaleKey='en_US',
           LocaleSidKey='en_US', ProfileId = p.Id,
           TimeZoneSidKey='America/Los_Angeles', UserName='standard7687@testorg.com');
           System.runAs(u1) {
             // The following code runs as user u1'
             system.assertEquals(p.id,UserInfo.getProfileId());
           }// end runAS
            System.runAs(u2) {
             // The following code runs as user u2'
             system.assertEquals(p.id,UserInfo.getProfileId());
            }// end runAS
            
           List<Opportunity> oppList2 = new List<Opportunity>();
                     
          // Creating and inserting a new record  
           Opportunity opp = new Opportunity();  
           opp.Name = 'TestProject11';
           opp.Project_Name_off_layout__c = 'TestProject11';
           opp.Full_Project_Title__c = 'TestProject11';
           opp.Project_Name_off_layout__c = 'TestProject11';
           opp.closeDate = System.now().date();
           opp.StageName = 'To Be Searhced';
           opp.Full_Project_Title__c = 'TestProject1';
           opp.PI_Name_User__c = u1.id;
           opp.PI_Project_Lead_2_Name_New__c = u1.id;
           opp.PI_Project_Lead_Designee_1_New__c = u1.id;
           opp.PI_Project_Lead_Designee_2_Name_New__c = u1.id;
           opp.Program_Associate__c = u1.id;
           opp.Program_Officer__c = u1.id;
           opp.Engagement_Officer__c = u1.id;
           opp.AO_Name_User__c = u2.id;
           opp.Contracts_Administrator__c = u1.id;
           opp.Contract_Coordinator__c = u1.id;
           
           insert opp;
           Id oppId = opp.Id; 
          
          /* Creating and Inserting new IRB Record   */ 
           IRB__C irb = new IRB__C();
           irb.Name = 'IRB Test';
           irb.IRB_Approval_Expiration_Date__c=Date.newInstance(2028,12,31);
           irb.Project_Title__c = oppId;
           insert irb;  
           
            /* Creating and Inserting new Task Record */
           task t = new task(Subject='Test',ActivityDate=Date.Today(), WhatId = opp.id);
           insert t;
           test.startTest();
            /* Creating and Inserting new Milestone Record */
           FGM_Base__Benchmark__c milestone = new FGM_Base__Benchmark__c();
           milestone.Milestone_Name__c = 'Mielstone1';
           milestone.Milestone_Status__c = 'Not Completed';
           milestone.FGM_Base__Due_Date__c = Date.Today();
           milestone.Milestone_Type__c = 'Recruitment';
           milestone.FGM_Base__Request__c = opp.id;
           insert milestone;
           
           /* Creating and Inserting new Progress Report Record */
           FGM_Base__Grantee_Report__c PR = new FGM_Base__Grantee_Report__c();
           PR.FGM_Base__Request__c = opp.id;
           PR.Progress_Report_Name__c = 'Test PR';
           PR.FGM_Base__Status__c = 'Open';
           insert PR;
         
         /* Updating the existing opportunity record */        
          opp.Project_Assessment_Status__c = 'Slight Delay';
          opp.Contract_Assessment_Status__c = 'Slight Delay';
          opp.PI_Name_User__c = u2.id;
          opp.PI_Project_Lead_2_Name_New__c = u2.id;
          opp.PI_Project_Lead_Designee_1_New__c = u2.id;
          opp.PI_Project_Lead_Designee_2_Name_New__c = u2.id;  
          opp.PI_Project_Lead_Designee_1_New__c = u2.id;
          opp.PI_Project_Lead_Designee_2_Name_New__c = u2.id;
          opp.Program_Associate__c = u2.id;
          opp.Program_Officer__c = u2.id;
          opp.Engagement_Officer__c = u2.id;
          opp.AO_Name_User__c = u1.id;
          opp.Contracts_Administrator__c = u2.id;
          opp.Contract_Coordinator__c = u2.id;
          update opp;    
           
          /* Updating the existing IRB record */                            
          irb.Name = 'IRB Test2';
          update irb;
                                
           /* Updating the existing Task record */ 
           t.Subject='Test123';
           update t;
           
           /* Updating the existing Milestone record */ 
           milestone.Milestone_Name__c = 'Mielstone1';
           milestone.Milestone_Status__c = 'Completed';
           milestone.FGM_Base__Completion_Date__c = System.now().date();
           update milestone;
                            
            /* Updating the existing ProgressReport record */ 
            PR.Progress_Report_Name__c = 'Test PR123';
            update PR;
                                 
test.stopTest();      
      }// end Testmethod


}//end class