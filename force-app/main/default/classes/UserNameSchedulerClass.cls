global class UserNameSchedulerClass implements Schedulable{
  
    public static String query = 'SELECT Id,contactId,username FROM User WHERE ContactId!=null AND Contact.User_Name_for_MR__c=NULL';
    
    global void execute(SchedulableContext ctx) {
         
        Database.executeBatch(new batchUpdateUserNameforMRonContact(query),50); 
    } 
}