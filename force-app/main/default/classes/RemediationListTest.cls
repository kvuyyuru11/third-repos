@isTest
public class RemediationListTest {

    private static testMethod void testConstructor(){
        RecordType rec = [SELECT Id FROM RecordType WHERE DeveloperName = 'Concern' AND sObjectType = 'Remediation__c'];
        RecordType recOpp = [SELECT Id FROM RecordType WHERE DeveloperName = 'Research_Awards' AND sObjectType = 'Opportunity'];
        RecordType recAcc = [SELECT Id FROM RecordType WHERE DeveloperName = 'Standard_Salesforce_Account' AND sObjectType = 'Account'];
        
        List<User> users = ObjectCreator.getUsers(20, 'Donnie');
        insert users;
        
        //Create account
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.RecordTypeId = recAcc.Id;
        insert acc;
        
        //Create an opportunity
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = recOpp.Id;
        opp.AccountId = acc.Id;
        opp.Contract_Number__c = '03423';
        opp.Name = 'Test';
        opp.StageName = 'In Progress';
        opp.Application_Number__c = '12213';
        opp.Full_Project_Title__c = 'Project Test';
        opp.CloseDate = Date.newInstance(2016, 12, 22);
        opp.Contract_Administrator__c = Users.get(0).id;
        opp.Program_Officer__c = users.get(1).id;
        insert opp;
   
        List<Remediation__c> RemList = new List<Remediation__c>();
        for(Integer i = 0; i < 5; i++){
            Remediation__c r = new Remediation__c();
            r.RecordTypeId = rec.Id;
            r.Project__c = opp.Id;
            r.Program_Officer__c = users.get(0).id;
            r.Contract_Administrator__c = users.get(1).id;
            r.Remediation_Status__c = 'Submitted';
            RemList.add(r);
        } 
        
  
//Remediation_Status__c = 'Pending Remediation Plan' OR
//Remediation_Status__c = 'Submitted' OR
//Remediation_Status__c = 'Rejected - Needs Revision' OR
//Remediation_Status__c = 'Pending Final Remediation Plan' OR
//Remediation_Status__c = 'Submitted Final Remediation Plan' OR
//Remediation_Status__c = 'Rejected Final Plan – Additional Information Requested' 


        RemediationList rlEmpty = new RemediationList(new ApexPages.StandardController(opp));
        insert RemList;
        RemediationList rlWithRecordss = new RemediationList(new ApexPages.StandardController(opp));
    }

}