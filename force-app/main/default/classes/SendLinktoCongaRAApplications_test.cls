@IsTest
public class SendLinktoCongaRAApplications_test{
    
    public static testmethod void SendLinktoCongaRAApplications_test1() {
        Id devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RA Applications').getRecordTypeId();
        Opportunity Opp = new Opportunity(Name='Test Opportunity',Project_Name_off_layout__c='Test Opportunity',StageName='Invited',closedate=system.today(),LOI_resubmission_D_I__c='Yes',D_I_Resubmission__c='Yes',Was_LOI_invited_D_I__c='Yes',Previous_Application__c='123456',Number_of_submissions__c='1',Summary_Statement_received__c='Yes',Application_Number__c='123',RecordTypeId=devRecordTypeId);
        insert Opp;
        
        ContentVersion contentVersion_1 = new ContentVersion(
            Title = Opp.Name+' '+Opp.Application_Number__c+' Application Budget.pdf',
            PathOnClient = 'Penguins.pdf',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert contentVersion_1;      
        
        ContentVersion contentVersion_2 = [Select Id,ContentDocumentId from Contentversion where Id=:contentVersion_1.Id];
        
        contentdocumentLink cdl = new contentdocumentLink();
        cdl.LinkedEntityId = Opp.Id;
        cdl.ContentDocumentId = contentVersion_2.ContentDocumentId;
        cdl.ShareType='I';
        insert cdl; 
        
        FGM_Portal__Quiz__c quiz = new FGM_Portal__Quiz__c();
        quiz.Name='17 cycle 3'; 
        Insert quiz;
        
        FGM_Portal__Related_List_Controller__c rlc = new FGM_Portal__Related_List_Controller__c();
        rlc.Name = 'Templates and Uploads';
        rlc.FGM_Portal__ObjectName__c = 'Opportunity';
        rlc.FGM_Portal__ParentObject__c = 'Opportunity';
        rlc.FGM_Portal__LookupField__c = 'Opportunity';
        rlc.FGM_Portal__Quiz__c = quiz.Id;
        rlc.RecordTypeId='012700000001ewl';
        insert rlc;
        
        FGM_Portal__Questions__c quest = new FGM_Portal__Questions__c();
        quest.FGM_Portal__Question__c = 'Research Plan';
        quest.FGM_Portal__Type__c = 'Attachment';
        insert quest;
        
        FGM_Portal__Quiz_Questions__c qq = new FGM_Portal__Quiz_Questions__c();
        qq.FGM_Portal__Question__c = quest.Id;
        qq.FGM_Portal__Quiz__c = quiz.Id;
        
        FGM_Portal__Question_Attachment__c qa = new FGM_Portal__Question_Attachment__c();
        qa.FGM_Portal__Question__c = quest.Id;
        qa.FGM_Portal__Opportunity__c = Opp.Id;
        insert qa;
        
        Attachment attach1=new Attachment(); 
        attach1.Name = '123_PriorSummaryStatement.pdf'; 
        Blob bodyBlob1=Blob.valueOf('Unit Test Attachment Body'); 
        attach1.body=bodyBlob1; 
        attach1.parentId=Opp.id;
        attach1.ContentType = 'pdf'; 
        attach1.IsPrivate = false; 
        attach1.Description = 'Test'; 
        insert attach1;
        
        Attachment attach2=new Attachment(); 
        attach2.Name='Unit Test Attachment'; 
        Blob bodyBlob2=Blob.valueOf('Unit Test Attachment Body'); 
        attach2.body=bodyBlob2; 
        attach2.parentId=qa.id;
        attach2.ContentType = '.pdf'; 
        attach2.IsPrivate = false; 
        attach2.Description = 'Test'; 
        insert attach2; 
        
        SendLinktoCongaRAApplications.updateCongarequestLink(Opp.Id,'abc','abc','abc');
        SendLinktoCongaRAApplicationsMethods.updateCongarequestLink(Opp.Id,'abc','abc','abc');
        
        Opp.D_I_Resubmission__c='';
        Opp.LOI_resubmission_D_I__c='';
        Opp.D_I_Resubmission__c='';
        Opp.Was_LOI_invited_D_I__c='';
        Opp.Number_of_submissions__c='';
        Opp.Summary_Statement_received__c='';
        update Opp;
        SendLinktoCongaRAApplications.updateCongarequestLink(Opp.Id,'abc','abc','abc');
        SendLinktoCongaRAApplicationsMethods.updateCongarequestLink(Opp.Id,'abc','abc','abc');
        
        Campaign cmp = new Campaign(Name='Dissemination and Implementation');
        cmp.FGM_Portal__Application_Quiz__c = quiz.Id;
        cmp.EndDate = Date.newInstance(2016, 12, 9);
        insert cmp;
        Opp.CampaignId = cmp.Id;
        
        update Opp;
        test.startTest();
        SendLinktoCongaRAApplications.updateQuiz(Opp.Id);
        //SendLinktoCongaRAApplicationsMethods.updateQuiz(Opp.Id);
        SendLinktoCongaRAApplications.updateCongarequestLink(Opp.Id,'abc','abc','abc');
        SendLinktoCongaRAApplicationsMethods.updateCongarequestLink(Opp.Id,'abc','abc','abc');
        SendLinktoCongaRAApplications.updateQuiz(Opp.Id);
        //SendLinktoCongaRAApplicationsMethods.updateQuiz(Opp.Id);
        test.stopTest();
        
    }
    public static testmethod void SendLinktoCongaRAApplications_test2(){
        Id devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RA Applications').getRecordTypeId();
        Opportunity Opp = new Opportunity(Name='Test Opportunity',Project_Name_off_layout__c='Test Opportunity',StageName='Invited',closedate=system.today(),LOI_resubmission_D_I__c='Yes',D_I_Resubmission__c='Yes',Previous_Application__c='123456',Was_LOI_invited_D_I__c='Yes',Number_of_submissions__c='1',Summary_Statement_received__c='Yes',Application_Number__c='123',RecordTypeId=devRecordTypeId);
        insert Opp;
        
        FGM_Portal__Questions__c quest2 = new FGM_Portal__Questions__c();
        quest2.FGM_Portal__Question__c = 'Research Plan';
        quest2.FGM_Portal__Type__c = 'Attachment';
        insert quest2;
        
        FGM_Portal__Question_Attachment__c qa2 = new FGM_Portal__Question_Attachment__c();
        qa2.FGM_Portal__Question__c = quest2.Id;
        qa2.FGM_Portal__Opportunity__c = Opp.Id;
        insert qa2;        
        
        SendLinktoCongaRAApplications.updateCongarequestLink(Opp.Id,'abc','abc','abc');
        SendLinktoCongaRAApplicationsMethods.updateCongarequestLink(Opp.Id,'abc','abc','abc');
        
        delete Opp;
        
        SendLinktoCongaRAApplications.updateCongarequestLink(Opp.Id,'abc','abc','abc');
        SendLinktoCongaRAApplicationsMethods.updateCongarequestLink(Opp.Id,'abc','abc','abc');
    }
    
    public static testmethod void SendLinktoCongaRAApplications_test3(){
        Id devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RA Applications').getRecordTypeId();
        Opportunity Opp = new Opportunity(Name='Test Opportunity',Project_Name_off_layout__c='Test Opportunity',StageName='Invited',closedate=system.today(),LOI_resubmission_D_I__c='Yes',D_I_Resubmission__c='Yes',Was_LOI_invited_D_I__c='Yes',Previous_Application__c='123456',Number_of_submissions__c='1',Summary_Statement_received__c='Yes',Application_Number__c='123',RecordTypeId=devRecordTypeId);
        insert Opp;       
        
        ContentVersion contentVersion_1 = new ContentVersion(
            Title = Opp.Name+' '+Opp.Application_Number__c+' Application Budget.pdf',
            PathOnClient = 'Penguins.pdf',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert contentVersion_1;      
        
        ContentVersion contentVersion_2 = [Select Id,ContentDocumentId from Contentversion where Id=:contentVersion_1.Id];
        
        contentdocumentLink cdl = new contentdocumentLink();
        cdl.LinkedEntityId = Opp.Id;
        cdl.ContentDocumentId = contentVersion_2.ContentDocumentId;
        cdl.ShareType='I';
        insert cdl;     
        
        FGM_Portal__Quiz__c quiz = new FGM_Portal__Quiz__c();
        quiz.Name='17 cycle 3'; 
        Insert quiz;
        
        FGM_Portal__Related_List_Controller__c rlc = new FGM_Portal__Related_List_Controller__c();
        rlc.Name = 'Templates and Uploads';
        rlc.FGM_Portal__ObjectName__c = 'Opportunity';
        rlc.FGM_Portal__ParentObject__c = 'Opportunity';
        rlc.FGM_Portal__LookupField__c = 'Opportunity';
        rlc.FGM_Portal__Quiz__c = quiz.Id;
        rlc.RecordTypeId='012700000001ewl';
        insert rlc;
        
        FGM_Portal__Questions__c quest = new FGM_Portal__Questions__c();
        quest.FGM_Portal__Question__c = 'Research Plan';
        quest.FGM_Portal__Type__c = 'Attachment';
        insert quest;
        
        FGM_Portal__Quiz_Questions__c qq = new FGM_Portal__Quiz_Questions__c();
        qq.FGM_Portal__Question__c = quest.Id;
        qq.FGM_Portal__Quiz__c = quiz.Id;
        
        FGM_Portal__Question_Attachment__c qa = new FGM_Portal__Question_Attachment__c();
        qa.FGM_Portal__Question__c = quest.Id;
        qa.FGM_Portal__Opportunity__c = Opp.Id;
        insert qa;
        
        Attachment attach1=new Attachment(); 
        attach1.Name = '123_PriorSummaryStatement.pdf'; 
        Blob bodyBlob1=Blob.valueOf('Unit Test Attachment Body'); 
        attach1.body=bodyBlob1; 
        attach1.parentId=Opp.id;
        attach1.ContentType = 'pdf'; 
        attach1.IsPrivate = false; 
        attach1.Description = 'Test'; 
        insert attach1;
        
        Attachment attach2=new Attachment(); 
        attach2.Name='Unit Test Attachment'; 
        Blob bodyBlob2=Blob.valueOf('Unit Test Attachment Body'); 
        attach2.body=bodyBlob2; 
        attach2.parentId=qa.id;
        attach2.ContentType = '.pdf'; 
        attach2.IsPrivate = false; 
        attach2.Description = 'Test'; 
        insert attach2; 
        
        SendLinktoCongaRAApplications.updateCongarequestLink(Opp.Id,'abc','abc','abc');
        SendLinktoCongaRAApplicationsMethods.updateCongarequestLink(Opp.Id,'abc','abc','abc');
        
        Opp.D_I_Resubmission__c='';
        Opp.LOI_resubmission_D_I__c='';
        Opp.D_I_Resubmission__c='';
        Opp.Was_LOI_invited_D_I__c='';
        Opp.Number_of_submissions__c='';
        Opp.Summary_Statement_received__c='';
        update Opp;
        SendLinktoCongaRAApplications.updateCongarequestLink(Opp.Id,'abc','abc','abc');
        SendLinktoCongaRAApplicationsMethods.updateCongarequestLink(Opp.Id,'abc','abc','abc');
        
        Campaign cmp = new Campaign(Name='Dissemination and Implementation');
        cmp.FGM_Portal__Application_Quiz__c = quiz.Id;
        cmp.EndDate = Date.newInstance(2016, 12, 9);
        insert cmp;
        Opp.CampaignId = cmp.Id;
        
        update Opp;        
        
        //SendLinktoCongaRAApplicationsMethods.updateQuiz(Opp.Id);
        SendLinktoCongaRAApplications.updateCongarequestLink(Opp.Id,'abc','abc','abc');
        SendLinktoCongaRAApplicationsMethods.updateCongarequestLink(Opp.Id,'abc','abc','abc');
        //SendLinktoCongaRAApplicationsMethods.updateQuiz(Opp.Id);
    }    
    
    public static testmethod void SendLinktoCongaRAApplications_test4(){
        Id devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RA Applications').getRecordTypeId();
        Opportunity Opp = new Opportunity(Name='Test Opportunity',Project_Name_off_layout__c='Test Opportunity',Previous_Application__c='123456',StageName='Invited',closedate=system.today(),LOI_resubmission_D_I__c='Yes',D_I_Resubmission__c='Yes',Was_LOI_invited_D_I__c='Yes',Number_of_submissions__c='1',Summary_Statement_received__c='Yes',Application_Number__c='123',RecordTypeId=devRecordTypeId);
        insert Opp;
        
        ContentVersion contentVersion_1 = new ContentVersion(
            Title = Opp.Name+' '+Opp.Application_Number__c+' Application Budget.pdf',
            PathOnClient = 'Penguins.pdf',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true);
        insert contentVersion_1;      
        
        ContentVersion contentVersion_2 = [Select Id,ContentDocumentId from Contentversion where Id=:contentVersion_1.Id];
        
        contentdocumentLink cdl = new contentdocumentLink();
        cdl.LinkedEntityId = Opp.Id;
        cdl.ContentDocumentId = contentVersion_2.ContentDocumentId;
        cdl.ShareType='I';
        insert cdl;
        
        FGM_Portal__Quiz__c quiz = new FGM_Portal__Quiz__c();
        quiz.Name='17 cycle 3'; 
        Insert quiz;
        
        FGM_Portal__Related_List_Controller__c rlc = new FGM_Portal__Related_List_Controller__c();
        rlc.Name = 'Templates and Uploads';
        rlc.FGM_Portal__ObjectName__c = 'Opportunity';
        rlc.FGM_Portal__ParentObject__c = 'Opportunity';
        rlc.FGM_Portal__LookupField__c = 'Opportunity';
        rlc.FGM_Portal__Quiz__c = quiz.Id;
        rlc.RecordTypeId='012700000001ewl';
        insert rlc;
        
        FGM_Portal__Questions__c quest = new FGM_Portal__Questions__c();
        quest.FGM_Portal__Question__c = 'Budget Detailed Attachment';
        quest.FGM_Portal__Type__c = 'Attachment';
        quest.FGM_Portal__Quiz__c = quiz.Id;
        insert quest;
        
        FGM_Portal__Quiz_Questions__c qq = new FGM_Portal__Quiz_Questions__c();
        qq.FGM_Portal__Question__c = quest.Id;
        qq.FGM_Portal__Quiz__c = quiz.Id;
        qq.FGM_Portal__Tab__c  = rlc.Id;
        insert qq;
        
        Campaign cmp = new Campaign(Name='Dissemination and Implementation');
        cmp.FGM_Portal__Application_Quiz__c = quiz.Id;
        cmp.EndDate = Date.newInstance(2016, 12, 9);
        insert cmp;
        Opp.CampaignId = cmp.Id;
        
        update Opp;
        
        SendLinktoCongaRAApplications.updateQuiz(Opp.Id);
        //SendLinktoCongaRAApplicationsMethods.updateQuiz(Opp.Id);
    }
}