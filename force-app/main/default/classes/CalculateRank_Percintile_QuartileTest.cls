@isTest
public class CalculateRank_Percintile_QuartileTest {
    private static testMethod void classController(){
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        RecordType recOpp = [SELECT Id FROM RecordType WHERE sObjectType = 'Opportunity' AND Name = 'RA Applications'];
        RecordType recAcc = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Standard Salesforce Account'];
        User usr;
        System.runAs (thisUser){
            Profile pr = [SELECT Id FROM Profile WHERE Name='Science Program Operations'];
            UserRole r = [SELECT Id FROM UserRole WHERE Name='Science Method Effectiveness Research Team'];
            usr = new User(alias = 'jonna', email='jonnasmith@acme.com',
                emailencodingkey='UTF-8', lastname='Smith',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = pr.Id, userroleid = r.Id,
                timezonesidkey='America/New_York',
                username='jonnasmith@acme.com');
            insert usr;
            
            Cycle__c cy = new Cycle__c();
            cy.Name = 'Test';
            cy.COI_Due_Date__c = Date.newInstance(2016,12,11);
            insert cy;
            
            Panel__c p = new Panel__c();
            p.Name = 'Test Panel';
            p.MRO__c = usr.Id;
            p.Cycle__c = cy.Id;
            p.Panel_Due_Dtae__c=  Date.newInstance(2019,12,20); 
            insert p;
            
            Account acc = new Account();
            acc.RecordTypeId = recAcc.Id;
            acc.Name = 'Test Acc';
            insert acc;
            
            List<Opportunity> opplst = new List<Opportunity>();
            for(Integer i = 0; i < 10; i++){
                Opportunity opp = new Opportunity();
                opp.RecordTypeId = recOpp.Id;
                opp.Panel__c = p.Id;
                opp.Awardee_Institution_Organization__c = acc.Id;
                opp.Full_Project_Title__c = 'test0';
                opp.Project_Name_off_layout__c='test0';
                opp.Name = 'Test' + i;
                opp.Discussion_Order_Rank__c = 12.0 + i;
                opp.Average_In_Person_Score__c = 1.0;
                opp.Percentile__c = 20.0;
                opp.Quartile__c = 2.0;
                opp.CloseDate = Date.newInstance(2016, 12, 31);
                opp.Application_Number__c = '45125';
                opp.StageName = 'In Progress';
                opplst.add(opp);
            } 
            insert opplst;
            
            PageReference pg = new PageReference('/apex/CalculateRank_ForwardPage');
            Test.setCurrentPage(pg);
            pg.getParameters().put('panel',p.Id);
            
            CalculateRank_Percintile_Quartile cpq = new CalculateRank_Percintile_Quartile();
            cpq.doRankCalc();
            
            CalculateRank_Percintile_Quartile cpq1 = new CalculateRank_Percintile_Quartile();
            cpq1.workAndRedirect();
        }
    }
}