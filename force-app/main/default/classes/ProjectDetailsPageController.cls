public class ProjectDetailsPageController {

 public Id recid;
 
Public Account kwa;
   public list<Opportunity> opps1; 
   set<Id> def= new set<Id>();
    
        
   public ProjectDetailsPageController(ApexPages.StandardController controller) {
        //Gets the particular record id
        
          recid=apexpages.currentpage().getparameters().get('id');
       
       
   }
    
    public Account getKWAccounts() {
        
        kwa=[select Name, kw__District__c,kw__StateOfCoverage__c from Account where Id=:recid ];
    return kwa;
            
    }
   
        public List<Opportunity> getProjects() {
                  
        
             list<kwzd__KnowWho_Elected_Official__c> a=[select Id,Name, kwzd__Account__c from kwzd__KnowWho_Elected_Official__c where kwzd__Elected_Official_Office__c=:recid ];
            
			 for(kwzd__KnowWho_Elected_Official__c act:a){
                 def.add(act.kwzd__Account__c);
             }
             
         
         opps1= [Select Id, AccountId,Account.Name,StageName,Awardee_Institution_Organization__c,Contracted_Budget__c,Contract_Number__c,Name,Project_End_Date__c,Project_Start_Date__c,Amount from Opportunity where AccountId IN:def And (StageName=:'Awarded' OR StageName=:'Closed' OR StageName=:'Executed' OR StageName=:'Executed Contract' OR StageName=:'Awarded - PPLNs' OR StageName=:'Executed Contract - PPLNs')];
            if(opps1.size()>0){return opps1;}
            else{
                  return null;
            }
     }
    
 
  
    
}