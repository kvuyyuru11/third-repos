/****************************************************************************************************
*Description:           Test unit for ap_HomeController
*                       
*
*Required Class(es):    N/A
*
*Organization: PCORI / Accenture
*Revision   |Date       |Author             |AdditionalNotes
*====================================================================================================
*   1.00     1/10/2016    Anirudh Sharma         Inital version
*   1.01     1/10/2016    Daniel Haro            Heavy redesign
*****************************************************************************************************/  

@isTest

private class AP_HomeControllerTest {

    private static testMethod void Mytest(){
   
         Contact contact1;// = new Contact;
        Profile Profile1 = [SELECT Id, Name FROM Profile WHERE Name = 'PCORI Community Partner' limit 1];
         String profileId = Profile1.id;
      String accountId = Label.CommunityAdminProfile;
      
      //Profile CProfile = [SELECT Id, Name FROM Profile WHERE Name ='Customer Community Login User'];
      Profile AProfile = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' limit 1];
      //User u = [SELECT Id FROM User WHERE ProfileId =:AProfile.Id AND isActive = true LIMIT 1];
      User u = [SELECT Id FROM User WHERE ProfileId =:AProfile.Id AND isActive = true AND UserRoleId!=null LIMIT 1];
      User temp = new User();
      User user2 = new User();
    system.runAs(u){ 
    Account account = new Account(Name='Test');
        insert(account);
        Contact contact = new Contact(FirstName='Rainmaker', LastName='Test', AccountId=account.Id);
        insert(contact);
        temp.Email = 'testinguib@test.com';
        temp.ContactId = contact.Id;
        temp.UserName = 'testinguib@test.com';
        temp.LastName = 'Padilla';
        temp.Alias = 'jpadille';
        temp.CommunityNickName = 'jpadilla';
        temp.TimeZoneSidKey = 'GMT';
        temp.LocaleSidKey = 'en_US';
        temp.EmailEncodingKey = 'ISO-8859-1';
        temp.ProfileId = profileId;
        temp.LanguageLocaleKey = 'en_US';
       
        String userId = Site.createPortalUser(temp, accountId, 'Testing');

        contact1 = new Contact(FirstName='Rainmaker1', LastName='Test1', AccountId=account.Id,Reviewer_Status__c='New Reviewer');
        insert(contact1); 
        contact1 = [select id,firstname,lastName,AccountID from Contact where lastName =: 'Test1' limit 1];
    
    }
        
   Test.startTest();     
       // Contact contact1 = [select id,firstname,lastName,AccountID from Contact where lastName =: 'Test1'];

        Advisory_Panel_Application__c app1 = new Advisory_Panel_Application__c();
        app1.Application_Status__c = 'Draft';
        app1.Contact__c = contact1.id;
        app1.Application_Status_New__c = 'Draft';
        
        Advisory_Panel_Application__c app2 = new Advisory_Panel_Application__c();
        app2.Application_Status__c = 'Submited';
        app2.Contact__c = contact1.id;
        app2.Application_Status_New__c = 'Submitted';
       
        System.runAs(temp){
            app1.Contact__c = temp.ContactId; //UserInfo.getUserId();
            
              insert app1;
              AP_HomeController testCon = new AP_HomeController();
             
            app1 = [select id,Application_Status__c,Contact__c,Application_Status_New__c from Advisory_Panel_Application__c
                     where Contact__c =: temp.ContactId limit 1];
             app1.Application_Status_New__c = 'Submitted';
            update app1;
               // AP_HomeController 
               testCon = new AP_HomeController();

             app1.Application_Status_New__c = 'Not Selected';
            update app1;
              testCon = new AP_HomeController();
            
            
           //  app1.Application_Status_New__c = 'Selected';
           // update app1;
             // testCon = new AP_HomeController();
            
            
            testCon.cloneApplication();
             // update app1;
               testCon = new AP_HomeController();
               app1.Application_Status__c = 'Withdrawn';
               testCon = new AP_HomeController();
               testcon.withdrawmethod();
              testCon.showButton2 = true;
              testCon.showButton3 = true;
              testCon.showButton4 = true;
            testCon.APAList = null;
            pagereference stub = testCon.SApplication();

        }
       
        System.runAs(temp){
              //insert app2;
              AP_HomeController testCon = new AP_HomeController();
              app2.Application_Status__c = 'Submitted';
               
            
            
           testCon.cloneApplication();
              //update app2;
               testCon = new AP_HomeController();
               app1.Application_Status__c = 'Withdrawn';
               testCon = new AP_HomeController();
               testcon.withdrawmethod();
              testCon.showButton2 = true;
              testCon.showButton3 = true;
              testCon.showButton4 = true;
            
            

        }
       
      
        
        
        
    test.stopTest();
    
     
    
}
}