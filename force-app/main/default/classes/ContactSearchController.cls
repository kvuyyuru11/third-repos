/**
    * ContactSearchController - <description>
    * @author: Rainmaker
    * @version: 1.0
*/

public  class ContactSearchController{

 private final Contact con;
    // the soql without the order and limit
    
    private String strLast {get;set;}
    private String strFirst {get;set;}
    public boolean enabled {get;set;}
  
    
  private String soql {get;set;}
  // the collection of contacts to display
  public List<Contact> contacts {get;set;}
 
  // the current sort direction. defaults to asc
  public String sortDir {
    get  { if (sortDir == null) {  sortDir = 'asc'; } return sortDir;  }
    set;
  }
 
  // the current field to sort by. defaults to last name
  public String sortField {
    get  { if (sortField == null) {sortField = 'lastName'; } return sortField;  }
    set;
  }
 
  // format the soql for display on the visualforce page
  public String debugSoql {
    get { return soql + ' order by ' + sortField + ' ' + sortDir + ' limit 20'; }
    set;
  }
 
  // init the controller and display some sample data when the page loads
 /* public ContactSearchController() {
    soql = 'select firstname, lastname, account.name from contact where account.name != null';
    runQuery();
  }*/
  
   public ContactSearchController(ApexPages.StandardController stdController) {
         this.con = (Contact)stdController.getRecord();
         
       
       enabled = true;
       soql = 'select id, firstname, lastname, Email, account.name from contact where account.name != null';
        runQuery();
        
      
    }
 
  // toggles the sorting of query from asc<-->desc
  public void toggleSort() {
    // simply toggle the direction
    sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
    // run the query again
    runQuery();
  }
 
  // runs the actual query
  public void runQuery() {
 
    try {
      contacts = Database.query(soql + ' order by ' + sortField + ' ' + sortDir + ' limit 20');
    } catch (Exception e) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'An error has occured. Please try again.'));
    }
 
  }
  
  public PageReference createNew()
    {
    if (ApexPages.currentPage().getParameters().get('RecordType') == null) {
    Schema.DescribeSObjectResult describeOpp = Schema.SObjectType.Contact ;    
    List<Schema.RecordTypeInfo> rtInfos = describeOpp.getRecordTypeInfos();
    for(Schema.RecordTypeInfo rtInfo : rtInfos) {
        if(rtInfo.isDefaultRecordTypeMapping()) {               
            ApexPages.currentPage().getParameters().put('RecordType', rtInfo.getRecordTypeId());
            String recordRT = ApexPages.currentPage().getParameters().get('RecordType');
        string url='/003/e?retURL=%2F003%2Fo&nooverride=1&name_lastcon2='+ strLast + '&name_firstcon2=' + strFirst +'&RecordType='+recordRT ;
        PageReference pageRef = new PageReference(url);
        return pageRef;
            
        }
    }
    }
   else{
Id rcdid=ApexPages.currentPage().getParameters().get('RecordType');

       Id i = Id.valueOf(rcdid);
        string url='/003/e?retURL=%2F003%2Fo&nooverride=1&name_lastcon2='+ strLast + '&name_firstcon2=' + strFirst +'&RecordType='+i;
        PageReference pageRef = new PageReference(url);
        return pageRef;
        }
        return null;
    }
 
  // runs the search with parameters passed via Javascript
  public PageReference runSearch() {
 
    String firstName = Apexpages.currentPage().getParameters().get('firstname');
    String lastName = Apexpages.currentPage().getParameters().get('lastname');
    String accountName = Apexpages.currentPage().getParameters().get('accountName');
      
      strLast = lastName;
      strFirst = firstName;
 
    soql = 'select id, firstname, lastname, email, account.name from contact where account.name != null';
    if (!firstName.equals(''))
      soql += ' and firstname LIKE \''+String.escapeSingleQuotes(firstName)+'%\'';
    if (!lastName.equals(''))
      soql += ' and lastname LIKE \''+String.escapeSingleQuotes(lastName)+'%\'';
    if (!accountName.equals(''))
      soql += ' and account.name LIKE \''+String.escapeSingleQuotes(accountName)+'%\'';  
   
    // run the query again
    runQuery();
      
      enabled = false;
 
    return null;
  }


}