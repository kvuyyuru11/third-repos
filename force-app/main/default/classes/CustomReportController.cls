/*
Requirement #: 16971
Application Inventory: AI-0748
*/
public class CustomReportController{
    public List<FC_Reviewer__External_Review__c> revlst{get;set;}
    public CustomReportController(ApexPages.StandardController c){
        revlst = [SELECT Id, Panel__c, FC_Reviewer__Opportunity__r.Program_R2__c, FC_Reviewer__Opportunity__r.Application_Number__c,
        FC_Reviewer__Opportunity__c, FC_Reviewer__Opportunity__r.AccountId, FC_Reviewer__Opportunity__r.Name, FC_Reviewer__Opportunity__r.App_Resubmission__c,
        FC_Reviewer__Opportunity__r.PI_Name_User__c, FC_Reviewer__Opportunity__r.City__c, FC_Reviewer__Opportunity__r.Average_Online_Review_Score__c,
        FC_Reviewer__Opportunity__r.State_Province__c, FC_Reviewer__Opportunity__r.Average_In_Person_Score__c, FC_Reviewer__Opportunity__r.Country__c,
        FC_Reviewer__Opportunity__r.Previous_Application__c, FC_Reviewer__Opportunity__r.Total_Direct_Costs__c, Overall_Score__c, Criterion_1_Score__c,
        Criterion_2_Score__c, Criterion_3_Score__c, Criterion_4_Score__c, Criterion_5_Score_R2__c
        FROM FC_Reviewer__External_Review__c WHERE FC_Reviewer__Status__c = 'Review Complete'];
    }
}