/*
* Class : ReviewmanualSharingtriggeremailhandler
* Author : Kalyan Vuyyuru (PCORI)
* Version History : 1.0
* Creation : 4/13/2017
* Last Modified By: Kalyan Vuyyuru (PCORI)
* Last Modified Date: 04/14/2017
* Modification Description: Finished the complete functionality of the class that needs to perform
* Description : This class is been called from ReviewManualSharingTrigger whenver the reviewer self identifies and indicates the COI either in In person-preview or In person-review phase. This class then send email to the MRO on the panel about the same.
*/

public class ReviewmanualSharingtriggeremailhandler {
    
    public static Messaging.SingleEmailMessage sendEmail(string ReviewerName, id targetid, string OpportunityName, string cycle, string PanelName){
        
        
        //selecting the template that we want to send in this case where reviewer indicates COI in review phase.
        EmailTemplate et=[Select id,body,subject from EmailTemplate where DeveloperName=:Label.Review_COI_Indicated];
        String sjt=et.Subject;
        String plainBody = et.Body;
        plainBody = plainBody.replace('{!FC_Reviewer__External_Review__c.Contact_Copied_due_to_COI__c}', ReviewerName);
        plainBody = plainBody.replace('{!FC_Reviewer__External_Review__c.FC_Reviewer__Opportunity__c}', OpportunityName);
        plainBody=  plainBody.replace('{!FC_Reviewer__External_Review__c.Cycle__c}', Cycle);
        plainBody=  plainBody.replace('{!FC_Reviewer__External_Review__c.Panel__c}', PanelName);
        OrgWideEmailAddress owea = [select id from OrgWideEmailAddress where Address =:Label.MRM_Org_wide_address];
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTargetObjectId(targetid);
        mail.setOrgWideEmailAddressId(owea.Id);
        mail.setUseSignature(false);
        mail.setBccSender(false); 
        mail.setSaveAsActivity(false);
        mail.setPlainTextBody(plainBody);
        mail.setsubject(sjt);
        //if(!Test.isRunningTest()){
        // Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});   
        // system.debug(r);
        //}
        return mail;
        
    }
    
    public static Messaging.SingleEmailMessage sendEmailtoRRCSReviews(attachment att, string ProjectName,string Name, id targetid, string link,  string username){
        //User u=  [select Name from User where id=:targetid];
        // string username=u.Name;
        string attachmentlink=Label.RRCS_PDF_attachment_link+att.Id;
        string reviewlink=Label.RRCS_Review_link+link;
        EmailTemplate et=[Select id,body,subject from EmailTemplate where DeveloperName=:Label.RRCS_Email_Template];
        String sjt=et.Subject;
        String plainBody = et.Body;
        plainBody = plainBody.replace('PrjtTle', ProjectName);
        plainBody = plainBody.replace('Review Number', Name);
        plainBody=  plainBody.replace('ApplicationLink', attachmentlink);
        plainBody=  plainBody.replace('InternalReviewer', username);
        plainBody=  plainBody.replace('Reviewlink', reviewlink);
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTargetObjectId(targetid);
        OrgWideEmailAddress owea = [select id from OrgWideEmailAddress where Address =:Label.RRCS_Org_Wide_Address];
        mail.setOrgWideEmailAddressId(owea.Id);
        mail.setUseSignature(false);
        mail.setBccSender(false); 
        mail.setSaveAsActivity(false);
        mail.setPlainTextBody(plainBody);
        mail.setsubject(sjt);
        /*if(!Test.isRunningTest()){
Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});   
system.debug(r);
}*/
        return mail;
        
    }
    
    @future
    public static void assignpermissionset(set<ID> usersetID){
        PermissionSet Pmst=[SELECT Id FROM PermissionSet where Label=:Label.RRCS_Permission_Set limit 1];
        Id pmstId=Pmst.Id;
        List<PermissionSetAssignment> permissionSetList = new List<PermissionSetAssignment>();
        set<ID> UserIdset = new set<ID>();
        List<PermissionSetAssignment> permissionSetListtocheck = new List<PermissionSetAssignment>();
        permissionSetListtocheck=[select AssigneeId,PermissionSetId from PermissionSetAssignment where AssigneeId IN : usersetID AND PermissionSetId=:pmstId];
        if(!permissionSetListtocheck.isEmpty() && permissionSetListtocheck.size()>0){
            for(PermissionSetAssignment psetas:permissionSetListtocheck){
                UserIdset.add(psetas.AssigneeId);
            }}
        for(Id uid:usersetID)  {
            if(!UserIdset.contains(uid)){
                PermissionSetAssignment psa = new PermissionSetAssignment (PermissionSetId = pmstId, AssigneeId = uid);
                permissionSetList.add(psa);
            }
        }
        
        database.insert(permissionSetList);
    }
    
    @future
    public static void deletepermissionset(set<Id> useridlisttodelete){
        PermissionSet Pmst=[SELECT Id FROM PermissionSet where Label=:Label.RRCS_Permission_Set limit 1];
        Id pmstId=Pmst.Id;
        Map<Id,Id> userpermsetmap= new Map<ID,Id>();
        Map<Id,integer> rvwcountmap= new Map<ID,integer>();
        List<Id> permissionSetListtodelete = new List<Id>();
        List<AggregateResult> rvlist= [select Internal_Reviewer__c Usr,COUNT(Id) rvwcount from FC_Reviewer__External_Review__c where Internal_Reviewer__c IN :useridlisttodelete GROUP BY Internal_Reviewer__c];
        for (AggregateResult ar : rvlist)  {
            rvwcountmap.put((Id)ar.get('Usr'),(Integer)ar.get('rvwcount')); 
        }
        List<PermissionSetAssignment> lstPsa = [select id,AssigneeId from PermissionsetAssignment where PermissionSetId =: pmstId AND AssigneeId IN :useridlisttodelete];
        for(PermissionSetAssignment psa :lstPsa){
            userpermsetmap.put(psa.AssigneeId,psa.Id);
        }
        
        for(Id did:useridlisttodelete)
        {
            if(rvwcountmap.get(did)==null){
                permissionSetListtodelete.add(userpermsetmap.get(did));
            }
        }
        if(!permissionSetListtodelete.isEmpty() && permissionSetListtodelete.size()>0)
        database.delete(permissionSetListtodelete);
    }
}