global class ShareApplicationsforInPersonPreview {
    
    webService static void callsharingreviewsclass(String pId){
        
 Id rvwRecordTypeId1 = Schema.SObjectType.FC_Reviewer__External_Review__c.getRecordTypeInfosByName().get(Label.InPersonPreview).getRecordTypeId();
        list<FC_Reviewer__External_Review__c> rlist=[select Id,FC_Reviewer__Contact__c,FC_Reviewer__Opportunity__c,FC_Reviewer__Lead__c from FC_Reviewer__External_Review__c where Panel__c = :pId  AND RecordTypeId = :rvwRecordTypeId1];  
        if(!rlist.isempty() && rlist.size()>0){
        ShareApplicationsforInPersonPreview.MethodonInsertofReviews(rlist);
            }
    }
        public static void MethodonInsertofReviews(list<FC_Reviewer__External_Review__c> reviewsList){
       // list<FC_Reviewer__External_Review__c> reviewsList=[select Id,FC_Reviewer__Contact__c,FC_Reviewer__Opportunity__c,FC_Reviewer__Lead__c from FC_Reviewer__External_Review__c where ID IN:reviewsListId];  
        set<Id> cntlist = new set<Id>();
        set<Id> oplist= new set<Id>();
        set<Id> ldlist= new set<Id>();
        set<Id> rvlist= new set<Id>();
        list<User> usrlist= new list<User>();
        //First ID Key is contactId and second Id Value is UserId
        Map<ID,ID> revscntandusermap=new Map<Id,Id>();
        Map<ID,set<ID>> opptandusermap=new Map<Id,set<Id>>();
        Map<ID,set<ID>> leadandusermap=new Map<Id,set<Id>>();
        List<FC_Reviewer__External_Review__share> reviewShares= new List<FC_Reviewer__External_Review__share>();
        set<ID> reviewSharesIds= new set<ID>();
        List<FC_Reviewer__External_Review__share> reviewShareslisttoInsert= new List<FC_Reviewer__External_Review__share>();
        List<OpportunityTeamMember> optShares= new List<OpportunityTeamMember>();
        List<OpportunityShare> oppsharelist=new list<OpportunityShare>();
        List<OpportunityTeamMember> optShareslisttoInsert= new List<OpportunityTeamMember>();
        List<Leadshare> leadShares= new List<Leadshare>();
        List<Leadshare> leadShareslisttoInsert= new List<Leadshare>();
        //Iterating the list and collecting all contacts, opportunities,reviews and leads Id's.
        for(FC_Reviewer__External_Review__c er:reviewsList){
            if(er.FC_Reviewer__Contact__c!=null){
                cntlist.add(er.FC_Reviewer__Contact__c);
            }
            if(er.FC_Reviewer__Opportunity__c!=null){
                oplist.add(er.FC_Reviewer__Opportunity__c);
                system.debug('***'+oplist);
            }
            

        }
        //from the list collected now checking for existing shares on reviews,opportunities and leads and also getting user related data from contacts on reviews   
        usrlist=[select id,contactId from User where contactid IN : cntlist];
        // reviewShares = [SELECT id, ParentId, UserOrGroupId From FC_Reviewer__External_Review__share where ParentId IN :rvlist];
        optShares=[SELECT id, OpportunityId , UserId FROM OpportunityTeamMember where OpportunityId IN : oplist];
         oppsharelist=[SELECT id, OpportunityId , userorgroupid FROM opportunityshare where OpportunityId IN : oplist];
        system.debug('***'+optShares);
        if(!usrlist.isEmpty() && usrlist.size()>0){
            for(User u:usrlist){
                revscntandusermap.put(u.contactId,u.Id); 
            }
        }
 
        
        for(OpportunityTeamMember os:optShares){
            if(!opptandusermap.containsKey(os.OpportunityId)) {
                set<Id> temp = new set<Id>();
                temp.add(os.UserId);
                opptandusermap.put(os.OpportunityId,temp);
            }
            else
            {
                set<Id> temp = opptandusermap.get(os.OpportunityId);
                temp.add(os.UserId);
                opptandusermap.put(os.OpportunityId,temp);
            }
            
        }
        system.debug('***'+opptandusermap);
        
        
        for(FC_Reviewer__External_Review__c er1:reviewsList){
            //checking if the contact on the reviews definitely has a user and also ensuring that user is not already having share to the review record and only then adding him in the list to insert review shares.
           
            //checking if the contact on the reviews definitely has a user and also ensuring that user is not already having share to the opportunity record and only then adding him in the list to insert opportunity shares.  
            if(opptandusermap.isEmpty()){
            if(revscntandusermap.get(er1.FC_Reviewer__Contact__c)!=null && er1.FC_Reviewer__Opportunity__c!=null){
                OpportunityTeamMember oppSharetoInsert = new OpportunityTeamMember();
                oppSharetoInsert.OpportunityAccessLevel='Read';
                oppSharetoInsert.OpportunityId=er1.FC_Reviewer__Opportunity__c;
                oppSharetoInsert.UserId=revscntandusermap.get(er1.FC_Reviewer__Contact__c);
                optShareslisttoInsert.add(oppSharetoInsert);
            }
}
            if(!opptandusermap.isEmpty()){
            if(revscntandusermap.get(er1.FC_Reviewer__Contact__c)!=null && er1.FC_Reviewer__Opportunity__c!=null && opptandusermap.get(er1.FC_Reviewer__Opportunity__c)!=null && !opptandusermap.get(er1.FC_Reviewer__Opportunity__c).contains(revscntandusermap.get(er1.FC_Reviewer__Contact__c))){
                OpportunityTeamMember oppSharetoInsert = new OpportunityTeamMember();
                oppSharetoInsert.OpportunityAccessLevel='Read';
                oppSharetoInsert.OpportunityId=er1.FC_Reviewer__Opportunity__c;
                oppSharetoInsert.UserId=revscntandusermap.get(er1.FC_Reviewer__Contact__c);
                optShareslisttoInsert.add(oppSharetoInsert);
            }
            }
            system.debug('***'+optShareslisttoInsert);
      
            
        }
 
        if(!optShareslisttoInsert.isEmpty() && optShareslisttoInsert.size()>0){
            database.insert(optShareslisttoInsert);
        }
        
        
    }
    

}