/****************************************************************************************************
*Description:           This is the controller for the ProjectPersonnel Related list
*                       

*Revision   |Date       |Author             |AdditionalNotes
*====================================================================================================
*   1.0     08/02/2018   Sowmya Goli        Initial Version (Jira Ticket SPS-3689).

*****************************************************************************************************/

public class PPlistController {
    
    
   public Id recid{get;set;}
 

   public list<Key_Personnel__c> opps1{get;set;}
    public PPlistController(ApexPages.StandardController controller) {
        //Gets the particular record id
        
         recid=apexpages.currentpage().getparameters().get('id');
        //getProjects(recdid);  
          
         opps1= [Select Id,Name, RecordType.DeveloperName,Opportunity__c,First_Name__c,Last_Name__c,Status__c,Role__c,Role_On_Project__c,Telephone__c,Email__c,Key_Personnel_Flag__c,Institution_Org__c from Key_Personnel__c where Opportunity__c=: recid];
                        
     }
    }