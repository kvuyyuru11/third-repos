@isTest
public class EmailUpdatesPIRTest {
    private static testMethod void triggerTest(){
      
        RecordType recOpp = [SELECT Id FROM RecordType WHERE sObjectType = 'Opportunity' AND Name = 'RA Applications'];
        RecordType recAcc = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Standard Salesforce Account'];
        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];  
      
        Account acc = new Account();
        acc.RecordTypeId = recAcc.Id;
        acc.Name = 'Test Acc';
        insert acc;
         
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = recOpp.Id;
        opp.Awardee_Institution_Organization__c = acc.Id;
        opp.Full_Project_Title__c = 'test0';
        opp.Project_Name_off_layout__c='test0';
        opp.RecordTypeId = recOpp.Id;
        opp.AO_Name_User__c = usr.Id;
        opp.PI_Name_User__c = usr.Id;
        opp.PI_Project_Lead_Designee_1_New__c = usr.Id;
        opp.Name = 'Test';
        opp.Discussion_Order_Rank__c = 12.0;
        opp.Average_In_Person_Score__c = 1.0;
        opp.Percentile__c = 20.0;
        opp.Quartile__c = 2.0;
        opp.CloseDate = Date.newInstance(2016, 12, 31);
        opp.Application_Number__c = '45125';
        opp.StageName = 'In Progress';
        opp.AO_Name_User__c = '00570000002dlMG';
            opp.PI_Name_User__c = '00570000002bg4e';
        insert opp;
        
        PIR__c pir = new PIR__c();
        pir.Administrative_Official_Email_v2__c = 'test@accentetes.com.org';
        pir.Application__c = opp.Id;
        pir.PIR_Response_Deadline__c = DateTime.newInstance(Date.newInstance(2020, 12, 29), Time.newInstance(12, 0, 0, 0));
        insert pir;
    }
}