@isTest
public class UpdatecononMRETriggerHandlerClasstest {
    
    public static Contact con(){
        Contact con = new Contact(LastName = 'test20161', Reviewer_Status__c='New Reviewer');
        insert con;
        return con;
    }
    public static Panel__c panel(){
        Cycle__c cy = new Cycle__c(name='test1', COI_Due_Date__c = Date.newInstance(2016,12,31));
        insert cy;
        Panel__c pan= new Panel__c(Name='testPanel1', Cycle__c = cy.Id,Panel_Due_Dtae__c=  Date.newInstance(2019,12,20));
        insert pan;
        return pan;
    }
    
    public static Merit_Reviewer_Evaluation__c eval(){
        Contact c= con(); 
        Panel__c p = panel();
        Id recordTypeId = Schema.SObjectType.Merit_Reviewer_Evaluation__c.getRecordTypeInfosByName().get('Reviewer Evaluation').getRecordTypeId();
        Merit_Reviewer_Evaluation__c merEval= new Merit_Reviewer_Evaluation__c(Reviewer_Name__c=c.id,Panel__c=p.id,Timeliness_of_communication_with_MRO__c='Met Expectations',Timeliness_in_completing_review_tasks__c='Met Expectations',Quality_of_reviewer_critiques__c='Met Expectations',Demeanor_conduct_and_tone_toward_MRO__c='Met Expectations',Quality_of_summaries_for_assigned_apps__c='Met Expectations',Discussion_of_apps_presented_by_others__c='Met Expectations',Understanding_of_criteria_and_goals__c='Met Expectations',Panelist_complete_merit_review_cycle__c='Yes, completed merit review and partipated in-person',Recommend_the_reviewer_be_invited_back__c='Yes',Recommend_to_serve_as_a_chair__c='Yes',Recommend_to_serve_as_a_mentor__c='Yes',Notes__c='testNotes',RecordTypeId=recordTypeId);
        insert merEval;
        return merEval;
    }
    
    public static testMethod void updateTest(){
        Test.startTest();
        Profile p1 = [SELECT Id FROM Profile WHERE Name='System Administrator - Accenture']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', LastName='test', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p1.Id, TimeZoneSidKey='America/Los_Angeles', UserName='user@myjob.com');
        Contact c1= con();
        insert u;
        Merit_Reviewer_Evaluation__c m= eval();
        System.RunAs(u) {
            
            m.Panelist_complete_merit_review_cycle__c='Yes, but participated by phone during merit review';
            update m;
            c1.Reviewer_Status__c='PCORI Reviewer';
            update c1;            
        }       
        Test.stopTest();   
    }
    
}