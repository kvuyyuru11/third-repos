@isTest
public class LOICampaignMemberProcess_Test
{
    Static testMethod void LOICampaignMemberProcess_TestMethod1()
        
    {
        Account Acc = new Account(name='Test Account',Community__c='TBD');
        insert Acc;
        
        FGM_Base__Program__c p = new FGM_Base__Program__c();
        p.Name='Addressing Disparities';
        p.Owning_Program__c = 'Addressing Disparities';
        insert p;
        
        FGM_Base__Program__c p2 = new FGM_Base__Program__c();
        p2.Name='Addressing Disparities';
        p2.Owning_Program__c = 'Infrastructure';
        insert p2;
        
        Campaign cmp = new Campaign();
        cmp.name = 'Test Campaign';
        cmp.Program__c = p.Id;
        insert cmp;
        
        Campaign cmp2 = new Campaign();
        cmp2.name = 'Test Campaign2';
        cmp.Program__c = p2.Id;
        insert cmp2;
        lead newLead = new lead(PFA__c=cmp.Id,Project_Name__c='Cole Swain',Company='test',Zendesk__organization__c=Acc.Id,
                                firstName = 'Cole', lastName = 'Swain', status = 'Submitted', Administrative_Official__c='00570000002dmiQ',
                                Principal_Investigator__c='00539000004jcWb',CMA_Owner__c ='00570000002dmiQ',CMA_Initial_Screen_Feedback__c='Test',
                                CMA_Initial_Screen_Complete__c='Compliant',Program_Owner__c='00570000002dmiQ',Program_Initial_Screen_Feedback__c='test',Program_Initial_Screen_Complete__c='test',
                                MRO_Owner__c='00570000002dmiQ',MRO_Initial_Screen_Feedback__c='test',MRO_Initial_Screen_Complete__c='Responsive');     
        
        insert newLead;
        CampaignMember cmpm = new CampaignMember();
        cmpm.campaignId = cmp.id;
        cmpm.LeadId = newLead.Id;
        cmpm.Status = 'Sent';
        insert cmpm;
        newLead.PFA__c = cmp2.Id;
        update newLead;
        test.startTest();
        database.leadConvert lc = new database.leadConvert();
        lc.setLeadId(newLead.id);
        lc.setDoNotCreateOpportunity(true);
        lc.setConvertedStatus('Approved');
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        newLead.Dual_PI_Name__c='abc';
        update newLead;
        test.stopTest();
    }
    
}