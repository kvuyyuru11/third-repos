@istest
public class AP3coverage {
    public static testmethod void TestMethod1(){        
        list<Account> acnt = TestDataFactory.getAccountTestRecords(1);
        list<contact> con  = TestDataFactory.getContactTestRecords(1,1);
        list<User> usrs    = TestDataFactory.getpartnerUserTestRecords(1,1);   
        Advisory_Panel_Application__c a = new Advisory_Panel_Application__c();
        a.Personal_Statement2__c='test';
        a.Bio__c='Test bio';
        a.Permission_to_post_bios_on_website__c='Yes';
        a.Please_tell_us_anything_else__c='anything';
        a.Additional_Supportive_Information__c='QWERTY';
        a.Application_Status_new__c='Draft';
        a.Application_Status__c='Draft';
        a.Contact__c=con[0].id;    
        insert a; 
        Test.startTest();
        System.RunAs(usrs[0]) 
        {            
            AP_Application3Controller testController  = new AP_Application3Controller();
            testController.apa = a;
            testcontroller.saveMethod();    
            testcontroller.Nextmethod();
            testcontroller.Previousmethod();
            testcontroller.getBioPermissions();
            testcontroller.personalStatement = 'Test';
            testcontroller.supportingDocuments1 = 'Test';
            testcontroller.supportingDocuments2 = 'Test';
            testcontroller.supportingDocuments3 = 'Test';
            testcontroller.supportingDocuments4 = 'Test';
            testcontroller.supportingDocuments5 = 'Test';            
        }  
        Test.stopTest();
    }
}