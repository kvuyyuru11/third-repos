global without sharing class ActionGridDeleteForNegotiation {

	webService static List<FGM_Base__Benchmark__c> deleteMilestoneIds(List<String> ids) {

		List<FGM_Base__Benchmark__c> milestones = new List<FGM_Base__Benchmark__c>();

		if(ids != null) {
			for(String milestoneId : ids){
				FGM_Base__Benchmark__c milestone = new FGM_Base__Benchmark__c();
				milestone.Id = milestoneId;
				milestones.add(milestone);
			}
		}

		if(!milestones.isEmpty()) {
			try{
				delete milestones;
			}
			catch (Exception e){
				//Possibly the provided ids were already deleted;
			}
		}
		return milestones;
    }

}