@isTest
public class MilestoneRedirectControllerTest {
    private static testMethod void milestoneTest(){
        RecordType recOpp = [SELECT Id FROM RecordType WHERE DeveloperName = 'Research_Awards' AND sObjectType = 'Opportunity'];
        RecordType recAcc = [SELECT Id FROM RecordType WHERE DeveloperName = 'Standard_Salesforce_Account' AND sObjectType = 'Account'];
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.RecordTypeId = recAcc.Id;
        insert acc;
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = recOpp.Id;
        opp.AccountId = acc.Id;
        opp.Contract_Number__c = '03423';
        opp.Name = 'Test';
        opp.StageName = 'In Progress';
        opp.Application_Number__c = '12213';
        opp.Full_Project_Title__c = 'Project Test';
        opp.CloseDate = Date.newInstance(2016, 12, 22);
        insert opp;
        FGM_Base__Benchmark__c md = new FGM_Base__Benchmark__c();
        md.Milestone_ID2__c = 'Test';
        md.Milestone_Name__c = 'Draft';
        md.Milestone_Status__c = 'In Progress';
        md.FGM_Base__Request__c = opp.Id;
        md.FGM_Base__Due_Date__c = System.Today()+10;
        insert md;
        
        MilestoneRedirectController m = new MilestoneRedirectController(new ApexPages.StandardController(md));
        m.redirect();
    }
    
    private static testMethod void milestoneTest2(){
        list<User> usr = TestDataFactory.getpartnerUserTestRecords(1,1);
        RecordType recOpp = [SELECT Id FROM RecordType WHERE DeveloperName = 'Research_Awards' AND sObjectType = 'Opportunity'];
        RecordType recAcc = [SELECT Id FROM RecordType WHERE DeveloperName = 'Standard_Salesforce_Account' AND sObjectType = 'Account'];
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.RecordTypeId = recAcc.Id;
        insert acc;
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = recOpp.Id;
        opp.AccountId = acc.Id;
        opp.Contract_Number__c = '03423';
        opp.Name = 'Test';
        opp.StageName = 'In Progress';
        opp.Application_Number__c = '12213';
        opp.Full_Project_Title__c = 'Project Test';
        opp.CloseDate = Date.newInstance(2016, 12, 22);
        insert opp;
        FGM_Base__Benchmark__c md = new FGM_Base__Benchmark__c();
        md.Milestone_ID2__c = 'Test';
        md.Milestone_Name__c = 'Draft';
        md.Milestone_Status__c = 'In Progress';
        md.FGM_Base__Request__c = opp.Id;
        md.FGM_Base__Due_Date__c = System.Today()+10;
        insert md;
        System.runAs(usr[0]){
            MilestoneRedirectController m = new MilestoneRedirectController(new ApexPages.StandardController(md));
            m.redirect(); 
        }        
    }
}