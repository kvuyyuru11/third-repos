/*------------------------------------------------------------------------------------------------------------------------------------
Code Written by SADC PCORI Team: Matt Hoffman
Written During Development Phase 2: June/2016 - September/2016
Application ID: AI-0615
Requirement ID: RAr2_C1_007

This class is used by the triggers LeadManualShareWithKP and LeadManualShareWithKP_Update. It has a function that returns a new LeadShare record by
passing in the LeadID and the user/group ID. It will and should always be a user in this case. 
If a group is added, it will cause problems when trying to give permission to the Opportunity on Lead conversion.
-------------------------------------------------------------------------------------------------------------------------------------*/
public without sharing class JobSharing {
   
    public static boolean run = true;
    public static boolean runOnce()
    {
        if(run)
        {
            run=false;
            return true;
        }
        else
        {
            return run;
        }
    }
    /*
    public static void setRunFalse()
    {
        run = false;
    }
    */
    
    public static LeadShare manualShareRead(Id recordId, Id userOrGroupId){
        // Create new sharing object for the custom object Job.
        LeadShare jobShr = new LeadShare();
        
        // Set the ID of record being shared.
        jobShr.LeadId = recordId;
        
        // Set the ID of user or group being granted access.
        jobShr.UserOrGroupId = userOrGroupId;
        
        // Set the access level.
        jobShr.LeadAccessLevel = 'Edit';
        
        // Set rowCause to 'manual' for manual sharing.
        // This line can be omitted as 'manual' is the default value for sharing objects.
        jobShr.RowCause = Schema.LeadShare.RowCause.Manual;
        
        return jobShr;
    }       
    
    
    public static integer findNextNumberInList(List<Fluxx_LOI_Review_Form__c> reviewsList)
    {
        integer nextExpectedNumber = 1;
        integer numberIterationsNeeded = reviewsList.size();
        //System.debug('list size: ' + numberIterationsNeeded);
        for (integer i=0; i<numberIterationsNeeded; i++)
        {
            boolean numberFound = false;
            for (Fluxx_LOI_Review_Form__c theReview : reviewsList)
            {
                //System.debug('reviewNum: ' + theReview.Review_Number__c);
                //System.debug('expected : ' + nextExpectedNumber);
                if ((Integer)theReview.Review_Number__c == nextExpectedNumber)
                {
                    //we found the expected number. can go to next number
                    numberFound = true;
                }
            }
            
            if (!numberFound)
            {
                // if we didn't find the expected number, then we can return this number as the next one needed
                //System.debug('didnt find expected number. returning early ');
                return nextExpectedNumber;
            }
            
            nextExpectedNumber++;
        }                
        return (numberIterationsNeeded + 1);
    }
    
    /*
    public void fillUserActivityHistory()
    {
        List<Task> newTaskList = new List<Task>();
        
        List<Profile> profList = [SELECT id FROM Profile WHERE Name =: 'PCORI Community Partner'];
        Id profileID = profList[0].id;        
        
        List<user> userList = [SELECT contactid FROM User WHERE ProfileID =: profileID AND IsActive =: true];
        List<Task> historyList = [select id, whoId, Description, Status, priority, start_date__c FROM Task];
        system.debug('history list size:' + historyList.size());
        Map<Id, Task> historyMap = new Map<Id, Task>();
        for(Task t : historyList)
        {
            historyMap.put(t.whoId, t);        
        }
             
        for (User u : userList)
        {
            //if (u.contactId == '0054C000000DlQhQAK')
            //{
            //    system.debug('found adam');
           //}
            
            if (historyMap.containsKey(u.contactId))
            {
                Task currentTask = historyMap.get(u.contactId);
                
                if (String.isBlank(currentTask.Description))
                {
                    system.debug('found an empty description');
                    //Task t = currentTask.clone(true, true, true, true);
                    currentTask.Description = 'defaultDescription';
                    
                    newTaskList.add(currentTask);
                }
            }
            
            else
            {
                //system.debug('found user with no task');
                Task t = new Task();
                t.Description = 'defaultDescriptionNew';
                t.WhoId = u.contactId;
                t.start_date__c = Date.today();
                t.Priority = 'Low';
                t.Status = 'In Progress';
                
                //newTaskList.add(t);           
            }
        }
        system.debug('upsert size: ' + newTaskList.size());
        //upsert newTaskList;
    }
    */
}