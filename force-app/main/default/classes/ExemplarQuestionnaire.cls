public class ExemplarQuestionnaire {
    public Science_Qualifications_Results__c objExemplar{get;set;}
    public opportunity objopportunity {get;set;}
  
   @testvisible private id projid{get;set;}
    
    public ExemplarQuestionnaire(ApexPages.StandardController controller){
        
        objExemplar=new Science_Qualifications_Results__c();
       
        objopportunity=new opportunity();
        objExemplar.Is_Study_Important__c=false;
       //objExemplar.Is_it_Impactful__c=false;
         string sReturl=ApexPages.currentPage().getParameters().get('Pid');
         string sReturl1=ApexPages.currentPage().getParameters().get('retURL');
        
        if(string.isBlank( sReturl) && string.isNotBlank(sReturl1)){
       
                 
            sReturl=sReturl1.remove('/');
            
        }
       string  sExempid= ApexPages.currentPage().getParameters().get('id');
        
        system.debug('id=='+sExempid);
         system.debug('projid=='+projid);
       
            
             if(string.isNotBlank(sExempid)){
                objExemplar= [select Contract_Id__c,Contract_Id__r.id,Reviewer2__c,Study_gone_through_Peer_Review__c,Enter_FRR_Date__c,
                                     CER__c,If_No_important__c,Is_Study_Important__c,Reviewer2__r.id,Reviewer1__c,
                                      Reduce_Patient_harms__c,Adverse_Effects__c,Out_of_pocket_costs__c,
                                     Ineffectiveness_Treatment_use__c,Increase_Patient_Benifits__c,Outcomes__c,Choice__c,
                                     Acceptability_convenience__c,Individualized_Treatment__c,Caregiver_and_Family_Burden__c,
                                     Improve_health_System_Efficiency__c,Access__c,Unnecessary_practice_variation__c,
                                     Delivery_models__c,Personnel__c,Improve_Disparities__c,Improve_generality__c,
                                     Is_it_Impactful__c,Innovation_Discovery__c,Fills_a_critical_gap__c,Widespread_Utility__c,Addresses_important_choice__c,
                                     Is_there_evidence_of_impact__c,Describe__c,Important_caveats_for_study__c,Status__c
                              
                             from  Science_Qualifications_Results__c  where id=:sExempid];
                 projid=objExemplar.Contract_Id__r.id;
                 sReturl=objExemplar.Contract_Id__r.id;
            }
         if(string.isNotBlank( sReturl)){
             projid=sReturl;
            if(string.isNotBlank(projid)){
                         objopportunity=[select id,name,Contract_Number__c,StageName,PI_Name_User__c,PI_Name_User__r.Name,Project_End_Date__c,
                                         Full_Project_Title__c,Program_Associate__c,Program_Officer__c,Program_Officer__r.Name 
                                         from Opportunity where id=:projid];
            if(objopportunity!=null){
                if(objExemplar.Contract_Id__r.id==null){
                    objExemplar.Contract_Id__C=objopportunity.id;}
                if(objExemplar.Reviewer1__c==null){
                    objExemplar.Reviewer1__c=objopportunity.Program_Officer__c;
                }
               
                }
            }
        }
          
        
        
    }
     /* public PageReference IsStudy()
    {

        if( objExemplar.Is_Study_Important__c==true){
              objExemplar.Is_Study_Important__c=false;
        }else{
              objExemplar.Is_Study_Important__c=true;
        } 
    return null;
    }
  public PageReference IsYesImpact()
    {
         /*if( objExemplar.Is_it_Impactful__c==true){
              objExemplar.Is_it_Impactful__c=false;
        }else{
              objExemplar.Is_it_Impactful__c=true;
        } 
    return null;
    }*/
    public pageReference Save(){
  
        upsert objExemplar;
         PageReference pr=new PageReference('/'+projid);
     pr.setRedirect(true);
   return pr; 
    }
    public pageReference cancel(){
        
         PageReference pr=new PageReference('/'+projid);
     pr.setRedirect(true);
   return pr; 
    }
    public pagereference ExempEditRedirect(){
        
        return null;
    }
}