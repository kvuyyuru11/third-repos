@isTest
public with sharing class ChatterFreePrivateGroupInviteesTest {
    static testMethod void validateChatterFreeLicense() {
        User usr = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND isActive = true LIMIT 1];
        System.runAs(usr){
//            Profile p = [SELECT Id, UserLicense.Name FROM Profile WHERE Name = 'Chatter Free User'];
            Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'Chatter Free User'].get(0);
            User newU = new User (  LastName = 'TUser11',
                                    FirstName = 'User66',
                                    Email = 'utuser55@testincc.com',
                                    Alias = 'TUser6',
                                    Username = 'utuser66@guest.pcori.org',
                                    CommunityNickname = 'utuser5@testinc.com',
                                    IsActive = true,
                                    TimeZoneSidKey= 'America/New_York',
                                    LocaleSidKey = 'en_US',
                                    EmailEncodingKey = 'ISO-8859-1',
                                    ProfileId = p.Id,
                                    LanguageLocaleKey = 'en_US'
                                );
            insert newU;        
            }
        
    }
}