/**
	* AccountSearchControllerTest - <description>
	* Created by BrainEngine Cloud Studio
	* @author: brian Matthews
	* @version: 1.0
*/

@isTest
private class AccountSearchControllerTest {
	static testMethod void myUnitTest() {
		
		Account a = new Account();
		a.Name = 'Acme';
		Database.insert(a);
		

	 	PageReference pageRef = Page.AccountSearch;
	  	Test.setCurrentPageReference(pageRef);
	  	// create an instance of the controller
		ApexPages.StandardController sc = new ApexPages.standardController(a);
	  	AccountSearchController myPageCon = new AccountSearchController(sc);

		ApexPages.CurrentPage().getparameters().put('accountName', 'Acme');
		myPageCon.searchText = 'ACME';
		myPageCon.enabled = true;
	  	myPageCon.runSearch();
		
		myPageCon.createNew();
		myPageCon.createNewA();
		myPageCon.runQuery();
		myPageCon.toggleSort();
		
		string sortDir = myPageCon.sortDir;
		string sortField = myPageCon.sortField;
		string debugSoql = myPageCon.debugSoql;
	  
	}
}