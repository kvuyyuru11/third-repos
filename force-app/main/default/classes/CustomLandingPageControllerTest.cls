@isTest
public class CustomLandingPageControllerTest  {
    static testMethod void testmethod1() {
        list<contact> cntlist= new list<contact>();
          list<Id> cntlistId= new list<Id>();
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt;
        acnt.IsPartner=true;
        update acnt;
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
        cntlist.add(con);
               Contact con1 = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
       
        cntlist.add(con1);
        insert cntlist;
        for(contact c: cntlist){
            cntlistId.add(c.Id);
        }
        User user = new User(alias = 'test123', email='test2156klu@mymail234.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con.Id,
                             timezonesidkey='America/New_York', username='test2156klu@mymail234.com');
        insert user;
    
        User user1 = new User(alias = 'test123', email='test1156klu@mymail234.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con1.Id,
                             timezonesidkey='America/New_York', username='test1156klu@mymail234.com');
        insert user1;
        System.RunAs(user) {
     CustomLandingPageController ctlpc = new CustomLandingPageController();  
             Ambassador__c a= new Ambassador__c();
            a.Contact__c=con.Id;
            a.Status__c='Active';
            insert a;
            ctlpc.giveGranteePermissionSetIfNeeded();
            ctlpc.RedirectToAmbform();
            ctlpc.redirectToAP();
            ctlpc.redirectToCOIPage1();
            ctlpc.redirectToMyProfile();
            ctlpc.redirectToMyProfileAP();
            ctlpc.redirectToRA();
            ctlpc.redirecttorvwportalap();
            ctlpc.redirectToVfPage();
            ctlpc.redirectToVfPage1();
            ctlpc.redirectToInfrastructure();
            ctlpc.redirectToDI();
            ctlpc.assignGranteePermission();
            ctlpc.redirectToPtoP();
            CustomLandingPageController.assignCustomerCommunityLoginPermissionSetToCommunityUser(cntlistId);
           
            
        }
        
    }   
    
    
       static testMethod void testmethod2() {
        list<contact> cntlist= new list<contact>();
          list<Id> cntlistId= new list<Id>();
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt;
        acnt.IsPartner=true;
        update acnt;
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
        cntlist.add(con);
               Contact con1 = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
       
        cntlist.add(con1);
        insert cntlist;
        for(contact c: cntlist){
            cntlistId.add(c.Id);
        }
        User user = new User(alias = 'test123', email='test2156klju@mymail234.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con.Id,
                             timezonesidkey='America/New_York', username='test2156klju@mymail234.com');
        insert user;
    
        User user1 = new User(alias = 'test123', email='test1156kltu@mymail234.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con1.Id,
                             timezonesidkey='America/New_York', username='test1156kltu@mymail234.com');
        insert user1;
        System.RunAs(user1) {
     CustomLandingPageController ctlpc = new CustomLandingPageController();  
            Ambassador__c a= new Ambassador__c();
            a.Contact__c=con1.Id;
            a.Status__c='Submitted';
            insert a;
           //ctlpc.giveGranteePermissionSetIfNeeded();
            ctlpc.RedirectToAmbform();
            ctlpc.redirectToAP();
            ctlpc.redirectToCOIPage1();
            ctlpc.redirectToMyProfile();
            ctlpc.redirectToMyProfileAP();
            ctlpc.redirectToRA();
            ctlpc.redirecttorvwportalap();
            ctlpc.redirectToVfPage();
            ctlpc.redirectToVfPage1();
            ctlpc.redirectToInfrastructure();
            ctlpc.redirectToDI();
            ctlpc.assignGranteePermission();
            ctlpc.redirectToPtoP();
            CustomLandingPageController.assignCustomerCommunityLoginPermissionSetToCommunityUser(cntlistId);
           
            
        }
        
    }  
    
          static testMethod void testmethod3() {
        list<contact> cntlist= new list<contact>();
          list<Id> cntlistId= new list<Id>();
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt;
        acnt.IsPartner=true;
        update acnt;
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
        cntlist.add(con);
               Contact con1 = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
       
        cntlist.add(con1);
        insert cntlist;
        for(contact c: cntlist){
            cntlistId.add(c.Id);
        }
        User user = new User(alias = 'test123', email='testw56klju@mymail234.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con.Id,
                             timezonesidkey='America/New_York', username='testw56klju@mymail234.com');
        insert user;
    
        User user1 = new User(alias = 'test123', email='teste56kltu@mymail234.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con1.Id,
                             timezonesidkey='America/New_York', username='teste56kltu@mymail234.com');
        insert user1;
              con1.OwnerId=user1.Id;
              update con1;
        System.RunAs(user1) {
     CustomLandingPageController ctlpc = new CustomLandingPageController();  
            ctlpc.redirectToRA();
            ctlpc.redirectToMyProfile();


           
            
        }
        
    }   
    
    
              static testMethod void testmethod4() {
        list<contact> cntlist= new list<contact>();
          list<Id> cntlistId= new list<Id>();
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt;
        acnt.IsPartner=true;
        update acnt;
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
        cntlist.add(con);
               Contact con1 = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
       
        cntlist.add(con1);
        insert cntlist;
        for(contact c: cntlist){
            cntlistId.add(c.Id);
        }
        User user = new User(alias = 'test123', email='testk@mymail234.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con.Id,
                             timezonesidkey='America/New_York', username='testk@mymail234.com');
        insert user;
    
        User user1 = new User(alias = 'test123', email='testv@mymail234.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con1.Id,
                             timezonesidkey='America/New_York', username='testv@mymail234.com');
        insert user1;
              con1.OwnerId=user1.Id;
              update con1;
        System.RunAs(user1) {
     CustomLandingPageController ctlpc = new CustomLandingPageController();  
            ctlpc.redirecttorvwportalap();


           
            
        }
        
    } 
    
    static testMethod void testmethod5() {
        list<contact> cntlist= new list<contact>();
          list<Id> cntlistId= new list<Id>();
        RecordType r1=[Select Id from RecordType where name=:'RA Applications' limit 1];
        RecordType rt=[Select Id from RecordType where name=:Label.COI_General_Record_Type limit 1];
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id sysprofileId = [select id from profile where name='System Administrator - Accenture'].id;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt;
        acnt.IsPartner=true;
        update acnt;
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
        cntlist.add(con);
               Contact con1 = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA',Reviewer_Status__c='New Reviewer');  
       
        cntlist.add(con1);
        insert cntlist;
        for(contact c: cntlist){
            cntlistId.add(c.Id);
        }
        User user = new User(alias = 'test123', email='testch@mymail234.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con.Id,
                             timezonesidkey='America/New_York', username='testch@mymail234.com');
        insert user;
    
        User user1 = new User(alias = 'test123', email='testfn@mymail234.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con1.Id,
                             timezonesidkey='America/New_York', username='testfn@mymail234.com');
        insert user1;
        
              User user2 = new User(alias = 'test123', email='systemuser2@hotmail.com',
                               emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                               localesidkey='en_US', profileid = sysprofileId , country='United States',IsActive = true,
                               timezonesidkey='America/New_York', username='systemuser2@hotmail.com');
        insert user2;
              User user3 = new User(alias = 'test123', email='systemuser3@hotmail.com',
                               emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                               localesidkey='en_US', profileid = sysprofileId , country='United States',IsActive = true,
                               timezonesidkey='America/New_York', username='systemuser3@hotmail.com');
        insert user3;
        
       
            Cycle__c cyc=new Cycle__c(Name='test2016', COI_Due_Date__c = Date.newInstance(2028,12,31));
        insert cyc;
        Panel__c pan= new Panel__c(name='test',Cycle__c=cyc.id,MRO__c=user3.Id,InPerson_Review_Deadline__c=Date.newInstance(2028,12,31),Panel_Due_Dtae__c=Date.newInstance(2028,12,31));
        insert pan;
            test.startTest();
             Opportunity o= new opportunity();

o.Awardee_Institution_Organization__c=acnt.Id;
o.RecordTypeId=r1.id;
o.Full_Project_Title__c='test';
o.Project_Name_off_layout__c='test';
o.Panel__c=pan.Id;
o.CloseDate=Date.today();
o.Application_Number__c='Ra-1234';
o.StageName='In Progress';
o.Name='test';
o.PI_Name_User__c=user2.Id;
o.AO_Name_User__c=user3.Id;
insert o;
      Panel_Assignment__c panAss=  new Panel_Assignment__c(Reviewer_Name__c=con1.id,panel__c=pan.id,Panel_Role__c='Reviewer');
       insert panAss;
     
        COI_Expertise__c co=new COI_Expertise__c(Reviewer_Name__c=con1.Id,Related_Project__c=o.Id);
        co.RecordTypeId=rt.Id;
        insert Co; 
       system.runAs(user){
             CustomLandingPageController ctlpc = new CustomLandingPageController();  
       }
test.stopTest();
       
       
        
    }   
    
    
     static testMethod void testmethod6() {
        list<contact> cntlist= new list<contact>();
          list<Id> cntlistId= new list<Id>();
        RecordType r1=[Select Id from RecordType where name=:'RA Applications' limit 1];
        RecordType rt=[Select Id from RecordType where name=:Label.COI_General_Record_Type limit 1];
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id sysprofileId = [select id from profile where name='System Administrator - Accenture'].id;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt;
        acnt.IsPartner=true;
        update acnt;
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
        cntlist.add(con);
               Contact con1 = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA',Reviewer_Status__c='New Reviewer');  
       
        cntlist.add(con1);
        insert cntlist;
        for(contact c: cntlist){
            cntlistId.add(c.Id);
        }
        User user = new User(alias = 'test123', email='testch1@mymail234.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con.Id,
                             timezonesidkey='America/New_York', username='testch1@mymail234.com');
        insert user;
    
        User user1 = new User(alias = 'test123', email='testfn1@mymail234.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con1.Id,
                             timezonesidkey='America/New_York', username='testfn1@mymail234.com');
        insert user1;
        
              User user2 = new User(alias = 'test123', email='system1user2@hotmail.com',
                               emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                               localesidkey='en_US', profileid = sysprofileId , country='United States',IsActive = true,
                               timezonesidkey='America/New_York', username='system1user2@hotmail.com');
        insert user2;
              User user3 = new User(alias = 'test123', email='system1user3@hotmail.com',
                               emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                               localesidkey='en_US', profileid = sysprofileId , country='United States',IsActive = true,
                               timezonesidkey='America/New_York', username='system1user3@hotmail.com');
        insert user3;
        
      
            Cycle__c cyc=new Cycle__c(Name='test2016', COI_Due_Date__c = Date.newInstance(2028,12,31));
        insert cyc;
        Panel__c pan= new Panel__c(name='test',Cycle__c=cyc.id,MRO__c=user3.Id,InPerson_Review_Deadline__c=Date.newInstance(2028,12,31),Panel_Due_Dtae__c=Date.newInstance(2028,12,31));
        insert pan;
            test.startTest();
             Opportunity o= new opportunity();

o.Awardee_Institution_Organization__c=acnt.Id;
o.RecordTypeId=r1.id;
o.Full_Project_Title__c='test';
o.Project_Name_off_layout__c='test';
o.Panel__c=pan.Id;
o.CloseDate=Date.today();
o.Application_Number__c='Ra-12345';
o.StageName='In Progress';
o.Name='test';
o.PI_Name_User__c=user2.Id;
o.AO_Name_User__c=user3.Id;
insert o;
       system.runAs(user){
             CustomLandingPageController ctlpc = new CustomLandingPageController();  
       }
test.stopTest();
       
       
        
    }   
    
    
         static testMethod void testmethod7() {
        list<contact> cntlist= new list<contact>();
          list<Id> cntlistId= new list<Id>();
        RecordType r1=[Select Id from RecordType where name=:'RA Applications' limit 1];
        RecordType rt=[Select Id from RecordType where name=:Label.COI_General_Record_Type limit 1];
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id sysprofileId = [select id from profile where name='System Administrator - Accenture'].id;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt;
        acnt.IsPartner=true;
        update acnt;
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
        cntlist.add(con);
               Contact con1 = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA',Reviewer_Status__c='New Reviewer');  
       
        cntlist.add(con1);
        insert cntlist;
        for(contact c: cntlist){
            cntlistId.add(c.Id);
        }
        User user = new User(alias = 'test123', email='testch1kl@mymail234.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con.Id,
                             timezonesidkey='America/New_York', username='testch1kl@mymail234.com');
        insert user;
    
        User user1 = new User(alias = 'test123', email='testfn1kl@mymail234.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con1.Id,
                             timezonesidkey='America/New_York', username='testfn1kl@mymail234.com');
        insert user1;
        
              User user2 = new User(alias = 'test123', email='system1user2kl@hotmail.com',
                               emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                               localesidkey='en_US', profileid = sysprofileId , country='United States',IsActive = true,
                               timezonesidkey='America/New_York', username='system1user2kl@hotmail.com');
        insert user2;
              User user3 = new User(alias = 'test123', email='system1user3kl@hotmail.com',
                               emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                               localesidkey='en_US', profileid = sysprofileId , country='United States',IsActive = true,
                               timezonesidkey='America/New_York', username='system1user3kl@hotmail.com');
        insert user3;
       
            Cycle__c cyc=new Cycle__c(Name='test2016', COI_Due_Date__c = Date.newInstance(2028,12,31));
        insert cyc;
        Panel__c pan= new Panel__c(name='test',Cycle__c=cyc.id,MRO__c=user3.Id,InPerson_Review_Deadline__c=Date.newInstance(2028,12,31),Panel_Due_Dtae__c=Date.newInstance(2028,12,31));
        insert pan;
            test.startTest();
             Opportunity o= new opportunity();

o.Awardee_Institution_Organization__c=acnt.Id;
o.RecordTypeId=r1.id;
o.Full_Project_Title__c='test';
o.Project_Name_off_layout__c='test';
o.Panel__c=pan.Id;
o.CloseDate=Date.today();
o.Application_Number__c='Ra-123456';
o.StageName='In Progress';
o.Name='test';
o.PI_Name_User__c=user2.Id;
o.AO_Name_User__c=user3.Id;
insert o;
             //Panel_Assignment__c panAss=  new Panel_Assignment__c(Reviewer_Name__c=con1.id,panel__c=pan.id,Panel_Role__c='Reviewer');
      //insert panAss;
     
        COI_Expertise__c co=new COI_Expertise__c(Reviewer_Name__c=con1.Id,Related_Project__c=o.Id,ownerId=user1.Id,Active__c=true);
        co.RecordTypeId=rt.Id;
            co.Conflict_of_Interest__c='No';
            co.Expertise_Level__c='Low';
           
        insert Co; 
        co.Submitted__c=true;
            update co;
            FC_Reviewer__External_Review__c rvs= new FC_Reviewer__External_Review__c();
          rvs.FC_Reviewer__Opportunity__c=o.id;
          rvs.FC_Reviewer__Contact__c=con1.Id;
          insert rvs;
             system.runAs(user){
             CustomLandingPageController ctlpc = new CustomLandingPageController();  
                 }
test.stopTest();
      
       
        
    }
    
}