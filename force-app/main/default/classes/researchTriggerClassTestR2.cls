@isTest
public class researchTriggerClassTestR2 {
    
    public static testMethod void updateCOItechnicalabstract(){
    test.starttest();
        Contact con = new Contact(LastName = 'testcontact');
      insert con;
       Cycle__c cyc=new Cycle__c(Name='test2017', COI_Due_Date__c = Date.newInstance(2016,12,31));
       insert cyc;
       Panel__c pan= new Panel__c(name='test17',Cycle__c=cyc.id,Panel_Due_Dtae__c=  Date.newInstance(2019,12,20));
        insert pan;
        Opportunity ra= new Opportunity();
        ra.Name='test';
        ra.StageName = 'In Progress';
        ra.Panel__c=pan.Id;
        ra.Technical_Abstract__c='xyz1';
        ra.CloseDate = Date.newInstance(2017, 2, 1);
        ra.Name = 'Test Opty1111';
        ra.Full_Project_Title__c ='testtile';
        ra.Project_Name_off_layout__c='testtile';
        insert ra;
       
        COI_Expertise__c c=new COI_Expertise__c();
        c.Related_Project__c=ra.Id;
        c.Technical_Abstract__c=ra.Technical_Abstract__c;
        c.Reviewer_Name__c=con.Id;
        insert c;
         ra.Technical_Abstract__c='xyza1';
        ra.Full_Project_Title__c='abcdefgh';
       update ra;
        c.Technical_Abstract__c=ra.Technical_Abstract__c;
        c.Project_Title_New__c=ra.Full_Project_Title__c;
        update c;
        test.stoptest();
       
    }
    
   
}