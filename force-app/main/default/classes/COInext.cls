//This Class is for displaying the associated Research Application Key Personnel details on the COI Page
public class COInext {
public Id recid;
    public string coiid;
    public COInext(ApexPages.StandardController controller) {
        //Gets the particular record id
recid=apexpages.currentpage().getparameters().get('id');
    }
    
    public list<COI_Expertise__c> lst;
     public List<COI_Expertise__c> getlistOfObject() {
      
            COI_Expertise__c c=new COI_Expertise__c();
            //Below soql query gets Research Application ID from the particular COI record
//              c=[select Research_Application__c,Principal_Investigator__c,Program_Organization__c from COI_Expertise__c where id=:recid limit 1];
              c=[select Research_Application__c,Principal_Investigator_Project__c,Program_Organization__c from COI_Expertise__c where id=:recid limit 1];

return null;    
  
     }
//  public list<Research_Key__c> cons1;
  public list<Key_Personnel__c> cons1;
//    public List<Research_Key__c> getFundingApplications() {
    public List<Key_Personnel__c> getFundingApplications() {
        system.debug(recid);
        if(recid!=null){
            COI_Expertise__c c=new COI_Expertise__c();
            //Below soql query gets Research Application ID from the particular COI record
//              c=[select Related_Project__c,Principal_Investigator__c,Program_Organization__c from COI_Expertise__c where id=:recid limit 1];
              c=[select Related_Project__c,Principal_Investigator_Project__c,Program_Organization__c from COI_Expertise__c where id=:recid limit 1];

        coiid=string.valueof(c.Related_Project__c); 
        }
        //Below is the soql query to get the details from the Research Application Key Personnel Object and return those values.
//       cons1=[Select Name, Key_Personnel_Organization__c, Key_Personnel_Role__c,Key_Personnel_Position__c from Key_Personnel__c where Research_Application__r.Id=:coiid];
//        cons1=[Select Name, Institution_Org__c, Role_Display__c,Representation__c from Key_Personnel__c where Opportunity__c=:coiid];
          cons1=[Select Name,First_Name__c,Last_Name__c,Institution_Org__c,Role__c,KP_Role__c, Role_Display__c,Representation__c from Key_Personnel__c where Opportunity__c=:coiid];
        if(cons1.size()>0){return cons1;}    
        else{
        return null;
        }
      }
}