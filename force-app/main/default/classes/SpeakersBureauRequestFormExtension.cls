/****************************************************************************************************
*Description:           This is the extension used by the SBProposalWebform for Speaker Bureau request
*                                         
*                       
*
*Required Class(es):    SBProposalExtension_Test
*
*Organization: 
*
*Revision   |Date       |Author             |AdditionalNotes
*====================================================================================================
*   1.0     03/02/2017   Himanshu Kalra     Initial Version
*   2.0     10/05/2019   Abhinav Polsani    Added two new fields to the page "Will_your_event_be_accredited_for_CME__c",
                                            Who_is_the_accredited_provider__c. 
*****************************************************************************************************/
public class SpeakersBureauRequestFormExtension{

    public string organizerPrefix{get;set;}
    public string organizerFirstName{get;set;}
    public string organizerLastName{get;set;}
    public string organization{get;set;} 
    public string orgType{get;set;}
    public string Email{get;set;}
    public string Phone{get;set;}
    public string venueCity{get;set;}
    public string venueState{get;set;}
    public string topicPPT{get;set;}
    public string formatPPT{get;set;}    
    public string eventType{get;set;}
    public string otherHostOrg{get;set;}
    public string preferSpeakrs{get;set;}
    public string otherPreferSpeakrs{get;set;}  
    public string requestType{get;set;}
    public string PCORI_StafName{get;set;}
    public string PCORI_StafEmail{get;set;}
    public integer audienceSize{get;set;}
    public string audienceComp{get;set;} 
    public string speakTime{get;set;}
    public Date speakDate {get;set;}  
    public Date alternSpeakDate {get;set;}
    public Date confirmationRequestDate {get;set;}
    public string eventTitle {get;set;}
    public String cmeAccredited {get; set;}
    public String accreditedProvider {get; set;}

    public string text1=Label.Single_Community_Profile;
    

    public boolean Submitted{get;set;}
    
    public SpeakersBureauRequestFormExtension(Apexpages.StandardController controller){
        Submitted = false;
    }
    
    public void Submit(){
        boolean error = false;
        
        if (requestType == '-Select-'){
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,System.Label.Request_Type));
        }
        if (orgType == '-Select-'){
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,System.Label.Organization_Type));
        }
        if (String.isBlank(organization)){
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,System.Label.Organization_Name_Required));
        }
        if (organizerPrefix == '-Select-'){
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,System.Label.Organizer_Name_Prefix_Required));
        }
        
        if (String.isBlank(organizerFirstName)){
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,System.Label.Organizer_First_Name_Required));
        }
        if (String.isBlank(organizerLastName)){
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,System.Label.Organizer_Last_Name_Required));
        }
        if (String.isBlank(Phone)){
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,System.Label.Phone_Number_Required));
        } 
        if (!String.isBlank(Phone)){
            string unformatPhone = Phone.replaceAll('\\D+', '');
            if(unformatPhone.length()< 10){
                error = true;
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,System.Label.Phone_number_should_only_be_10_digits));
            }
        }
        if (String.isBlank(Email)){
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,System.Label.Email_Required));
        }
        if (String.isBlank(eventTitle)){
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,System.Label.Event_Title_Required));
        }
        if (eventType == '-Select-'){
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,System.Label.Type_of_Event_Required));
        } 
        if (String.isBlank(speakTime)){
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,System.Label.Speaking_Time_Required));
        }
        if (String.isBlank(topicPPT)){
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,System.Label.Presentation_Topic_Required));
        }
        if (formatPPT == '-Select-'){
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,System.Label.Presentation_Format_Required));
        }
        if (String.isBlank(String.valueOf(speakDate))){
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,System.Label.Speaking_Date_Required));
        }
        if (String.isBlank(otherPreferSpeakrs)){
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,System.Label.Other_Confirmed_Speakers_Required));
        }
        if (audienceComp == null || audienceSize== 0){ 
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,System.Label.Audience_Size_Required));
        }
        if (String.isBlank(audienceComp)){
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,System.Label.Audience_Composition_Required));
        }
        if (String.isBlank(venueCity)){
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,System.Label.Venue_City_Required));
        }
        if (String.isBlank(venueState)){
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,System.Label.Venue_State_Required));
        } 
        if (String.isBlank(String.valueOf(confirmationRequestDate))){
            error = true;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,System.Label.Confirmation_Request_Date_Required));
        }
        if((String.isNotBlank(Email)) && !Pattern.matches('[a-zA-Z0-9._-]+@[a-zA-Z]+.[a-zA-Z]{2,4}[.]{0,1}[a-zA-Z]{0,2}', Email)){
            error = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, System.Label.Check_your_email)); 
        }
        if((String.isNotBlank(PCORI_StafEmail)) && !Pattern.matches('[a-zA-Z0-9._-]+@[a-zA-Z]+.[a-zA-Z]{2,4}[.]{0,1}[a-zA-Z]{0,2}', PCORI_StafEmail)){
            error = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, System.Label.Check_PCORI_Staff_Email)); 
        }
        if (cmeAccredited == '-Select-') {
            error = true;
            ApexPages.addMessage(new Apexpages.message(Apexpages.Severity.Error, Label.CME_Accredited_Required));
        }
        if (cmeAccredited == 'Yes' && String.isBlank(accreditedProvider)) {
            error = true;
            ApexPages.addMessage(new Apexpages.message(Apexpages.Severity.Error, Label.Accredited_Provider_Required));
        }
        if (!error){
            try {
                User validUser;
                Id UserId = Userinfo.getuserid();
                validUser = [SELECT Id FROM User WHERE IsActive = true AND Id =:  UserId LIMIT 1];

                if (validUser != null){
                    Speakers_Bureau__c speakBur = new Speakers_Bureau__c();
                    speakBur.Organization_Type__c = orgType;
                    speakBur.Organization__c = organization;
                    //speakBur.Organizer_Contact_Prefix__c = organizerPrefix;
                    if(String.isNotBlank(organizerPrefix) && organizerPrefix != 'None') speakBur.Organizer_Contact_Prefix__c = organizerPrefix;
                    speakBur.Organizer_Contact_First_Name__c = organizerFirstName;
                    speakBur.Organizer_Contact_Last_Name__c = organizerLastName;
                    speakBur.Email__c = Email;
                    string unformatPhoneLength = Phone.replaceAll('\\D+', '');
                    if(unformatPhoneLength.length()==10)  speakBur.Phone_Number__c = unformatPhoneLength;
                    speakBur.OwnerId = validUser.Id;
                    speakBur.Topic_Presentation_Title__c = topicPPT; 
                    speakBur.Event_Title__c = eventTitle;
                    speakBur.Type_of_Event__c = eventType;
                    speakBur.Presentation_Format__c = formatPPT;
                    speakBur.Other_Host_Organization_s__c = otherHostOrg;
                    speakBur.Preferred_Speakers__c = preferSpeakrs;
                    speakBur.Other_Confirmed_Speakers__c = otherPreferSpeakrs;
                    speakBur.Audience_Size__c = audienceSize;
                    speakBur.Audience_Composition__c = audienceComp;
                    speakBur.Venue_City__c = venueCity;
                    speakBur.Venue_State_Province_Region_or_Country__c = venueState;  
                    speakBur.Speaking_Time__c = speakTime;
                    speakBur.Speaking_Date__c = speakDate;
                    speakBur.Alternative_Speaking_Date__c = alternSpeakDate;
                    speakBur.Request_Confirmation_Date__c = confirmationRequestDate;    
                    speakBur.Request_Type__c = requestType;         
                    speakBur.PCORI_Staff_Name__c = PCORI_StafName;
                    speakBur.PCORI_Staff_Email__c = PCORI_StafEmail;
                    speakBur.Will_your_event_be_accredited_for_CME__c = cmeAccredited;
                    speakBur.Who_is_the_accredited_provider__c = accreditedProvider;
                    insert(speakBur);
                    Submitted = true;
                }
            }
            catch (Exception ex){
                //Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Error: '+ex.getMessage()));
                System.debug('The Save method Exception '+ex.getMessage()); //must have been an issue with the insert
            }
        }
    }
}