/*
This Batch Class removes the values from all the email fields accross the selected object.

*/

global class SearchAndReplaceRevert implements Database.Batchable<sObject>{

   Public String Query;
   Public String Value;
   Public String realEmail;
   Public String tempObj;
   Public List<String>fieldsArray = new List<String>();


    public SearchAndReplaceRevert(String obj, String v){
    tempObj = obj;
    Value=v;
    //Gets the Object Description
    SObjectType objToken = Schema.getGlobalDescribe().get(tempObj);
        //Get all the fields from the Object
        DescribeSObjectResult objDef = objToken.getDescribe();
        Map<String, SObjectField> fields = objDef.fields.getMap(); 
        SObjectField fieldToken;
        DescribeFieldResult selectedField;
        
        Set<String> fieldSet = fields.keySet();
         //Iterate through the list of all the fields on the object 
        for(String s:fieldSet)
        {
                fieldToken = fields.get(s);
                selectedField = fieldToken.getDescribe();
                //Check whether the field is an Email field and is editable or not
                if(string.valueof(selectedField.getType())== 'Email' && selectedField.isUpdateable()){
                fieldsArray.add(s);
                
                } 
        }

    String tempquery = 'select ID,';
    //Add all the fields to the query string.
    for(String FA:fieldsArray){
       tempquery+=FA+',';
    }
    //Remove the last comma from the query
     tempquery = tempquery.substring(0,tempquery.length()-1);
       Query = tempquery +' from '+obj;
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      //Execute the query
      return Database.getQueryLocator(Query);
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
   //Iterate through the list of records retrieved from the query
     for(sobject s : scope){
    
     
     for(integer i=0;i<fieldsArray.size();i++){
     //Check whether the field is null
     if(s.get(fieldsArray[i])!=null){
         realemail = (String)s.get(fieldsArray[i]);
         if(realemail!=null && realemail.contains(value)){
         //Replace the Pvided text with empty value.
         s.put(fieldsArray[i],realemail.replace(value,''));
        
     }
     }
     }
     }
     //update scope;

    }

   global void finish(Database.BatchableContext BC){
  //Queries the batch job from Apex Jobs and display the number of errors in Debug logs.   
 AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,  
  TotalJobItems, CreatedBy.Email, ExtendedStatus  
  from AsyncApexJob where Id = :BC.getJobId()];  
   system.debug('The total number of Jobs are '+ a.TotalJobItems);  
   system.debug('The number of errors are '+a.NumberOfErrors);
 // Email the Batch Job's submitter that the Job is finished.  
 /*Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
 String[] toAddresses = new String[] {a.CreatedBy.Email};  
 mail.setToAddresses(toAddresses);  
 mail.setSubject('BatchJob Revert Emails Status: ' + a.Status);  
 mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems +  
  ' batches with '+ a.NumberOfErrors + ' failures. ExtendedStatus: ' + a.ExtendedStatus);  
    
 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
*/

   }
}