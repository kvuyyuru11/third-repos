global class postrefreshbatch  implements SandboxPostCopy{
    
    global void runApexClass(SandboxContext context) {
       createAccounts();
       createCampaign();
   }
    public static void createAccounts() {
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        List<Account> accts = new List<Account>();
        List<Account> act = new List<Account>();
        
        for(Integer i=0;i<2;i++) {
            Account a;
            if(i==0)
            a = new Account(Name='PCORI Portal Account',ownerId = '00570000002eX5e',recordtypeid=recordTypeId);
            if(i==1)
            a = new Account(Name='Not Provided',ownerId = '00570000002eX5e',recordtypeid=recordTypeId);
            accts.add(a);
        }
        insert accts;
        for(Account a:accts){
            a.IsPartner=true;
            act.add(a);
        }
        update act;
    }
    
    public static void createCampaign() {
        Id recordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('PCORI Portal').getRecordTypeId();
        Campaign cmp = new Campaign(Name='Supplemental Funding', IsActive=True, FGM_Portal__Visibility__c='Public', Status='In Progress', OwnerId='00570000002eX5e');
        insert cmp;
    }
}