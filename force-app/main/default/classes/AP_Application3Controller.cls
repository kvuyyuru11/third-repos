/****************************************************************************************************
*Description:           Controler class for the Application page 1 vfp
*                       
*
*Required Class(es):    N/A
*
*Organization: Rainmaker-LLC
*Revision   |Date       |Author             |AdditionalNotes
*====================================================================================================
*   1.0      12/02/15       Anirudh Sharma      Initial Version
*   1.01     1/10/2016      Anirudh Sharma      Updated for Pcori-Portal
*   1.01     1/10/2016      Daniel Haro         Bug fixes
*****************************************************************************************************/




public with sharing class AP_Application3Controller
{
    // Class members.    
    public String personalStatement {get;set;}
    public String biography {get;set;}
   
    public String supportingDocuments1 {get;set;}
    public String supportingDocuments2 {get;set;}
    public String supportingDocuments3 {get;set;}
    public String supportingDocuments4 {get;set;}
    public String supportingDocuments5 {get;set;}
    public String phonestyle{ get; set; }
    public String phonestyle1{ get; set; }
    public String phonestyle2{ get; set; }
    public String phonestyle3{ get; set; }
    public String bio1{get;set;}
        public String estyle{ get; set; }
        public String poiu{ get;set;}
        public boolean r{get;set;}
        
    public integer x;
    

    
     
    public Advisory_Panel_Application__c apa {get;set;}
    public List<Advisory_Panel_Application__c> myApp;
    
       
    // Constructor.
    public AP_Application3Controller()
    {
        
        poiu='';
        phonestyle='';
        phonestyle1='';
        phonestyle2='';
        phonestyle3='';
        
        bio1='';
        x=0;
        // Query application for current user.
        myApp = [SELECT Id, Personal_Statement2__c, Bio__c, Permission_to_post_bios_on_website__c, Please_tell_us_anything_else__c, Additional_Supportive_Information__c, Application_Status_New__c from Advisory_Panel_Application__c where CreatedBy.Id =:userinfo.getUserId() and Application_Status_New__c = 'Draft' order by CreatedDate DESC limit 1];
        // If existent, extract application and fields.
        if (myApp.size() > 0)
        {
            
            apa = myApp[0];
        }
    
        }
    // Login for "Next" button.
    
        public pagereference Nextmethod()
        {
        phonestyle='';
            phonestyle1='';
            phonestyle2='';
            phonestyle3='';
            bio1='';
        poiu='';
            x=0;
            //saveMethod();
            
         if(String.isBlank(apa.Personal_Statement2__c)){
      phonestyle= label.css_value; 
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.')); 
      x=1;
         } 
        
            if(String.isBlank(apa.Permission_to_post_bios_on_website__c)){
            poiu=label.css_value;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
            x=1;
            }
            if(apa.Bio__c!=null)
            if(apa.Bio__c.length()>1500){
            phonestyle1= label.css_value; 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You have exceeded the character limit for a response. Please revise your text before continuing your application.'));
             
            x=1;
            }
             if(apa.Personal_Statement2__c.length()>3400){
            phonestyle= label.css_value; 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You have exceeded the character limit for a response. Please revise your text before continuing your application.'));
             
            x=1;
            }
             if(apa.Please_tell_us_anything_else__c.length()>1000){
            phonestyle2= label.css_value; 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You have exceeded the character limit for a response. Please revise your text before continuing your application.'));
             x=1;
            }
             if(apa.Additional_Supportive_Information__c.length()>18000){
            phonestyle3= label.css_value; 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You have exceeded the character limit for a response. Please revise your text before continuing your application.'));
             
            x=1;
            }
            if(apa.Permission_to_post_bios_on_website__c!=null)
            if(apa.Permission_to_post_bios_on_website__c.equals('Yes')){
            if(String.isblank(apa.Bio__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
            bio1=label.css_value;
            x=1;
            }
            }
             if(x==1){
            return null;
             } else {
               saveMethod();
              
        pagereference pg = new pagereference(label.AP_Application_Summary);
        
                return pg;}
    }
    
    // Logic for "Previous" button.
    public pagereference Previousmethod()
    {
       //saveMethod();  
        pagereference pg = new pagereference(label.AP_Application2);
        return pg;
    }
    
    // Logic for "Save as Draft" button.
    public pagereference saveMethod()
    {
        phonestyle='';
         phonestyle1='';
         phonestyle2='';
         phonestyle3='';
        estyle='';
        x=0;
        // Get current user.
        User us = [select id, contactid, Lastname from user where id =: userinfo.getUserId() limit 1];
        
        
        // If no application exists for user,
        if(myApp.size() == 0)
        {   
            // Create application, save fields to it,
            Advisory_Panel_Application__c saveAPA = new Advisory_Panel_Application__c();
            saveAPA.Personal_Statement2__c = apa.Personal_Statement2__c;
            saveAPA.Bio__c = apa.Bio__c;
            saveAPA.Permission_to_post_bios_on_website__c = apa.Permission_to_post_bios_on_website__c;
            saveAPA.Please_tell_us_anything_else__c = apa.Please_tell_us_anything_else__c;
            saveAPA.Additional_Supportive_Information__c = apa.Additional_Supportive_Information__c;
            saveAPA.Contact__c = us.contactId;
            saveAPA.Application_Status_New__c = 'Draft';
            saveAPA.Last_Name__c = us.LastName;
                
              if(apa.Permission_to_post_bios_on_website__c!=Null)
                if(apa.Permission_to_post_bios_on_website__c.equals('No')){
   apa.Bio__c='';
    }
            
            if(apa.Bio__c.length()>1500){
            phonestyle1= label.css_value; 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You have exceeded the character limit for a response. Please revise your text before continuing your application.'));
             
            x=1;
            }
             if(apa.Personal_Statement2__c.length()>3400){
            phonestyle= label.css_value; 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You have exceeded the character limit for a response. Please revise your text before continuing your application.'));
             
            x=1;
            }
             if(apa.Please_tell_us_anything_else__c.length()>1000){
            phonestyle2= label.css_value; 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You have exceeded the character limit for a response. Please revise your text before continuing your application.'));
             x=1;
            }
             if(apa.Additional_Supportive_Information__c.length()>18000){
            phonestyle3= label.css_value; 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You have exceeded the character limit for a response. Please revise your text before continuing your application.'));
             
            x=1;
             }
              
            // and insert.
            if(x==1){
            return null;
            }
            else{
            insert saveAPA;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Your application has been saved as a draft.',''));
            }
        }
            
          
        // Otherwise
        else
        {
            if(myApp[0]!=null)
                 if(apa.Permission_to_post_bios_on_website__c!=Null)
                if(apa.Permission_to_post_bios_on_website__c.equals('No')){
   apa.Bio__c='';
    }
            // update.
            if(myApp[0].Bio__c!=Null){
                
            if(myApp[0].Bio__c.length()>1500){
            phonestyle1= label.css_value; 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You have exceeded the character limit for a response. Please revise your text before continuing your application.'));
             
            x=1;
            }
            }
             if(myApp[0].Personal_Statement2__c.length()>3400){
            phonestyle= label.css_value; 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You have exceeded the character limit for a response. Please revise your text before continuing your application.'));
             
            x=1;
            }
             if(myApp[0].Please_tell_us_anything_else__c.length()>1000){
            phonestyle2= label.css_value; 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You have exceeded the character limit for a response. Please revise your text before continuing your application.'));
             x=1;
            }
             if(myApp[0].Additional_Supportive_Information__c.length()>18000){
            phonestyle3= label.css_value; 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You have exceeded the character limit for a response. Please revise your text before continuing your application.'));
             
            x=1;
            }
            if(x==1){
            return null;
            }
             else{
            update myApp[0]; 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Your application has been saved as a draft.',''));
            }
        }
        
        // Refresh.
        return null;
    }
  
   
    // Returns "Yes" or "No" options for bio permission question.
     public List<SelectOption> getBioPermissions()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Yes', 'Yes'));
        options.add(new SelectOption('No', 'No'));
        return options;
    }
    public void checkCount2(){
    if(apa.Permission_to_post_bios_on_website__c.equals('Yes')){
    r=true;
    }
     
    }
  
}