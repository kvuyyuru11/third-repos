/* This class belongs to the trigger IRBEmailUpdate. 
This class will update Email Fields in the IRB only when the IRB Object is edited or created.*/

Public without Sharing class UpdateEmailIRB{
//Constructor
    Public UpdateEmailIRB(){
    }

   Public static void UpdateEmailonIRB (List<IRB__C> irbList){ /* This method will update email fields in IRB*/
   
    Set <Id> parentIds = new set<Id>();
    for(IRB__c irb : irbList) {
        parentIds.add(irb.Project_Title__c);
    }
      
    List<Opportunity> oppSOQLList =[SELECT Id, Name, PI_Name_User__c,PI_Name_User__r.email, PI_Project_Lead_2_Name_New__r.Email, PI_Project_Lead_Designee_1_New__r.Email, PI_Project_Lead_Designee_2_Name_New__r.Email,Program_Associate__r.Email,Program_Officer__r.Email,AO_Name_User__r.Email,Contract_Administrator__r.Email,Contract_Coordinator__r.Email FROM Opportunity where id IN : parentIds Limit 50000 ]; 

    List<IRB__c> irbSOQLList =[SELECT id,Email_PI_Project_Lead_1__c, Email_PI_Project_Lead_2__c, Email_PI_Project_Lead_Designee_1__c, Email_PI_Project_Lead_Designee_2__c, Email_Program_Associate__c, Email_Program_Officer__c, Email_Administrative_Official__c, Email_Contract_Administrator__c, Email_Contract_Coordinator__c,Project_Title__c FROM IRB__c WHERE Project_Title__c IN : parentIds Limit 50000];
       
    Map<Id, IRB__C> irbmap = new Map<Id, IRB__C>();
    Map<Id, Opportunity> oppmap = new Map<Id, Opportunity>();

   for(Opportunity opp: oppSOQLList)
   {
       oppmap.put(opp.id, opp);
   }//end For loop
   try{
        for(IRB__c irb:irbList)
          {
      
            irb.Email_PI_Project_Lead_1__c = oppmap.get(irb.Project_Title__c).PI_Name_User__r.email;
            irb.Email_PI_Project_Lead_2__c = oppmap.get(irb.Project_Title__c).PI_Project_Lead_2_Name_New__r.Email;
            irb.Email_PI_Project_Lead_Designee_1__c = oppmap.get(irb.Project_Title__c).PI_Project_Lead_Designee_1_New__r.Email;
            irb.Email_PI_Project_Lead_Designee_2__c = oppmap.get(irb.Project_Title__c).PI_Project_Lead_Designee_2_Name_New__r.Email;
            irb.Email_Program_Associate__c = oppmap.get(irb.Project_Title__c).Program_Associate__r.Email;
            irb.Email_Program_Officer__c = oppmap.get(irb.Project_Title__c).Program_Officer__r.Email;
            irb.Email_Administrative_Official__c = oppmap.get(irb.Project_Title__c).AO_Name_User__r.Email;
            irb.Email_Contract_Administrator__c = oppmap.get(irb.Project_Title__c).Contract_Administrator__r.Email;
            irb.Email_Contract_Coordinator__c = oppmap.get(irb.Project_Title__c).Contract_Coordinator__r.Email;
          
          }//end for loop
       }catch(DmlException e) {
           System.debug('The following exception has occurred: ' + e.getMessage());       
       }Catch(Exception e)  {
           System.debug('The following exception has occurred: ' + e.getMessage());
       }// try catch block
         
    }// end UpdateEmailonIRB
    
}// end UpdateEmailIRB