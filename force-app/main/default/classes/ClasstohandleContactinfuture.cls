/*
* Class : ClasstohandleContactinfuture
* Author : Kalyan Vuyyuru
* Version History : 1.0
* Creation : 8/17/2017
* Last Modified By: Kalyan Vuyyuru
* Last Modified Date: 8/21/2017
* Modification Description: Edited this class to add comments
* Description : This Class is a handler class that is called from checkuseractive trigger when email changed, username changed,isactive changed on
insert or update operations. For these three operations to avoid the mixed dml issues we have future method.
*/

public class ClasstohandleContactinfuture {
    
    //This method collects all users where user inserted or changed for email, username and isactive and updates related contact's email, username and isactive accordingly.
    @future
    public static void collectusrid(List<Id> unmrrlist){
        Map<Id, string> ucmap1= new Map<Id, string>();
        Map<Id, string> ucmap2= new Map<Id, string>();
        Map<Id, boolean> ucmap3= new Map<Id, boolean>();
        List<contact> cntlisttoupdt= new List<contact>();
        list<User> unlist=[select Id, Username,contactId,email,isactive from User where ID IN:unmrrlist];
        for(User u:unlist){
            ucmap1.put(u.contactId,u.Email);
            ucmap2.put(u.contactId,u.username);
            ucmap3.put(u.contactId,u.isactive);
        }
        list<Contact> cntlist=[select Id,User_Name_for_MR__c,Is_user_Active__c,Email from Contact where ID IN:ucmap1.keyset() OR ID IN:ucmap2.keyset() OR ID IN:ucmap3.keyset()];
        for (Contact c:cntlist){
            c.email=ucmap1.get(c.Id);
            c.User_Name_for_MR__c=ucmap2.get(c.Id);
            c.Is_User_Active__c=ucmap3.get(c.Id);
            cntlisttoupdt.add(c);
        }
        database.update(cntlisttoupdt);
    }
    
}