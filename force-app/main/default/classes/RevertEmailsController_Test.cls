@isTest
private class RevertEmailsController_Test{
    static testMethod void RevertEmailsController_Test_Method1() {
        Account acc = new Account();
        acc.name = 'Wimbeldon Account';
        insert acc;
        contact con = new contact();
        con.AccountId=acc.Id;
        con.firstname = 'Sara';
        con.LastName = 'Tancredi';
        con.email = 'sara7jald@nomail.com.test';
        con.community__c = 'TBD';
        insert con;
        
        RevertEmailsController REC = new RevertEmailsController();
        REC.batchClassMessage='Completed';
        REC.selectedObject='contact';
        REC.sendappendtext = '.test';
        REC.executeBatch();
        
        REC.getMessage();
        REC.sendObjectName = 'Contact';
        REC.sendappendtext = '.test';
        REC.getObjectNames();
        REC.batchClassMessage='Completed';
        REC.selectedObject='contact';
        REC.getMessage();
    }
    static testMethod void AppendEmailsTextController_Test_Method1() {
        Account acc = new Account();
        acc.name = 'Australian Open Account';
        insert acc;
        list<Contact> conlist = new List<Contact>();
        contact con = new contact();
        con.accountId = acc.Id;
        con.firstname = 'DB';
        con.LastName = 'Cooper';
        con.email = 'dbcooper7jald@nomail.com.test';
        con.community__c = 'TBD';
        insert con;
        AppendEmailsTextController AEC = new AppendEmailsTextController();
        AEC.selectedObject='contact';
        AEC.executeBatch();
        AEC.getMessage();
        AEC.sendObjectName = 'Contact';
        AEC.sendappendtext = '.test';
        AEC.getObjectNames();
        AEC.getMessage();
        AEC.batchClassMessage='Aborted';
        AEC.batchClassMessage='Completed';
    }   
}