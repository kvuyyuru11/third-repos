/****************************************************************************************************
*Description:           This is the test class for the EAProposalTrigger and EAProposal class
*                                         
*                       
*
*Required Class(es):    N/A
*
*Organization: 
*
*Revision   |Date       |Author             |AdditionalNotes
*====================================================================================================
*   1.0     07/03/2014   Justin Padilla     Initial Version
*****************************************************************************************************/
@isTest
private class EAProposalTrigger_Test {

  private static Id recordTypeId{get;set;}
  
    static testMethod void TriggerTest()
  {
    //Retreive the RecordType Id
    for (RecordType rt: [SELECT Id from RecordType where DeveloperName = 'EA_Proposal_Webform' and SObjectType = 'Lead'])
    {
      recordTypeId = rt.Id;
    }
    //Set the owner to the Grantee Portal User
    Id ProfileId;
    for (Profile p: [SELECT Id FROM Profile WHERE Name ='Grantee Portal User'])
    {
      ProfileId = p.Id;
    }
    User validUser;
    for (User u: [SELECT Id FROM User WHERE IsActive = true AND ProfileId =: ProfileId LIMIT 1])
    {
      validUser = u;
    }
    if (validUser != null)
    {
          Lead l = new Lead();
          l.FirstName = 'Justin';
          l.LastName = 'Padilla';
          l.Company = 'Rainmaker-LLC';
          l.Email = 'testlkjert489@rainmaker-llc.com';
          l.Phone = '410-555-5555';
        l.Project_Name__c='myprojecttesting';
          l.State = 'MD';
          l.RecordTypeId = recordTypeId;
          l.OwnerId = validUser.Id;
          l.Straight_to_Proposal__c = true;
          insert(l);
    }
    }
}