@isTest
public class TestMRAPage{
    public static testmethod void TestMethod1(){
        //User u = [select id, email, firstname, lastname from user where user.id = :userinfo.getuserid()];
        // User u = [Select u.IsPortalEnabled, u.id, u.contactid, u.IsActive,u.profile.name From User u where u.isActive=true and  u.profile.name ='PCORI Community Partner' limit 1];
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt; 
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
        insert con;  
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con.Id,
                             timezonesidkey='America/New_York', username='testerXXI@noemail.com');
        
        insert user;
        System.RunAs(user) {
            Merit_Reviewer_Application__c a = new Merit_Reviewer_Application__c();
            a.Submitted__c = false;
            a.State__c = 'Alabama';
            a.Reviewer_Role__c = 'Patient';
            a.Required_Trainings__c = 'Yes';
            a.Program_Preference__c = 'Addressing Disparities';
            a.Population_Ranking__c = 'test';
            a.Population_Expertise__c='Children (0-12 years);Adolescents (13-18 years);Young adults (18-21 years);Older adults (65 years and older);';
            a.Personal_Statement__c='test';
            a.Online_Downloading__c = 'No';
            a.Methodologic_Ranking__c='test';
            a.Methodologic_Expertise__c='Methods related to systematic reviews;Methods related to research prioritization;';
            a.How_did_you_hear_about_PCORI__c='Joined PCORI email list';
            a.Highest_Level_of_Education__c='Professional school degree or Doctorate degree (MD, DDC, JD, PhD, EdD, etc.)';
            a.Highest_Degree_of_Education__c='MD';
            a.Healthcare_Ranking__c='test';
            a.Healthcare_Expertise__c='Access to care';
            a.Federal_Employee__c='Yes';
            a.Expertise_in_Diseases_Conditions__c='Blood disorders;Cancer;Cardiac health;Chronic conditions;';
            a.Disease_Condition_Ranking__c='test';
            a.Current_Position_or_Title__c='test';
            a.Current_Employer__c='test';       
            a.Internet_Access__c='yes';
            a.City__c='Vienna';
            a.Application_Status__c='Ready for Review';
            insert a; 
            Test.startTest();
            
            PageReference myVfPage = Page.MRAPage;
            Test.setCurrentPage(myVfPage);
            ApexPAges.StandardController sc = new ApexPages.StandardController(a);
            MRAController testController = new MRAController(sc);
            testController.attachment.Name='Test';
            testController.attachment.Body=Blob.valueOf('Unit Test Attachment Body');
            testController.getOptions();testController.getOptions1();testController.getOptions2();testController.getOptions3();
            testController.getOptions4();testController.sReviewerName='';
            testController.getItems();testController.getItems1();testController.getItems3();testController.upload();testController.getexpertise();
            testController.getItems2();testcontroller.setinternet('internet');testcontroller.setonline('online');
            List<String> test1 = new List<String>();
            test1.add('abc');
            test1.add('abcd');
            testcontroller.setpopulation(test1);
            testcontroller.sethearabout(test1);
            testcontroller.sethealthcare(test1);
            testcontroller.setMethodologic(test1);
            testcontroller.setexpertise(test1);            
            testController.saveManager();  testcontroller.previousmethod1();testcontroller.ShowLongTextField();
            a.Submitted__c = true;
            update a;
            testController.Errormsg41=false;
            testController.Errormsg43=false;
            testController.Errormsg51=false;
            testController.Errormsg53=false;
            testController.ShowLongText1=false;
            testController.ShowLongText2=false;
            testController.ShowLongText3=false;
            testController.ShowLongText4=false;    
            testController.submit();
            testcontroller.ShowLongTextField();
            testcontroller.ResumeApp='true';
            a.Population_Expertise__c='Individuals with rare diseases';
            a.How_did_you_hear_about_PCORI__c='Other';
            a.Internet_Access__c='';
            a.Online_Downloading__c='';
            update a;
            MRAController testController3 = new MRAController(sc);
            testcontroller3.diseaseranking='';
            testcontroller3.populationranking='';
            testcontroller3.healthcareranking='';
            testcontroller3.methodologicranking='';
            testcontroller3.personalstatement='';
            testcontroller3.program='';
            testcontroller3.trainings='';
            //testcontroller3.Internet='';
            //testcontroller3.online='';
            List<String> exptest=new List<String>();
            testcontroller3.savemanager();
            testcontroller3.submit();
            a.Population_Expertise__c='Individuals whose genetic make-up affects their medical outcomes';
            a.How_did_you_hear_about_PCORI__c='';
            update a;
            MRAController testController4 = new MRAController(sc);
            testcontroller4.submit();
        }
    }
    
    static testMethod void testMethod2()
    {
        //User u = [Select u.IsPortalEnabled, u.id, u.contactid, u.IsActive,u.profile.name From User u where u.isPortalEnabled!=null and u.isActive=true and u.profile.name ='PCORI Community Partner' limit 1];
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt; 
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
        insert con;  
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con.Id,
                             timezonesidkey='America/New_York', username='testerXXII@noemail.com');
        
        insert user;
        
        System.RunAs(user) {
            
            Merit_Reviewer_Application__c a = new Merit_Reviewer_Application__c();
            a.Submitted__c = false;
            a.State__c = 'Alabama';
            a.Reviewer_Role__c = 'Stakeholder';
            a.Required_Trainings__c = 'Yes';
            a.Program_Preference__c = 'Addressing Disparities';
            a.Population_Ranking__c = 'test';
            //a.Population_Expertise__c='Children (0-12 years);Adolescents (13-18 years);Young adults (18-21 years);Older adults (65 years and older);Racial and ethnic minority groups ';
            a.Personal_Statement__c='test';
            a.Online_Downloading__c = 'No';
            a.Methodologic_Ranking__c='test';
            a.Methodologic_Expertise__c='Methods related to systematic reviews;Methods related to research prioritization;';
            a.How_did_you_hear_about_PCORI__c='Joined PCORI email list';
            a.Highest_Level_of_Education__c='Master’s degree (MA, MS, MENG, MSW, etc.)';
            a.Highest_Degree_of_Education__c='RDH';
            a.Healthcare_Ranking__c='test';
            a.Healthcare_Expertise__c='Access to care';
            a.Federal_Employee__c='Yes';
            a.Expertise_in_Diseases_Conditions__c='Blood disorders;Cancer;Cardiac health;Chronic conditions;';
            a.Disease_Condition_Ranking__c='test';
            a.Current_Position_or_Title__c='test';
            a.Current_Employer__c='test';
            a.Internet_Access__c='yes';
            a.City__c='test';
            a.Application_Status__c='Draft';
            //a.Population_Expertise__c='Individuals with special healthcare needs, including individuals with disabilities';
            a.Population_Expertise__c='Racial and ethnic minority groups';
            insert a; 
            Test.startTest();
            
            PageReference myVfPage = Page.MRAPage;
            Test.setCurrentPage(myVfPage);
            ApexPAges.StandardController sc = new ApexPages.StandardController(a);
            MRAController testController = new MRAController(sc);
            testcontroller.flag=true;
            testController.attachment.Name='Test';
            testController.attachment.Body=Blob.valueOf('Unit Test Attachment Body');
            testController.getOptions();testController.getOptions1();testController.getOptions2();testController.getOptions3();
            testController.getOptions4(); testController.sReviewerName='';
            testController.getItems();testController.getItems1();testController.getItems3();testController.upload();testController.getexpertise();
            testController.getItems2();testcontroller.setinternet('internet');testcontroller.setonline('online');
            testController.saveManager(); testcontroller.previousmethod1();testcontroller.ShowLongTextField();
            Attachment att = new Attachment();
            att.Name='test';
            att.Body=Blob.valueOf('Unit Test Attachment Body');
            att.OwnerId= UserInfo.getUserId();
            att.ParentId = testController.MRPID;
            insert att;
            testController.selectedAttachmentId=att.id;
            testController.removeMember();
            a.Submitted__c = true;
            update a;
            testController.Errormsg41=false;
            testController.Errormsg43=false;
            testController.Errormsg51=false;
            testController.Errormsg53=false;
            testController.submit();
            testController.saveManager();  
            testcontroller.previousmethod1();
            ApexPages.currentPage().getParameters().put('mypar','Options1');       
            // testcontroller.ShowLongTextField();
            List<String> emplist = new List<String>();
            ApexPages.currentPage().getParameters().put('mypar','Options2');       
            testcontroller.ShowLongTextField();
            ApexPages.currentPage().getParameters().put('mypar','Options3');       
            testcontroller.ShowLongTextField();
            ApexPages.currentPage().getParameters().put('mypar','Options4');       
            testcontroller.ShowLongTextField();
            ApexPages.currentPage().getParameters().put('mypar','Options');       
            testcontroller.ShowLongTextField();
            
            a.Population_Expertise__c='Individuals with special healthcare needs, including individuals with disabilities';
            update a;
            MRAController testController1 = new MRAController(sc);
            testcontroller1.submit();
            a.Population_Expertise__c='Individuals with multiple chronic diseases';
            update a;
            MRAController testController2 = new MRAController(sc);
            testcontroller2.submit();
            Test.stopTest(); 
            
        }
    }
    
    static testMethod void testMethod3()
    {
        // User u = [Select u.IsPortalEnabled, u.id, u.contactid, u.IsActive,u.profile.name From User u where u.isPortalEnabled!=null and u.isActive=true and u.profile.name ='PCORI Community Partner' limit 1];
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt; 
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
        insert con;  
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con.Id,
                             timezonesidkey='America/New_York', username='t2esterXXIII@noemail.com');
        
        insert user;        
        System.RunAs(user) {
            
            Merit_Reviewer_Application__c a = new Merit_Reviewer_Application__c();
            a.Submitted__c = false;
            a.State__c = 'Alabama';            
            a.Required_Trainings__c = 'Yes';
            a.Program_Preference__c = 'Addressing Disparities';
            a.Population_Ranking__c = 'test';
            //a.Population_Expertise__c='';
            a.Population_Expertise__c='Children (0-12 years);Adolescents (13-18 years);Young adults (18-21 years);Older adults (65 years and older);Racial and ethnic minority groups ;Individuals with special healthcare needs, including individuals with disabilities ;Individuals with multiple chronic diseases;Individuals with rare diseases;Individuals whose genetic make-up affects their medical outcomes ;';          
            a.Personal_Statement__c='';
            a.Online_Downloading__c = '';
            a.Methodologic_Ranking__c='';
            a.Methodologic_Expertise__c='';
            a.How_did_you_hear_about_PCORI__c='Other;';
            a.Highest_Level_of_Education__c='Master’s degree (MA, MS, MENG, MSW, etc.)';
            a.Highest_Degree_of_Education__c='RDH';
            a.Healthcare_Ranking__c='';
            a.Healthcare_Expertise__c='';
            a.Federal_Employee__c='';
            a.Expertise_in_Diseases_Conditions__c='';
            a.Disease_Condition_Ranking__c='';
            a.Current_Position_or_Title__c='';
            a.Current_Employer__c='';
            a.Reviewer_Role__c = 'Scientist';
            a.Internet_Access__c='';
            a.City__c='';
            a.Application_Status__c='Ready for Review';
            a.Submitted__c=true;
            insert a; 
            Test.startTest();
            
            PageReference myVfPage = Page.MRAPage;
            Test.setCurrentPage(myVfPage);
            ApexPAges.StandardController sc = new ApexPages.StandardController(a);
            MRAController testController = new MRAController(sc);
            
            testController.getOptions();testController.getOptions1();testController.getOptions2();testController.getOptions3();
            testController.getOptions4();
            testController.getItems();testController.getItems1();testController.getItems3();testController.getexpertise();testController.getInternet();testController.getonline();
            testController.getItems2();testController.sReviewerName='';testcontroller.setinternet('internet');testcontroller.setonline('online');
            testController.getPopulation();testController.getMethodologic();testController.getHealthcare();testController.gethearabout();
            testController.saveManager(); testcontroller.previousmethod1();testcontroller.ShowLongTextField();
            
            Attachment att = new Attachment();
            att.Name='test';
            att.Body=Blob.valueOf('Unit Test Attachment Body');
            att.OwnerId= UserInfo.getUserId();
            att.ParentId = testController.MRPID;
            insert att;
            testController.selectedAttachmentId=att.id;
            
            testController.removeMember();
            a.Submitted__c = true;
            update a;
            testController.Errormsg41=false;
            testController.Errormsg43=false;
            testController.Errormsg51=false;
            testController.Errormsg53=false;
            testController.bRacial = false;
            testController.bIndi1=false;
            testController.bIndi2=false;
            testController.bIndi3=false;
            testController.bIndi4=false;
            testController.bOthers=false;
            ApexPages.currentPage().getParameters().put('ShowValuee','Racial and ethnic minority groups ');
            ApexPages.currentPage().getParameters().put('HideValuee','Racial and ethnic minority groups ');
            testController.ShowValue();
            testController.HideValue();
            ApexPages.currentPage().getParameters().put('ShowValuee','Individuals with special healthcare needs, including individuals with disabilities ');
            ApexPages.currentPage().getParameters().put('HideValuee','Individuals with special healthcare needs, including individuals with disabilities ');
            testController.ShowValue();
            testController.HideValue();
            ApexPages.currentPage().getParameters().put('ShowValuee','Individuals with multiple chronic diseases');
            ApexPages.currentPage().getParameters().put('HideValuee','Individuals with multiple chronic diseases');
            testController.ShowValue();
            testController.HideValue();
            ApexPages.currentPage().getParameters().put('ShowValuee','Individuals with rare diseases');
            ApexPages.currentPage().getParameters().put('HideValuee','Individuals with rare diseases');
            testController.ShowValue();
            testController.HideValue();
            ApexPages.currentPage().getParameters().put('ShowValuee','Individuals whose genetic make-up affects their medical outcomes ');
            ApexPages.currentPage().getParameters().put('HideValuee','Individuals whose genetic make-up affects their medical outcomes ');
            testController.ShowValue();
            testController.HideValue();
            ApexPages.currentPage().getParameters().put('ShowValuee','Other');
            ApexPages.currentPage().getParameters().put('HideValuee','Other');
            testController.ShowValue();    
            //testController.ShowValue();
            testController.HideValue();
            testController.updateRacial();
            testController.submit();
            testcontroller.previousmethod1();
            ApexPages.currentPage().getParameters().put('mypar','Options1');       
            //testcontroller.ShowLongTextField();
            // Do something
            Test.stopTest(); 
        }
    }
    
    static testMethod void testMethod4()
    {
        // User u = [Select u.IsPortalEnabled, u.id, u.contactid, u.IsActive,u.profile.name From User u where u.isPortalEnabled!=null and u.isActive=true and u.profile.name ='PCORI Community Partner' limit 1];
        User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
        Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
        insert acnt; 
        Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                                  Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
        insert con;  
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
                             ContactId = con.Id,
                             timezonesidkey='America/New_York', username='testerXXIV@noem2ail.com');
        
        insert user;
        System.RunAs(user) {
            
            Merit_Reviewer_Application__c a = new Merit_Reviewer_Application__c();
            a.Submitted__c = false;
            a.State__c = 'Alabama';    
            a.Required_Trainings__c = 'Yes';
            a.Program_Preference__c = 'Addressing Disparities';
            a.Population_Ranking__c = '';
            a.Population_Expertise__c='None of the above';
            a.Personal_Statement__c='';
            a.Online_Downloading__c = '';
            a.Methodologic_Ranking__c='';
            a.Methodologic_Expertise__c='None of the above';
            a.How_did_you_hear_about_PCORI__c='Other;';
            a.Highest_Level_of_Education__c='Master’s degree (MA, MS, MENG, MSW, etc.)';
            a.Highest_Degree_of_Education__c='RDH';
            a.Healthcare_Ranking__c='';
            a.Healthcare_Expertise__c='None of the above';
            a.Federal_Employee__c='';
            a.Expertise_in_Diseases_Conditions__c='None of the above';
            a.Disease_Condition_Ranking__c='';
            a.Current_Position_or_Title__c='';
            a.Current_Employer__c='';
            a.Reviewer_Role__c = 'Scientist';
            a.Internet_Access__c='';
            a.City__c='';
            a.Application_Status__c='Ready for Review';
            a.Submitted__c=true;
            insert a; 
            Test.startTest();
            
            PageReference myVfPage = Page.MRAPage;
            Test.setCurrentPage(myVfPage);
            ApexPAges.StandardController sc = new ApexPages.StandardController(a);
            MRAController testController = new MRAController(sc);
            testcontroller.ResumeApp='true';
            
            Attachment att = new Attachment();
            att.Name='test';
            att.Body=Blob.valueOf('Unit Test Attachment Body');
            att.OwnerId= UserInfo.getUserId();
            att.ParentId = testController.MRPID;
            insert att;
            testController.selectedAttachmentId=att.id;
            testController.removeMember();
            a.Submitted__c = true;
            update a;
            testController.Errormsg41=false;
            testController.Errormsg43=false;
            testController.Errormsg51=false;
            testController.Errormsg53=false;
            testController.bRacial = false;
            testController.bIndi1=false;
            testController.bIndi2=false;
            testController.bIndi3=false;
            testController.bIndi4=false;
            testController.bOthers=false;
            ApexPages.currentPage().getParameters().put('ShowValuee','Racial and ethnic minority groups');
            testController.ShowValue();
            //ApexPages.currentPage().getParameters().put('id','a1b18000000A8BXAA0');
            testcontroller.mrpEdit();
            
            //testcontroller.MRPID='a1b18000000A8BXAA0';
            testController.submit();
            testcontroller.savemanager();
            testcontroller.previousmethod1();
            ApexPages.currentPage().getParameters().put('mypar','Options1');       
            //testcontroller.ShowLongTextField();
            // Do something
            Test.stopTest(); 
        }
    }
}