@istest
public class testInterestedinPeerReviewController {
    
    public static testmethod void submit(){
        InterestedinPeerReviewController ac= new InterestedinPeerReviewController();
        ac.emaill='rajat@test.com';
        List<SelectOption> testoptions = new List<SelectOption>{};
        testoptions = ac.getitemsInterested();
        system.debug(testoptions);
        system.assertequals(2,testoptions.size());
        ac.submit();
    }  
    
    public static testmethod void submit1(){
        InterestedinPeerReviewController ac= new InterestedinPeerReviewController();
        ac.emaill='';
        List<SelectOption> testoptions = new List<SelectOption>{};
        testoptions = ac.getitemsInterested();
        system.debug(testoptions);
        system.assertequals(2,testoptions.size());
        ac.submit();     
    }
}