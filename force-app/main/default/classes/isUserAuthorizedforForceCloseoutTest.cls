@isTest
public class isUserAuthorizedforForceCloseoutTest {
    private static testMethod void testAuthorization(){
        
        list<User> usr=TestDataFactory.getstandardUserTestRecords(1);
            list<Opportunity> opp= TestDataFactory.getresearchawardsopportunitytestrecords(1,5,1);
        
        Contract_Checklist__c cc = new Contract_Checklist__c();
            cc.Was_this_project_terminated__c = 'No';
            cc.Contract__c = opp[0].Id;
            //cc.All_Financial_Matters_Resolved__c = 'Yes';
            cc.recordtypeid='01239000000QUBx';
            
             cc.Lay_Abstract__c='Yes';  
            cc.Closeout_Checklist_Status__c = 'Ready for Closeout';
            cc.Conflict_of_Interest_Form_Closeout__c = 'Yes';
            cc.Fully_Executed_Contract__c = 'Yes';
            CC.Conflict_of_Interest_Form_Closeout__c='Yes';
            CC.Fully_Executed_Contract__c='Yes'; 
            CC.Final_Progress_Report__c='Yes'; 
            CC.Final_Research_Report__c='Yes';
            CC.Finalized_Budget__c='Yes';
            CC.Finalized_Budget_Justification__c='Yes'; 
            CC.Finalized_Milestone_Schedule__c='Yes';
            CC.Finalized_Project_Plan__c='Yes'; 
            CC.Fully_Executed_Modification_1__c='Yes'; 
            CC.Fully_Executed_Modification_2__c='Yes';
            CC.Fully_Executed_Modification_3__c='Yes';
            CC.Fully_Executed_Modification_4__c='Yes';
            CC.Fully_Executed_Modification_5__c='Yes';
            CC.Special_Progress_Reports_and_PO_Approval__c='Yes'; 
            CC.Draft_Final_Research_Report_Submitted__c='Yes';
            CC.Report_10_with_PO_Approval__c='Yes';
            CC.Report_1_with_PO_Approval__c='Yes'; 
            CC.Report_2_with_PO_Approval__c='Yes';
            CC.Report_3_with_PO_Approval__c='Yes';
            CC.Report_4_with_PO_Approval__c='Yes';
            CC.Report_5_with_PO_Approval__c='Yes';
            CC.Report_6_with_PO_Approval__c='Yes';
            CC.Report_7_with_PO_Approval__c='Yes';
            CC.Report_8_with_PO_Approval__c='Yes';
            CC.Report_9_with_PO_Approval__c='Yes';

            insert cc;
        try{
         Contract_Checklist__c ccc1 = new Contract_Checklist__c();
            ccc1.Was_this_project_terminated__c = 'No';
            ccc1.Contract__c = opp[0].Id;
            //cc.All_Financial_Matters_Resolved__c = 'Yes';
            ccc1.recordtypeid='01239000000QUBx';
            
             ccc1.Lay_Abstract__c='Yes';  
            ccc1.Closeout_Checklist_Status__c = 'Ready for Closeout';
        insert ccc1;
        }catch(exception e){
            
        }
         Contract_Checklist__c cc1 = new Contract_Checklist__c();
        cc1.Contract__c=opp[0].Id;
cc1.Are_all_cumulative_totals_correct__c='Yes';
cc1.Is_the_FE_budget_correct__c='Yes';
cc1.Is_the_IDC_rate_correct__c='Yes';
cc1.Finance_Checklist_Status__c='In Progress';
cc1.If_FFP_are_all_payments_paid__c='Yes';
cc1.If_IC_is_there_a_remaining_balance__c='Yes';
cc1.Is_the_POP_correct__c='Yes';
cc1.All_credits_resolved__c='Yes';
cc1.recordtypeid='01239000000QUBy';
        insert cc1;
        try{
        Contract_Checklist__c cc2 = new Contract_Checklist__c();
        cc2.Contract__c=opp[0].Id;
cc2.Are_all_cumulative_totals_correct__c='Yes';
cc2.Is_the_FE_budget_correct__c='Yes';
cc2.Is_the_IDC_rate_correct__c='Yes';
cc2.Finance_Checklist_Status__c='In Progress';
cc2.If_FFP_are_all_payments_paid__c='Yes';
cc2.If_IC_is_there_a_remaining_balance__c='Yes';
cc2.Is_the_POP_correct__c='Yes';
cc2.All_credits_resolved__c='Yes';
cc2.recordtypeid='01239000000QUBy';
        insert cc2;
        }catch(exception e){
            
        }
        try{
        cc.Closeout_Checklist_Status__c = 'Completed';
            update cc;
        }catch(exception e){
            
        }
       
        
        
            System.runAs(usr[0]){
                 Test.startTest();
                try{
                    cc.Forced_Closeout__c = TRUE;
                    cc.Closeout_Letter_Counter_Signed__c = 'Yes';
                    update cc;
                }
                catch(Exception e){
                    Boolean expectedExceptionThrown =  e.getMessage().contains('You are not authorized to set the status to Forced Closeout') ? true : false;
                }
                test.stopTest();
            }
            
    }
    

    private static testMethod void testAuthorization2(){
        KeyValueStore__c kv = new KeyValueStore__c(Name='ProjectInvoiceCalculation', Boolean__c = true);
        insert kv;
        test.startTest();
        list<User> usr=TestDataFactory.getstandardUserTestRecords(1);
            list<Opportunity> opp= TestDataFactory.getresearchawardsopportunitytestrecords(1,5,1);
        Project_Invoice_Calculation__c pic= new Project_Invoice_Calculation__c();
    pic.Project__c=opp[0].Id;
    pic.Indirect_Costs_TRP_Cumulative__c=100.00;
    pic.Indirect_Costs_TRP_Budget__c=100.00;
    insert pic;
        Contract_Checklist__c cc = new Contract_Checklist__c();
            cc.Was_this_project_terminated__c = 'No';
            cc.Contract__c = opp[0].Id;
            //cc.All_Financial_Matters_Resolved__c = 'Yes';
            cc.recordtypeid='01239000000QUBx';
            
             cc.Lay_Abstract__c='Yes';  
            cc.Closeout_Checklist_Status__c = 'Ready for Closeout';
            cc.Conflict_of_Interest_Form_Closeout__c = 'Yes';
            cc.Fully_Executed_Contract__c = 'Yes';
            CC.Conflict_of_Interest_Form_Closeout__c='Yes';
            CC.Fully_Executed_Contract__c='Yes'; 
            CC.Final_Progress_Report__c='Yes'; 
            CC.Final_Research_Report__c='Yes';
            CC.Finalized_Budget__c='Yes';
            CC.Finalized_Budget_Justification__c='Yes'; 
            CC.Finalized_Milestone_Schedule__c='Yes';
            CC.Finalized_Project_Plan__c='Yes'; 
            CC.Fully_Executed_Modification_1__c='Yes'; 
            CC.Fully_Executed_Modification_2__c='Yes';
            CC.Fully_Executed_Modification_3__c='Yes';
            CC.Fully_Executed_Modification_4__c='Yes';
            CC.Fully_Executed_Modification_5__c='Yes';
            CC.Special_Progress_Reports_and_PO_Approval__c='Yes'; 
            CC.Draft_Final_Research_Report_Submitted__c='Yes';
            CC.Report_10_with_PO_Approval__c='Yes';
            CC.Report_1_with_PO_Approval__c='Yes'; 
            CC.Report_2_with_PO_Approval__c='Yes';
            CC.Report_3_with_PO_Approval__c='Yes';
            CC.Report_4_with_PO_Approval__c='Yes';
            CC.Report_5_with_PO_Approval__c='Yes';
            CC.Report_6_with_PO_Approval__c='Yes';
            CC.Report_7_with_PO_Approval__c='Yes';
            CC.Report_8_with_PO_Approval__c='Yes';
            CC.Report_9_with_PO_Approval__c='Yes';

            insert cc;
        
       
        try{
        cc.Closeout_Checklist_Status__c = 'Completed';
            update cc;
        }catch(exception e){
            
        }
       test.stopTest();
        }
    
        private static testMethod void testAuthorization3(){
        KeyValueStore__c kv = new KeyValueStore__c(Name='ProjectInvoiceCalculation', Boolean__c = true);
        insert kv;
        
        list<User> usr=TestDataFactory.getstandardUserTestRecords(2);
            list<Opportunity> opp= TestDataFactory.getresearchawardsopportunitytestrecords(1,5,1);
        test.startTest();
        Project_Invoice_Calculation__c pic= new Project_Invoice_Calculation__c();
    pic.Project__c=opp[0].Id;
    pic.Indirect_Costs_TRP_Cumulative__c=100.00;
    pic.Indirect_Costs_TRP_Budget__c=100.00;
    insert pic;
        Contract_Checklist__c cc = new Contract_Checklist__c();
            cc.Was_this_project_terminated__c = 'No';
            cc.Contract__c = opp[0].Id;
            //cc.All_Financial_Matters_Resolved__c = 'Yes';
            cc.recordtypeid='01239000000QUBx';
            
             cc.Lay_Abstract__c='Yes';  
            cc.Closeout_Checklist_Status__c = 'Ready for Closeout';
            cc.Conflict_of_Interest_Form_Closeout__c = 'Yes';
            cc.Fully_Executed_Contract__c = 'Yes';
            CC.Conflict_of_Interest_Form_Closeout__c='Yes';
            CC.Fully_Executed_Contract__c='Yes'; 
            CC.Final_Progress_Report__c='Yes'; 
            CC.Final_Research_Report__c='Yes';
            CC.Finalized_Budget__c='Yes';
            CC.Finalized_Budget_Justification__c='Yes'; 
            CC.Finalized_Milestone_Schedule__c='Yes';
            CC.Finalized_Project_Plan__c='Yes'; 
            CC.Fully_Executed_Modification_1__c='Yes'; 
            CC.Fully_Executed_Modification_2__c='Yes';
            CC.Fully_Executed_Modification_3__c='Yes';
            CC.Fully_Executed_Modification_4__c='Yes';
            CC.Fully_Executed_Modification_5__c='Yes';
            CC.Special_Progress_Reports_and_PO_Approval__c='Yes'; 
            CC.Draft_Final_Research_Report_Submitted__c='Yes';
            CC.Report_10_with_PO_Approval__c='Yes';
            CC.Report_1_with_PO_Approval__c='Yes'; 
            CC.Report_2_with_PO_Approval__c='Yes';
            CC.Report_3_with_PO_Approval__c='Yes';
            CC.Report_4_with_PO_Approval__c='Yes';
            CC.Report_5_with_PO_Approval__c='Yes';
            CC.Report_6_with_PO_Approval__c='Yes';
            CC.Report_7_with_PO_Approval__c='Yes';
            CC.Report_8_with_PO_Approval__c='Yes';
            CC.Report_9_with_PO_Approval__c='Yes';

            insert cc;
        
        Contract_Checklist__c cc1 = new Contract_Checklist__c();
        cc1.Contract__c=opp[0].Id;
cc1.Are_all_cumulative_totals_correct__c='Yes';
cc1.Is_the_FE_budget_correct__c='Yes';
cc1.Is_the_IDC_rate_correct__c='Yes';
cc1.Finance_Checklist_Status__c='In Progress';
cc1.If_FFP_are_all_payments_paid__c='Yes';
cc1.If_IC_is_there_a_remaining_balance__c='Yes';
cc1.Is_the_POP_correct__c='Yes';
cc1.All_credits_resolved__c='Yes';
cc1.recordtypeid='01239000000QUBy';
          
        insert cc1;
     cc1.Finance_Checklist_Status__c='Completed';  
            update cc1;
        cc.Closeout_Checklist_Status__c = 'Completed';
            update cc;
     
            
       
       test.stopTest();
        }

    
}