/*
Application Inventory#: AI-0919
Requirement ID: 16895
*/

global class UpdatePIR{
    webService static void UpdateStatus(String pId){
        PIR__c pir = [SELECT PIR_Status__c FROM PIR__c WHERE Id = :pId];
        pir.PIR_Status__c = 'Response Received';
        update pir;
    }
}