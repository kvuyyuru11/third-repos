@isTest
public class AssignPermissionSetTest {
    private static testMethod void testPermissionSet(){
        List<User> usrlst = [SELECT Id FROM User Where IsActive=True LIMIT 2];
        List<Id> idlst = new List<Id>();
        for(User usr :usrlst){
            idlst.add(usr.Id);
        }
        AssignPermissionSet.assign(idlst);
    }
}