/* This class belongs to the trigger TaskEmailUpdate. 
This class will update Email Fields in the Task only when the Task Object is edited or created.*/

Public  with sharing class UpdateEmailTask{

//Construtor
    Public UpdateEmailTask(){
    }

   Public void UpdateEmailonTask (List<Task> tList){  /*Method to update email fields in the task*/
   
         Set <Id> parentIds = new set<Id>();
    for(Task tsk : tList) {
        parentIds.add(tsk.WhatId);
    }// end for loop
    
    Map<String,Id> recTypeNameWithIdMap=new Map<String,Id>();
    for(Schema.RecordTypeInfo recInfo : Schema.getGlobalDescribe().get('Task').getDescribe().getRecordTypeInfosByName().values()){
         recTypeNameWithIdMap.put(recInfo.getName(),recInfo.getRecordTypeId());
    }
    
     
   List<Opportunity> oppSOQLList =[SELECT Id, Name,  Program_Associate__r.Email,Program_Officer__r.Email,Contract_Administrator__r.Email,Contract_Coordinator__r.Email FROM Opportunity where id IN : parentIds LIMIT 50000]; 
   List<FGM_Base__Benchmark__c> msSOQLList =[SELECT id,Email_Program_Associate__c, Email_Program_Officer__c, Email_Contract_Administrator__c, Email_Contract_Coordinator__c FROM FGM_Base__Benchmark__c WHERE Id IN : parentIds LIMIt 50000];

   Map<Id, FGM_Base__Benchmark__c> msmap = new Map<Id, FGM_Base__Benchmark__c>();
   Map<Id, Opportunity> oppmap = new Map<Id, Opportunity>();

  for(Opportunity opp: oppSOQLList)
  {
     oppmap.put(opp.id, opp);

  }//end for loop
  
  for(FGM_Base__Benchmark__c mst: msSOQLList)
  {
     msmap.put(mst.id, mst);

  }//end for loop   
  
  try{
  
       for(Task tskl: tList)
       {
     
         if(tskl.RecordTypeId == recTypeNameWithIdMap.get('Project Task(SOW11)')) //ProjectTaskId)
         {
                       
            tskl.Email_Program_Associate__c = oppmap.get(tskl.WhatId).Program_Associate__r.Email;                   
            tskl.Email_Program_Officer__c = oppmap.get(tskl.WhatId).Program_Officer__r.Email;
            tskl.Email_Contract_Administrator__c = oppmap.get(tskl.WhatId).Contract_Administrator__r.Email;
            tskl.Email_Contract_Coordinator__c = oppmap.get(tskl.WhatId).Contract_Coordinator__r.Email;
            
          } else if(tskl.RecordTypeId == recTypeNameWithIdMap.get('Milestone Task(SOW11)')) //MilestoneTaskId)
          {
            
            tskl.Email_Program_Associate__c = msmap.get(tskl.WhatId).Email_Program_Associate__c;            
            tskl.Email_Program_Officer__c = msmap.get(tskl.WhatId).Email_Program_Officer__c;
            tskl.Email_Contract_Administrator__c = msmap.get(tskl.WhatId).Email_Contract_Administrator__c;
            tskl.Email_Contract_Coordinator__c = msmap.get(tskl.WhatId).Email_Contract_Coordinator__c;
            
           }  //end if
                         
        }//end for loop
        
     }catch(DmlException e) {
           System.debug('The following exception has occurred: ' + e.getMessage());       
       }Catch(Exception e)  {
           System.debug('The following exception has occurred: ' + e.getMessage());
       }// try catch block
       
  }//end Method
    
}//end UpdateEmailTask