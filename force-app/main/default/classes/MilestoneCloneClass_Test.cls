@isTest
private class MilestoneCloneClass_Test {
	
	@isTest static void testMilestoneCloneClass() {
		Id r1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RA Applications').getRecordTypeId();
		Id r2 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RA Supplemental Funding').getRecordTypeId();
		Id pcoriCommunityPartID = [SELECT Id FROM Profile WHERE name = 'PCORI Community Partner'].Id;

		Account a = new Account();
		a.Name = 'Acme1';
		insert a;

		List<Contact> cons = new List<Contact>();
		Contact con = new Contact(LastName = 'testCon', AccountId = a.Id);
		cons.add(con);
		Contact con1 = new Contact(LastName = 'testCon1', AccountId = a.Id);
		cons.add(con1);
		insert cons;
		
		User user = new User(Alias = 'test123', Email = 'test123fvb@noemail.com',
							 Emailencodingkey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',    
							 LocaleSidKey = 'en_US', ProfileId = pcoriCommunityPartID , Country='United States',IsActive = true,   
							 ContactId = con.Id, CommunityNickname = 'test12',
							 TimeZoneSidKey='America/New_York', UserName = 'testerfvb1@noemail.com');

		User user1 = new User(Alias = 'test1234', Email = 'test1234fvb@noemail.com',
							  Emailencodingkey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',    
							  LocaleSidKey = 'en_US', ProfileId = pcoriCommunityPartID , Country='United States',IsActive = true,   
							  ContactId = con1.Id, CommunityNickname = 'test12345',
							  TimeZoneSidKey='America/New_York', UserName = 'testerfvb123@noemail.com');

		List<User> users = new List<User>();
		users.add(user);
		users.add(user1);
		insert users;
		
		Cycle__c c = new Cycle__c();
		c.Name = 'testcycle';
		c.COI_Due_Date__c = date.valueof(System.now());
		insert c;

		Campaign camp = new Campaign();
		camp.Name = 'ResearchCamp';
		camp.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('PCORI Portal').getRecordTypeId();
		camp.Cycle__c = c.Id;
		camp.IsActive = true;
		camp.FGM_Portal__Visibility__c = 'Public';
		camp.Status = 'In Progress';
		camp.StartDate = System.today();
		camp.EndDate = System.today() + 7;
		camp.FGM_Base__Board_Meeting_Date__c = System.today() + 14;
		insert camp;

		opportunity o1 = new opportunity();
		o1.Awardee_Institution_Organization__c=a.Id;
		o1.CampaignId = camp.Id;
		o1.RecordTypeId = r1;
		o1.Full_Project_Title__c='test';
		o1.Project_Name_off_layout__c='test';
		o1.CloseDate = Date.today() + 14;
		o1.Application_Number__c='RA-1234';
		o1.StageName = 'Pending PI Application';
		o1.External_Status__c = 'Draft';
		o1.Name='test';
		o1.PI_Name_User__c = user.id;
		o1.AO_Name_User__c = user1.id;
		o1.Project_Start_Date__c = system.Today();
		o1.Bypass_Flow__c = true;
		insert o1;

		opportunity o2 = new opportunity();
		o2.Awardee_Institution_Organization__c=a.Id;
		o2.CampaignId = camp.Id;
		o2.RecordTypeId = r1;
		o2.Full_Project_Title__c='test';
		o2.Project_Name_off_layout__c='test';
		o2.CloseDate = Date.today() + 14;
		o2.Application_Number__c='RA-1234';
		o2.StageName = 'Pending PI Application';
		o2.Name='test';
		o2.PI_Name_User__c = user.id;
		o2.AO_Name_User__c = user1.id;
		o2.Project_Start_Date__c = system.Today();
		o2.Parent_Contract__c = o1.id;
		o2.Bypass_Flow__c = true;
		insert o2;

		//create a list of 10 milestones to clone later in web method
		List<FGM_Base__Benchmark__c> milestones = new List<FGM_Base__Benchmark__c>();
		for(Integer i=0; i<5; i++) {
			FGM_Base__Benchmark__c milestone = new FGM_Base__Benchmark__c();
			milestone.RecordTypeId = Schema.SObjectType.FGM_Base__Benchmark__c.getRecordTypeInfosByName().get('Milestone - Deliverable - RA').getRecordTypeId();
			milestone.Milestone_Status__c = 'Not Completed';
			milestone.FGM_Base__Due_Date__c = System.today() + 7;
			milestone.Milestone_Name__c = 'Unit Test ' + i;
			milestone.Milestone_ID2__c = 'A';
			milestone.FGM_Base__Request__c = o1.Id;
			milestone.Bypass_Flow__c = true;
			milestones.add(milestone);
		}
		for(Integer i=0; i<5; i++) {
			FGM_Base__Benchmark__c milestone = new FGM_Base__Benchmark__c();
			milestone.RecordTypeId = Schema.SObjectType.FGM_Base__Benchmark__c.getRecordTypeInfosByName().get('Milestone - Deliverable - RA').getRecordTypeId();
			milestone.Milestone_Status__c = 'Not Completed';
			milestone.FGM_Base__Due_Date__c = System.today() + 7;
			milestone.Milestone_Name__c = 'Unit Test ' + i;
			milestone.Milestone_ID2__c = 'A';
			milestone.FGM_Base__Request__c = o2.Id;
			milestone.Bypass_Flow__c = true;
			milestones.add(milestone);
		}

		//insert milestones for testing
		insert milestones;

		//populate id string
		List<String> mIds = new List<String>();
		for(FGM_Base__Benchmark__c milestone : milestones) {
			mIds.add(milestone.id);
		}

		Test.startTest();

		//insure the milestones have been inserted, then clone them and assert that they have been cloned.
		System.assert(![SELECT Id FROM FGM_Base__Benchmark__c WHERE Id IN : mIds].isEmpty());
		MilestonesCloneCLass.milestonesClone(mIds);
		System.assertEquals(20,[SELECT Id FROM FGM_Base__Benchmark__c WHERE FGM_Base__Request__c = : o1.Id OR FGM_Base__Request__c = : o2.Id].size());

		//make sure that web method can work with an emtpy list
		mIds.clear();
		MilestonesCloneCLass.milestonesClone(mIds);

		Test.stopTest();
	}
	
}