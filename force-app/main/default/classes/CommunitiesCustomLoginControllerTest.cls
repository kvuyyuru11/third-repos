/**
 * An apex page controller that exposes the site login functionality
 */
@IsTest global with sharing class CommunitiesCustomLoginControllerTest {
    @IsTest
    global static void testCommunitiesCustomLoginController () {
        CommunitiesCustomLoginController controller = new CommunitiesCustomLoginController();
        controller.username = 'test@force.com';
        controller.password = 'abcd1234';
        controller.baseURL='https://dev2-pcori.cs52.force.com/engagement/COIBoardofDirectors';
        System.assertEquals(null, controller.login());       
    }     
}