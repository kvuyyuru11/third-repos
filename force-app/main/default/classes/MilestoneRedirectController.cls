public class MilestoneRedirectController {
    public ID UserProfileID;
    public FGM_Base__Benchmark__c md;
    public MilestoneRedirectController(ApexPages.StandardController controller) {
        this.md = (FGM_Base__Benchmark__c)controller.getRecord();
    	UserProfileID=userInfo.getProfileId();
	}
    
	public pagereference redirect(){
        if (UserProfileID==Label.MRA_Profile){
            return new PageReference('/Milestone_PCORIUser_PgLayout?Id=' + md.Id);
        }
        else{
            PageReference pg = new PageReference('/' + md.Id + '/e');
            pg.setRedirect(true);
            return pg;
        }          
	}
}