public with sharing class AP_ApplicationController {
    
    public  Advisory_Panel_Application__c apa{get;set;}
    public contact mycon{get;set;}
    public List<Advisory_Panel_Application__c> myApp;
    public Boolean showMsg1{get;set;}
    public Boolean disb{get;set;}
    public String stipend{get;set;}
    public String Reimbursements{get;set;}
    public List<Attachment> att{get;set;}
    public Id selectedAttachmentId{get;set;}
    public Contact cnt;
    public user us;
    public string style1{get;set;}
    public string style2{get;set;}
    public string style3{get;set;}
    integer x;
    public boolean ShowTextformyprofile{get;set;}
     public boolean ShowTextformyprofilemissing{get;set;}
    // us=[select id,contactid, Lastname from user where id=:userinfo.getUserId()];
    //Id cid = us.contactid;
    
    public AP_ApplicationController(){
          
        style1='';
        style2='';
        style3='';
        att = new List<Attachment>();
        list <Contact> tempContact;
        list <Advisory_Panel_Application__c> tempApp;
        
        us=[select id,contactid, Lastname from user where id=:userinfo.getUserId()];
        Id cid = us.contactid;
        tempContact=[select Id, Name,Accepts_Reimbursements__c,Accepts_Stipend_s__c from contact where Id=:cid ];
        //myApp = [SELECT Id, Able_to_attend_a_one_to_two_day_meeting__c, Contact__c, Last_Name__c, Application_Status_New__c, Abide_by_PCORI_COI_Policy__c from Advisory_Panel_Application__c where CreatedBy.Id =:userinfo.getUserId() and Application_Status_New__c = 'Draft' order by CreatedDate DESC limit 1];
        if(tempContact.size()> 0)
            cnt = tempContact[0];
        
        //get contact object of current portal user
        contact cnt = [select id,Highest_Degree1__c,Accepts_Reimbursements__c,Previous_involvement_with_PCORI__c,Accepts_Stipend_s__c,Gender_Research__c,Exact_Age__c,Salutation,Hispanic_Latino__c,Race__c,Current_Employer__c,Department,Highest_Level_Of_Education__c,Federal_Employee__c from Contact where id=:cid limit 1];
        if(tempContact.size() >0){  
            stipend = cnt.Accepts_Stipend_s__c;
            Reimbursements = cnt.Accepts_Reimbursements__c;
            
        }
        
        apa = new Advisory_Panel_Application__c();
        //get existing app (if exist) for current portal user 
        myApp = [SELECT Id, Able_to_attend_a_one_to_two_day_meeting__c, Contact__c, Last_Name__c, Application_Status_New__c, Abide_by_PCORI_COI_Policy__c from Advisory_Panel_Application__c where CreatedBy.Id =:userinfo.getUserId() and Application_Status_New__c = 'Draft' order by CreatedDate DESC limit 1];
        showMsg1 = true;
        if(myApp.size()>0){
            apa.Abide_by_PCORI_COI_Policy__c= myApp[0].Abide_by_PCORI_COI_Policy__c;
            apa.Able_to_attend_a_one_to_two_day_meeting__c = myApp[0].Able_to_attend_a_one_to_two_day_meeting__c;
            apa.Application_Status_New__c = myApp[0].Application_Status_New__c;
            
            
        }
        
    }
    
    
    
    //Naviagete to next page on the app and save portal user information to user record(s)
   
    
    public pagereference Nextmethod() {
        style1='';
        style2='';
        style3='';
        x=0;
        string poin=userinfo.getUserId();
        string king=poin.substring(0, 15);
        
        user us=[select id,contactid, Lastname from user where id=:userinfo.getUserId()];
        Id cid = us.contactid;
        contact cnt = [select id,Highest_Degree1__c,Previous_involvement_with_PCORI__c,AP_COI_form_attached__c,Accepts_Reimbursements__c,Accepts_Stipend_s__c,Gender_Research__c,Current_Position_or_Title__c,Exact_Age__c,Salutation,Hispanic_Latino__c,Race__c,Current_Employer__c,Department,Highest_Level_Of_Education__c,Degrees__c,Federal_Employee__c from Contact where id=:cid limit 1];
        cnt.Accepts_Stipend_s__c = stipend;
        cnt.Accepts_Reimbursements__c = Reimbursements;
        database.update(cnt);
        
        myApp = [SELECT Id, Able_to_attend_a_one_to_two_day_meeting__c, Contact__c, Last_Name__c, Application_Status_New__c, Abide_by_PCORI_COI_Policy__c from Advisory_Panel_Application__c where CreatedBy.Id =:userinfo.getUserId() and Application_Status_New__c = 'Draft' order by CreatedDate DESC limit 1];
        system.debug('Very Interesting'+myApp.isEmpty());
        
        If (myApp.isEmpty()){
            Advisory_Panel_Application__c saveAPA1 = new Advisory_Panel_Application__c ();
            saveAPA1.Abide_by_PCORI_COI_Policy__c = apa.Abide_by_PCORI_COI_Policy__c;
            
            saveAPA1.Contact__c = us.contactId;
            saveAPA1.Last_Name__c = us.Lastname;
            saveAPA1.Application_Status_new__c='Draft';
            saveAPA1.Gender_del__c =cnt.Gender_Research__c;
            saveAPA1.Permission_ID__c= king;
            saveAPA1.Current_Employer__c=cnt.Current_Employer__c;
            saveAPA1.What_is_your_highest_level_of_education__c=cnt.Highest_Level_Of_Education__c;
            saveAPA1.Highest_Degree_of_Education__c= cnt.Highest_Degree1__c;
            saveAPA1.Current_Position_or_Title__c=cnt.Current_Position_or_Title__c;
            saveAPA1.Previous_involvement_with_PCORI__c=cnt.Previous_involvement_with_PCORI__c;
            cnt.Highest_Level_Of_Education__c=SaveAPA1.What_is_your_highest_level_of_education__c;
            saveAPA1.Permission_ID__c= userinfo.getUserId();
            saveAPA1.Are_you_Hispanic_Latino__c = cnt.Hispanic_Latino__c;
            saveAPA1.Race_Ethnicity__c=cnt.Race__c;
            saveAPA1.Currently_employed_by_the_federal_govt__c=cnt.Federal_Employee__c;
            saveAPA1.Title__c =cnt.Salutation;
            saveAPA1.Age_in_Years__c =cnt.Exact_Age__c;
            
           
               if(stipend== 'None' || stipend==''){
                    style1=label.css_value;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));

                    x=1;
                }
                if (Reimbursements == 'None' ||Reimbursements == '')
                {
                    
                    style2=label.css_value;
       ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.')); 

                    x=1;
                }
               if(cnt.AP_COI_form_attached__c==false){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter a File Name and an Attachment.  Then click Save.'));  
             x=1;
            }
                 if(apa.Abide_by_PCORI_COI_Policy__c == 'No'){
                 style3=label.css_value;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'To be considered for membership on any PCORI Advisory Panel, applicants must agree to abide by PCORI’s Conflict of Interest Policy and publicly disclose any potential conflicts. Unfortunately, as you have answered “No” to the question of whether you would agree to this provision, we are unable to consider you for our Advisory Panels. If you intended to indicate that you would abide by our Conflict of Interest policy, please select “Yes” below.'));            
                x=1;
                }
                if(apa.Abide_by_PCORI_COI_Policy__c == null||apa.Abide_by_PCORI_COI_Policy__c == ''){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.')); 
                    style3=label.css_value;
                    x=1;
                }
            
            
            
            if(x==1){
            return null;
            }
            else{
            insert saveAPA1;
            pagereference pg=new pagereference(label.AP_Application2);
            return pg;
            }
        }
        
         else{
                
               
               if(stipend== 'None' || stipend==''){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.'));
                    style1=label.css_value;
                    x=1;
                }
                if (Reimbursements == 'None' ||Reimbursements== '')
                {
                    
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.')); 
                    style2=label.css_value;
                    x=1;
                }
                if(cnt.AP_COI_form_attached__c==false){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter a File Name and an Attachment.  Then click Save.'));  
             x=1;
            }
                if(apa.Abide_by_PCORI_COI_Policy__c == null||apa.Abide_by_PCORI_COI_Policy__c == ''){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields.')); 
                    style3=label.css_value;
                    x=1;
                }
                 if(apa.Abide_by_PCORI_COI_Policy__c == 'No'){
                 style3=label.css_value;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'To be considered for membership on any PCORI Advisory Panel, applicants must agree to abide by PCORI’s Conflict of Interest Policy and publicly disclose any potential conflicts. Unfortunately, as you have answered “No” to the question of whether you would agree to this provision, we are unable to consider you for our Advisory Panels. If you intended to indicate that you would abide by our Conflict of Interest policy, please select “Yes” below.'));            
                x=1;
                }
           
            if(x==1){
            return null;
            }
            
            else{
              database.update(myapp);
                
                pagereference pg=new pagereference(label.AP_Application2);
                return pg;
                }
            }
            
        
        
        
        
    }

    //Used to save current work
    public pagereference saveMethod() {
        string poin=userinfo.getUserId();
        string king=poin.substring(0, 15);
        //list <Contact> temp;
        
        //contact cnt = new Contact();
        //mycon = new contact();
        //mycon = new contact();
        user us=[select id,contactid, Lastname from user where id=:userinfo.getUserId()];
        Id cid = us.contactid;
        contact cnt = [select id,Highest_Degree1__c,Previous_involvement_with_PCORI__c,Accepts_Reimbursements__c,Accepts_Stipend_s__c,Gender_Research__c,Current_Position_or_Title__c,Exact_Age__c,Salutation,Hispanic_Latino__c,Race__c,Current_Employer__c,Department,Highest_Level_Of_Education__c,Degrees__c,Federal_Employee__c from Contact where id=:cid limit 1];
        
        
        system.debug('*******Contact is'+cnt);
        cnt.Accepts_Stipend_s__c = stipend;
        cnt.Accepts_Reimbursements__c = Reimbursements;
        if(apa.Abide_by_PCORI_COI_Policy__c == 'No'){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'To be considered for membership on any PCORI Advisory Panel, applicants must agree to abide by PCORI’s Conflict of Interest Policy and publicly disclose any potential conflicts. Unfortunately, as you have answered “No” to the question of whether you would agree to this provision, we are unable to consider you for our Advisory Panels. If you intended to indicate that you would abide by our Conflict of Interest policy, please select “Yes” below.'));            
        }
        
        update cnt; 
        myApp = [SELECT Id, Able_to_attend_a_one_to_two_day_meeting__c, Contact__c, Last_Name__c, Application_Status_New__c, Abide_by_PCORI_COI_Policy__c from Advisory_Panel_Application__c where CreatedBy.Id =:userinfo.getUserId() and Application_Status_New__c = 'Draft' order by CreatedDate DESC limit 1];
        
        system.debug('*******'+myApp.size());
        if(myApp.size()==0){
            
            Advisory_Panel_Application__c saveAPA = new Advisory_Panel_Application__c ();
            saveAPA.Gender_del__c =cnt.Gender_Research__c;
            
            
            saveAPA.Current_Employer__c=cnt.Current_Employer__c;
            //saveAPA.Current_Position_or_Title__c=cnt.Department;
            //saveAPA.Highest_Degree_of_Education__c=cnt.Degrees__c;
            saveAPA.What_is_your_highest_level_of_education__c=cnt.Highest_Level_Of_Education__c;
            saveAPA.Are_you_Hispanic_Latino__c = cnt.Hispanic_Latino__c;
            saveAPA.Race_Ethnicity__c=cnt.Race__c;
            saveAPA.Currently_employed_by_the_federal_govt__c=cnt.Federal_Employee__c;
            saveAPA.Title__c =cnt.Salutation;
            saveAPA.Age_in_Years__c =cnt.Exact_Age__c;
            saveAPA.Current_Position_or_Title__c=cnt.Current_Position_or_Title__c;
            saveAPA.Previous_involvement_with_PCORI__c=cnt.Previous_involvement_with_PCORI__c;
            
            saveAPA.Permission_ID__c= king;
            saveAPA.Abide_by_PCORI_COI_Policy__c = apa.Abide_by_PCORI_COI_Policy__c;
            saveAPA.Able_to_attend_a_one_to_two_day_meeting__c = apa.Able_to_attend_a_one_to_two_day_meeting__c;
            saveAPA.Contact__c = us.contactId;
            saveAPA.Last_Name__c = us.Lastname;
            
            saveAPA.Application_Status_New__c = 'Draft';
            //saveAPA.SurveyGizmo_ID__c = '12345';
            if(apa.Abide_by_PCORI_COI_Policy__c == 'No'){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'To be considered for membership on any PCORI Advisory Panel, applicants must agree to abide by PCORI’s Conflict of Interest Policy and publicly disclose any potential conflicts. Unfortunately, as you have answered “No” to the question of whether you would agree to this provision, we are unable to consider you for our Advisory Panels. If you intended to indicate that you would abide by our Conflict of Interest policy, please select “Yes” below.'));            
            }
            else{
                insert saveAPA;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Your application has been saved as a draft.',''));
            }
            
            
            
            
        }
        else{
            // myApp[0].Abide_by_PCORI_COI_Policy__c= apa.Abide_by_PCORI_COI_Policy__c;
            //myApp[0].Accept_Compensation__c = apa.Accept_Compensation__c;
            // myApp[0].Travel_Reimbursement__c = apa.Travel_Reimbursement__c;
            
            
            
            /*myApp[0].Able_to_attend_a_one_to_two_day_meeting__c= apa.Able_to_attend_a_one_to_two_day_meeting__c;*/
            //   if(myApp[0]!=null)
            if(apa.Abide_by_PCORI_COI_Policy__c== 'No'){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all of the required fields'));            
            }
            else{
                if(myApp.size() != 0)
                    database.update (myApp);
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Your application has been saved as a draft.',''));
            }
            
            
            //  attachment = new attachment();
        }
        
        
        /*Pagereference pg1 = new Pagereference('/' + saveAPA.id);
pg1.setredirect(true);
return pg1;*/
        return null;
        
    }
    /* public Attachment attachment {
get {
if (attachment == null)
attachment = new Attachment();
return attachment;
}
set;
}*/
    
    /*public PageReference upload() {

us=[select id,contactid, Lastname from user where id=:userinfo.getUserId()];
Id cid = us.contactid;

attachment.OwnerId = UserInfo.getUserId();

attachment.ParentId = cid;

//if(myApp!=null)
//{

//attachment.ParentId = cid;
//}



//attachment.ParentId = myApp[0].Id; // the record the file is attached to
attachment.IsPrivate = false;
try {
insert attachment;
} catch (DMLException e) {
ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
return null;
} finally {
attachment = new Attachment();
}

ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
return null;
}*/
    
    //used to hold attchment document for user, document is for nodisclouser agreement 
    public Attachment attachment {
        get {
            if (attachment == null)
                attachment = new Attachment();
            return attachment;
        }
        set;
    }
	
	public String attname {get;set;}
	public String attFilename {get;set;}
	public String attdescription {get;set;}
    //uploads non disclouser agreement to system
    public PageReference upload() {
        us=[select id,contactid, Lastname from user where id=:userinfo.getUserId()order by CreatedDate DESC Limit 1];
        Id cid = us.contactid;
         Contact ctnt=[select id,AP_COI_form_attached__c from Contact where id=:cid];
        if(attachment.body != null){
			attachment.OwnerId = UserInfo.getUserId();
		   if(attachment.ParentId == null) 
			attachment.ParentId = cid; // the record the file is attached to
			attachment.IsPrivate = false;
        }
        try {
            if(attname!= null && attname != '' && attFilename!= null && attFilename!='' && attachment.body != null){
				system.debug('attachment body '+attachment.body);
				Attachment__c customAttachment = new Attachment__c();
				customAttachment.Name = attname;
				customAttachment.File_Name__c = attFilename;
				customAttachment.Contact__c = attachment.ParentId;
				insert customAttachment;
				attachment.ParentId = customAttachment.Id;
				attachment.Name = attFilename;
				insert attachment;
				customAttachment.Attachment__c = attachment.Id;
				update customAttachment;
                ctnt.AP_COI_form_attached__c=true;
                update ctnt;
                ShowTextformyprofile=true;
                ShowTextformyprofilemissing=false;
				//ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Note: After uploading, the COI form file will be visible on the My Profile tab.'));
			}else{
                ShowTextformyprofilemissing=true;
                ShowTextformyprofile=false;
				//ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter a File Name and an Attachment.  Then click Save.'));
			}
            
            
        } catch (DMLException e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
            return null;
        } finally {
            attachment = new Attachment(); 
        }
        
        
        
        return null;
    }
    
   
    
    /*public pagereference removeMember(){

Attachment AttachmenttoDel=new Attachment(id=selectedAttachmentId);
delete AttachmenttoDel;
//pagereference pr = new pagereference('/servlet/servlet.FileDownload?file='+selectedAttachmentId);
string urlm ='/apex/MRAPage?id='+MRPID;
pagereference pr = new pagereference(urlm);
pr.setredirect(true);

return pr;
}*/
    
    //function to update system messages on vfp 
    public void updateMe(){
        
        
        //if(apa.Abide_by_PCORI_COI_Policy__c  != 'Yes')
        //    showMsg1 = true;
        //   else
        //showMsg1 = false;
    }
}