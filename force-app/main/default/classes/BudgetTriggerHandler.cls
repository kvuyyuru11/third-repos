public class BudgetTriggerHandler {

    public static Boolean cloningLineItems = false;

    /* Trigger handler class clones a budget
       Logic Moved from trigger BudgetCloneTrigger
    */
    public with sharing class CloneBudget implements Triggers.HandlerInterface {
    
        public void handle() {
            List<Budget__c> clonedList = new List<Budget__c>(); //list of cloned budgets
            List<Key_Personnel_Costs__c> clonedItemList = new List<Key_Personnel_Costs__c>(); //list of cloned budget line items
            Map<id,Budget__c> budWithClonedBudMap = new Map<id,Budget__c>(); //Map of old budget Ids to 
            set<id> parentIds = new set<id>();
            map<id,opportunity> oppparentmap = new map<id,opportunity>();

            for(sObject o : trigger.new)
            {
                Budget__c budg = (Budget__c) o;
                parentIds.add(budg.Associated_App_Project__c);
            }

            for(opportunity opp : [select id, name, Parent_Contract__c, StageName from opportunity where id in: parentIds])
            {
                oppparentmap.put(opp.id,opp);
            }

            for(sObject o : trigger.new ) {
                Budget__c bud = (Budget__c) o;
                Budget__c oldBud = (Budget__c) trigger.oldmap.get(bud.id);

                //clone on budgetNegotiation__c change to 'Yes'
                if( bud.Clone__c != oldBud.Clone__c && bud.Clone__c == system.label.Yes)
               // if( bud.Clone__c == system.label.Yes)
                {
                    Budget__c budClone = bud.clone(false, true, false, false);
                    budclone.Associated_App_Project__c = oppparentmap.get(bud.Associated_App_Project__c).Parent_Contract__c;
                    budClone.SF_Cloned_From__c = bud.id;
                    budWithClonedBudMap.put(bud.id,budClone);
                    clonedList.add(budClone);
                }
                else if(bud.BudgetNegotiation__c != oldBud.BudgetNegotiation__c && bud.BudgetNegotiation__c == system.label.Yes && bud.No_Awardee_Edits_Required__c==false) {

                    //clone budget with 'Pending Submission' status
                    Budget__c budClone = bud.clone(false, true, false, false); //deep clone, don't keep id, timestamps, or autonumber
                    budClone.Approve_Working_Budget__c = null;
                    budClone.Approve_PIR_Budget__c = null;
                    budClone.Date_Budget_Approved__c = null;
                    budClone.Budget_Status__c = system.label.Working_Budget_Pending_Submission;
                    budClone.BudgetNegotiation__c = null;
                    budClone.Budget_Executed__c = false;
                    budClone.Ready_for_Archive__c = false;
                    budClone.Comments_for_Rejected_Budget__c = null;
                    budClone.clonedfrom__c = bud.id;

                    //map original budget id to cloned budget
                    budWithClonedBudMap.put(bud.id,budClone);

                    //add cloned budget to clonedList
                    clonedList.add(budClone);
                }
                else if(bud.BudgetNegotiation__c != oldBud.BudgetNegotiation__c && bud.BudgetNegotiation__c == system.label.Yes && bud.No_Awardee_Edits_Required__c==true && bud.Supplemental_Funding_Flag__c==true && oppparentmap.get(bud.Associated_App_Project__c).StageName== system.label.Programmatic_Review) {

                    //clone budget with 'Pending Submission' status
                    Budget__c budClone = bud.clone(false, true, false, false); //deep clone, don't keep id, timestamps, or autonumber
                    
                    budClone.Budget_Status__c = system.label.Working_Budget_Submitted;
                   
                    budClone.clonedfrom__c = bud.id;
                    //map original budget id to cloned budget
                    budWithClonedBudMap.put(bud.id,budClone);
                    //add cloned budget to clonedList
                    clonedList.add(budClone);
                   
                }
            }

            //if nothing was cloned we are done
            if(clonedList.size() == 0) {
                return;
            }

            //insert all cloned budgets
            insert clonedList; //must be inserted first to populate budget ids

            //if any budget was cloned we need to clone the line items for it
            for(Key_Personnel_Costs__c budLinItems : [SELECT Applicable_Rate__c, Budget__c, Calendar_Months__c, ConnectionReceivedId,
                                                                ConnectionSentId, Cost_Category__c, Description__c, End_Date__c, Fringe_Benefits__c,
                                                                Fringe_Rate2__c, Fringe_Rate__c, Hourly_Unit_Rate__c, Id, Inst_Base_Salary__c,
                                                                IsDeleted, Key__c, Line_Item_Name__c, Modified_Direct_Cost__c, Name,
                                                                Peer_Review__c, Percent_Effort__c, RecordTypeId, Record_Count_Per_Year__c,
                                                                Record_Id__c, record__c, Role_On_Project__c, Roll_on_Project__c, Salary_Requested__c,
                                                                Start_Date__c, Subcontractor_Name__c, SubTotal__c, SystemModstamp, Total__c, Total_1__c,
                                                                Total_Indirect_Costs2__c, Total_Indirect_Costs__c, Total_Prime_Indirect_Costs__c,
                                                                Variable_Count__c, Year__c
                                                        FROM Key_Personnel_Costs__c
                                                        WHERE Budget__c IN :budWithClonedBudMap.keySet()])
            {
                Key_Personnel_Costs__c budLinItemsClone = budLinItems.clone(false, true, false, false); //deep clone, don't keep id, timestamps, or autonumber
                budLinItemsClone.Budget__c = budWithClonedBudMap.get(budLinItems.Budget__c).id; //get cloned budget's id from map
                clonedItemList.add(budLinItemsClone);
            }

            //insert all cloned budgets line items
            if(!clonedItemList.isEmpty()) {
                BudgetTriggerHandler.cloningLineItems = true; //needed so cloned items don't double the calculated totals for each year
                insert clonedItemList;
                BudgetTriggerHandler.cloningLineItems = false;
            }
        }
    }
    
    public static void avoidDuplicateBudget(List<Budget__c> triggerNewList){
        List<Budget__c> duplicateBudgetList = new List<Budget__c>();
        set<id> parentIds = new set<id>();
        set<id> ApplicationIds = new set<id>();
        for(Budget__c b : triggerNewList){
        system.debug('vamoooo '+b.Associated_App_Project__c);
            if(string.valueOf(b.Associated_App_Project__c) !=null && string.valueOf(b.Associated_App_Project__c) !='N/A' ){
                parentIds.add(b.Associated_App_Project__c);
            }
        }
        for (Budget__c b : [Select id, Associated_App_Project__c, Budget_Status__c from Budget__c where Budget_Status__c='' AND Associated_App_Project__c IN : parentIds]){
            ApplicationIds.add(b.Associated_App_Project__c); 
        }
        for(Budget__c b : triggerNewList){
            if(ApplicationIds.contains(b.Associated_App_Project__c)){
                b.addError('The budget has already been created for this application. Please refresh your page');
            }
        }
        
    }

}