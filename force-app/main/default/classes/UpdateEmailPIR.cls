public without sharing class UpdateEmailPIR {
    public UpdateEmailPIR() {
    }
   
   Public Void UpdateEmailonPIR(List<PIR__c> pList){
   
   Set <Id> parentIds = new set<Id>();
    for(PIR__c pir : pList) {
        parentIds.add(pir.Application__c);
    }
      
    List<Opportunity> oppSOQLList =[SELECT Id, Name, AO_Name_User__r.email, PI_Name_User__r.email, PI_Project_Lead_Designee_1_New__r.email FROM Opportunity where id IN : parentIds LIMIT 50000]; 

    List<PIR__c> pirSOQLList =[SELECT id,Administrative_Official_Email_v2__c,PI_Lead_Email_v2__c, PI_Designee_Email_v2__c from PIR__c WHERE Application__c IN : parentIds LIMIT 50000];
          // System.debug('############# irbSOQLList'+irbSOQLList); 

   //Map<Id, IRB__C> irbmap = new Map<Id, IRB__C>();
   Map<Id, Opportunity> oppmap = new Map<Id, Opportunity>();

  for(Opportunity opp: oppSOQLList)
  {
     oppmap.put(opp.id, opp);

  }
    try{ 
        for(PIR__c pir:pList){     
                pir.Administrative_Official_Email_v2__c = oppmap.get(pir.Application__c).AO_Name_User__r.email;
                pir.PI_Lead_Email_v2__c = oppmap.get(pir.Application__c).PI_Name_User__r.email;
                pir.PI_Designee_Email_v2__c = oppmap.get(pir.Application__c).PI_Project_Lead_Designee_1_New__r.email;
        }// end for
    }
    catch(DmlException e) {
           System.debug('The following exception has occurred: ' + e.getMessage());   
    } //end for UpdateEmailonPIR       
   }
}