/****************************************************************************************************
*Description:           Test unit for ap_Controller2
*                       
*
*Required Class(es):    N/A
*
*Organization: PCORI / Accenture
*Revision   |Date       |Author             |AdditionalNotes
*====================================================================================================
*   1.00     1/10/2016    Daniel Haro            Inital version
*   
*****************************************************************************************************/  


@isTest
private  without sharing class AP_Controller2Test {
public static testMethod void myTest(){
  PageReference pageRef = new pagereference(label.AP_Application2); //replace with your VF page name
  Test.setCurrentPage(pageRef);
    
  // String profileId = Label.CommunityAdminAccount;
         Profile Profile1 = [SELECT Id, Name FROM Profile WHERE Name = 'PCORI Community Partner'];
         String profileId = Profile1.id;
      String accountId = Label.CommunityAdminProfile;
       Contact tempContact;
     Contact contact1;
      //Profile CProfile = [SELECT Id, Name FROM Profile WHERE Name ='Customer Community Login User'];
      Profile AProfile = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator'];
      //User u = [SELECT Id FROM User WHERE ProfileId =:AProfile.Id AND isActive = true LIMIT 1];
      User u = [SELECT Id FROM User WHERE ProfileId =:AProfile.Id AND isActive = true AND UserRoleId!=null LIMIT 1];
      User temp = new User();
      User user2 = new User();
     Account account;
    system.runAs(u){ 
        account = new Account(Name='Test');
        database.insert(account);
        Contact contact = new Contact(FirstName='Rainmaker', LastName='Test', AccountId=account.Id);
        insert(contact);
        temp.Email = 'testing@test.com';
        temp.ContactId = contact.Id;
        temp.UserName = 'testingXIV@test.com';
        temp.LastName = 'Padilla';
        temp.Alias = 'jpadille';
        temp.CommunityNickName = 'jpadilla';
        temp.TimeZoneSidKey = 'GMT';
        temp.LocaleSidKey = 'en_US';
        temp.EmailEncodingKey = 'ISO-8859-1';
        temp.ProfileId = profileId;
        temp.LanguageLocaleKey = 'en_US';
       
        String userId = Site.createPortalUser(temp, accountId, 'Testing');
        
        contact1 = new Contact(FirstName='Rainmaker1', LastName='Test1', AccountId=account.Id,Reviewer_Status__c='New Reviewer');
        database.insert(contact1); 
    
    }
   test.startTest(); 
   System.runAs(u)
       
   {
       
       // Contact contact1 = new Contact(FirstName='Rainmaker1', LastName='Test1', AccountId=account.Id,Reviewer_Status__c='New Reviewer');
        //database.insert(contact1);
         tempContact = [select firstName,LastName,accountID from Contact where LastName = 'Test' limit 1];
       
   }
    System.runAs(temp)
    {
 
    
AP_Application2Controller testCon = new AP_Application2Controller();
       
    testCon.apa.Application_Status_new__c = 'YES';
  testCon.checkCount1();
        testCon.Identify();
      
        testCon.apa = new Advisory_Panel_Application__c();
        testCon.apa.Involved_in_Research_Prioritization__c = 'Yes';
        testCon.apa.Data_related_research_involvement__c = 'Yes';
        testCon.apa.Describe_data_research_involvement__c ='test data';
        testCon.apa.Multi_stakeholder_processes_frameworks__c ='Yes';
        testCon.apa.Participated_in_Clinical_Trial__c = 'Yes';
        testCon.apa.Patient_centric_clinicals_involvment__c ='Yes';
        testCon.apa.Ever_engaged_in_PCORI_research__c = 'Yes';
        testCon.apa.engaging_patients_and_other_stakeholders__c = 'Yes';
        testCon.apa.Engaged_in_PCOR_research__c = 'Yes';
        testCon.apa.Categories_identified_with__c = 'testing';
        testCon.test1();
    testCon.saveMethod();
    testCon.Nextmethod();
       
        
        
        
        
        testCon.apa.Data_related_research_involvement__c = null;
        testCon.apa.Multi_stakeholder_processes_frameworks__c = '';
        // apa.Advisory_Panels_Applied_For__c
        testcon.apa.Advisory_Panels_Applied_For__c ='';
       testCon.apa.Involved_in_Research_Prioritization__c = '';
        testcon.apa.Advisory_Panels_Applied_For__c =  testcon.apa.Advisory_Panels_Applied_For__c +'Patient Engagement;';
          testCon.Previousmethod();
   //testCon.updateMe();
        
      
    }  
    
    
    
     
    System.runAs(temp)
    {
        
        
        

        AP_Application2Controller testCon;// = new AP_Application2Controller();
        Advisory_Panel_Application__c tempApp = new Advisory_Panel_Application__c();
        tempApp.Contact__c = tempContact.id;
        tempApp.Application_Status_New__c = 'Draft';
        tempApp.Preferred_Advisory_Panel__c = 'test';
        tempApp.Primary_Clinical_Trials_Community__c = 'test';
        tempApp.Primary_Rare_Disease_Community__c = 'test';
        tempApp.Patient_centric_clinicals_involvment__c = 'test';
        tempApp.Participated_in_Clinical_Trial__c = 'test';
        tempApp.Multi_stakeholder_processes_frameworks__c = 'test';
        tempApp.Data_related_research_involvement__c = 'test';
        tempApp.Involved_in_Research_Prioritization__c = 'test';
        database.insert(tempApp);
        testCon = new AP_Application2Controller();
        tempApp = [select id, Contact__C from Advisory_Panel_Application__c where Contact__C =: tempContact.id limit 1]; 
        testCon.myApp.add(tempApp);// = new Advisory_Panel_Application__c();
        testCon.apa.Advisory_Panels_Applied_For__c = 'Addressing Disparities;Clinical Trials;Rare Disease;APDTO;Communication and Dissemination Research;Improving Healthcare Systems;Patient Engagement;';
     
        
        testCon.apa.Categories_identified_with__c = 'Patient/Consumer;Clinician;';
       testCon.apa.Categories_identified_with__c =  testCon.apa.Categories_identified_with__c + 'Patient/Consumer;';
          testCon.apa.Categories_identified_with__c =  testCon.apa.Categories_identified_with__c + 'Caregiver/Family Member of Patient;';
         testCon.apa.Categories_identified_with__c =  testCon.apa.Categories_identified_with__c + 'Patient/Caregiver Advocate/Advocacy Org;';
              testCon.apa.Categories_identified_with__c =  testCon.apa.Categories_identified_with__c + 'Clinician;';
              testCon.apa.Categories_identified_with__c =  testCon.apa.Categories_identified_with__c + 'Hospital/Health System;';
              testCon.apa.Categories_identified_with__c =  testCon.apa.Categories_identified_with__c + 'showHospital;';
              testCon.apa.Categories_identified_with__c =  testCon.apa.Categories_identified_with__c + 'showPurchaser;';
         testCon.apa.Categories_identified_with__c =  testCon.apa.Categories_identified_with__c + 'Payer;';
                 testCon.apa.Categories_identified_with__c =  testCon.apa.Categories_identified_with__c + 'Industry;';
         testCon.apa.Categories_identified_with__c =  testCon.apa.Categories_identified_with__c + 'Health Research;';
         testCon.apa.Categories_identified_with__c =  testCon.apa.Categories_identified_with__c + 'Policy Maker;';
         testCon.apa.Categories_identified_with__c =  testCon.apa.Categories_identified_with__c + 'Training Institution;';
         testCon.apa.Categories_identified_with__c =  testCon.apa.Categories_identified_with__c + 'Purchaser;';

        
        
        
        //testCon.apa.Categories_identified_with__c = 'Clinician Hospital/Health System;Caregiver/Family Member of Patient;Purchaser;Payer;Industry;Health Research;Training Institution';
        
        testCon.apa.Involved_in_Research_Prioritization__c = 'Yes';
        testCon.apa.Data_related_research_involvement__c = 'Yes';
        testCon.apa.Multi_stakeholder_processes_frameworks__c = 'Yes';
        testCon.apa.Participated_in_Clinical_Trial__c = 'Yes';
        testCon.apa.Patient_centric_clinicals_involvment__c = 'Yes';
        testCon.apa.Ever_engaged_in_PCORI_research__c = 'Yes';
        testCon.apa.engaging_patients_and_other_stakeholders__c = 'Yes';
        testCon.apa.Engaged_in_PCOR_research__c = 'Yes';
        
        
        
        testCon.Nextmethod();
        testCon.apa.Advisory_Panels_Applied_For__c = 'Clinician';
        testCon.apa.Clinician_categories_identified_with__c = NULL;
        testCon.Identify();
        testCon.apa.Advisory_Panels_Applied_For__c = 'go;go;Clinical Trials;';
        testCon.checkCount1();
            
        
        
        
        testCon.saveMethod();
        testCon.getValues();
        testCon.education = null;
        testCon.holddata = null;
        testCon.degree = null;
        testCon.pap = null;
        testCon.allOptions1 =null;
        testCon.selectedOptions2 = null;
        testCon.allOptions2 = null;
        testCon.checkCount1 = null;
        testCon.Identify = null;
        testCon.message = null;
        testCon.getValues = null;
        testCon.test4 = null;
        
        
        testCon.apa.Involved_in_Research_Prioritization__c = null;
        testCon.apa.Involved_in_Research_Prioritization__c = null;
        testCon.apa.Data_related_research_involvement__c =  '';//null;
        testCon.apa.Advisory_Panels_Applied_For__c = testCon.apa.Advisory_Panels_Applied_For__c + 'Patient Engagement;';
         testCon.apa.Advisory_Panels_Applied_For__c = testCon.apa.Advisory_Panels_Applied_For__c + 'APDTO;';
         testCon.apa.Advisory_Panels_Applied_For__c = testCon.apa.Advisory_Panels_Applied_For__c + 'Rare Disease;';
                 testCon.apa.Advisory_Panels_Applied_For__c = testCon.apa.Advisory_Panels_Applied_For__c + 'Clinical Trials;';
                // testCon.apa.Advisory_Panels_Applied_For__c = testCon.apa.Advisory_Panels_Applied_For__c + 'Clinical Trials;';
        testCon.apa.Primary_Clinical_Trials_Community__c = null;
        testCon.apa.Preferred_Advisory_Panel__c = null;
        

        
        
        testCon.apa.Multi_stakeholder_processes_frameworks__c = null;
        testCon.apa.Participated_in_Clinical_Trial__c = null;
        testCon.apa.Patient_centric_clinicals_involvment__c = null;
        testCon.apa.Ever_engaged_in_PCORI_research__c = null;
        testCon.apa.engaging_patients_and_other_stakeholders__c = null;
        testCon.apa.Engaged_in_PCOR_research__c = null;
        testCon.Nextmethod();
        testCon.saveMethod();
        
    }
    
    test.stopTest();
    
    
    
    
    
}
  
    
}