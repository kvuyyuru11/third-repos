@isTest
public class UpdatePIRTest{
    private static testMethod void TestStatus(){
        RecordType recOpp = [SELECT Id FROM RecordType WHERE DeveloperName = 'Research_Awards' AND sObjectType = 'Opportunity'];
        RecordType recAcc = [SELECT Id FROM RecordType WHERE DeveloperName = 'Standard_Salesforce_Account' AND sObjectType = 'Account'];
        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()]; 
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.RecordTypeId = recAcc.Id;
        insert acc;
        
        List<Opportunity> opplst = new List<Opportunity>();
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = recOpp.Id;
        opp.AccountId = acc.Id;
        opp.AO_Name_User__c = usr.Id;
        opp.PI_Name_User__c = usr.Id;
        opp.PI_Project_Lead_Designee_1_New__c = usr.Id;
        opp.Contract_Number__c = '03423';
        opp.Name = 'Test';
        opp.StageName = 'In Progress';
        opp.Application_Number__c = '12213';
        opp.Full_Project_Title__c = 'Project Test';
        opp.CloseDate = Date.newInstance(2016, 12, 22);
        opplst.add(opp);
        insert opp;
        
        
        PIR__c p = new PIR__c();
        p.Application__c = opp.Id;
        p.Administrative_Official_Email_v2__c = 'pirtest10222016@testaccent.com';
        p.PIR_Response_Deadline__c = DateTime.newInstance(2016, 12, 31);
        insert p;
        
        UpdatePIR.UpdateStatus(p.Id);
        PIR__c pi = [SELECT PIR_Status__c FROM PIR__c WHERE Id = :p.Id];
        UpdateInPIRHelper pHelp = new UpdateInPIRHelper();
        pHelp.updateEmailIdsPIR(opplst);
        System.assertEquals(pi.PIR_Status__c, 'Response Received');
        
        
    }
}