public with sharing class FoundationConnectLink {
private List<Link> links {get;set;}
private string UserName {get;set;}
private string Password {get;set;}
public boolean P2PTier2{get;set;}
public string recType{get;set;}
public boolean P2PTierA{get;set;} 
public boolean P2PBothTiers{get;set;}     
private Set<String> accessibleLinks{get;set;}
Public Boolean b {get;set;}
Public List<OpportunityShare> oslist = new List<OpportunityShare>();
Public Set<ID> OpportunitySet = new Set<ID>();
Public List<Opportunity> OpportunityList = new List<Opportunity>();
Public RecordType OpportunityRecordType = new RecordType();
Public List<Opportunity> EngagementOpportunityList = new List<Opportunity>();
string recdid = ApexPages.currentPage().getParameters().get('recType');
public FoundationConnectLink()
{
    if(recdid=='P2P'){
        P2PTier2=True;
    }
    if(recdid=='P2PTierA'){
        P2PTierA=True;    
    }
    if(recdid=='P2PBothTiers'){
        P2PBothTiers=True;        
    }
    if (recdid == 'NoP2Precs') {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Currently no P2P projects are shared with you'));
    }
    links = new List<Link>();
    accessibleLinks = new Set<String>();
    
    //Get the Current User's Email Address
    for (User u:[SELECT ContactId, Contact.FGM_Portal__Username__c, Contact.FGM_Portal__Password__c, Contact.FGM_Portal__User_Profile__c FROM User WHERE Id=:UserInfo.getUserId()])
    {
        UserName = u.Contact.FGM_Portal__Username__c;
        Password = u.Contact.FGM_Portal__Password__c;
        if (u.Contact.FGM_Portal__User_Profile__c != null)
        for (string s: u.Contact.FGM_Portal__User_Profile__c.split(';'))
        {
            accessibleLinks.add(s.toLowerCase());
        }
    }
    //Get the Links from the Custom Settings object for FoundationConnectionLinks
    for (FoundationConnectionLink__c fl: [SELECT Name, Url__c, Access__c  FROM FoundationConnectionLink__c WHERE isActive__c = true ORDER BY Order__c])
    {
        if (accessibleLinks.contains(fl.Access__c.toLowerCase()))
            links.add(new Link(fl.Name,fl.Url__c,UserName,Password,fl.Access__c));
    } 
    try{
   b=false;
    oslist = [SELECT Id,IsDeleted,LastModifiedById,LastModifiedDate,OpportunityAccessLevel,OpportunityId,RowCause,UserOrGroupId FROM OpportunityShare where UserOrGroupId=:userinfo.getuserid() LIMIT 50000];
    //system.debug('Here is the Opportunityshare'+ oslist);
    for(OpportunityShare Os:oslist){
    OpportunitySet.add(Os.OpportunityId);
  
    }
    OpportunityList = [Select Id,name,RecordtypeID from Opportunity Where ID IN : OpportunitySet];
    system.debug('Here is the Opportunity List'+OpportunityList);
    OpportunityRecordType = [SELECT Id, Name FROM RecordType WHERE Name = 'Engagement Awards' AND SobjectType='opportunity'];
    system.debug('Here is the Opportynity Recordtye'+OpportunityRecordType);
    for(Opportunity Olist:OpportunityList){
    if(OpportunityRecordType.Id == Olist.RecordtypeID && OpportunityRecordType.Id!=null){
    EngagementOpportunityList .add(Olist);
    }
    }
    if(!EngagementOpportunityList.isEmpty()){
      b=true;
    }
    }catch(exception e){
        system.debug('***'+'The exception is '+e);
    }
   
}

public List<Link> getGranteeLinks()
{
    List<Link> retval = new List<Link>();
    for (Link l:links)
    {
        if (l.Type == 'Grantee')
            retval.add(l);
    }
    return retval;
}
public List<Link> getReviewerLinks()
{
    List<Link> retval = new List<Link>();
    for (Link l:links)
    {
        if (l.Type == 'Reviewer')
            retval.add(l);
    }
    return retval;
}
public Boolean showGrantee
{
    get
    {
        return getGranteeLinks().size() > 0;
    }
}
public Boolean showReviewer
{
    get
    {
        return getReviewerLinks().size() > 0;
    }
}
public Boolean showNotConfigured
{
    get
    {
        return (!showGrantee && !showReviewer);
    }
}

public class Link
{
    public string Description{get;set;}
    public string URL{get;set;}
    public string Type{get;set;}
    
    public Link(string d, string u, string un, string p, string t)
    {
        this.Description = d;
        this.URL = u+'&un='+un+'&pw='+p+'&lang=en';
        this.Type = t;
    }

}
        
         public pagereference Engagementmethod(){
       
       /* for (Link l:links)
    {
        if(l.Description=='Engagement Awards Dashboard'){
        PageReference result1=new PageReference(l.URl);
         
         return result1;
         }
         }*/
        return null;
       
               
    }
       public pagereference CreateLOImethod(){
       
      /*  for (Link l:links)
    {
    if(l.Description=='Create LOI'){
        //FoundationConnectionLink__c link2= FoundationConnectionLink__c.getInstance('Create LOI');
        PageReference result2=new PageReference(l.URl);
        
           return result2;
           }
           } */
           return null;
           }
    
    public pagereference CreateMeetingmethod() {
        /*string urlth=Label.Meeting_Link;
         PageReference result3= new PageReference(urlth);
         return result3;  */
         return null;
    }
    
    ///////////////////
    // FOR RESEARCH AWARDS
    // Below was added by Matt Hoffman for Release 2 - 8/15/2016
    // Application Inventory: AI-1059
    // Client Requirement ID: RAr2_C4_022
    // Before, the research awards button from the landing page went straight to the opportunity dashboard. 
    // Now it goes to another page which shares this controller which shares this controller and uses the below buttons.

    
    
    // button 1 - PRE
    public pageReference RA_RedirectToLOIs()
    {
        try{
            PageReference pageReferenceAP = new pageReference('/'+'fgm_portal__communitydashboard'); 
            pageReferenceAP.setRedirect(true);
            return pageReferenceAP;       
        }
        catch(exception ee)
        {
            throw ee;
        }
    }
    
    // button 2 - PRE
    public pageReference RA_RedirectToFundingOpportunities()
    {
        try{
            Long startTime = DateTime.now().getTime();
            Long finishTime = DateTime.now().getTime();
            while ((finishTime - startTime) < 3500) {
                
                finishTime = DateTime.now().getTime();
            }
            PageReference pageReferenceAP = new pageReference('/'+'FGM_Portal__CommunityLanding'); 
            pageReferenceAP.setRedirect(true);
            return pageReferenceAP;       
        }
        catch(exception ee)
        {
            throw ee;
        }
    }
    
    // button 3 - PRE
    public pageReference RA_RedirectToPIRs()
    {
        try{
            PageReference pageReferenceAP = new pageReference('/'+'a38/o'); 
            pageReferenceAP.setRedirect(true);
            return pageReferenceAP;       
        }
        catch(exception ee)
        {
            throw ee;
        }
    }
    
    // button4 - POST
    public pageReference RA_RedirectToProjects()
    {
        try{
            PageReference pageReferenceAP = new pageReference('/'+'MyProjectCustomView?recType=research'); 
            pageReferenceAP.setRedirect(true);
            return pageReferenceAP;       
        }
        catch(exception ee)
        {
            throw ee;
        }
    }
    public pageReference RA_RedirectToDI()
    {
        try{
            PageReference pageReferenceAP = new pageReference('/'+'MyProjectCustomView?recType=DI'); 
            pageReferenceAP.setRedirect(true);
            return pageReferenceAP;       
        }
        catch(exception ee)
        {
            throw ee;
        }
    }
        //P2P Code Snippet Author #Anudeep Buddineni
        public pageReference RedirectToP2P()
    {
        try{
            PageReference pageReferenceAP = new pageReference('/'+'MyProjectCustomView?recType=P2P'); 
            pageReferenceAP.setRedirect(true);
            return pageReferenceAP;       
        }
        catch(exception ee)
        {
            throw ee;
        }
    }
    //P2P Code Snippet Author #Anudeep Buddineni
        public pageReference RedirectToP2PTierA()
    {
        try{
            PageReference pageReferenceAP = new pageReference('/'+'MyProjectCustomView?recType=P2PTierA'); 
            pageReferenceAP.setRedirect(true);
            return pageReferenceAP;       
        }
        catch(exception ee)
        {
            throw ee;
        }
    }
    public pageReference RA_RedirectToInfrastructure()
    {
        try{
            PageReference pageReferenceAP = new pageReference('/'+'MyProjectCustomView?recType=Infrastructure'); 
            pageReferenceAP.setRedirect(true);
            return pageReferenceAP;       
        }
        catch(exception ee)
        {
            throw ee;
        }
    }
    
    public pageReference EA_RedirectToProjects()
    {
        try{
            PageReference pageReferenceAP = new pageReference('/'+'MyProjectCustomView?recType=engage'); 
            pageReferenceAP.setRedirect(true);
            return pageReferenceAP;       
        }
        catch(exception ee)
        {
            throw ee;
        }
    } 
    
    // button 5 - POST
    public pageReference RA_RedirectToSiteVisits()
    {
        try{
            PageReference pageReferenceAP = new pageReference('/'+'a15/o'); 
            pageReferenceAP.setRedirect(true);
            return pageReferenceAP;       
        }
        catch(exception ee)
        {
            throw ee;
        }
    }
    
     /*    //My Projects button
    public pageReference redirectToRA()
          {
              try{
                                   
                   PageReference pageReferenceAP = new pageReference('/'+'006?fcf='+Label.Redirect_to_Engagement_Awards_List_view); 
                   pageReferenceAP.setRedirect(true);
                   return pageReferenceAP;       
                 }
              catch(exception ee)
                 {
                    throw ee;
                    return null;
                 }
          } */ 
    
    public pagereference redirecttoRRCA(){
        string urlth=Label.RRCA_Meeting_Link;
         PageReference result3= new PageReference(urlth);
         return result3;
    }
        public PageReference redirecttodashboard() {
      string urlth2=Label.RRCA_Dashboard;
         PageReference result4= new PageReference(urlth2);
         return result4;
    }
    
    
     /**
    * @author        Abhinav Polsani(EA Migration)
    * @date          03/26/2018
    * @description   Create LOI button on Engagement Awards -- Modified on 4/7/2017 as part of Release 4 .Net migration AI-1398 AI-1405
    * @param         None
    * @return        pageReference 
    */
    public PageReference EAIN_Meetingmethod() {
        giveGranteePermissionSetIfNeeded();
        try {
            Long startTime = DateTime.now().getTime();
            Long finishTime = DateTime.now().getTime();
            while ((finishTime - startTime) < 3500) {    
                finishTime = DateTime.now().getTime();
            }
            PageReference pageReferenceAP = new pageReference('/'+'FGM_Portal__CommunityCampaign?id='+Label.EAINCampaign); 
            pageReferenceAP.setRedirect(true);
            return pageReferenceAP;       
        }
        catch (exception ee) {
            throw ee;
        }
    }

    
    public PageReference EAINPPRN_Meetingmethod() {
        giveGranteePermissionSetIfNeeded();
        try {
            Long startTime = DateTime.now().getTime();
            Long finishTime = DateTime.now().getTime();
            while ((finishTime - startTime) < 3500) {    
                finishTime = DateTime.now().getTime();
            }
            PageReference pageReferenceAP = new pageReference('/'+'FGM_Portal__CommunityCampaign?id='+Label.EAIN_PPRN); 
            pageReferenceAP.setRedirect(true);
            return pageReferenceAP;       
        }
        catch (exception ee) {
            throw ee;
        }
    }
    
    /**
    * @author        Abhinav Polsani(EA Migration)
    * @date          03/26/2018
    * @description   EA Migration
    * @param         None
    * @return        pageReference 
    */
    public pageReference EA_RedirectToFundingOpportunities() {
        giveGranteePermissionSetIfNeeded();
        try {
            Long startTime = DateTime.now().getTime();
            Long finishTime = DateTime.now().getTime();
            while ((finishTime - startTime) < 3500) {
                finishTime = DateTime.now().getTime();
            }
            PageReference pageReferenceAP = new pageReference('/'+'FGM_Portal__CommunityCampaign?id='+Label.EACampaign); 
            pageReferenceAP.setRedirect(true);
            return pageReferenceAP;       
        }
        catch (exception ee) {
            throw ee;
        }
    }
 
    /**
    * @author        Abhinav Polsani(EA Migration)
    * @date          03/26/2018
    * @description   Engagement Awards Dashboard button on Engagement Awards -- Modified on 4/7/2017 as part of Release 4 .Net migration
    * @param         None
    * @return        pageReference 
    */ 
    public pageReference EA_RedirectToLOIs() {
        giveGranteePermissionSetIfNeeded();
        try {
            PageReference pageReferenceAP = new pageReference('/'+'fgm_portal__communitydashboard'); 
            pageReferenceAP.setRedirect(true);
            return pageReferenceAP;       
        }
        catch (exception ee) {
            throw ee;
        }
    }

    /**
    * @author        Abhinav Polsani(EA Migration)
    * @date          03/26/2018
    * @description   Assigning Community Grantee User login permission set to new users - Added by Sunitha 5/3/2017 AI-1395
    * @param         None
    * @return        pageReference 
    */ 
     public void giveGranteePermissionSetIfNeeded() {
        List<PermissionSet> granteePermSet = [SELECT id FROM PermissionSet WHERE Name =: 'Community_Grantee_Permission_set_User_Login_Licence'];
        Id granteePermSetID = granteePermSet[0].id;
        List<PermissionSetAssignment> communityPermAssignmentsList = [SELECT AssigneeId FROM PermissionSetAssignment 
                                                                      WHERE PermissionSetId =: granteePermSetID
                                                                      AND AssigneeId =: userinfo.getUserId()];
        if (communityPermAssignmentsList.isEmpty()) {
            PermissionSetAssignment permAssign = new PermissionSetAssignment();
            permAssign.AssigneeId = userinfo.getUserId();
            permAssign.PermissionSetId = granteePermSetID;
            insert permAssign;           
        }
    }
    
    
    
    
}