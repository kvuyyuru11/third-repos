/*
    Author     : Venu
    Date       : 16th mar 2017
    Name       : 
    Description: 
*/
public class MethodologyStandardProject 
{

   public Methodology_Standard_for_Project__c mos  {get;set;}
   public string sProjname{get;set;}
   @testvisible private id projid{get;set;}
   @testvisible private static id Abbrid{get;set;}
   
    public MethodologyStandardProject()
    {
    
       mos  =new Methodology_Standard_for_Project__c ();
       string sReturl=ApexPages.currentPage().getHeaders().get('Referer');
        if(string.isNotBlank( sReturl)){
        system.debug('==sReturl'+sReturl);
       string[] sSplit=sReturl.split('/');
        system.debug('==sReturl'+sSplit.size());
       if(sSplit[sSplit.size()-1].startsWithIgnoreCase('006')){
          projid= sSplit[sSplit.size()-1];
         }
        }
          
         if(!String.isEmpty(projid)){
         opportunity objopportunity= [select id, name from opportunity where id =:projid];
         sProjname=objopportunity.name;
         mos.Project_Name__c=  projid;
         }
       
    }
   
  
  public PageReference save(){
   insert mos;
   PageReference pr=new PageReference('/'+projid);
   pr.setRedirect(true);
   return pr; 
 }
 
 
 
  public PageReference savenew(){
     insert mos;
     mos=new Methodology_Standard_for_Project__c ();
     mos.Project_Name__c=projid;
     PageReference pr=new PageReference('/apex/MethodologyProjectRedirect?scontrolCaching=1&projsid='+projid);
     pr.setRedirect(false);
   return pr; 
 }
 public pagereference cancel(){
     PageReference pr=new PageReference('/'+projid);
     pr.setRedirect(true);
   return pr; 
 }
  
  @remoteaction
  public static string GetAbbrProjName(string abbrname){
    string sRetval;
    Methodology_Standard__c  objMethodology_Standard= [select id,Abbrev__c,Name from Methodology_Standard__c where Abbrev__c= :abbrname];
    Abbrid=objMethodology_Standard.id;
     return objMethodology_Standard.name;
  
  }
}