public with sharing class Tasks{
    public List<Task> tasks {get;set;}
    @TestVisible private Integer page;
    @TestVisible private List<Task> allTasks;
    public Boolean showNext {get;set;}
    public Boolean showPrevious {get;set;}
    public Boolean showTable {get;set;}
    @TestVisible private Integer temp = 0;
    public Tasks__c tl;
    @TestVisible private FGM_Base__Benchmark__c bench;
    public Tasks(ApexPages.StandardController c){
        this.bench = (FGM_Base__Benchmark__c)c.getRecord();
        tl = Tasks__c.getValues('Number of tasks');
        page = 0;
        if(Integer.valueOf(tl.TasksList__c) != null && Integer.valueOf(tl.TasksList__c) >= 0){
            temp = Integer.valueOf(tl.TasksList__c);    
        }
        if(bench.Id != null){
            showNext = false;
            showPrevious = false;
            allTasks = [SELECT Id, Description__c, Subject, Milestone_Task_Type__c, OwnerId, WhatId, WhoId, Owner.Name, Start_Date__c, ActivityDate, Priority, Status FROM Task WHERE WhatId = :bench.Id];
            getTasks();
            if(allTasks.size() == 0){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'No tasks were created for this record'));
            }
            else{
                if(allTasks.size() > temp){
                    showNext = true;
                }
                showTable = true; 
            }
        }
    }
    public void getTasks(){
        tasks = [SELECT Id, Subject, Description__c, Milestone_Task_Type__c, WhatId, OwnerId, Owner.Name, WhoId, Start_Date__c, ActivityDate, Priority, Status FROM Task WHERE WhatId = :bench.Id LIMIT 5 OFFSET :page];
    }
    public void next(){
        if(allTasks.size() - (temp + page) > 0){
            page = page + temp;
            showPrevious = true;
            if(allTasks.size() - (temp + page) <= 0){
                showNext = false;
            }
            getTasks();
            }
        else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.warning, 'No more tasks to show'));
        }
    }
    public void previous(){
        if(page > 0){
            page = page - temp;
            showNext = true;
            if(page - temp < 0){
                showPrevious = false;      
            }
            getTasks();
            
        }
        else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.warning, 'You reached the first page of the tasks list'));
        }
    }
}