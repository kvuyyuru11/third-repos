@isTest
private class BudgetSummaryTableController_Test {
    
    @isTest static void testBudgetSummaryTableController() {
        Id r1 =Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Research Awards').getRecordTypeId();
        Id rKeyPersonnel =Schema.SObjectType.Key_Personnel_Costs__c.getRecordTypeInfosByName().get('Key Personnel').getRecordTypeId();
        Id rConsultantCost =Schema.SObjectType.Key_Personnel_Costs__c.getRecordTypeInfosByName().get('Consultant Cost').getRecordTypeId();
        Id rSubcontractorDirect =Schema.SObjectType.Key_Personnel_Costs__c.getRecordTypeInfosByName().get('Subcontractor Direct').getRecordTypeId();
        
        Id pcoriCommunityPartID = [SELECT Id FROM Profile WHERE name = 'PCORI Community Partner'].Id;

        Account.SObjectType.getDescribe().getRecordTypeInfos();

        Account a = new Account();
        a.Name = 'Acme1';
        insert a;

        Contact con = new Contact(LastName = 'testCon', AccountId = a.Id);
        Contact con1 = new Contact(LastName = 'testCon1', AccountId = a.Id);
        Contact con2 = new Contact(LastName = 'testCon1', AccountId = a.Id);
        Contact con3 = new Contact(LastName = 'testCon2', AccountId = a.Id);

        List<Contact> cons = new List<Contact>();
        cons.add(con);
        cons.add(con1);
        cons.add(con2);
        cons.add(con3);
        insert cons;
        
        User user = new User(Alias = 'test123', Email = 'test123fvb@noemail.com',
                             Emailencodingkey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',    
                             LocaleSidKey = 'en_US', ProfileId = pcoriCommunityPartID , Country='United States',IsActive = true,   
                             ContactId = con.Id, CommunityNickname = 'test12',
                             TimeZoneSidKey='America/New_York', UserName = 'testerfvb1@noemail.com');

        User user1 = new User(Alias = 'test1234', Email = 'test1234fvb@noemail.com',
                              Emailencodingkey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',    
                              LocaleSidKey = 'en_US', ProfileId = pcoriCommunityPartID , Country='United States',IsActive = true,   
                              ContactId = con1.Id, CommunityNickname = 'test12345',
                              TimeZoneSidKey='America/New_York', UserName = 'testerfvb123@noemail.com');

        User user2 = new User(Alias = 'test145', Email = 'test12345fvb@noemail.com',
                              Emailencodingkey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',    
                              LocaleSidKey = 'en_US', ProfileId = pcoriCommunityPartID , Country='United States',IsActive = true,   
                              ContactId = con2.Id, CommunityNickname = 'test14',
                              TimeZoneSidKey='America/New_York', UserName = 'testerfvb12@noemail.com');
        
        User user3 = new User(Alias = 'test52', Email = 'test135fvb@noemail.com',
                              Emailencodingkey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',    
                              LocaleSidKey = 'en_US', ProfileId = pcoriCommunityPartID , Country='United States',IsActive = true,   
                              ContactId = con3.Id, CommunityNickname = 'test5',
                              TimeZoneSidKey = 'America/New_York', UserName = 'testerfvb82@noemail.com');

        List<User> users = new List<User>();
        users.add(user);
        users.add(user1);
        users.add(user2);
        users.add(user3);
        insert users;
        
        Cycle__c c = new Cycle__c();
        c.Name = 'testcycle';
        c.COI_Due_Date__c = date.valueof(System.now());
        insert c;
        
        opportunity o = new opportunity();
        
        o.Awardee_Institution_Organization__c=a.Id;
        o.RecordTypeId = r1;
        o.Full_Project_Title__c='test';
        o.Project_Name_off_layout__c='test';
        o.CloseDate = Date.today();
        o.Application_Number__c='Ra-1234';
        o.StageName = 'Executed';
        o.Name='test';
        o.PI_Name_User__c = user.id;
        o.AO_Name_User__c = user1.id;
        o.Project_Start_Date__c = system.Today();
        
        insert o;

        Budget__c b = new Budget__c();
        b.Associated_App_Project__c= o.id;
        b.Bypass_Flow__c = true;
        insert b;

        //create data test insert for following three records
        Key_Personnel_Costs__c keyCostBudgetLine = new Key_Personnel_Costs__c();
        keyCostBudgetLine.Budget__c = b.id;
        keyCostBudgetLine.RecordTypeId = rKeyPersonnel;
        keyCostBudgetLine.Cost_Category__c = 'Key Personnel';
        keyCostBudgetLine.Year__c = 1;
        keyCostBudgetLine.Name = 'Henry Winkler';
        keyCostBudgetLine.Key__c = false;
        keyCostBudgetLine.Role_On_Project__c='Principal Investigator 1';
        keyCostBudgetLine.Percent_Effort__c =75.0;
        keyCostBudgetLine.Calendar_Months__c = 6;
        keyCostBudgetLine.Inst_Base_Salary__c = 10;
        keyCostBudgetLine.Salary_Requested__c = 50;
        keyCostBudgetLine.Fringe_Benefits__c = 20;

        Key_Personnel_Costs__c consultantCostBudgetLine = new Key_Personnel_Costs__c();
        consultantCostBudgetLine.Budget__c = b.id;
        consultantCostBudgetLine.RecordTypeId = rConsultantCost;
        consultantCostBudgetLine.Cost_Category__c = 'Consultant Cost';
        consultantCostBudgetLine.Year__c = 1;
        consultantCostBudgetLine.Description__c = 'Test class';
        consultantCostBudgetLine.Hourly_Unit_Rate__c = 10;
        consultantCostBudgetLine.Total_1__c= 100;

        Key_Personnel_Costs__c subContractorDirectBudgetLine = new Key_Personnel_Costs__c();
        subContractorDirectBudgetLine.Budget__c = b.id;
        subContractorDirectBudgetLine.RecordTypeId = rSubcontractorDirect;
        subContractorDirectBudgetLine.Cost_Category__c = 'Subcontractor Direct';
        subContractorDirectBudgetLine.Year__c = 1;
        subContractorDirectBudgetLine.Subcontractor_Name__c = 'Henry Winkler';
        subContractorDirectBudgetLine.Total_1__c = 100;

        //insert the three records
        List<Key_Personnel_Costs__c> lineItems = new List<Key_Personnel_Costs__c>();
        lineItems.add(keyCostBudgetLine);
        lineItems.add(consultantCostBudgetLine);
        lineItems.add(subContractorDirectBudgetLine);

        insert lineItems;

        Test.startTest();

        //test BudgetSummary and BudgetSummaryTableController
        BudgetSummaryTableController bt = new BudgetSummaryTableController(new ApexPages.StandardController(b));
        List<BudgetSummary> bs = bt.budgetSummaryList;
        BudgetSummary summaryRow = bs.get(0);
        System.assert(bs != null);
        System.assert(String.isNotBlank(summaryRow.category));
        for(String str : summaryRow.yearMap.values()){
            System.assert(String.isNotBlank(str));
        }
        System.assert(String.isNotBlank(summaryRow.rowTotals));
        System.assert(!bs.isEmpty());
        System.assert(bt.years.size() == Integer.valueOf(System.Label.Max_Years_Budget_Covers));
        System.assert(bt.hasPeerReviewPeriod);

        Test.stopTest();
    }
    
}