/****************************************************************************************************
*Description:           This Utility handles password encoding and decoding for Foundation Connect
*
*Required Class(es):    FCUtility_Test
*
*Organization: Rainmaker-LLC
*Revision   |Date       |Author             |AdditionalNotes
*====================================================================================================
*   1.0     03/21/2014   Justin Padilla      Initial Version
*****************************************************************************************************/
public class FCUtility {

    //Generates Foundation Connect Password
    public static string GeneratePassword(User u)
    {
        //string retval = '';
        //string salt = '';
        //salt += u.Email;
        //string temp = EncodingUtil.base64Encode(blob.valueOf(salt));
        //retval = EncodingUtil.urlEncode(temp,'UTF-8');
        //retval = 'Rain1234567890';
        //system.debug('Encoded: '+temp);
        //return retval;
        Integer len = 10;
        Blob blobKey = crypto.generateAesKey(128);
        String key = EncodingUtil.convertToHex(blobKey);
        String pwd = key.substring(0,len);
        System.debug('************ '+pwd);
        return pwd;
    }
    
    //Decodes the information within the encoded password in case this information is needed at a later time
    //Returns a string containing the UserName and Email Address seperated by a comma
    public static string DecodePassword(string s)
    {
        /*
        string urlDecode = EncodingUtil.urlDecode(s,'UTF-8');
        blob decoded = EncodingUtil.base64Decode(urlDecode);
        return decoded.toString();
        */
        return s;
    }
    
    @future
    public static void PasswordUpdate(Set<Id> userIds)
    {
        if (userIds == null) return;
        //Query for needed User Information
        //Map<Id,User> users = new Map<Id,User>([SELECT Id, UserName, Email, ContactId, Contact.FGM_Portal__Username__c, Contact.FGM_Portal__Password__c FROM User WHERE Id IN: userIds]);
        List<Contact> contactsToUpdate = new List<Contact>();
        //for (User u:users.values())
        for (User u:[SELECT Id, UserName, Email, ContactId, Contact.FGM_Portal__Username__c, Contact.FGM_Portal__Password__c,Contact.FGM_Portal__Confirm_Password__c FROM User WHERE Id IN: userIds])
        {
            system.debug('---------------->' + u);
            if (u.ContactId != null)
            {
                u.Contact.FGM_Portal__Username__c = u.Email.replace('@','');
                u.Contact.FGM_Portal__Password__c = FCUtility.GeneratePassword(u);
                u.Contact.FGM_Portal__Confirm_Password__c = u.Contact.FGM_Portal__Password__c;
                contactsToUpdate.add(u.Contact);
            }
        }
        if (!contactsToUpdate.isEmpty()) update(contactsToUpdate);
    }
}