@Istest
public class CommunityReportClassControl_Test{
    
    static testmethod void ipr(){
        
        List<FGM_Base__Grantee_Report__c> prlist= Testdatafactory.getProgressReportTestRecords(1, 6, 3, 2);
        List<FGM_Base__Benchmark__c> mslist= Testdatafactory.getMilestonesTestRecords(1, 6, 7, 1);
        /*ProcessInstanceStep pis= new ProcessInstanceStep();
pis.ActorId=prlist[0].PI_Sign_off_Name__c;
pis.StepStatus='Approved';
pis.ProcessInstance.TargetObjectId=prlist[0].id;
insert pis;*/
        
        apexpages.currentpage().getParameters().put('id',prlist[0].id);
        test.starttest();
        apexpages.standardcontroller stdcont = new apexpages.standardcontroller(prlist[0]);
        CommunityReportClassControl extcont = new CommunityReportClassControl(stdcont); 
        test.stopTest();
        extcont.getStaticTexts();
        extcont.getAdditiondoc();
        extcont.getuploadCohort();
        extcont.uploadCohort();
        
        
        // extcont.attacher();
        extcont.getfundingtrackerdoc();
        extcont.uploadFundingTracker();
        extcont.getclaimdatasummarydoc();
        extcont.uploadclaimdatasummary();
        extcont.getcollaborationTracker();
        extcont.uploadcollaborationTracker();
        extcont.getExpenditureReport();
        extcont.uploadExpenditureReport();
        extcont.getTable1();
        extcont.uploadTable1();
        extcont.NextSection();
        extcont.submit();
        extcont.getCanSubmit2();
        extcont.updatestudypartner();
        extcont.profileRedirection();
        extcont.BreadcrumbMethod(1);
        extcont.BreadcrumbPage1();
        extcont.BreadcrumbPage2();
        extcont.BreadcrumbPage3();
        extcont.BreadcrumbPage4();
        extcont.BreadcrumbPage5();
        extcont.BreadcrumbPage6();
        extcont.BreadcrumbPage7();
        extcont.BreadcrumbPage8();
        extcont.doStuff();
        extcont.next();
        extcont.back();
        extcont.quickSave();
        extcont.cancel();
        extcont.pdfView();
    }
    
    static testmethod void recruitment()
    {
        
        List<Account> accts = ObjectCreator.getAccts('Jimbo', 2);
        insert accts;
        
        List<User> users = ObjectCreator.getUsers(2, 'Donnie');
        insert users;
        
        List<Opportunity> opps = ObjectCreator.getOpps(2, 'CoolioHoolio');    
        insert opps;
        
        List<FGM_Base__Benchmark__c> milInserts = ObjectCreator.getMilestones('Jim', 12);
        insert milInserts;
        
        List<FGM_Base__Grantee_Report__c> repInserts = ObjectCreator.getReports(2);
        insert repInserts;
        
        CommunityReportClassControl contr = new CommunityReportClassControl();
        
        contr.MileMap.putAll(milInserts);
        contr.MileMapConstant.putAll(milInserts);
        contr.gRecordInserts = repInserts[0];
        contr.recordTypeName  = 'Final Progress Report - RA';
        PPRN_CDRN_text_setups__mdt statictext = contr.getStaticTexts();
        contr.AOFieldUpdater();
        contr.MilestoneDisplay();
        contr.MilestoneUpdate();
        contr.MilestonePages();
        contr.MileStoneBack();
        contr.PIFieldUpdater();
        contr.ProjData();
        contr.quickSave();
        contr.cancel();
        contr.cancelPagin();
        contr.linkToOlderReport();
        contr.pdfView();
        List<Recruitment__c> recinlist = new List<Recruitment__c>();
        List<Recruitment__c> recinlist2 = new List<Recruitment__c>();
        List<FGM_Base__Benchmark__c> mStoneList = new List<FGM_Base__Benchmark__c>(); 
        
        list<Account> acnt=TestDataFactory.getAccountTestRecords(1);
        list<contact> con=TestDataFactory.getContactTestRecords(1,1); 
        
        
        test.startTest();
        list<opportunity> opp=TestDataFactory.getresearchawardsopportunitytestrecords(1,5,1); 
        list<user> usr=TestDataFactory.getpartnerUserTestRecords(1,2);
        FGM_Base__Grantee_Report__c FGMtest = new FGM_Base__Grantee_Report__c();
        FGMtest.Project__c = opp[0].Id;
        FGMtest.FGM_Base__Status__c='Submitted';
        FGMtest.FGM_Base__Request__c = opp[0].Id;
        FGMtest.When_in_the_project_were_patients_and__c='what the study is about';
        FGMtest.PI_Sign_off_Name__c=usr[0].Id;
        FGMtest.AO_Sign_off_Name__c=usr[1].Id;
        insert FGMtest;
        
        FGM_Base__Benchmark__c ms= new FGM_Base__Benchmark__c();
        ms.Active__c=true;
        ms.FGM_Base__Request__c=opp[0].Id;
        ms.Progress_Report_Name__c=FGMtest.Id;
        ms.Milestone_Status__c='In Progress';
        ms.Milestone_Name__c='TestMilestonehirdwordfifthwordtest123'+' '+'test';
        ms.Milestone_ID2__c='1234567';
        ms.FGM_Base__Description__c='TestDescriptiondelayeddescription test againtest';
        ms.Reason_For_Delay__c='TestDelayedagaindelayed test';
        ms.FGM_Base__Due_Date__c=system.today()+7;
        mStoneList.add(ms);
        
        FGM_Base__Benchmark__c ms2= new FGM_Base__Benchmark__c();
        ms2.Active__c=true;
        ms2.FGM_Base__Request__c=opp[0].Id;
        ms2.Progress_Report_Name__c=FGMtest.Id;
        ms2.Milestone_Status__c='In Progress';
        ms2.Milestone_Name__c='Test MS2';
        ms2.Milestone_ID2__c='1234567';
        ms2.FGM_Base__Due_Date__c=system.today()+7;
        mStoneList.add(ms2);
        
        FGM_Base__Benchmark__c ms3= new FGM_Base__Benchmark__c();
        ms3.Active__c=true;
        ms3.FGM_Base__Request__c=opp[0].Id;
        ms3.Progress_Report_Name__c=FGMtest.Id;
        ms3.Milestone_Status__c='In Progress';
        ms3.Milestone_Name__c='Test MS3';
        ms3.Milestone_ID2__c='1234567';
        ms3.FGM_Base__Due_Date__c=system.today()+7;
        mStoneList.add(ms3);
        
        FGM_Base__Benchmark__c ms4= new FGM_Base__Benchmark__c();
        ms4.Active__c=true;
        ms4.FGM_Base__Request__c=opp[0].Id;
        ms4.Progress_Report_Name__c=FGMtest.Id;
        ms4.Milestone_Status__c='In Progress';
        ms4.Milestone_Name__c='Test MS4';
        ms4.Milestone_ID2__c='1234567';
        ms4.FGM_Base__Due_Date__c=system.today()+7;
        mStoneList.add(ms4);
        
        FGM_Base__Benchmark__c ms5= new FGM_Base__Benchmark__c();
        ms5.Active__c=true;
        ms5.FGM_Base__Request__c=opp[0].Id;
        ms5.Progress_Report_Name__c=FGMtest.Id;
        ms5.Milestone_Status__c='In Progress';
        ms5.Milestone_Name__c='Test MS5';
        ms5.Milestone_ID2__c='1234567';
        ms5.FGM_Base__Due_Date__c=system.today()+7;
        mStoneList.add(ms5);
        
        FGM_Base__Benchmark__c ms6= new FGM_Base__Benchmark__c();
        ms6.Active__c=true;
        ms6.FGM_Base__Request__c=opp[0].Id;
        ms6.Progress_Report_Name__c=FGMtest.Id;
        ms6.Milestone_Status__c='In Progress';
        ms6.Milestone_Name__c='Test MS6';
        ms6.Milestone_ID2__c='1234567';
        ms6.FGM_Base__Due_Date__c=system.today()+7;
        mStoneList.add(ms6);
        
        FGM_Base__Benchmark__c ms7= new FGM_Base__Benchmark__c();
        ms7.Active__c=true;
        ms7.FGM_Base__Request__c=opp[0].Id;
        ms7.Progress_Report_Name__c=FGMtest.Id;
        ms7.Milestone_Status__c='In Progress';
        ms7.Milestone_Name__c='Test MS7';
        ms7.Milestone_ID2__c='1234567';
        ms7.FGM_Base__Due_Date__c=system.today()+7;
        mStoneList.add(ms7);
        
        FGM_Base__Benchmark__c ms8= new FGM_Base__Benchmark__c();
        ms8.Active__c=true;
        ms8.FGM_Base__Request__c=opp[0].Id;
        ms8.Progress_Report_Name__c=FGMtest.Id;
        ms8.Milestone_Status__c='In Progress';
        ms8.Milestone_Name__c='Test MS8';
        ms8.Milestone_ID2__c='1234567';
        ms8.FGM_Base__Due_Date__c=system.today()+7;
        mStoneList.add(ms8);
        
        FGM_Base__Benchmark__c ms9= new FGM_Base__Benchmark__c();
        ms9.Active__c=true;
        ms9.FGM_Base__Request__c=opp[0].Id;
        ms9.Progress_Report_Name__c=FGMtest.Id;
        ms9.Milestone_Status__c='In Progress';
        ms9.Milestone_Name__c='Test MS9';
        ms9.Milestone_ID2__c='1234567';
        ms9.FGM_Base__Due_Date__c=system.today()+7;
        mStoneList.add(ms9);
        
        FGM_Base__Benchmark__c ms10= new FGM_Base__Benchmark__c();
        ms10.Active__c=true;
        ms10.FGM_Base__Request__c=opp[0].Id;
        ms10.Progress_Report_Name__c=FGMtest.Id;
        ms10.Milestone_Status__c='In Progress';
        ms10.Milestone_Name__c='Test MS10';
        ms10.Milestone_ID2__c='1234567';
        ms10.FGM_Base__Due_Date__c=system.today()+7;
        mStoneList.add(ms10);
        insert mStoneList;
        
        
        apexpages.currentpage().getParameters().put('id',FGMtest.id);
        apexpages.standardcontroller stdcont = new apexpages.standardcontroller(FGMtest); 
        CommunityReportClassControl extcont = new CommunityReportClassControl(stdcont);
        extcont.getclaimdatasummarydoc();
        extcont.uploadclaimdatasummary();
        extcont.getcollaborationTracker();
        extcont.uploadcollaborationTracker();
        extcont.updatetacetable();
        extcont.destroy=true;
        extcont.openSurvey();
        
        
        CommunityReportClassControl.WrapperStudyPartners objWrapperStudyPartners= new CommunityReportClassControl.WrapperStudyPartners();
        list<CommunityReportClassControl.WrapperStudyPartners> lstWrapperStudyPartners = new list<CommunityReportClassControl.WrapperStudyPartners>();
        objWrapperStudyPartners.NameofOrg='abc';
        objWrapperStudyPartners.Description='XYZ';
        lstWrapperStudyPartners.add(objWrapperStudyPartners);
        extcont.GenerateWrapperList(json.serialize(lstWrapperStudyPartners));
        extcont.GetStudyPartners();
        extcont.AddStudyPartnersRecord();
        extcont.cnt=0;
        extcont.deleteRow();
        test.stopTest();
    }
    
    static testmethod void ipr2(){
        
        List<FGM_Base__Grantee_Report__c> prlist= Testdatafactory.getProgressReportTestRecords2(1, 6, 3, 2);
        List<FGM_Base__Benchmark__c> mslist= Testdatafactory.getMilestonesTestRecords(1, 6, 7, 1);
        apexpages.currentpage().getParameters().put('id',prlist[0].id);
        test.starttest();
        apexpages.standardcontroller stdcont = new apexpages.standardcontroller(prlist[0]);
        CommunityReportClassControl extcont = new CommunityReportClassControl(stdcont); 
        test.stopTest();
        extcont.getStaticTexts();
        extcont.getAdditiondoc();
        extcont.getuploadCohort();
        extcont.uploadCohort();
        
        
        // extcont.attacher();
        extcont.getfundingtrackerdoc();
        extcont.uploadFundingTracker();
        extcont.getclaimdatasummarydoc();
        extcont.uploadclaimdatasummary();
        extcont.getcollaborationTracker();
        extcont.uploadcollaborationTracker();
        extcont.getExpenditureReport();
        extcont.uploadExpenditureReport();
        extcont.getTable1();
        extcont.uploadTable1();
        extcont.NextSection();
        extcont.submit();
        //extcont.getCanSubmit2();
        extcont.updatestudypartner();
        extcont.profileRedirection();
        extcont.BreadcrumbMethod(1);
        extcont.BreadcrumbPage1();
        extcont.BreadcrumbPage2();
        extcont.BreadcrumbPage3();
        extcont.BreadcrumbPage4();
        extcont.BreadcrumbPage5();
        extcont.BreadcrumbPage6();
        extcont.BreadcrumbPage7();
        extcont.BreadcrumbPage8();
        extcont.doStuff();
        extcont.next();
        extcont.next();
        extcont.next();
        extcont.next();
        extcont.next();
        extcont.next();
        extcont.next();
        extcont.back();
        extcont.back();
        extcont.back();
        extcont.back();
        extcont.back();
        extcont.back();
        extcont.back();
        extcont.quickSave();
        extcont.cancel();
        extcont.pdfView();
    }
    
    
    
}