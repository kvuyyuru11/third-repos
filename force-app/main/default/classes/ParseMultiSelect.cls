global class ParseMultiSelect
{

    @InvocableMethod
    public static List<String> ParseMultiSelect(List<String> optionsList)
    {
        
        list<String> ls= new list<String>();
        list<String> ls1= new list<String>();
        ls.addAll(optionsList);
        
        String finalString = '';
        String outputfinalstring ='';
        String firstResponse = '';
        String secondResponse = '';
        integer x=0;
        String str='';
        list<String> finalStringList = new list<String>();
        ls1 = ls[0].split(';');
        for(String str1 : ls1)
        {
            str='';
            str = str1.trim();
            if(!(str == 'Patient/consumer'||str == 'Caregiver/family member of patient'||str == 'Advocacy organization'||
                str == 'Clinician'||str == 'Clinic/ Hospital/ Health System'||str == 'Purchaser (small or large employers)'||
                str=='Payer (public or private insurance)'||str == 'Life sciences industry'||
                str == 'Policy maker (government official)'||str == 'Training institution (non-research health professions educator)'||
                str =='Subject matter expert - Please describe below'||str =='Other - Please describe below') && x==0) {
                firstResponse = str;
                system.debug('Here is the first response'+firstResponse);
                x=1;
            }
            else if(x==1) {
                secondResponse = str;
                system.debug('Here is the second response'+secondResponse);
                
            }
            if(x==0){
            
            finalString = finalString + str + ';';
            
            }
            
           }
           system.debug('Here is the final String'+finalString);
           
            system.debug('Here is the PDF' + finalString.contains('Subject matter expert - Please describe below'));
            if(finalString.contains('Subject matter expert - Please describe below')) {
                outputfinalstring = finalString.replace('Subject matter expert - Please describe below', 'Subject matter expert - Please describe below: '+firstResponse);
                
            }else {
                outputfinalstring = finalString;
            }
            system.debug('First output final String' + outputfinalstring);
            
             system.debug('Here is the PDF2' + finalString.contains('Other - Please describe below'));
            if(outputfinalstring.contains('Other - Please describe below')) {
                outputfinalstring = outputfinalstring.replace('Other - Please describe below', 'Other - Please describe below: '+secondResponse);
            }else {
                outputfinalstring = outputfinalstring;
            }
            system.debug('Second output final String' + outputfinalstring);
            
        finalStringList.add(outputfinalstring);
        return finalStringList ;
    }
}