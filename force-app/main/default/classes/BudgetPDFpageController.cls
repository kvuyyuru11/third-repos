/*-------------------------------------------------------------------------------------------------------------------------------------------------------------

Code Written at by - Pradeep Bokkala
Written as a Fix for Item SO-267
Test Class - Budget_Test
Parent Component - BudgetPDFpage
Used in senario(s) - This class is used as a controller for the BudgetPDFPage displays Budget information in PDF view  
------------------------------------------------------------------------------------------------------------------------------------------------------------*/


public class BudgetPDFpageController {
    
    //Instatiating all the variables to use them on the Page
    public boolean isBudgetLocked {get; set;}
    Public List<Budget__c> budget {get; set;}
    Public List<Key_Personnel_Costs__c> budgetLineItems {get; set;}
    Public Boolean noAppId{get;set;}
    public Boolean totalPrimeRow{get;set;}
    public Budget__c budgetPrime{get;set;}
    public String appId;
    public Integer travelBudget{get;set;}
    public string PFAName;
    public Map<string,List<Key_Personnel_Costs__c>> budgetLineItemsMap;
    public Map<decimal,List<Key_Personnel_Costs__c>> KeyPersonnelMap;
    public Map<decimal,List<Key_Personnel_Costs__c>> ConsultantCostMap;
    public Map<decimal,List<Key_Personnel_Costs__c>> SuppliesMap;
    public Map<decimal,List<Key_Personnel_Costs__c>> ScientificTravelMap; 
    public Map<decimal,List<Key_Personnel_Costs__c>> ProgrammaticTravelMap; 
    public Map<decimal,List<Key_Personnel_Costs__c>> OtherExpensesMap;
    public Map<decimal,List<Key_Personnel_Costs__c>> EquipmentMap;
    public Map<decimal,List<Key_Personnel_Costs__c>> SubcontractorDirectMap;
    public Map<decimal,List<Key_Personnel_Costs__c>> SubcontractorIndirectMap;
    public Map<decimal,List<Key_Personnel_Costs__c>> TotalPrimeIndirectMap;
    public Decimal Total1{get;set;}
    public Decimal Total2{get;set;}
    public Decimal Total3{get;set;}
    public Decimal Total4{get;set;}
    public Decimal Total5{get;set;}
    public Decimal Total6{get;set;}
    public Decimal Total7{get;set;}
    public Decimal Total8{get;set;}
    public Decimal Total9{get;set;}
    public Decimal Total10{get;set;}
    public Decimal Total11{get;set;}
    public Decimal Total12{get;set;}
    public Decimal Total13{get;set;}
    public list<Opportunity> OppList;
    public String YearValue{get;set;}
    
    public BudgetPDFpageController()
    {
        appId = ApexPages.currentPage().getParameters().get('id');
        if(appId!=null){
            grabListData();
        }
    }
    
    public void grabListData()
    {
        
        budget = [SELECT Id, Associated_App_Project__c, Name,PersonnelBudget_Year1_BT__c, 
                  End_Date_Year_1__c, End_Date_Year_2__c, End_Date_Year_3__c, End_Date_Year_4__c, End_Date_Year_5__c, End_Date_Year_6__c,
                  Start_Date_Year_1__c, Start_Date_Year_2__c, Start_Date_Year_3__c, Start_Date_Year_4__c, Start_Date_Year_5__c, Start_Date_Year_6__c,
                  Consultant_Costs_Total__c, Consultant_Costs_Year_1__c, Consultant_Costs_Year_2__c, Consultant_Costs_Year_3__c, 
                  Consultant_Costs_Year_4__c, Consultant_Costs_Year_5__c, Consultant_Costs_Year_6__c,
                  Direct_Subcontractor_Costs_Total__c, Direct_Subcontractor_Costs_Year_1__c, Direct_Subcontractor_Costs_Year_2__c,
                  Direct_Subcontractor_Costs_Year_3__c, Direct_Subcontractor_Costs_Year_4__c, Direct_Subcontractor_Costs_Year_5__c,
                  Direct_Subcontractor_Costs_Year_6__c,
                  Equipment_Costs_Total__c, Equipment_Costs_Year_1__c, Equipment_Costs_Year_2__c, Equipment_Costs_Year_3__c,
                  Equipment_Costs_Year_4__c, Equipment_Costs_Year_5__c, Equipment_Costs_Year_6__c,
                  Grand_Total_All_Years__c, Grand_Total_Year_1__c, Grand_Total_Year_2__c, Grand_Total_Year_3__c,
                  Grand_Total_Year_4__c, Grand_Total_Year_5__c, Grand_Total_Year_6__c,
                  Final_Grand_Total__c,
                  Other_Expenses_Total__c, Other_Expenses_Year_1__c, Other_Expenses_Year_2__c, Other_Expenses_Year_3__c, 
                  Other_Expenses_Year_4__c, Other_Expenses_Year_5__c, Other_Expenses_Year_6__c,
                  Personnel_Costs_Total__c, Personnel_Costs_Year_1__c, Personnel_Costs_Year_2__c, Personnel_Costs_Year_3__c,
                  Personnel_Costs_Year_4__c, Personnel_Costs_Year_5__c, Personnel_Costs_Year_6__c,
                  Programmatic_Travel_Costs_Total__c, Programmatic_Travel_Costs_Year_1__c, Programmatic_Travel_Costs_Year_2__c,
                  Programmatic_Travel_Costs_Year_3__c, Programmatic_Travel_Costs_Year_4__c, Programmatic_Travel_Costs_Year_5__c, 
                  Programmatic_Travel_Costs_Year_6__c,
                  Scientific_Travel_Costs_Total__c, Scientific_Travel_Costs_Year_1__c, Scientific_Travel_Costs_Year_2__c,
                  Scientific_Travel_Costs_Year_3__c, Scientific_Travel_Costs_Year_4__c, Scientific_Travel_Costs_Year_5__c,                        
                  Scientific_Travel_Costs_Year_6__c,
                  Subcontractor_Indirect_Costs_Total__c, Subcontractor_Indirect_Costs_Year_1__c, Subcontractor_Indirect_Costs_Year_2__c,
                  Subcontractor_Indirect_Costs_Year_3__c, Subcontractor_Indirect_Costs_Year_4__c, Subcontractor_Indirect_Costs_Year_5__c,                         
                  Subcontractor_Indirect_Costs_Year_6__c,
                  SubtotalDirectCosts_Total__c, Subtotal_Direct_Costs_Year_1__c, Subtotal_Direct_Costs_Year_2__c, Subtotal_Direct_Costs_Year_3__c,
                  Subtotal_Direct_Costs_Year_4__c, Subtotal_Direct_Costs_Year_5__c, Subtotal_Direct_Costs_Year_6__c,
                  Supplies_Costs_Total__c, Supplies_Costs_Year_1__c, Supplies_Costs_Year_2__c, Supplies_Costs_Year_3__c,
                  Supplies_Costs_Year_4__c, Supplies_Costs_Year_5__c, Supplies_Costs_Year_6__c,
                  Total_Direct_Costs_Total__c, Total_Direct_Costs_Year_1__c, Total_Direct_Costs_Year_2__c, Total_Direct_Costs_Year_3__c,
                  Total_Direct_Costs_Year_4__c, Total_Direct_Costs_Year_5__c, Total_Direct_Costs_Year_6__c,
                  Total_Prime_Indirect_Costs_Total__c, Total_Prime_Indirect_Costs_Year_1__c, Total_Prime_Indirect_Costs_Year_2__c,
                  Total_Prime_Indirect_Costs_Year_3__c, Total_Prime_Indirect_Costs_Year_4__c, Total_Prime_Indirect_Costs_Year_5__c, 
                  Total_Prime_Indirect_Costs_Year_6__c,
                  Applicable_Rate_Year_1__c, Applicable_Rate_Year_2__c, Applicable_Rate_Year_3__c, Applicable_Rate_Year_4__c,
                  Applicable_Rate_Year_5__c, Applicable_Rate_Year_6__c,PersonnelBudget_Year1_ST__c,PersonnelBudget_Year2_BT__c,
                  PersonnelBudget_Year2_ST__c,PersonnelBudget_Year4_ST__c,PersonnelBudget_Year4_BT__c,PersonnelBudget_Year5_BT__c,
                  PersonnelBudget_Year5_ST__c,PersonnelBudget_Year6_BT__c,PersonnelBudget_Year6_ST__c,PersonnelBudget_Year3_BT__c,
                  PersonnelBudget_Year3_ST__c
                  FROM Budget__c WHERE Associated_App_Project__c = :appId];
        
        budgetLineItems = [SELECT Name,Applicable_Rate__c,Budget__c,Calendar_Months__c,
                           Cost_Category__c,Description__c,End_Date__c,Fringe_Benefits__c,Fringe_Rate__c,Fringe_Rate2__c,Hourly_Unit_Rate__c,Inst_Base_Salary__c,Key__c,
                           Line_Item_Name__c,Modified_Direct_Cost__c,Peer_Review__c,Percent_Effort__c,record__c,Record_Count_Per_Year__c,Role_On_Project__c,
                           Roll_on_Project__c,Salary_Requested__c,Start_Date__c,Subcontractor_Name__c,SubTotal__c,Total_1__c,Total__c,Total2__c,Total_Indirect_Costs2__c,
                           Total_Indirect_Costs__c,Total_Prime_Indirect_Costs__c,Variable_Count__c,Year__c FROM Key_Personnel_Costs__c WHERE Budget__c = :budget[0].Id ORDER BY CreatedDate];
        
        OppList = new List<Opportunity>([Select Id,name,Campaign.Name from Opportunity where Id=:appId Limit 1]);
        
        
        try{
            //If the Campain Name to which the Application is Attached to is 'Dissemination and Implementation' update the YearValue to Year 6 
            if(OppList[0].Campaign.Name==Label.Dissemination_and_Implementation){
                YearValue = 'Year 6';
            }else{
                //If the Campain Name to which the Application is Attached to is not 'Dissemination and Implementation' update the YearValue to Peer Review Period
                YearValue = 'Peer Review Period';
            }
        }Catch(exception e){
            system.debug('===>'+e);
            YearValue = 'Peer Review Period';
        }
        if(budget.isEmpty())
        {
        }
        else
        {
            //Initialize the variables to calculate the totals by categoery
            budgetPrime = budget[0];
            totalPrimeRow = true;
            integer i=1;
            Total1=0;
            Total2=0;
            Total3=0;
            Total4=0;
            Total5=0;
            Total6=0;
            Total7=0;
            Total8=0;
            Total9=0;
            Total10=0;
            Total11=0;
            Total12=0;
            Total13=0;
            
            //Iterate through the records till the year value is 6 and add the totals to the variables to be displayed on the Page
            while(i<7){
                if(budget[0].get('Personnel_Costs_Year_'+i+'__c')!=null){
                    system.debug('here is the value'+budget[0].get('Personnel_Costs_Year_'+i+'__c'));
                    system.debug('here is the value'+Double.valueOf(budget[0].get('Personnel_Costs_Year_'+i+'__c')));
                    Total1 += Integer.valueOf(budget[0].get('Personnel_Costs_Year_'+i+'__c'));
                }
                if(budget[0].get('Consultant_Costs_Year_'+i+'__c')!=null){
                    Total2 += Integer.valueOf(budget[0].get('Consultant_Costs_Year_'+i+'__c'));
                }
                if(budget[0].get('Supplies_Costs_Year_'+i+'__c')!=null){
                    Total3 += Integer.valueOf(budget[0].get('Supplies_Costs_Year_'+i+'__c'));
                }
                if(budget[0].get('Scientific_Travel_Costs_Year_'+i+'__c')!=null){
                    Total4 += Integer.valueOf(budget[0].get('Scientific_Travel_Costs_Year_'+i+'__c'));
                }
                if(budget[0].get('Programmatic_Travel_Costs_Year_'+i+'__c')!=null){
                    Total5 += Integer.valueOf(budget[0].get('Programmatic_Travel_Costs_Year_'+i+'__c'));
                }
                if(budget[0].get('Programmatic_Travel_Costs_Year_'+i+'__c')!=null){
                    Total6 += Integer.valueof(budget[0].get('Other_Expenses_Year_'+i+'__c'));
                }
                if(budget[0].get('Equipment_Costs_Year_'+i+'__c')!=null){
                    Total7 += Integer.valueof(budget[0].get('Equipment_Costs_Year_'+i+'__c'));
                }
                if(budget[0].get('Equipment_Costs_Year_'+i+'__c')!=null){
                    Total8 += Integer.valueof(budget[0].get('Direct_Subcontractor_Costs_Year_'+i+'__c'));
                }
                if(budget[0].get('Equipment_Costs_Year_'+i+'__c')!=null){
                    Total9 += Integer.valueof(budget[0].get('Subtotal_Direct_Costs_Year_'+i+'__c'));
                }
                if(budget[0].get('Subcontractor_Indirect_Costs_Year_'+i+'__c')!=null){
                    Total10 += Integer.valueof(budget[0].get('Subcontractor_Indirect_Costs_Year_'+i+'__c'));
                }
                if(budget[0].get('Total_Direct_Costs_Year_'+i+'__c')!=null){
                    Total11 += Integer.valueof(budget[0].get('Total_Direct_Costs_Year_'+i+'__c'));
                }
                if(budget[0].get('Total_Prime_Indirect_Costs_Year_'+i+'__c')!=null){
                    Total12 += Integer.valueof(budget[0].get('Total_Prime_Indirect_Costs_Year_'+i+'__c'));
                }
                if(budget[0].get('Grand_Total_Year_'+i+'__c')!=null){
                    Total13 += Integer.valueof(budget[0].get('Grand_Total_Year_'+i+'__c'));
                }
                i++;
                
            }
        }
    }
    
    
    //Below Methods returns the Map of various BudgetlineItems which are later Iterated and displayed on tables on the Page
    
    Public Map<decimal,List<Key_Personnel_Costs__c>> getKeyPersonnel() {
        KeyPersonnelMap = new Map<decimal,List<Key_Personnel_Costs__c>>();
        for(Key_Personnel_Costs__c bli : budgetLineItems){
            if(bli.Cost_Category__c=='Key Personnel'){
                if(KeyPersonnelMap.containskey(bli.Year__c))
                {
                    KeyPersonnelMap.get(bli.Year__c).add(bli);
                }
                else{
                    
                    KeyPersonnelMap.put(bli.Year__c,new lIST<Key_Personnel_Costs__c> {bli});
                }
            }  
        }
        return KeyPersonnelMap;
    }
    
    
    Public Map<decimal,List<Key_Personnel_Costs__c>> getConsultantCost() {
        ConsultantCostMap = new Map<decimal,List<Key_Personnel_Costs__c>>();
        for(Key_Personnel_Costs__c bli : budgetLineItems){
            if(bli.Cost_Category__c=='Consultant Cost'){
                if(ConsultantCostMap.containskey(bli.Year__c))
                {
                    ConsultantCostMap.get(bli.Year__c).add(bli);
                }
                else{
                    
                    ConsultantCostMap.put(bli.Year__c,new lIST<Key_Personnel_Costs__c> {bli});
                }
            }  
        }
        return ConsultantCostMap;
    }
    
    Public Map<decimal,List<Key_Personnel_Costs__c>> getSupplies() {
        SuppliesMap = new Map<decimal,List<Key_Personnel_Costs__c>>();
        for(Key_Personnel_Costs__c bli : budgetLineItems){
            if(bli.Cost_Category__c=='Supplies'){
                if(SuppliesMap.containskey(bli.Year__c))
                {
                    SuppliesMap.get(bli.Year__c).add(bli);
                }
                else{
                    
                    SuppliesMap.put(bli.Year__c,new lIST<Key_Personnel_Costs__c> {bli});
                }
            }  
        }
        return SuppliesMap;
    }
    
    Public Map<decimal,List<Key_Personnel_Costs__c>> getScientificTravel() {
        ScientificTravelMap = new Map<decimal,List<Key_Personnel_Costs__c>>();
        for(Key_Personnel_Costs__c bli : budgetLineItems){
            if(bli.Cost_Category__c=='Scientific Travel'){
                if(ScientificTravelMap.containskey(bli.Year__c))
                {
                    ScientificTravelMap.get(bli.Year__c).add(bli);
                }
                else{
                    
                    ScientificTravelMap.put(bli.Year__c,new lIST<Key_Personnel_Costs__c> {bli});
                }
            }  
        }
        return ScientificTravelMap ;
    }
    
    Public Map<decimal,List<Key_Personnel_Costs__c>> getProgrammaticTravel() {
        ProgrammaticTravelMap = new Map<decimal,List<Key_Personnel_Costs__c>>();
        for(Key_Personnel_Costs__c bli : budgetLineItems){
            if(bli.Cost_Category__c=='Programmatic Travel'){
                if(ProgrammaticTravelMap.containskey(bli.Year__c))
                {
                    ProgrammaticTravelMap.get(bli.Year__c).add(bli);
                }
                else{
                    
                    ProgrammaticTravelMap.put(bli.Year__c,new lIST<Key_Personnel_Costs__c> {bli});
                }
            }  
        }
        return ProgrammaticTravelMap;
    }
    
    Public Map<decimal,List<Key_Personnel_Costs__c>> getOtherExpenses() {
        OtherExpensesMap = new Map<decimal,List<Key_Personnel_Costs__c>>();
        for(Key_Personnel_Costs__c bli : budgetLineItems){
            if(bli.Cost_Category__c=='Other Expenses'){
                if(OtherExpensesMap.containskey(bli.Year__c))
                {
                    OtherExpensesMap.get(bli.Year__c).add(bli);
                }
                else{
                    
                    OtherExpensesMap.put(bli.Year__c,new lIST<Key_Personnel_Costs__c> {bli});
                }
            }  
        }
        return OtherExpensesMap;
    }
    
    Public Map<decimal,List<Key_Personnel_Costs__c>> getEquipment() {
        EquipmentMap = new Map<decimal,List<Key_Personnel_Costs__c>>();
        for(Key_Personnel_Costs__c bli : budgetLineItems){
            if(bli.Cost_Category__c=='Equipment'){
                if(EquipmentMap.containskey(bli.Year__c))
                {
                    EquipmentMap.get(bli.Year__c).add(bli);
                }
                else{
                    
                    EquipmentMap.put(bli.Year__c,new lIST<Key_Personnel_Costs__c> {bli});
                }
            }  
        }
        return EquipmentMap;
    }
    
    
    Public Map<decimal,List<Key_Personnel_Costs__c>> getSubcontractorDirect() {
        SubcontractorDirectMap = new Map<decimal,List<Key_Personnel_Costs__c>>();
        for(Key_Personnel_Costs__c bli : budgetLineItems){
            if(bli.Cost_Category__c=='Subcontractor Direct'){
                if(SubcontractorDirectMap.containskey(bli.Year__c))
                {
                    SubcontractorDirectMap.get(bli.Year__c).add(bli);
                }
                else{
                    
                    SubcontractorDirectMap.put(bli.Year__c,new lIST<Key_Personnel_Costs__c> {bli});
                }
            }  
        }
        return SubcontractorDirectMap;
    }
    
    
    
    Public Map<decimal,List<Key_Personnel_Costs__c>> getSubcontractorIndirect() {
        SubcontractorIndirectMap= new Map<decimal,List<Key_Personnel_Costs__c>>();
        for(Key_Personnel_Costs__c bli : budgetLineItems){
            if(bli.Cost_Category__c=='Subcontractor Indirect'){
                if(SubcontractorIndirectMap.containskey(bli.Year__c))
                {
                    SubcontractorIndirectMap.get(bli.Year__c).add(bli);
                }
                else{
                    
                    SubcontractorIndirectMap.put(bli.Year__c,new lIST<Key_Personnel_Costs__c> {bli});
                }
            }  
        }
        return SubcontractorIndirectMap;
    }
    
}