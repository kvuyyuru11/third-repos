/* This class belongs to Trigger TotalTaskCount. This class will count Total number of open tasks under Milestone Object.*/
public without sharing class ActivityUtils {
 
    //config
    Private String fieldToUpdate = System.label.Total_Open_Tasks; 
 
    //state
    Private set<id> positionIds;
 /*Constructor for class which passes array of sObject */
    public ActivityUtils(sObject[] records) {
        positionIds = new set<id>();
        captureWhatAndWhoIds(records);
        //addAccountIdsFromRlatedObjects();
    }
 /*Method to update the Activity count*/
    public void updatePositionActivityCount() {
    try{
     if(positionIds.size() == 0) {
        return;
        }
        updateActivityCount(system.label.FGM_Base_Benchmark,System.label.WhatId, getStringFromIdSet(positionIds));
     }catch(dmlexception e) {
        //System.debug('The following exception has occurred: ' + e.getMessage());

     }   
     }//Method
  /* updateActivityCount Method to update Total number of open tasks */  
    private void updateActivityCount(String objToUpdate, String queryFld, String updateIds) {
        
        string strQuery = System.Label.selectValue+ ' ' + objToUpdate + ' ' +System.Label.whereCondition + updateIds + System.Label.closeBracket;
        sObject[] sobjects = new list<sobject>();
        for(sObject so : database.query(strQuery)) {
            OpenActivity[] oActivities = so.getSObjects(System.label.OpenActivities);
            Integer openActivityCount; //= oActivities == null ? 0 : oActivities.size();
            if(oActivities == null)
            {
              openActivityCount = 0;
            }else
            {
            openActivityCount = oActivities.size();
            }
            sObject obj = createObject(objToUpdate, so.Id);
            obj.put(fieldToUpdate, openActivityCount);
            sobjects.add(obj);
            //system.debug('openActivityCount: ' + openActivityCount);
        }
        update sobjects;
    }
 /*captureWhatAndWhoIds Method to capture whatId and whoIds */
    private void captureWhatAndWhoIds(sObject[] objects) {
        for(sObject o : objects) {
            Id whatId = (Id)o.get(System.label.WhatId);
            Id whoId = (Id)o.get(System.label.WhoId);
            if(whatId != null) {
                String objectName = system.label.FGM_Base_Benchmark;
                  if(objectName.equalsIgnoreCase(system.label.FGM_Base_Benchmark))
                    {
                       positionIds.add(whatId);
                    } //end if
               }//end if
        }//end for
    }
 
 /* getStringFromIdSet Method to convert Id into string */
    private String getStringFromIdSet(set<id> idSet) {
        string idString = ''; String valueNull ='';
        for(Id i : idSet) 
        {
        idString+= '\'' + i + '\',';
        
        }
        if(idString.equalsIgnoreCase(valueNull)){
               return idString;
        }else{
              return idString.left(idString.length()-1);
         }  
        
    }
 
      
    private sObject createObject(String typeName, Id objId) {
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(typeName);
        if (targetType == null) {
            // throw an exception
        }
 
        // Instantiate an sObject with the type passed in as an argument
        //  at run time.
        return targetType.newSObject(objId);
    }
 
}