@isTest
public class PopulateKeyProjectPersonnelEmailTest {
    private static testMethod void testAudit(){
        User usr;
        User usr1;
        RecordType recOpp = [SELECT Id FROM RecordType WHERE sObjectType = 'Opportunity' AND Name = 'RA Applications'];
        RecordType recAcc = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Standard Salesforce Account'];
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser){
            Profile p = [SELECT Id FROM Profile WHERE Name='Science Program Operations'];
            UserRole r = [SELECT Id FROM UserRole WHERE Name='Science Method Effectiveness Research Team'];
            usr = new User(alias = 'jonna', email='jonnasmith@acme.com',
                emailencodingkey='UTF-8', lastname='Smith',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
                timezonesidkey='America/New_York',
                username='jonnasmith@acme.com');
            insert usr;
            usr1 = new User(alias = 'jonna', email='jonnasmith@acme.com',
                emailencodingkey='UTF-8', lastname='Smith',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
                timezonesidkey='America/New_York',
                username='vk678jonnasmith@acme.com');
            insert usr1;
            
            Account acc = new Account();
            acc.RecordTypeId = recAcc.Id;
            acc.Name = 'Test Acc';
            insert acc;
            
            Opportunity opp = new Opportunity();
            opp.RecordTypeId = recOpp.Id;
            opp.Awardee_Institution_Organization__c = acc.Id;
            opp.Full_Project_Title__c = 'test0';
            opp.Project_Name_off_layout__c='test0';
            opp.Name = 'Test';
            opp.CloseDate = Date.newInstance(2016, 12, 31);
            opp.Application_Number__c = '45125';
            opp.StageName = 'In Progress';
            opp.AO_Name_User__c = usr1.Id;
            opp.PI_Project_Lead_Designee_1_New__c = usr.Id;
            opp.PI_Name_User__c = usr.Id;
             
            insert opp;
            
            Audit__c a = new Audit__c();
            a.Project__c = opp.Id;
            a.Status__c = 'Draft';
            insert a;
        }
    }
    private static testMethod void testAuditNull(){
        User usr;
        RecordType recOpp = [SELECT Id FROM RecordType WHERE sObjectType = 'Opportunity' AND Name = 'RA Applications'];
        RecordType recAcc = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Standard Salesforce Account'];
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser){
            Profile p = [SELECT Id FROM Profile WHERE Name='Science Program Operations'];
            UserRole r = [SELECT Id FROM UserRole WHERE Name='Science Method Effectiveness Research Team'];
            usr = new User(alias = 'jonna', email='jonnasmith@acme.com',
                emailencodingkey='UTF-8', lastname='Smith',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
                timezonesidkey='America/New_York',
                username='jonnasmith1@acme.com');
            insert usr;
            
            Account acc = new Account();
            acc.RecordTypeId = recAcc.Id;
            acc.Name = 'Test Acc';
            insert acc;
            
            Opportunity opp = new Opportunity();
            opp.RecordTypeId = recOpp.Id;
            opp.Awardee_Institution_Organization__c = acc.Id;
            opp.Full_Project_Title__c = 'test0';
            opp.Project_Name_off_layout__c='test0';
            opp.Name = 'Test';
            opp.CloseDate = Date.newInstance(2016, 12, 31);
            opp.Application_Number__c = '45125';
            opp.StageName = 'In Progress';
            insert opp;
            
            Audit__c a = new Audit__c();
            a.Project__c = opp.Id;
            a.Status__c = 'Draft';
            insert a;
        }
        
    }
}