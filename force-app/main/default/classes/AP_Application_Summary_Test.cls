@isTest
public  with sharing class AP_Application_Summary_Test {
public static testMethod void Mytest(){
  PageReference pageRef = new pagereference(label.AP_Application_3); //replace with your VF page name
  Test.setCurrentPage(pageRef);
    
  // String profileId = Label.CommunityAdminAccount;
         Profile Profile1 = [SELECT Id, Name FROM Profile WHERE Name = 'PCORI Community Partner'];
         String profileId = Profile1.id;
      String accountId = Label.CommunityAdminProfile;
      
      //Profile CProfile = [SELECT Id, Name FROM Profile WHERE Name ='Customer Community Login User'];
      Profile AProfile = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator'];
      //User u = [SELECT Id FROM User WHERE ProfileId =:AProfile.Id AND isActive = true LIMIT 1];
      User u = [SELECT Id FROM User WHERE ProfileId =:AProfile.Id AND isActive = true AND UserRoleId!=null LIMIT 1];
      User temp = new User();
      User user2 = new User();
    
    system.runAs(u){ 
    Account account = new Account(Name='Test');
        insert(account);
        Contact contact = new Contact(FirstName='Rainmaker', LastName='Test', AccountId=account.Id);
        insert(contact);
        temp.Email = 'testing1dfgy612@test.com';
        temp.ContactId = contact.Id;
        temp.UserName = 'testing1dfgy612@test.com';
        temp.LastName = 'Padilla';
        temp.Alias = 'jpadille';
        temp.CommunityNickName = 'jpadilla';
        temp.TimeZoneSidKey = 'GMT';
        temp.LocaleSidKey = 'en_US';
        temp.EmailEncodingKey = 'ISO-8859-1';
        temp.ProfileId = profileId;
        temp.LanguageLocaleKey = 'en_US';
       
        String userId = Site.createPortalUser(temp, accountId, 'Testing');
        
        Contact contact1 = new Contact(FirstName='Rainmaker1', LastName='Test1', AccountId=account.Id,Reviewer_Status__c='New Reviewer');
        insert(contact1); 
        
    }
   test.startTest(); 
   
    System.runAs(temp)
    {
  
    
APApplicationSummary testCon = new APApplicationSummary();
   
        
    testCon.Previousmethod();
    //testCon.saveMethod();
   // testCon.Nextmethod();
   //testCon.updateMe();
        
      
    }  
    
        
    System.runAs(temp)
    {
        APApplicationSummary testCon = new APApplicationSummary();
    testCon.apa = new Advisory_Panel_Application__c();    
    testCon.apa.Personal_Statement2__c = 'I like to test';
    testCon.apa.Bio__c = 'I like to test';   
    testCon.apa.Bio__c = 'I like to test';  
    testCon.apa.Permission_to_post_bios_on_website__c = 'YES';
    testCon.apa.Please_tell_us_anything_else__c = 'I love to test';
    testCon.apa.Additional_Supportive_Information__c = 'I love to test';
        
        
        
        
    }
    
    System.runAs(temp){
        
        Contact contact1 = [select id,firstname,lastName,AccountID from Contact where lastName =: 'Test1'];

        Advisory_Panel_Application__c app1 = new Advisory_Panel_Application__c();
        app1.Application_Status__c = 'Draft';
        Advisory_Panel_Application__c myApp2 = new Advisory_Panel_Application__c();

        app1.Contact__c = contact1.id;
        app1.Application_Status_new__c ='Draft';
        
        
        insert app1;
        
       APApplicationSummary testCon = new APApplicationSummary();
        testCon.apa.id = app1.id;
        testCon.submit();
		testCon.fullName = '';
        testcon.cntct = null;
        
        
        
        
    }
    
    System.runAs(temp)
        
    {
        Advisory_Panel_Application__c myApp2 = new Advisory_Panel_Application__c();
        myApp2.Application_Status_new__c ='Draft';
  //      insert myApp2;
        
        
        
    }
     test.stopTest();
}
   
}