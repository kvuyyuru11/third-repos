//This is a Class that routes a user to different COI views based on Profile
public class COIRouterClass1 {
PageReference pageref;
    Id recdid;
    string PName = Label.Single_Community_Profile;
   profile p=[select id from Profile where name=:PName];
Id MRAid=p.id; 
 

public COIRouterClass1(ApexPages.StandardController controller){}
    //This is a method that routes to the standard page layout edit view or VFpage edit view based on profile
public pagereference router4(){
    //Gets the record id
       recdid =ApexPages.currentPage().getParameters().get('Id');
     RecordType objRecordType=  [SELECT Id,name FROM RecordType where name =: 'Awardee COI'];
    RecordType gnrlrecordtype=  [SELECT Id,name FROM RecordType where name =: 'General Record Type'];
   COI_Expertise__c objCOI_Expertise= new   COI_Expertise__c();
    if(recdid!=null){
   objCOI_Expertise=[ select id,RecordTypeId,Related_Project__c from COI_Expertise__c where id=:recdid ];
    
    }
    
    //Checks the profile id and routes to the page required
    String url1;
if (UserInfo.getProfileId().equals(MRAid) && objCOI_Expertise.RecordTypeId==gnrlrecordtype.Id) 
    {
          

         url1 = '/apex/COIPage?id='+recdid;
        
        
       }
    
   else if (UserInfo.getProfileId().equals(MRAid) && objCOI_Expertise.RecordTypeId==objRecordType.Id) 
    {
       url1='/apex/COIReviewer?Pid='+objCOI_Expertise.Related_Project__c+'&coid='+recdID;            

        
       }
   
    else{
         url1='/'+recdID+'/e?nooverride=1&retURL='+recdID;
   
        }
      pageRef = new PageReference(url1);
pageref.setredirect(True);
    return pageRef;
}

}