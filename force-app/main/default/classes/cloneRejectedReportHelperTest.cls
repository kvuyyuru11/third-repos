@isTest
public class cloneRejectedReportHelperTest {
    
    static testmethod void testmethodforclonereject(){
        
                Id r3 = Schema.SObjectType.FGM_Base__Grantee_Report__c.getRecordTypeInfosByName().get('Interim Progress Report - RA').getRecordTypeId();

        list<opportunity> opplist=testdatafactory.getresearchawardsopportunitytestrecords(1, 7, 1);
      
         FGM_Base__Grantee_Report__c f= new FGM_Base__Grantee_Report__c();

                 f.FGM_Base__Status__c='Open';
                     f.Start_Date__c=system.today();
                     f.FGM_Base__Due_Date__c=system.today()+30;
        f.Progress_Report_Name__c='Test Progess report';
                     f.RecordTypeId=r3;
     
        f.FGM_Base__Request__c=opplist[0].Id;  
        insert f;

       // prlist[0].FGM_Base__Status__c='PCORI rejected: Returned for revisions';
      
        opplist[0].Clone_Report_ID__c=f.Id;
       
        opplist[0].Reject_Temp__c=True;
        update opplist[0];
        cloneRejectedReportHelper.cloneRejectedMethod(opplist);
        
    }

}