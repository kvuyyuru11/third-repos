/**
 * @File Name          : ContentVersionTestClass.cls
 * @Description        :
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              :
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 6/28/2019, 3:57:38 PM
 * @Modification Log   :vcsdvdv
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    6/14/2019, 2:52:13 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
//First Change to First Branch
//Trying to do second change commit
//Third time to change
//4th
//5th
@IsTest
public with sharing class ContentVersionTestClass {
@TestSetup
static void makeData(){
    CollaborationGroup cg = new CollaborationGroup(Name = 'Files Sharing Group to test', CollaborationType = 'Public');
        insert cg;
        system.debug('abc');
//CollaborationGroup FilessharingGroup=[SELECT Id,Name From CollaborationGroup WHERE Name=:Label.Group_Name_for_Files_Sharing];
//list<CollaborationGroupMember> collaborationgroupmmbrslist= [select id,memberid from CollaborationGroupMember where CollaborationGroupId=:FilessharingGroup.Id];
//system.assertNotEquals(NULL, collaborationgroupmmbrslist);
ContentVersion cv=new Contentversion();
cv.title='ABC';
cv.PathOnClient ='test';
cv.versiondata=EncodingUtil.base64Decode('This is version data');
insert cv;
ContentVersion cv1=[SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: cv.Id];
system.assertNotEquals(NULL, cv1.ContentDocumentId);
}
static testmethod void methodtotestifcontentversioninsertsharesfilestodelete(){
}

}