/*
* Apex Class : PIRRelatedListPageController
* Author : Kalyan Vuyyuru (PCORI)
* Version History : 1.0
* Description : This is a class that pulls the PIR's associated with the Project with the status of Letter Closed and returns the list to display on PIRRelatedListPage.
*/
public class PIRRelatedListPageController {
    public Id recid;
    public list<PIR__c> pr;
    public PIRRelatedListPageController(ApexPages.StandardController controller)
    {
        recid=apexpages.currentpage().getparameters().get('id');  
    }
    
    public List<PIR__c> getPIRS()
    {
        pr=[select id,Name,Application_Number__c,PIR_Response_Deadline__c,PIR_Complete__c,Application__c from PIR__c where Application__c=:recid AND PIR_Status__c=:'Letter Closed'];
        return pr;
    }
}