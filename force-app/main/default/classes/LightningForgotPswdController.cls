global class LightningForgotPswdController {
    
    @AuraEnabled
      public static String forgotpswd(String username) {
        try{
            boolean success = Site.forgotPassword(username);
            
            return null;
        }
        catch (Exception ex) {
            return ex.getMessage();            
        }
    }

}