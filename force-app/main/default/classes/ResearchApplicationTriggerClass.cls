//This is a TriggerHandler Class that handles the methods on the ResearchApplication Object
public class ResearchApplicationTriggerClass {
public ResearchApplicationTriggerClass(){}
      
    //This method copies the technical abstract on Research Application to the COI associated to it.
        public void updateCOItechnicalabstract(list<Opportunity> rapl)
    {
        list<COI_Expertise__c> coi= new list<COI_Expertise__c>();
        Map<Id,string> TechnicalabstractMap= new Map<Id,string>();
        Map<Id,string> TechnicalabstractMap1= new Map<Id,string>();


        for(opportunity o:rapl){
            TechnicalabstractMap.put(o.Id, o.Technical_Abstract__c);
            TechnicalabstractMap1.put(o.Id, o.Full_Project_Title__c);
        }
       
        
        
//Below is the soql query to get the COI records from that particular research application
 list<COI_Expertise__c> c=[select id,Project_Title_New__c from COI_Expertise__c where Related_Project__c IN :rapl];
  //Below for loop assigns the technical abstract from Research Application to respective COI's
    System.debug('List size is '+c.size());
            for(COI_Expertise__c ci:c)
    {
  //ci.Technical_Abstract__c=TechnicalabstractMap.get(ci.Related_Project__c);
        ci.Project_Title_New__c=TechnicalabstractMap1.get(ci.Related_Project__c);
    coi.add(ci);
    }

     update coi;
   
        }
    
//     public void InsertprincipalInvestigator(list<Research_Application__c> rapl1)
     public void InsertprincipalInvestigator(list<Opportunity> rapl1)

    {
        
//        list<Research_Key__c> rkp= new list<Research_Key__c>();    
//        for(Research_Application__c rn:rapl1){
//        Research_Key__c rk= new Research_Key__c();

        list<Key_Personnel__c> rkp= new list<Key_Personnel__c>();    
        for(Opportunity rn:rapl1){
            Key_Personnel__c rk= new Key_Personnel__c();
//        rk.Key_Personnel_Organization__c=rn.Program_Organization__c;
        rk.Institution_Org__c=rn.Awardee_Institution_Organization__c;
        rk.Role__c='Principal Investigator';
        rk.Opportunity__c=rn.Id;
        rk.Degree_Other__c=rn.PI_Name_User__c;
        rkp.add(rk);
        }
     //database.insert(rkp);
        
        
    }
     
}