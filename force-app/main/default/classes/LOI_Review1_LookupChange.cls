/*----------------------------------------------------------------------------------------------------------------------------------------
Code Written at by SADC PCORI Team: Matt Hoffman
Written During Development Phase 2: June/2016 - September/2016
Application ID: AI-0615
Requirement ID: RAr2_C1_007

This Invokable class/method is one of the last steps in keeping the Rich Text fields on Lead populated with reviews summary information.
If one of the picklists are changed (indicating that another review's info should be pulled up) then we match the picklist value to the review
number.
The reason this is so tricky is because of the Process Builders on Review-- as long as the review meets the requirements of those processes,
its information will get pulled up and refresh the "roll-up" (its not really a roll-up) Rich Text field. If the process builder ends up not firing, 
meaning it is (1.) Not marked as complete and (2.) not reccomended for an alternate PFA, then the Roll-up Rich text fields never get refreshed.
It is a rare case, but we have to check, otherwise information will not be updated correctly until the next time a review is saved.
This process is confusing and involves many steps. Feel free to reach out to me if there are any questions.
Note: There are 3 invokable classes like this that are almost exactly the same. One class per Lead look-up from Review.
---------------------------------------------------------------------------------------------------------------------------------------*/
public class LOI_Review1_LookupChange {

    @InvocableMethod 
    public static void updateReviewer1(List<Lead> leadList)
    { 
        List<Lead> updateList = new List<Lead>();
        List<Fluxx_LOI_Review_Form__c> updateListReviews = new List<Fluxx_LOI_Review_Form__c>();
        set<Id> LeadIds = new set<Id>();
        
        for(lead ll:leadList){
            LeadIds.add(ll.Id);
        }
        
         List<Fluxx_LOI_Review_Form__c> reviewsList = [SELECT id, Review_Number__c,Lead_LOI__c FROM Fluxx_LOI_Review_Form__c WHERE Lead_LOI__c IN:LeadIds Limit 50000];
        
        for (Lead theLead : leadList)
        {   
            Lead updatedLead = theLead.clone(false, true);
            updatedLead.id = theLead.id;
            boolean reviewMathced = false;
            
            for (Fluxx_LOI_Review_Form__c theReview : reviewsList) 
            {
            
                if(theReview.Lead_LOI__c == theLead.Id){
                if (String.valueOf(theReview.Review_Number__c)  == theLead.Review_Roll_up_1__c)
                {
                    // need to update the first lookup on lead to this review       

                    updatedLead.LOI_Review1__c = theReview.id;                   
                    reviewMathced = true;
                }
                updateListReviews.add(theReview);
            }
            }
            if (!reviewMathced) 
            {
                updatedLead.LOI_Review1__c = null;
            }

            // The "Final Comments for Public Reply" and the "Review Status" (and the PFA Alternate Update) on the lead will not actually update unless the process on Review 
            // pushes it to do so. That is why we are inserting reviews, so that the conditions will be met and the process builder will catch it even though nothing on Review
            // is being updated in this process. The reason all of this is happening is because Rich text fields are needed and formulas don't recognize them so we can't just simply
            // roll up the fields with a big formula.
            // However, if the conditions are not met for this process builder then these fields on Lead will never get updated. So we have to do here what the
            // process builder would normally do because of the chance of it not updating if the process conditions are not met.
            /*String finalComments = '';
            String reviewsDecision = '';
            String proposedPFA = '';
            boolean recommendedAlternatePFA = false;
            
            
            if (!String.isBlank(updatedLead.LOI_Review1__c))
            {
                // LOI Final Decision
                if (updatedLead.LOI_Review1__r.Review_Completed__c == true && (updatedLead.RecordTypeDevName__c == 'RA_LOI' || updatedLead.RecordTypeDevName__c == 'RA_LOI_Methods'))
                {

                    finalComments += updatedLead.LOI_Review1__r.Reviewer_Label_RA__c + ': ' + updatedLead.LOI_Review1__r.Science_Justification_Screen__c + '\n' + '\n' + '\n';
                    reviewsDecision += updatedLead.LOI_Review1__r.Reviewer_Label_RA__c + ': ' + updatedLead.LOI_Review1__r.Final_Decision__c + '\n';
                }
                else
                {
                    finalComments += '\n';
                    reviewsDecision += '\n';
                }
                
                // LOI Alternate PFA Update
                if ((updatedLead.LOI_Review1__r.Programmatically_Responsive__c == 'No' && updatedLead.LOI_Review1__r.If_No_can_it_move_to_alternate_PFA__c == 'Yes') 
                    && (updatedLead.RecordTypeDevName__c == 'RA_LOI' || updatedLead.RecordTypeDevName__c == 'RA_LOI_Methods'))
                {
                    proposedPFA += updatedLead.LOI_Review1__r.Reviewer_Label_RA__c + ': ' + updatedLead.LOI_Review1__r.If_Yes_then_which_PFA__c + '\n';
                    recommendedAlternatePFA = true;
                }
                else
                {
                    proposedPFA += '\n';
                }
            }
            if (!String.isBlank(updatedLead.LOI_Review2__c))
            {
           
                if (updatedLead.LOI_Review2__r.Review_Completed__c == true && (updatedLead.RecordTypeDevName__c == 'RA_LOI' || updatedLead.RecordTypeDevName__c == 'RA_LOI_Methods'))
                {
                    finalComments += updatedLead.LOI_Review2__r.Reviewer_Label_RA__c + ': ' + updatedLead.LOI_Review2__r.Science_Justification_Screen__c + '\n' + '\n' + '\n';
                    reviewsDecision += updatedLead.LOI_Review2__r.Reviewer_Label_RA__c + ': ' + updatedLead.LOI_Review2__r.Final_Decision__c + '\n';
                }
                else
                {
                    finalComments += '\n';
                    reviewsDecision += '\n';
                }
                
                // LOI Alternate PFA Update
                if ((updatedLead.LOI_Review2__r.Programmatically_Responsive__c == 'No' && updatedLead.LOI_Review2__r.If_No_can_it_move_to_alternate_PFA__c == 'Yes') 
                    && (updatedLead.RecordTypeDevName__c == 'RA_LOI' || updatedLead.RecordTypeDevName__c == 'RA_LOI_Methods'))
                {
                    proposedPFA += updatedLead.LOI_Review2__r.Reviewer_Label_RA__c + ': ' + updatedLead.LOI_Review2__r.If_Yes_then_which_PFA__c + '\n';
                    recommendedAlternatePFA = true;
                }
                else
                {
                    proposedPFA += '\n';
                }
            }
            if (!String.isBlank(updatedLead.LOI_Review3__c))
            {

                if (updatedLead.LOI_Review3__r.Review_Completed__c == true && (updatedLead.RecordTypeDevName__c == 'RA_LOI' || updatedLead.RecordTypeDevName__c == 'RA_LOI_Methods'))
                {
                    finalComments += updatedLead.LOI_Review3__r.Reviewer_Label_RA__c + ': ' + updatedLead.LOI_Review3__r.Science_Justification_Screen__c + '\n' + '\n' + '\n';
                    reviewsDecision += updatedLead.LOI_Review3__r.Reviewer_Label_RA__c + ': ' + updatedLead.LOI_Review3__r.Final_Decision__c + '\n';
                }
                else
                {
                    finalComments += '\n';
                    reviewsDecision += '\n';
                }
                
                // LOI Alternate PFA Update
                if ((updatedLead.LOI_Review3__r.Programmatically_Responsive__c == 'No' && updatedLead.LOI_Review3__r.If_No_can_it_move_to_alternate_PFA__c == 'Yes') 
                    && (updatedLead.RecordTypeDevName__c == 'RA_LOI' || updatedLead.RecordTypeDevName__c == 'RA_LOI_Methods'))
                {
                    proposedPFA += updatedLead.LOI_Review3__r.Reviewer_Label_RA__c + ': ' + updatedLead.LOI_Review3__r.If_Yes_then_which_PFA__c + '\n';
                    recommendedAlternatePFA = true;
                }
                else
                {
                    proposedPFA += '\n';
                }
            }
            if (!String.isBlank(updatedLead.LOI_Review4__c))
            {
                if (updatedLead.LOI_Review4__r.Review_Completed__c == true && (updatedLead.RecordTypeDevName__c == 'RA_LOI' || updatedLead.RecordTypeDevName__c == 'RA_LOI_Methods'))
                {
                    finalComments += updatedLead.LOI_Review4__r.Reviewer_Label_RA__c + ': ' + updatedLead.LOI_Review4__r.Science_Justification_Screen__c + '\n' + '\n' + '\n';
                    reviewsDecision += updatedLead.LOI_Review4__r.Reviewer_Label_RA__c + ': ' + updatedLead.LOI_Review4__r.Final_Decision__c + '\n';
                }
                else
                {
                    finalComments += '\n';
                    reviewsDecision += '\n';
                }
                
                // LOI Alternate PFA Update
                if ((updatedLead.LOI_Review4__r.Programmatically_Responsive__c == 'No' && updatedLead.LOI_Review4__r.If_No_can_it_move_to_alternate_PFA__c == 'Yes') 
                    && (updatedLead.RecordTypeDevName__c == 'RA_LOI' || updatedLead.RecordTypeDevName__c == 'RA_LOI_Methods'))
                {
                    proposedPFA += updatedLead.LOI_Review4__r.Reviewer_Label_RA__c + ': ' + updatedLead.LOI_Review4__r.If_Yes_then_which_PFA__c + '\n';
                    recommendedAlternatePFA = true;
                }
                else
                {
                    proposedPFA += '\n';
                }
            }
            
            
            updatedLead.Public_Comments_Internal__c = finalComments;
            updatedLead.Reviews_Status_JP__c = reviewsDecision;
            updatedLead.Proposed_PFA__c = proposedPFA; */
            updateList.add(updatedLead);
        }
        
        update updateList;
        update updateListReviews;
    }
    
}