@isTest
public class PreventNoteUpdationandDeletionTest {
    
        public static  testMethod  void TestNotdel(){
        
        
                  RecordType r1=[Select Id from RecordType where name=:'RA Applications' limit 1];

    
     User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
    Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
    insert acnt;
        acnt.IsPartner=true;
        update acnt;
    Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                              Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
    insert con;  
      User user = new User(alias = 'test123', email='test156klu@mymail234.com',
      emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
      localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
      ContactId = con.Id,
      timezonesidkey='America/New_York', username='test156klu@mymail234.com');
        
     insert user;
        
 

   
  
 
     
        
        
        Opportunity o= new opportunity();

o.Awardee_Institution_Organization__c=acnt.Id;
o.RecordTypeId=r1.id;
o.Full_Project_Title__c='test';
      o.Project_Name_off_layout__c='test';
o.CloseDate=Date.today();
o.Application_Number__c='Ra-1234';
o.StageName='In Progress';
o.Name='test';
      o.PI_Name_User__c='00570000002dmiQ';
          o.AO_Name_User__c='005700000047Ydq';
insert o;
       
      Note n= new Note(); 
      n.Body='test';
      n.Title='test1';
      n.ParentID = o.Id;
               n.OwnerID = User.Id;
               insert n;

              try {
    delete n;
} catch (Exception e) {
 system.debug('****'+e);
}
      

    
        
        
    }
    
     public static  testMethod  void TestNotupdt(){
        
        
                  RecordType r1=[Select Id from RecordType where name=:'RA Applications' limit 1];

    
     User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
    Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
    insert acnt;
        acnt.IsPartner=true;
        update acnt;
    Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                              Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
    insert con;  
      User user = new User(alias = 'test123', email='test156klu@mymail234.com',
      emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
      localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
      ContactId = con.Id,
      timezonesidkey='America/New_York', username='test156klu@mymail234.com');
        
     insert user;

          Opportunity o= new opportunity();

o.Awardee_Institution_Organization__c=acnt.Id;
o.RecordTypeId=r1.id;
o.Full_Project_Title__c='test12';
      o.Project_Name_off_layout__c='test12';
o.CloseDate=Date.today();
o.Application_Number__c='Ra-12345';
o.StageName='In Progress';
o.Name='test12';
            o.PI_Name_User__c='00570000002dmiQ';
          o.AO_Name_User__c='005700000047Ydq';
insert o;
      
       
         Note n= new Note(); 
      n.Body='test2';
      n.Title='test12';
      n.ParentID = o.Id;
               n.OwnerID = user.Id;
         n.CreatedById=sysAdminUser.Id;
               insert n;
 
      n.Body='xyz';
      n.Title='abc';

              try {
   
                  update n;
} catch (Exception e) {
 system.debug('****'+e);
}
      
       
         
     }
}