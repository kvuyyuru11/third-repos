public class COIandExpertiseCustomViewController {

    //Get list of ListViews
    public List<ListView> listIds;
    
    //Used to set our listView
    public String selectedView{get;set;}
    
    Public List<COI_Expertise__c> COIandExpertise;
    
    public Id PnlId;
    Public Id Usr1;
    Public Contact Cnt;

    
    public COIandExpertiseCustomViewController() {
    
     PnlId = ApexPages.currentPage().getParameters().get('id');
     Usr1 = UserInfo.getUserId();
     User Usr2 = [Select Id, ContactId from User where id=:Usr1];
     Cnt = [Select Id, name from contact where Id=:Usr2.ContactId];
        
    }    
    
    public List<COI_Expertise__c> getCOIandExpertise(){
    Id devRecordTypeId = Schema.SObjectType.COI_Expertise__c.getRecordTypeInfosByName().get('General Record Type').getRecordTypeId();
    //List<RecordType> COIRecordType = [Select ID, name from RecordType Where SObject]
    COIandExpertise = new List<COI_Expertise__c>();
    //COIandExpertise = [Select Id,Name,Panel_R2__c,Principal_Investigator_Project__c,Program_Organization_Project__c,Panel_Program_Name__c,Related_Project__r.Account_Name__c,Panel_R2__r.Program_Lookup__r.Name from COI_Expertise__c Where Panel_R2__c=:PnlId AND Reviewer_Name__c=:Cnt.Id AND Conflict_of_Interest__c=null AND Active__c=True AND Due_Date__c>=:system.Today() AND RecordTypeId=:devRecordTypeId AND Submitted__c=:false];
    COIandExpertise = [Select Id,Name,Panel_R2__c,Principal_Investigator_Project__c,Program_Organization_Project__c,Panel_Program_Name__c,Related_Project__r.Account_Name__c,Panel_R2__r.Program_Lookup__r.Name,Panel_R2__r.PFA__r.Name from COI_Expertise__c Where Panel_R2__c=:PnlId AND Reviewer_Name__c=:Cnt.Id AND Conflict_of_Interest__c=null AND Active__c=True AND COI_Due_Date__c>=:system.Today() AND RecordTypeId=:devRecordTypeId AND Submitted__c=:false AND Panel_R2__r.Active__c = True];
        //COIandExpertise = [Select Id,Name,Panel_R2__c from COI_Expertise__c Where Panel_R2__c=:PnlId];
    return COIandExpertise;
    }
}