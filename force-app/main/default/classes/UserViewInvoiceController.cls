/*------------------------------------------------------------------------------
*  Date      Project                   Developer                   Justification
*  05-01-17  Invoicing-Phase2       Himanshu Kalra, M&S Consulting  Creation
*  05-01-17  Invoicing-Phase2       Himanshu Kalra, M&S Consulting  Controller designed to route the user (Awardee/PCORI staff) to NewInvoiceDetail/InvoiceEdit respectively.
* -----------------------------------------------------------------------------
*/ 

public class UserViewInvoiceController{
    // Initializtion
    public boolean checkFinanceEdit {get;set;}
    public Invoice__c inv { get;set; }
    // constructor function
    public UserViewInvoiceController (ApexPages.StandardController controller){
    }   
    //@Purpose: Onclick of View on Invoice takes user to different Visualforce page on the basis of logged in Users Profile and Invoice Status  (Internal and External)
    public PageReference routeUserOnStatusBased(){ 
        
        String profileName = InvoiceServices.currentUserProfile(Userinfo.getUserId());
        //system.debug('ProfileName'+profileName);
        string paramInvoiceId= ApexPages.currentPage().getParameters().get('Id');
        //system.debug('routeUserOnStatusBased paramInvoiceId '+paramInvoiceId);
        if(!String.isBlank(paramInvoiceId)){
            system.debug('routeUserOnStatusBased paramInvoiceId INSIDE'+paramInvoiceId);
            inv = [select Id, Invoice_Status_Internal__c,Invoice_Status_External__c,Invoice_Type__c,
                   Total_Research_Period_Finance__c ,Total_Supplement_Funding_Finance__c,
                   Total_Peer_Review_Finance__c,Total_Engagement_Invoice_Amount_Finance__c,                                     
                   Total_Peer_Review_Hybrid_Finance__c, Total_Firm_Fixed_Price_Invoice_Finance__c,
                   Total_Firm_Fixed_Price_Amount__c                                                   
                   from Invoice__c where Id=:paramInvoiceId];
        }
        //system.debug('routeUserOnStatusBased inv LIST '+inv);
        string extStatus = inv.Invoice_Status_External__c;
        //  system.debug('routeUserOnStatusBased inv extStatus '+extStatus);
        string intStatus = inv.Invoice_Status_Internal__c;
        //system.debug('routeUserOnStatusBased inv intStatus '+intStatus);
        string invoiceId = inv.Id; 
        
        // Fetches the Id and Name of the logged in AP Manager
        List<User> AOManager= [Select Id, Name from User where Name =:System.Label.AO_Manager and Id =: UserInfo.getUserId() Limit 1];
        
        // retrieve external  profiles
        Set<String> externalprofileName_Map  = InvoiceServices.getSetofAllProfilesInvoices(true); 
        // retrieve internal profiles    
        Set<String> internalProfileName_Map  = InvoiceServices.getSetofAllProfilesInvoices(false);   
        Set<String> internalEditProfile_Map  = InvoiceServices.getInvoiceEditProfiles(false,true);  
        Set<String> pNames = new Set<String>();
        for(Invoice_Access_to_profiles__mdt pRecord : [select Profile_Label__c from Invoice_Access_to_profiles__mdt])
            pNames.add(pRecord.Profile_Label__c);
        
        checkFinanceEdit = checkInvoiceTypeFinanceEdit(inv);  
        //system.debug('routeUserOnStatusBased checkFinanceEdit ' + checkFinanceEdit);		
        Boolean externalProfile     = externalProfileName_Map.contains(profileName);
        //  system.debug('routeUserOnStatusBased externalProfile ' + externalProfile);
        Boolean internalProfile     = internalProfileName_Map.contains(profileName);
        //  system.debug('routeUserOnStatusBased internalProfile ' + internalProfile);
        Boolean internalEditProfile = internalEditProfile_Map.contains(profileName);   
        // system.debug('routeUserOnStatusBased internalEditProfile ' + internalEditProfile);        
        
        PageReference nextPage; //= new PageReference(); 
        
        if(( externalProfile ) && extStatus=='Draft'){
            nextPage =  new PageReference('/apex/NewInvoiceDetail?id='+invoiceId+'&edit=true'+'&prof=awardee');
        }else if(externalProfile && extStatus!='Draft' ){  
            nextPage = new PageReference('/apex/NewInvoiceDetail?id='+invoiceId+'&edit=true'+'&prof=All');
        }else if( internalEditProfile  && intStatus==System.Label.Finance_Review && !checkFinanceEdit){
            nextPage =  new PageReference('/apex/InvoiceEdit?id='+invoiceId+'&edit=true');
        }else if( internalEditProfile && intStatus==System.Label.Finance_Review && checkFinanceEdit){  
            nextPage =  new PageReference('/apex/InvoiceEdit?id='+invoiceId+'&edit=false');
        }
        /*else if (internalProfile  || profileName==System.Label.CMA_Operations || profileName==System.Label.DMOS || (!AOManager.isEmpty() && AOManager[0].Name==System.Label.AO_Manager) && intStatus!=System.Label.Finance_Review){
nextPage =  new PageReference('/apex/InvoiceEdit?id='+invoiceId+'&edit=false');         
}*/       
        else if(           
            internalProfile  || pNames.contains(profileName)  || (!AOManager.isEmpty() && AOManager[0].Name==System.Label.AO_Manager) && intStatus!=System.Label.Finance_Review){
                nextPage =  new PageReference('/apex/InvoiceEdit?id='+invoiceId+'&edit=false');
            } 
        nextPage.setRedirect(true); 
        return nextPage;
    }
    //@Purpose: Onclick of Edit on Invoice takes Awardee to Invoice Header page if external status is Draft otherwise shows an error
    public PageReference userEditRoute(){ 
        Id profileId= userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        //system.debug('ProfileName Krishna'+profileName);
        PageReference nextPage; 
        
        try {
            Invoice__c inv = [select Id, Invoice_Status_Internal__c,Invoice_Status_External__c,Invoice_Type__c 
                              from Invoice__c where Id=:ApexPages.currentPage().getParameters().get('Id')];
            // system.debug('Invoice List'+inv);
            string extStatus = inv.Invoice_Status_External__c;
            //string intStatus = inv.Invoice_Status_Internal__c;
            //system.debug('extStatus HK extStatus'+extStatus);  
            string invoiceId = inv.Id; 
            //system.debug('invoiceId HK invoiceId'+invoiceId); 
            
            if((profileName==System.Label.PCORI_Community_Partner || profileName== System.Label.System_Administrator_Invoices) && 
               extStatus== System.Label.Draft ){
                   nextPage =  new PageReference('/apex/NewInvoiceDetail?id='+invoiceId+'&edit=false'+'&prof=awardee');
                   //system.debug('nextPage EDIT DRAFT'+nextPage); 
                   nextPage.setRedirect(true);
                   return nextPage;
               }else {
                   ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Invoice cannot be edited after an Invoice is submitted for review');
                   ApexPages.AddMessage(myMsg);
                   //system.debug('nextPage EDIT NOT DRAFT' );
               } 
        } catch(exception e) {
            apexpages.addmessages(e);
        }
        return null;
    }
    //@Purpose: Checks whether Finance Screeners are allowed to edit an Invoice on the basis of Invoice Type picklist field
    private boolean checkInvoiceTypeFinanceEdit(Invoice__c inv){
        if(inv.Invoice_Type__c == System.Label.Cost_Reimbursable_Research_Award  && 
           inv.Total_Research_Period_Finance__c != null && 
           inv.Total_Supplement_Funding_Finance__c != null && 
           inv.Total_Peer_Review_Finance__c != null){
               return true;
           }
        if(inv.Invoice_Type__c == System.Label.Cost_Reimbursable_Engagement_Awards && 
           inv.Total_Engagement_Invoice_Amount_Finance__c != null){
               return true;
           }
        if(inv.Invoice_Type__c == System.Label.Firm_Fixed_Price && 
           inv.Total_Firm_Fixed_Price_Invoice_Finance__c != null){
               return true;
           }
        if(inv.Invoice_Type__c == System.Label.Hybrid  && 
           inv.Total_Peer_Review_Hybrid_Finance__c != null &&
           inv.Total_Supplement_Funding_Finance__c != null &&
           inv.Total_Research_Period_Finance__c != null){ 
               return true;
           }
        return false;
    }
}