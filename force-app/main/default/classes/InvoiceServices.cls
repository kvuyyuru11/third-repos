/*------------------------------------------------------------------------------
 *  Date            Project             Developer                       Justification
 *  02/02/2017      Invoicing-Phase1  Ashish Kothari, M&S Consulting  Creation 
 *  06/25/17        Invoicing-Phase2  Vinayak Sharma, REI Systems  Added methods:  currentUserRole(), currentUserProfile(), getListofProfilesInvoices(), getSetofAllProfilesInvoices(), getInvoiceEditProfiles(), getAllProfilesInvoices()
 *------------------------------------------------------------------------------
*/ 

// Services class for Invoices
public with sharing class InvoiceServices{
    //Commented as this logic not in use but could be used to expand the functionality in the future
    //To retrieve the Record Type of the Invoices object.
    public static Map<String, Id> recordTypeIdMap{
        get{
            if (recordTypeIdMap == null){
                recordTypeIdMap = new map<String,Id>();
                for (RecordType rt: [Select Id, DeveloperName from RecordType where sObjectType = 'Invoice__c'])
                    recordTypeIdMap.put(rt.DeveloperName, rt.Id);
            }
            return recordTypeIdMap;
        }
        set;
    } 
                   
    /*@Purpose: This method returns the Map of RecordType Id and Name*/                                          
    public static Map<Id,String> recordIdNameMap(String sObjectName ){
        
        Map<Id,String> recIdNameMap = new Map<Id,String>();     
        Map<Id,RecordType> lstRecType = new Map<ID,RecordType>([Select Id,Name from RecordType where sObjectType =:sObjectName]);       
        System.debug('The Record Type is '+lstRecType);    
        for(String key   : lstRecType.keyset()){          
           recIdNameMap.put(key,lstRecType.get(key).Name);       
        }
        return  recIdNameMap;   
    }

    /*@Purpose: Returns the Role of Current logged in User */ 
    public static String  currentUserRole(Id userId){
        User currentUser = [Select Id , UserRole.Name from User where Id =: userId];
        return currentUser.UserRole.Name;
    }
    /*@Purpose: Returns the Profile of Current logged in User */ 
     public static String  currentUserProfile(Id userId){
        User currentUser = [Select Id , Profile.Name from User where Id =: userId];
        return currentUser.Profile.Name;  
    }
    /*@Purpose: Method to get the list of all the Profiles currently being used for Invoices 
     We are using a Flag of False to identify the internal PCORI staff  and External
     PCORI community Partner Licenses*/    
    public static List<String>  getListofAllProfilesInvoices(Boolean externalCommunityProfile){

        List<String> profiles_Lst = new List<String>();
        List<KeyValueStore__c> keyValueStore_Lst = [Select Text__c,Category__c,Module__c from  KeyValueStore__c where Category__c =:'Profiles' 
                                                     and Module__c =:'Invoices' and Boolean__c =: externalCommunityProfile ];
        system.debug('keyValueStore_Lst getListofAllProfilesInvoices'+keyValueStore_Lst);
        for(KeyValueStore__c kvStore: keyValueStore_Lst){ 
            profiles_Lst.add(kvStore.Text__c); 
        }

        if(!profiles_Lst.isEmpty()){
            return profiles_Lst;
        }
        return null;
    }
    /*@Purpose: Method to get the Set of all the Profiles currently being used for Invoices 
     We are using a Flag of False to identify the internal PCORI staff and Flag of False for Awardees 
     we are using a Flag of True */  
    public static Set<String>  getSetofAllProfilesInvoices(Boolean externalCommunityProfile){

        Set<String> profiles_Map = new Set<String>();
        List<KeyValueStore__c> keyValueStore_Lst = [Select Text__c,Category__c,Module__c from  KeyValueStore__c where Category__c =:'Profiles' 
                                                     and Module__c =:'Invoices' and Boolean__c =: externalCommunityProfile ];
    system.debug('keyValueStore_Lst getSetofAllProfilesInvoices'+keyValueStore_Lst);

        for(KeyValueStore__c kvStore: keyValueStore_Lst){ 
            profiles_Map.add(kvStore.Text__c); 
        }

        if(!profiles_Map.isEmpty()){
            return profiles_Map;  
        }
        return null;
    }

    /*@Purpose: Method to get the Set of all the Profiles who can edit Invoice */ 
    public static Set<String>  getInvoiceEditProfiles(Boolean externalCommunityProfile,Boolean canEditInvoice){

        Set<String> profiles_Map = new Set<String>();
        List<KeyValueStore__c> keyValueStore_Lst = [Select Text__c,Category__c,Module__c from  KeyValueStore__c where Category__c =:'Profiles' 
                                                     and Module__c =:'Invoices' and Boolean__c =: externalCommunityProfile  and Can_Edit_Invoice__c =: canEditInvoice];
    system.debug('keyValueStore_Lst getInvoiceEditProfiles'+keyValueStore_Lst);
        for(KeyValueStore__c kvStore: keyValueStore_Lst){ 
            profiles_Map.add(kvStore.Text__c); 
        }

        if(!profiles_Map.isEmpty()){
            return profiles_Map;  
        }
        return null;
    }
     
    /*@Purpose: Method to get the Set of all the Profiles currently being used for Invoices 
    We are using a Flag of False to identify the internal PCORI staff  and External
    PCORI community Partner Licenses*/  
  public static List<String>  getAllProfilesInvoices(){
      List<String> profiles_List = new List<String>();
      List<KeyValueStore__c> keyValueStore_Lst = [Select Text__c,Category__c,Module__c from  KeyValueStore__c where Category__c =:'Profiles' 
                                                    and Module__c =:'Invoices'];
     system.debug('keyValueStore_Lst getAllProfilesInvoices'+keyValueStore_Lst);
      for(KeyValueStore__c kvStore: keyValueStore_Lst){ 
           profiles_List.add(kvStore.Text__c); 
      }
      return profiles_List;
  }
        
}