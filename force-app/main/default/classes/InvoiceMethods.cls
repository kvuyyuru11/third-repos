/*------------------------------------------------------------------------------------------
 *  Date            Project             Developer                       Justification
 *  04/10/2017   Invoicing-Phase2        Vinayak Sharma                  Developer, REI Systems
 *  04/10/2017   Invoicing-Phase2        Vinayak Sharma                  REI Systems Inc This class is called from the Invoice Trigger to Clone an Invoice whenever the Invoice is Rejected by a Finance or whenver the Status of the Invoice changes from Finance Review to Rejected
 * ---------------------------------------------------------------------------------------------
*/
global without sharing class InvoiceMethods {    
    //@purpose : This method is called to create a cloned invoice , Record ID is passed for all the invoices which needs to be cloned     
    @future
    public static void cloneAnInvoice(Set<id> invoiceIDs){   
        list<InvoiceLineItemWrapper> lineItems = new list<InvoiceLineItemWrapper>(); 
        InvoiceWrapper newInvoiceW ;
        list<InvoiceWrapper>  newInvoices = new list<InvoiceWrapper>();
        Map<Id,InvoiceWrapper> invoiceIdWrapperMap =new Map<Id,InvoiceWrapper>();
        List<Invoice__c> invoicesClonedlst = new List<Invoice__c>();

      //  to do Select the appropriate Ivnoice RecordType
        List<Invoice__c> invoicelst = [Select Invoice_Name__c, version__c, Invoice_Status_Internal__c, Invoice_Status_External__c , Project__c,createdById,Notes__c,Internal_Notes__c,
        Start_of_Billing_Period__c,End_of_Billing_Period__c,Grand_Total_Total_Research_Period_Budget__c,Grand_Total_Supplemental_Funding_Budget__c,Grand_Total_Peer_Review_Budget__c,
        Total_Invoice_Amount_Engagement__c,Total_Firm_Fixed_Price_Amount__c,Total_Research_Period_Finance__c,Total_Supplement_Funding_Finance__c,
                                       Total_Peer_Review_Finance__c,Total_Peer_Review_Hybrid_Finance__c,Is_Final_Invoice__c,Certification__c,	
                                       Total_Engagement_Invoice_Amount_Finance__c,Total_Firm_Fixed_Price_Invoice_Finance__c,ownerId,Award_Level_notes_text__c  From
        Invoice__c where id in :invoiceIDs and isclone__c=false ]; 

        System.debug('The Invoice lst is  11 >>>>>>>>>> '+ invoicelst);

        try{ 
            for(Invoice__c invoice : invoicelst){
                                       
                Invoice__c newInvoice = invoice.clone(false, true, false, false);
                newInvoice.Invoice_Name__c = invoice.Invoice_Name__c;
                newInvoice.parent_invoice__c = invoice.id;
                newInvoice.Project__c = invoice.Project__c;   
                newInvoice.Grand_Total_Total_Research_Period_Budget__c = invoice.Total_Research_Period_Finance__c; 
                newInvoice.Grand_Total_Supplemental_Funding_Budget__c = invoice.Total_Supplement_Funding_Finance__c; 
                newInvoice.Grand_Total_Peer_Review_Budget__c        = invoice.Grand_Total_Peer_Review_Budget__c; 
                newInvoice.Total_Invoice_Amount_Engagement__c = invoice.Total_Engagement_Invoice_Amount_Finance__c; 
                newInvoice.Total_Firm_Fixed_Price_Amount__c  = invoice.Total_Firm_Fixed_Price_Invoice_Finance__c; 
                            
                newInvoice.Notes__c = invoice.Notes__c;
                newInvoice.Internal_Notes__c = invoice.Internal_Notes__c;
                newInvoice.Award_Level_notes_text__c= invoice.Award_Level_notes_text__c;
               // newInvoice.RecordTypeId = newInvoice.RecordTypeId;
                newInvoice.Invoice_Status_External__c = 'Draft';
                newInvoice.Invoice_Status_Internal__c = '';
             if(invoice.version__c>1){
                     newInvoice.ownerId = invoice.ownerId ;
            
                }
                else{
                newInvoice.ownerId = invoice.createdById ; // the new invoice is assigned to the Invoice queue
                   
                }
                newInvoice.version__c = invoice.version__c + 1; 
                newInvoice.Start_of_Billing_Period__c= invoice.Start_of_Billing_Period__c;
                newInvoice.End_of_Billing_Period__c= invoice.End_of_Billing_Period__c;
                
                 newInvoice.Is_Final_Invoice__c=invoice.Is_Final_Invoice__c;
                 newInvoice.Certification__c=false;
                 newInvoice.Invoice_Submission_Date__c=null;
                 newInvoice.Total_Research_Period_Finance__c=null;
                 newInvoice.Total_Supplement_Funding_Finance__c=null;
                 newInvoice.Total_Peer_Review_Finance__c=null;
                 newInvoice.Total_Engagement_Invoice_Amount_Finance__c=null;
                 newInvoice.Total_Firm_Fixed_Price_Invoice_Finance__c=null;
                 newInvoice.Total_Peer_Review_Hybrid_Finance__c = null;
                
                // now get the invoice line items                      
                newInvoiceW = new InvoiceWrapper();
                newInvoiceW.invoice= newInvoice;
                newInvoiceW.clonedId = invoice.Id; 
              
                
                newInvoices.add(newInvoiceW);

                invoiceIdWrapperMap.put(invoice.Id,newInvoiceW);
            }
           
            System.debug('The Invoice lst is  22 >>>>>>>>>> '+ invoiceIdWrapperMap);

            List<Invoice_line_item__c> invLineItemsLst = [Select id, Line_Item_Type__c, Salary__c,Fringe_Benefits__c, Subtotal_Personal_Cost__c,Consortium_Contractual_Costs__c, Consultant_Cost__c, 
                                                           Equipment__c, Other_Costs__c, Invoice__c,Indirect_Costs__c,
                                                           Supplies__c, Travel__c,Salary_CED__c ,Fringe_Benefits_CED__c ,Consultant_Cost_CED__c,Supplies_CED__c ,Travel_CED__c,
                                                           Other_Costs_CED__c,Equipment_CED__c ,Consortium_Contractual_Costs_CED__c , Indirect_Costs_CED__c,
                                                           Salary_AFR__c ,Fringe_Benefits_AFR__c,Subtotal_Personal_Cost_AFR__c , Consultant_Cost_AFR__c ,Supplies_AFR__c ,Travel_AFR__c,
                                                           Other_Costs_AFR__c, Equipment_AFR__c , Consortium_Contractual_Costs_AFR__c , Indirect_Costs_AFR__c ,
                                                           Milestone_Number1__c , Milestone_Name1__c  ,Payment_Amount1__c, Available_Funds1__c ,Cumulative1__c, First_peer_review_invoice__c ,  
                                                           Second_peer_review_invoice__c from Invoice_Line_Item__c where Invoice__c in :invoiceIDs];

            List<Invoice_line_item__c> invLineItemsLstCloned = new  List<Invoice_line_item__c>();                                               
             for(Invoice_line_item__c obj: invLineItemsLst){
                    Invoice_line_item__c CloneLineItem    = new Invoice_line_item__c(); 
                    CloneLineItem.Line_Item_Type__c       =   obj.Line_Item_Type__c; 
                   
                   /* Current Expense fields */
                    CloneLineItem.Salary__c               = obj.Salary__c;
                    CloneLineItem.Fringe_Benefits__c      = obj.Fringe_Benefits__c;
                    CloneLineItem.Consortium_Contractual_Costs__c  =   obj.Consortium_Contractual_Costs__c;
                    CloneLineItem.Consultant_Cost__c     =   obj.Consultant_Cost__c;
                    CloneLineItem.Equipment__c           =   obj.Equipment__c ;
                    //CloneLineItem.Research_Related_Inpatient_and_Outp__c  =   obj.Research_Related_Inpatient_and_Outp__c;
                    CloneLineItem.Invoice__c             =   obj.Invoice__c;
                    CloneLineItem.Supplies__c            =   obj.Supplies__c; 
                    CloneLineItem.Travel__c              =   obj.Travel__c;
                    CloneLineItem.Other_Costs__c         = obj.Other_Costs__c;
                    CloneLineItem.Indirect_Costs__c       = obj.Indirect_Costs__c;
                    
                    /* CED Fields */ 
                    CloneLineItem.Salary_CED__c              = obj.Salary_CED__c;
                    CloneLineItem.Fringe_Benefits_CED__c     = obj.Fringe_Benefits_CED__c;
                    CloneLineItem.Consultant_Cost_CED__c      =   obj.Consultant_Cost_CED__c;
                    CloneLineItem.Supplies_CED__c             =   obj.Supplies_CED__c;
                    CloneLineItem.Travel_CED__c                =   obj.Travel_CED__c ;
                    CloneLineItem.Other_Costs_CED__c            =   obj.Other_Costs_CED__c;
                    //CloneLineItem.Research_Related_IP_OP_Costs_CED__c  =   obj.Research_Related_IP_OP_Costs_CED__c;
                    CloneLineItem.Equipment_CED__c              =   obj.Equipment_CED__c;
                    CloneLineItem.Consortium_Contractual_Costs_CED__c  =   obj.Consortium_Contractual_Costs_CED__c; 
                    CloneLineItem.Indirect_Costs_CED__c              =   obj.Indirect_Costs_CED__c;

                    /* Available Funds */
                    CloneLineItem.Salary_AFR__c          =  obj.Salary_AFR__c;
                    CloneLineItem.Fringe_Benefits_AFR__c = obj.Fringe_Benefits_AFR__c;
                    //CloneLineItem.Subtotal_Personal_Cost_AFR__c = obj.Subtotal_Personal_Cost_AFR__c;
                    CloneLineItem.Consultant_Cost_AFR__c  = obj.Consultant_Cost_AFR__c; 
                    CloneLineItem.Supplies_AFR__c         = obj.Supplies_AFR__c;
                    CloneLineItem.Travel_AFR__c           = obj.Travel_AFR__c ;
                    CloneLineItem.Other_Costs_AFR__c      =   obj.Other_Costs_AFR__c;
                   // CloneLineItem.Research_Related_IP_OP_Costs_AFR__c  = obj.Research_Related_IP_OP_Costs_AFR__c ;
                    CloneLineItem.Equipment_AFR__c =  obj.Equipment_AFR__c ;   
                    CloneLineItem.Consortium_Contractual_Costs_AFR__c  = obj.Consortium_Contractual_Costs_AFR__c;
                    CloneLineItem.Indirect_Costs_AFR__c  = obj.Indirect_Costs_AFR__c;

                    /* hybrid RA Fields */
                    CloneLineItem.First_peer_review_invoice__c  =  obj.First_peer_review_invoice__c;
                    CloneLineItem.Second_peer_review_invoice__c    =  obj.Second_peer_review_invoice__c ;
                     

                    // Firm Fixed Price 
                    CloneLineItem.Milestone_Number1__c  =  obj.Milestone_Number1__c;
                    CloneLineItem.Milestone_Name1__c    =  obj.Milestone_Name1__c ;
                    CloneLineItem.Payment_Amount1__c    =  obj.Payment_Amount1__c ;
                    CloneLineItem.Available_Funds1__c   =  obj.Available_Funds1__c ;
                    CloneLineItem.Cumulative1__c        =  obj.Cumulative1__c ;

                    CloneLineItem.Parent_Clone_Invoice__c    = obj.Invoice__c;    /* so thae we are able to pull the record later */
                    invLineItemsLstCloned.add(CloneLineItem);
            }                                                 
 
            for(Invoice_Line_Item__c  invLineItem : invLineItemsLstCloned){                                 
                    InvoiceLineItemWrapper temp = new  InvoiceLineItemWrapper('',  invLineItem, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                    lineItems.add(temp);                      
                    InvoiceWrapper InvoiceObj = (InvoiceWrapper)invoiceIdWrapperMap.get(invLineItem.Parent_Clone_Invoice__c);
                    InvoiceObj.InvoiceLineItems = lineItems;
                    System.debug('The Invoice lst is 33 >>>>>>>>>> '+ InvoiceObj);
                    System.debug('The Invoice lst is 44 >>>>>>>>>> '+ InvoiceObj.InvoiceLineItems);
            }
  
            List<Id> clonedInvoicesId = new List<Id>();
    
            if(newInvoices.size() > 0){
                for(InvoiceWrapper oneInvWrapper : newInvoices){
                     // add the save logic
                     invoicesClonedlst.add(oneInvWrapper.invoice);
                 } 
                   System.debug('The Invoice lst is 55 >>>>>>>>>> '+ invoicesClonedlst);

                   Database.SaveResult[] srList = Database.insert(invoicesClonedlst, false);
                   // Iterate through each returned result
                    for (Database.SaveResult sr : srList) { 
                        if(sr.isSuccess()){
                            clonedInvoicesId.add(sr.getId());
                            // Operation was successful, so get the ID of the record that was processed
                            System.debug('Successfully Inserted the Record 66 >>>>>>>>>>  ' + sr.getId());
                        }
                        else {
                            // Operation failed, so get all errors                
                            for(Database.Error err : sr.getErrors()) {
                                System.debug('The following error has occurred. 77 >>>>>>>>>>');                    
                                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                System.debug('Account fields that affected this error: 88 >>>>>>>>>>' + err.getFields());
                            }
                        }
                    }
                 }   
                
                 Map<Id,Id> oldInvoiceNewInvoice = new Map<Id,Id>();                    
                 List<Invoice__c> lstNewInvoices = [Select Id,Parent_Invoice__c from Invoice__c where Parent_Invoice__c in:invoiceIdWrapperMap.keyset() order by Name Desc Limit 1 ];
                 System.debug('The New Invoice lst is '+ lstNewInvoices);  
                  
                 for(Invoice__c obj: lstNewInvoices){
                    oldInvoiceNewInvoice.put(obj.parent_invoice__c, obj.Id);
                 }
                 List<Invoice_line_item__c> updateInvoiceLineItemLst = new List<Invoice_Line_Item__c>();
                 List<Invoice_line_item__c> updateInvoiceSet = new List<Invoice_Line_Item__c>();

                 if(lstNewInvoices.size()>0){
                    for(Invoice__c objInv :lstNewInvoices){
                        for(InvoiceWrapper obj :newInvoices){
                            if(objInv.Parent_Invoice__c == obj.clonedId){
                                for(InvoiceLineItemWrapper lineItemObj : obj.InvoiceLineItems){     
                                     Id newClonedId = oldInvoiceNewInvoice.get(objInv.Parent_Invoice__c);  
                                    // System.debug('The Invoice lst is 6666 >>>>>>>>>> '+ newClonedId);
                                     lineItemObj.lineItem.Invoice__c =   newClonedId;    
                                     updateInvoiceSet.add(lineItemObj.lineItem);   
                                     //System.debug('The Invoice lst is 7777 >>>>>>>>>> '+ updateInvoiceSet.size());                              
                                } 
                            }
                        }            
                    }                                      
                }
                updateInvoiceLineItemLst.addAll(updateInvoiceSet);
                //System.debug('The Invoice lst is 8888 >>>>>>>>>> '+ updateInvoiceLineItemLst);  
                Database.upsert(updateInvoiceLineItemLst, false);  
            }catch(exception ex){ 
                    system.debug('*****Exception'+ex.getMessage());
                    system.debug('*****Exception'+ex.getStackTraceString());
                    system.debug('*****Exception'+ex.getLineNumber());
                    system.debug('*****Exception'+ex.getCause());
            }    
        }   
    }