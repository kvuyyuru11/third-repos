@isTest
public class COITriggerTestR2 {
    
    public static  testMethod  void TestCOI(){
        List<User> usrlist= TestDataFactory.getpartnerUserTestRecords(1,1);
        
            
            Cycle__c cyc=new Cycle__c(Name='test2016',COI_Due_Date__c = Date.newInstance(2016,12,31));
            insert cyc;
            Panel__c pan= new Panel__c(name='test12',Cycle__c=cyc.id,Panel_Due_Dtae__c=  Date.newInstance(2019,12,20) );
            insert pan;
        test.startTest();
            Opportunity rac= new Opportunity(Name = 'test1',Full_Project_Title__c='testtile1', Project_Name_off_layout__c='testtile1',Technical_Abstract__c='test',CloseDate= Date.newInstance(2017, 2, 1),Panel__c=pan.id,ownerid=usrlist[0].ID);
            insert rac;  
       
            Panel_Assignment__c panAss=  new Panel_Assignment__c(Reviewer_Name__c=usrlist[0].contactId,panel__c=pan.id,Panel_Role__c='Chair');
            insert panAss;
             System.RunAs(usrlist[0]) {
            COI_Expertise__c co=new COI_Expertise__c(Reviewer_Name__c=usrlist[0].contactId,Related_Project__c=rac.Id);      
            insert Co;
            co.OwnerId=usrlist[0].Id;
            update co;
                 test.stopTest();
        }
    }
    
    public static  testMethod  void TestCOI1(){
        
        List<User> usrlist= TestDataFactory.getpartnerUserTestRecords(1,1);
        
        
            
            Cycle__c cyc=new Cycle__c(Name='test2016',COI_Due_Date__c = Date.newInstance(2025,12,31));
            insert cyc;
            Panel__c pan= new Panel__c(name='test12',Cycle__c=cyc.id,Panel_Due_Dtae__c=  Date.newInstance(2019,12,20) );
            insert pan;
        test.startTest();
            Opportunity rac= new Opportunity(Name = 'test1',Full_Project_Title__c='testtile1',Project_Name_off_layout__c='testtile1', Technical_Abstract__c='test',CloseDate= Date.newInstance(2017, 2, 1),Panel__c=pan.id,ownerid=usrlist[0].ID);
            insert rac; 
        
            Panel_Assignment__c panAss=  new Panel_Assignment__c(Reviewer_Name__c=usrlist[0].contactId,panel__c=pan.id,Active__c=true,Panel_Role__c='Chair',ownerid=usrlist[0].ID);
            insert panAss;
        System.RunAs(usrlist[0]) {
            COI_Expertise__c co=new COI_Expertise__c(Reviewer_Name__c=usrlist[0].contactId,Related_Project__c=rac.Id);         
            insert Co;
            co.OwnerId=usrlist[0].Id;
            update co;
            panAss.Active__c=false;
            update panAss;
            co.Active__c=false;
            update co;
test.stopTest();
        }
    }
    
    public static  testMethod  void TestCOI2(){
        
        List<User> usrlist= TestDataFactory.getpartnerUserTestRecords(1,1);
        
      
            
            Cycle__c cyc=new Cycle__c(Name='test2016',COI_Due_Date__c = Date.newInstance(2025,12,31));
            insert cyc;
            Panel__c pan= new Panel__c(name='test12',Cycle__c=cyc.id,Panel_Due_Dtae__c=  Date.newInstance(2019,12,20) );
            insert pan;
        test.startTest();
            Opportunity rac= new Opportunity(Name = 'test1',Full_Project_Title__c='testtile1', Project_Name_off_layout__c='testtile1',Technical_Abstract__c='test',CloseDate= Date.newInstance(2017, 2, 1),Panel__c=pan.id,ownerid=usrlist[0].ID);
            insert rac; 
         
            Panel_Assignment__c panAss=  new Panel_Assignment__c(Reviewer_Name__c=usrlist[0].contactId,panel__c=pan.id,Active__c=true,Panel_Role__c='Chair',ownerid=usrlist[0].ID);
            insert panAss;
         System.RunAs(usrlist[0]) {
            COI_Expertise__c co=new COI_Expertise__c(Reviewer_Name__c=usrlist[0].contactId,Related_Project__c=rac.Id);         
            insert Co;
            co.OwnerId=usrlist[0].Id;
            update co;
            
            delete panAss;
            co.Active__c=false;
            update co;
            test.stopTest();
            
        }
    }
}