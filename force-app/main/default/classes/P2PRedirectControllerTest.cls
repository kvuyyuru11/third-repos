@isTest
public class P2PRedirectControllerTest {
     public static testMethod void Redirect(){
          Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Pipeline to Proposal').getRecordTypeId();
        Opportunity opp = new Opportunity(Name = 'TestName', Closedate = System.today(), RecordTypeId = recordTypeId, StageName = 'In Progress',
                                          Proceed_To_Tier_3__c = true);
        insert opp;
       P2P_TierA__c objP2P=new P2P_TierA__c();
       objP2P.Projects__c= opp.id; 
         insert objP2P;
       ApexPages.StandardController sc = new ApexPages.StandardController(objP2P);
        P2PFormRedirectionController p2p = new P2PFormRedirectionController(sc);
       p2p.projid= opp.id; 
        p2p.Save();
         p2p.cancel();
     }
}