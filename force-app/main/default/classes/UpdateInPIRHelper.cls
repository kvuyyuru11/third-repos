////////////////////////////This class is for Trigger PIR Email Updates////////
///////////////////Req:RAr2_C3_019; AI-0827////////////////////////////////////

public with sharing class UpdateInPIRHelper {
    public UpdateInPIRHelper() {
    }
    
    public void updateEmailIdsPIR(List<Opportunity> oplist) {
        Set<id> idList = new Set<Id>();
        for(Opportunity p : oplist){
           idList.add(p.id);
        }// for loop

            List<PIR__c> pirList = new List<PIR__c>();
            Map<Id, Opportunity> oppmap = new Map<Id, Opportunity>();
            Map<Id, PIR__c> pirmap = new Map<Id, PIR__c>();
                                      
            List<PIR__c> pirSOQLList =[SELECT id,Administrative_Official_Email_v2__c, PI_Designee_Email_v2__c, PI_Lead_Email_v2__c,Application__c FROM PIR__c WHERE Application__c IN : idList Limit 50000];
            List<Opportunity> oppSOQLList = [SELECT Id, AO_Name_User__r.email, PI_Name_User__r.email, PI_Project_Lead_Designee_1_New__r.email FROM Opportunity where Id IN : idList Limit 50000]; 
             
                for(Opportunity opp: oppSOQLList)
                {
                   oppmap.put(opp.id, opp);
                }
             
                for(PIR__c pirc: pirSOQLList)
                {
                   pirmap.put(pirc.id, pirc);
                }
                
         try{
                for(PIR__c pir : pirSOQLList)
                 {
                   pir.Administrative_Official_Email_v2__c = oppmap.get(pir.Application__c).AO_Name_User__r.email;
                   pir.PI_Lead_Email_v2__c = oppmap.get(pir.Application__c).PI_Name_User__r.email;         
                   pir.PI_Designee_Email_v2__c = oppmap.get(pir.Application__c).PI_Project_Lead_Designee_1_New__r.email; 
                   
                   pirList.add(pir);
                   system.debug('pirList'+pirList);
                  }// for loop
                
            if(!pirList.isEmpty())
            {
                  upsert pirList;
                  system.debug('Updated email fields');
            } 
                      
           }catch(DmlException e) {
              System.debug('The following exception has occurred: ' + e.getMessage());       
           }  
           
    
    }//end updateEmailIdsPIR
}