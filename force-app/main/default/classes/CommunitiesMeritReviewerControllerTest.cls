@isTest
public class CommunitiesMeritReviewerControllerTest {
   
    public static testMethod void CommunitiesMeritReviewerControllertest1(){
        list<Merit_Reviewer_Application__c> myapp2=new list<Merit_Reviewer_Application__c>();
        //User u = [Select u.IsPortalEnabled, u.id, u.contactid, u.IsActive,u.profile.name From User u where u.isActive=true and  u.profile.name ='Merit Reviewer-Candidate' limit 1];
         User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
    Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
    insert acnt; 
    Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                              Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='USA');  
    insert con;  
      User user = new User(alias = 'test123', email='test123h6666@noemail.com',
      emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
      localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
      ContactId = con.Id,
      timezonesidkey='America/New_York', username='test123h6666@noemail.com');
        
     insert user;
   
        System.RunAs(user) {
        list<contact> c;
       /* Contact mycon=new contact();
        mycon.FirstName='Test class';
        mycon.LastName='required Name';    
        mycon.Age__c='15';
        mycon.MailingCity='Vienna';
        mycon.Current_Employer__c='test class1';
        mycon.Current_Position_or_Title__c='test';
        mycon.Federal_Employee__c='No';
        mycon.Gender__c='Male';
        mycon.phone='1234567890';
        mycon.Race__c='test';
        mycon.MailingState='virginia';
        mycon.mailingstreet='electric avenue';
        mycon.MailingPostalcode='12345';     
       insert mycon;*/
        c = [select id,Age__c,MailingCity ,MailingCountry ,Current_Employer__c,Current_Position_or_Title__c,Federal_Employee__c,Gender__c,Race__c,MailingState,MailingPostalcode,phone,mailingstreet from Contact where id=:user.contactId limit 1];    
       Merit_Reviewer_Application__c mr=new Merit_Reviewer_Application__c();
        for(contact mycon1:c){
        mr.Reviewer_Name__c=mycon1.id;
        mr.Country__c=mycon1.MailingCountry ;
        mr.Age__c=mycon1.Age__c; 
        mr.City__c= mycon1.MailingCity;
        mr.Current_Employer__c=mycon1.Current_Employer__c;
        mr.Current_Position_or_Title__c=mycon1.Current_Position_or_Title__c;
        mr.Federal_Employee__c=mycon1.Federal_Employee__c;
        mr.Gender__c=mycon1.Gender__c;
        mr.Highest_Degree_of_Education_New__c='';
        mr.Highest_Level_of_Education__c='';
        mr.Number_of_Publications__c='';
        mr.Number_of_Scientific_Panels__c='';
       // mr.Patient_Communities__c='';
        mr.patient_coommunities__c='';
        mr.Phone__c=mycon1.phone;
        mr.Race__c=mycon1.Race__c;
        mr.Reviewer_Role__c='';
        mr.Served_as_Chair_or_Co_Chair__c='';
       // mr.Stakeholder_Communities__c='';
        mr.State__c=mycon1.MailingState;
        mr.Street_Address__c=mycon1.mailingstreet;
        mr.Zip_Code__c=mycon1.MailingPostalcode;     
      myapp2.add(mr);
        }

        PageReference pg= Page.Flowpage;
        Test.setCurrentPage(pg);
        CommunitiesMeritReviewerController CMReviewer= new CommunitiesMeritReviewerController(); 
        CMReviewer.Flow();
        CMReviewer.open();
        CMReviewer.showbutton=true;
        CMReviewer.showsubmit=true; 
        CMReviewer.showhide=true; 
            CMReviewer.MRlist1=myapp2;
        }
       insert myapp2; 
    }  
    public static testMethod void CommunitiesMeritReviewerControllertest11(){
    list<Merit_Reviewer_Application__c> myapp2=new list<Merit_Reviewer_Application__c>();
     //User u = [Select u.IsPortalEnabled, u.id, u.contactid, u.IsActive,u.profile.name From User u where u.isActive=true and  u.profile.name ='Merit Reviewer-Candidate' limit 1];
    User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
    Id profileId = [select id from profile where name='PCORI Community Partner'].id;
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();         
    Account acnt = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId);
    insert acnt; 
    Contact con = new Contact(LastName ='testCon',AccountId = acnt.Id,Age__c='15',MailingCity='Vienna',Current_Employer__c='test class1',Current_Position_or_Title__c='test',Federal_Employee__c='No',
                              Gender__c='Male',phone='1234567890',Race__c='test',mailingState='virginia',mailingstreet='electric avenue',MailingPostalcode='12345',MailingCountry='India');  
    insert con;  
      User user = new User(alias = 'test123', email='test123j777@noemail.com',
      emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
      localesidkey='en_US', profileid = profileId , country='india',IsActive = true,     
      ContactId = con.Id,
      timezonesidkey='America/New_York', username='test123j777@noemail.com');
        
     insert user;

        System.RunAs(user) {
        list<contact> c;
       /* Contact mycon=new contact();
        mycon.FirstName='Test class';
        mycon.LastName='required Name';    
        mycon.Age__c='15';
        mycon.MailingCity='Vienna';
        mycon.Current_Employer__c='test class1';
        mycon.Current_Position_or_Title__c='test';
        mycon.Federal_Employee__c='No';
        mycon.Gender__c='Male';
        mycon.phone='1234567890';
        mycon.Race__c='test';
        mycon.MailingState='virginia';
        mycon.mailingstreet='electric avenue';
        mycon.MailingPostalcode='12345';     
       insert mycon;*/
        c = [select id,Age__c,MailingCity ,MailingCountry ,Current_Employer__c,Current_Position_or_Title__c,Federal_Employee__c,Gender__c,Race__c,MailingState,MailingPostalcode,phone,mailingstreet from Contact where id=:user.contactId limit 1];    
       Merit_Reviewer_Application__c mr=new Merit_Reviewer_Application__c();
        for(contact mycon1:c){
        mr.Reviewer_Name__c=mycon1.id;
        mr.Country__c=mycon1.MailingCountry ;
        mr.Age__c=mycon1.Age__c; 
        mr.City__c= mycon1.MailingCity;
        mr.Current_Employer__c=mycon1.Current_Employer__c;
        mr.Current_Position_or_Title__c=mycon1.Current_Position_or_Title__c;
        mr.Federal_Employee__c=mycon1.Federal_Employee__c;
        mr.Gender__c=mycon1.Gender__c;
       mr.Highest_Degree_of_Education_New__c='';
        mr.Highest_Level_of_Education__c='';
        mr.Number_of_Publications__c='';
        mr.Number_of_Scientific_Panels__c='';
        //mr.Patient_Communities__c='';
        mr.patient_coommunities__c='';
        mr.Phone__c=mycon1.phone;
        mr.Race__c=mycon1.Race__c;
        mr.Reviewer_Role__c='';
        mr.Served_as_Chair_or_Co_Chair__c='';
        //mr.Stakeholder_Communities__c='';
        mr.State__c=mycon1.MailingState;
        mr.Street_Address__c=mycon1.mailingstreet;
        mr.Zip_Code__c=mycon1.MailingPostalcode;  
      myapp2.add(mr);
        }
        PageReference pg= Page.Flowpage;
        Test.setCurrentPage(pg);
        CommunitiesMeritReviewerController CMReviewer= new CommunitiesMeritReviewerController(); 
        CMReviewer.Flow();
        CMReviewer.open();
        CMReviewer.showbutton=true;
        CMReviewer.showsubmit=true; 
        CMReviewer.showhide=true;
        CMReviewer.MRlist1=myapp2;
        CMReviewer.i = 1;   
          system.debug('the value of i******************************************************************'+CMReviewer.i);  
        }
      insert myapp2; 
    }   
   }