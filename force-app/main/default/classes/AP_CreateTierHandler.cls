/*
* Class : AP_CreateTierHandler
* Author : Sathish Kumar (Accenture)
* Version History : 1.0
* Creation : 02/09/2015
* Last Modified By: Sathish Kumar (Accenture)
* Last Modified Date: 01/22/2015
* Modification Description: Updated Commenting
* Description : This class takes care of the inserting the Advisory Panel Application review records. It's referenced in the AP_CreateTierReviewers trigger.
Modification Log:
-----------------------------------------------------------------------------------------------------------------
Developer              Release Date            Description
-----------------------------------------------------------------------------------------------------------------
@Anudeep Buddineni      04/06/2018      SD-3018  - Enhancement to Rename "AD" and "IHS" as "HDDR" , Similarly "CDR" and "APDTO" to "CEDS" for TIER 2 AP Reviews
@Anudeep Buddineni      09/28/2018      SPS-3764 - Modified logic to cover all combinations for Tier 1 and Tier 2 Tag values
*/
public without sharing class AP_CreateTierHandler {
    /*This is a method which checks both review records for both Tier 1 and Tier 2 of the same application.*/
    
    public static void checkTier(Map<Id,Advisory_Panel_Application__c> advPanelApplicationMap, Map<Advisory_Panel_Application_Review__c,Id> advPanelApplicationReviewMap){
        Id recordTypeId  = Schema.SObjectType.Advisory_Panel_Application_Review__c.getRecordTypeInfosByName().get(Label.Tier_2_Review_Advsiory_Panel_on_Addressing_Disparities).getRecordTypeId();
        Id recordTypeId1 = Schema.SObjectType.Advisory_Panel_Application_Review__c.getRecordTypeInfosByName().get(Label.Tier_2_Review_Advisory_Panel_on_Clinical_Trials).getRecordTypeId(); 
        Id recordTypeId2 = Schema.SObjectType.Advisory_Panel_Application_Review__c.getRecordTypeInfosByName().get(Label.Tier_2_Review_Advisory_Panel_on_APDTO).getRecordTypeId();         
        Id recordTypeId3 = Schema.SObjectType.Advisory_Panel_Application_Review__c.getRecordTypeInfosByName().get(Label.Tier_2_Review_Advisory_Panel_on_Improving_Healthcare_System).getRecordTypeId(); 
        Id recordTypeId4 = Schema.SObjectType.Advisory_Panel_Application_Review__c.getRecordTypeInfosByName().get(Label.Tier_2_Review_Advisory_Panel_on_Rare_Disease).getRecordTypeId(); 
        Id recordTypeId5 = Schema.SObjectType.Advisory_Panel_Application_Review__c.getRecordTypeInfosByName().get(Label.Tier_2_Review_Advsiory_Panel_on_Communication_and_Dissemination_Research).getRecordTypeId(); 
        Id recordTypeId6 = Schema.SObjectType.Advisory_Panel_Application_Review__c.getRecordTypeInfosByName().get(Label.Tier_2_Review_Advsiory_Panel_on_Patient_Engagement).getRecordTypeId(); 
        Id recordTypeId7 = Schema.SObjectType.Advisory_Panel_Application_Review__c.getRecordTypeInfosByName().get(Label.AP_Tier_2_Review_Advisory_Panel_on_CEDS).getRecordTypeId(); 
        Id recordTypeId8 = Schema.SObjectType.Advisory_Panel_Application_Review__c.getRecordTypeInfosByName().get(Label.AP_Tier_2_Review_Advisory_Panel_on_HDDR).getRecordTypeId(); 
        String addressing         = Label.Addressing_Disparities;
        String apdto              = Label.APDTO; 
        String CEDS               = Label.CEDS; 
        String HDDR               = Label.HDDR; 
        String clinical           = Label.Clinical_Trials;
        String improvinghs        = Label.Improving_Healthcare_Systems;
        String rared              = Label.Rare_Disease;        
        String commdere           = Label.Communication_and_Dissemination_Research;
        String patiente           = Label.Patient_Engagement;
        String pass               = Label.Pass;
        String fail               = Label.Fail;
        String yes                = Label.Yes;
        String maybe              = Label.Maybe;
        String noo                = Label.No;
        String alternate          = Label.Alternate;
        String adminname          = Label.AdminUser;
        String tier1name          = Label.Tier_1_name;
        String tier1name1         = Label.Tier_1_name2;
        String tier2name          = Label.Tier_2_name;
        String tier2name1         = Label.Tier_2_name1;
        String tier1Status        = Label.Tier_1;
        String tier1SubStatus     = Label.Tier_1_Under_Review;
        String tier1SubStatusHold = Label.Tier_1_Hold;
        String tier2Status        = Label.Tier_2;
        String tier2SubStatus     = Label.Tier_2_Under_Review;
        String tier2SubStatusHold = Label.Tier_2_Hold;
        String tier3Status        = Label.Tier_3;
        String tier3SubStatus     = Label.Tier_3_Under_Review;
        
        
        User usr = [Select id, name from User where name=:adminname limit 1];
        // U- returns the user id of the User with name 'PCORI SF Administrator'
        
        Set<Id> panelApplicationIdSet = new Set<Id>();
        Set<String> reviewNameSet     = new Set<String>{tier1name,tier1name1,tier2name,tier2name1};
        Set<String> panelSet          = new Set<String>();
        
        for(Advisory_Panel_Application_Review__c originPanelReview : advPanelApplicationReviewMap.keySet())
        {
            panelApplicationIdSet.add(originPanelReview.Advisory_Panel_Application__c);
            panelSet.add(originPanelReview.Advisory_Panel__c);
        }
        
        Map<String,Advisory_Panel_Application_Review__c> panelAppAndReviewAndPanelMap = new Map<String,Advisory_Panel_Application_Review__c>();     
        
        for(Advisory_Panel_Application_Review__c advPanelReviewTemp : [select id,Tier_2_Tag__c, Review_Name__c,Review_Complete__c,Tier_1_Tag__c,
                                                                       Advisory_panel_s_applied_to__c,OwnerId,Advisory_Panel_Application__c,
                                                                       Advisory_Panel__c,Owner.Name,Candidate_Name__c 
                                                                       from Advisory_Panel_Application_Review__c
                                                                       where Advisory_Panel_Application__c in :panelApplicationIdSet and 
                                                                       Review_Name__c in :reviewNameSet and Advisory_Panel__c in :panelSet])
        {            
            String key = advPanelReviewTemp.Advisory_Panel_Application__c+'&&&'+advPanelReviewTemp.Review_Name__c+'&&&'+advPanelReviewTemp.Advisory_Panel__c;
            panelAppAndReviewAndPanelMap.put(key,advPanelReviewTemp);
        }
        
        
        for( String temp : panelAppAndReviewAndPanelMap.keySet())
        {
            //  system.assert(false,'value: ' +temp);       
            
        }
        
        for(Advisory_Panel_Application_Review__c originPanelReview : advPanelApplicationReviewMap.keySet())
        {
            Advisory_Panel_Application__c advisoryp = advPanelApplicationMap.get(advPanelApplicationReviewMap.get(originPanelReview));
            String key = originPanelReview.Advisory_Panel_Application__c+'&&&'+tier1name+'&&&'+originPanelReview.Advisory_Panel__c;
            String key1 = originPanelReview.Advisory_Panel_Application__c+'&&&'+tier1name1+'&&&'+originPanelReview.Advisory_Panel__c;
            Advisory_Panel_Application_Review__c advisoryr1 = panelAppAndReviewAndPanelMap.get(key);
            // ap - Returns the Advisory Panel Application Review record for Tier 1 Review 1 with the details queried in the SOQL
            Advisory_Panel_Application_Review__c advisoryr2 = panelAppAndReviewAndPanelMap.get(key1);
            // ap1 - Returns the Advisory Panel Application Review record for Tier 1 Review 2 with the details queried in the SOQL
            system.debug('===advisoryr1==='+advisoryr1);
            system.debug('===advisoryr2==='+advisoryr2);
            system.debug('===originPanelReview==='+originPanelReview);
            
            if((originPanelReview.Tier_2_Tag__c == null || (originPanelReview.Tier_2_Tag__c != null && originPanelReview.Tier_2_Tag__c != yes && originPanelReview.Tier_2_Tag__c != noo &&
                                                            originPanelReview.Tier_2_Tag__c != alternate )) && advisoryr1.Review_Complete__c == true && advisoryr2.Review_Complete__c==true)
            {
                
                system.debug('===tier2 record creation process satisfied===');
                
                if((advisoryr1.Tier_1_Tag__c == yes|| advisoryr1.Tier_1_Tag__c== Maybe) && (advisoryr2.Tier_1_Tag__c == yes ||advisoryr2.Tier_1_Tag__c == Maybe))
                {
                    
                    if(advisoryp.Clinical_Trials_Result__c!=pass && advisoryr1.Advisory_Panel__c.equals(clinical) && advisoryr2.Advisory_Panel__c.equals(clinical))
                    {
                        AP_CreateReviewersPanelHandler.createTier2(advisoryr1,advisoryr2,recordTypeId1,usr.id,clinical);
                        advisoryp.Clinical_Trials_Result__c=pass;
                    }
                    if(advisoryp.CEDS_Tier_1_Result__c!=pass && advisoryr1.Advisory_Panel__c.equals(CEDS) && advisoryr2.Advisory_Panel__c.equals(CEDS))
                    {
                        AP_CreateReviewersPanelHandler.createTier2(advisoryr1,advisoryr2,recordTypeId7,usr.id,CEDS);
                        advisoryp.CEDS_Tier_1_Result__c=pass;
                    }
                    if(advisoryp.HDDR_Tier_1_Result__c!=pass && advisoryr1.Advisory_Panel__c.equals(HDDR) && advisoryr2.Advisory_Panel__c.equals(HDDR))
                    {
                        AP_CreateReviewersPanelHandler.createTier2(advisoryr1,advisoryr2,recordTypeId8,usr.id,HDDR);
                        advisoryp.HDDR_Tier_1_Result__c=pass;
                    }                    
                    if(advisoryp.Rare_Disease_Result__c!=pass && advisoryr1.Advisory_Panel__c.equals(rared) && advisoryr2.Advisory_Panel__c.equals(rared))
                    {
                        AP_CreateReviewersPanelHandler.createTier2(advisoryr1,advisoryr2,recordTypeId4,usr.id,rared);
                        advisoryp.Rare_Disease_Result__c=pass;
                    }                    
                    if(advisoryp.Patient_Engagement_Result__c!=pass && advisoryr1.Advisory_Panel__c.equals(patiente) && advisoryr2.Advisory_Panel__c.equals(patiente))
                    {
                        AP_CreateReviewersPanelHandler.createTier2(advisoryr1,advisoryr2,recordTypeId6,usr.id,patiente);
                        advisoryp.Patient_Engagement_Result__c=pass;
                    }
                    if( advisoryp.Application_Status__c!=tier3Status)
                    {
                        advisoryp.Application_Status__c=tier2Status;
                        advisoryp.Application_Sub_Status__c=tier2SubStatus;
                    }
                }
                if((advisoryr1.Tier_1_Tag__c ==Maybe && advisoryr2.Tier_1_Tag__c==noo) || (advisoryr2.Tier_1_Tag__c==Maybe && advisoryr1.Tier_1_Tag__c==noo))
                {
                    if(advisoryp.Application_Status__c!=tier2Status && advisoryp.Application_Status__c!=tier3Status)
                    { 
                        advisoryp.Application_Status__c=tier1Status;
                        advisoryp.Application_Sub_Status__c=tier1SubStatusHold;
                    }                   
                    if(advisoryr1.Advisory_Panel__c.equals(clinical) || advisoryr2.Advisory_Panel__c.equals(clinical))
                    {
                        advisoryp.Clinical_Trials_Result__c=fail;
                    }
                    if(advisoryr1.Advisory_Panel__c.equals(CEDS) || advisoryr2.Advisory_Panel__c.equals(CEDS))
                    {
                        advisoryp.CEDS_Tier_1_Result__c=fail;
                    }
                    if(advisoryr1.Advisory_Panel__c.equals(HDDR) || advisoryr2.Advisory_Panel__c.equals(HDDR))
                    {
                        advisoryp.HDDR_Tier_1_Result__c=fail;
                    }
                    if(advisoryr1.Advisory_Panel__c.equals(rared) || advisoryr2.Advisory_Panel__c.equals(rared))
                    {
                        advisoryp.Rare_Disease_Result__c=fail;
                    }
                    if(advisoryr1.Advisory_Panel__c.equals(patiente) || advisoryr2.Advisory_Panel__c.equals(patiente))
                    {
                        advisoryp.Patient_Engagement_Result__c=fail;
                    }
                }                                                            
                
                if((advisoryr1.Tier_1_Tag__c ==noo && advisoryr2.Tier_1_Tag__c==noo))
                {
                    if(advisoryp.Application_Status__c!=tier2Status && advisoryp.Application_Status__c!=tier3Status)
                    { 
                        advisoryp.Application_Status__c=tier1Status;
                        advisoryp.Application_Sub_Status__c=tier1SubStatusHold;
                    }
                    if(advisoryr1.Advisory_Panel__c.equals(clinical) || advisoryr2.Advisory_Panel__c.equals(clinical))
                    {
                        advisoryp.Clinical_Trials_Result__c=fail;
                    }
                    if(advisoryr1.Advisory_Panel__c.equals(CEDS) || advisoryr2.Advisory_Panel__c.equals(CEDS))
                    {
                        advisoryp.CEDS_Tier_1_Result__c=fail;
                    }
                    if(advisoryr1.Advisory_Panel__c.equals(HDDR) || advisoryr2.Advisory_Panel__c.equals(HDDR))
                    {
                        advisoryp.HDDR_Tier_1_Result__c=fail;
                    }
                    if(advisoryr1.Advisory_Panel__c.equals(rared) || advisoryr2.Advisory_Panel__c.equals(rared))
                    {
                        advisoryp.Rare_Disease_Result__c=fail;
                    }
                    if(advisoryr1.Advisory_Panel__c.equals(patiente) || advisoryr2.Advisory_Panel__c.equals(patiente))
                    {
                        advisoryp.Patient_Engagement_Result__c=fail;
                    }
                }
                if((advisoryr1.Tier_1_Tag__c ==Yes && advisoryr2.Tier_1_Tag__c==Yes))
                {                    
                    if(advisoryr1.Advisory_Panel__c.equals(clinical) || advisoryr2.Advisory_Panel__c.equals(clinical))
                    {
                        advisoryp.Clinical_Trials_Result__c=pass;
                    }
                    if(advisoryr1.Advisory_Panel__c.equals(CEDS) || advisoryr2.Advisory_Panel__c.equals(CEDS))
                    {
                        advisoryp.CEDS_Tier_1_Result__c=pass;
                    }
                    if(advisoryr1.Advisory_Panel__c.equals(HDDR) || advisoryr2.Advisory_Panel__c.equals(HDDR))
                    {
                        advisoryp.HDDR_Tier_1_Result__c=pass;
                    }
                    if(advisoryr1.Advisory_Panel__c.equals(rared) || advisoryr2.Advisory_Panel__c.equals(rared))
                    {
                        advisoryp.Rare_Disease_Result__c=pass;
                    }
                    if(advisoryr1.Advisory_Panel__c.equals(patiente) || advisoryr2.Advisory_Panel__c.equals(patiente))
                    {
                        advisoryp.Patient_Engagement_Result__c=pass;
                    }
                }
                if((advisoryr1.Tier_1_Tag__c ==Maybe && advisoryr2.Tier_1_Tag__c==Maybe))
                {                    
                    if(advisoryr1.Advisory_Panel__c.equals(clinical) || advisoryr2.Advisory_Panel__c.equals(clinical))
                    {
                        advisoryp.Clinical_Trials_Result__c=pass;
                    }
                    if(advisoryr1.Advisory_Panel__c.equals(CEDS) || advisoryr2.Advisory_Panel__c.equals(CEDS))
                    {
                        advisoryp.CEDS_Tier_1_Result__c=pass;
                    }
                    if(advisoryr1.Advisory_Panel__c.equals(HDDR) || advisoryr2.Advisory_Panel__c.equals(HDDR))
                    {
                        advisoryp.HDDR_Tier_1_Result__c=pass;
                    }
                    if(advisoryr1.Advisory_Panel__c.equals(rared) || advisoryr2.Advisory_Panel__c.equals(rared))
                    {
                        advisoryp.Rare_Disease_Result__c=pass;
                    }
                    if(advisoryr1.Advisory_Panel__c.equals(patiente) || advisoryr2.Advisory_Panel__c.equals(patiente))
                    {
                        advisoryp.Patient_Engagement_Result__c=pass;
                    }
                }
                
                if((advisoryr1.Tier_1_Tag__c == yes|| advisoryr1.Tier_1_Tag__c == noo) && (advisoryr2.Tier_1_Tag__c == yes|| advisoryr2.Tier_1_Tag__c == noo) )
                {
                    if ((advisoryr1.Tier_1_Tag__c==yes && advisoryr2.Tier_1_Tag__c==noo) || (advisoryr2.Tier_1_Tag__c==yes && advisoryr1.Tier_1_Tag__c==noo) )
                    { 
                        if(advisoryp.Application_Status__c!=tier2Status && advisoryp.Application_Status__c!=tier3Status)
                        {                             
                            advisoryp.Application_Status__c=tier1Status;
                            advisoryp.Application_Sub_Status__c=tier1SubStatusHold; 
                        }                    
                        if(advisoryr1.Advisory_Panel__c.equals(clinical) || advisoryr2.Advisory_Panel__c.equals(clinical))
                        {
                            advisoryp.Clinical_Trials_Result__c=fail;
                        }
                        if(advisoryr1.Advisory_Panel__c.equals(CEDS) || advisoryr2.Advisory_Panel__c.equals(CEDS))
                        {
                            advisoryp.CEDS_Tier_1_Result__c=fail;
                        }
                        if(advisoryr1.Advisory_Panel__c.equals(HDDR) || advisoryr2.Advisory_Panel__c.equals(HDDR))
                        {
                            advisoryp.HDDR_Tier_1_Result__c=fail;
                        }                        
                        if(advisoryr1.Advisory_Panel__c.equals(rared) || advisoryr2.Advisory_Panel__c.equals(rared))
                        {
                            advisoryp.Rare_Disease_Result__c=fail;
                        }            
                        if(advisoryr1.Advisory_Panel__c.equals(patiente) || advisoryr2.Advisory_Panel__c.equals(patiente))
                        {
                            advisoryp.Patient_Engagement_Result__c=fail;
                        }
                    }
                }
                
            }            
            
            if(originPanelReview.Tier_2_Tag__c == yes||originPanelReview.Tier_2_Tag__c== noo ||originPanelReview.Tier_2_Tag__c == alternate)
            {
                String tier2Key = originPanelReview.Advisory_Panel_Application__c+'&&&'+tier2name+'&&&'+originPanelReview.Advisory_Panel__c;
                String tier2Key1 = originPanelReview.Advisory_Panel_Application__c+'&&&'+tier2name1+'&&&'+originPanelReview.Advisory_Panel__c;
                Advisory_Panel_Application_Review__c advisoryre1 = panelAppAndReviewAndPanelMap.get(tier2Key);
                Advisory_Panel_Application_Review__c advisoryre2 = panelAppAndReviewAndPanelMap.get(tier2Key1);                 
                
                if(advisoryre1.Review_Complete__c == true && advisoryre2.Review_Complete__c==true)
                {
                    if((advisoryre1.Tier_2_Tag__c == yes|| advisoryre1.Tier_2_Tag__c == alternate || advisoryre1.Tier_2_Tag__c == noo ) && (advisoryre2.Tier_2_Tag__c == yes|| advisoryre2.Tier_2_Tag__c==alternate || advisoryre2.Tier_2_Tag__c==noo) ){
                        if(advisoryre1.Tier_2_Tag__c==yes && advisoryre2.Tier_2_Tag__c==yes)
                        {                       
                            if(advisoryre1.Advisory_Panel__c.equals(clinical) || advisoryre2.Advisory_Panel__c.equals(clinical))
                            {
                                advisoryp.Clinical_Trials_Result1__c=pass;
                            }
                            
                            if(advisoryre1.Advisory_Panel__c.equals(CEDS) || advisoryre2.Advisory_Panel__c.equals(CEDS))
                            {
                                advisoryp.CEDS_Tier_2_Result__c=pass;
                            }
                            if(advisoryre1.Advisory_Panel__c.equals(HDDR) || advisoryre2.Advisory_Panel__c.equals(HDDR))
                            {
                                advisoryp.HDDR_Tier_2_Result__c=pass;
                            }
                            
                            if(advisoryre1.Advisory_Panel__c.equals(rared) || advisoryre2.Advisory_Panel__c.equals(rared))
                            {
                                advisoryp.Rare_Disease_Result1__c=pass;
                            }                            
                            if(advisoryre1.Advisory_Panel__c.equals(patiente) || advisoryre2.Advisory_Panel__c.equals(patiente))
                            {
                                advisoryp.Patient_Engagement_Result1__c=pass;
                            }
                            advisoryp.Application_Status__c=tier3Status;
                            advisoryp.Application_Sub_Status__c=tier3SubStatus;
                        }
                        if(advisoryre1.Tier_2_Tag__c == yes && advisoryre2.Tier_2_Tag__c == noo || advisoryre1.Tier_2_Tag__c==noo && advisoryre2.Tier_2_Tag__c==yes)
                        {
                            advisoryp.Application_Status__c=tier3Status;
                            advisoryp.Application_Sub_Status__c=tier3SubStatus;                           
                            if(advisoryre1.Advisory_Panel__c.equals(clinical) || advisoryre2.Advisory_Panel__c.equals(clinical))
                            {
                                advisoryp.Clinical_Trials_Result1__c=pass;
                            }                           
                            if(advisoryre1.Advisory_Panel__c.equals(CEDS) || advisoryre2.Advisory_Panel__c.equals(CEDS))
                            {
                                advisoryp.CEDS_Tier_2_Result__c=pass;
                            }
                            if(advisoryre1.Advisory_Panel__c.equals(HDDR) || advisoryre2.Advisory_Panel__c.equals(HDDR))
                            {
                                advisoryp.HDDR_Tier_2_Result__c=pass;
                            }
                            if(advisoryre1.Advisory_Panel__c.equals(rared) || advisoryre2.Advisory_Panel__c.equals(rared))
                            {
                                advisoryp.Rare_Disease_Result1__c=pass;
                            }                            
                            if(advisoryre1.Advisory_Panel__c.equals(patiente) || advisoryre2.Advisory_Panel__c.equals(patiente))
                            {
                                advisoryp.Patient_Engagement_Result1__c=pass;
                            }
                        }                        
                        
                        if(advisoryre1.Tier_2_Tag__c==alternate && advisoryre2.Tier_2_Tag__c==alternate){ 
                            
                            if(advisoryp.Application_Status__c!=tier3Status)
                            { 
                                advisoryp.Application_Status__c=tier2Status;
                                advisoryp.Application_Sub_Status__c=tier2SubStatusHold;
                            }                           
                            if(advisoryre1.Advisory_Panel__c.equals(clinical) || advisoryre2.Advisory_Panel__c.equals(clinical))
                            {
                                advisoryp.Clinical_Trials_Result1__c=fail;
                            }                           
                            if(advisoryre1.Advisory_Panel__c.equals(CEDS) || advisoryre2.Advisory_Panel__c.equals(CEDS))
                            {
                                advisoryp.CEDS_Tier_2_Result__c=fail;
                            }
                            if(advisoryre1.Advisory_Panel__c.equals(HDDR) || advisoryre2.Advisory_Panel__c.equals(HDDR))
                            {
                                advisoryp.HDDR_Tier_2_Result__c=fail;
                            }                           
                            if(advisoryre1.Advisory_Panel__c.equals(rared) || advisoryre2.Advisory_Panel__c.equals(rared))
                            {
                                advisoryp.Rare_Disease_Result1__c=fail;
                            }                            
                            if(advisoryre1.Advisory_Panel__c.equals(patiente)|| advisoryre2.Advisory_Panel__c.equals(patiente))
                            {
                                advisoryp.Patient_Engagement_Result1__c=fail;
                            }
                        }
                        if(advisoryre1.Tier_2_Tag__c==noo && advisoryre2.Tier_2_Tag__c==noo){ 
                            
                            if(advisoryp.Application_Status__c!=tier3Status)
                            { 
                                advisoryp.Application_Status__c=tier2Status;
                                advisoryp.Application_Sub_Status__c=tier2SubStatusHold;
                            }                          
                            if(advisoryre1.Advisory_Panel__c.equals(clinical) || advisoryre2.Advisory_Panel__c.equals(clinical))
                            {
                                advisoryp.Clinical_Trials_Result1__c=fail;
                            }                            
                            if(advisoryre1.Advisory_Panel__c.equals(CEDS) || advisoryre2.Advisory_Panel__c.equals(CEDS))
                            {
                                advisoryp.CEDS_Tier_2_Result__c=fail;
                            }
                            if(advisoryre1.Advisory_Panel__c.equals(HDDR) || advisoryre2.Advisory_Panel__c.equals(HDDR))
                            {
                                advisoryp.HDDR_Tier_2_Result__c=fail;
                            }                            
                            if(advisoryre1.Advisory_Panel__c.equals(rared) || advisoryre2.Advisory_Panel__c.equals(rared))
                            {
                                advisoryp.Rare_Disease_Result1__c=fail;
                            }                            
                            if(advisoryre1.Advisory_Panel__c.equals(patiente) || advisoryre2.Advisory_Panel__c.equals(patiente))
                            {
                                advisoryp.Patient_Engagement_Result1__c=fail;
                            }
                        }
                        if(advisoryre1.Tier_2_Tag__c==alternate && advisoryre2.Tier_2_Tag__c==yes || advisoryre1.Tier_2_Tag__c==yes && advisoryre2.Tier_2_Tag__c==alternate)
                        {
                            advisoryp.Application_Status__c=tier3Status;
                            advisoryp.Application_Sub_Status__c=tier3SubStatus;                            
                            if(advisoryre1.Advisory_Panel__c.equals(clinical) || advisoryre2.Advisory_Panel__c.equals(clinical))
                            {
                                advisoryp.Clinical_Trials_Result1__c=pass;
                            }                            
                            if(advisoryre1.Advisory_Panel__c.equals(CEDS) || advisoryre2.Advisory_Panel__c.equals(CEDS))
                            {
                                advisoryp.CEDS_Tier_2_Result__c=pass;
                            }
                            if(advisoryre1.Advisory_Panel__c.equals(HDDR) || advisoryre2.Advisory_Panel__c.equals(HDDR))
                            {
                                advisoryp.HDDR_Tier_2_Result__c=pass;
                            }                           
                            if(advisoryre1.Advisory_Panel__c.equals(rared) || advisoryre2.Advisory_Panel__c.equals(rared))
                            {
                                advisoryp.Rare_Disease_Result1__c=pass;
                            }                            
                            if(advisoryre1.Advisory_Panel__c.equals(patiente) || advisoryre2.Advisory_Panel__c.equals(patiente))
                            {
                                advisoryp.Patient_Engagement_Result1__c=pass;
                            }
                        }
                        if(advisoryre1.Tier_2_Tag__c==alternate && advisoryre2.Tier_2_Tag__c==noo || advisoryre1.Tier_2_Tag__c==noo && advisoryre2.Tier_2_Tag__c==alternate)
                        {
                            advisoryp.Application_Status__c=tier2Status;
                            advisoryp.Application_Sub_Status__c=tier2SubStatusHold;                            
                            if(advisoryre1.Advisory_Panel__c.equals(clinical) || advisoryre2.Advisory_Panel__c.equals(clinical))
                            {
                                advisoryp.Clinical_Trials_Result1__c=fail;
                            }                            
                            if(advisoryre1.Advisory_Panel__c.equals(CEDS) || advisoryre2.Advisory_Panel__c.equals(CEDS))
                            {
                                advisoryp.CEDS_Tier_2_Result__c=fail;
                            }
                            if(advisoryre1.Advisory_Panel__c.equals(HDDR) || advisoryre2.Advisory_Panel__c.equals(HDDR))
                            {
                                advisoryp.HDDR_Tier_2_Result__c=fail;
                            }                           
                            if(advisoryre1.Advisory_Panel__c.equals(rared) || advisoryre2.Advisory_Panel__c.equals(rared))
                            {
                                advisoryp.Rare_Disease_Result1__c=fail;
                            }                            
                            if(advisoryre1.Advisory_Panel__c.equals(patiente) || advisoryre2.Advisory_Panel__c.equals(patiente))
                            {
                                advisoryp.Patient_Engagement_Result1__c=fail;
                            }
                        }
                        
                    } 
                }
            }
        }
    }
}