public class ChatterFreePrivateGroupInvitees {      // (List<User> guestUsers)
    public static void UpdateProfile (Set<Id> guestUsersIds) {
        List<User> guestUsers = new List<User>();       
        //List<Profile> ChatterFreeUser = [Select Id From Profile where Name = 'Chatter Free User' limit 1]; // '00e700000013qfh' Chatter Free User profile id
        
        for ( user u: [Select Profile.UserLicense.Id,Profile.UserLicense.Name, ProfileId, Id From User where id in :guestUsersIds] ) {
            if ( u.Profile.UserLicense.Name == 'Chatter External' ) {
                //u.ProfileId = ChatterFreeUser[0].Id;
                u.ProfileId = Label.ChatterFreeLicense;		//License id setup in ChatterFreeLicense custom label
               guestUsers.add(u);
            } 
        } //end for loop
        update guestUsers;
    }
}