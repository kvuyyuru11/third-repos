@isTest
public class Dispatchtest {
    
    static testMethod void testRouterController() {
        
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();  
        list<User> u2 = TestDataFactory.getpartnerUserTestRecords(1,1);
        
        System.runAs(u2[0]) {            
            Merit_Reviewer_Application__c mr = new Merit_Reviewer_Application__c();
            // insert mr
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(mr);
            ApexPages.currentPage().getParameters().put('RecordType',recordTypeId);
            
            DispatcherLeadViewController RC = new DispatcherLeadViewController(sc);
            RC.redirectDefaultLead();
        }        
    }
    
    static testMethod void testRouterController2() {
        
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();  
        list<User> u2 = TestDataFactory.getpartnerUserTestRecords(1,1);
        
        System.runAs(u2[0]) {            
            Merit_Reviewer_Application__c   mr = new Merit_Reviewer_Application__c();
            // insert mr;
            
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(mr);
            ApexPages.currentPage().getParameters().put('RecordType',recordTypeId);
            
            DispatcherLeadViewController RC = new DispatcherLeadViewController(sc);
            RC.redirectDefaultLead();
        }
    }
}