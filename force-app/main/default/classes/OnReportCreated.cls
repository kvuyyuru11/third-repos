@IsTest
public with sharing class OnReportCreated
{
    public static testmethod void TestCreatePermission()
    {
        Account objAcc =  new Account(Name = 'Test');
        insert objAcc;
        
        FGM_Portal__Portal_Account__c objPortAcc = new FGM_Portal__Portal_Account__c(Name = 'TestPortAcc');
        insert  objPortAcc;
        
        Contact objPortalUser = new Contact();
        objPortalUser.LastName='TestLastName';
        objPortalUser.Email='testuser@gmail.com';
        objPortalUser.FGM_Portal__Confirm_Email__c ='testuser@gmail.com';
        objPortalUser.FGM_Portal__Password__c='123';
        objPortalUser.FGM_Portal__Confirm_Password__c='123';
        insert objPortalUser;
        
        Opportunity objOpp = new Opportunity();
        objOpp.Name = 'TestOpp';
        objOpp.Full_Project_Title__c = 'TestOpp';
        objOpp.Project_Name_off_layout__c='TestOpp';
        objOpp.AccountId = objAcc.Id;
        objOpp.Amount =  12000;
        objOpp.CloseDate = Date.today();
        objOpp.FGM_Base__Amount_Requested__c = 12000;
        objOpp.StageName = 'Invited';
        insert objOpp;
        
        FGM_Base__Grantee_Report__c objReport1  = new FGM_Base__Grantee_Report__c();
        objReport1.FGM_Base__Request__c = objOpp.Id;
        objReport1.FGM_Base__Due_Date__c = Date.today().addDays(30);
        objReport1.FGM_Base__Interim_Report__c = true;
        insert objReport1;
        
        FGM_Portal__Portal_Permission__c objPortPerm = new FGM_Portal__Portal_Permission__c();
        objPortPerm.FGM_Portal__Opportunity__c = objOpp.Id;
        objPortPerm.FGM_Portal__Portal_Account__c  = objPortAcc.Id;
        objPortPerm.FGM_Portal__Contact__c = objPortalUser.Id;
        objPortPerm.Grantee_Report__c = objReport1.Id;
        insert objPortPerm;
        
        Test.startTest();
        
        list<FGM_Base__Grantee_Report__c> lstReports =  new list<FGM_Base__Grantee_Report__c>();
        
        FGM_Base__Grantee_Report__c objReport2  = new FGM_Base__Grantee_Report__c();
        objReport2.FGM_Base__Request__c = objOpp.Id;
        objReport2.FGM_Base__Due_Date__c = Date.today().addDays(35);
        objReport2.FGM_Base__Interim_Report__c = true;
        lstReports.add(objReport2);
        
        FGM_Base__Grantee_Report__c objReport3  = new FGM_Base__Grantee_Report__c();
        objReport3.FGM_Base__Request__c = objOpp.Id;
        objReport3.FGM_Base__Due_Date__c = Date.today().addDays(40);
        objReport3.FGM_Base__Interim_Report__c = true;
        lstReports.add(objReport3);
        
        FGM_Base__Grantee_Report__c objReport4  = new FGM_Base__Grantee_Report__c();
        objReport4.FGM_Base__Request__c = objOpp.Id;
        objReport4.FGM_Base__Due_Date__c = Date.today().addDays(45);
        objReport4.FGM_Base__Final_Report__c = true;
        lstReports.add(objReport4);
        
        insert lstReports;
        
        Test.stopTest();
    }
    
    public static void CreatePermission(FGM_Base__Grantee_Report__c[] arrNewGranteeReport)
    {
        set<ID> setOpportunityID = new set<ID>();
        for(FGM_Base__Grantee_Report__c objGranteeReport : arrNewGranteeReport)
            setOpportunityID.add(objGranteeReport.FGM_Base__Request__c);
        
        List<FGM_Portal__Portal_Permission__c> lstPortalPermission = [Select id,Name,FGM_Portal__Portal_Account__c,FGM_Portal__Opportunity__c, FGM_Portal__Lead__c, FGM_Portal__Account__c,FGM_Portal__Contact__c From FGM_Portal__Portal_Permission__c where FGM_Portal__Opportunity__c in :setOpportunityID];
        
        Map<ID,List<FGM_Portal__Portal_Permission__c>> mapOpportunityPermission = new Map<ID,List<FGM_Portal__Portal_Permission__c>>();
        
        if(lstPortalPermission != null && lstPortalPermission.size() > 0)
        {
            for(FGM_Portal__Portal_Permission__c objPortalPermission : lstPortalPermission)
            {
                List<FGM_Portal__Portal_Permission__c> lstPermission = new List<FGM_Portal__Portal_Permission__c>();
                
                if(mapOpportunityPermission.containsKey(objPortalPermission.FGM_Portal__Opportunity__c))
                    lstPermission = mapOpportunityPermission.get(objPortalPermission.FGM_Portal__Opportunity__c);
                lstPermission.add(objPortalPermission);
                
                mapOpportunityPermission.put(objPortalPermission.FGM_Portal__Opportunity__c,lstPermission);
            }
        }
        
        List<FGM_Portal__Portal_Permission__c> lstNewPermission = new List<FGM_Portal__Portal_Permission__c>();
        for(FGM_Base__Grantee_Report__c objGranteeReport : arrNewGranteeReport)
        {
            List<FGM_Portal__Portal_Permission__c> lstPermission = mapOpportunityPermission.get(objGranteeReport.FGM_Base__Request__c);
            
            if(lstPermission != null)
            {
                for(FGM_Portal__Portal_Permission__c objPermission : lstPermission)
                {
                    FGM_Portal__Portal_Permission__c objTempPermission = new FGM_Portal__Portal_Permission__c();
                    objTempPermission.Grantee_Report__c = objGranteeReport.Id;
                    objTempPermission.FGM_Portal__Portal_Account__c = objPermission.FGM_Portal__Portal_Account__c;
                    objTempPermission.FGM_Portal__Account__c = objPermission.FGM_Portal__Account__c;
                    objTempPermission.FGM_Portal__Contact__c = objPermission.FGM_Portal__Contact__c;
                    lstNewPermission.add(objTempPermission);
                }
            }
        }
        
        Database.Saveresult [] arrSvRes = null;
        //try
        //{
        if(lstNewPermission != null && lstNewPermission.size() > 0)
            arrSvRes = Database.insert(lstNewPermission);
        
        if(arrSvRes != null && (arrNewGranteeReport != null && arrNewGranteeReport.size() > 0))
        {
            for(Database.Saveresult svRes : arrSvRes)
            {
                if(!svRes.isSuccess())
                    arrNewGranteeReport[0].addError(svRes.getErrors()[0].getMessage());
            }
        }
    }
}