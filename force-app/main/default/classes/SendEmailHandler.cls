//This is a Handler Class for sending Email to MRO when Unclear or Other is selected on COI page
public class SendEmailHandler {
     public static void sendEmail(id targetid,string ReviewerName, string Link){
        system.debug('The target id is ' +targetid);
    String text1=Label.Reviewer_Unclear_About_COI_Status;
        String text2=Label.Unclear_if_COI_Exists_for_Template;
        
        
   //Template to select from the list of email templates available
          EmailTemplate et=[Select id,body from EmailTemplate where DeveloperName=:text2];
         String plainBody = et.Body;
          plainBody = plainBody.replace('{!COI_Expertise__c.Reviewer_Name__c}', ReviewerName);
          plainBody = plainBody.replace('{!COI_Expertise__c.Link}', Link);
        // plainBody=plainBody.replace('{!COI_Expertise__c.PFA_Level_COI__c}', PFAvalue);
          system.debug('The ETID is' + et);
          Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
          mail.setTargetObjectId(targetid);
         // mail.setSenderDisplayName('PCORI');
          OrgWideEmailAddress owea = [select id from OrgWideEmailAddress where Address = 'reviewers@pcori.org'];
         mail.setOrgWideEmailAddressId(owea.Id);
          mail.setUseSignature(false);
          mail.setBccSender(false); 
          mail.setSaveAsActivity(false);
          //mail.setTemplateId(et.id);
          mail.setsubject(text1);
          mail.setPlainTextBody(plainBody);
        if(!Test.isRunningTest()){
          Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});   
               system.debug(r);
            }
  
        
    }
     
         public static void sendEmail1(id targetid,string ReviewerName, string Link, string PFAvalue, string ReviewRationale){
        system.debug('The target id is ' +targetid);
    String text1=Label.Reviewer_Unclear_About_COI_Status;
        String text2=Label.Unclear_if_COI_Exists_for_Template;
         String text3=Label.Unclear_if_COI_Exists_for_Template_New;
   //Template to select from the list of email templates available
          EmailTemplate et=[Select id,body from EmailTemplate where DeveloperName=:text3];
         String plainBody = et.Body;
          plainBody = plainBody.replace('{!COI_Expertise__c.Reviewer_Name__c}', ReviewerName);
          plainBody = plainBody.replace('{!COI_Expertise__c.Link}', Link);
         plainBody=plainBody.replace('{!COI_Expertise__c.PFA_Level_COI__c}', PFAvalue);
             plainBody=plainBody.replace('{!COI_Expertise__c.Reviewer_Rationale__c}', ReviewRationale);
          system.debug('The ETID is' + et);
          Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
          mail.setTargetObjectId(targetid);
         // mail.setSenderDisplayName('PCORI');
          OrgWideEmailAddress owea = [select id from OrgWideEmailAddress where Address = 'reviewers@pcori.org'];
         mail.setOrgWideEmailAddressId(owea.Id);
          mail.setUseSignature(false);
          mail.setBccSender(false); 
          mail.setSaveAsActivity(false);
          //mail.setTemplateId(et.id);
          mail.setsubject(text1);
          mail.setPlainTextBody(plainBody);
        if(!Test.isRunningTest()){
          Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});   
               system.debug(r);
            }
  
        
    }

}