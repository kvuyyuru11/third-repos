/*------------------------------------------------------------------------------------------------------------------------------------
Code Written by SADC PCORI Team: Matt Hoffman
Written During Development Phase 2: June/2016 - September/2016
Application ID: 
Requirement ID: 

This class is called from the Panel Standard Detail page and calculated the rank, percentile, and quartile for each valid opportunity
in this panel.
When the button is pressed, it forwards to a blank page, Calculate Rank Forward Page, (for about 2 seconds) with this controller that does the work 
and then sends them back to the panel detail page with the information filled in.
-------------------------------------------------------------------------------------------------------------------------------------*/
global class CalculateRank_Percintile_Quartile {
    
    public String returnPanelId {get;set;}

    public CalculateRank_Percintile_Quartile()
    {
        returnPanelId = ApexPages.currentPage().getParameters().get('panel');   
    }
    
    @TestVisible void doRankCalc()
    {
        if (!String.isBlank(returnPanelId))
        {
            List<Updater_Worker__c> insertList = new List<Updater_Worker__c>();
            Map<Id, Opportunity> allOppsForPanel = new Map<Id, Opportunity>();
            
           
            List<Opportunity> oppList = new List<Opportunity>();
            List<Opportunity> oppListQuery = [SELECT ID, Discussion_Order_Rank__c, Average_In_Person_Score__c, Percentile__c, Quartile__c
                                    FROM Opportunity
                                    WHERE Panel__c =: returnPanelId];
                                   //AND Average_In_Person_Score__c != Null
                                   //AND Average_In_Person_Score__c != 0];                       
            System.debug('query size: ' + oppListQuery.size());
            
            integer numApplicationsForPanel = 0;
            for (Opportunity opp : oppListQuery)
            {
                if (!String.isBlank(String.valueOf(opp.Average_In_Person_Score__c)))
                {
                    oppList.add(opp);
                    numApplicationsForPanel++;
                }
                allOppsForPanel.put(opp.id, opp);
            }
            //System.debug('valid size:' + oppList.size());
            
            // Begin sorting. Could have implemented the Comparable interface, but I opted out.
            List<Integer> usedIndexes = new List<Integer>();
            
            List<Opportunity> sortedList = new List<Opportunity>();
           
            integer listSize = oppList.size();
            
            for (integer i=0; i<listSize; i++)
            {
                integer tempLowestIndex = 0;
                double tempLowestScore = 101;
                
                for (integer j=0; j<listSize; j++)
                {                  
                    if ((oppList[j].Average_In_Person_Score__c < tempLowestScore) && indexNotAlreadyUsed(usedIndexes, j))
                    {
                        tempLowestScore = oppList[j].Average_In_Person_Score__c;
                        tempLowestIndex = j;
                    }                    
                }
                
                usedIndexes.add(tempLowestIndex);
                sortedList.add(oppList[tempLowestIndex]);
            }
            
            // list should now be sorted. go through one by one and see if we need to update the rank or not.
            // We could update the opportunity directly in this class, but that would involve querying over all the Opportunity fields
            // and that needs to be avoided. Instead, we use a hidden class that we insert records into. 
            // The dummy records update the Opportunity using the process builder and then delete themselves.
             
            for (integer i=0; i<listSize; i++)
            {
                Updater_Worker__c newWorker = new Updater_Worker__c();
                newWorker.Opportunity__c = sortedList[i].id;
                newWorker.New_Rank__c = (i+1);
                
                // calculate percentile

                if (numApplicationsForPanel == 0)
                    newWorker.New_Percentile__c = 0;
                else
                    newWorker.New_Percentile__c = ((100*newWorker.New_Rank__c) / numApplicationsForPanel);
                
                // calculate quartile
                integer quartile;
                if (newWorker.New_Percentile__c < 25)
                {
                    quartile = 1;
                }
                else if (newWorker.New_Percentile__c < 50)
                {
                    quartile = 2;
                }
                else if (newWorker.New_Percentile__c < 75)
                {
                    quartile = 3;
                }
                else quartile = 4;
                
                newWorker.New_Quartile__c = quartile;
                newWorker.Operation_Number__c = 1;
                // see if anything is different
                Opportunity theOpp = allOppsForPanel.get(newWorker.Opportunity__c);
                if ((theOpp.Discussion_Order_Rank__c != newWorker.New_Rank__c) || 
                    (theOpp.Percentile__c != newWorker.New_Percentile__c) || 
                    (theOpp.Quartile__c != newWorker.New_Quartile__c))
                {
                    insertList.add(newWorker);
                }
                
                             
                // we no longer need to make a null/blank Update Worker for this opportunity, so remove it from the map
                allOppsForPanel.remove(newWorker.Opportunity__c);
            }            
            
            // any opportunities left in the allOppsForPanel map did not get updated, so make all of those fields null/empty
            for (Id oppId : allOppsForPanel.keySet())
            {
                // if all of those fields on Opp are already null, then don't need to do anything to them.
                Opportunity theOpp = allOppsForPanel.get(oppId);
                if (String.isBlank(String.valueOf(theOpp.Discussion_Order_Rank__c)) && String.isBlank(String.valueOf(theOpp.Percentile__c)) && String.isBlank(String.valueOf(theOpp.Quartile__c)))
                {
                    // do nothing
                    // system.debug('found previous nulls that im now skipping!!');
                }
                else 
                {
                    Updater_Worker__c newWorker = new Updater_Worker__c();
                    newWorker.Opportunity__c = oppId;
                    newWorker.New_Rank__c = null;
                    newWorker.New_Quartile__c = null;
                    newWorker.New_Percentile__c = null;
                    insertList.add(newWorker);
                }
                
            }
            
            //System.debug('insert list size:' + insertList.size());
            insert insertList;
        }
        
    }
    
    
    @TestVisible boolean indexNotAlreadyUsed(List<Integer> indexList, integer index)
    {
        for (Integer i=0; i<indexList.size(); i++)
        {
            if (indexList[i] == index)
            {
                return false;
            }
        }      
        return true;
    }
    
  
    @TestVisible public PageReference workAndRedirect()
    {
        doRankCalc();
        
        PageReference pr = new PageReference('/' + returnPanelId);
        pr.setRedirect(true);
        
        return pr;
    }
    
    
}