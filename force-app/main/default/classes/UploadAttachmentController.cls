//This is a class that is used to upload attachments on Attachments custom Object due to limitation of notes and attachments not available in communities on contact object
public class UploadAttachmentController {
    
    public String description {get;set;}
    private Contact contact {get;set;} 
    public String fileName {get;set;}
    public Blob fileBody {get;set;}
    
    public UploadAttachmentController(ApexPages.StandardController controller) { 
        this.contact = (Contact)controller.getRecord();
    }   
    
    // Below method creates a new Contact_Attachment__c record
    public Database.SaveResult saveCustomAttachment() {
        Attachment__c obj = new Attachment__c();
        obj.contact__c = contact.Id; 
        obj.File_Name__c = description;
        // fill out cust obj fields
        return Database.insert(obj);
    }
    
    // creates an actual Attachment record with the Contact_Attachment__c as parent
    public Database.SaveResult saveStandardAttachment(Id parentId) {
        Database.SaveResult result;
        
        Attachment attachment = new Attachment();
        attachment.body = this.fileBody;
        attachment.name = this.fileName;
        attachment.parentId = parentId;
        // insert the attahcment
        result = Database.insert(attachment);
        // resets the file for the view state
        fileBody = Blob.valueOf(' ');
        return result;
    }
    
    /**
* Upload process is:
*  1. Insert new Contact_Attachment__c record
*  2. Insert new Attachment with the new Contact_Attachment__c record as parent
*  3. Update the Contact_Attachment__c record with the ID of the new Attachment
**/
    public PageReference processUpload() {
        try {
            Database.SaveResult customAttachmentResult = saveCustomAttachment();
            
            if (customAttachmentResult == null || !customAttachmentResult.isSuccess()) {
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                                                           'Could not save attachment.'));
                return null;
            }
            
            Database.SaveResult attachmentResult = saveStandardAttachment(customAttachmentResult.getId());
            
            if (attachmentResult == null || !attachmentResult.isSuccess()) {
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                                                           'Could not save attachment.'));            
                return null;
            } else {
                // update the custom attachment record with some attachment info
                Attachment__c customAttachment = [select id from Attachment__c where id = :customAttachmentResult.getId()];
                customAttachment.name = this.fileName;
                customAttachment.Attachment__c = attachmentResult.getId();
                update customAttachment;
            }
            
        } catch (Exception e) {
            ApexPages.AddMessages(e);
            return null; 
        }
        
        return new PageReference('/'+contact.Id);
    }
    
    public PageReference back() {
        return new PageReference('/'+contact.Id);
    }     
    
}