/****************************************************************************************************
*Description:   		This is the test class for the FCUtility class that handles password encoding 
*						and decoding for Foundation Connect
*
*Required Class(es):	FCUtility_Test
*
*Organization: Rainmaker-LLC
*Revision   |Date       |Author             |AdditionalNotes
*====================================================================================================
*   1.0     03/21/2014   Justin Padilla      Initial Version
*****************************************************************************************************/
@isTest
private class FCUtility_Test {

    static testMethod void GeneratePasswordTest()
	{
        User u = [SELECT Id,Email FROM User Where IsActive = true LIMIT 1];
        string newPassword = FCUtility.GeneratePassword(u);
        system.debug(newPassword);
    }
    
    static testMethod void DecodePasswordTest()
    {
    	User u = [SELECT Id,Email FROM User Where IsActive = true LIMIT 1];
        string newPassword = FCUtility.GeneratePassword(u);
        //system.assertEquals(u.Email, FCUtility.DecodePassword(newPassword));
        string decoded = FCUtility.DecodePassword(newPassword);
    }
    
    static testMethod void PasswordUpdateTest()
    {
    	String profileId = Label.CommunityAdminAccount;
    	String accountId = Label.CommunityAdminProfile;
    	
    	//Profile CProfile = [SELECT Id, Name FROM Profile WHERE Name ='Customer Community Login User'];
    	Profile AProfile = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator'];
    	User u = [SELECT Id FROM User WHERE ProfileId =:AProfile.Id AND isActive = true LIMIT 1];
    	User temp = new User();
    	
    	system.runAs(u) //Insert Setup Objects
    	{
    		Account account = new Account(Name='Test');
    		insert account;
    		Contact contact = new Contact(FirstName='Rainmaker', LastName='Test', AccountId=account.Id);
    		insert contact;
    		temp.Email = 'testing@test.com';
    		temp.ContactId = contact.Id;
    		temp.UserName = 'testing' + Integer.valueOf(Math.random()) +'@test' + Integer.valueOf(Math.random()) + '.com';
    		temp.LastName = 'Padilla';
    		temp.Alias = 'jpadilla';
    		temp.CommunityNickName = 'jpadilla';
    		temp.TimeZoneSidKey = 'GMT';
    		temp.LocaleSidKey = 'en_US';
    		temp.EmailEncodingKey = 'ISO-8859-1';
    		temp.ProfileId = profileId;
    		temp.LanguageLocaleKey = 'en_US';
    		insert temp;
    		String userId = Site.createPortalUser(temp, accountId, 'Testing');
    	}
        
        FCUtility.PasswordUpdate(new Set<Id>{temp.Id});
    }
    
}