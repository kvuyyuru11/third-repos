public class InterestedinPeerReviewController {
    
     public String emaill {get; set;} 
    public string Interested {get; set;} 
    
    public InterestedinPeerReviewController() {
        interested='Yes';
    }
    
     public List<SelectOption> getitemsInterested() {
        List<SelectOption> options = new List<SelectOption>(); 
       
       options.add(new SelectOption('Yes','Yes, include me in the PCORI Peer Review Program'));
  options.add(new SelectOption('No','No, do not include me in the PCORI Peer Review Program'));

        return options; 
    } 
    
    
    public PageReference submit() {
       
         String err = 'The email provided does not match a PCORI contact on file.  Please use the email address associated to your PCORI involvement. To update your email address with PCORI, please email<a href="mailto:peerreview@pcori.org" style="font-size:13px">peerreview@pcori.org.</a>';
  		list<Contact> cnt=[select id,email,Interested_in_Peer_Review__c from Contact where email=:emaill];
        if(emaill==null||emaill==''){
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, err));
            return null;
        }
        if(!cnt.isempty()){
        cnt[0].Interested_in_Peer_Review__c=Interested;
        database.update(cnt);
  		PageReference pr = Page.ConfirmedInterestedinPeerReview;
  		pr.setRedirect(true);
  		return pr;
        }
      
       
        else{
              ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, err));
            return null;
        }
        
        
  	}

}