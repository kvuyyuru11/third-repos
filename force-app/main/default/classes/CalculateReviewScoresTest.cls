@isTest
public class CalculateReviewScoresTest {
    public static testMethod void TestScoreCalculation(){
        RecordType recOpp = [SELECT Id FROM RecordType WHERE sObjectType = 'Opportunity' AND Name = 'RA Applications'];
        RecordType recAcc = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Standard Salesforce Account'];
            
    Account acc = new Account();
        acc.RecordTypeId = recAcc.Id;
        acc.Name = 'Test Acc';
        insert acc;
       
            set<Id> opprtlist= new set<Id>();
        List<Opportunity> opplst = new List<Opportunity>();
        for(Integer i = 0; i < 10; i++){
          Opportunity opp = new Opportunity();
            opp.RecordTypeId = recOpp.Id;
            opp.Awardee_Institution_Organization__c = acc.Id;
            opp.Full_Project_Title__c = 'test0';
            opp.Project_Name_off_layout__c= 'test0';
            opp.Name = 'Test' + i;
            opp.Discussion_Order_Rank__c = 12.0 + i;
            opp.Average_In_Person_Score__c = 1.0;
            opp.Percentile__c = 20.0;
            opp.Quartile__c = 2.0;
            //opp.Panel__c=pnl[0].Id;
            opp.CloseDate = Date.newInstance(2016, 12, 31);
            opp.Application_Number__c = '45125';
            opp.StageName = 'In Progress';
            opplst.add(opp);
        }
        insert opplst;
        
        RecordType rec = [SELECT Id FROM RecordType WHERE sObjectType = 'FC_Reviewer__External_Review__c' AND DeveloperName = 'In_Person_Review'];
        List<FC_Reviewer__External_Review__c> revlst = new List<FC_Reviewer__External_Review__c>();
        for(Integer i = 0; i < 3; i++){
            FC_Reviewer__External_Review__c rev = new FC_Reviewer__External_Review__c();
            rev.Overall_Score__c = '4';
            rev.RecordTypeId = rec.Id;
            rev.FC_Reviewer__Opportunity__c = opplst[i].Id;
            rev.FC_Reviewer__Status__c = 'Review Submitted';
            //rev.Panel__c=pnl[0].Id;
            revlst.add(rev);
        }
        insert revlst;
        for(FC_Reviewer__External_Review__c r:revlst){
            opprtlist.add(r.FC_Reviewer__Opportunity__c);
        }
        list<opportunity>  roplist=[select Id,Average_Online_Review_Score__c,All_Reviews_Completed__c,Average_In_Person_Score__c from opportunity where ID IN:opprtlist];

        RecordType rec1 = [SELECT Id FROM RecordType WHERE sObjectType = 'FC_Reviewer__External_Review__c' AND DeveloperName = 'Initial_Review'];
        FC_Reviewer__External_Review__c rev1 = new FC_Reviewer__External_Review__c();
        rev1.RecordTypeId = rec1.Id;
       // rev1.Panel__c=pnl[0].Id;
        rev1.FC_Reviewer__Opportunity__c = opplst[5].Id;
        rev1.FC_Reviewer__Status__c = 'Review Submitted';
        insert rev1;        
        
        CalculateReviewScores.calculateScore(roplist);
   
    }
    
   public static testMethod void TestScoreCalculation1(){
    list<panel__c> pnl=TestDataFactory.getpanelwithoppandpanelassgnandcoiTestRecords(1,20,10,1,7);
        
        list<COI_Expertise__c> coilist=[select id,Active__c,Conflict_of_Interest__c from COI_Expertise__c where Panel_R2__c=:pnl[0].Id];
        list<COI_Expertise__c> coilistnew = new list<COI_Expertise__c>();
        for(COI_Expertise__c c:coilist){
            c.Conflict_of_Interest__c='No';
            coilistnew.add(c);
        }
        update coilistnew;
        test.startTest();
        CreateInPersonReview.createInPersonRecord(pnl[0].Id);
        Id rvwRecordTypeId1 = Schema.SObjectType.FC_Reviewer__External_Review__c.getRecordTypeInfosByName().get(Label.InPersonReview).getRecordTypeId();
        Id rvwRecordTypeId2 = Schema.SObjectType.FC_Reviewer__External_Review__c.getRecordTypeInfosByName().get(Label.InPersonPreview).getRecordTypeId(); 
 
       List<FC_Reviewer__External_Review__c> rlist = [SELECT Id,FC_Reviewer__Contact__c,FC_Reviewer__Opportunity__c FROM FC_Reviewer__External_Review__c WHERE Panel__c = :pnl[0].Id  AND (RecordTypeId = :rvwRecordTypeId2 OR RecordTypeId = :rvwRecordTypeId1)];
      List<FC_Reviewer__External_Review__c> rlisttoupdate= new List<FC_Reviewer__External_Review__c>();
       for(FC_Reviewer__External_Review__c r:rlist){
            r.Overall_Score__c = '4';
            r.RecordTypeId = rvwRecordTypeId1;
            r.FC_Reviewer__Status__c = 'Review Submitted';
            rlisttoupdate.add(r);
       }
        update rlisttoupdate;
       CalculateInpersonAvgscores.calculatewocallingreviewscoresclass(pnl[0].Id);
    }
    
    
       public static testMethod void TestScoreCalculation2(){
    list<panel__c> pnl=TestDataFactory.getpanelwithoppandpanelassgnandcoiTestRecords(1,20,10,1,7);
        
        list<COI_Expertise__c> coilist=[select id,Active__c,Conflict_of_Interest__c from COI_Expertise__c where Panel_R2__c=:pnl[0].Id];
        list<COI_Expertise__c> coilistnew = new list<COI_Expertise__c>();
        for(COI_Expertise__c c:coilist){
            c.Conflict_of_Interest__c='No';
            coilistnew.add(c);
        }
        update coilistnew;
        test.startTest();
        CreateInPersonReview.createInPersonRecord(pnl[0].Id);
        Id rvwRecordTypeId1 = Schema.SObjectType.FC_Reviewer__External_Review__c.getRecordTypeInfosByName().get(Label.InPersonReview).getRecordTypeId();
        Id rvwRecordTypeId2 = Schema.SObjectType.FC_Reviewer__External_Review__c.getRecordTypeInfosByName().get(Label.InPersonPreview).getRecordTypeId(); 
 
       List<FC_Reviewer__External_Review__c> rlist = [SELECT Id,FC_Reviewer__Contact__c,FC_Reviewer__Opportunity__c FROM FC_Reviewer__External_Review__c WHERE Panel__c = :pnl[0].Id  AND (RecordTypeId = :rvwRecordTypeId2 OR RecordTypeId = :rvwRecordTypeId1)];
  
       CalculateInpersonAvgscores.calculatewocallingreviewscoresclass(pnl[0].Id);
    }
}