public with sharing class BudgetSummary {

	/* this class represents one row of the budget summary table
	   its static method getBudgetSummaryList(budget) will return the entire list of rows for the budget summary
	   To add rows to the budget summary do not alter this class, instead add the field to the Budget_Summary fieldset.
	   Also it will be necessary to add to the query in either BudgetPageController.cls or BudgetSummaryTableController.cls.
	   If any fields will not have a peer review period add them to the Final_Year_NA fieldset.
	*/

	private Budget__c budget;

	public String category {get;set;}
	public Map<Decimal, String> yearMap {get;set;}
	public String rowTotals {get;set;}

	public BudgetSummary() {
		
	}

	public BudgetSummary(Budget__c budget) {
		this.budget = budget;
	}

	/* Assumes anyBudgetYearCostField has format (substring)(numerical year)(non-numerical substring)
	   Will get all of the row data for the cost category which is determined by anyBudgetYearCostField
	   Determines the row label from anyBudgetYearCostField's label and assumes it has the format (Text)('Year ')(numerical year)
	   See static method getBudgetSummaryList() below for example on how to use this method
	*/
	public void setRow(String anyBudgetYearCostField) {
		List<String> fieldParts;
		String fieldStart;
		String fieldEnd;
		Decimal amount;
		Decimal tempRowTotals = 0;

		this.yearMap = new Map<Decimal,String>();

		//set the category equal to the label of the field
		Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
		Schema.SObjectType sObj = schemaMap.get('Budget__c');
		Map<String, Schema.SObjectField> fieldMap = sObj.getDescribe().fields.getMap();
		this.category = fieldMap.get(anyBudgetYearCostField).getDescribe().getLabel().replaceFirst('\\s*([Ff]or)?\\s*[yY]ear.*$','');

		//fields with no peer review period
		List<Schema.FieldSetMember> naFields = Schema.SObjectType.Budget__c.fieldSets.Final_Year_NA.getFields();

		//get field start and field end
		fieldParts = this.getBudgetYearCostFieldParts(anyBudgetYearCostField);
		if(fieldParts == null) {
			return;
		}
		fieldStart = fieldParts[0];
		fieldEnd = fieldParts[2];

		//map each year's costs to yearMap
		for(Integer i=1; i<=Integer.valueOf(System.Label.Max_Years_Budget_Covers); i++) {
			//don't map if it is a cost category for which the peer review year isn't used
			if(i == Integer.valueOf(System.Label.Max_Years_Budget_Covers)) {
				Boolean naField = false;
				for(Schema.FieldSetMember field : naFields) {
					String naYear = this.getBudgetYearCostFieldParts(field.getFieldPath())[1];
					if(field.getFieldPath().equals(fieldStart + naYear + fieldEnd)) {
						this.yearMap.put(i,'NA');
						naField = true;
						break;
					}
				}
				if(naField == true) {
					break;
				}
			}
			
			if(this.budget != null && this.budget.get(fieldStart + i + fieldEnd) != null) {
				amount = (Decimal) this.budget.get(fieldStart + i + fieldEnd);
			}
			else
			{
				amount = 0;
			}
			this.yearMap.put(i, '$' + getcurrency(amount.setScale(2)) );
			tempRowTotals += amount;
		}
		this.rowTotals = '$' + getcurrency(tempRowTotals.setScale(2));
	}

	private List<String> getBudgetYearCostFieldParts(String anyBudgetYearCostField) {
		//get non numerical parts of anyBudgetYearCostField into fieldStart and fieldEnd
		Pattern p = Pattern.compile('^((.*\\.)?.*[^\\d]*)(\\d)+([^\\d]*)$');
		Matcher pm = p.matcher(anyBudgetYearCostField);
		List<String> parts = new List<String>();
		if( pm.matches() ) {
			parts.add(pm.group(1));
			parts.add(pm.group(3));
			parts.add(pm.group(4));
			return parts;
		} else {
			return null;
		}
	}

	public static List<BudgetSummary> getBudgetSummaryList(Budget__c budget){
		List<BudgetSummary> budgetSummaryList = new List<BudgetSummary>();
		BudgetSummary summaryRow;

		List<Schema.FieldSetMember> budgetSummaryFields = Schema.SObjectType.Budget__c.fieldSets.Budget_Summary.getFields();

		for (Schema.FieldSetMember field : budgetSummaryFields) {
			summaryRow = new BudgetSummary(budget);
			summaryRow.setRow(field.getFieldPath());
			budgetSummaryList.add(summaryRow);
		}

		summaryRow = new BudgetSummary();
		summaryRow.category = Schema.SobjectType.Budget__c.fields.Final_Grand_Total__c.label;
		//summaryRow.category = 'Total Costs for Entire Proposed Project Period';
		summaryRow.rowTotals = '$' + getcurrency(budget.Final_Grand_Total__c.setScale(2));
		budgetSummaryList.add(summaryRow);

		return budgetSummaryList;
	}
public static string getcurrency(Decimal num){
     
        /*List<String> args = new String[]{'0.00','number','########,##,##0.00'};
         String sval = String.format(num.format(), args);
        return sval;*/
         if(num!=null){
    String y = String.valueOf(num);
    String z = '.';
    if(y.contains(',')) z = ',';
    y = y.substring(0, y.indexOf(z));
    if(num - Decimal.valueOf(y) == 0)
        return String.valueOf(num.format()) + z + '00';
            else return String.valueOf(num.format());}
        else{
            return '0.00';
        }
    }
}