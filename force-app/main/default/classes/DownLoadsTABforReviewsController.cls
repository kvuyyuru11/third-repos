/*
* Class : DownLoadsTABforReviewsController
* Author : Kalyan Vuyyuru (PCORI)
* Version History : 1.0
* Creation : 1/27/2017
* Last Modified By: Sowmya Goli (Accenture)
* Last Modified Date: 04/14/2017
* Modification Description: Updated Code to ensure the application downloads tab shows applications when they have been finalized as well.
* Description : This class takes care of displaying the list of projects or applications to which the approved reviewer is associated in the reviews which when selected and clicked on the button below will display the Application Review Downloads data based on the record type.
* Last Modified By: Kalyan Vuyyuru (PCORI)
* Last Modified Date: 09/18/2017
* Modification Description: Updated the code to show the application review dasboard data based on review type and record type at the moment
apart from the earlier code checking on last modified date.
*/
public class DownLoadsTABforReviewsController 
{
    public  DownLoadsTABforReviewsController() {}
    
    public String selectedOpt{get;set;}
    
    Id UserId = UserInfo.getUserId();
    User u = [select Id,contactId from User where id=:UserId];
    Contact c = [select Id from Contact where id=:u.contactId];
    
    //Below Soql query gets all the related reviews associated to the logged in user contact along with opportunities.
    List<FC_Reviewer__External_Review__c> reviewlist = [select Panel__c, 
                                                               RecordTypeDevName__c,
                                                               Opportunity_Name__c,
                                                               FC_Reviewer__Contact__c,
                                                               FC_Reviewer__Opportunity__c,
                                                               FC_Reviewer__Opportunity__r.Name,
                                                               FC_Reviewer__Opportunity__r.Application_Number__c 
                                                               from
                                                               FC_Reviewer__External_Review__c 
                                                               where
                                                               FC_Reviewer__Contact__c=:u.ContactId 
                                                               AND 
                                                               Panel__r.Active__c = True
                                                               order by 
                                                               FC_Reviewer__Opportunity__r.Application_Number__c asc];
    
    
    
    //Adding all the related opportunites to select options from the soql query list got above. 
    public List<SelectOption> getopptoptions()
    {
        list<SelectOption> options = new list<SelectOption>();
        Map<string, Id> opportmap= new Map<String, Id>();
        
        for(FC_Reviewer__External_Review__c rvw:reviewlist)
        {
            if(rvw.RecordTypeDevName__c.contains('Online')){
                opportmap.put(rvw.Opportunity_Name__c,rvw.FC_Reviewer__Contact__c);
                
            }
            
        }
        for(FC_Reviewer__External_Review__c rvw:reviewlist)
        {
            
            if(!(opportmap.containsKey(rvw.Opportunity_Name__c) && !rvw.RecordTypeDevName__c.contains('Online')))
                
                options.add(new SelectOption(rvw.FC_Reviewer__Opportunity__r.Name,rvw.FC_Reviewer__Opportunity__r.Application_Number__c+' : '+rvw.FC_Reviewer__Opportunity__r.Name));
            
        }
        return options; 
    }
    
    //This method is fired on click of show application review downloads data button which inturn opens the page and the links applicable based on the record type of the review to which the selected project or application is associated with.
    public pagereference showlinks()
    {
        Id rvwRecordTypeId1 = Schema.SObjectType.FC_Reviewer__External_Review__c.getRecordTypeInfosByName().get(Label.InPersonReview).getRecordTypeId();
        Id rvwRecordTypeId2 = Schema.SObjectType.FC_Reviewer__External_Review__c.getRecordTypeInfosByName().get(Label.InPersonPreview).getRecordTypeId();
        // Id rvwRecordTypeId3 = Schema.SObjectType.FC_Reviewer__External_Review__c.getRecordTypeInfosByName().get(Label.OnlineReviewAD).getRecordTypeId();
        
        list<FC_Reviewer__External_Review__c> revw = new list<FC_Reviewer__External_Review__c>();
        revw= [select ID, RecordtypeID,Application_Download_R2__c,Combined_Online_Critique_Download__c, 
               FC_Reviewer__Opportunity__c,FC_Reviewer__Reviewer_Type__c from FC_Reviewer__External_Review__c where 
               FC_Reviewer__Opportunity__r.Name=:selectedOpt AND FC_Reviewer__Contact__c=:u.ContactId AND Panel__r.Active__c = True];

        if(!revw.isempty() && revw.size()>0){
            if(revw.size()==1 && revw[0].FC_Reviewer__Reviewer_Type__c!='In-Person' && revw[0].RecordtypeID != rvwRecordTypeId1 && revw[0].RecordtypeID != rvwRecordTypeId2 ){
                
                PageReference pageReference3 = new PageReference(Label.LinksDownloadingPageOnlineReviewAD+revw[0].Id);
                pageReference3.setRedirect(true);
                return pageReference3 ;
            }
            
            else if(revw.size()==1 && revw[0].FC_Reviewer__Reviewer_Type__c=='In-Person' && revw[0].RecordtypeID == rvwRecordTypeId2){
                 PageReference pageReference7 = new PageReference(Label.LinksDownloadingPageInPersonPreview+revw[0].Id);
                    pageReference7.setRedirect(true);
                    return pageReference7 ;
            }
            
                      else if(revw.size()==1 && revw[0].FC_Reviewer__Reviewer_Type__c=='In-Person' && revw[0].RecordtypeID == rvwRecordTypeId1){
                 PageReference pageReference8 = new PageReference(Label.LinksDownloadingPageInPersonReview+revw[0].Id);
                    pageReference8.setRedirect(true);
                    return pageReference8 ;
            }
            
         else if(revw.size()>1 && (revw[0].FC_Reviewer__Reviewer_Type__c=='In-Person'||revw[1].FC_Reviewer__Reviewer_Type__c=='In-Person' ) && (revw[0].RecordtypeID == rvwRecordTypeId2 ||revw[1].RecordtypeID == rvwRecordTypeId2 )) {
                if(revw[0].FC_Reviewer__Reviewer_Type__c=='In-Person'){
                    PageReference pageReference2 = new PageReference(Label.LinksDownloadingPageInPersonPreview+revw[0].Id);
                    pageReference2.setRedirect(true);
                    return pageReference2 ;
                }
                if(revw[1].FC_Reviewer__Reviewer_Type__c=='In-Person'){
                    PageReference pageReference2 = new PageReference(Label.LinksDownloadingPageInPersonPreview+revw[1].Id);
                    pageReference2.setRedirect(true);
                    return pageReference2 ;
                }
            }
            
             
             else if(revw.size()>1 && (revw[0].FC_Reviewer__Reviewer_Type__c=='In-Person'||revw[1].FC_Reviewer__Reviewer_Type__c=='In-Person' ) && (revw[0].RecordtypeID == rvwRecordTypeId1 ||revw[1].RecordtypeID == rvwRecordTypeId1 )) {
                if(revw[0].FC_Reviewer__Reviewer_Type__c=='In-Person'){
                    PageReference pageReference1 = new PageReference(Label.LinksDownloadingPageInPersonReview+revw[0].Id);
                    pageReference1.setRedirect(true);
                    return pageReference1 ;
                }
                if(revw[1].FC_Reviewer__Reviewer_Type__c=='In-Person'){
                    PageReference pageReference1 = new PageReference(Label.LinksDownloadingPageInPersonReview+revw[1].Id);
                    pageReference1.setRedirect(true);
                    return pageReference1 ;
                }
            }
        }
        return null;
    }
    
}