@isTest
private class BudgetTriggerHandlerTest
{
    static testMethod void Test() 
    {
    
    RecordType r1=[Select Id from RecordType where name=:'Research Awards' limit 1];
    RecordType r2=[Select Id from RecordType where name=:'Consultant Cost' limit 1];
      Account a = new Account();
        a.Name = 'Acme1';
        insert a;
        
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;  
        Contact con = new Contact(LastName ='testCon',AccountId = a.Id);  
        insert con;  
        
        User user = new User(alias = 'test123', email='test123fvb@noemail.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,   
                             contactId = con.Id,
                             timezonesidkey='America/New_York', username='testerfvb1@noemail.com');
        insert user;
        
        Id profileId1 = [select id from profile where name='PCORI Community Partner'].id;  
        Contact con1 = new Contact(LastName ='testCon1',AccountId = a.Id);  
        insert con1;
        User user1 = new User(alias = 'test1234', email='test1234fvb@noemail.com',
                              emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                              localesidkey='en_US', profileid = profileId1 , country='United States',IsActive = true,   
                              contactId = con1.Id,
                              timezonesidkey='America/New_York', username='testerfvb123@noemail.com');
        insert user1;
        
        Id profileId2 = [select id from profile where name='PCORI Community Partner'].id;  
        Contact con2 = new Contact(LastName ='testCon1',AccountId = a.Id);  
        insert con2;
        User user2 = new User(alias = 'test145', email='test12345fvb@noemail.com',
                              emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                              localesidkey='en_US', profileid = profileId2 , country='United States',IsActive = true,   
                              contactId = con2.Id,
                              timezonesidkey='America/New_York', username='testerfvb12@noemail.com');
        insert user2;
        
        Id profileId3 = [select id from profile where name='PCORI Community Partner'].id;  
        Contact con3 = new Contact(LastName ='testCon2',AccountId = a.Id);  
        insert con3;
        
        User user3 = new User(alias = 'test52', email='test135fvb@noemail.com',
                              emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                              localesidkey='en_US', profileid = profileId3 , country='United States',IsActive = true,   
                              contactId = con3.Id,
                              timezonesidkey='America/New_York', username='testerfvb82@noemail.com');
        insert user3;
        
        
        Cycle__c c = new Cycle__c();
        c.Name = 'testcycle';
        c.COI_Due_Date__c = date.valueof(System.now());
        insert c;
        
        Panel__c p = new Panel__c();
        p.name= 'testpanel';
        p.Cycle__c= c.id ;
        p.MRO__c = user2.id;
        p.Panel_Due_Dtae__c=  Date.newInstance(2019,12,20); 
        insert p;
        
        opportunity o=new opportunity();
        
        o.Awardee_Institution_Organization__c=a.Id;
        o.RecordTypeId=r1.Id;
        o.Full_Project_Title__c='test';
        o.Project_Name_off_layout__c='test';
        //o.Panel__c=p.Id;
        o.CloseDate=Date.today();
        o.Application_Number__c='Ra-1234';
        o.StageName='Executed';
        o.Name='test';
        o.PI_Name_User__c=user.id;
        o.AO_Name_User__c=user1.id;
        o.Project_Start_Date__c=system.Today();
        
        insert o;
        
        opportunity o1=new opportunity();
        
        o1.Awardee_Institution_Organization__c=a.Id;
        o1.RecordTypeId=r1.Id;
        o1.Full_Project_Title__c='test5';
        o1.Project_Name_off_layout__c='test5';
        //o.Panel__c=p.Id;
        o1.CloseDate=Date.today();
        o1.Application_Number__c='Ra-12354';
        o1.StageName='Programmatic Review';
        o1.Name='test';
        o1.PI_Name_User__c=user.id;
        o1.AO_Name_User__c=user1.id;
        o1.Project_Start_Date__c=system.Today();
        
        insert o1;
        
        Budget__c b = new Budget__c();
        b.Associated_App_Project__c= o.id;
        // b.Clone__c = 'Yes';
        b.Bypass_Flow__c=true;
        //b.Supplemental_Funding_Flag__c=true;

        Test.startTest();
              
        insert b;
        
        Key_Personnel_Costs__c budline = new Key_Personnel_Costs__c();
        budline.name = 'Test';
        budline.Description__c = 'Test class';
        budline.Total_1__c= 100;
        budline.Year__c = 1;
        budline.Hourly_Unit_Rate__c = 10;
        budline.Cost_Category__c = 'Supplies';
        budline.Budget__c = b.id;
        budline.Applicable_Rate__c=5;
        budline.Calendar_Months__c=7;
        budline.End_Date__c=system.Today();
        budline.Fringe_Benefits__c=4;
      // budline.Fringe_Rate2__c=5;
        budline.Fringe_Rate__c=4;
        budline.Hourly_Unit_Rate__c=4;
        budline.Inst_Base_Salary__c=5;
        budline.Modified_Direct_Cost__c=7;
        budline.RecordTypeId=r2.id;
        budline.Record_Count_Per_Year__c='Test Re';
        budline.Role_On_Project__c='Contract Manager';
        budline.Roll_on_Project__c='test r';
        
        budline.Salary_Requested__c=56;
        budline.Subcontractor_Name__c='test3';
      // budline.Total__c=3;
        budline.Total_1__c=6;
        budline.Total_Indirect_Costs2__c=7; 
        budline.Total_Indirect_Costs__c=7;
        budline.Total_Prime_Indirect_Costs__c='Total Prime Indirect Costs Year 1 ';
        budline.Variable_Count__c=3;       
        budline.Bypass_Flow__c=true;
      insert budline;
      //  update b;
      
      
      
        b.Clone__c = 'Yes';
        
        

        update b;
                  
      Budget__c b1 = new Budget__c();
        b1.Associated_App_Project__c= o1.id;
        b1.Budget_Status__c='Application Budget';
        b1.Bypass_Flow__c=true; 
        insert b1;
        b1.BudgetNegotiation__c= 'Yes';
        b1.No_Awardee_Edits_Required__c = false;
        
        update b1; 
        
    /*     Budget__c b2 = new Budget__c();
        b2.Associated_App_Project__c= o1.id;
        b2.Budget_Status__c= 'Application Budget';
      
        insert b2; 
      b2.Bypass_Flow__c=true;
      b2.BudgetNegotiation__c='Yes';
      b2.No_Awardee_Edits_Required__c = True;
        b2.Supplemental_Funding_Flag__c = true;
        
        
        update b2;*/

        Test.stopTest();
           
    
    }
    
    
    
      static testMethod void Test1() 
    {
    
    RecordType r1=[Select Id from RecordType where name=:'Research Awards' limit 1];
      Account a = new Account();
        a.Name = 'Acme1';
        insert a;
        
        Id profileId = [select id from profile where name='PCORI Community Partner'].id;  
        Contact con = new Contact(LastName ='testCon',AccountId = a.Id);  
        insert con;  
        
        User user = new User(alias = 'test123', email='test123fvb@noemail.com',
                             emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                             localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,   
                             contactId = con.Id,
                             timezonesidkey='America/New_York', username='testerfvb1@noemail.com');
        insert user;
        
        Id profileId1 = [select id from profile where name='PCORI Community Partner'].id;  
        Contact con1 = new Contact(LastName ='testCon1',AccountId = a.Id);  
        insert con1;
        User user1 = new User(alias = 'test1234', email='test1234fvb@noemail.com',
                              emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                              localesidkey='en_US', profileid = profileId1 , country='United States',IsActive = true,   
                              contactId = con1.Id,
                              timezonesidkey='America/New_York', username='testerfvb123@noemail.com');
        insert user1;
        
        Id profileId2 = [select id from profile where name='PCORI Community Partner'].id;  
        Contact con2 = new Contact(LastName ='testCon1',AccountId = a.Id);  
        insert con2;
        User user2 = new User(alias = 'test145', email='test12345fvb@noemail.com',
                              emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                              localesidkey='en_US', profileid = profileId2 , country='United States',IsActive = true,   
                              contactId = con2.Id,
                              timezonesidkey='America/New_York', username='testerfvb12@noemail.com');
        insert user2;
        
        Id profileId3 = [select id from profile where name='PCORI Community Partner'].id;  
        Contact con3 = new Contact(LastName ='testCon2',AccountId = a.Id);  
        insert con3;
        
        User user3 = new User(alias = 'test52', email='test135fvb@noemail.com',
                              emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
                              localesidkey='en_US', profileid = profileId3 , country='United States',IsActive = true,   
                              contactId = con3.Id,
                              timezonesidkey='America/New_York', username='testerfvb82@noemail.com');
        insert user3;
        
        
            Cycle__c c = new Cycle__c();
            c.Name = 'testcycle';
            c.COI_Due_Date__c = date.valueof(System.now());
            insert c;
            
            Panel__c p = new Panel__c();
            p.name= 'testpanel';
            p.Cycle__c= c.id ;
            p.MRO__c = user2.id;
            p.Panel_Due_Dtae__c=  Date.newInstance(2019,12,20); 
            insert p;
           
            
             opportunity o1=new opportunity();
            
            o1.Awardee_Institution_Organization__c=a.Id;
            o1.RecordTypeId=r1.Id;
            o1.Full_Project_Title__c='test5';
            o1.Project_Name_off_layout__c='test5';
            //o.Panel__c=p.Id;
            o1.CloseDate=Date.today();
            o1.Application_Number__c='Ra-12354';
            o1.StageName='Programmatic Review';
            o1.Name='test';
            o1.PI_Name_User__c=user.id;
            o1.AO_Name_User__c=user1.id;
            o1.Project_Start_Date__c=system.Today();
            
            insert o1;

            Test.startTest();
          
            Budget__c b2 = new Budget__c();
           b2.Associated_App_Project__c= o1.id;
            b2.Budget_Status__c= 'Application Budget';
          b2.Bypass_Flow__c=true;
           insert b2; 
         
         b2.BudgetNegotiation__c='Yes';
          b2.No_Awardee_Edits_Required__c = True;
           b2.Supplemental_Funding_Flag__c = true;
           
           
           update b2;

           Test.stopTest();
           
    
    }
    }