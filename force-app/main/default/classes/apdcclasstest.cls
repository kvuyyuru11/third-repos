@isTest
public class apdcclasstest {    
    public static testmethod void TestMethod1(){   
        list<Account> acnt = TestDataFactory.getAccountTestRecords(1);
        list<contact> cons  = TestDataFactory.getContactTestRecords(1,5);
        Advisory_Panel_Application__c a = new Advisory_Panel_Application__c();
        a.Contact__c = cons[0].Id;
        a.SurveyGizmo_ID__c='test';
        a.Last_Name__c='test';
        a.Personal_Statement2__c='test';
        insert a;
        
        Test.startTest();
        contact c=[select Personal_Statement__c from Contact where id=:a.Contact__c];
        if(c.Personal_Statement__c==null){
            c.Personal_Statement__c=a.Personal_Statement2__c;
            database.update(c);
            Test.stopTest(); 
        }        
    }
}