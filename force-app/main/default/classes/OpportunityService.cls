public class OpportunityService{
//Method to insert the Recruitment record with recordtype "Race and Ethnicity" whenever an Opportunity with recordtype "Research awards" is inserted.
public static Void insertRecruitment(List<Opportunity> Listopp){
List<Recruitment__c> Reclist2=new List<Recruitment__c>();

//Gets the recordtypeId of the Opportunity Recordtype "Research Awards"
Schema.DescribeSObjectResult oppsche = Schema.SObjectType.Opportunity;
Map<string, schema.RecordtypeInfo> sObjectMap = oppsche.getRecordtypeInfosByName();
Id stroppRecordTypeId = sObjectMap.get('Research Awards').getRecordTypeId();


//Gets the recordtypeId of the Recruitment Recordtype "Race and Ethnicity"
Schema.DescribeSObjectResult recsche = Schema.SObjectType.Recruitment__c;
Map<string, schema.RecordtypeInfo> sObjectMap1 = recsche.getRecordtypeInfosByName();
Id strrecRecordTypeId = sObjectMap1.get('Race and Ethnicity').getRecordTypeId();

try{
  Recruitment__c rec = new Recruitment__c();
  for(Opportunity opp:Listopp){
  if(opp.RecordtypeId==stroppRecordTypeId){
  if(Opp.Contract_Number__c!=null){ 
   rec.Name=Opp.Contract_Number__c+' Race/Ethnicity';
   rec.Project__c=Opp.Id;
   rec.RecordTypeId=strrecRecordTypeId;
   Reclist2.add(rec);
   }
   }
 
   }
   if(!Reclist2.isempty()) {
   database.insert(Reclist2);
   }
}
 catch(exception e){
 system.debug('Exception occured while Inserting the recruitment');
 }     
}

//Method to check whether a Recruitment record with recordtype "Race and Ethnicity" exists for an Opportunity with recordtype "Research awards" and insert if no record is present.
public static void updateRecruitment(List<Opportunity> Listopp){

//Gets the recordtypeId of the Opportunity Recordtype "Research Awards"
Schema.DescribeSObjectResult oppsche = Schema.SObjectType.Opportunity;
Map<string, schema.RecordtypeInfo> sObjectMap = oppsche.getRecordtypeInfosByName();
Id stroppRecordTypeId = sObjectMap.get('Research Awards').getRecordTypeId();

//Gets the recordtypeId of the Recruitment Recordtype "Race and Ethnicity"
Schema.DescribeSObjectResult recsche = Schema.SObjectType.Recruitment__c;
Map<string, schema.RecordtypeInfo> sObjectMap1 = recsche.getRecordtypeInfosByName();
Id strrecRecordTypeId = sObjectMap1.get('Race and Ethnicity').getRecordTypeId();


List<Recruitment__c> Reclist2=new List<Recruitment__c>();
  set<id> ids=new set<id>();
  Map<Id,Id> ml = new Map<Id,Id>();
  for(Opportunity opp:Listopp){
      if(opp.recordtypeid==stroppRecordTypeId){
      ids.add(opp.id);
      }
  }
  //Iterate through the List of Recruitments records with recordtype "Race and ethnicity" associated for particular Opportunity
  List<Recruitment__c> reclist = [Select id,recordtypeid,Project__c from Recruitment__c where Project__c =:ids and recordtypeid=:strrecRecordTypeId];  
  for(Recruitment__c r:reclist ){
      ml.put(r.Project__c, r.Id);
  }
  
  try{
  Recruitment__c rec = new Recruitment__c();
  for(Opportunity opp:Listopp){
  
  //Checks whether Recruitment Records of type "Race and Ethnicity" are present or not.
  if(ml.get(opp.Id)==null||ml.get(opp.Id)==''){
      
  //Checks whether Oppoetunity record is of type "Research awards"
  if(opp.RecordtypeId==stroppRecordTypeId){
  if(Opp.Contract_Number__c!=null){ 
  rec.Name=Opp.Contract_Number__c+' Race/Ethnicity';
  rec.Project__c=Opp.Id;
  rec.RecordTypeId=strrecRecordTypeId;
  Reclist2.add(rec);
  }
 }
 }
  }
if(!Reclist2.isempty()){ 
database.insert(Reclist2);
}
}

catch(exception e){
system.debug('Exception has occured while updating the recruitment');
}
      
}

    /**
    * @author        Pradeep Bokkala
    * @date          4/20/2018
    * @description   This method is to display validation rule on Applications on Insert and Update which are assigned with Reviewers who assigned COI on Panel and Application Level. 
    * @jiraItem      SO-2089
    * @param         Trigger.newMap, Trigger.oldMap
    */

public static void validationOnApplicationsForCOI(Map<Id, Opportunity> nMap, Map<Id, Opportunity> pOldMap){
Set<Id> panelIdSet = new Set<Id>();
List<Id> contactIdList = new List<Id>();
Map<Id,List<Id>> reviewerOpportunityMap = new Map<Id,List<Id>>();
List<Id> validationOpportunitiesList = new List<Id>();
Map<Id, List<Id>> pfa_ReviewerCoiIdMap = new Map<Id, List<Id>>();
Map<Id,List<Id>> application_ReviewerCoiIdMap = new Map<Id,List<Id>>();
Map<Id,String> pfa_ReviewerIdNameMap = new Map<Id,String>();
Map<Id,String> application_ReviewerIdNameMap = new Map<Id,String>();
Map<Id,List<Id>> pfa_PanelIdReviewerIds = new Map<Id,List<Id>>();
Map<Id,List <Id>> application_reviewerIdApplicationIdMap = new Map<Id,List<Id>>();

try{
for (Opportunity o : nMap.values()) {
    //Check if the Reviewer fields has changed on Insert or update.
    if (pOldMap.containsKey(o.Id) && (pOldMap.get(o.Id).Reviewer_1__c != o.Reviewer_1__c || 
        pOldMap.get(o.Id).Reviewer_2__c != o.Reviewer_2__c || 
        pOldMap.get(o.Id).Reviewer_3__c != o.Reviewer_3__c || 
        pOldMap.get(o.Id).Reviewer_4__c != o.Reviewer_4__c || 
        pOldMap.get(o.Id).Reviewer_5__c != o.Reviewer_5__c )) {
            if((o.RecordTypeDevName__c == 'RA_Applications' || o.RecordTypeDevName__c == 'RA_Applications_Methods') && o.Panel__c != null){
                panelIdSet.add(o.Panel__c); 
                if(reviewerOpportunityMap.containsKey(o.Id)){
                  reviewerOpportunityMap.get(o.Id).addAll(processreviewerOpportunityMap(o));
                  }else{
                        reviewerOpportunityMap.put(o.Id, processreviewerOpportunityMap(o));
                    }
                validationOpportunitiesList.add(o.Id);
                contactIdList.addAll(processreviewerOpportunityMap(o));    
            }
    }
}
Map<Id, String> contactIdNameMap = new Map<Id, String>();
if(contactIdList != null && !contactIdList.isEmpty()){
    for (Contact c: getContacts(contactIdList)){
        contactIdNameMap.put(c.Id, c.Name);
    }
}

List<Panel_Assignment__c> panelAssignmentsList = new List<Panel_Assignment__c>(getPanelAssignments(panelIdSet));
Map <Id, List<Id>> panelIdReviewerIdsList = new Map <Id, List<Id>>();
for (Panel_Assignment__c pa : panelAssignmentsList ){
    if (panelIdReviewerIdsList.containsKey(pa.Panel__c)) {
        panelIdReviewerIdsList.get(pa.Panel__c).add(pa.Reviewer_Name__c);
    }else{
        panelIdReviewerIdsList.put(pa.Panel__c, new List<Id>{pa.Reviewer_Name__c});
    }
}
List<COI_Expertise__c> coiRecords = new List<COI_Expertise__c>(getPFALevelCOIrecords(panelIdSet));

for(COI_Expertise__c coi : coiRecords){

  if(coi.Recordtype.Name == 'PFA RecordType'){
    if(coi.PFA_Level_COI__c == 'Yes' || coi.PFA_Level_COI__c == 'Unclear' || coi.PFA_Level_COI__c == '' || coi.PFA_Level_COI__c == null){

        if(pfa_ReviewerCoiIdMap.containsKey(coi.Reviewer_Name__c)){
          pfa_ReviewerCoiIdMap.get(coi.Reviewer_Name__c).add(coi.Id);
        }else{
          pfa_ReviewerCoiIdMap.put(coi.Reviewer_Name__c, new List<Id>{coi.Id});
        }
        pfa_ReviewerIdNameMap.put(coi.Reviewer_Name__c, coi.Reviewer_Name__r.Name);

        if(pfa_PanelIdReviewerIds.containsKey(coi.Panel_R2__c)){
          pfa_PanelIdReviewerIds.get(coi.Panel_R2__c).add(coi.Reviewer_Name__c);
        }else{
          pfa_PanelIdReviewerIds.put(coi.Panel_R2__c, new List<Id>{coi.Reviewer_Name__c});
        }
    }    
  }
  else if(coi.Recordtype.Name == 'General Record Type' ){
    system.debug('the panel level coi is ' + pfa_ReviewerCoiIdMap.keySet() + coi.Reviewer_Name__c);
    if(nMap.containsKey(coi.Related_Project__c) && !(pfa_ReviewerCoiIdMap.containsKey(coi.Reviewer_Name__c)) &&(coi.Conflict_of_Interest__c == 'Personal' || 
       coi.Conflict_of_Interest__c == 'Professional' || 
       coi.Conflict_of_Interest__c == 'Institutional' || 
       coi.Conflict_of_Interest__c == 'Financial' || 
       coi.Conflict_of_Interest__c == 'Unclear if COI Exists' || 
       coi.Conflict_of_Interest__c == '' || 
       coi.Conflict_of_Interest__c == null)){
      
      if(application_ReviewerCoiIdMap.containsKey(coi.Reviewer_Name__c)){
          application_ReviewerCoiIdMap.get(coi.Reviewer_Name__c).add(coi.Id);
        }else{
          application_ReviewerCoiIdMap.put(coi.Reviewer_Name__c, new List<Id>{coi.Id});
        }

        application_ReviewerIdNameMap.put(coi.Reviewer_Name__c, coi.Reviewer_Name__r.Name);
        if(application_reviewerIdApplicationIdMap.containsKey(coi.Related_Project__c)){
          application_reviewerIdApplicationIdMap.get(coi.Related_Project__c).add(coi.Reviewer_Name__c);
        }else{
          application_reviewerIdApplicationIdMap.put(coi.Related_Project__c, new List<Id>{coi.Reviewer_Name__c});
        }
        
    } 

  }
  
}



String applicationLevelCoiErrorMessage = '';
String pfaLevelCoiErrorMessage = '';
String nonPanelErrorMessage = '';
String combinedErrorMessage = '';
List<Id> nonPanelReviewers = new List<Id>();


for(Id oppId : validationOpportunitiesList){

applicationLevelCoiErrorMessage = '';
pfaLevelCoiErrorMessage = '';
combinedErrorMessage = '';
nonPanelErrorMessage = '';
nonPanelReviewers.clear();

    if(panelIdReviewerIdsList.containsKey(nMap.get(oppId).Panel__c)) {
        nonPanelReviewers = checkReviewerInPanelAssignment(reviewerOpportunityMap.get(oppId),panelIdReviewerIdsList.get(nMap.get(oppId).Panel__c));
        if(!String.isEmpty(getNonPanelReviewerNames(nonPanelReviewers, contactIdNameMap))) {
            nonPanelErrorMessage = ' ' + getNonPanelReviewerNames(nonPanelReviewers, contactIdNameMap) + ' has not been Assigned to the Panel';
        }
    }
    
  if(application_reviewerIdApplicationIdMap.containsKey(oppId)){
    if(!String.isEmpty(getReviewerNames(application_reviewerIdApplicationIdMap.get(oppId),application_ReviewerIdNameMap, reviewerOpportunityMap.get(oppId))))
   applicationLevelCoiErrorMessage =  ' ' + getReviewerNames(application_reviewerIdApplicationIdMap.get(oppId),application_ReviewerIdNameMap, reviewerOpportunityMap.get(oppId)) + ' has indicated COI for this Application'; 
  }

    
    if(pfa_PanelIdReviewerIds.containsKey(nMap.get(oppId).Panel__c)){
    if(!String.isEmpty(getReviewerNames(pfa_PanelIdReviewerIds.get(nMap.get(oppId).Panel__c),pfa_ReviewerIdNameMap, reviewerOpportunityMap.get(oppId))))
   pfaLevelCoiErrorMessage =  ' ' + getReviewerNames(pfa_PanelIdReviewerIds.get(nMap.get(oppId).Panel__c),pfa_ReviewerIdNameMap, reviewerOpportunityMap.get(oppId)) + ' has indicated COI for this PFA on the Panel'; 
  }


      if(getFinalErrorMessage(applicationLevelCoiErrorMessage, pfaLevelCoiErrorMessage, nonPanelErrorMessage) != null && getFinalErrorMessage(applicationLevelCoiErrorMessage, pfaLevelCoiErrorMessage, nonPanelErrorMessage) != ''){
        nMap.get(oppId).addError(getFinalErrorMessage(applicationLevelCoiErrorMessage, pfaLevelCoiErrorMessage, nonPanelErrorMessage));
      }      
}
}
catch(exception e) {
system.debug('Here is the exception ' +e.getMessage());
}
}

    /**
    * @author        Pradeep Bokkala
    * @date          4/20/2018
    * @description   This method returns the string which contains the combined error message for Application level COI and PFA level COI. 
    * @jiraItem      SO-2089
    * @param         string, string
    * @return        string
    */
    
public static string getFinalErrorMessage(string applicationLevelCoiErrorMessage, string pfaLevelCoiErrorMessage, string nonPanelErrorMessage){
List<String> stringList = new List<String>{nonPanelErrorMessage, applicationLevelCoiErrorMessage, pfaLevelCoiErrorMessage};
String str = '';
for(String s : stringList){
    if(!string.isEmpty(s)){
        str += s + '. ';
    }
}
return str; 
}


    /**
    * @author        Pradeep Bokkala
    * @date          4/20/2018
    * @description   This method returns the string of reviwer names on the application who indicated Application level COI or PFA level COI. 
    * @jiraItem      SO-2089
    * @param         List<Id>, Map<Id, String>, List<Id>
    * @return        string
    */
    
public static string getReviewerNames(List<Id> reviewerList, Map<Id, String> reviewerIdNameMap, List<Id> completeListofReviewers){
system.debug('Here is the reviewerList' + reviewerList);
system.debug('Here is the reviewerIdNameMap' + reviewerIdNameMap);
system.debug('Here is the completeListofReviewers' + completeListofReviewers);

string names = '';
for(Id rid : completeListofReviewers){
  system.debug('Here is the rid '+reviewerList.contains(rid));
if(reviewerList.contains(rid)){
  if(names == ''){
  names = reviewerIdNameMap.get(rid);
  }else{
    names += ', '+reviewerIdNameMap.get(rid);
    }
}

}
return names;
}

    /**
    * @author        Pradeep Bokkala
    * @date          4/20/2018
    * @description   This method returns the List of IDs of reviwer on an Application. 
    * @jiraItem      SO-2089
    * @param         opportunity
    * @return        List<Id>
    */

public static List<Id> processreviewerOpportunityMap(Opportunity o){
List<Id> reviewerIdsList = new List<Id>();
for(integer i=1; i<6; i++){
  if(string.valueOf(o.get('Reviewer_'+i+'__c'))!=null && string.valueOf(o.get('Reviewer_'+i+'__c'))!=''){
      reviewerIdsList.add((Id)o.get('Reviewer_'+i+'__c'));
    }
}
return reviewerIdsList;
}

private static List<COI_Expertise__c> getPFALevelCOIrecords(set<Id> panelIds){
return [select Id, PFA_Level_COI__c, Panel_R2__c, Recordtype.Name, Reviewer_Name__c, Reviewer_Name__r.Name, Related_Project__c, Conflict_of_Interest__c from COI_Expertise__c where Panel_R2__c IN : panelIds ORDER BY Recordtype.Name Desc];
}

private static List<Panel_Assignment__c> getPanelAssignments(set<Id> panelIds){
return [select Id,Reviewer_Name__c,Panel__c from Panel_Assignment__c where Panel__c IN : panelIds];
}

private static List<Contact> getContacts(List<Id> contactIds){
return [select Id,Name from Contact where ID IN : contactIds];
}

public static List<Id> checkReviewerInPanelAssignment (List<Id> reviewersOnOpportunity, List<Id> reviewersOnPanel) {
    List<Id> reviewersNotonPanel = new List<Id>();
    system.debug('that girl'+reviewersOnOpportunity);
    system.debug('that boy'+reviewersOnPanel);
    for (Id rid : reviewersOnOpportunity) {
        if(!reviewersOnPanel.contains(rid)) {
            reviewersNotonPanel.add(rid);
        }
    }
    return reviewersNotonPanel;
}

public static string getNonPanelReviewerNames(List<Id> reviewersNotonPanel, Map<Id, String> contactIdNameMap){

string nonPanelReviewerNames = '';
for(Id rid : reviewersNotonPanel){
if(contactIdNameMap.containsKey(rid)){
  if(nonPanelReviewerNames == ''){
  nonPanelReviewerNames = contactIdNameMap.get(rid);
  }else{
    nonPanelReviewerNames  += ', ' + contactIdNameMap.get(rid);
    }
}

}
return nonPanelReviewerNames;
}
  
 /*  public static void insertCnt(list<Opportunity> opt1)
}
      
{
 list<string> opemaillist= new list<string>();
  list<contact> contactlisttoinsert= new list<contact>();
  list<Contact_Roles__c> contactroleslisttoinsert = new list<Contact_Roles__c>();
  
   
  Id OpntRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('FoundationConnect - Portal').getRecordTypeId();
  for(opportunity o:opt1){
      opemaillist.add(o.Project_Lead_Email_Address__c);
  }
  
  list<contact> ct= [select id from Contact where Email IN :opemaillist ORDER BY CreatedDate ASC]; 
  for(opportunity o:opt1){
      if(o.RecordTypeId == OpntRecordTypeId){
          
          if(ct.size()==0){
              if(o.Project_Lead_Last_Name__c!=null && o.Project_Lead_Email_Address__c!=null){
              Contact c= new Contact();
            c.FirstName=o.Project_Lead_First_Nam__c;
            c.LastName=o.Project_Lead_Last_Name__c;
            c.AccountId=o.AccountId;
            c.Email=o.Project_Lead_Email_Address__c;
              contactlisttoinsert.add(c);
          }
          }
           if(ct.size()>0) {
      for(Contact ct1:ct){
          Contact_Roles__c crs= new Contact_Roles__c();
             crs.Engagement_Award__c=o.Id;
             crs.Contact__c=ct1.Id;
             crs.Role__c='Project Lead';
             crs.PCORI_Department__c='Engagement';
             crs.PCORI_Program_Area__c='Engagement Awards';
          contactroleslisttoinsert.add(crs);
      }
      }
      }
      
  }
    database.insert(contactlisttoinsert);
          database.insert(contactroleslisttoinsert);
}*/

}