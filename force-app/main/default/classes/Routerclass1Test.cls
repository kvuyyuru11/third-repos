@isTest
public class Routerclass1Test {
    
    static testMethod void testRouter1Controller() {        
        list<User> u2 = TestDataFactory.getpartnerUserTestRecords(1,1);       
        System.runAs(u2[0]) {            
            Merit_Reviewer_Application__c mr1 = new Merit_Reviewer_Application__c();
            //insert mr1;            
            ApexPages.StandardController cntrll = new ApexPages.StandardController(mr1);            
            RouterClass1 RC1 = new RouterClass1(cntrll);
            RC1.router1();
        }        
    }
    
    static testMethod void testRouter1Controller2() {
        list<User> u2 = TestDataFactory.getpartnerUserTestRecords(1,1);        
        System.runAs(u2[0]) {            
            Merit_Reviewer_Application__c   mr1 = new Merit_Reviewer_Application__c();
            // insert mr1;            
            ApexPages.StandardController cntrll = new ApexPages.StandardController(mr1);            
            RouterClass1 RC1 = new RouterClass1(cntrll);
            RC1.router1();
        }
    }
}