/**********
This is the controller for AppendEmailsText VF page which calls the SearchAndReplace Batch class to append text
to all the email fields in the selected object.

HelperClass : SearchAndReplace (Batch Class)
Test Class  : RevertEmailsController_Test

***********/

public class AppendEmailsTextController
{
 
    public String sendappendtext { get; set; }
    public String status {get;set;}
    public Id Batchjobid;
    public Boolean keepPolling { get; set; }
    public Boolean hidebutton { get; set; }
    public String sendObjectName { get; set; }
    public String batchClassMessage {get;set;}
    Public Boolean showMessage {get;set;}
    //public Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
    public Boolean show{get;set;}
    public String selectedObject {get; set;}
    public Boolean showstatus{get;set;}
    public List<String> DropdownObjects = new List<String>();
    public List<AppendEmails__c> CustomValues = new List<AppendEmails__c>();

    Public AppendEmailsTextController()
    {   keepPolling = false;
        show=false;
        hidebutton=true;
        sendObjectName= 'contact';
        showMessage=false;
         showstatus = false;   
    }

    public List<SelectOption> getObjectNames() 
    {
        List<SelectOption> objNames = new List<SelectOption>();
        CustomValues = AppendEmails__c.getall().values();
        CustomValues.sort();
        //List<String> entities = new List<String>(schemaMap.keySet());
        //entities.sort();
        //for(String name : entities)
        //{
          //  objNames.add(new SelectOption(name,name));
        //}
        for(AppendEmails__c cv:CustomValues){
           objNames.add(new SelectOption(cv.name,cv.name));
        }
        return objNames;
     }
    
    public PageReference executeBatch() {
    
    SearchAndReplace sp=new SearchAndReplace(sendObjectName,sendappendtext);
    Batchjobid=database.executebatch(sp,200);
    /*
    AsyncApexJob job= [SELECT Status FROM AsyncApexJob WHERE ID =: Batchjobid];           
    status = job.Status;
    
     if(job.Status == 'Queued' || job.Status == 'Holding' || job.Status == 'Preparing') {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'We are in the '+job.Status.toLowerCase()+' status...'));
        } else if(job.Status != 'Completed' && job.Status != 'Aborted') {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'We are in '+job.Status.toLowerCase()+' status: '+Math.floor(100.0*job.JobItemsProcessed/job.TotalJobItems)+'%...'));
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The job is now '+job.Status.toLowerCase()+'.'));
            keepPolling = false;
        }
     */    
    hidebutton=false;
    showstatus = true;
        return null;
    }


public String getMessage(){
            /*if(batchProcessed){
                Double itemsProcessed;
                Double totalItems ;
                for(AsyncApexJob a : [select TotalJobItems, Status, NumberOfErrors, MethodName, JobType, JobItemsProcessed, Id, 
CreatedDate, CreatedById, CompletedDate, ApexClassId, ApexClass.Name From AsyncApexJob where ApexClass.Name = 'SearchAndReplace' order by CreatedDate desc limit 1]){
                    itemsProcessed = a.JobItemsProcessed;
                    totalItems = a.TotalJobItems;
                }
                //Determine the percent complete based on the number of batches complete and set message
                if(totalItems == 0){
                    //A little check here as we don't want to divide by 0.
                    return 'Batch Job 0% Complete';
                }else{
                    return 'Batch Job ' + String.valueof(((itemsProcessed  / totalItems) * 100.0).intValue())+'% Complete.';
                }

            } else {
                return 'Batch Job not yet executed !!';
            }*/
        AsyncApexJob job= [SELECT Status FROM AsyncApexJob where ApexClass.Name = 'SearchAndReplace' order by CreatedDate desc limit 1];           
    status = job.Status;
    String str='';
     if(job.Status == 'Queued' || job.Status == 'Holding' || job.Status == 'Preparing') {
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'We are in the '+job.Status.toLowerCase()+' status...'));
        str = job.Status.toLowerCase();
        return str;
        } else if(job.Status != 'Completed' && job.Status != 'Aborted') {
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'We are in '+job.Status.toLowerCase()+' status: '+Math.floor(100.0*job.JobItemsProcessed/job.TotalJobItems)+'%...'));
       str = job.Status.toLowerCase();
        return str;
       
        } else {
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The job is now '+job.Status.toLowerCase()+'.'));
            keepPolling = false;
            str = job.Status.toLowerCase();
        return str;
        }
     }    
}