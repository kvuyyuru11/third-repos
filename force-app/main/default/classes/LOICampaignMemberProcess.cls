/*-------------------------------------------------------------------------------------------------------------------------------------------------------------

Code Written at by - Pradeep Bokkala
Written as a Fix for Item SO-607
Test Class - LOICampaignMemberProcess_Test
Parent Component - Lead_MasterTrigger
Used in senario(s) - After Update on Leads which are converted (Not fired during Lead Conversion Process)
------------------------------------------------------------------------------------------------------------------------------------------------------------*/

public class LOICampaignMemberProcess{
    public static void checkCampaignMember(List<Lead> LeadList){
        
        //Instantiate different Maps and Lists to use them in the class below
        Map<Id,Id> CampaignMemberLOIMap = new Map<Id,Id>();
        Map<Id,Id> LOICampaignMap = new Map<Id,Id>();
        Map<Id,CampaignMember> CampaignMemberMap = new Map<Id,CampaignMember>();  
        List<CampaignMember> deleteList = new List<CampaignMember>();
        List<CampaignMember> InsertList = new List<CampaignMember>();
        Map<Id,CampaignMember> correctcampaignmemberMap = new Map<Id,CampaignMember>();
        List<Id> LeadListIds=new List<Id>();
        
        //Iterate through the List of Leads and add them to the Maps
        for(Lead L:LeadList){
            LeadListIds.add(L.Id);
            LOICampaignMap.put(l.id,l.PFA__c);
        }
        
        //Query the CampaignMembers which are associated to the Leads which are being Processed
        List<CampaignMember> CampaignMemberList = [Select Id,LeadId,CampaignId from CampaignMember where LeadId IN : LeadListIds];
        
        //Iterate through the List of CampaignmemberList and put them in Maps
        for(CampaignMember cm : CampaignMemberList){
            CampaignMemberMap.put(cm.Id,cm);
            CampaignMemberLOIMap.put(cm.Id,cm.LeadId);
        }
        /*for(Lead l:LeadList){
           LOICampaignMap.put(l.id,l.PFA__c); 
        }*/
        
        /*Iterating the Campaignmembers and Verifying the PFA__c field associated to the LeadId for the
        campaignmember is same as the CampaignID associated to the CampaignMember. If the CampaignId's doesn't match,
        add them to the deleteList to delete the records else add them to a Map to further use it. Adding them to 
        a map and further checking the campaignmembers avoids Duplicate campainMembers errors in the system.
        */
        for(Id cmId : CampaignMemberMap.keySet()){
            if(LOICampaignMap.get(CampaignMemberLOIMap.get(cmId))!=CampaignMemberMap.get(cmId).CampaignId){
                deleteList.add(CampaignMemberMap.get(cmId));
            }else{
                correctcampaignmemberMap.put(CampaignMemberLOIMap.get(cmId),CampaignMemberMap.get(cmId));
            }
        }
        
        //Deleting the unnecessary CampaignMembers
        if(deleteList.size()>0 && !deleteList.isEmpty()){
            database.delete(deleteList);
        }
        
        //Iterating through the List of Leads and verifying whether they are associated to the correct campaign via campaignmembers
        for(Id LeId : LeadListIds){
            
            /*campaignmember cmm = new campaignmember();
              cmm.CampaignId = LOICampaignMap.get(LeId);
              cmm.LeadId = LeId;
              cmm.Status = 'Sent';
              insertList.add(cmm);*/
            
            //If the campaignMember for a Lead doesnt Exist, then create a campaignMember
            if(!correctcampaignmemberMap.containsKey(LeId)){
                campaignmember cmm = new campaignmember();
                cmm.CampaignId = LOICampaignMap.get(LeId);
                cmm.LeadId = LeId;
                cmm.Status = 'Sent';
                insertList.add(cmm);
            }
        }
        
        //Insert the CampaignMember List
        if(insertList.size()>0 && !insertList.isEmpty()){
            database.insert(insertList);
        }
    }
}