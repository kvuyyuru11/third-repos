@isTest

public class COITriggerTest {
   
 
     static  testMethod  void TestCOI(){
                 list<panel__c> pnl=TestDataFactory.getpanelwithoppandpanelassgnandcoiTestRecords(1,20,10,1,1);
  list<COI_Expertise__c> coilist=[select id,Active__c,Conflict_of_Interest__c from COI_Expertise__c where Panel_R2__c=:pnl[0].Id];
system.assertEquals(coilist.size(), 11);
         Panel_Assignment__c pa=[select id,active__c,Reviewer_Name__c from Panel_Assignment__c where panel__c=:pnl[0].Id];
         pa.Active__c=false;
         update pa;
         delete pa;


    }
    
    
     
     static  testMethod  void TestCOI2(){
                 list<panel__c> pnl=TestDataFactory.getpanelwithoppandpanelassgnandcoiTestRecords(1,20,10,1,1);
  list<COI_Expertise__c> coilist=[select id,Active__c,Conflict_of_Interest__c from COI_Expertise__c where Panel_R2__c=:pnl[0].Id];
system.assertEquals(coilist.size(), 11);
         try{
         Panel_Assignment__c pa=[select id,active__c,Reviewer_Name__c from Panel_Assignment__c where panel__c=:pnl[0].Id];
         Panel_Assignment__c pa1=new Panel_Assignment__c();
         pa1.Panel__c=pnl[0].Id;
         pa1.Reviewer_Name__c=pa.Reviewer_Name__c;
         pa1.Panel_Role__c='Reviewer';
         insert pa1;

}catch (dmlexception dmx){}

    }
    
     
     static  testMethod  void TestCOI3(){
                 list<panel__c> pnl=TestDataFactory.getpanelwithoppandpanelassgnandcoiTestRecords(1,20,10,1,1);
  list<COI_Expertise__c> coilist=[select id,Active__c,Conflict_of_Interest__c from COI_Expertise__c where Panel_R2__c=:pnl[0].Id];
system.assertEquals(coilist.size(), 11);
         
         Panel_Assignment__c pa=[select id,active__c,Reviewer_Name__c from Panel_Assignment__c where panel__c=:pnl[0].Id];
         pa.Active__c=false;
         update pa;
         try{
         Panel_Assignment__c pa2=new Panel_Assignment__c();
         pa2.Panel__c=pnl[0].Id;
         pa2.Reviewer_Name__c=pa.Reviewer_Name__c;
         pa2.Panel_Role__c='Reviewer';
         insert pa2;
         
        
}catch (dmlexception dmx){}

    }
}