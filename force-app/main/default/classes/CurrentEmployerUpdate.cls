global without sharing class CurrentEmployerUpdate {
    //instance fields
    public String employerchange { get; set; }
    public Boolean emplist { get; set; }
    public String title { get; set; }
    public String curposstyle { get; set; }
    public Boolean curposdisable { get; set; }
    public String Organization { get; set; }
    public String searchTerm { get; set; }
    public String searchtermstyle { get; set; }
    public Boolean searchdisable { get; set; }
    public String empzipstyle { get; set; }
    public String empstatetextstyle { get; set; }
    public String empcitystyle { get; set; }
    public String empstreetstyle { get; set; }
    public String empcurposstyle { get; set; }
    public String empnamestyle { get; set; }
    public String empcountry { get; set; }
    public String empzip { get; set; }
    public String empzipcode { get; set; }
    public String empcity { get; set; }
    public String empstreet { get; set; }
    public String empcurrentpos { get; set; }
    public String empname { get; set; }
    public String empstatelist {get; set; }
    public String empstatetext { get; set;}
    public String dept {get; set;}
    public String empdept {get; set;}
    public String mailingaddress1 { get; set; }
    public Boolean empstatelistpopup { get; set; }
    public Boolean empstatetextpopup { get; set; }
    
    //contructor
    public CurrentEmployerUpdate(){
        empstatelistpopup=true;  
        employerchange='Employer found';
        emplist=false;
        curposdisable = false;
        searchdisable = false;
    }
    String[] countryList= new String[]{'USA',
        'Afghanistan','Aland Islands','Albania','Algeria','American Samoa','Angola','Anguilla','Antarctica','Antigua and Barbuda','Argentina','Armenia','Aruba',
        'Australia','Austria','Azerbaijan','Bahamas','Bahrain','Bangladesh','Barbados','Belarus','Belgium','Belize','Benin','Bermuda','Bhutan','Bolivia, Plurinational State of','Bonaire, Sint Eustatius and Saba',
        'Bosnia and Herzegovina','Botswana','Bouvet Island','Brazil','British Indian Ocean Territory','Brunei Darussalam','Bulgaria','Burkina Faso','Burundi',
        'Cambodia','Cameroon','Canada','Cape Verde','Cayman Islands','Central African Republic','Chad','Chile','China','Chinese Taipei','Christmas Island','Cocos (Keeling) Islands',
        'Colombia','Comoros','Congo','Congo, the Democratic Republic of the','Cook Islands','Costa Rica','Cote d \'Ivoire','Croatia','Cuba','Curaao','Cyprus',
        'Czech Republic','Denmark','Djibouti','Dominica','Dominican Republic','Ecuador','Egypt','El Salvador','Equatorial Guinea','Eritrea','Estonia','Ethiopia',
        'Falkland Islands (Malvinas)','Faroe Islands','Fiji','Finland','France','French Guiana','French Polynesia','French Southern Territories','Gabon',
        'Gambia','Georgia','Germany','Ghana','Gibraltar','Greece','Greenland','Grenada','Guadeloupe','Guatemala','Guernsey','Guinea','Guinea-Bissau','Guyana',
        'Haiti','Heard Island and McDonald Islands','Holy See (Vatican City State)','Honduras','Hungary','Iceland','India','Indonesia','Iran, Islamic Republic of','Iraq',
        'Ireland','Isle of Man','Israel','Italy','Jamaica','Japan','Jersey','Jordan','Kazakhstan','Kenya','Kiribati','Korea, Democratic People\'s Republic of','Korea, Republic of',
        'Kuwait','Kyrgyzstan','Lao People\'s Democratic Republic','Latvia','Lebanon','Lesotho','Liberia','Libyan Arab Jamahiriya','Liechtenstein','Lithuania','Luxembourg',
        'Macao','Macedonia, the former Yugoslav Republic of','Madagascar','Malawi','Malaysia','Maldives','Mali','Malta','Martinique','Mauritania','Mauritius',
        'Mayotte','Mexico','Moldova, Republic of','Monaco','Mongolia','Montenegro','Montserrat','Morocco','Mozambique','Myanmar','Namibia','Nauru','Nepal',
        'Netherlands','New Caledonia','New Zealand','Nicaragua','Niger','Nigeria','Niue','Norfolk Island','Norway','Oman','Pakistan','Palestinian Territory, Occupied',
        'Panama','Papua New Guinea','Paraguay','Peru','Philippines','Pitcairn','Poland','Portugal','Qatar','Reunion','Romania','Russian Federation','Rwanda',
        'Saint BarthŽlemy','Saint Helena, Ascension and Tristan da Cunha','Saint Kitts and Nevis','Saint Lucia','Saint Martin (French part)','Saint Pierre and Miquelon','Saint Vincent and the Grenadines',
        'Samoa','San Marino','Sao Tome and Principe','Saudi Arabia','Senegal','Serbia','Seychelles','Sierra Leone','Singapore','Sint Maarten (Dutch part)','Slovakia',
        'Slovenia','Solomon Islands','Somalia','South Africa','South Georgia and the South Sandwich Islands','South Sudan','Spain','Sri Lanka',
        'Sudan','Suriname','Svalbard and Jan Mayen','Swaziland','Sweden','Switzerland','Syrian Arab Republic','Tajikistan','Tanzania, United Republic of','Thailand','Timor-Leste','Togo','Tokelau','Tonga','Trinidad and Tobago',
        'Tunisia','Turkey','Turkmenistan','Turks and Caicos Islands','Tuvalu','Uganda','Ukraine','United Arab Emirates','United Kingdom','Uruguay',
        'Uzbekistan','Vanuatu','Venezuela, Bolivarian Republic of','Viet Nam','Virgin Islands, British','Wallis and Futuna','Western Sahara','Yemen','Zambia','Zimbabwe'
        };
            //list of states for USA
            String[] usaStatesList= new String[]{'Alabama',
                'Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District of Columbia','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada',
                'New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','West Virginia','Wisconsin','Wyoming'};
                    
                    @RemoteAction
                    global static List<Account> searchMovie(String searchTerm) {
                         RecordType RecType = [Select Id From RecordType  Where SobjectType = 'Account' and DeveloperName = 'Standard_Salesforce_Account'];
    					Id devRecordTypeId=RecType.ID;
                        List<Account> org = Database.query('Select Id, Name from Account where name like \'%' + String.escapeSingleQuotes(searchTerm) + '%\' AND  RecordTypeID =:devRecordTypeId');
                        return org;
                    }  
    
    public PageReference cancel() {
        PageReference pgRef = new PageReference(label.Open_Contact); 
        return pgRef ;
    }
    
    
    public PageReference savecurrent() {
        Integer x =0;
        Id MyId=userinfo.getUserId();
        User u=[select contactId from User where id=:MyId];
        Contact c=[select Id,AccountId,Reviewer_Role__c,Community__c, Current_Position_or_Title__c,Title,Department from Contact where id=:u.ContactId];
        
        if(employerchange=='Employer found') 
        {
            //This line editted 2/27/2017 to address Security Source Scanner issue by Elizabeth Bryant.
            //Editted from [select name from account] to [select name from account limit 50000]
            List<Account> a = [select name from account limit 50000];
            Integer flag,flag1;
            
            for(Account a1: a)
            {
                if(string.valueof(a1).contains(searchterm))
                {
                    flag=1;
                }
                else{
                    // flag1=1;
                }
            }
            //custom validation for Employer search and employer current position
            
            if(searchTerm==null || searchterm==''||flag!=1){
                searchtermstyle= Label.ErrormsgStyling;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));
                x=1;
            }
            if(title==null || title==''){
                curposstyle=Label.ErrormsgStyling;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));
                x=1;
            }
        }
        //Added by Vijay
        else if (employerchange=='Unemployed')
        {            
            //c.AccountId='';     
            c.Current_Employer__c='';
            c.Department='';
            c.Current_Position_or_Title__c='';
        }
        
        else{
            //check for employer lookup   
        }  
        
        if(x==1){
            return null;
        }        
        //the below code is to get the contact created on the self registration page and update it with the information entered on AutoComplete page     
        
              
        
        if(searchterm!=null && employerchange=='Employer found')
        {
            account act=[select Id,ispartner,Name from Account where name=:searchTerm]; 
            if(act.isPartner==False){
            system.debug('this is inside the condition'+ act.isPartner);
            act.isPartner=True;
            database.update(act);
            }     
            c.AccountId=act.ID;
            c.Current_Position_or_Title__c=title;
            c.Current_Employer__c=act.name;
            c.Department=dept;
            update c;
        }
        if(employerchange=='Unemployed')
        {
            if(c.Reviewer_Role__c!=''||c.Reviewer_Role__c!=null)
            {
                if(c.Reviewer_Role__c=='Scientist'){
                    account act=[select Id,Name from Account where name=:Label.Scientist_Unassociated_Individuals_Account];     
                    c.AccountId=act.ID;     
                    c.Current_Employer__c='';
                    c.Department='';
                    c.Current_Position_or_Title__c='';
                }
                if(c.Reviewer_Role__c=='Patient'){
                    account act=[select Id,Name from Account where name=:Label.Patient_Unassociated_Individuals_Account];     
                    c.AccountId=act.ID;     
                    c.Current_Employer__c='';
                    c.Department='';
                    c.Current_Position_or_Title__c='';
                }
                if(c.Reviewer_Role__c=='Stakeholder'){
                    account act=[select Id,Name from Account where name=:Label.Stakeholder_Unassociated_Individuals_Account];     
                    c.AccountId=act.ID;     
                    c.Current_Employer__c='';
                    c.Department='';
                    c.Current_Position_or_Title__c='';
                }
            }
            else if(c.community__c!=null||c.community__c!='')
            {
                if(c.Community__c=='Scientist'){
                    account act=[select Id,Name from Account where name=:Label.Scientist_Unassociated_Individuals_Account];     
                    c.AccountId=act.ID;     
                    c.Current_Employer__c='';
                    c.Department='';
                    c.Current_Position_or_Title__c='';
                }
                if(c.Community__c=='Patient'){
                    account act=[select Id,Name from Account where name=:Label.Patient_Unassociated_Individuals_Account];     
                    c.AccountId=act.ID;     
                    c.Current_Employer__c='';
                    c.Department='';
                    c.Current_Position_or_Title__c='';
                }
                if(c.Community__c=='Other Stakeholder'){
                    account act=[select Id,Name from Account where name=:Label.Stakeholder_Unassociated_Individuals_Account];     
                    c.AccountId=act.ID;     
                    c.Current_Employer__c='';
                    c.Department='';
                    c.Current_Position_or_Title__c='';
                }
            }            
            
            if((c.community__c==null||c.community__c=='')&&(c.Reviewer_Role__c==''||c.Reviewer_Role__c==null)){
                account act=[select Id,Name from Account where name=:Label.Patient_Unassociated_Individuals_Account limit 1];     
                c.AccountId=act.ID;     
                c.Current_Employer__c='';
                c.Department='';
                c.Current_Position_or_Title__c='';
            }
            update c;
        }
        
        
        PageReference pgRef1 = new PageReference(label.Open_Contact); 
        return pgRef1 ;
    }
    
    public PageReference saveemp() {
        
        Integer x,flag,flag1;
        empzipstyle ='';
        empstatetextstyle='';
        empcitystyle ='';
        empstreetstyle='';
        empcurposstyle='';
        empnamestyle='';
        if(empname==null || empname==''){
            empnamestyle =Label.ErrormsgStyling;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));
            x=1;
        }
        if(empcurrentpos==''|| empcurrentpos==null){
            empcurposstyle = Label.ErrormsgStyling;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));
            x=1;
        }
        if(empstreet== '' || empstreet==null){
            empstreetstyle = Label.ErrormsgStyling;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));
            x=1;} 
        if(empcity==null|| empcity==''){
            empcitystyle = Label.ErrormsgStyling;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));
            x=1;}
        if(CheckValidZip(empzip)==false || empzip==''){
            empzipstyle = Label.ErrormsgStyling;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));
            x=1;}
        if(empcountry!='USA')
        {
            if(empstatetext==''||empstatetext==null)
            {    
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please address required fields in red.'));
                empstatetextstyle=Label.ErrormsgStyling;
                x=1;
            }
        }
        
        //This line editted 2/27/2017 to address Security Source Scanner issue by Elizabeth Bryant.
        //Editted from [select name from account] to [select name from account limit 50000]
        List<Account> a = [select name from account limit 50000];
        for(Account a1: a)
        {
            if(string.valueof(a1.Name).equals(empname)){
                flag=1;
            }
            else{
                flag1=1;}
        }
        
        if(flag==1){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Employer exists,please lookup for your employer'));
            x=1;    
        }
        if(x==1)
        {
            return null;
        }
        else   { 
            Id myId2=userinfo.getUserId();
            user u1=[select contactid from User where id=:myId2];
            Contact cnt1=[select accountid,Current_Employer__c,Current_Position_or_Title__c,Title,Department from Contact where id=:u1.ContactId for update];
            account acc = new account();
            acc.name = empname;
            acc.Website = Label.Account_Website;
            acc.Community__c=Label.Account_Community;
            acc.BillingStreet=empstreet;
            acc.BillingCity=empcity;
            if(empcountry=='USA')
                acc.BillingState=empstatelist;
            else
                acc.BillingState=empstatetext;
            acc.BillingPostalCode=empzip;
            acc.BillingCountry=empcountry;
            acc.ownerId=Label.OwnerId_Account;
            acc.New_Account_created_by_Community__c=true;
            database.insert(acc);
            cnt1.AccountId=acc.id;
            cnt1.Title=empcurrentpos;
            cnt1.Current_Position_or_Title__c=empcurrentpos;
            cnt1.Current_Employer__c=acc.name;
            cnt1.Title=empcurrentpos;
            cnt1.Department=empdept;
            database.update(cnt1);     
            return new PageReference('/apex/AccountUpdateSuccess');
        }
    }
    public PageReference showstate() {
        if(empcountry=='USA'){
            empstatelistpopup=true;
            empstatetextpopup=false;
        }
        else{
            empstatelistpopup=false;
            empstatetextpopup=true;
        }
        return null;
    }
    
    //To validate the zip code field to accept numbers only
    public static Boolean CheckValidZip(String sZip) {
        return Pattern.matches('\\d*',sZip);
    }
    public List<SelectOption> getItemsCurrentemployer() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Employer found','Employer found'));
        options.add(new SelectOption('Employer not found','Employer not found'));
        options.add(new SelectOption('Unemployed','Unemployed'));           
        return options;
    }
    public List<SelectOption> getItemsCountry(){
        List<SelectOption> options = new List<SelectOption>();
        
        for(String c:countryList){
            options.add(new SelectOption(c,c));
        }
        return options;
        
    }
    public List<SelectOption> getItemsState(){
        List<SelectOption> options = new List<SelectOption>();
        for(String c:usaStatesList){
            options.add(new SelectOption(c,c));
        }
        return options;
    }
}