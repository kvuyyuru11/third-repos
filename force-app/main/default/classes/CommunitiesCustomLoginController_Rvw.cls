/*
08/09/2016 Matt Hoffman - This controller was cloned from the existing PCORI portal login controller (CommunitiesCustomLoginController_Rsch). 
Application Inventory: AI-1059
Client Requirement ID: RAr2_C4_022
*/

global class CommunitiesCustomLoginController_Rvw {
    
    global String username {get; set;}
    global String password {get; set;}
    global CommunitiesCustomLoginController_Rvw () {}
    global PageReference login() {
        return Site.login(username, password, null); 
    } 

}