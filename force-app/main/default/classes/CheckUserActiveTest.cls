@isTest
private class CheckUserActiveTest{
static testmethod void  UserActiveTest(){
User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
Id profileId = [select id from profile where name='PCORI Community Partner'].id;
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();
 Account ac = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId) ;
 insert ac; 
  Contact con = new Contact(LastName ='testCon',AccountId = ac.Id);  
        insert con;  
      User user = new User(alias = 'test123', email='test123fvb@noemail.com',
      emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
      localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
      ContactId = con.Id,
      timezonesidkey='America/New_York', username='testerfvb@noemail.com');
     insert user;
     System.runAs(sysAdminUser ) {
     user.IsActive = false;
     update user;
     }
   }
    static testmethod void  UserActiveTest1(){
User sysAdminUser = [select id from User where profile.name='System Administrator' and userroleid != null and IsActive = true limit 1]  ;
Id profileId = [select id from profile where name='PCORI Community Partner'].id;
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Standard Salesforce Account').getRecordTypeId();
 Account ac = new Account(name ='Test Account',ownerId = sysAdminUser.Id,recordtypeid=recordTypeId) ;
 insert ac; 
  Contact con = new Contact(LastName ='testCon',AccountId = ac.Id);  
        insert con;  
      User user = new User(alias = 'test123', email='test123zxd@noemail.com',
      emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',    
      localesidkey='en_US', profileid = profileId , country='United States',IsActive = true,     
      ContactId = con.Id,
      timezonesidkey='America/New_York', username='testerzxd@noemail.com');
     insert user;
     System.runAs(sysAdminUser ) {
     user.IsActive = true;
     update user;
     }
   }
 }