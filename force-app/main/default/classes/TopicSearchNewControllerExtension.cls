/**
    * TopicSearchControllerExtension - <description>
    * @author: Rainmaker
    * @version: 1.0
*/
public with sharing class TopicSearchNewControllerExtension {
  

public List <Topic__c> topicList {get;set;}
    
public  String populationSelected{get;set;}
public  String stakeholderSelected{get;set;}
    

    public String soql {get;set;}
    public String soqlBase {get;set;}  
    public Opportunity ownerList {get; set;}
    
    public Topic__c topicVals {get;set;}    
    
    public Opportunity opprVals {get;set;}   
    
    public boolean showColumn {get; set;}

     // Owner Sort Fields 
    public String sortPointer {get;set;}     
    public String sortDir {get;set;}
    public String sortField {get; set;}
    
    public String divider {get; set;}    
    
    public String searchCriteria {get; set;}  
    
    public Integer resultCount {get; set;} 
    
    public TopicSearchNewControllerExtension () {
    
          this.showColumn = true;
          this.topicVals = new Topic__c();
          this.topicList = new List <Topic__c>();
          divider = '____________________________________________________________________________';
         
          sortPointer = 'Owner';
          sortDir = 'desc';
          
          populationSelected=null;
          stakeholderSelected=null;
          
          this.soqlBase = 'SELECT Research_Question__c, name, Stakeholder_Group__c FROM Topic__c WHERE Name <> NULL and Do_Not_Display_on_Website__c = false ';          

            
    }
    
    
    public void getTopicSearchResults() {
    
        String topicIdSelected = topicVals.Reason__c;   
        
        String stakeHolderGroupSelected = null;
        
        if(stakeholderSelected != '-None-')       
        	stakeHolderGroupSelected =  stakeholderSelected;
        	
        	
  		String populationListSelected = null;
  		
    	
        	
        String keywordsSelected = topicVals.Keywords__c;  
        
        
        String disease1Selected = topicVals.Disease_Condition_1__c;
        String disease1SubtopicSelected = topicVals.Disease_Condition_Subtopic_1__c;
        

        String healthSelected = topicVals.Health_Topic__c;
        String statusSelected = topicVals.External_Status__c;
        
        
//         if(populationSelected != '-None-')       
//        	populationListSelected = populationSelected;
        populationListSelected = topicVals.Specific_Population__c;
        	        
                   
        
		searchCriteria = '';
		resultCount = 0;
		
        this.soql = this.soqlBase;
         
         sortPointer = 'Owner';                     
       
         if(topicIdSelected != null && topicIdSelected != ''){
                  
            this.soql += ' AND name = \''+String.escapeSingleQuotes(topicIdSelected)+'\'';
            
            searchCriteria += 'Topic ID: ' + topicIdSelected + '\n';
            
          }       
       
         if(stakeHolderGroupSelected != null && stakeHolderGroupSelected != ''){
            
            this.soql += ' AND Stakeholder_Group__c = \''+String.escapeSingleQuotes(stakeHolderGroupSelected )+'\'';
            
             searchCriteria += 'Stakeholder: ' + stakeHolderGroupSelected + '\n';
                  
          }    
                           
          if(disease1Selected != null && disease1Selected != '' ){
                  
            this.soql += ' AND Disease_Condition_1__c = \''+String.escapeSingleQuotes(disease1Selected )+'\'';
            searchCriteria += 'Disease Condition: ' + disease1Selected + '<br>';
                
          }
          
          if(disease1SubtopicSelected != null && disease1SubtopicSelected != '' ){
                  
            this.soql += ' AND Disease_Condition_Subtopic_1__c = \''+String.escapeSingleQuotes(disease1SubtopicSelected )+'\'';
            searchCriteria += 'Disease Condition 1 Subtopic: ' + disease1SubtopicSelected + '<br>';
                
          }       
          
        
          if(statusSelected != null && statusSelected != '' ){
                  
            this.soql += ' AND External_Status__c = \''+String.escapeSingleQuotes(statusSelected )+'\'';
            searchCriteria += 'External Status: ' + statusSelected + '\n';
                
          }  
          
         // if(populationListSelected != null && populationListSelected != '' ){
                  
         //   this.soql += ' AND Priority_Population__c = \''+String.escapeSingleQuotes(populationListSelected )+'\'';
         //   searchCriteria += 'Population: ' + populationListSelected + '\n';
                
        //  }   
 
 //     Population change
 
           if(populationListSelected != null && populationListSelected != ''){
          	
            String[] serials = new string[0];    
            serials = populationListSelected.split(';');   
            String addToSql = ' AND Specific_Population__c INCLUDES (';
            System.debug('populationListSelected Selected --->' + populationListSelected + ' Serial --> ' + serials);
            
            for(integer i=0; i<serials.size();i++){
            	
            	if(i==serials.size() -1)
            		addToSql = addToSql + '\''+String.escapeSingleQuotes(serials[i])+'\'';
            	else
            	    addToSql = addToSql + '\''+String.escapeSingleQuotes(serials[i])+'\',';
            	
            }    
              
            addToSql = addToSql + ')';
              
            System.debug('populationListSelected addToSql --->' + addToSql);
            	
           	this.soql += addToSql;
           	 	
          }
 
 
 // Population change         

          if(healthSelected != null && healthSelected != ''){
          	
            String[] serials = new string[0];    
            serials = healthSelected.split(';');   
            String addToSql = ' AND Health_Topic__c INCLUDES (';
            System.debug('healthSelected Selected --->' + healthSelected + ' Serial --> ' + serials);
            
            for(integer i=0; i<serials.size();i++){
            	
            	if(i==serials.size() -1)
            		addToSql = addToSql + '\''+String.escapeSingleQuotes(serials[i])+'\'';
            	else
            	    addToSql = addToSql + '\''+String.escapeSingleQuotes(serials[i])+'\',';
            	
            }    
              
            addToSql = addToSql + ')';
              
            System.debug('healthSelected addToSql --->' + addToSql);
            	
           	this.soql += addToSql;
           	 	
          }                                   
 
 
          if(keywordsSelected != null && keywordsSelected != ''){
            
            String addToSql = ' AND (Research_Question_Text1__c LIKE ' + '\'' + '%'; 
            
            addToSql = addToSql +String.escapeSingleQuotes(keywordsSelected);
            
            addToSql = addToSql + '%' + '\'';
            
            addToSql = addToSql + ' OR Research_Question_Text2__c LIKE ' + '\'' + '%'; 
            
            addToSql = addToSql +String.escapeSingleQuotes(keywordsSelected);
            
            addToSql = addToSql + '%' + '\'' + ')';           
            
              
            System.debug('keywordsSelected addToSql --->' + addToSql);
            	
           	this.soql += addToSql;            
                  
          }     
                           
         
         // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'SQL:  ' + soql));   
                            
          try{
          	
          	
          	
             system.debug('TESTER searchCriteria results--> ' + searchCriteria );  
                 
            String soqlSort = this.soql + ' order by name LIMIT 500';   
                     
            system.debug('TESTER soql --> ' + soqlSort);  
                                  
            this.topicList =  Database.query(soqlSort);
            
            resultCount = this.topicList.size();
            
            
            system.debug('TESTER results--> ' );  
            
            System.debug('during initial ' + this.topicList.size() + ' ');
           //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Send ' + ' SQL ' + this.soql));  
            
         }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Ooops! ' + e.getmessage() + ' SQL ' + this.soql));  
         }
    
    }   
    
    public PageReference cancel() {
        return null;
    }  
    
    public PageReference save() {
        return null;
    }    
    
     public PageReference clearFields() {
        topicVals.Reason__c= null;          
        topicVals.Stakeholder_Group__c= null;
        topicVals.Disease_Condition__c= null;   
        topicVals.Keywords__c= null;  
        topicVals.Disease_Condition_1__c =null;
        topicVals.Disease_Condition_Subtopic_1__c =null;
        
        topicVals.Disease_Condition_2__c =null;
        topicVals.Disease_Condition_Subtopic_2__c =null;   
        
        topicVals.PCORI_Research_Priority_Area__c =null;
        topicVals.Priority_Population__c= null;
        topicVals.Specific_Population__c = null;
        topicVals.Health_Topic__c =null;
        topicVals.Care_Setting__c =null;   
        topicVals.External_Status__c = null; 
        populationSelected=null;
        stakeholderSelected=null;
                 
        this.topicList.clear();	
        resultCount = 0;
        
        return null;
    }     
     // Confirm List sort
     public void toggleSort() {
        
        if(sortDir == 'asc'){
            sortDir = 'desc';
            sortPointer = 'Owner v';                
        }else{
            sortDir = 'asc';
            sortPointer = 'Owner ^';    

        }
        
        String soqlOrder = this.soql  +  ' order by ' + sortField + ' ' + sortDir + ' LIMIT 500';  
        
        system.debug('soqlOrder @ toggleSort --> ' + soqlOrder);                            
        this.topicList =  Database.query(soqlOrder);
        
  
     }      
    
 	 /*
 	    Change to picklist values
 	 */
 	 
  	public List<SelectOption> getPriorityPopulation()
	{
  		List<SelectOption> options = new List<SelectOption>();
        
   		Schema.DescribeFieldResult fieldResult = Topic__c.Specific_Population__c.getDescribe();
   		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
     	options.add(new SelectOption('-None-', '-None-'));
        
        
   		for( Schema.PicklistEntry f : ple)
   		{
   			if(f.getValue() == 'Other (please specify)') continue;
      		options.add(new SelectOption(f.getValue(), f.getValue()));
   		}       
   			
   			return options;
	}	 
	
 	
 	public List<SelectOption> getStakeholdersGroup()
	{
  		List<SelectOption> options = new List<SelectOption>();
        
   		Schema.DescribeFieldResult fieldResult = Topic__c.Submission_Page_Stakeholder__c.getDescribe();
   		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
         
        options.add(new SelectOption('-None-', '-None-'));
         

   		for( Schema.PicklistEntry f : ple)
   		{
      		options.add(new SelectOption(f.getLabel(), f.getValue()));
   		}       
   			return options;
	} 	 
 	 // End
 	 
 	 
 	     

}