@isTest
private class BudgetListComponentController_Test {
	
	@isTest static void testBudgetList() {
		Id r1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RA Applications').getRecordTypeId();
		Id pcoriCommunityPartID = [SELECT Id FROM Profile WHERE name = 'PCORI Community Partner'].Id;

		Account.SObjectType.getDescribe().getRecordTypeInfos();

		Account a = new Account();
		a.Name = 'Acme1';
		insert a;

		Contact con = new Contact(LastName = 'testCon', AccountId = a.Id);
		Contact con1 = new Contact(LastName = 'testCon1', AccountId = a.Id);

		List<Contact> cons = new List<Contact>();
		cons.add(con);
		cons.add(con1);
		insert cons;
		
		User user = new User(Alias = 'test123', Email = 'test123fvb@noemail.com',
							 Emailencodingkey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',    
							 LocaleSidKey = 'en_US', ProfileId = pcoriCommunityPartID , Country='United States',IsActive = true,   
							 ContactId = con.Id, CommunityNickname = 'test12',
							 TimeZoneSidKey='America/New_York', UserName = 'testerfvb1@noemail.com');

		User user1 = new User(Alias = 'test1234', Email = 'test1234fvb@noemail.com',
							  Emailencodingkey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',    
							  LocaleSidKey = 'en_US', ProfileId = pcoriCommunityPartID , Country='United States',IsActive = true,   
							  ContactId = con1.Id, CommunityNickname = 'test12345',
							  TimeZoneSidKey='America/New_York', UserName = 'testerfvb123@noemail.com');

		List<User> users = new List<User>();
		users.add(user);
		users.add(user1);
		insert users;
		
		Cycle__c c = new Cycle__c();
		c.Name = 'testcycle';
		c.COI_Due_Date__c = date.valueof(System.now());
		insert c;

		Campaign camp = new Campaign();
		camp.Name = System.Label.Symptom_Management_for_Patients_with_Advanced_Illness;
		camp.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('PCORI Portal').getRecordTypeId();
		camp.Cycle__c = c.Id;
		camp.IsActive = true;
		camp.FGM_Portal__Visibility__c = 'Public';
		camp.Status = 'In Progress';
		camp.StartDate = System.today();
		camp.EndDate = System.today() + 7;
		camp.FGM_Base__Board_Meeting_Date__c = System.today() + 14;

		insert camp;
		
		opportunity o = new opportunity();
		
		o.Awardee_Institution_Organization__c=a.Id;
		o.CampaignId = camp.Id;
		o.RecordTypeId = r1;
		o.Full_Project_Title__c='test';
		o.Project_Name_off_layout__c='test';
		o.CloseDate = Date.today() + 14;
		o.Application_Number__c='RA-1234';
		o.StageName = 'Pending PI Application';
		o.External_Status__c = 'Draft';
		o.Name='test';
		o.PI_Name_User__c = user.id;
		o.AO_Name_User__c = user1.id;
		o.Project_Start_Date__c = system.Today();
		o.Bypass_Flow__c = true;
		
		insert o;

		Budget__c b = new Budget__c();
		b.Associated_App_Project__c= o.id;
		b.Budget_Status__c = 'Archived';
		b.Bypass_Flow__c = true;
		insert b;

		BudgetListComponentController bList1 = new BudgetListComponentController();
		bList1.oppId = o.id;
		bList1.retId = o.id;
		bList1.displayArchived = false;

		BudgetListComponentController bList2 = new BudgetListComponentController();
		bList2.oppId = o.id;
		bList2.retId = o.id;
		bList2.displayArchived = true;

		Test.startTest();
		Boolean init;
		init = bList1.init; //initialize bList1 (no archived displayed)
		System.assert(bList1.budgetlst == null);
		init = bList2.init; //initialize bList2 (archived displayed)
		System.assert(!bList2.budgetlst.isEmpty());
		Test.stopTest();
	}
	
}