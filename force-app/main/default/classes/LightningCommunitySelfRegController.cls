global class LightningCommunitySelfRegController {
    
    
     @AuraEnabled
    public static String login(String username, String password, String startUrl) {
        try{
            
            ApexPages.PageReference lgn = Site.login(username, password, startUrl);
            aura.redirect(lgn);
            return null;
        }
        catch (Exception ex) {
            return ex.getMessage();            
        }
    }

}