@isTest
public class PublicationProjectUpdatetest {
    
static testMethod void PublicationPUtest1() {
    
    Schema.DescribeSObjectResult oppsche = Schema.SObjectType.Opportunity;
Map<string, schema.RecordtypeInfo> sObjectMap = oppsche.getRecordtypeInfosByName();
Id stroppRecordTypeId = sObjectMap.get('Research Awards').getRecordTypeId();
    
     Schema.DescribeSObjectResult pubsche = Schema.SObjectType.Publication__c;
Map<string, schema.RecordtypeInfo> sObjectMap1 = pubsche.getRecordtypeInfosByName();
Id pubRecordTypeId = sObjectMap1.get('Journal Article').getRecordTypeId();
    
    Account a = new Account();
    a.Name = 'Acme';
    insert a;
    
     opportunity o=new opportunity();
    o.name='testopp';
    o.StageName='Qualification';
    o.closedate=system.today()+5;
    o.accountid=a.id;
    o.Full_Project_Title__c='testpms';
    o.Project_Name_off_layout__c='testpms';
    //o.Project_Name__c='testpm';
    o.Project_End_Date__c=system.today()+1;
    o.recordtypeid=stroppRecordTypeId;
    o.AO_Name_User__c = '00570000002dlMG';
            o.PI_Name_User__c = '00570000002bg4e';
     insert o;

FGM_Base__Grantee_Report__c FGMtest = new FGM_Base__Grantee_Report__c();
    FGMtest.Project__c = o.id;
    FGMtest.Status__c='Open';
    FGMtest.FGM_Base__Status__c='Open';
    FGMtest.FGM_Base__Request__c = o.id;
    FGMtest.When_in_the_project_were_patients_and__c='what the study is about';
    insert FGMtest;
    
    Publication__c pb= new Publication__c();
    pb.Peer_reviewed__c='Yes';
    pb.Progress_Report_del__c=FGMtest.Id;
    pb.Publication_Type__c='Final Study Results';
    pb.Status__c='In Preparation';
    pb.Title__c='test';
    pb.Authors__c='kalyan';
    pb.RecordTypeId=pubRecordTypeId;
    insert pb;
}
    
    static testMethod void PublicationPUtest2() {
    
    Schema.DescribeSObjectResult oppsche = Schema.SObjectType.Opportunity;
Map<string, schema.RecordtypeInfo> sObjectMap = oppsche.getRecordtypeInfosByName();
Id stroppRecordTypeId = sObjectMap.get('Research Awards').getRecordTypeId();
    
     Schema.DescribeSObjectResult pubsche = Schema.SObjectType.Publication__c;
Map<string, schema.RecordtypeInfo> sObjectMap1 = pubsche.getRecordtypeInfosByName();
Id pubRecordTypeId = sObjectMap1.get('Journal Article').getRecordTypeId();
    
    Account a = new Account();
    a.Name = 'Acme';
    insert a;
    
     opportunity o=new opportunity();
    o.name='testopp';
    o.StageName='Qualification';
    o.closedate=system.today()+5;
    o.accountid=a.id;
    o.Full_Project_Title__c='testpms';
    o.Project_Name_off_layout__c='testpms';
    //o.Project_Name__c='testpm';
    o.Project_End_Date__c=system.today()+1;
    o.recordtypeid=stroppRecordTypeId;
        o.AO_Name_User__c = '00570000002dlMG';
            o.PI_Name_User__c = '00570000002bg4e';
     insert o;


    
    Publication__c pb= new Publication__c();
    pb.Peer_reviewed__c='Yes';
    pb.Publication_Type__c='Final Study Results';
    pb.Status__c='In Preparation';
    pb.Title__c='test';
    pb.Authors__c='kalyan';
    pb.RecordTypeId=pubRecordTypeId;
    insert pb;
}
}