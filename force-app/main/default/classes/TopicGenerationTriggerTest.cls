@isTest
public class TopicGenerationTriggerTest {
    
    static testMethod void testTopicSummaryForm1() {
        Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Reviewer-Panelist').getRecordTypeId();
        Account acc= new Account(Name='Test',BillingStreet='Test',BillingCity='Test',BillingState='Test',BillingCountry='Test',BillingPostalCode='60301');
        insert acc;
        Contact con1 = new Contact(LastName = 'testcontact' ,email='test@test.com',RecordtypeId=recordTypeId,AccountId=acc.id,Advisory_Panel_Status__c='Current Panelist',Assigned_Advisory_Panel__c='Patient Engagement');
        insert con1;
        con1.LastName='Test1';
        update con1;
        
        Topic__c t = new Topic__c();
        t.Email__c='test@test.com';
        t.External_Status__c='Topic Already Being Considered'; 
        insert t;
    }    
    
    
}