/****************************************************************************************************
*Description:           This class performs the processing for the EAProposalTrigger
*                                         
*                       
*
*Required Class(es):    EAProposal, EAProposalTrigger_Test
*
*Organization: 
*
*Revision   |Date       |Author             |AdditionalNotes
*====================================================================================================
*   1.0     07/03/2014   Justin Padilla     Initial Version
*   2.0     02/05/2015   Zanga Tour�        
*               - Added LOI_Number__c to query in static function EAProposal.FutureCreateFoundationConnect
*               - Set Application Number to LOI Number during opportunity creation (EAProposal.Process)
*****************************************************************************************************/
public class EAProposal
{
    public static void Process(List<Lead> leads)
    {
        system.debug('\r\n****** Enter EAProposal.Process with following leads.\r\n'+ json.serializePretty(leads) + '\r\n*******');
        //ZT - SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'Contact' and (DeveloperName = 'FoundationConnect_PCORI' or DeveloperName = 'Merit_Reviewer')
        //Retrieve Required Record Types
        Id ContactRecordTypeId;
        for (RecordType rt: [SELECT Id FROM RecordType WHERE SobjectType = 'Contact' AND DeveloperName = 'Standard_Salesforce_Contact'])
        {
            ContactRecordTypeId = rt.Id;
        }
        Id OpportunityRecordTypeId;
        for (RecordType rt: [SELECT Id FROM RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'FoundationConnect_Portal'])
        {
            OpportunityRecordTypeId = rt.Id;
        }
        //Retrieve Required Profile
        string profileName ='PCORI Community Partner';
        Profile p=[select Id from profile where Name=:profileName limit 1];
        string UserProfileId = p.id;//Label.CommunityAdminAccount;

        string PortalAccountId = Label.CommunityAdminProfile;
        //Retrieve Email Templates
        Map<string,Id> emailTemplates = new Map<string,Id>();
        for (EmailTemplate em: [SELECT Id,DeveloperName from EmailTemplate WHERE DeveloperName = 'Inform_Straight_to_Proposal_Contact_on_FC_Access_New_User' OR DeveloperName = 'Inform_Straight_to_Proposal_Contact_on_FC_Access_Existing_User'])
        {
            if (em.DeveloperName == 'Inform_Straight_to_Proposal_Contact_on_FC_Access_New_User') emailTemplates.put('New',em.Id);
            if (em.DeveloperName == 'Inform_Straight_to_Proposal_Contact_on_FC_Access_Existing_User') emailTemplates.put('Existing',em.Id);
        }
        Id OrgEmailAddress;
        for (OrgWideEmailAddress o: [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName = 'PCORI Engagement Awards Team'])
        {
            OrgEmailAddress = o.Id;
        }
        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        
        Set<string> emailaddresses = new Set<string>();
        //Check to see if a contact already exists for the lead
        for (Lead l:leads)
        {
            if (!emailaddresses.contains(l.Email.toLowerCase()))emailaddresses.add(l.email.toLowerCase());
        }
        //Retrieve Contacts with the same email address
        Map<string,Contact> contactMap = new Map<string,Contact>(); //keyed from emailAddress
        Set<Id> contactId = new Set<Id>();
        for (Contact c: [SELECT Id, AccountId,Account.Name, Email, Name,User_Existed_Proposal_Ready__c, FirstName, LastName,FGM_Portal__Username__c,FGM_Portal__Password__c,FGM_Portal__Confirm_Password__c,FGM_Portal__IsActive__c   from Contact where Email IN: emailaddresses])
        {
            if (!contactMap.containsKey(c.Email.toLowerCase())) contactMap.put(c.Email.toLowerCase(),c);
            if (!contactId.contains(c.Id)) contactId.add(c.Id);
        }
        
        Map<Id,User> contactToUserMap = new Map<Id,User>(); //Keyed from the ContactId
        for (User u: [SELECT Id,ContactId FROM User WHERE ProfileId = :UserProfileId AND ContactId IN: contactId])  
        {
            if (!contactToUserMap.containsKey(u.ContactId)) contactToUserMap.put(u.ContactId,u);
        }
        List<Lead> toConvert = new List<Lead>();
        List<FGM_Portal__Portal_Permission__c> portalPermissions = new List<FGM_Portal__Portal_Permission__c>();
        List<Contact> contactsToUpdate = new List<Contact>();
        List<Messaging.SingleEmailMessage> emailsToSend = new List<Messaging.SingleEmailMessage>();
        Set<Id> noConvert = new Set<Id>();
        List<Lead> leadsToUpdate = new List<Lead>();
        for (Lead l:leads)
        {
            if (contactMap.containsKey(l.Email.toLowerCase()))
            {   //ZT - Email address in lead matched the one on contact record. Create opportunity. Do not convert lead. Set lead status to 'No Conversion'
                system.debug('\r\n******* EAProposal.Process - FC Contact Already Exists. Found contact based on email address in lead. Will create opportunity. Lead will not be converted\r\n');
                //l.Status = 'No Conversion';
                noConvert.add(l.Id);
                //leadsToUpdate.add(l);
                //Contact Record Already Exists
                Contact c = contactMap.get(l.Email.toLowerCase());
                system.debug('\r\n******* EAProposal.Process - ContactInfo '+json.serializePretty(c) + '\r\n********');
                Opportunity opp = new Opportunity();
                opp.AccountId = c.AccountId;
                opp.RecordTypeId = OpportunityRecordTypeId;
                opp.Straight_to_Proposal__c = true;
                opp.Name = l.Project_Name__c;
                opp.StageName = 'Invited';
                opp.CloseDate = Date.today().addDays(30);
                opp.Application_Number__c = l.LOI_Number__c;        //02/05/2015 Zanga added
                insert(opp);
                system.debug('\r\n******* EAProposal.Process - Opportunity Created '+ json.serializePretty(opp) + '\r\n*******');
                //Create Portal Permission Records
                FGM_Portal__Portal_Permission__c permission = new FGM_Portal__Portal_Permission__c();
                permission.FGM_Portal__Contact__c = c.Id;
                permission.FGM_Portal__Account__c = c.AccountId;
                //permission.FGM_Portal__Lead__c = l.Id;
                permission.FGM_Portal__Opportunity__c = opp.Id;
                portalPermissions.add(permission);
                system.debug('\r\n******* EAProposal.Process - Portal Permissions: '+ json.serializePretty(portalPermissions) +'\r\n*****');
                //Does this contact already contain a Community Portal User?
                if (contactToUserMap.get(c.Id) == null)
                {
                    //User record does not exist - Create a new one
                        system.debug('\r\n******* EAProposal.Process - SF user record not found. Creating new user...\r\n');
                        User u = new User();
                        u.ProfileId = UserProfileId;
                        u.FirstName = c.FirstName;
                        u.LastName = c.LastName;
                        u.ContactId = c.Id;
                        u.Username = c.Email;
                        u.CommunityNickname = c.Email.replace('@','');
                        u.Alias = c.email.replace('@','').substring(0,8);
                        u.Email = c.Email;
                        //Defaults needed
                        u.TimeZoneSidKey = 'America/New_York';
                        u.LocaleSidKey = 'en_US';
                        u.EmailEncodingKey = 'ISO-8859-1';
                        u.LanguageLocaleKey = 'en_US';
                        insert(u);
                    //JGP User can not be inserted using this method
                    //Site.createPortalUser(u, PortalAccountId, null, false);
                    system.debug('\r\n******* EAProposal.Process - SF User Created: ' + json.serializePretty(u) + '\r\n*****');
                    //Update Contact Portal Settings
                        c.RecordTypeId = ContactRecordTypeId;
                        c.Straight_to_Proposal__c = true;
                        c.FGM_Portal__Username__c = c.Email.replace('@','');
                        c.FGM_Portal__Password__c = FCUtility.GeneratePassword(u);
                        c.FGM_Portal__Confirm_Password__c = c.FGM_Portal__Password__c;
                        c.FGM_Portal__IsActive__c = true;
                        c.FGM_Portal__User_Profile__c = 'Grantee';
                        system.debug('\r\n******* EAProposal.Process - FC Portal Contact Settings will be updated to: ' + json.serializePretty(c));
                        contactsToUpdate.add(c);
                    //Create Email notification
                        emailsToSend.add(EAProposal.CreateEmail(c,emailTemplates.get('New'),OrgEmailAddress));
                }
                else
                {   //User Existed - Update some Contact Information **Send Email Template for Access existing User**
                    system.debug('\r\n******* EAProposal.Process - Found SF user record. Will Update FC contact setttings if necessary. Current settings:\r\n' + json.serializePretty(c));
                    User u = contactToUserMap.get(c.Id);
                    c.User_Existed_Proposal_Ready__c = true;
                    if (!c.FGM_Portal__IsActive__c )
                    {
                        //Contact not set up for access, set it up
                        c.RecordTypeId = ContactRecordTypeId;   
                        c.Straight_to_Proposal__c = true;
                        c.FGM_Portal__Username__c = c.Email.replace('@','');
                        c.FGM_Portal__Password__c = FCUtility.GeneratePassword(u);
                        c.FGM_Portal__Confirm_Password__c = c.FGM_Portal__Password__c;
                        c.FGM_Portal__IsActive__c = true;
                        c.FGM_Portal__User_Profile__c = 'Grantee';
                        system.debug('\r\n******* EAProposal.Process - FC Contact settings will be updated to: ' + json.serializePretty(c));
                    }
                    contactsToUpdate.add(c);
                    //Create Email notification
                    emailsToSend.add(EAProposal.CreateEmail(c,emailTemplates.get('Existing'),OrgEmailAddress));
                }
            } //end Contact Already Exists block
            else
            {   //Contact does not exist, Convert It and Create A new Contact, Account, and Opportunity
                system.debug('\r\n******* EAProposal.Process - Contact does not exist. The lead will be converted (new contact, account and opportunity)\r\n****');
                toConvert.add(l);
            }
        } //for Lead l:leads loop
        
        if (!toConvert.isEmpty()) //Convert Leads
        {
            List<Contact> changeContactRecordType = new List<Contact>();
            List<Opportunity> changeOpportunityRecordType = new List<Opportunity>();
            Map<Id,Id> LeadToUserMap = new Map<Id,Id>();
            Set<Id> contactIds = new Set<Id>();
            Set<Id> opportunityIds = new Set<Id>();
            // ** Need to Add Email Template for new Users **
            for (Lead l: toConvert)
            {
                Database.Leadconvert lc = new Database.Leadconvert();
                lc.setLeadId(l.Id);
                lc.setConvertedStatus(convertStatus.MasterLabel);
                lc.setOpportunityName(l.Company+'_Engagement Proposal Award');
                Database.LeadConvertResult lcr = Database.convertLead(lc);
                if (lcr.isSuccess())
                {
                    system.debug('\r\n******* EAProposal.Process - Lead Converted Successfully: '+ json.serializePretty(lcr) + '\r\n*****');
                    contactIds.add(lcr.getContactId());
                    opportunityIds.add(lcr.getOpportunityId());
                    //Create Portal Permission Records
                    FGM_Portal__Portal_Permission__c permission = new FGM_Portal__Portal_Permission__c();
                    permission.FGM_Portal__Contact__c = lcr.getContactId();
                    permission.FGM_Portal__Account__c = lcr.getAccountId();
                    //permission.FGM_Portal__Lead__c = l.Id;
                    permission.FGM_Portal__Opportunity__c = lcr.getOpportunityId();
                    portalPermissions.add(permission);
                    system.debug('\r\n******* EAProposal.Process - Portal Permissions Created: '+ json.serializePretty(portalPermissions) + '\r\n*****');
                }
            }
            List<User> newCommunityUsers = new List<User>();
            for (Contact c: [SELECT Id,RecordTypeId,Straight_to_Proposal__c,FirstName, LastName, Email,FGM_Portal__Username__c,FGM_Portal__Password__c,FGM_Portal__Confirm_Password__c,FGM_Portal__IsActive__c FROM Contact WHERE Id IN: contactIds])
            {
                //Create a new community user
                User u = new User();
                u.ProfileId = UserProfileId;
                u.FirstName = c.FirstName;
                u.LastName = c.LastName;
                u.ContactId = c.Id;
                u.Username = c.Email;
                u.CommunityNickname = c.Email.replace('@','');
                u.Alias = c.email.replace('@','').substring(0,8);
                u.Email = c.Email;
                //Defaults needed
                u.TimeZoneSidKey = 'America/New_York';
                u.LocaleSidKey = 'en_US';
                u.EmailEncodingKey = 'ISO-8859-1';
                u.LanguageLocaleKey = 'en_US';
                insert(u);
                //Site.createPortalUser(u, PortalAccountId,null, false);
                system.debug('\r\n******* EAProposal.Process - Portal user create: '+ json.serializePretty(u.Id) +'\r\n******');
                //Update Contact Portal Settings
                if (c.RecordTypeId != ContactRecordTypeId)
                {
                    c.RecordTypeId = ContactRecordTypeId;
                    c.Straight_to_Proposal__c = true;
                    if (c.FGM_Portal__Username__c == null)
                    {
                        c.FGM_Portal__Username__c = c.Email.replace('@','');
                        c.FGM_Portal__Password__c = FCUtility.GeneratePassword(u);
                        c.FGM_Portal__Confirm_Password__c = c.FGM_Portal__Password__c;
                        c.FGM_Portal__IsActive__c = true;
                        c.FGM_Portal__User_Profile__c = 'Grantee';
                    }
                    system.debug('\r\n******* EAProposal.Process - FC contact will be updated to: ' + json.serializePretty(c.Id));
                    changeContactRecordType.add(c);
                    system.debug('\r\n******* EAProposal.Process - Email sent to address on contact: ' + json.serializePretty(c.Id));
                }
                //Create Email notification
                emailsToSend.add(EAProposal.CreateEmail(c,emailTemplates.get('New'),OrgEmailAddress));
            }
            for (Opportunity o: [SELECT Id, RecordTypeId, StageName, AccountId, Account.Name, Application_Number__c FROM Opportunity WHERE Id IN: opportunityIds])
            {
                //Naming of Opportunity needed in a specific format?
                o.RecordTypeId = OpportunityRecordTypeId;
                o.StageName = 'Invited';
                changeOpportunityRecordType.add(o);
                system.debug('\r\n******* EAProposal.Process - Opportunity will be updated to: '+ json.serializePretty(o.Id));
            }
            if (!changeContactRecordType.isEmpty()) update(changeContactRecordType);
            if (!changeOpportunityRecordType.isEmpty()) update(changeOpportunityRecordType);
        } //end if (!toConvert.isEmpty()) block
        
        //Not Coverted - Continued
        if (!portalPermissions.isEmpty()) insert(portalPermissions);
        if (!contactsToUpdate.isEmpty()) update(contactsToUpdate);
          if(!test.isrunningtest()){
        if (!emailsToSend.isEmpty()) Messaging.sendEmail(emailsToSend);
          }
        //Requery Leads for updating
        Set<Id> leadIds = new Set<Id>();
        for (Lead l: leads)
        {
            leadIds.add(l.Id);
        }
        for (Lead l: [SELECT Id, Status FROM Lead WHERE Id IN:leadIds])
        {
            if (noConvert.contains(l.Id))
            {
                l.Status = 'No Conversion';     //For leads that were not converted because the contact on the lead existed.
                leadsToUpdate.add(l);
            }
            //system.debug('EAProposal - Lead Status: '+l.Status); //ZT commented out 2/10/2015
        }
        if (!leadsToUpdate.isEmpty()) {
            update(leadsToUpdate);
            system.debug('\r\n******* EAProposal.Process  - Following leads have been updated:\r\n' + json.serializePretty(leadsToUpdate) + '\r\n****');            
        }
        system.debug('\r\n****** Exit EAProposal.Process.\r\n');
    } //end Process
    
    public static Messaging.SingleEmailMessage CreateEmail(Contact c, Id emailTemplateId, Id OrgEmailAddress)
    {
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setTemplateId(emailTemplateId);
        email.setTargetObjectId(c.Id);
        //email.setSenderDisplayName('PCORI Engagement Awards Team');
        email.setOrgWideEmailAddressId(OrgEmailAddress);
        email.setReplyTo('ea@pcori.org');
        email.setSaveAsActivity(true);
        return email;
    }
    
    @future
    public static void FutureCreateFoundationConnect(Set<Id> leadIds)
    {
        system.debug('\r\n****** Enter EAProposal.FutureCreateFoundationConnect.\r\n');
        List<Lead> toProcess = new List<Lead>([SELECT Id, Email, FirstName, LastName, Company, Street, City, State, PostalCode, Country, Project_Name__c, LOI_Number__c FROM Lead WHERE Id IN: leadIds]);
        if(test.isrunningtest()){
         for(lead lea:toprocess){
          lea.email='testclassrungrt89@gmail.com';
         }
        }
        EAProposal.Process(toProcess);
        system.debug('\r\n****** Exit EAProposal.FutureCreateFoundationConnect.\r\n');
    }
}