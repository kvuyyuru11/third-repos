@isTest
public class RouterclassTest {
    
    static testMethod void testRouterController() {
        list<User> u2 = TestDataFactory.getpartnerUserTestRecords(1,1);
        
        System.runAs(u2[0]) {            
            Merit_Reviewer_Application__c mr = new Merit_Reviewer_Application__c();
            // insert mr;            
            ApexPages.StandardController cntrl = new ApexPages.StandardController(mr);            
            RouterClass RC = new RouterClass(cntrl);
            RC.router();
        }        
    }
    
    static testMethod void testRouterController2() {
        list<User> u2 = TestDataFactory.getpartnerUserTestRecords(1,1);
        System.runAs(u2[0]) {            
            Merit_Reviewer_Application__c   mr = new Merit_Reviewer_Application__c();
            // insert mr;            
            ApexPages.StandardController cntrl = new ApexPages.StandardController(mr);           
            RouterClass RC = new RouterClass(cntrl);
            RC.router();
        }
    }
}