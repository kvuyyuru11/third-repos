public class AssignPermissionSet{
    @future
    public static void assign(List<Id> usrIds){

        PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Label = 'Community_Reviewer_Permission_set_User_Login_License'];
        List<PermissionSetAssignment> psa1lst = [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId = :ps.Id AND AssigneeId IN :usrIds];
        if(psa1lst.size() == 0){
            List<PermissionSetAssignment> psalst = new List<PermissionSetAssignment>();
            for(Id usrId :usrIds){
                PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = ps.Id, AssigneeId = usrId);
                psalst.add(psa);
            }
            if(!psalst.isEmpty()){
                upsert psalst;
            }
        }
        
    }
}