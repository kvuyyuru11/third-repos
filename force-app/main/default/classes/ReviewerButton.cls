Global Class ReviewerButton
{
     

    webservice static String pickPage(String userId, String urlVal)
    {
        List<PermissionSet> permSetIds = [SELECT Name, Id from PermissionSet where Label ='Merit Reviewer Approved- PCL' limit 1];
        String permId = permSetIds[0].Id; 
               
        if(permId != null)
        { 
            List<PermissionSetAssignment> permSetAssignIds = [SELECT Id from PermissionSetAssignment where PermissionSetId = :permId AND AssigneeId = :userId Limit 1];

            if(urlVal.contains('reviewer') && !permSetAssignIds.isEmpty())
            {
                 String URLString = 'https://'+[SELECT Domain, DomainType FROM Domain Limit 1].Domain + '/reviewer' + '/EmployerUpdate';
                 system.debug('asdasdasd '+URLString);
                 return URLString;
            }
            else
            {
                String URLString = Label.Portal_URL+'/EmployerUpdate';
                 return URLString;
            } 
        }
        else
        {
            String URLString ='#';
            return URLString;
        }
       
    }
}