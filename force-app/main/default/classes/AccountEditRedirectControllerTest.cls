/*
    Author     : Vijaya Kumar Sripathi
    Date       : 30th Mar 2017
    Name       : AccountEditRedirectControllerTest
    Description: AccountEditRedirectControllerTest Class
*/
@isTest
public class AccountEditRedirectControllerTest
{       
    static testMethod void AccountEditRedirectControllerTest()
    {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser)
        {            
            test.startTest();
            
            Profile p = [SELECT Id FROM Profile WHERE Name='PCORI Community Partner'];
            UserRole r = [SELECT Id FROM UserRole WHERE Name='Opt Out Partner User' LIMIT 1];
               
            Account testAcc = new Account(Name = 'Test Account');
            insert testAcc;
                    
            Contact testCon= new Contact(LastName = 'TestContact', Email = 'test@pcori.org', 
                                            AccountId = testAcc.Id);        
            insert testCon;  
                
            User testUser = new User(alias = 'PI', email='PI1@pcori.com',
                                    emailencodingkey='UTF-8', lastname='Lead1',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id, ContactId = testCon.Id,
                                    timezonesidkey='America/New_York',
                                    username='PI1@pcori.com');
            insert testUser;  
            String testLbls = Label.MRA_Profile;
            Profile profiles = [SELECT Id FROM Profile WHERE Name='PCORI Community Partner'];
                                   
            ID usrProfileIds = profiles.ID;
            ApexPages.StandardController controller =new ApexPages.StandardController(testAcc);
            Account objAcc = testAcc;
            
            AccountEditRedirectController objAccredirect=new AccountEditRedirectController(controller);
            objAccredirect.usrProfileId=usrProfileIds;
            objAccredirect.accEditRedirect();
            objAccredirect.usrProfileId=testAcc.id;
            objAccredirect.accEditRedirect();
            ID usrProfileId = userInfo.getProfileId();
       
            test.stopTest();
        }         
    }

    
}