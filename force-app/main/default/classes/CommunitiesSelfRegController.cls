/**
* An apex page controller that supports self registration of users in communities that allow self registration
*/
public without sharing class CommunitiesSelfRegController {
    
    public String getCheckemail() {
        return null;
    }
    
    
    //public String checkEmail { get; set; }
    
    public String firstName {get; set;}
    public String lastName {get; set;}
    public String email {get; set;}
    public String password {get; set {password = value == null ? value : value.trim(); } }
    public String confirmPassword {get; set { confirmPassword = value == null ? value : value.trim(); } }
    public String communityNickname {get; set { communityNickname = value == null ? value : value.trim(); } }
    public boolean checkbox01 {get; set;}
    
    //This is the Constructor for the self registration controller of the communities
    public CommunitiesSelfRegController() {}
    
    //The below method checks if the value in password and confirm password are the same.
    public boolean isValidPassword() {
        return string.valueof(password).equals(string.valueof(confirmPassword));
    }
    
    //Vijay: The below method checks if the value in email and confirm email are the same. 
    public boolean isValidEmail() 
    { 
        return string.valueof(communityNickname).equals(string.valueof(email)); 
    }     
    
    //The below method checks if the Acknowledgement check box is checked or unchecked.
    public boolean confirmAgreement() {
        if(!checkbox01) {
            return false;
        } else {
            return true;
        }
    }
    
    //Below method allows user to self register with the below conditions
    //1.If the user already exists in the salesforce database then it throws the custom error.
    //2. If user does not exists but contact exists with same first and last name and email then it allows to self register and associates the same contact to the user without creating duplicate.
    //3. If user or contact does not exist in the system then creates a user and a relevant contact.
    //4. In the case the contact is created with self registration then the same contact is updated with the foundation connect details as below.
    public PageReference registerUser() {
        // it's okay if password is null - we'll send the user a random password in that case
        
        try{
            if (!isValidPassword()) {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.site.passwords_dont_match);
                ApexPages.addMessage(msg);
                return null;
            }
            
            //Vijay: The below method checks if the value in email and confirm email are the same.
            if (!isValidEmail()) 
            { 
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.site.email + ' did not match.'); 
                ApexPages.addMessage(msg); 
                return null; 
            }         
            
            if(!confirmAgreement()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Acknowledgement));
                return null;
                
            }
            String err = 'We cannot complete your request at this time because the email address you entered may be in use by a current user in the PCORI Community. If you would like more information, please submit a ticket <a href="https://help.pcori.org/hc/en-us/requests/new" target="_blank">here</a>.';
            
            list<user> usrlist=[select username,email from user where email=:email];
            if(usrlist!=null){
                
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, err));
            }
            
            
            RecordType RecType = [Select Id From RecordType  Where SobjectType = 'Contact' and DeveloperName = 'EA_Internal_Reviewers'];
            RecordType RecType1 = [Select Id From RecordType  Where SobjectType = 'Contact' and DeveloperName = 'FoundationConnect_PCORI'];
            RecordType RecType2 = [Select Id From RecordType  Where SobjectType = 'Contact' and DeveloperName = 'Merit_Reviewer'];
            RecordType RecType3 = [Select Id From RecordType  Where SobjectType = 'Contact' and DeveloperName = 'Standard_Salesforce_Contact'];
            for(contact c:[select id,accountid,firstname, lastname, Registered__c,email,FGM_Portal__Username__c,FGM_Portal__Password__c,FGM_Portal__Confirm_Password__c,FGM_Portal__IsActive__c from contact where Registered__c=false  and email=:email]){
                if(c.Email==email && c.LastName==lastName && c.FirstName==firstName){
                    String profileId = Label.MRA_Profile; // To be filled in by customer.
                    String roleEnum = null; // To be filled in by customer.
                    String accountId = c.AccountId; // To be filled in by customer.
                    
                    String userName = email;
                    
                    User u = new User();
                    u.Username = userName;
                    u.Email = email;
                    u.FirstName = firstName;
                    u.LastName = lastName;
                    u.CommunityNickname = communityNickname;
                    u.ProfileId = profileId;
                    u.TimeZoneSidKey='America/New_York';
                    
                    
                    String userId = Site.createPortalUser(u, accountId, password);
                    if (userId != null) { 
                        if (password != null && password.length() > 1) {
                            
                            if(c.FGM_Portal__Username__c==null && c.FGM_Portal__Password__c==null){     
                                c.FGM_Portal__Username__c = u.Email.replace('@','');
                                c.FGM_Portal__Password__c = FCUtility.GeneratePassword(u);
                                c.FGM_Portal__Confirm_Password__c = c.FGM_Portal__Password__c;
                                c.FGM_Portal__IsActive__c = true;
                                c.FGM_Portal__User_Profile__c = Label.Grantee;
                                c.RecordTypeId = Label.Standard_Contact_Record_Type;  
                                database.update(c);
                            }
                            return Site.login(userName, password, null);
                        }
                        else {
                            PageReference page = System.Page.CommunitiesSelfRegConfirm;
                            page.setRedirect(true);
                            return page;
                        }
                    }
                }
            }
            
            String profileId = Label.MRA_Profile; // To be filled in by customer.
            String roleEnum = null; // To be filled in by customer.
            String accountId = Label.MRA_Account; // To be filled in by customer.
            
            String userName = email;
            
            User u = new User();
            u.Username = userName;
            u.Email = email;
            u.FirstName = firstName;
            u.LastName = lastName;
            u.CommunityNickname = communityNickname;
            u.ProfileId = profileId;
            u.TimeZoneSidKey='America/New_York';
            
            String userId = Site.createPortalUser(u, accountId, password);
            
            if (userId != null) { 
                if (password != null && password.length() > 1) {
                    //Now that the User and Contact has been created - Update the Contact with generated FC password
                    Contact cnt = [SELECT Id,FGM_Portal__Username__c,FGM_Portal__Password__c,FGM_Portal__Confirm_Password__c,FGM_Portal__IsActive__c FROM Contact WHERE Id =: u.ContactId limit 1];
                    if(cnt.FGM_Portal__Username__c==null && cnt.FGM_Portal__Password__c==null){     
                        cnt.FGM_Portal__Username__c = u.Email.replace('@','');
                        cnt.FGM_Portal__Password__c = FCUtility.GeneratePassword(u);
                        cnt.FGM_Portal__Confirm_Password__c = cnt.FGM_Portal__Password__c;
                        cnt.FGM_Portal__IsActive__c = true;
                        cnt.FGM_Portal__User_Profile__c = Label.Grantee;
                        cnt.RecordTypeId = Label.Standard_Contact_Record_Type;  
                        database.update(cnt);
                    }
                    return Site.login(userName, password, null);
                }
                
                else {
                    PageReference page = System.Page.CommunitiesSelfRegConfirm;
                    page.setRedirect(true);
                    return page;
                }
            } 
        }
        
        catch(exception e){
            throw e;
        }
        
        
        return null;
    }
}