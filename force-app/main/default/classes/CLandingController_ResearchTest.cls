/**
 * An apex page controller that takes the user to the right start page based on credentials or lack thereof
 */
@IsTest public with sharing class CLandingController_ResearchTest {
    @IsTest public static void testCommunitiesLandingController() {
        // Instantiate a new controller with all parameters in the page
        CommunitiesLandingController_Research controller = new CommunitiesLandingController_Research();
        controller.forwardToStartPage();    
      }
}