@isTest
public class TestRedirectReportController {
    
    // test 1
    static testMethod void testNoReportFound () {
        redirectReportController controller = new redirectReportController();
        ApexPages.StandardController sc;
        redirectReportController controller2 = new redirectReportController(sc);
        controller.sendToPage();        
        System.Test.startTest();
        System.assertEquals(controller.isInterimEA, false);
        System.assertEquals(controller.isInterimRA, false);
        System.assertEquals(controller.isFinalEA, false);
        System.assertEquals(controller.isFinalRA, false);
        System.Test.stopTest();
    }    
    
    //test 2
    static testMethod void testInterimEA () {
        List<User> users = ObjectCreator.getUsers(1, 'user');
        insert users;        
        List<Account> accounts = ObjectCreator.getAccts('account', 4);
        insert accounts;    
        List<Opportunity> projects = ObjectCreator.getOpps(4, 'project');
        Opportunity project = projects[0];
        insert projects;        
        FGM_Base__Grantee_Report__c report = new FGM_Base__Grantee_Report__c();
        List <RecordType> recordTypes = [SELECT id from RecordType WHERE name =: 'Interim Progress Report - EA'];
        if (!recordTypes.isEmpty()) {
            report.recordTypeId = recordTypes[0].id;
        }
        else {
            // record type not found. throw assert error.
            System.assert(false);
        }
        
        report.FGM_Base__Request__c = project.id;
        insert report;
        
        redirectReportController controller = new redirectReportController((String)report.id);      
        // call contructor functions
        controller.gRecordInserts = [SELECT FGM_Base__Request__c, recordTypeId, id, RecordTypeName__c FROM FGM_Base__Grantee_Report__c WHERE Id =: report.Id];
        controller.setBooleansFalse();
        controller.setReportTypeBoolean();
        controller.portalProfileName = 'PCORI Community Partner SOW 11';
        // end constructor setup
        controller.sendToPage();
        
        System.Test.startTest();
        System.assertEquals(controller.isInterimEA, true);
        System.Test.stopTest();
    }    
    
    // test 3
    static testMethod void testInterimRA () {
        List<Account> accounts = ObjectCreator.getAccts('account', 4);
        insert accounts;    
        List<Opportunity> projects = ObjectCreator.getOpps(4, 'project');
        Opportunity project = projects[0];
        insert projects;        
        FGM_Base__Grantee_Report__c report = new FGM_Base__Grantee_Report__c();
        List <RecordType> recordTypes = [SELECT id from RecordType WHERE name =: 'Interim Progress Report - RA'];
        if (!recordTypes.isEmpty()) {
            report.recordTypeId = recordTypes[0].id;
        }
        else {
            // record type not found. throw assert error.
            System.assert(false);
        }
        
        report.FGM_Base__Request__c = project.id;
        insert report;
        
        redirectReportController controller = new redirectReportController(report.id);      
        // call contructor functions
        controller.gRecordInserts = [SELECT FGM_Base__Request__c, FGM_Base__Request__r.Networktype__c, recordTypeId, id, RecordTypeName__c, FGM_Base__Request__r.Program_R2__c FROM FGM_Base__Grantee_Report__c WHERE Id =: report.Id];
        controller.setBooleansFalse();
        controller.setReportTypeBoolean();
        controller.portalProfileName = 'PCORI Community Partner SOW 11';
        // end constructor setup
        controller.sendToPage();
        
        System.Test.startTest();
        System.assertEquals(controller.isInterimRA, true);
        System.Test.stopTest();
    }    
    
    // test 4
    static testMethod void testFinalRA () {
        List<Account> accounts = ObjectCreator.getAccts('account', 4);
        insert accounts;
        
        List<Opportunity> projects = ObjectCreator.getOpps(4, 'project');
        Opportunity project = projects[0];
        insert projects;
        
        FGM_Base__Grantee_Report__c report = new FGM_Base__Grantee_Report__c();
        List <RecordType> recordTypes = [SELECT id from RecordType WHERE name =: 'Final Progress Report - RA'];
        if (!recordTypes.isEmpty()) {
            report.recordTypeId = recordTypes[0].id;
        }
        else {
            // record type not found. throw assert error.
            System.assert(false);
        }
        
        report.FGM_Base__Request__c = project.id;
        insert report;
        
        redirectReportController controller = new redirectReportController(report.id);      
        // call contructor functions
        controller.gRecordInserts = [SELECT FGM_Base__Request__c, FGM_Base__Request__r.Networktype__c, recordTypeId, id, RecordTypeName__c, FGM_Base__Request__r.Program_R2__c FROM FGM_Base__Grantee_Report__c WHERE Id =: report.Id];
        controller.setBooleansFalse();
        controller.setReportTypeBoolean();
        controller.portalProfileName = 'PCORI Community Partner SOW 11';
        // end constructor setup
        controller.sendToPage();
        
        System.Test.startTest();
        System.assertEquals(controller.isFinalRA, true);
        System.Test.stopTest();
    }    
    
    // test 5
    static testMethod void testFinalEA () {
        List<Account> accounts = ObjectCreator.getAccts('account', 4);
        insert accounts;
        
        List<Opportunity> projects = ObjectCreator.getOpps(4, 'project');
        Opportunity project = projects[0];
        insert projects;
        
        FGM_Base__Grantee_Report__c report = new FGM_Base__Grantee_Report__c();
        List <RecordType> recordTypes = [SELECT id from RecordType WHERE name =: 'Final Progress Report - EA'];
        if (!recordTypes.isEmpty()) {
            report.recordTypeId = recordTypes[0].id;
        }
        else {
            // record type not found. throw assert error.
            System.assert(false);
        }
        
        report.FGM_Base__Request__c = project.id;
        insert report;
        
        redirectReportController controller = new redirectReportController(report.id);      
        // call contructor functions
        controller.gRecordInserts = [SELECT FGM_Base__Request__c, recordTypeId, id, RecordTypeName__c FROM FGM_Base__Grantee_Report__c WHERE Id =: report.Id];
        controller.setBooleansFalse();
        controller.setReportTypeBoolean();
        controller.portalProfileName = 'PCORI Community Partner SOW 11';
        // end constructor setup
        controller.sendToPage();
        
        System.Test.startTest();
        System.assertEquals(controller.isFinalEA, true);
        System.Test.stopTest();
    }    
}