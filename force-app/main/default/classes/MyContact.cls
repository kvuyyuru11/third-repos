//This is a class that opens the detail page of contact record by clicking the My profile link in the Community
public class MyContact {
    String cnt;
    //Method which redirects based on the userid of the logged in user
public PageReference redirect() {
    //Below soql query gets the contact of the user who logged in 
User u= [Select contactid from User where id=:userinfo.getUserId()];
cnt=string.valueof(u.contactid);
PageReference pageRef = new PageReference('/' + cnt);
pageref.setredirect(True);
        return pageref;
    }
public MyContact(){}
}