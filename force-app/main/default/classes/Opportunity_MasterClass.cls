/*------------------------------------------------------------------------------------------------------------------------------------
Code Written by SADC PCORI Team: Matt Hoffman
Written During Development Phase 2: June/2016 - September/2016
Last ModifiedBy :Kalyan (Pcori) 
Modification Description : This class was touched to modify 2 things
1. Ensure the lead when converted to opportunity the opportunity will have opportunityteammembershares method is written efficiently using the 
where clauses.
2. Previously all the review records that were created for online had the owner as the Panel MRO we are changing that now to be the contact's user on the review record.
--------------------------------------------------------------------------------------------------------------------------------------*/

public without sharing class Opportunity_MasterClass {
    public Map<String,Id> recTypeNameWithIdMap;
      
    private static Opportunity_MasterClass omc = new Opportunity_MasterClass();
    Opportunity_MasterClass() {
        recTypeNameWithIdMap=new Map<String,Id>();
        List<RecordType> oppRecLst = [SELECT Name, Id FROM RecordType WHERE sObjectType =: Label.Opportunity OR sObjectType =: Label.FC_Reviewer_External_Review];
        for(RecordType recInfo : oppRecLst){
            recTypeNameWithIdMap.put(recInfo.Name,recInfo.Id);
        }
        //Get all the reviews in the system. 
        
    }
    
    public static void afterUpdate(object[] newTrigger, Map<Id, Object> oldObjectMap)
    {
        List<Opportunity> newOppList = (List<Opportunity>) newTrigger;
        Map<Id, Opportunity> oldMap = (Map<Id, Opportunity>) oldObjectMap;
        omc.createOpportunityReviews(newOppList);
        OpportunityService.updateRecruitment(trigger.new);
        UpdateSharingToReadIfProjectCompleted(newOppList, oldMap);        
    }
    
    public static void afterInsert(object[] newTrigger)
    {
        List<Opportunity> newOppList = (List<Opportunity>) newTrigger;
        omc.leadToOpp_ConversionSharing(newOppList); //Added Feb 16th. TO verify that functionality still works -LR
        
        omc.createOpportunityReviews(newOppList);
        LOI_Attachments_ToOpp(newOppList);
        
    }
    
    public static Boolean CheckContractChecklist(List<Opportunity> opplst){
        Set<Id> setId = new Set<Id>();
          Id MouParentrcdtypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('MOU').getRecordTypeId();
      Id MourelatedrcdtypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('MOU - Related Project').getRecordTypeId();

        for(Opportunity opp :opplst){
            
            setId.add(opp.Id);
               
        }
        
        Map<Id,Integer> ccmap = new Map<Id,Integer>();
        List<Contract_Checklist__c> cclst = [SELECT Id, Contract__c, Closeout_Checklist_Status__c FROM Contract_Checklist__c WHERE Contract__c IN :setId];
        if(cclst.size() > 0){
            for(Contract_Checklist__c cc :cclst){
                if(!ccmap.containsKey(cc.Contract__c) && cc.Closeout_Checklist_Status__c == Label.Completed){
                    ccmap.put(cc.Contract__c, 1);
                }
                else if(ccmap.containsKey(cc.Contract__c)){
                    Integer value = ccmap.get(cc.Contract__c);
                    ccmap.put(cc.Contract__c, value + 1);
                }
            }
        }
        
        for(Opportunity opp :opplst){
            if(((ccmap.size() == 0 && opp.StageName == Label.Complete) || (ccmap.get(opp.Id) == 0 && opp.StageName == Label.Complete)) && (opp.recordtypeId!=MouParentrcdtypeId && opp.recordtypeId!=MourelatedrcdtypeId)){
                opp.addError(Label.Complete_Status_Not_Allowed);
                return false;
            }
        }
        return true;
    }
    /*
When a Lead is converted to an Opp, we want to share the new Opp with all the people that had access to the Lead. Those people are in 
the "Contacts" section of the Lead page layout. Those users have a LeadShare record with the lead that gives them the access.
In this trigger, we look at an Opportunity and get its associated Lead record and all of the LeadShare records for that lead. 
We put them in a map. Then we go through each corresponding LeadShare record and give them access using the Opportunity Team functionality.

Note: A process builder (Autofill Contacts On Project From LOI) that occurs after this actually fills in the fields onto Opportunity from LOI. 
This trigger just sets up the Opportunity Team.
*/
    
    //Kalyan Touched this method to handle better than it was before and touched code start here
    
    public void leadToOpp_ConversionSharing(List<Opportunity> oppList)
    {
        set<Id> ldids= new set<Id>();
        //trying to collect all leadids which are actually converted
        for(opportunity ot:oppList){
            ldids.add(ot.FGM_Portal__LOI__c);
        }
        //To get record Ids for Opportunity object
        
        
        List<OpportunityTeamMember> OppMemberInsertList = new List<OpportunityTeamMember>();
        Map<Id, list<LeadShare>> leadShareMap = new Map<Id, list<LeadShare>>();
        Map<Id, Id> oppandleadmap=new Map<Id, Id>(); 
        
        //Trying to get the lead shares of only those leads which are converted which we got from above set.
        List<LeadShare> allCurrentLeadShares = [SELECT id, userorgroupid, leadid, RowCause FROM LeadShare where leadid IN:ldids ORDER BY LeadId ASC];
        //Building my map with Lead Id as Key and Listofleadshares as corresponding values.
        for (LeadShare theShareRule : allCurrentLeadShares){
            if (!leadShareMap.containsKey(theShareRule.LeadId)){
                list<leadshare> temp=new list<leadshare>();
                temp.add(theShareRule);
                leadShareMap.put(theShareRule.LeadId,temp);
            }
            else{
                list<leadshare> temp=leadShareMap.get(theShareRule.LeadId);
                temp.add(theShareRule);
                leadShareMap.put(theShareRule.LeadId,temp);
            }
        }
        
        //Iterating the opportunity to get the relevant lead id and the respective lead shares and for them create opportunityteam members
        for (Opportunity theOpp : oppList) 
        {
            if((theOpp.RecordTypeId == recTypeNameWithIdMap.get(Label.RA_Applications)) || (theOpp.RecordTypeId == recTypeNameWithIdMap.get(Label.RA_Applications_Methods)) )
            {                             
                if(theOpp.FGM_Portal__LOI__c!=null){
                    for(LeadShare lshare:leadShareMap.get(theOpp.FGM_Portal__LOI__c)){
                        
                        if(lshare.RowCause != Label.Owner && lshare.RowCause != Label.Rule)
                        {
                            OpportunityTeamMember oppMember = new OpportunityTeamMember();
                            oppMember.OpportunityId = theOpp.Id;
                            oppMember.UserId = lshare.userorgroupid;
                            oppMember.OpportunityAccessLevel = Label.Edit;
                            
                            OppMemberInsertList.add(oppMember);
                        }                       
                    }
                }
            }
        }
        
        
        insert OppMemberInsertList;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    //Kalyan touched code ends here.
    /*-------------------------------------------------------------------------------------------------------------------------------
When a Lead is converted to an Opp, we want to share the new Opp with all the people that had access to the Lead. Those people are in 
the "Contacts" section of the Lead page layout. Those users have a LeadShare record with the lead that gives them the access.
In this trigger, we look at an Opportunity and get its associated Lead record and all of the LeadShare records for that lead. 
We put them in a map. Then we go through each corresponding LeadShare record and give them access using the Opportunity Team functionality.

Note: A process builder (Autofill Contacts On Project From LOI) that occurs after this actually fills in the fields onto Opportunity from LOI. 
This trigger just sets up the Opportunity Team.
--------------------------------------------------------------------------------------------------------------------------------*/
    static void LOI_Attachments_ToOpp(List<Opportunity> oppList)
    {
        ///////////////////////////////////////// Begin Trigger Setup ///////////////////////////////////
        
        Map<Id, FGM_Portal__Question_Attachment__c> questionAttachmentInsertList = new Map<Id, FGM_Portal__Question_Attachment__c>();
        List<ContentDocumentLink> FileInsertList = new List<ContentDocumentLink>();
        List<Attachment> AttachmentInsertList = new List<Attachment>();
        List<Note> NoteInsertList = new List<Note>();
        
        // Get all Question Attachments that do not currently point to an Opportunity
        List<FGM_Portal__Question_Attachment__c> qAttachmentList = [SELECT ID, Name, FGM_Portal__Inquiry__c, FGM_Portal__Opportunity__c
                                                                    FROM FGM_Portal__Question_Attachment__c
                                                                    WHERE FGM_Portal__Inquiry__c !=: Null];
        
        // Put all Question Attachments in a map                                                                
        Map<Id, FGM_Portal__Question_Attachment__c> qAttachmentMap = new Map<Id, FGM_Portal__Question_Attachment__c>();                                                               
        for (FGM_Portal__Question_Attachment__c qAttachment : qAttachmentList) 
        {
            qAttachmentMap.put(qAttachment.Id, qAttachment);
        }
        
        ///////////////////////////////////////// End Trigger Setup ////////////////////////////////////
        ///////////////////////////////////////// Begin Trigger Loop ///////////////////////////////////
        for (Opportunity opp : oppList)
        {
            // Question Attachments (FGM_Portal__Question_Attachment__c)
            // Make sure the field on Opp that points to the Lead is not Null
            if (opp.FGM_Portal__LOI__c != Null)
            {            
                // For every Question Attachment that points to Opportunity's Lead
                for (Id qAttachmentID : qAttachmentMap.keySet())
                {
                    FGM_Portal__Question_Attachment__c currentAttachment = qAttachmentMap.get(qAttachmentID);
                    
                    // If this Question Attachment's Lead field is the same as the Opportunity's Lead field
                    if (currentAttachment.FGM_Portal__Inquiry__c == opp.FGM_Portal__LOI__c) 
                    {
                        system.debug('QA match');
                        // Clone the attachment
                        FGM_Portal__Question_Attachment__c newAttachment = currentAttachment.clone(false, true);
                        
                        // Assign the new attachment to point to the Opportunity;
                        newAttachment.FGM_Portal__Opportunity__c = opp.id;
                        
                        // Match the ID's of the attachment so we can update
                        newAttachment.id = currentAttachment.Id;
                        
                        questionAttachmentInsertList.put(newAttachment.Id, newAttachment);
                    }
                }
                
                // Files (ContentDocument, ContentDocumentLink)
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // THIS MAY CAUSE DML/BULKIFICATION PROBLEMS!
                //  We cant use a map with all of the attachments because it will overload the heap
                List<ContentDocumentLink> ContentDocumentListThisOppsLead = [SELECT linkedEntityID, contentDocumentID, isDeleted,
                                                                             shareType, Visibility
                                                                             FROM ContentDocumentLink                                                            
                                                                             WHERE linkedEntityID =: opp.FGM_Portal__LOI__c];
                System.debug(LoggingLevel.ERROR, 'contentDocNoteSize: ' + ContentDocumentListThisOppsLead.size());
                for (ContentDocumentLink theFile : ContentDocumentListThisOppsLead)
                {
                    ContentDocumentLink newFile = theFile.clone(false, true);
                    newFile.visibility = Label.AllUsers;
                    newFile.linkedEntityID = opp.id;
                    FileInsertList.add(newFile);
                }
                
                
                
                // Attachments (Attachments)
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // THIS MAY CAUSE DML/BULKIFICATION PROBLEMS!
                //  We cant use a map with all of the notes because it will overload the heap
                List<Attachment> attachmentsThisOpp = [SELECT ID, Body, IsPrivate, OwnerId, ParentId, Name, createdById, contentType, bodylength
                                                       FROM Attachment
                                                       WHERE ParentId =: opp.FGM_Portal__LOI__c
                                                       AND isDeleted =: false];
                System.debug(LoggingLevel.ERROR, 'attachListSize: ' + attachmentsThisOpp.size());
                for (Attachment theAttachment : attachmentsThisOpp)
                {
                    Attachment newAttachment = theAttachment.clone(false, true);
                    //newAttachment.id = theAttachment.Id;
                    newAttachment.parentID = opp.id;
                    AttachmentInsertList.add(newAttachment);
                }  
                
                // Notes (Notes)
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // THIS MAY CAUSE DML/BULKIFICATION PROBLEMS!
                //  We cant use a map with all of the notes because it will overload the heap
                List<Note> NoteListThisOppsLead = [SELECT ID, Body, IsPrivate, OwnerId, ParentId, Title
                                                   FROM Note
                                                   WHERE ParentId =: opp.FGM_Portal__LOI__c];
                System.debug(LoggingLevel.Error, 'notesListSize: ' + NoteListThisOppsLead.size());
                //for (ID attachmentID : NotesAndAttachmentMap.keySet())
                for (Note theNote : NoteListThisOppsLead)
                {
                    //Attachment theAttachment = NotesAndAttachmentMap.get(attachmentID);
                    Note newNote = theNote.clone(false, true);
                    //newAttachment.id = theAttachment.Id;
                    newNote.parentID = opp.id;
                    NoteInsertList.add(newNote);
                }
            }
        }
        ///////////////////////////////////////// End Trigger Loop ///////////////////////////////////
        update questionAttachmentInsertList.values();
        //System.debug(LoggingLevel.Error, 'insert_N_A_size: ' + AttachmentInsertList.size());
        insert AttachmentInsertList;
        insert FileInsertList;
        insert NoteInsertList;
    }
    
    
    
    /*-----------------------------------------------------------------------------------------------------------------------------------
Code Written at by SADC PCORI Team: Luis M. Rocha
Written During Development Phase 2: June/2016 - September/2016
Application ID: AI-0866             AI-0885
Requirement ID: RAr2_C4_010         RAr2_C4_022

Note: When a new review is created the trigger is called and gets the ID's of the record types. Then all the reviews are collected into a list. 
Then it checks to see if the checkbox [project.Create_Online_Reviews__c] has been checked. It will also check that the panel is finalized.
If those are both true. A new review record will be created with the assigned reviewer by the matrix. 

Once review recors are created a manual sharing rule is ran. View [ReviewManualSharing] for more details
-------------------------------------------------------------------------------------------------------------------------------------*/
    //Kalyan again touched the code to ensure the owner is the contacts's user himself instead of the Panel MRO.
    //Kalyan Touched code starts here
    public void createOpportunityReviews(List<Opportunity> oppList)
    {
        //To get record Ids for Opportunity object
        List<FC_Reviewer__External_Review__c> reviewListToInsert = new List<FC_Reviewer__External_Review__c>(); //Reviews to be inserted. 
        Map<Id,Id> reviewsMap  = new Map<Id,Id>(); // Map of the reviews. 
        List<user> userlistforMap = new List<user>();
        // get the set of opp ID's
        // get reviewers id which is nothing but contactIds
        Set<Id> oppIds = new Set<Id>();
        Set<Id> contids = new Set<Id>();
        map<Id,Id> contactusermap= new map<Id,Id>();
        for (Opportunity opp : oppList)
        {
            oppIds.add(opp.id);
            if(opp.Reviewer_1__c!=null){
                contids.add(opp.Reviewer_1__c);
            }
            if(opp.Reviewer_2__c!=null){
                contids.add(opp.Reviewer_2__c);
            }
            if(opp.Reviewer_3__c!=null){
                contids.add(opp.Reviewer_3__c);
            }
            if(opp.Reviewer_4__c!=null){
                contids.add(opp.Reviewer_4__c);
            }
            if(opp.Reviewer_5__c!=null){
                contids.add(opp.Reviewer_5__c);
            }
        }
        //from the above got contactids preparing map to get contactId and respective userId
        userlistforMap=[select Id,contactId from User where contactId IN:contids];
        for(User u:userlistforMap){
            contactusermap.put(u.ContactId, u.Id);
        }
        List<FC_Reviewer__External_Review__c> reviewsList = [SELECT id, FC_Reviewer__Opportunity__c FROM FC_Reviewer__External_Review__c
                                                             WHERE FC_Reviewer__Opportunity__c IN: oppList];
        
        //Insert them all in a map to check they are no duplicates in the following logic. . 
        for(FC_Reviewer__External_Review__c Review : reviewsList){
            reviewsMap.put(Review.FC_Reviewer__Opportunity__c, Review.Id);
        }
        
        
        for (Opportunity project : oppList)
        {
            if((project.RecordTypeId == recTypeNameWithIdMap.get(Label.RA_Applications)) || (project.RecordTypeId == recTypeNameWithIdMap.get(Label.RA_Applications_Methods)) )
            {              
                
                //If there are already reviews with the project id then this will not fire. Otherwise continue
                if(!reviewsMap.containsKey(project.Id))
                {
                    Id recordid = recTypeNameWithIdMap.get(project.Online_Review_Record_Type__c);
                    
                    if(project.Create_Online_Reviews__c)//Has the checkbox been checked.
                    {
                        if(project.Panel_Finalized__c)//Is the panel finalized?
                        {
                            system.debug('*****'+project.Campaign.Name);
                           
                                //Verify that all reviewer are filled in. Change to is empty
                                if(!String.isBlank(project.Reviewer_1__c) &&
                                   !String.isBlank(project.Reviewer_2__c) &&  
                                   !String.isBlank(project.Reviewer_3__c) &&
                                   !String.isBlank(project.Reviewer_4__c) &&
                                   !String.isBlank(project.Reviewer_5__c) )
                                { 
                                    /* When the rest of the piclklist are added delete above line and uncomment the line below. And erase the above if statement*/
                                    //if(project.Reviewer_1__c != null &&  project.reviewer_2__c != null && project.reviewer_3__c != null && project.reviewer_4__c != null &&project.reviewer_5__c != null){ 
                                    System.debug('The MRO id:' + Project.Panel__r.MRO__c);
                                    
                                    reviewListToInsert.add(CreateReviews(project,'1',recordid,project.Reviewer_1__c,contactusermap.get(project.Reviewer_1__c)));
                                    reviewListToInsert.add(CreateReviews(project,'2',recordid,project.Reviewer_2__c,contactusermap.get(project.Reviewer_2__c)));
                                    reviewListToInsert.add(CreateReviews(project,'3',recordid,project.Reviewer_3__c,contactusermap.get(project.Reviewer_3__c)));                                                       
                                    reviewListToInsert.add(CreateReviews(project,'4',recordid,project.Reviewer_4__c,contactusermap.get(project.Reviewer_4__c)));
                                    reviewListToInsert.add(CreateReviews(project,'5',recordid,project.Reviewer_5__c,contactusermap.get(project.Reviewer_5__c)));
                                    
                                }
                                else{
                                    //System.debug('One of the reviewers is empty');
                                }
                            
                            
                           
                        }
                    }
                }   
            }
        }
        //Insert the list of reviews for all projects that need to be changed. 
        insert reviewListToInsert;
    }    
    //Kalyan Touched code ends here
    
    // If the status is changed to completed, all the oppShare records need to be read, along with a few other minor tweaks
    public static void UpdateSharingToReadIfProjectCompleted (List<Opportunity> oppList, Map<Id, Opportunity> oldMap)
    {
        try{
            Set<Id> oppIds = new Set<Id>();
            Set<Id> userSetIds = new Set<Id>();
            Set<Id> setOwnerId = new Set<Id>();
            Profile p = [SELECT Id FROM Profile WHERE Name =: Label.PCORI_Community_Partner LIMIT 1];
            for(Opportunity opp :oppList){
                if(opp.StageName == Label.Complete || opp.StageName == Label.Terminated){
                    oppIds.add(opp.Id);
                    setOwnerId.add(opp.OwnerId);
                }
            }
            List<OpportunityTeamMember> otmlst1 = new List<OpportunityTeamMember>();
            List<OpportunityTeamMember> otmlst = [SELECT UserId, OpportunityAccessLevel, OpportunityId FROM OpportunityTeamMember WHERE OpportunityAccessLevel =: Label.Edit AND OpportunityId IN :oppIds];
            if(otmlst.size() > 0){
                Set<Id> userIds = new Set<Id>();
                for(OpportunityTeamMember otm :otmlst){
                    userIds.add(otm.UserId);
                }
                List<User> usrlst = [SELECT Id, FirstName FROM User WHERE ProfileId = :p.Id AND Id IN :userIds];
                
                for(User usr :usrlst){
                    userSetIds.add(usr.Id);
                }
                
                for(OpportunityTeamMember otm :otmlst){
                    if(userSetIds.contains(otm.UserId)){
                        otmlst1.add(otm);
                    }
                }
                
                for(OpportunityTeamMember otm :otmlst1){
                    otm.OpportunityAccessLevel = Label.Read;
                }
            }
            
            List<OpportunityTeamMember> otmlstToInsert = new List<OpportunityTeamMember>();
            for(Opportunity opp :oppList){
                Id partnerId = oldMap.get(opp.Id).OwnerId;
                if(!userSetIds.contains(partnerId) && (opp.StageName == Label.Complete || opp.StageName == Label.Terminated)){
                    OpportunityTeamMember otm = new OpportunityTeamMember();
                    otm.OpportunityAccessLevel = Label.Read;
                    otm.OpportunityId = opp.Id;
                    otm.TeamMemberRole = Label.PI_Designee;
                    otm.UserId = partnerId;
                    otmlstToInsert.add(otm);
                    
                    System.debug('List to Insert ----->' + otmlstToInsert);
                }         
                
                update otmlst1;
                insert otmlstToInsert;    
            }
        }
        catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());       
        }
        catch(Exception e)  {
            System.debug('The following exception has occurred: ' + e.getMessage());
        }// try catch block   
        
    }
    
    
    
    public static FC_Reviewer__External_Review__c CreateReviews(Opportunity project, String reviewerNumber,Id rt, String reviewer, Id user){
        
        FC_Reviewer__External_Review__c tempReview = new FC_Reviewer__External_Review__c();
        tempReview.Panel__c = project.Panel__c;
        tempReview.FC_Reviewer__Contact__c = reviewer;//project.Reviewer_1__c;
        tempReview.FC_Reviewer__Deadline__c = project.Online_Review_Deadline__c;
        tempReview.FC_Reviewer__Opportunity__c = project.Id;
        tempReview.FC_Reviewer__Reviewer_Type__c = reviewerNumber;
        tempReview.FC_Reviewer__Status__c = Label.Ready_to_Review;
        tempReview.RecordTypeId = rt;
        tempReview.Panel_MRO__c= project.Panel_MRO_Lookup_Formula__c;
        tempReview.OwnerId = user;
        return tempReview;
    }
    
    
}