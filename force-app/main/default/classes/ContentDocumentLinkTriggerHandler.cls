/*
* Apex Class : ContentDocumentLinkTriggerHandler 
* Author : Kalyan Vuyyuru (PCORI)
* Version History : 1.0
* Description : This is a Handlerclass called from autoPopulateLink on after insert to share files to the chatter group "Files Sharing Group"
*/
public class ContentDocumentLinkTriggerHandler {
    
    /**
* filesharingmethodtogrouponfilesinsertmethod : This is a method called from autoPopulateLink on after insert to share files to the chatter group "Files Sharing Group"
* @param  cvlist: cvlist is a list of all new files inserted(Trigger.new) on ContentDocumentLink(Files).
*/
    public static void filesharingmethodtogrouponfilesinsert(list<ContentDocumentLink> cvlist){
        Set<Id> contentDocumentIdSet = new Set<Id>();
        Set<Id> contentDocumentIdSet2 = new Set<Id>();
        Set<Id> contentDocumentIdSet3 = new Set<Id>();
        Set<Id> contentDocumentIdSet4 = new Set<Id>();
        for(ContentDocumentLink cv:cvlist)
        {
            if(cv.ContentDocumentId != null)
            {
                contentDocumentIdSet.add(cv.ContentDocumentId);
            }
        }
        List<ContentDocumentLink> lstContentDocumentlink = [SELECT ContentDocumentId, LINKEDENTITYID
                                                            FROM ContentDocumentLink WHERE ContentDocumentId IN:contentDocumentIdSet];
        for(ContentDocumentLink lcd:lstContentDocumentlink)
        {
            if(lcd.ContentDocumentId != null)
            {
                contentDocumentIdSet2.add(lcd.ContentDocumentId);
                contentDocumentIdSet3.add(lcd.LINKEDENTITYID);
                if(lcd.LinkedEntityId!=null){
                    string str = lcd.linkedentityid;
                    string str1 = str.substring(0,3);
                    
                    if(str1 == '006'){
                        contentDocumentIdSet4.add(str);
                    }
                }    
            }
        }
        List<ContentDocumentLink> ContentDocumentList = new List<ContentDocumentLink>();
        CollaborationGroup FilessharingGroup = new CollaborationGroup();
        if(!test.isRunningTest()){
            FilessharingGroup=[SELECT Id,Name From CollaborationGroup WHERE Name=:Label.Group_Name_for_Files_Sharing];
        }
        else {
            String orgId = UserInfo.getOrganizationId();
            String dateString = 
                String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
            Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
            String uniqueName = orgId + dateString + randomInt;
            
            CollaborationGroup newGroup = new CollaborationGroup(Name=uniqueName,CollaborationType='Public');
            insert newGroup;
            FilessharingGroup.Id=newGroup.Id;
        }
       
        for(ContentDocumentLink cv:cvlist)
        {
            
            if(cv.ContentDocumentId != null  && contentDocumentIdSet2.contains(cv.ContentDocumentId)  && !contentDocumentIdSet3.contains(FilessharingGroup.Id) && !contentDocumentIdSet4.isEmpty())
            {
                ContentDocumentLink cdl = new ContentDocumentLink(ContentDocumentId=cv.ContentDocumentId,LINKEDENTITYID =FilessharingGroup.Id,SHARETYPE='C');
                ContentDocumentList.add(cdl);
            }
        }
        if(!ContentDocumentList.isempty() && ContentDocumentList!=null){
            if(recursiveTrigger.runOnce()){
            insert ContentDocumentList;
            }
        }
     
    }
}