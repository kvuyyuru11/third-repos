//This is a Class for incorporating Flow page after clicking start my application
global class CommunitiesMeritReviewerController {
 global integer i {get;set;}
    global integer k {get;set;}
 public list<Merit_Reviewer_Application__c> MRlist1{get;set;}
 public Merit_Reviewer_Application__c MRreject{get;set;}
 global boolean showbutton{get;set;}
 global boolean showhide{get;set;}
 global boolean showsubmit{get;set;}
    global boolean rejeshow{get;set;}   
 global contact mycon;
    //global string USA;
    //global string Canada;
    //global string Puerto_Rico;
    //global string US_Virgin_Islands  ;
    //global string Mexico;
    global CommunitiesMeritReviewerController () {
    user us=[select id,contactid from user where id=:userinfo.getUserId()];
        mycon= [select id,Age__c,MailingCity ,MailingCountry ,Current_Employer__c,Current_Position_or_Title__c,Federal_Employee__c,Gender__c,Race__c,MailingState,MailingPostalcode,phone,mailingstreet,Reviewer_Status__c,Armed_Services__c
              from contact where Id=:us.contactid];
   
        i = [select count() from Merit_Reviewer_Application__c where OwnerId=:userinfo.getUserId() ];
         k = [select count() from Merit_Reviewer_Application__c where OwnerId=:userinfo.getUserId() AND Application_Status__c='Not Accepted as Reviewer'];
         system.debug('the value of***************************'+k);
        if(k>0){
             rejeshow=true;
        }else{
            rejeshow=false;
        }
         system.debug('the value of***************************'+rejeshow);
                    if(i>0){
             showhide=false;
             }else{
             showhide=true;
             }
        if(i>0){
         MRlist1 = [select id,Application_Status__c,Name, Reviewer_Role__c,rejected__c from Merit_Reviewer_Application__c where OwnerId=:userinfo.getUserId() And rejected__c=false limit 1];
         for(Merit_Reviewer_Application__c MRlist:MRlist1){

                   
                     if(MRlist.Application_Status__c =='Ready for Review'||MRlist.Application_Status__c=='Under Review'||MRlist.Application_Status__c=='Conditionally Accepted'||MRlist.Application_Status__c=='Expired'){
                 showsubmit=true;
                         rejeshow=false;
                 } 
                 if(MRlist.Application_Status__c=='Not Accepted as Reviewer' && MRlist.rejected__c==false ){
                  showhide=true;
                 // showbutton=true;
                  system.debug('the case value of the *************************************'+ showhide);
                 }
                  if(  MRlist.Application_Status__c == 'Draft'){
                      showbutton=true;
                      rejeshow=false;
                      //showhide=false;
                      }else{
                      showbutton=false;
                      //showhide=true;
                    }
            }
            }
 
   }      
     
 
    
global PageReference Flow(){
     
    list<Merit_Reviewer_Application__c> mra=new list<Merit_Reviewer_Application__c>();
     list<Merit_Reviewer_Application__c> mra1=new list<Merit_Reviewer_Application__c>();
    Integer j=[select count() from Merit_Reviewer_Application__c where OwnerId=:userinfo.getUserId() AND Application_Status__c='Not Accepted as Reviewer' ];
    if(j>0){
       mra = [select id,Application_Status__c,Name, Reviewer_Role__c,rejected__C from Merit_Reviewer_Application__c where OwnerId=:userinfo.getUserId() AND Application_Status__c='Not Accepted as Reviewer' AND rejected__c=false];   
        for(merit_reviewer_application__c mrr:mra){
            mrr.rejected__c=true;
            mra1.add(mrr);
        }
        if(!mra1.isempty()) update mra1;
    }

    
        Merit_Reviewer_Application__c mr=new Merit_Reviewer_Application__c();
         mr.Country__c=mycon.MailingCountry ;

   if( mr.Country__c=='USA' ||mr.Country__c=='United States'|| mr.Country__c=='Canada' || mr.Country__c=='Mexico' || mr.Country__c=='Puerto Rico'|| mr.Country__c=='US Virgin Islands'){ 
        //mr.Age__c=mycon.Age__c;
        mr.Reviewer_Name__c=mycon.id;
        mr.City__c= mycon.MailingCity;
        //mr.Current_Employer__c=mycon.Current_Employer__c;
        //mr.Current_Position_or_Title__c=mycon.Current_Position_or_Title__c;
        mr.Federal_Employee__c=mycon.Federal_Employee__c;
        mr.Armed_Services__c=mycon.Armed_Services__c;
        mr.Gender__c=mycon.Gender__c;
       mr.Highest_Degree_of_Education_New__c='';
        mr.Highest_Level_of_Education__c='';
        mr.Number_of_Publications__c='';
        mr.Number_of_Scientific_Panels__c='';
       // mr.Patient_Communities__c='';
        mr.patient_coommunities__c='';
        mr.Phone__c=mycon.phone;
        mr.Race__c=mycon.Race__c;
        mr.Reviewer_Role__c='';
        mr.Served_as_Chair_or_Co_Chair__c='';
       // mr.Stakeholder_Communities__c='';
        mr.State__c=mycon.MailingState;
        mr.Street_Address__c=mycon.mailingstreet;
        mr.Zip_Code__c=mycon.MailingPostalcode;  
        //mr.Current_Position_or_Title__c=mycon.Reviewer_Status__c;  
       insert mr;  
        Long startTime = DateTime.now().getTime();
        Long finishTime = DateTime.now().getTime();
        while ((finishTime - startTime) < 3500) {
            
            finishTime = DateTime.now().getTime();
        }
       PageReference pg = new pagereference('/apex/Stylesheet');
       pg.setRedirect(true);
        return pg;
  }
    else{
         ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Dear applicant, if you live outside of North America, we cannot accept your application. Thank you in advance for your understanding.'));
         return null;
    }
    
    
    
 }
    
    global pagereference open(){
       
       // Merit_Reviewer_Application__c m= new Merit_Reviewer_Application__c();
        //Integer i;
        PageReference MRAPage;
        i = [select count() from Merit_Reviewer_Application__c where OwnerId=:userinfo.getUserId() and Application_Status__c = 'Draft'];
        if(i>0){
            //MRAPage = new pagereference ('/apex/MRAPage?resume=true');
            MRAPage = new pagereference ('/apex/Stylesheet');
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'There is no Resume application, Please create a new one.'));
            MRAPage.setRedirect(true);
        }else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'You do not currently have a saved draft application. Please click on the “Start My Application” button to start a new application.'));
        }
        return MRAPage;
        
        
    }
      
        
    }