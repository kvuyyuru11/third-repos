@isTest
private class opportunityService_Test
{ 
	@testSetup
	private static void testDataCreation() {
		        
        List<Account> accts = TestDataFactory.getAccountTestRecords(1);
        
        List<User> users = TestDataFactory.getpartnerUserTestRecords(1, 30);

        List<Panel__c> panels = new List<Panel__c>();

        for (Panel__c pan : TestDataFactory.getpaneltestrecords(3)){
        	pan.Panel_Finalized__c = False;
        	panels.add(pan);
        }
        update panels;

        List<Opportunity> opportunityList = new List<Opportunity>();

        for(integer i=0; i<60; i++){
        	Opportunity opp = new Opportunity();
        	opp.Name = 'Assigning Reviewer '+ i;
        	opp.Project_Name_off_layout__c = 'Assigning Reviewer '+ i;
        	opp.Closedate = system.today();
        	opp.RecordTypeId = (Id)('01239000000Hr50');
        	opp.StageName = 'Programmatic Review';
        	opp.Application_Number__c = String.valueOf(i);
        	opp.AccountId = accts[0].Id;
        	if(Math.floor(i/10) < 2){
        		opp.Panel__c = panels[0].Id;
        	}
        	else if (Math.floor(i/10) < 4){
        		opp.Panel__c = panels[1].Id;
        	}
        	else
        	{
        		opp.Panel__c = panels[2].Id;
        	}
        	opportunityList.add(opp);

        }
        insert opportunityList;
        system.assertEquals(opportunityList.size(),60);
		List<Panel_Assignment__c> panelAssignment = new List <Panel_Assignment__c>();
        integer i=0;
        for(User u : users){
        	Panel_Assignment__c panAssign = new Panel_Assignment__c();
        	panAssign.Panel__c = panels[(Integer)Math.floor(i/10)].Id;
        	panAssign.Reviewer_Name__c = u.contactId;
        	panAssign.Panel_Role__c = 'Reviewer';
        	panAssign.Active__c = True;
        	panelAssignment.add(panAssign);
        	i++;

        }
        insert panelAssignment;

        


	}
	
	/*private static testMethod void singleTestForValidationOnApplicationsForCOI()
	{
		List<Panel__c> panels = [Select Id, Name from Panel__c Limit 1];
		List<COI_Expertise__c> coiList = [select Id, Name, Related_Project__c, Reviewer_Name__c,Conflict_of_Interest__c, PFA_Level_COI__c, RecordType.Name from COI_Expertise__c Where Panel_R2__c =: panels[0].Id];
		List<Opportunity> opportunityList = [select Id, Name, Reviewer_1__c, Reviewer_2__c, Reviewer_3__c, Reviewer_4__c, Reviewer_5__c, Panel__c from Opportunity Where Panel__c =: panels[0].Id Limit 3];
		List<Panel_Assignment__c> panelAssignment = [select Id,Panel__c, Reviewer_Name__c from Panel_Assignment__c where Panel__c =: panels[0].Id Limit 1];
		List<COI_Expertise__c> pfaCoiListUpdateList = new List<COI_Expertise__c>();
		List<COI_Expertise__c> applicatyionCoiListUpdateList = new List<COI_Expertise__c>();
		
		//update pfaCoiListUpdateList;

		opportunityList[0].Reviewer_1__c = panelAssignment[0].Reviewer_Name__c;
		try {
			update opportunityList[0];
		} catch (exception e) {
			System.assert(e.getMessage().contains('Testcnt0 Testcnt0 has indicated COI for this PFA on the Panel'));
		}
Test.startTest();
		for(COI_Expertise__c coi : coiList){
			if(coi.Reviewer_Name__c == panelAssignment[0].Reviewer_Name__c && coi.RecordType.Name == 'PFA RecordType'){
				coi.PFA_Level_COI__c = 'No';
				pfaCoiListUpdateList.add(coi);
			}
			else if (coi.Reviewer_Name__c == panelAssignment[0].Reviewer_Name__c && coi.RecordType.Name == 'General Record Type'){
				if(coi.Related_Project__c == opportunityList[1].Id) {
					coi.Conflict_of_Interest__c = 'Professional';
					applicatyionCoiListUpdateList.add(coi);
				}
				else if (coi.Related_Project__c == opportunityList[2].Id) {
					coi.Conflict_of_Interest__c = 'No';
					applicatyionCoiListUpdateList.add(coi);
				} 
				
			}
		}
		update pfaCoiListUpdateList;
		update applicatyionCoiListUpdateList;
		opportunityList[1].Reviewer_1__c = panelAssignment[0].Reviewer_Name__c;
		opportunityList[2].Reviewer_1__c = panelAssignment[0].Reviewer_Name__c;
		
		try {
			
			update opportunityList[1];
			
			} catch (exception e) {
				System.assert(e.getMessage().contains('Testcnt0 Testcnt0 has indicated COI for this Application'));
			}

			try {
			
			update opportunityList[2];
			
			System.assertEquals(opportunityList[2].Reviewer_1__c, panelAssignment[0].Reviewer_Name__c);
			} catch (exception e) {
				
			}
		Test.stopTest();
	}*/

	private static testMethod void bulkTestForValidationOnApplicationsForCOI()
	{
		List<Panel__c> panels = [Select Id, Name from Panel__c Limit 1];
		List<COI_Expertise__c> coiList = [select Id, Name, Related_Project__c, Reviewer_Name__c,Conflict_of_Interest__c, PFA_Level_COI__c, RecordType.Name from COI_Expertise__c Where Panel_R2__c =: panels[0].Id];
		List<Opportunity> opportunityList = [select Id, Name, Reviewer_1__c, Reviewer_2__c, Reviewer_3__c, Reviewer_4__c, Reviewer_5__c, Panel__c from Opportunity Where Panel__c =: panels[0].Id];
		List<Panel_Assignment__c> panelAssignment = [select Id,Panel__c, Reviewer_Name__c from Panel_Assignment__c where Panel__c =: panels[0].Id];
		List<Opportunity> opportunityUpdateList = new List<Opportunity>();
		for (integer i=0; i<2; i++) {
			opportunityList[i].Reviewer_1__c = panelAssignment[0].Reviewer_Name__c;
			opportunityUpdateList.add(opportunityList[i]);
		}
		
		try {
			update opportunityUpdateList;
		} catch (exception e) {
			System.assert(e.getMessage().contains('Testcnt0 Testcnt0 has indicated COI for this PFA on the Panel'));
		}
		
	}	

}