@isTest
private class ActivityUtilsTest {
    
    static testMethod void mainTest() {
        list<opportunity> opplist = TestDataFactory.getopportunitytestrecords(1,7,1);        
        FGM_Base__Benchmark__c m1 = new FGM_Base__Benchmark__c();
        m1.Milestone_Name__c = 'Mielstone1';
        m1.Milestone_Status__c = 'Not Completed';
        m1.FGM_Base__Due_Date__c = System.now().date();
        //m.Milestone_ID__c = '1232';
        m1.FGM_Base__Request__c = opplist[0].id;
        m1.Milestone_Type__c = 'Recruitment';
        insert m1;
        FGM_Base__Benchmark__c m2 = new FGM_Base__Benchmark__c();
        m2.Milestone_Name__c = 'Mielstone1';
        m2.Milestone_Status__c = 'Not Completed';
        m2.FGM_Base__Due_Date__c = System.now().date();
        //m.Milestone_ID__c = '1232';
        m2.FGM_Base__Request__c = opplist[0].id;
        m2.Milestone_Type__c = 'Recruitment';
        insert m2;
        
        RecordType TaskType1 = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Task' AND Name = 'Project Task' LIMIT 1];
        RecordType TaskType2 = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Task' AND Name = 'Milestone Task' LIMIT 1];
        
        Task[] tList = new list<task>();
        
        for(Integer i=0; i<2; i++) {
            Task t = new Task();
            t.Status = 'Not Started';
            t.Priority = 'Normal';
            t.Type = 'Scheduled Call Back';
            
            if(i==0){ 
                t.WhatId = opplist[0].Id;
                t.RecordTypeId = TaskType1.id;
            }
            if(i==1)
            { 
                t.WhatId = m2.Id;
                t.RecordTypeId = TaskType2.id;
            }
            
            tList.add(t);
        }
        insert tList;
        //insert eList;
        
        test.startTest();
        
        system.assert(1 == [SELECT Total_Open_Tasks__c FROM FGM_Base__Benchmark__c WHERE Id = :m2.Id].size());
        
        test.stopTest();
        
    }
    
}