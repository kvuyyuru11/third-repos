@isTest
public class FilteredAuditTest{
    private static testMethod void testConstructor(){
        RecordType rec = [SELECT Id FROM RecordType WHERE DeveloperName = 'Site_Visit' AND sObjectType = 'Audit__c'];
        RecordType recOpp = [SELECT Id FROM RecordType WHERE DeveloperName = 'Research_Awards' AND sObjectType = 'Opportunity'];
        RecordType recAcc = [SELECT Id FROM RecordType WHERE DeveloperName = 'Standard_Salesforce_Account' AND sObjectType = 'Account'];
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.RecordTypeId = recAcc.Id;
        insert acc;
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = recOpp.Id;
        opp.AccountId = acc.Id;
        opp.Contract_Number__c = '03423';
        opp.Name = 'Test';
        opp.StageName = 'In Progress';
        opp.Application_Number__c = '12213';
        opp.Full_Project_Title__c = 'Project Test';
        opp.CloseDate = Date.newInstance(2016, 12, 22);
        insert opp;
        List<Audit__c> alst = new List<Audit__c>();
        for(Integer i = 0; i < 5; i++){
            Audit__c a = new Audit__c();
            a.RecordTypeId = rec.Id;
            a.Status__c = 'Sent to Awardee';
            a.Project__c = opp.Id;
            alst.add(a);
        }
        insert alst;
        FilteredAudit fa = new FilteredAudit(new ApexPages.StandardController(opp));
    }
}