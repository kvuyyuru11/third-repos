/*
* Apex Class : ChangeOwnerofRRCSrecords
* Author : Kalyan Vuyyuru (PCORI)
* Version History : 1.0
* Description : This class is called from Reviews Trigger to change the owner id of the review record if it is not equal to Internal reviewer when and 
RRCS review is created or edited.
*/

public class ChangeOwnerofRRCSrecords {
    
    
    public static void checkowneridandchangetointernalreviewerid(list<FC_Reviewer__External_Review__c> rrcsreviewids){
        list<FC_Reviewer__External_Review__c> listofrrcsreviewstoupdate = new list<FC_Reviewer__External_Review__c>();
        for(FC_Reviewer__External_Review__c rvss:rrcsreviewids){
            FC_Reviewer__External_Review__c r= new FC_Reviewer__External_Review__c(Id = rvss.Id);
            r.ownerid=rvss.Internal_Reviewer__c;
            listofrrcsreviewstoupdate.add(r);
        }
        
        database.update(listofrrcsreviewstoupdate);
    }

}